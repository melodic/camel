/**
 */
package camel.scalability;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Event Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.BinaryEventPattern#getLeftEvent <em>Left Event</em>}</li>
 *   <li>{@link camel.scalability.BinaryEventPattern#getRightEvent <em>Right Event</em>}</li>
 *   <li>{@link camel.scalability.BinaryEventPattern#getLowerOccurrenceBound <em>Lower Occurrence Bound</em>}</li>
 *   <li>{@link camel.scalability.BinaryEventPattern#getUpperOccurrenceBound <em>Upper Occurrence Bound</em>}</li>
 *   <li>{@link camel.scalability.BinaryEventPattern#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see camel.scalability.ScalabilityPackage#getBinaryEventPattern()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='binary_event_pattern_at_least_left_right no_timer_both_events_in_pattern no_same_left_right_event_binary_pattern binary_event_pattern_timer_one_event binary_event_pattern_occur_bounds'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot binary_event_pattern_at_least_left_right='Tuple {\n\tmessage : String = \'In BinaryEventPattern: \' + self.name + \' no left or right event has been specified\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.leftEvent &lt;&gt; null or self.rightEvent &lt;&gt; null)\n}.status' no_timer_both_events_in_pattern='Tuple {\n\tmessage : String = \'In BinaryEventPattern: \' + self.name + \' only one or zero events have been specified with no timer at all\'\n\t\t\t,\n\tstatus : Boolean = asError(self.oclAsType(EventPattern).timer = null implies (self.leftEvent &lt;&gt; null and self.rightEvent &lt;&gt; null))\n}.status' no_same_left_right_event_binary_pattern='Tuple {\n\tmessage : String = \'In BinaryEventPattern: \' + self.name + \' both the left and right events are the same\'\n\t\t\t,\n\tstatus : Boolean = asError(leftEvent &lt;&gt; null and rightEvent &lt;&gt; null implies leftEvent &lt;&gt; rightEvent)\n}.status' binary_event_pattern_timer_one_event='Tuple {\n\tmessage : String = \'In BinaryEventPattern: \' + self.name + \' both a timer as well as left and right events have been specified or only one \',\n\tstatus : Boolean = \n\t\t\t\t\tasError(self.oclAsType(EventPattern).timer &lt;&gt; null implies (self.leftEvent = null or self.rightEvent = null))\n}.status' binary_event_pattern_occur_bounds='Tuple {\n\tmessage : String = \'In BinaryEventPattern: \' + self.name + \' incorrect values were given for the lowerOccurrenceBound and upperOccurrenceBound attributes in conjunction with the use of the respective pattern operator: \' + operator.toString(),\n\tstatus : Boolean = \n\t\t\t\tasError((self.operator &lt;&gt; BinaryPatternOperatorType::REPEAT_UNTIL implies\n\t\t\t\t\t(self.lowerOccurrenceBound = 0 and self.upperOccurrenceBound = 0)) \n\t\t\t\t\tand (self.operator = BinaryPatternOperatorType::REPEAT_UNTIL \n\t\t\t\t\t\timplies (not (self.lowerOccurrenceBound = 0 and self.upperOccurrenceBound = 0) \n\t\t\t\t\t\t\tand (self.upperOccurrenceBound &gt; 0 implies self.lowerOccurrenceBound &lt;= upperOccurrenceBound)\n\t\t\t\t\t\t\tand \n\t\t\t\t\t\t\tif (self.lowerOccurrenceBound &gt;= 1.0) then\n\t\t\t\t\t\t\t\t(self.lowerOccurrenceBound / self.lowerOccurrenceBound.round()) = 1 and \n\t\t\t\t\t\t\t\t\t(if (self.upperOccurrenceBound &gt; 0) then (self.upperOccurrenceBound / self.upperOccurrenceBound.round()) = 1\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif)\n\t\t\t\t\t\t\telse \n\t\t\t\t\t\t\t\tif (self.lowerOccurrenceBound &lt;&gt; 0.0) then \n\t\t\t\t\t\t\t\t\tif (self.upperOccurrenceBound &lt;= 1.0) then true\n\t\t\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\telse\n\t\t\t\t\t\t\t\t\tif (self.upperOccurrenceBound &gt;= 1.0) then (self.upperOccurrenceBound / self.upperOccurrenceBound.round()) = 1\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t)\n\t\t\t\t\t))\n}.status'"
 * @generated
 */
public interface BinaryEventPattern extends EventPattern {
	/**
	 * Returns the value of the '<em><b>Left Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Left Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Event</em>' reference.
	 * @see #setLeftEvent(Event)
	 * @see camel.scalability.ScalabilityPackage#getBinaryEventPattern_LeftEvent()
	 * @model
	 * @generated
	 */
	Event getLeftEvent();

	/**
	 * Sets the value of the '{@link camel.scalability.BinaryEventPattern#getLeftEvent <em>Left Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left Event</em>' reference.
	 * @see #getLeftEvent()
	 * @generated
	 */
	void setLeftEvent(Event value);

	/**
	 * Returns the value of the '<em><b>Right Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Right Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Event</em>' reference.
	 * @see #setRightEvent(Event)
	 * @see camel.scalability.ScalabilityPackage#getBinaryEventPattern_RightEvent()
	 * @model
	 * @generated
	 */
	Event getRightEvent();

	/**
	 * Sets the value of the '{@link camel.scalability.BinaryEventPattern#getRightEvent <em>Right Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right Event</em>' reference.
	 * @see #getRightEvent()
	 * @generated
	 */
	void setRightEvent(Event value);

	/**
	 * Returns the value of the '<em><b>Lower Occurrence Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower Occurrence Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower Occurrence Bound</em>' attribute.
	 * @see #setLowerOccurrenceBound(double)
	 * @see camel.scalability.ScalabilityPackage#getBinaryEventPattern_LowerOccurrenceBound()
	 * @model required="true"
	 * @generated
	 */
	double getLowerOccurrenceBound();

	/**
	 * Sets the value of the '{@link camel.scalability.BinaryEventPattern#getLowerOccurrenceBound <em>Lower Occurrence Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower Occurrence Bound</em>' attribute.
	 * @see #getLowerOccurrenceBound()
	 * @generated
	 */
	void setLowerOccurrenceBound(double value);

	/**
	 * Returns the value of the '<em><b>Upper Occurrence Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper Occurrence Bound</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper Occurrence Bound</em>' attribute.
	 * @see #setUpperOccurrenceBound(double)
	 * @see camel.scalability.ScalabilityPackage#getBinaryEventPattern_UpperOccurrenceBound()
	 * @model required="true"
	 * @generated
	 */
	double getUpperOccurrenceBound();

	/**
	 * Sets the value of the '{@link camel.scalability.BinaryEventPattern#getUpperOccurrenceBound <em>Upper Occurrence Bound</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Occurrence Bound</em>' attribute.
	 * @see #getUpperOccurrenceBound()
	 * @generated
	 */
	void setUpperOccurrenceBound(double value);

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.scalability.BinaryPatternOperatorType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see camel.scalability.BinaryPatternOperatorType
	 * @see #setOperator(BinaryPatternOperatorType)
	 * @see camel.scalability.ScalabilityPackage#getBinaryEventPattern_Operator()
	 * @model required="true"
	 * @generated
	 */
	BinaryPatternOperatorType getOperator();

	/**
	 * Sets the value of the '{@link camel.scalability.BinaryEventPattern#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see camel.scalability.BinaryPatternOperatorType
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(BinaryPatternOperatorType value);

} // BinaryEventPattern
