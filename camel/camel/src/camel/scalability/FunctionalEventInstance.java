/**
 */
package camel.scalability;

import camel.deployment.ComponentInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Functional Event Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.FunctionalEventInstance#getComponentInstance <em>Component Instance</em>}</li>
 * </ul>
 *
 * @see camel.scalability.ScalabilityPackage#getFunctionalEventInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='functional_event_instance_correct_type functional_event_instance_correct_component'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot functional_event_instance_correct_type='Tuple {\n\tmessage : String = \'FunctionalEventInstance: \' + self.name + \' should have a functional event as type\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.type.oclIsTypeOf(FunctionalEvent))\n}.status' functional_event_instance_correct_component='Tuple {\n\tmessage : String = \'FunctionalEventInstance: \' + self.name + \' should refer to an instance of a component, which is referred to by its type\',\n\tstatus : Boolean = \n\t\t\t\tasError(componentInstance &lt;&gt; null implies (if type.oclIsTypeOf(FunctionalEvent) and type.oclAsType(FunctionalEvent).component &lt;&gt; null then type.oclAsType(FunctionalEvent).component = componentInstance.type else false endif))\n}.status'"
 * @generated
 */
public interface FunctionalEventInstance extends EventInstance {
	/**
	 * Returns the value of the '<em><b>Component Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Instance</em>' reference.
	 * @see #setComponentInstance(ComponentInstance)
	 * @see camel.scalability.ScalabilityPackage#getFunctionalEventInstance_ComponentInstance()
	 * @model
	 * @generated
	 */
	ComponentInstance getComponentInstance();

	/**
	 * Sets the value of the '{@link camel.scalability.FunctionalEventInstance#getComponentInstance <em>Component Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Instance</em>' reference.
	 * @see #getComponentInstance()
	 * @generated
	 */
	void setComponentInstance(ComponentInstance value);

} // FunctionalEventInstance
