/**
 */
package camel.scalability;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.scalability.ScalabilityPackage#getEvent()
 * @model abstract="true"
 * @generated
 */
public interface Event extends Feature {
} // Event
