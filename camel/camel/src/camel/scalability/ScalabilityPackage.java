/**
 */
package camel.scalability;

import camel.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see camel.scalability.ScalabilityFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface ScalabilityPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "scalability";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.camel-dsl.org/camel/scalability";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "scalability";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ScalabilityPackage eINSTANCE = camel.scalability.impl.ScalabilityPackageImpl.init();

	/**
	 * The meta object id for the '{@link camel.scalability.impl.ScalabilityModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.ScalabilityModelImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getScalabilityModel()
	 * @generated
	 */
	int SCALABILITY_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__NAME = CorePackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__DESCRIPTION = CorePackage.MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__ANNOTATIONS = CorePackage.MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__ATTRIBUTES = CorePackage.MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__SUB_FEATURES = CorePackage.MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__IMPORT_URI = CorePackage.MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Rules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__RULES = CorePackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Events</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__EVENTS = CorePackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Event Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__EVENT_INSTANCES = CorePackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__ACTIONS = CorePackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Patterns</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__PATTERNS = CorePackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Timers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL__TIMERS = CorePackage.MODEL_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL_FEATURE_COUNT = CorePackage.MODEL_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL___AS_ERROR__BOOLEAN = CorePackage.MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_MODEL_OPERATION_COUNT = CorePackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.EventImpl <em>Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.EventImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getEvent()
	 * @generated
	 */
	int EVENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.EventPatternImpl <em>Event Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.EventPatternImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getEventPattern()
	 * @generated
	 */
	int EVENT_PATTERN = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN__NAME = EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN__DESCRIPTION = EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN__ANNOTATIONS = EVENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN__ATTRIBUTES = EVENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN__SUB_FEATURES = EVENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Timer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN__TIMER = EVENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Event Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN_FEATURE_COUNT = EVENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN___AS_ERROR__BOOLEAN = EVENT___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Includes Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN___INCLUDES_EVENT__SINGLEEVENT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Includes Left Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN___INCLUDES_LEFT_EVENT__SINGLEEVENT = EVENT_OPERATION_COUNT + 1;

	/**
	 * The operation id for the '<em>Includes Right Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN___INCLUDES_RIGHT_EVENT__SINGLEEVENT = EVENT_OPERATION_COUNT + 2;

	/**
	 * The number of operations of the '<em>Event Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_PATTERN_OPERATION_COUNT = EVENT_OPERATION_COUNT + 3;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.BinaryEventPatternImpl <em>Binary Event Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.BinaryEventPatternImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getBinaryEventPattern()
	 * @generated
	 */
	int BINARY_EVENT_PATTERN = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN__NAME = EVENT_PATTERN__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN__DESCRIPTION = EVENT_PATTERN__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN__ANNOTATIONS = EVENT_PATTERN__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN__ATTRIBUTES = EVENT_PATTERN__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN__SUB_FEATURES = EVENT_PATTERN__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Timer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN__TIMER = EVENT_PATTERN__TIMER;

	/**
	 * The feature id for the '<em><b>Left Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN__LEFT_EVENT = EVENT_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Right Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN__RIGHT_EVENT = EVENT_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Lower Occurrence Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN__LOWER_OCCURRENCE_BOUND = EVENT_PATTERN_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Upper Occurrence Bound</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN__UPPER_OCCURRENCE_BOUND = EVENT_PATTERN_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN__OPERATOR = EVENT_PATTERN_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Binary Event Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN_FEATURE_COUNT = EVENT_PATTERN_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN___AS_ERROR__BOOLEAN = EVENT_PATTERN___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Includes Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN___INCLUDES_EVENT__SINGLEEVENT = EVENT_PATTERN___INCLUDES_EVENT__SINGLEEVENT;

	/**
	 * The operation id for the '<em>Includes Left Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN___INCLUDES_LEFT_EVENT__SINGLEEVENT = EVENT_PATTERN___INCLUDES_LEFT_EVENT__SINGLEEVENT;

	/**
	 * The operation id for the '<em>Includes Right Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN___INCLUDES_RIGHT_EVENT__SINGLEEVENT = EVENT_PATTERN___INCLUDES_RIGHT_EVENT__SINGLEEVENT;

	/**
	 * The number of operations of the '<em>Binary Event Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_EVENT_PATTERN_OPERATION_COUNT = EVENT_PATTERN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.UnaryEventPatternImpl <em>Unary Event Pattern</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.UnaryEventPatternImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getUnaryEventPattern()
	 * @generated
	 */
	int UNARY_EVENT_PATTERN = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN__NAME = EVENT_PATTERN__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN__DESCRIPTION = EVENT_PATTERN__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN__ANNOTATIONS = EVENT_PATTERN__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN__ATTRIBUTES = EVENT_PATTERN__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN__SUB_FEATURES = EVENT_PATTERN__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Timer</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN__TIMER = EVENT_PATTERN__TIMER;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN__EVENT = EVENT_PATTERN_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Occurrence Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN__OCCURRENCE_NUM = EVENT_PATTERN_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN__OPERATOR = EVENT_PATTERN_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Unary Event Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN_FEATURE_COUNT = EVENT_PATTERN_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN___AS_ERROR__BOOLEAN = EVENT_PATTERN___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Includes Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN___INCLUDES_EVENT__SINGLEEVENT = EVENT_PATTERN___INCLUDES_EVENT__SINGLEEVENT;

	/**
	 * The operation id for the '<em>Includes Left Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN___INCLUDES_LEFT_EVENT__SINGLEEVENT = EVENT_PATTERN___INCLUDES_LEFT_EVENT__SINGLEEVENT;

	/**
	 * The operation id for the '<em>Includes Right Event</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN___INCLUDES_RIGHT_EVENT__SINGLEEVENT = EVENT_PATTERN___INCLUDES_RIGHT_EVENT__SINGLEEVENT;

	/**
	 * The number of operations of the '<em>Unary Event Pattern</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_EVENT_PATTERN_OPERATION_COUNT = EVENT_PATTERN_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.SingleEventImpl <em>Single Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.SingleEventImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getSingleEvent()
	 * @generated
	 */
	int SINGLE_EVENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT__NAME = EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT__DESCRIPTION = EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT__ANNOTATIONS = EVENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT__ATTRIBUTES = EVENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT__SUB_FEATURES = EVENT__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Single Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT_FEATURE_COUNT = EVENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT___AS_ERROR__BOOLEAN = EVENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Single Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_EVENT_OPERATION_COUNT = EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.FunctionalEventImpl <em>Functional Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.FunctionalEventImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getFunctionalEvent()
	 * @generated
	 */
	int FUNCTIONAL_EVENT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT__NAME = SINGLE_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT__DESCRIPTION = SINGLE_EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT__ANNOTATIONS = SINGLE_EVENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT__ATTRIBUTES = SINGLE_EVENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT__SUB_FEATURES = SINGLE_EVENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Functional Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT__FUNCTIONAL_TYPE = SINGLE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT__COMPONENT = SINGLE_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Functional Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_FEATURE_COUNT = SINGLE_EVENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT___AS_ERROR__BOOLEAN = SINGLE_EVENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Functional Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_OPERATION_COUNT = SINGLE_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.NonFunctionalEventImpl <em>Non Functional Event</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.NonFunctionalEventImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getNonFunctionalEvent()
	 * @generated
	 */
	int NON_FUNCTIONAL_EVENT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT__NAME = SINGLE_EVENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT__DESCRIPTION = SINGLE_EVENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT__ANNOTATIONS = SINGLE_EVENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT__ATTRIBUTES = SINGLE_EVENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT__SUB_FEATURES = SINGLE_EVENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT__METRIC_CONSTRAINT = SINGLE_EVENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Violation</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT__IS_VIOLATION = SINGLE_EVENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Non Functional Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_FEATURE_COUNT = SINGLE_EVENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT___AS_ERROR__BOOLEAN = SINGLE_EVENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Non Functional Event</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_OPERATION_COUNT = SINGLE_EVENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.EventInstanceImpl <em>Event Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.EventInstanceImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getEventInstance()
	 * @generated
	 */
	int EVENT_INSTANCE = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_INSTANCE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_INSTANCE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_INSTANCE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_INSTANCE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_INSTANCE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_INSTANCE__STATUS = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_INSTANCE__LAYER = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_INSTANCE__TYPE = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Event Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_INSTANCE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_INSTANCE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Event Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_INSTANCE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.FunctionalEventInstanceImpl <em>Functional Event Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.FunctionalEventInstanceImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getFunctionalEventInstance()
	 * @generated
	 */
	int FUNCTIONAL_EVENT_INSTANCE = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE__NAME = EVENT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE__DESCRIPTION = EVENT_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE__ANNOTATIONS = EVENT_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE__ATTRIBUTES = EVENT_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE__SUB_FEATURES = EVENT_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE__STATUS = EVENT_INSTANCE__STATUS;

	/**
	 * The feature id for the '<em><b>Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE__LAYER = EVENT_INSTANCE__LAYER;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE__TYPE = EVENT_INSTANCE__TYPE;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE__COMPONENT_INSTANCE = EVENT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Functional Event Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE_FEATURE_COUNT = EVENT_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE___AS_ERROR__BOOLEAN = EVENT_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Functional Event Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTIONAL_EVENT_INSTANCE_OPERATION_COUNT = EVENT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.NonFunctionalEventInstanceImpl <em>Non Functional Event Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.NonFunctionalEventInstanceImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getNonFunctionalEventInstance()
	 * @generated
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE__NAME = EVENT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE__DESCRIPTION = EVENT_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE__ANNOTATIONS = EVENT_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE__ATTRIBUTES = EVENT_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE__SUB_FEATURES = EVENT_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Status</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE__STATUS = EVENT_INSTANCE__STATUS;

	/**
	 * The feature id for the '<em><b>Layer</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE__LAYER = EVENT_INSTANCE__LAYER;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE__TYPE = EVENT_INSTANCE__TYPE;

	/**
	 * The feature id for the '<em><b>Metric Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE__METRIC_INSTANCE = EVENT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Non Functional Event Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE_FEATURE_COUNT = EVENT_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE___AS_ERROR__BOOLEAN = EVENT_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Non Functional Event Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NON_FUNCTIONAL_EVENT_INSTANCE_OPERATION_COUNT = EVENT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.ScalabilityRuleImpl <em>Rule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.ScalabilityRuleImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getScalabilityRule()
	 * @generated
	 */
	int SCALABILITY_RULE = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_RULE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_RULE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_RULE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_RULE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_RULE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_RULE__EVENT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Actions</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_RULE__ACTIONS = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_RULE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_RULE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Rule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALABILITY_RULE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.ScalingActionImpl <em>Scaling Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.ScalingActionImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getScalingAction()
	 * @generated
	 */
	int SCALING_ACTION = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALING_ACTION__NAME = CorePackage.ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALING_ACTION__DESCRIPTION = CorePackage.ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALING_ACTION__ANNOTATIONS = CorePackage.ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALING_ACTION__ATTRIBUTES = CorePackage.ACTION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALING_ACTION__SUB_FEATURES = CorePackage.ACTION__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Software Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALING_ACTION__SOFTWARE_COMPONENT = CorePackage.ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Scaling Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALING_ACTION_FEATURE_COUNT = CorePackage.ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALING_ACTION___AS_ERROR__BOOLEAN = CorePackage.ACTION___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Scaling Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALING_ACTION_OPERATION_COUNT = CorePackage.ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.HorizontalScalingActionImpl <em>Horizontal Scaling Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.HorizontalScalingActionImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getHorizontalScalingAction()
	 * @generated
	 */
	int HORIZONTAL_SCALING_ACTION = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALING_ACTION__NAME = SCALING_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALING_ACTION__DESCRIPTION = SCALING_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALING_ACTION__ANNOTATIONS = SCALING_ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALING_ACTION__ATTRIBUTES = SCALING_ACTION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALING_ACTION__SUB_FEATURES = SCALING_ACTION__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Software Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALING_ACTION__SOFTWARE_COMPONENT = SCALING_ACTION__SOFTWARE_COMPONENT;

	/**
	 * The feature id for the '<em><b>Count</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALING_ACTION__COUNT = SCALING_ACTION_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Horizontal Scaling Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALING_ACTION_FEATURE_COUNT = SCALING_ACTION_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALING_ACTION___AS_ERROR__BOOLEAN = SCALING_ACTION___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Horizontal Scaling Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALING_ACTION_OPERATION_COUNT = SCALING_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.VerticalScalingActionImpl <em>Vertical Scaling Action</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.VerticalScalingActionImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getVerticalScalingAction()
	 * @generated
	 */
	int VERTICAL_SCALING_ACTION = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALING_ACTION__NAME = SCALING_ACTION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALING_ACTION__DESCRIPTION = SCALING_ACTION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALING_ACTION__ANNOTATIONS = SCALING_ACTION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALING_ACTION__ATTRIBUTES = SCALING_ACTION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALING_ACTION__SUB_FEATURES = SCALING_ACTION__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Software Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALING_ACTION__SOFTWARE_COMPONENT = SCALING_ACTION__SOFTWARE_COMPONENT;

	/**
	 * The number of structural features of the '<em>Vertical Scaling Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALING_ACTION_FEATURE_COUNT = SCALING_ACTION_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALING_ACTION___AS_ERROR__BOOLEAN = SCALING_ACTION___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Vertical Scaling Action</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALING_ACTION_OPERATION_COUNT = SCALING_ACTION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.impl.TimerImpl <em>Timer</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.impl.TimerImpl
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getTimer()
	 * @generated
	 */
	int TIMER = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__TYPE = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Time Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__TIME_VALUE = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Max Occurrence Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__MAX_OCCURRENCE_NUM = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER__UNIT = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Timer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Timer</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TIMER_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.scalability.BinaryPatternOperatorType <em>Binary Pattern Operator Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.BinaryPatternOperatorType
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getBinaryPatternOperatorType()
	 * @generated
	 */
	int BINARY_PATTERN_OPERATOR_TYPE = 16;

	/**
	 * The meta object id for the '{@link camel.scalability.TimerType <em>Timer Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.TimerType
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getTimerType()
	 * @generated
	 */
	int TIMER_TYPE = 17;

	/**
	 * The meta object id for the '{@link camel.scalability.UnaryPatternOperatorType <em>Unary Pattern Operator Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.UnaryPatternOperatorType
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getUnaryPatternOperatorType()
	 * @generated
	 */
	int UNARY_PATTERN_OPERATOR_TYPE = 18;

	/**
	 * The meta object id for the '{@link camel.scalability.StatusType <em>Status Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.scalability.StatusType
	 * @see camel.scalability.impl.ScalabilityPackageImpl#getStatusType()
	 * @generated
	 */
	int STATUS_TYPE = 19;


	/**
	 * Returns the meta object for class '{@link camel.scalability.ScalabilityModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see camel.scalability.ScalabilityModel
	 * @generated
	 */
	EClass getScalabilityModel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.scalability.ScalabilityModel#getRules <em>Rules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rules</em>'.
	 * @see camel.scalability.ScalabilityModel#getRules()
	 * @see #getScalabilityModel()
	 * @generated
	 */
	EReference getScalabilityModel_Rules();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.scalability.ScalabilityModel#getEvents <em>Events</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Events</em>'.
	 * @see camel.scalability.ScalabilityModel#getEvents()
	 * @see #getScalabilityModel()
	 * @generated
	 */
	EReference getScalabilityModel_Events();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.scalability.ScalabilityModel#getEventInstances <em>Event Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event Instances</em>'.
	 * @see camel.scalability.ScalabilityModel#getEventInstances()
	 * @see #getScalabilityModel()
	 * @generated
	 */
	EReference getScalabilityModel_EventInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.scalability.ScalabilityModel#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Actions</em>'.
	 * @see camel.scalability.ScalabilityModel#getActions()
	 * @see #getScalabilityModel()
	 * @generated
	 */
	EReference getScalabilityModel_Actions();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.scalability.ScalabilityModel#getPatterns <em>Patterns</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Patterns</em>'.
	 * @see camel.scalability.ScalabilityModel#getPatterns()
	 * @see #getScalabilityModel()
	 * @generated
	 */
	EReference getScalabilityModel_Patterns();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.scalability.ScalabilityModel#getTimers <em>Timers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Timers</em>'.
	 * @see camel.scalability.ScalabilityModel#getTimers()
	 * @see #getScalabilityModel()
	 * @generated
	 */
	EReference getScalabilityModel_Timers();

	/**
	 * Returns the meta object for class '{@link camel.scalability.Event <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event</em>'.
	 * @see camel.scalability.Event
	 * @generated
	 */
	EClass getEvent();

	/**
	 * Returns the meta object for class '{@link camel.scalability.EventPattern <em>Event Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Pattern</em>'.
	 * @see camel.scalability.EventPattern
	 * @generated
	 */
	EClass getEventPattern();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.EventPattern#getTimer <em>Timer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Timer</em>'.
	 * @see camel.scalability.EventPattern#getTimer()
	 * @see #getEventPattern()
	 * @generated
	 */
	EReference getEventPattern_Timer();

	/**
	 * Returns the meta object for the '{@link camel.scalability.EventPattern#includesEvent(camel.scalability.SingleEvent) <em>Includes Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Includes Event</em>' operation.
	 * @see camel.scalability.EventPattern#includesEvent(camel.scalability.SingleEvent)
	 * @generated
	 */
	EOperation getEventPattern__IncludesEvent__SingleEvent();

	/**
	 * Returns the meta object for the '{@link camel.scalability.EventPattern#includesLeftEvent(camel.scalability.SingleEvent) <em>Includes Left Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Includes Left Event</em>' operation.
	 * @see camel.scalability.EventPattern#includesLeftEvent(camel.scalability.SingleEvent)
	 * @generated
	 */
	EOperation getEventPattern__IncludesLeftEvent__SingleEvent();

	/**
	 * Returns the meta object for the '{@link camel.scalability.EventPattern#includesRightEvent(camel.scalability.SingleEvent) <em>Includes Right Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Includes Right Event</em>' operation.
	 * @see camel.scalability.EventPattern#includesRightEvent(camel.scalability.SingleEvent)
	 * @generated
	 */
	EOperation getEventPattern__IncludesRightEvent__SingleEvent();

	/**
	 * Returns the meta object for class '{@link camel.scalability.BinaryEventPattern <em>Binary Event Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Event Pattern</em>'.
	 * @see camel.scalability.BinaryEventPattern
	 * @generated
	 */
	EClass getBinaryEventPattern();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.BinaryEventPattern#getLeftEvent <em>Left Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Left Event</em>'.
	 * @see camel.scalability.BinaryEventPattern#getLeftEvent()
	 * @see #getBinaryEventPattern()
	 * @generated
	 */
	EReference getBinaryEventPattern_LeftEvent();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.BinaryEventPattern#getRightEvent <em>Right Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Right Event</em>'.
	 * @see camel.scalability.BinaryEventPattern#getRightEvent()
	 * @see #getBinaryEventPattern()
	 * @generated
	 */
	EReference getBinaryEventPattern_RightEvent();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.BinaryEventPattern#getLowerOccurrenceBound <em>Lower Occurrence Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Lower Occurrence Bound</em>'.
	 * @see camel.scalability.BinaryEventPattern#getLowerOccurrenceBound()
	 * @see #getBinaryEventPattern()
	 * @generated
	 */
	EAttribute getBinaryEventPattern_LowerOccurrenceBound();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.BinaryEventPattern#getUpperOccurrenceBound <em>Upper Occurrence Bound</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upper Occurrence Bound</em>'.
	 * @see camel.scalability.BinaryEventPattern#getUpperOccurrenceBound()
	 * @see #getBinaryEventPattern()
	 * @generated
	 */
	EAttribute getBinaryEventPattern_UpperOccurrenceBound();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.BinaryEventPattern#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see camel.scalability.BinaryEventPattern#getOperator()
	 * @see #getBinaryEventPattern()
	 * @generated
	 */
	EAttribute getBinaryEventPattern_Operator();

	/**
	 * Returns the meta object for class '{@link camel.scalability.UnaryEventPattern <em>Unary Event Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Event Pattern</em>'.
	 * @see camel.scalability.UnaryEventPattern
	 * @generated
	 */
	EClass getUnaryEventPattern();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.UnaryEventPattern#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see camel.scalability.UnaryEventPattern#getEvent()
	 * @see #getUnaryEventPattern()
	 * @generated
	 */
	EReference getUnaryEventPattern_Event();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.UnaryEventPattern#getOccurrenceNum <em>Occurrence Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Occurrence Num</em>'.
	 * @see camel.scalability.UnaryEventPattern#getOccurrenceNum()
	 * @see #getUnaryEventPattern()
	 * @generated
	 */
	EAttribute getUnaryEventPattern_OccurrenceNum();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.UnaryEventPattern#getOperator <em>Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Operator</em>'.
	 * @see camel.scalability.UnaryEventPattern#getOperator()
	 * @see #getUnaryEventPattern()
	 * @generated
	 */
	EAttribute getUnaryEventPattern_Operator();

	/**
	 * Returns the meta object for class '{@link camel.scalability.SingleEvent <em>Single Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Event</em>'.
	 * @see camel.scalability.SingleEvent
	 * @generated
	 */
	EClass getSingleEvent();

	/**
	 * Returns the meta object for class '{@link camel.scalability.FunctionalEvent <em>Functional Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Functional Event</em>'.
	 * @see camel.scalability.FunctionalEvent
	 * @generated
	 */
	EClass getFunctionalEvent();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.FunctionalEvent#getFunctionalType <em>Functional Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Functional Type</em>'.
	 * @see camel.scalability.FunctionalEvent#getFunctionalType()
	 * @see #getFunctionalEvent()
	 * @generated
	 */
	EReference getFunctionalEvent_FunctionalType();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.FunctionalEvent#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component</em>'.
	 * @see camel.scalability.FunctionalEvent#getComponent()
	 * @see #getFunctionalEvent()
	 * @generated
	 */
	EReference getFunctionalEvent_Component();

	/**
	 * Returns the meta object for class '{@link camel.scalability.NonFunctionalEvent <em>Non Functional Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Non Functional Event</em>'.
	 * @see camel.scalability.NonFunctionalEvent
	 * @generated
	 */
	EClass getNonFunctionalEvent();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.NonFunctionalEvent#getMetricConstraint <em>Metric Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metric Constraint</em>'.
	 * @see camel.scalability.NonFunctionalEvent#getMetricConstraint()
	 * @see #getNonFunctionalEvent()
	 * @generated
	 */
	EReference getNonFunctionalEvent_MetricConstraint();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.NonFunctionalEvent#isIsViolation <em>Is Violation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Violation</em>'.
	 * @see camel.scalability.NonFunctionalEvent#isIsViolation()
	 * @see #getNonFunctionalEvent()
	 * @generated
	 */
	EAttribute getNonFunctionalEvent_IsViolation();

	/**
	 * Returns the meta object for class '{@link camel.scalability.EventInstance <em>Event Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Instance</em>'.
	 * @see camel.scalability.EventInstance
	 * @generated
	 */
	EClass getEventInstance();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.EventInstance#getStatus <em>Status</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Status</em>'.
	 * @see camel.scalability.EventInstance#getStatus()
	 * @see #getEventInstance()
	 * @generated
	 */
	EAttribute getEventInstance_Status();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.EventInstance#getLayer <em>Layer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Layer</em>'.
	 * @see camel.scalability.EventInstance#getLayer()
	 * @see #getEventInstance()
	 * @generated
	 */
	EAttribute getEventInstance_Layer();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.EventInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see camel.scalability.EventInstance#getType()
	 * @see #getEventInstance()
	 * @generated
	 */
	EReference getEventInstance_Type();

	/**
	 * Returns the meta object for class '{@link camel.scalability.FunctionalEventInstance <em>Functional Event Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Functional Event Instance</em>'.
	 * @see camel.scalability.FunctionalEventInstance
	 * @generated
	 */
	EClass getFunctionalEventInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.FunctionalEventInstance#getComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component Instance</em>'.
	 * @see camel.scalability.FunctionalEventInstance#getComponentInstance()
	 * @see #getFunctionalEventInstance()
	 * @generated
	 */
	EReference getFunctionalEventInstance_ComponentInstance();

	/**
	 * Returns the meta object for class '{@link camel.scalability.NonFunctionalEventInstance <em>Non Functional Event Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Non Functional Event Instance</em>'.
	 * @see camel.scalability.NonFunctionalEventInstance
	 * @generated
	 */
	EClass getNonFunctionalEventInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.NonFunctionalEventInstance#getMetricInstance <em>Metric Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metric Instance</em>'.
	 * @see camel.scalability.NonFunctionalEventInstance#getMetricInstance()
	 * @see #getNonFunctionalEventInstance()
	 * @generated
	 */
	EReference getNonFunctionalEventInstance_MetricInstance();

	/**
	 * Returns the meta object for class '{@link camel.scalability.ScalabilityRule <em>Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule</em>'.
	 * @see camel.scalability.ScalabilityRule
	 * @generated
	 */
	EClass getScalabilityRule();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.ScalabilityRule#getEvent <em>Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event</em>'.
	 * @see camel.scalability.ScalabilityRule#getEvent()
	 * @see #getScalabilityRule()
	 * @generated
	 */
	EReference getScalabilityRule_Event();

	/**
	 * Returns the meta object for the reference list '{@link camel.scalability.ScalabilityRule#getActions <em>Actions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Actions</em>'.
	 * @see camel.scalability.ScalabilityRule#getActions()
	 * @see #getScalabilityRule()
	 * @generated
	 */
	EReference getScalabilityRule_Actions();

	/**
	 * Returns the meta object for class '{@link camel.scalability.ScalingAction <em>Scaling Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scaling Action</em>'.
	 * @see camel.scalability.ScalingAction
	 * @generated
	 */
	EClass getScalingAction();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.ScalingAction#getSoftwareComponent <em>Software Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Software Component</em>'.
	 * @see camel.scalability.ScalingAction#getSoftwareComponent()
	 * @see #getScalingAction()
	 * @generated
	 */
	EReference getScalingAction_SoftwareComponent();

	/**
	 * Returns the meta object for class '{@link camel.scalability.HorizontalScalingAction <em>Horizontal Scaling Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Scaling Action</em>'.
	 * @see camel.scalability.HorizontalScalingAction
	 * @generated
	 */
	EClass getHorizontalScalingAction();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.HorizontalScalingAction#getCount <em>Count</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Count</em>'.
	 * @see camel.scalability.HorizontalScalingAction#getCount()
	 * @see #getHorizontalScalingAction()
	 * @generated
	 */
	EAttribute getHorizontalScalingAction_Count();

	/**
	 * Returns the meta object for class '{@link camel.scalability.VerticalScalingAction <em>Vertical Scaling Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Scaling Action</em>'.
	 * @see camel.scalability.VerticalScalingAction
	 * @generated
	 */
	EClass getVerticalScalingAction();

	/**
	 * Returns the meta object for class '{@link camel.scalability.Timer <em>Timer</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Timer</em>'.
	 * @see camel.scalability.Timer
	 * @generated
	 */
	EClass getTimer();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.Timer#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Type</em>'.
	 * @see camel.scalability.Timer#getType()
	 * @see #getTimer()
	 * @generated
	 */
	EAttribute getTimer_Type();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.Timer#getTimeValue <em>Time Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Value</em>'.
	 * @see camel.scalability.Timer#getTimeValue()
	 * @see #getTimer()
	 * @generated
	 */
	EAttribute getTimer_TimeValue();

	/**
	 * Returns the meta object for the attribute '{@link camel.scalability.Timer#getMaxOccurrenceNum <em>Max Occurrence Num</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Occurrence Num</em>'.
	 * @see camel.scalability.Timer#getMaxOccurrenceNum()
	 * @see #getTimer()
	 * @generated
	 */
	EAttribute getTimer_MaxOccurrenceNum();

	/**
	 * Returns the meta object for the reference '{@link camel.scalability.Timer#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Unit</em>'.
	 * @see camel.scalability.Timer#getUnit()
	 * @see #getTimer()
	 * @generated
	 */
	EReference getTimer_Unit();

	/**
	 * Returns the meta object for enum '{@link camel.scalability.BinaryPatternOperatorType <em>Binary Pattern Operator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Binary Pattern Operator Type</em>'.
	 * @see camel.scalability.BinaryPatternOperatorType
	 * @generated
	 */
	EEnum getBinaryPatternOperatorType();

	/**
	 * Returns the meta object for enum '{@link camel.scalability.TimerType <em>Timer Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Timer Type</em>'.
	 * @see camel.scalability.TimerType
	 * @generated
	 */
	EEnum getTimerType();

	/**
	 * Returns the meta object for enum '{@link camel.scalability.UnaryPatternOperatorType <em>Unary Pattern Operator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Unary Pattern Operator Type</em>'.
	 * @see camel.scalability.UnaryPatternOperatorType
	 * @generated
	 */
	EEnum getUnaryPatternOperatorType();

	/**
	 * Returns the meta object for enum '{@link camel.scalability.StatusType <em>Status Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Status Type</em>'.
	 * @see camel.scalability.StatusType
	 * @generated
	 */
	EEnum getStatusType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ScalabilityFactory getScalabilityFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link camel.scalability.impl.ScalabilityModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.ScalabilityModelImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getScalabilityModel()
		 * @generated
		 */
		EClass SCALABILITY_MODEL = eINSTANCE.getScalabilityModel();

		/**
		 * The meta object literal for the '<em><b>Rules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALABILITY_MODEL__RULES = eINSTANCE.getScalabilityModel_Rules();

		/**
		 * The meta object literal for the '<em><b>Events</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALABILITY_MODEL__EVENTS = eINSTANCE.getScalabilityModel_Events();

		/**
		 * The meta object literal for the '<em><b>Event Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALABILITY_MODEL__EVENT_INSTANCES = eINSTANCE.getScalabilityModel_EventInstances();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALABILITY_MODEL__ACTIONS = eINSTANCE.getScalabilityModel_Actions();

		/**
		 * The meta object literal for the '<em><b>Patterns</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALABILITY_MODEL__PATTERNS = eINSTANCE.getScalabilityModel_Patterns();

		/**
		 * The meta object literal for the '<em><b>Timers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALABILITY_MODEL__TIMERS = eINSTANCE.getScalabilityModel_Timers();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.EventImpl <em>Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.EventImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getEvent()
		 * @generated
		 */
		EClass EVENT = eINSTANCE.getEvent();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.EventPatternImpl <em>Event Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.EventPatternImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getEventPattern()
		 * @generated
		 */
		EClass EVENT_PATTERN = eINSTANCE.getEventPattern();

		/**
		 * The meta object literal for the '<em><b>Timer</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_PATTERN__TIMER = eINSTANCE.getEventPattern_Timer();

		/**
		 * The meta object literal for the '<em><b>Includes Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EVENT_PATTERN___INCLUDES_EVENT__SINGLEEVENT = eINSTANCE.getEventPattern__IncludesEvent__SingleEvent();

		/**
		 * The meta object literal for the '<em><b>Includes Left Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EVENT_PATTERN___INCLUDES_LEFT_EVENT__SINGLEEVENT = eINSTANCE.getEventPattern__IncludesLeftEvent__SingleEvent();

		/**
		 * The meta object literal for the '<em><b>Includes Right Event</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation EVENT_PATTERN___INCLUDES_RIGHT_EVENT__SINGLEEVENT = eINSTANCE.getEventPattern__IncludesRightEvent__SingleEvent();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.BinaryEventPatternImpl <em>Binary Event Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.BinaryEventPatternImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getBinaryEventPattern()
		 * @generated
		 */
		EClass BINARY_EVENT_PATTERN = eINSTANCE.getBinaryEventPattern();

		/**
		 * The meta object literal for the '<em><b>Left Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EVENT_PATTERN__LEFT_EVENT = eINSTANCE.getBinaryEventPattern_LeftEvent();

		/**
		 * The meta object literal for the '<em><b>Right Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_EVENT_PATTERN__RIGHT_EVENT = eINSTANCE.getBinaryEventPattern_RightEvent();

		/**
		 * The meta object literal for the '<em><b>Lower Occurrence Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_EVENT_PATTERN__LOWER_OCCURRENCE_BOUND = eINSTANCE.getBinaryEventPattern_LowerOccurrenceBound();

		/**
		 * The meta object literal for the '<em><b>Upper Occurrence Bound</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_EVENT_PATTERN__UPPER_OCCURRENCE_BOUND = eINSTANCE.getBinaryEventPattern_UpperOccurrenceBound();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BINARY_EVENT_PATTERN__OPERATOR = eINSTANCE.getBinaryEventPattern_Operator();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.UnaryEventPatternImpl <em>Unary Event Pattern</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.UnaryEventPatternImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getUnaryEventPattern()
		 * @generated
		 */
		EClass UNARY_EVENT_PATTERN = eINSTANCE.getUnaryEventPattern();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_EVENT_PATTERN__EVENT = eINSTANCE.getUnaryEventPattern_Event();

		/**
		 * The meta object literal for the '<em><b>Occurrence Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_EVENT_PATTERN__OCCURRENCE_NUM = eINSTANCE.getUnaryEventPattern_OccurrenceNum();

		/**
		 * The meta object literal for the '<em><b>Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_EVENT_PATTERN__OPERATOR = eINSTANCE.getUnaryEventPattern_Operator();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.SingleEventImpl <em>Single Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.SingleEventImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getSingleEvent()
		 * @generated
		 */
		EClass SINGLE_EVENT = eINSTANCE.getSingleEvent();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.FunctionalEventImpl <em>Functional Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.FunctionalEventImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getFunctionalEvent()
		 * @generated
		 */
		EClass FUNCTIONAL_EVENT = eINSTANCE.getFunctionalEvent();

		/**
		 * The meta object literal for the '<em><b>Functional Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTIONAL_EVENT__FUNCTIONAL_TYPE = eINSTANCE.getFunctionalEvent_FunctionalType();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTIONAL_EVENT__COMPONENT = eINSTANCE.getFunctionalEvent_Component();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.NonFunctionalEventImpl <em>Non Functional Event</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.NonFunctionalEventImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getNonFunctionalEvent()
		 * @generated
		 */
		EClass NON_FUNCTIONAL_EVENT = eINSTANCE.getNonFunctionalEvent();

		/**
		 * The meta object literal for the '<em><b>Metric Constraint</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NON_FUNCTIONAL_EVENT__METRIC_CONSTRAINT = eINSTANCE.getNonFunctionalEvent_MetricConstraint();

		/**
		 * The meta object literal for the '<em><b>Is Violation</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute NON_FUNCTIONAL_EVENT__IS_VIOLATION = eINSTANCE.getNonFunctionalEvent_IsViolation();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.EventInstanceImpl <em>Event Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.EventInstanceImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getEventInstance()
		 * @generated
		 */
		EClass EVENT_INSTANCE = eINSTANCE.getEventInstance();

		/**
		 * The meta object literal for the '<em><b>Status</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_INSTANCE__STATUS = eINSTANCE.getEventInstance_Status();

		/**
		 * The meta object literal for the '<em><b>Layer</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_INSTANCE__LAYER = eINSTANCE.getEventInstance_Layer();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_INSTANCE__TYPE = eINSTANCE.getEventInstance_Type();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.FunctionalEventInstanceImpl <em>Functional Event Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.FunctionalEventInstanceImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getFunctionalEventInstance()
		 * @generated
		 */
		EClass FUNCTIONAL_EVENT_INSTANCE = eINSTANCE.getFunctionalEventInstance();

		/**
		 * The meta object literal for the '<em><b>Component Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FUNCTIONAL_EVENT_INSTANCE__COMPONENT_INSTANCE = eINSTANCE.getFunctionalEventInstance_ComponentInstance();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.NonFunctionalEventInstanceImpl <em>Non Functional Event Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.NonFunctionalEventInstanceImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getNonFunctionalEventInstance()
		 * @generated
		 */
		EClass NON_FUNCTIONAL_EVENT_INSTANCE = eINSTANCE.getNonFunctionalEventInstance();

		/**
		 * The meta object literal for the '<em><b>Metric Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference NON_FUNCTIONAL_EVENT_INSTANCE__METRIC_INSTANCE = eINSTANCE.getNonFunctionalEventInstance_MetricInstance();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.ScalabilityRuleImpl <em>Rule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.ScalabilityRuleImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getScalabilityRule()
		 * @generated
		 */
		EClass SCALABILITY_RULE = eINSTANCE.getScalabilityRule();

		/**
		 * The meta object literal for the '<em><b>Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALABILITY_RULE__EVENT = eINSTANCE.getScalabilityRule_Event();

		/**
		 * The meta object literal for the '<em><b>Actions</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALABILITY_RULE__ACTIONS = eINSTANCE.getScalabilityRule_Actions();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.ScalingActionImpl <em>Scaling Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.ScalingActionImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getScalingAction()
		 * @generated
		 */
		EClass SCALING_ACTION = eINSTANCE.getScalingAction();

		/**
		 * The meta object literal for the '<em><b>Software Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCALING_ACTION__SOFTWARE_COMPONENT = eINSTANCE.getScalingAction_SoftwareComponent();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.HorizontalScalingActionImpl <em>Horizontal Scaling Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.HorizontalScalingActionImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getHorizontalScalingAction()
		 * @generated
		 */
		EClass HORIZONTAL_SCALING_ACTION = eINSTANCE.getHorizontalScalingAction();

		/**
		 * The meta object literal for the '<em><b>Count</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HORIZONTAL_SCALING_ACTION__COUNT = eINSTANCE.getHorizontalScalingAction_Count();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.VerticalScalingActionImpl <em>Vertical Scaling Action</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.VerticalScalingActionImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getVerticalScalingAction()
		 * @generated
		 */
		EClass VERTICAL_SCALING_ACTION = eINSTANCE.getVerticalScalingAction();

		/**
		 * The meta object literal for the '{@link camel.scalability.impl.TimerImpl <em>Timer</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.impl.TimerImpl
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getTimer()
		 * @generated
		 */
		EClass TIMER = eINSTANCE.getTimer();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMER__TYPE = eINSTANCE.getTimer_Type();

		/**
		 * The meta object literal for the '<em><b>Time Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMER__TIME_VALUE = eINSTANCE.getTimer_TimeValue();

		/**
		 * The meta object literal for the '<em><b>Max Occurrence Num</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TIMER__MAX_OCCURRENCE_NUM = eINSTANCE.getTimer_MaxOccurrenceNum();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TIMER__UNIT = eINSTANCE.getTimer_Unit();

		/**
		 * The meta object literal for the '{@link camel.scalability.BinaryPatternOperatorType <em>Binary Pattern Operator Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.BinaryPatternOperatorType
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getBinaryPatternOperatorType()
		 * @generated
		 */
		EEnum BINARY_PATTERN_OPERATOR_TYPE = eINSTANCE.getBinaryPatternOperatorType();

		/**
		 * The meta object literal for the '{@link camel.scalability.TimerType <em>Timer Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.TimerType
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getTimerType()
		 * @generated
		 */
		EEnum TIMER_TYPE = eINSTANCE.getTimerType();

		/**
		 * The meta object literal for the '{@link camel.scalability.UnaryPatternOperatorType <em>Unary Pattern Operator Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.UnaryPatternOperatorType
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getUnaryPatternOperatorType()
		 * @generated
		 */
		EEnum UNARY_PATTERN_OPERATOR_TYPE = eINSTANCE.getUnaryPatternOperatorType();

		/**
		 * The meta object literal for the '{@link camel.scalability.StatusType <em>Status Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.scalability.StatusType
		 * @see camel.scalability.impl.ScalabilityPackageImpl#getStatusType()
		 * @generated
		 */
		EEnum STATUS_TYPE = eINSTANCE.getStatusType();

	}

} //ScalabilityPackage
