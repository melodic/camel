/**
 */
package camel.scalability;

import camel.constraint.MetricConstraint;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Non Functional Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.NonFunctionalEvent#getMetricConstraint <em>Metric Constraint</em>}</li>
 *   <li>{@link camel.scalability.NonFunctionalEvent#isIsViolation <em>Is Violation</em>}</li>
 * </ul>
 *
 * @see camel.scalability.ScalabilityPackage#getNonFunctionalEvent()
 * @model
 * @generated
 */
public interface NonFunctionalEvent extends SingleEvent {
	/**
	 * Returns the value of the '<em><b>Metric Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Constraint</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Constraint</em>' reference.
	 * @see #setMetricConstraint(MetricConstraint)
	 * @see camel.scalability.ScalabilityPackage#getNonFunctionalEvent_MetricConstraint()
	 * @model required="true"
	 * @generated
	 */
	MetricConstraint getMetricConstraint();

	/**
	 * Sets the value of the '{@link camel.scalability.NonFunctionalEvent#getMetricConstraint <em>Metric Constraint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metric Constraint</em>' reference.
	 * @see #getMetricConstraint()
	 * @generated
	 */
	void setMetricConstraint(MetricConstraint value);

	/**
	 * Returns the value of the '<em><b>Is Violation</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Violation</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Violation</em>' attribute.
	 * @see #setIsViolation(boolean)
	 * @see camel.scalability.ScalabilityPackage#getNonFunctionalEvent_IsViolation()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isIsViolation();

	/**
	 * Sets the value of the '{@link camel.scalability.NonFunctionalEvent#isIsViolation <em>Is Violation</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Violation</em>' attribute.
	 * @see #isIsViolation()
	 * @generated
	 */
	void setIsViolation(boolean value);

} // NonFunctionalEvent
