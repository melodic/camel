/**
 */
package camel.scalability;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertical Scaling Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.scalability.ScalabilityPackage#getVerticalScalingAction()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='vertical_scaling_action'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot vertical_scaling_action='Tuple {\n\tmessage : String = \'VerticalScalingAction: \' + self.name + \' should have at least one feature or attribute being specified\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.subFeatures-&gt;size() &lt;&gt; 0 xor self.attributes-&gt;size() &lt;&gt; 0)\n}.status'"
 * @generated
 */
public interface VerticalScalingAction extends ScalingAction {

} // VerticalScalingAction
