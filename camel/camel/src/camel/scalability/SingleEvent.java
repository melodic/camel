/**
 */
package camel.scalability;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Event</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.scalability.ScalabilityPackage#getSingleEvent()
 * @model abstract="true"
 * @generated
 */
public interface SingleEvent extends Event {
} // SingleEvent
