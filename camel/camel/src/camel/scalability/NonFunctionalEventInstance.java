/**
 */
package camel.scalability;

import camel.metric.MetricInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Non Functional Event Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.NonFunctionalEventInstance#getMetricInstance <em>Metric Instance</em>}</li>
 * </ul>
 *
 * @see camel.scalability.ScalabilityPackage#getNonFunctionalEventInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='nonfunctional_event_instance_correct_type event_instance_metric_in_event'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot nonfunctional_event_instance_correct_type='Tuple {\n\tmessage : String = \'NonFunctionalEventInstance: \' + self.name + \' should have a nonfunctional event as type\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.type.oclIsTypeOf(NonFunctionalEvent))\n}.status' event_instance_metric_in_event='Tuple {\n\tmessage : String = \'EventInstance: \' + self.name + \' has a metric instance with a metric which is not identical to the one associated to the event instance\\\'s event: \' + self.type.name,\n\tstatus : Boolean = \n\t\t\t\tasError(if (self.type.oclIsTypeOf(NonFunctionalEvent))\n\t\t\t\t\tthen metricInstance.metricContext.metric = type.oclAsType(NonFunctionalEvent).metricConstraint.metricContext.metric\n\t\t\t\t\telse true\n\t\t\t\t\tendif)\n}.status'"
 * @generated
 */
public interface NonFunctionalEventInstance extends EventInstance {
	/**
	 * Returns the value of the '<em><b>Metric Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Instance</em>' reference.
	 * @see #setMetricInstance(MetricInstance)
	 * @see camel.scalability.ScalabilityPackage#getNonFunctionalEventInstance_MetricInstance()
	 * @model
	 * @generated
	 */
	MetricInstance getMetricInstance();

	/**
	 * Sets the value of the '{@link camel.scalability.NonFunctionalEventInstance#getMetricInstance <em>Metric Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metric Instance</em>' reference.
	 * @see #getMetricInstance()
	 * @generated
	 */
	void setMetricInstance(MetricInstance value);

} // NonFunctionalEventInstance
