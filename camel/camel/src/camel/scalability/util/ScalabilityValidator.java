/**
 */
package camel.scalability.util;

import camel.scalability.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.scalability.ScalabilityPackage
 * @generated
 */
public class ScalabilityValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final ScalabilityValidator INSTANCE = new ScalabilityValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.scalability";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScalabilityValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return ScalabilityPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case ScalabilityPackage.SCALABILITY_MODEL:
				return validateScalabilityModel((ScalabilityModel)value, diagnostics, context);
			case ScalabilityPackage.EVENT:
				return validateEvent((Event)value, diagnostics, context);
			case ScalabilityPackage.EVENT_PATTERN:
				return validateEventPattern((EventPattern)value, diagnostics, context);
			case ScalabilityPackage.BINARY_EVENT_PATTERN:
				return validateBinaryEventPattern((BinaryEventPattern)value, diagnostics, context);
			case ScalabilityPackage.UNARY_EVENT_PATTERN:
				return validateUnaryEventPattern((UnaryEventPattern)value, diagnostics, context);
			case ScalabilityPackage.SINGLE_EVENT:
				return validateSingleEvent((SingleEvent)value, diagnostics, context);
			case ScalabilityPackage.FUNCTIONAL_EVENT:
				return validateFunctionalEvent((FunctionalEvent)value, diagnostics, context);
			case ScalabilityPackage.NON_FUNCTIONAL_EVENT:
				return validateNonFunctionalEvent((NonFunctionalEvent)value, diagnostics, context);
			case ScalabilityPackage.EVENT_INSTANCE:
				return validateEventInstance((EventInstance)value, diagnostics, context);
			case ScalabilityPackage.FUNCTIONAL_EVENT_INSTANCE:
				return validateFunctionalEventInstance((FunctionalEventInstance)value, diagnostics, context);
			case ScalabilityPackage.NON_FUNCTIONAL_EVENT_INSTANCE:
				return validateNonFunctionalEventInstance((NonFunctionalEventInstance)value, diagnostics, context);
			case ScalabilityPackage.SCALABILITY_RULE:
				return validateScalabilityRule((ScalabilityRule)value, diagnostics, context);
			case ScalabilityPackage.SCALING_ACTION:
				return validateScalingAction((ScalingAction)value, diagnostics, context);
			case ScalabilityPackage.HORIZONTAL_SCALING_ACTION:
				return validateHorizontalScalingAction((HorizontalScalingAction)value, diagnostics, context);
			case ScalabilityPackage.VERTICAL_SCALING_ACTION:
				return validateVerticalScalingAction((VerticalScalingAction)value, diagnostics, context);
			case ScalabilityPackage.TIMER:
				return validateTimer((Timer)value, diagnostics, context);
			case ScalabilityPackage.BINARY_PATTERN_OPERATOR_TYPE:
				return validateBinaryPatternOperatorType((BinaryPatternOperatorType)value, diagnostics, context);
			case ScalabilityPackage.TIMER_TYPE:
				return validateTimerType((TimerType)value, diagnostics, context);
			case ScalabilityPackage.UNARY_PATTERN_OPERATOR_TYPE:
				return validateUnaryPatternOperatorType((UnaryPatternOperatorType)value, diagnostics, context);
			case ScalabilityPackage.STATUS_TYPE:
				return validateStatusType((StatusType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScalabilityModel(ScalabilityModel scalabilityModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)scalabilityModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEvent(Event event, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)event, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEventPattern(EventPattern eventPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)eventPattern, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBinaryEventPattern(BinaryEventPattern binaryEventPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)binaryEventPattern, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validateBinaryEventPattern_binary_event_pattern_at_least_left_right(binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validateBinaryEventPattern_no_timer_both_events_in_pattern(binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validateBinaryEventPattern_no_same_left_right_event_binary_pattern(binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validateBinaryEventPattern_binary_event_pattern_timer_one_event(binaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validateBinaryEventPattern_binary_event_pattern_occur_bounds(binaryEventPattern, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the binary_event_pattern_at_least_left_right constraint of '<em>Binary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String BINARY_EVENT_PATTERN__BINARY_EVENT_PATTERN_AT_LEAST_LEFT_RIGHT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In BinaryEventPattern: ' + self.name + ' no left or right event has been specified',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.leftEvent <> null or self.rightEvent <> null)\n" +
		"}.status";

	/**
	 * Validates the binary_event_pattern_at_least_left_right constraint of '<em>Binary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBinaryEventPattern_binary_event_pattern_at_least_left_right(BinaryEventPattern binaryEventPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.BINARY_EVENT_PATTERN,
				 (EObject)binaryEventPattern,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "binary_event_pattern_at_least_left_right",
				 BINARY_EVENT_PATTERN__BINARY_EVENT_PATTERN_AT_LEAST_LEFT_RIGHT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the no_timer_both_events_in_pattern constraint of '<em>Binary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String BINARY_EVENT_PATTERN__NO_TIMER_BOTH_EVENTS_IN_PATTERN__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In BinaryEventPattern: ' + self.name + ' only one or zero events have been specified with no timer at all'\n" +
		"\t\t\t,\n" +
		"\tstatus : Boolean = asError(self.oclAsType(EventPattern).timer = null implies (self.leftEvent <> null and self.rightEvent <> null))\n" +
		"}.status";

	/**
	 * Validates the no_timer_both_events_in_pattern constraint of '<em>Binary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBinaryEventPattern_no_timer_both_events_in_pattern(BinaryEventPattern binaryEventPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.BINARY_EVENT_PATTERN,
				 (EObject)binaryEventPattern,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "no_timer_both_events_in_pattern",
				 BINARY_EVENT_PATTERN__NO_TIMER_BOTH_EVENTS_IN_PATTERN__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the no_same_left_right_event_binary_pattern constraint of '<em>Binary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String BINARY_EVENT_PATTERN__NO_SAME_LEFT_RIGHT_EVENT_BINARY_PATTERN__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In BinaryEventPattern: ' + self.name + ' both the left and right events are the same'\n" +
		"\t\t\t,\n" +
		"\tstatus : Boolean = asError(leftEvent <> null and rightEvent <> null implies leftEvent <> rightEvent)\n" +
		"}.status";

	/**
	 * Validates the no_same_left_right_event_binary_pattern constraint of '<em>Binary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBinaryEventPattern_no_same_left_right_event_binary_pattern(BinaryEventPattern binaryEventPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.BINARY_EVENT_PATTERN,
				 (EObject)binaryEventPattern,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "no_same_left_right_event_binary_pattern",
				 BINARY_EVENT_PATTERN__NO_SAME_LEFT_RIGHT_EVENT_BINARY_PATTERN__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the binary_event_pattern_timer_one_event constraint of '<em>Binary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String BINARY_EVENT_PATTERN__BINARY_EVENT_PATTERN_TIMER_ONE_EVENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In BinaryEventPattern: ' + self.name + ' both a timer as well as left and right events have been specified or only one ',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\t\tasError(self.oclAsType(EventPattern).timer <> null implies (self.leftEvent = null or self.rightEvent = null))\n" +
		"}.status";

	/**
	 * Validates the binary_event_pattern_timer_one_event constraint of '<em>Binary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBinaryEventPattern_binary_event_pattern_timer_one_event(BinaryEventPattern binaryEventPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.BINARY_EVENT_PATTERN,
				 (EObject)binaryEventPattern,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "binary_event_pattern_timer_one_event",
				 BINARY_EVENT_PATTERN__BINARY_EVENT_PATTERN_TIMER_ONE_EVENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the binary_event_pattern_occur_bounds constraint of '<em>Binary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String BINARY_EVENT_PATTERN__BINARY_EVENT_PATTERN_OCCUR_BOUNDS__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In BinaryEventPattern: ' + self.name + ' incorrect values were given for the lowerOccurrenceBound and upperOccurrenceBound attributes in conjunction with the use of the respective pattern operator: ' + operator.toString(),\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((self.operator <> BinaryPatternOperatorType::REPEAT_UNTIL implies\n" +
		"\t\t\t\t\t(self.lowerOccurrenceBound = 0 and self.upperOccurrenceBound = 0)) \n" +
		"\t\t\t\t\tand (self.operator = BinaryPatternOperatorType::REPEAT_UNTIL \n" +
		"\t\t\t\t\t\timplies (not (self.lowerOccurrenceBound = 0 and self.upperOccurrenceBound = 0) \n" +
		"\t\t\t\t\t\t\tand (self.upperOccurrenceBound > 0 implies self.lowerOccurrenceBound <= upperOccurrenceBound)\n" +
		"\t\t\t\t\t\t\tand \n" +
		"\t\t\t\t\t\t\tif (self.lowerOccurrenceBound >= 1.0) then\n" +
		"\t\t\t\t\t\t\t\t(self.lowerOccurrenceBound / self.lowerOccurrenceBound.round()) = 1 and \n" +
		"\t\t\t\t\t\t\t\t\t(if (self.upperOccurrenceBound > 0) then (self.upperOccurrenceBound / self.upperOccurrenceBound.round()) = 1\n" +
		"\t\t\t\t\t\t\t\t\telse true\n" +
		"\t\t\t\t\t\t\t\t\tendif)\n" +
		"\t\t\t\t\t\t\telse \n" +
		"\t\t\t\t\t\t\t\tif (self.lowerOccurrenceBound <> 0.0) then \n" +
		"\t\t\t\t\t\t\t\t\tif (self.upperOccurrenceBound <= 1.0) then true\n" +
		"\t\t\t\t\t\t\t\t\telse false\n" +
		"\t\t\t\t\t\t\t\t\tendif\n" +
		"\t\t\t\t\t\t\t\telse\n" +
		"\t\t\t\t\t\t\t\t\tif (self.upperOccurrenceBound >= 1.0) then (self.upperOccurrenceBound / self.upperOccurrenceBound.round()) = 1\n" +
		"\t\t\t\t\t\t\t\t\telse true\n" +
		"\t\t\t\t\t\t\t\t\tendif\n" +
		"\t\t\t\t\t\t\t\tendif\n" +
		"\t\t\t\t\t\t\tendif\n" +
		"\t\t\t\t\t\t)\n" +
		"\t\t\t\t\t))\n" +
		"}.status";

	/**
	 * Validates the binary_event_pattern_occur_bounds constraint of '<em>Binary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBinaryEventPattern_binary_event_pattern_occur_bounds(BinaryEventPattern binaryEventPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.BINARY_EVENT_PATTERN,
				 (EObject)binaryEventPattern,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "binary_event_pattern_occur_bounds",
				 BINARY_EVENT_PATTERN__BINARY_EVENT_PATTERN_OCCUR_BOUNDS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnaryEventPattern(UnaryEventPattern unaryEventPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)unaryEventPattern, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)unaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)unaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)unaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)unaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)unaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)unaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)unaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)unaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validateUnaryEventPattern_unary_event_pattern_correct_values_per_operator(unaryEventPattern, diagnostics, context);
		if (result || diagnostics != null) result &= validateUnaryEventPattern_unary_event_pattern_correct_occur_num(unaryEventPattern, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the unary_event_pattern_correct_values_per_operator constraint of '<em>Unary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String UNARY_EVENT_PATTERN__UNARY_EVENT_PATTERN_CORRECT_VALUES_PER_OPERATOR__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In UnaryEventPattern: ' + self.name + ' either a REPEAT operator there is an incorrect combination of values of the operator and occurrenceNum attributes and of the timer association. When operator is REPEAT, occurrenceNum should be positive; otherwise, equal to zero. When operator is WHERE, then a timer must be specified; otherwise, no timer should be specified',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((self.operator\n" +
		"\t\t\t\t\t= UnaryPatternOperatorType::REPEAT implies occurrenceNum > 0) and (self.operator <>\n" +
		"\t\t\t\t\tUnaryPatternOperatorType::REPEAT implies occurrenceNum = 0) and (self.operator = UnaryPatternOperatorType::WHEN\n" +
		"\t\t\t\t\timplies self.oclAsType(EventPattern).timer <> null) and (self.operator <> UnaryPatternOperatorType::WHEN implies\n" +
		"\t\t\t\t\tself.oclAsType(EventPattern).timer = null))\n" +
		"}.status";

	/**
	 * Validates the unary_event_pattern_correct_values_per_operator constraint of '<em>Unary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnaryEventPattern_unary_event_pattern_correct_values_per_operator(UnaryEventPattern unaryEventPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.UNARY_EVENT_PATTERN,
				 (EObject)unaryEventPattern,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "unary_event_pattern_correct_values_per_operator",
				 UNARY_EVENT_PATTERN__UNARY_EVENT_PATTERN_CORRECT_VALUES_PER_OPERATOR__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the unary_event_pattern_correct_occur_num constraint of '<em>Unary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String UNARY_EVENT_PATTERN__UNARY_EVENT_PATTERN_CORRECT_OCCUR_NUM__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In UnaryEventPattern: ' + self.name + ' the occurrence num of an event should be either a positive integer or a real in [0.0,1.0]',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(\n" +
		"\t\t\t\t\tif (occurrenceNum >= 1.0) then (occurrenceNum / occurrenceNum.round()) = 1\n" +
		"\t\t\t\t\telse true\n" +
		"\t\t\t\t\tendif\n" +
		"\t\t\t\t)\n" +
		"}.status";

	/**
	 * Validates the unary_event_pattern_correct_occur_num constraint of '<em>Unary Event Pattern</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnaryEventPattern_unary_event_pattern_correct_occur_num(UnaryEventPattern unaryEventPattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.UNARY_EVENT_PATTERN,
				 (EObject)unaryEventPattern,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "unary_event_pattern_correct_occur_num",
				 UNARY_EVENT_PATTERN__UNARY_EVENT_PATTERN_CORRECT_OCCUR_NUM__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSingleEvent(SingleEvent singleEvent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)singleEvent, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFunctionalEvent(FunctionalEvent functionalEvent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)functionalEvent, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNonFunctionalEvent(NonFunctionalEvent nonFunctionalEvent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)nonFunctionalEvent, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEventInstance(EventInstance eventInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)eventInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFunctionalEventInstance(FunctionalEventInstance functionalEventInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)functionalEventInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)functionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)functionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)functionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)functionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)functionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)functionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)functionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)functionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateFunctionalEventInstance_functional_event_instance_correct_type(functionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateFunctionalEventInstance_functional_event_instance_correct_component(functionalEventInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the functional_event_instance_correct_type constraint of '<em>Functional Event Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String FUNCTIONAL_EVENT_INSTANCE__FUNCTIONAL_EVENT_INSTANCE_CORRECT_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'FunctionalEventInstance: ' + self.name + ' should have a functional event as type',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.type.oclIsTypeOf(FunctionalEvent))\n" +
		"}.status";

	/**
	 * Validates the functional_event_instance_correct_type constraint of '<em>Functional Event Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFunctionalEventInstance_functional_event_instance_correct_type(FunctionalEventInstance functionalEventInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.FUNCTIONAL_EVENT_INSTANCE,
				 (EObject)functionalEventInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "functional_event_instance_correct_type",
				 FUNCTIONAL_EVENT_INSTANCE__FUNCTIONAL_EVENT_INSTANCE_CORRECT_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the functional_event_instance_correct_component constraint of '<em>Functional Event Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String FUNCTIONAL_EVENT_INSTANCE__FUNCTIONAL_EVENT_INSTANCE_CORRECT_COMPONENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'FunctionalEventInstance: ' + self.name + ' should refer to an instance of a component, which is referred to by its type',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(componentInstance <> null implies (if type.oclIsTypeOf(FunctionalEvent) and type.oclAsType(FunctionalEvent).component <> null then type.oclAsType(FunctionalEvent).component = componentInstance.type else false endif))\n" +
		"}.status";

	/**
	 * Validates the functional_event_instance_correct_component constraint of '<em>Functional Event Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFunctionalEventInstance_functional_event_instance_correct_component(FunctionalEventInstance functionalEventInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.FUNCTIONAL_EVENT_INSTANCE,
				 (EObject)functionalEventInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "functional_event_instance_correct_component",
				 FUNCTIONAL_EVENT_INSTANCE__FUNCTIONAL_EVENT_INSTANCE_CORRECT_COMPONENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNonFunctionalEventInstance(NonFunctionalEventInstance nonFunctionalEventInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)nonFunctionalEventInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)nonFunctionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)nonFunctionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)nonFunctionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)nonFunctionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)nonFunctionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)nonFunctionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)nonFunctionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)nonFunctionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateNonFunctionalEventInstance_nonfunctional_event_instance_correct_type(nonFunctionalEventInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateNonFunctionalEventInstance_event_instance_metric_in_event(nonFunctionalEventInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the nonfunctional_event_instance_correct_type constraint of '<em>Non Functional Event Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String NON_FUNCTIONAL_EVENT_INSTANCE__NONFUNCTIONAL_EVENT_INSTANCE_CORRECT_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'NonFunctionalEventInstance: ' + self.name + ' should have a nonfunctional event as type',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.type.oclIsTypeOf(NonFunctionalEvent))\n" +
		"}.status";

	/**
	 * Validates the nonfunctional_event_instance_correct_type constraint of '<em>Non Functional Event Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNonFunctionalEventInstance_nonfunctional_event_instance_correct_type(NonFunctionalEventInstance nonFunctionalEventInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.NON_FUNCTIONAL_EVENT_INSTANCE,
				 (EObject)nonFunctionalEventInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "nonfunctional_event_instance_correct_type",
				 NON_FUNCTIONAL_EVENT_INSTANCE__NONFUNCTIONAL_EVENT_INSTANCE_CORRECT_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the event_instance_metric_in_event constraint of '<em>Non Functional Event Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String NON_FUNCTIONAL_EVENT_INSTANCE__EVENT_INSTANCE_METRIC_IN_EVENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'EventInstance: ' + self.name + ' has a metric instance with a metric which is not identical to the one associated to the event instance\\'s event: ' + self.type.name,\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(if (self.type.oclIsTypeOf(NonFunctionalEvent))\n" +
		"\t\t\t\t\tthen metricInstance.metricContext.metric = type.oclAsType(NonFunctionalEvent).metricConstraint.metricContext.metric\n" +
		"\t\t\t\t\telse true\n" +
		"\t\t\t\t\tendif)\n" +
		"}.status";

	/**
	 * Validates the event_instance_metric_in_event constraint of '<em>Non Functional Event Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNonFunctionalEventInstance_event_instance_metric_in_event(NonFunctionalEventInstance nonFunctionalEventInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.NON_FUNCTIONAL_EVENT_INSTANCE,
				 (EObject)nonFunctionalEventInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "event_instance_metric_in_event",
				 NON_FUNCTIONAL_EVENT_INSTANCE__EVENT_INSTANCE_METRIC_IN_EVENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScalabilityRule(ScalabilityRule scalabilityRule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)scalabilityRule, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScalingAction(ScalingAction scalingAction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)scalingAction, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHorizontalScalingAction(HorizontalScalingAction horizontalScalingAction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)horizontalScalingAction, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)horizontalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)horizontalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)horizontalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)horizontalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)horizontalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)horizontalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)horizontalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)horizontalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validateHorizontalScalingAction_horizontal_scale_action_correct_count(horizontalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validateHorizontalScalingAction_horizontal_scale_action_req_align(horizontalScalingAction, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the horizontal_scale_action_correct_count constraint of '<em>Horizontal Scaling Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String HORIZONTAL_SCALING_ACTION__HORIZONTAL_SCALE_ACTION_CORRECT_COUNT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'HorizontalScalingAction: ' + self.name + ' has a wrong (negative) value for the count attribute',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(count <> 0)\n" +
		"}.status";

	/**
	 * Validates the horizontal_scale_action_correct_count constraint of '<em>Horizontal Scaling Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHorizontalScalingAction_horizontal_scale_action_correct_count(HorizontalScalingAction horizontalScalingAction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.HORIZONTAL_SCALING_ACTION,
				 (EObject)horizontalScalingAction,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "horizontal_scale_action_correct_count",
				 HORIZONTAL_SCALING_ACTION__HORIZONTAL_SCALE_ACTION_CORRECT_COUNT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the horizontal_scale_action_req_align constraint of '<em>Horizontal Scaling Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String HORIZONTAL_SCALING_ACTION__HORIZONTAL_SCALE_ACTION_REQ_ALIGN__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'HorizontalScalingAction: ' + self.name + ' has a count which along with the minimum instance number of the respective sw component exceeds the maximum instance number of that component',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(softwareComponent.requirementSet <> null and softwareComponent.requirementSet.horizontalScaleRequirement <> null implies \n" +
		"\t\t\t\t(softwareComponent.requirementSet.horizontalScaleRequirement.maxInstances <> -1 implies  \n" +
		"\t\t\t\t\tsoftwareComponent.requirementSet.horizontalScaleRequirement.maxInstances >= softwareComponent.requirementSet.horizontalScaleRequirement.minInstances + count\n" +
		"\t\t\t\t\tand softwareComponent.requirementSet.horizontalScaleRequirement.minInstances <= softwareComponent.requirementSet.horizontalScaleRequirement.maxInstances + count\n" +
		"\t\t\t\t))\n" +
		"}.status";

	/**
	 * Validates the horizontal_scale_action_req_align constraint of '<em>Horizontal Scaling Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHorizontalScalingAction_horizontal_scale_action_req_align(HorizontalScalingAction horizontalScalingAction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.HORIZONTAL_SCALING_ACTION,
				 (EObject)horizontalScalingAction,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "horizontal_scale_action_req_align",
				 HORIZONTAL_SCALING_ACTION__HORIZONTAL_SCALE_ACTION_REQ_ALIGN__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVerticalScalingAction(VerticalScalingAction verticalScalingAction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)verticalScalingAction, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)verticalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)verticalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)verticalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)verticalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)verticalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)verticalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)verticalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)verticalScalingAction, diagnostics, context);
		if (result || diagnostics != null) result &= validateVerticalScalingAction_vertical_scaling_action(verticalScalingAction, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the vertical_scaling_action constraint of '<em>Vertical Scaling Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String VERTICAL_SCALING_ACTION__VERTICAL_SCALING_ACTION__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'VerticalScalingAction: ' + self.name + ' should have at least one feature or attribute being specified',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.subFeatures->size() <> 0 xor self.attributes->size() <> 0)\n" +
		"}.status";

	/**
	 * Validates the vertical_scaling_action constraint of '<em>Vertical Scaling Action</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVerticalScalingAction_vertical_scaling_action(VerticalScalingAction verticalScalingAction, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.VERTICAL_SCALING_ACTION,
				 (EObject)verticalScalingAction,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "vertical_scaling_action",
				 VERTICAL_SCALING_ACTION__VERTICAL_SCALING_ACTION__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimer(Timer timer, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)timer, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)timer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)timer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)timer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)timer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)timer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)timer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)timer, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)timer, diagnostics, context);
		if (result || diagnostics != null) result &= validateTimer_timer_correct_values(timer, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the timer_correct_values constraint of '<em>Timer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String TIMER__TIMER_CORRECT_VALUES__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Timer: ' + self.toString() + ' as wrong value combinations for its attributes. This means that either the timeValue is non-positive or the time type is WITHIN_MAX and a non-positive maxOccurrenceNum has been provided or that the time type is not WITHIN_MAX and a positive maxOccurrenceNum has been given',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(timeValue > 0 and (self.type = TimerType::WITHIN_MAX implies self.maxOccurrenceNum > 0) and (self.type <>\n" +
		"\t\t\t\t\tTimerType::WITHIN_MAX implies self.maxOccurrenceNum = 0))\n" +
		"}.status";

	/**
	 * Validates the timer_correct_values constraint of '<em>Timer</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimer_timer_correct_values(Timer timer, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ScalabilityPackage.Literals.TIMER,
				 (EObject)timer,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "timer_correct_values",
				 TIMER__TIMER_CORRECT_VALUES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBinaryPatternOperatorType(BinaryPatternOperatorType binaryPatternOperatorType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTimerType(TimerType timerType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnaryPatternOperatorType(UnaryPatternOperatorType unaryPatternOperatorType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStatusType(StatusType statusType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //ScalabilityValidator
