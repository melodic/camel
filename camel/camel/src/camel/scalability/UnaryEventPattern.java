/**
 */
package camel.scalability;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Event Pattern</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.UnaryEventPattern#getEvent <em>Event</em>}</li>
 *   <li>{@link camel.scalability.UnaryEventPattern#getOccurrenceNum <em>Occurrence Num</em>}</li>
 *   <li>{@link camel.scalability.UnaryEventPattern#getOperator <em>Operator</em>}</li>
 * </ul>
 *
 * @see camel.scalability.ScalabilityPackage#getUnaryEventPattern()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='unary_event_pattern_correct_values_per_operator unary_event_pattern_correct_occur_num'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot unary_event_pattern_correct_values_per_operator='Tuple {\n\tmessage : String = \'In UnaryEventPattern: \' + self.name + \' either a REPEAT operator there is an incorrect combination of values of the operator and occurrenceNum attributes and of the timer association. When operator is REPEAT, occurrenceNum should be positive; otherwise, equal to zero. When operator is WHERE, then a timer must be specified; otherwise, no timer should be specified\',\n\tstatus : Boolean = \n\t\t\t\tasError((self.operator\n\t\t\t\t\t= UnaryPatternOperatorType::REPEAT implies occurrenceNum &gt; 0) and (self.operator &lt;&gt;\n\t\t\t\t\tUnaryPatternOperatorType::REPEAT implies occurrenceNum = 0) and (self.operator = UnaryPatternOperatorType::WHEN\n\t\t\t\t\timplies self.oclAsType(EventPattern).timer &lt;&gt; null) and (self.operator &lt;&gt; UnaryPatternOperatorType::WHEN implies\n\t\t\t\t\tself.oclAsType(EventPattern).timer = null))\n}.status' unary_event_pattern_correct_occur_num='Tuple {\n\tmessage : String = \'In UnaryEventPattern: \' + self.name + \' the occurrence num of an event should be either a positive integer or a real in [0.0,1.0]\',\n\tstatus : Boolean = \n\t\t\t\tasError(\n\t\t\t\t\tif (occurrenceNum &gt;= 1.0) then (occurrenceNum / occurrenceNum.round()) = 1\n\t\t\t\t\telse true\n\t\t\t\t\tendif\n\t\t\t\t)\n}.status'"
 * @generated
 */
public interface UnaryEventPattern extends EventPattern {
	/**
	 * Returns the value of the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' reference.
	 * @see #setEvent(Event)
	 * @see camel.scalability.ScalabilityPackage#getUnaryEventPattern_Event()
	 * @model required="true"
	 * @generated
	 */
	Event getEvent();

	/**
	 * Sets the value of the '{@link camel.scalability.UnaryEventPattern#getEvent <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(Event value);

	/**
	 * Returns the value of the '<em><b>Occurrence Num</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Occurrence Num</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Occurrence Num</em>' attribute.
	 * @see #setOccurrenceNum(double)
	 * @see camel.scalability.ScalabilityPackage#getUnaryEventPattern_OccurrenceNum()
	 * @model required="true"
	 * @generated
	 */
	double getOccurrenceNum();

	/**
	 * Sets the value of the '{@link camel.scalability.UnaryEventPattern#getOccurrenceNum <em>Occurrence Num</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Occurrence Num</em>' attribute.
	 * @see #getOccurrenceNum()
	 * @generated
	 */
	void setOccurrenceNum(double value);

	/**
	 * Returns the value of the '<em><b>Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.scalability.UnaryPatternOperatorType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Operator</em>' attribute.
	 * @see camel.scalability.UnaryPatternOperatorType
	 * @see #setOperator(UnaryPatternOperatorType)
	 * @see camel.scalability.ScalabilityPackage#getUnaryEventPattern_Operator()
	 * @model required="true"
	 * @generated
	 */
	UnaryPatternOperatorType getOperator();

	/**
	 * Sets the value of the '{@link camel.scalability.UnaryEventPattern#getOperator <em>Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Operator</em>' attribute.
	 * @see camel.scalability.UnaryPatternOperatorType
	 * @see #getOperator()
	 * @generated
	 */
	void setOperator(UnaryPatternOperatorType value);

} // UnaryEventPattern
