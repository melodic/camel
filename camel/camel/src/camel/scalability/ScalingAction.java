/**
 */
package camel.scalability;

import camel.core.Action;
import camel.deployment.SoftwareComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Scaling Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.ScalingAction#getSoftwareComponent <em>Software Component</em>}</li>
 * </ul>
 *
 * @see camel.scalability.ScalabilityPackage#getScalingAction()
 * @model abstract="true"
 * @generated
 */
public interface ScalingAction extends Action {

	/**
	 * Returns the value of the '<em><b>Software Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software Component</em>' reference.
	 * @see #setSoftwareComponent(SoftwareComponent)
	 * @see camel.scalability.ScalabilityPackage#getScalingAction_SoftwareComponent()
	 * @model required="true"
	 * @generated
	 */
	SoftwareComponent getSoftwareComponent();

	/**
	 * Sets the value of the '{@link camel.scalability.ScalingAction#getSoftwareComponent <em>Software Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Software Component</em>' reference.
	 * @see #getSoftwareComponent()
	 * @generated
	 */
	void setSoftwareComponent(SoftwareComponent value);
} // ScalingAction
