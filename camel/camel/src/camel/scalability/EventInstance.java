/**
 */
package camel.scalability;

import camel.core.Feature;
import camel.core.LayerType;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.EventInstance#getStatus <em>Status</em>}</li>
 *   <li>{@link camel.scalability.EventInstance#getLayer <em>Layer</em>}</li>
 *   <li>{@link camel.scalability.EventInstance#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see camel.scalability.ScalabilityPackage#getEventInstance()
 * @model abstract="true"
 * @generated
 */
public interface EventInstance extends Feature {
	/**
	 * Returns the value of the '<em><b>Status</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.scalability.StatusType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Status</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Status</em>' attribute.
	 * @see camel.scalability.StatusType
	 * @see #setStatus(StatusType)
	 * @see camel.scalability.ScalabilityPackage#getEventInstance_Status()
	 * @model required="true"
	 * @generated
	 */
	StatusType getStatus();

	/**
	 * Sets the value of the '{@link camel.scalability.EventInstance#getStatus <em>Status</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Status</em>' attribute.
	 * @see camel.scalability.StatusType
	 * @see #getStatus()
	 * @generated
	 */
	void setStatus(StatusType value);

	/**
	 * Returns the value of the '<em><b>Layer</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.core.LayerType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Layer</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Layer</em>' attribute.
	 * @see camel.core.LayerType
	 * @see #setLayer(LayerType)
	 * @see camel.scalability.ScalabilityPackage#getEventInstance_Layer()
	 * @model
	 * @generated
	 */
	LayerType getLayer();

	/**
	 * Sets the value of the '{@link camel.scalability.EventInstance#getLayer <em>Layer</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Layer</em>' attribute.
	 * @see camel.core.LayerType
	 * @see #getLayer()
	 * @generated
	 */
	void setLayer(LayerType value);

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(SingleEvent)
	 * @see camel.scalability.ScalabilityPackage#getEventInstance_Type()
	 * @model required="true"
	 * @generated
	 */
	SingleEvent getType();

	/**
	 * Sets the value of the '{@link camel.scalability.EventInstance#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(SingleEvent value);

} // EventInstance
