/**
 */
package camel.scalability;

import camel.core.Attribute;

import camel.deployment.Component;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Functional Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.FunctionalEvent#getFunctionalType <em>Functional Type</em>}</li>
 *   <li>{@link camel.scalability.FunctionalEvent#getComponent <em>Component</em>}</li>
 * </ul>
 *
 * @see camel.scalability.ScalabilityPackage#getFunctionalEvent()
 * @model
 * @generated
 */
public interface FunctionalEvent extends SingleEvent {
	/**
	 * Returns the value of the '<em><b>Functional Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functional Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functional Type</em>' reference.
	 * @see #setFunctionalType(Attribute)
	 * @see camel.scalability.ScalabilityPackage#getFunctionalEvent_FunctionalType()
	 * @model required="true"
	 * @generated
	 */
	Attribute getFunctionalType();

	/**
	 * Sets the value of the '{@link camel.scalability.FunctionalEvent#getFunctionalType <em>Functional Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Functional Type</em>' reference.
	 * @see #getFunctionalType()
	 * @generated
	 */
	void setFunctionalType(Attribute value);

	/**
	 * Returns the value of the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component</em>' reference.
	 * @see #setComponent(Component)
	 * @see camel.scalability.ScalabilityPackage#getFunctionalEvent_Component()
	 * @model
	 * @generated
	 */
	Component getComponent();

	/**
	 * Sets the value of the '{@link camel.scalability.FunctionalEvent#getComponent <em>Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component</em>' reference.
	 * @see #getComponent()
	 * @generated
	 */
	void setComponent(Component value);

} // FunctionalEvent
