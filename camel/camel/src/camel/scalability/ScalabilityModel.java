/**
 */
package camel.scalability;

import camel.core.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.ScalabilityModel#getRules <em>Rules</em>}</li>
 *   <li>{@link camel.scalability.ScalabilityModel#getEvents <em>Events</em>}</li>
 *   <li>{@link camel.scalability.ScalabilityModel#getEventInstances <em>Event Instances</em>}</li>
 *   <li>{@link camel.scalability.ScalabilityModel#getActions <em>Actions</em>}</li>
 *   <li>{@link camel.scalability.ScalabilityModel#getPatterns <em>Patterns</em>}</li>
 *   <li>{@link camel.scalability.ScalabilityModel#getTimers <em>Timers</em>}</li>
 * </ul>
 *
 * @see camel.scalability.ScalabilityPackage#getScalabilityModel()
 * @model
 * @generated
 */
public interface ScalabilityModel extends Model {
	/**
	 * Returns the value of the '<em><b>Rules</b></em>' containment reference list.
	 * The list contents are of type {@link camel.scalability.ScalabilityRule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rules</em>' containment reference list.
	 * @see camel.scalability.ScalabilityPackage#getScalabilityModel_Rules()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ScalabilityRule> getRules();

	/**
	 * Returns the value of the '<em><b>Events</b></em>' containment reference list.
	 * The list contents are of type {@link camel.scalability.SingleEvent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Events</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Events</em>' containment reference list.
	 * @see camel.scalability.ScalabilityPackage#getScalabilityModel_Events()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SingleEvent> getEvents();

	/**
	 * Returns the value of the '<em><b>Event Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.scalability.EventInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Instances</em>' containment reference list.
	 * @see camel.scalability.ScalabilityPackage#getScalabilityModel_EventInstances()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<EventInstance> getEventInstances();

	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link camel.scalability.ScalingAction}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see camel.scalability.ScalabilityPackage#getScalabilityModel_Actions()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ScalingAction> getActions();

	/**
	 * Returns the value of the '<em><b>Patterns</b></em>' containment reference list.
	 * The list contents are of type {@link camel.scalability.EventPattern}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Patterns</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Patterns</em>' containment reference list.
	 * @see camel.scalability.ScalabilityPackage#getScalabilityModel_Patterns()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<EventPattern> getPatterns();

	/**
	 * Returns the value of the '<em><b>Timers</b></em>' containment reference list.
	 * The list contents are of type {@link camel.scalability.Timer}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Timers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Timers</em>' containment reference list.
	 * @see camel.scalability.ScalabilityPackage#getScalabilityModel_Timers()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Timer> getTimers();

} // ScalabilityModel
