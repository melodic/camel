/**
 */
package camel.scalability;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Horizontal Scaling Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.HorizontalScalingAction#getCount <em>Count</em>}</li>
 * </ul>
 *
 * @see camel.scalability.ScalabilityPackage#getHorizontalScalingAction()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='horizontal_scale_action_correct_count horizontal_scale_action_req_align'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot horizontal_scale_action_correct_count='Tuple {\n\tmessage : String = \'HorizontalScalingAction: \' + self.name + \' has a wrong (negative) value for the count attribute\',\n\tstatus : Boolean = \n\t\t\t\tasError(count &lt;&gt; 0)\n}.status' horizontal_scale_action_req_align='Tuple {\n\tmessage : String = \'HorizontalScalingAction: \' + self.name + \' has a count which along with the minimum instance number of the respective sw component exceeds the maximum instance number of that component\',\n\tstatus : Boolean = \n\t\t\t\tasError(softwareComponent.requirementSet &lt;&gt; null and softwareComponent.requirementSet.horizontalScaleRequirement &lt;&gt; null implies \n\t\t\t\t(softwareComponent.requirementSet.horizontalScaleRequirement.maxInstances &lt;&gt; -1 implies  \n\t\t\t\t\tsoftwareComponent.requirementSet.horizontalScaleRequirement.maxInstances &gt;= softwareComponent.requirementSet.horizontalScaleRequirement.minInstances + count\n\t\t\t\t\tand softwareComponent.requirementSet.horizontalScaleRequirement.minInstances &lt;= softwareComponent.requirementSet.horizontalScaleRequirement.maxInstances + count\n\t\t\t\t))\n}.status'"
 * @generated
 */
public interface HorizontalScalingAction extends ScalingAction {
	/**
	 * Returns the value of the '<em><b>Count</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Count</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Count</em>' attribute.
	 * @see #setCount(int)
	 * @see camel.scalability.ScalabilityPackage#getHorizontalScalingAction_Count()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getCount();

	/**
	 * Sets the value of the '{@link camel.scalability.HorizontalScalingAction#getCount <em>Count</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Count</em>' attribute.
	 * @see #getCount()
	 * @generated
	 */
	void setCount(int value);

} // HorizontalScalingAction
