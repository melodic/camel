/**
 */
package camel.scalability;

import camel.core.Action;
import camel.core.Feature;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.ScalabilityRule#getEvent <em>Event</em>}</li>
 *   <li>{@link camel.scalability.ScalabilityRule#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @see camel.scalability.ScalabilityPackage#getScalabilityRule()
 * @model
 * @generated
 */
public interface ScalabilityRule extends Feature {
	/**
	 * Returns the value of the '<em><b>Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event</em>' reference.
	 * @see #setEvent(Event)
	 * @see camel.scalability.ScalabilityPackage#getScalabilityRule_Event()
	 * @model required="true"
	 * @generated
	 */
	Event getEvent();

	/**
	 * Sets the value of the '{@link camel.scalability.ScalabilityRule#getEvent <em>Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event</em>' reference.
	 * @see #getEvent()
	 * @generated
	 */
	void setEvent(Event value);

	/**
	 * Returns the value of the '<em><b>Actions</b></em>' reference list.
	 * The list contents are of type {@link camel.core.Action}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actions</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' reference list.
	 * @see camel.scalability.ScalabilityPackage#getScalabilityRule_Actions()
	 * @model required="true"
	 * @generated
	 */
	EList<Action> getActions();

} // ScalabilityRule
