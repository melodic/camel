/**
 */
package camel.scalability.impl;

import camel.scalability.ScalabilityPackage;
import camel.scalability.VerticalScalingAction;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vertical Scaling Action</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VerticalScalingActionImpl extends ScalingActionImpl implements VerticalScalingAction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VerticalScalingActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.VERTICAL_SCALING_ACTION;
	}

} //VerticalScalingActionImpl
