/**
 */
package camel.scalability.impl;

import camel.scalability.ScalabilityPackage;
import camel.scalability.SingleEvent;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Single Event</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class SingleEventImpl extends EventImpl implements SingleEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SingleEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.SINGLE_EVENT;
	}

} //SingleEventImpl
