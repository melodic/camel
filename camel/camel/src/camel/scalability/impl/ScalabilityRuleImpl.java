/**
 */
package camel.scalability.impl;

import camel.core.Action;

import camel.core.impl.FeatureImpl;
import camel.scalability.Event;
import camel.scalability.ScalabilityPackage;
import camel.scalability.ScalabilityRule;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.impl.ScalabilityRuleImpl#getEvent <em>Event</em>}</li>
 *   <li>{@link camel.scalability.impl.ScalabilityRuleImpl#getActions <em>Actions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScalabilityRuleImpl extends FeatureImpl implements ScalabilityRule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScalabilityRuleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.SCALABILITY_RULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event getEvent() {
		return (Event)eGet(ScalabilityPackage.Literals.SCALABILITY_RULE__EVENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEvent(Event newEvent) {
		eSet(ScalabilityPackage.Literals.SCALABILITY_RULE__EVENT, newEvent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Action> getActions() {
		return (EList<Action>)eGet(ScalabilityPackage.Literals.SCALABILITY_RULE__ACTIONS, true);
	}

} //ScalabilityRuleImpl
