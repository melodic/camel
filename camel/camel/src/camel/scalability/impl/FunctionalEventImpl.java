/**
 */
package camel.scalability.impl;

import camel.core.Attribute;

import camel.deployment.Component;

import camel.scalability.FunctionalEvent;
import camel.scalability.ScalabilityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Functional Event</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.impl.FunctionalEventImpl#getFunctionalType <em>Functional Type</em>}</li>
 *   <li>{@link camel.scalability.impl.FunctionalEventImpl#getComponent <em>Component</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionalEventImpl extends SingleEventImpl implements FunctionalEvent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionalEventImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.FUNCTIONAL_EVENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getFunctionalType() {
		return (Attribute)eGet(ScalabilityPackage.Literals.FUNCTIONAL_EVENT__FUNCTIONAL_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFunctionalType(Attribute newFunctionalType) {
		eSet(ScalabilityPackage.Literals.FUNCTIONAL_EVENT__FUNCTIONAL_TYPE, newFunctionalType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component getComponent() {
		return (Component)eGet(ScalabilityPackage.Literals.FUNCTIONAL_EVENT__COMPONENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponent(Component newComponent) {
		eSet(ScalabilityPackage.Literals.FUNCTIONAL_EVENT__COMPONENT, newComponent);
	}

} //FunctionalEventImpl
