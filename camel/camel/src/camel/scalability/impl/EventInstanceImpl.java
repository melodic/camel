/**
 */
package camel.scalability.impl;

import camel.core.LayerType;

import camel.core.impl.FeatureImpl;
import camel.scalability.EventInstance;
import camel.scalability.ScalabilityPackage;
import camel.scalability.SingleEvent;
import camel.scalability.StatusType;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.impl.EventInstanceImpl#getStatus <em>Status</em>}</li>
 *   <li>{@link camel.scalability.impl.EventInstanceImpl#getLayer <em>Layer</em>}</li>
 *   <li>{@link camel.scalability.impl.EventInstanceImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class EventInstanceImpl extends FeatureImpl implements EventInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.EVENT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StatusType getStatus() {
		return (StatusType)eGet(ScalabilityPackage.Literals.EVENT_INSTANCE__STATUS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStatus(StatusType newStatus) {
		eSet(ScalabilityPackage.Literals.EVENT_INSTANCE__STATUS, newStatus);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LayerType getLayer() {
		return (LayerType)eGet(ScalabilityPackage.Literals.EVENT_INSTANCE__LAYER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLayer(LayerType newLayer) {
		eSet(ScalabilityPackage.Literals.EVENT_INSTANCE__LAYER, newLayer);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleEvent getType() {
		return (SingleEvent)eGet(ScalabilityPackage.Literals.EVENT_INSTANCE__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(SingleEvent newType) {
		eSet(ScalabilityPackage.Literals.EVENT_INSTANCE__TYPE, newType);
	}

} //EventInstanceImpl
