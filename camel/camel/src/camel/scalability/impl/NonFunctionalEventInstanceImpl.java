/**
 */
package camel.scalability.impl;

import camel.metric.MetricInstance;

import camel.scalability.NonFunctionalEventInstance;
import camel.scalability.ScalabilityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Non Functional Event Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.impl.NonFunctionalEventInstanceImpl#getMetricInstance <em>Metric Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class NonFunctionalEventInstanceImpl extends EventInstanceImpl implements NonFunctionalEventInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected NonFunctionalEventInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.NON_FUNCTIONAL_EVENT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricInstance getMetricInstance() {
		return (MetricInstance)eGet(ScalabilityPackage.Literals.NON_FUNCTIONAL_EVENT_INSTANCE__METRIC_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetricInstance(MetricInstance newMetricInstance) {
		eSet(ScalabilityPackage.Literals.NON_FUNCTIONAL_EVENT_INSTANCE__METRIC_INSTANCE, newMetricInstance);
	}

} //NonFunctionalEventInstanceImpl
