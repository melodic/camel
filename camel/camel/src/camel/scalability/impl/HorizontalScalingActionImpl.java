/**
 */
package camel.scalability.impl;

import camel.scalability.HorizontalScalingAction;
import camel.scalability.ScalabilityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Horizontal Scaling Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.impl.HorizontalScalingActionImpl#getCount <em>Count</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HorizontalScalingActionImpl extends ScalingActionImpl implements HorizontalScalingAction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HorizontalScalingActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.HORIZONTAL_SCALING_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getCount() {
		return (Integer)eGet(ScalabilityPackage.Literals.HORIZONTAL_SCALING_ACTION__COUNT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCount(int newCount) {
		eSet(ScalabilityPackage.Literals.HORIZONTAL_SCALING_ACTION__COUNT, newCount);
	}

} //HorizontalScalingActionImpl
