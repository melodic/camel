/**
 */
package camel.scalability.impl;

import camel.constraint.ConstraintPackage;

import camel.constraint.impl.ConstraintPackageImpl;

import camel.core.CorePackage;

import camel.core.impl.CorePackageImpl;

import camel.data.DataPackage;

import camel.data.impl.DataPackageImpl;

import camel.deployment.DeploymentPackage;

import camel.deployment.impl.DeploymentPackageImpl;

import camel.execution.ExecutionPackage;

import camel.execution.impl.ExecutionPackageImpl;

import camel.location.LocationPackage;

import camel.location.impl.LocationPackageImpl;

import camel.metric.MetricPackage;

import camel.metric.impl.MetricPackageImpl;

import camel.mms.MmsPackage;

import camel.mms.impl.MmsPackageImpl;

import camel.organisation.OrganisationPackage;

import camel.organisation.impl.OrganisationPackageImpl;

import camel.requirement.RequirementPackage;

import camel.requirement.impl.RequirementPackageImpl;

import camel.scalability.BinaryEventPattern;
import camel.scalability.BinaryPatternOperatorType;
import camel.scalability.Event;
import camel.scalability.EventInstance;
import camel.scalability.EventPattern;
import camel.scalability.FunctionalEvent;
import camel.scalability.FunctionalEventInstance;
import camel.scalability.HorizontalScalingAction;
import camel.scalability.NonFunctionalEvent;
import camel.scalability.NonFunctionalEventInstance;
import camel.scalability.ScalabilityFactory;
import camel.scalability.ScalabilityModel;
import camel.scalability.ScalabilityPackage;
import camel.scalability.ScalabilityRule;
import camel.scalability.ScalingAction;
import camel.scalability.SingleEvent;
import camel.scalability.StatusType;
import camel.scalability.Timer;
import camel.scalability.TimerType;
import camel.scalability.UnaryEventPattern;
import camel.scalability.UnaryPatternOperatorType;
import camel.scalability.VerticalScalingAction;

import camel.scalability.util.ScalabilityValidator;

import camel.security.SecurityPackage;

import camel.security.impl.SecurityPackageImpl;

import camel.type.TypePackage;

import camel.type.impl.TypePackageImpl;

import camel.unit.UnitPackage;

import camel.unit.impl.UnitPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ScalabilityPackageImpl extends EPackageImpl implements ScalabilityPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scalabilityModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass binaryEventPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass unaryEventPatternEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass singleEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionalEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nonFunctionalEventEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionalEventInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass nonFunctionalEventInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scalabilityRuleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scalingActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass horizontalScalingActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass verticalScalingActionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass timerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum binaryPatternOperatorTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum timerTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum unaryPatternOperatorTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum statusTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see camel.scalability.ScalabilityPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ScalabilityPackageImpl() {
		super(eNS_URI, ScalabilityFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ScalabilityPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ScalabilityPackage init() {
		if (isInited) return (ScalabilityPackage)EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI);

		// Obtain or create and register package
		ScalabilityPackageImpl theScalabilityPackage = (ScalabilityPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ScalabilityPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ScalabilityPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		MmsPackageImpl theMmsPackage = (MmsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI) instanceof MmsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI) : MmsPackage.eINSTANCE);
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		DeploymentPackageImpl theDeploymentPackage = (DeploymentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI) instanceof DeploymentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI) : DeploymentPackage.eINSTANCE);
		ExecutionPackageImpl theExecutionPackage = (ExecutionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI) instanceof ExecutionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI) : ExecutionPackage.eINSTANCE);
		LocationPackageImpl theLocationPackage = (LocationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) instanceof LocationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) : LocationPackage.eINSTANCE);
		MetricPackageImpl theMetricPackage = (MetricPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI) instanceof MetricPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI) : MetricPackage.eINSTANCE);
		OrganisationPackageImpl theOrganisationPackage = (OrganisationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) instanceof OrganisationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) : OrganisationPackage.eINSTANCE);
		RequirementPackageImpl theRequirementPackage = (RequirementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) instanceof RequirementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) : RequirementPackage.eINSTANCE);
		SecurityPackageImpl theSecurityPackage = (SecurityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) instanceof SecurityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) : SecurityPackage.eINSTANCE);
		TypePackageImpl theTypePackage = (TypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI) instanceof TypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI) : TypePackage.eINSTANCE);
		UnitPackageImpl theUnitPackage = (UnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) instanceof UnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) : UnitPackage.eINSTANCE);
		ConstraintPackageImpl theConstraintPackage = (ConstraintPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) instanceof ConstraintPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) : ConstraintPackage.eINSTANCE);
		DataPackageImpl theDataPackage = (DataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) instanceof DataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) : DataPackage.eINSTANCE);

		// Create package meta-data objects
		theScalabilityPackage.createPackageContents();
		theMmsPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theDeploymentPackage.createPackageContents();
		theExecutionPackage.createPackageContents();
		theLocationPackage.createPackageContents();
		theMetricPackage.createPackageContents();
		theOrganisationPackage.createPackageContents();
		theRequirementPackage.createPackageContents();
		theSecurityPackage.createPackageContents();
		theTypePackage.createPackageContents();
		theUnitPackage.createPackageContents();
		theConstraintPackage.createPackageContents();
		theDataPackage.createPackageContents();

		// Initialize created meta-data
		theScalabilityPackage.initializePackageContents();
		theMmsPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theDeploymentPackage.initializePackageContents();
		theExecutionPackage.initializePackageContents();
		theLocationPackage.initializePackageContents();
		theMetricPackage.initializePackageContents();
		theOrganisationPackage.initializePackageContents();
		theRequirementPackage.initializePackageContents();
		theSecurityPackage.initializePackageContents();
		theTypePackage.initializePackageContents();
		theUnitPackage.initializePackageContents();
		theConstraintPackage.initializePackageContents();
		theDataPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theScalabilityPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return ScalabilityValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theScalabilityPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ScalabilityPackage.eNS_URI, theScalabilityPackage);
		return theScalabilityPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScalabilityModel() {
		return scalabilityModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalabilityModel_Rules() {
		return (EReference)scalabilityModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalabilityModel_Events() {
		return (EReference)scalabilityModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalabilityModel_EventInstances() {
		return (EReference)scalabilityModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalabilityModel_Actions() {
		return (EReference)scalabilityModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalabilityModel_Patterns() {
		return (EReference)scalabilityModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalabilityModel_Timers() {
		return (EReference)scalabilityModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEvent() {
		return eventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventPattern() {
		return eventPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventPattern_Timer() {
		return (EReference)eventPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEventPattern__IncludesEvent__SingleEvent() {
		return eventPatternEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEventPattern__IncludesLeftEvent__SingleEvent() {
		return eventPatternEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getEventPattern__IncludesRightEvent__SingleEvent() {
		return eventPatternEClass.getEOperations().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBinaryEventPattern() {
		return binaryEventPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinaryEventPattern_LeftEvent() {
		return (EReference)binaryEventPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getBinaryEventPattern_RightEvent() {
		return (EReference)binaryEventPatternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryEventPattern_LowerOccurrenceBound() {
		return (EAttribute)binaryEventPatternEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryEventPattern_UpperOccurrenceBound() {
		return (EAttribute)binaryEventPatternEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBinaryEventPattern_Operator() {
		return (EAttribute)binaryEventPatternEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getUnaryEventPattern() {
		return unaryEventPatternEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getUnaryEventPattern_Event() {
		return (EReference)unaryEventPatternEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnaryEventPattern_OccurrenceNum() {
		return (EAttribute)unaryEventPatternEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getUnaryEventPattern_Operator() {
		return (EAttribute)unaryEventPatternEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSingleEvent() {
		return singleEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionalEvent() {
		return functionalEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionalEvent_FunctionalType() {
		return (EReference)functionalEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionalEvent_Component() {
		return (EReference)functionalEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNonFunctionalEvent() {
		return nonFunctionalEventEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNonFunctionalEvent_MetricConstraint() {
		return (EReference)nonFunctionalEventEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getNonFunctionalEvent_IsViolation() {
		return (EAttribute)nonFunctionalEventEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventInstance() {
		return eventInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventInstance_Status() {
		return (EAttribute)eventInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventInstance_Layer() {
		return (EAttribute)eventInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventInstance_Type() {
		return (EReference)eventInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunctionalEventInstance() {
		return functionalEventInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getFunctionalEventInstance_ComponentInstance() {
		return (EReference)functionalEventInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNonFunctionalEventInstance() {
		return nonFunctionalEventInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getNonFunctionalEventInstance_MetricInstance() {
		return (EReference)nonFunctionalEventInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScalabilityRule() {
		return scalabilityRuleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalabilityRule_Event() {
		return (EReference)scalabilityRuleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalabilityRule_Actions() {
		return (EReference)scalabilityRuleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScalingAction() {
		return scalingActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getScalingAction_SoftwareComponent() {
		return (EReference)scalingActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHorizontalScalingAction() {
		return horizontalScalingActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHorizontalScalingAction_Count() {
		return (EAttribute)horizontalScalingActionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVerticalScalingAction() {
		return verticalScalingActionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTimer() {
		return timerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimer_Type() {
		return (EAttribute)timerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimer_TimeValue() {
		return (EAttribute)timerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getTimer_MaxOccurrenceNum() {
		return (EAttribute)timerEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTimer_Unit() {
		return (EReference)timerEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getBinaryPatternOperatorType() {
		return binaryPatternOperatorTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getTimerType() {
		return timerTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getUnaryPatternOperatorType() {
		return unaryPatternOperatorTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getStatusType() {
		return statusTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScalabilityFactory getScalabilityFactory() {
		return (ScalabilityFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		scalabilityModelEClass = createEClass(SCALABILITY_MODEL);
		createEReference(scalabilityModelEClass, SCALABILITY_MODEL__RULES);
		createEReference(scalabilityModelEClass, SCALABILITY_MODEL__EVENTS);
		createEReference(scalabilityModelEClass, SCALABILITY_MODEL__EVENT_INSTANCES);
		createEReference(scalabilityModelEClass, SCALABILITY_MODEL__ACTIONS);
		createEReference(scalabilityModelEClass, SCALABILITY_MODEL__PATTERNS);
		createEReference(scalabilityModelEClass, SCALABILITY_MODEL__TIMERS);

		eventEClass = createEClass(EVENT);

		eventPatternEClass = createEClass(EVENT_PATTERN);
		createEReference(eventPatternEClass, EVENT_PATTERN__TIMER);
		createEOperation(eventPatternEClass, EVENT_PATTERN___INCLUDES_EVENT__SINGLEEVENT);
		createEOperation(eventPatternEClass, EVENT_PATTERN___INCLUDES_LEFT_EVENT__SINGLEEVENT);
		createEOperation(eventPatternEClass, EVENT_PATTERN___INCLUDES_RIGHT_EVENT__SINGLEEVENT);

		binaryEventPatternEClass = createEClass(BINARY_EVENT_PATTERN);
		createEReference(binaryEventPatternEClass, BINARY_EVENT_PATTERN__LEFT_EVENT);
		createEReference(binaryEventPatternEClass, BINARY_EVENT_PATTERN__RIGHT_EVENT);
		createEAttribute(binaryEventPatternEClass, BINARY_EVENT_PATTERN__LOWER_OCCURRENCE_BOUND);
		createEAttribute(binaryEventPatternEClass, BINARY_EVENT_PATTERN__UPPER_OCCURRENCE_BOUND);
		createEAttribute(binaryEventPatternEClass, BINARY_EVENT_PATTERN__OPERATOR);

		unaryEventPatternEClass = createEClass(UNARY_EVENT_PATTERN);
		createEReference(unaryEventPatternEClass, UNARY_EVENT_PATTERN__EVENT);
		createEAttribute(unaryEventPatternEClass, UNARY_EVENT_PATTERN__OCCURRENCE_NUM);
		createEAttribute(unaryEventPatternEClass, UNARY_EVENT_PATTERN__OPERATOR);

		singleEventEClass = createEClass(SINGLE_EVENT);

		functionalEventEClass = createEClass(FUNCTIONAL_EVENT);
		createEReference(functionalEventEClass, FUNCTIONAL_EVENT__FUNCTIONAL_TYPE);
		createEReference(functionalEventEClass, FUNCTIONAL_EVENT__COMPONENT);

		nonFunctionalEventEClass = createEClass(NON_FUNCTIONAL_EVENT);
		createEReference(nonFunctionalEventEClass, NON_FUNCTIONAL_EVENT__METRIC_CONSTRAINT);
		createEAttribute(nonFunctionalEventEClass, NON_FUNCTIONAL_EVENT__IS_VIOLATION);

		eventInstanceEClass = createEClass(EVENT_INSTANCE);
		createEAttribute(eventInstanceEClass, EVENT_INSTANCE__STATUS);
		createEAttribute(eventInstanceEClass, EVENT_INSTANCE__LAYER);
		createEReference(eventInstanceEClass, EVENT_INSTANCE__TYPE);

		functionalEventInstanceEClass = createEClass(FUNCTIONAL_EVENT_INSTANCE);
		createEReference(functionalEventInstanceEClass, FUNCTIONAL_EVENT_INSTANCE__COMPONENT_INSTANCE);

		nonFunctionalEventInstanceEClass = createEClass(NON_FUNCTIONAL_EVENT_INSTANCE);
		createEReference(nonFunctionalEventInstanceEClass, NON_FUNCTIONAL_EVENT_INSTANCE__METRIC_INSTANCE);

		scalabilityRuleEClass = createEClass(SCALABILITY_RULE);
		createEReference(scalabilityRuleEClass, SCALABILITY_RULE__EVENT);
		createEReference(scalabilityRuleEClass, SCALABILITY_RULE__ACTIONS);

		scalingActionEClass = createEClass(SCALING_ACTION);
		createEReference(scalingActionEClass, SCALING_ACTION__SOFTWARE_COMPONENT);

		horizontalScalingActionEClass = createEClass(HORIZONTAL_SCALING_ACTION);
		createEAttribute(horizontalScalingActionEClass, HORIZONTAL_SCALING_ACTION__COUNT);

		verticalScalingActionEClass = createEClass(VERTICAL_SCALING_ACTION);

		timerEClass = createEClass(TIMER);
		createEAttribute(timerEClass, TIMER__TYPE);
		createEAttribute(timerEClass, TIMER__TIME_VALUE);
		createEAttribute(timerEClass, TIMER__MAX_OCCURRENCE_NUM);
		createEReference(timerEClass, TIMER__UNIT);

		// Create enums
		binaryPatternOperatorTypeEEnum = createEEnum(BINARY_PATTERN_OPERATOR_TYPE);
		timerTypeEEnum = createEEnum(TIMER_TYPE);
		unaryPatternOperatorTypeEEnum = createEEnum(UNARY_PATTERN_OPERATOR_TYPE);
		statusTypeEEnum = createEEnum(STATUS_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		DeploymentPackage theDeploymentPackage = (DeploymentPackage)EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI);
		ConstraintPackage theConstraintPackage = (ConstraintPackage)EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI);
		MetricPackage theMetricPackage = (MetricPackage)EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI);
		UnitPackage theUnitPackage = (UnitPackage)EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		scalabilityModelEClass.getESuperTypes().add(theCorePackage.getModel());
		eventEClass.getESuperTypes().add(theCorePackage.getFeature());
		eventPatternEClass.getESuperTypes().add(this.getEvent());
		binaryEventPatternEClass.getESuperTypes().add(this.getEventPattern());
		unaryEventPatternEClass.getESuperTypes().add(this.getEventPattern());
		singleEventEClass.getESuperTypes().add(this.getEvent());
		functionalEventEClass.getESuperTypes().add(this.getSingleEvent());
		nonFunctionalEventEClass.getESuperTypes().add(this.getSingleEvent());
		eventInstanceEClass.getESuperTypes().add(theCorePackage.getFeature());
		functionalEventInstanceEClass.getESuperTypes().add(this.getEventInstance());
		nonFunctionalEventInstanceEClass.getESuperTypes().add(this.getEventInstance());
		scalabilityRuleEClass.getESuperTypes().add(theCorePackage.getFeature());
		scalingActionEClass.getESuperTypes().add(theCorePackage.getAction());
		horizontalScalingActionEClass.getESuperTypes().add(this.getScalingAction());
		verticalScalingActionEClass.getESuperTypes().add(this.getScalingAction());
		timerEClass.getESuperTypes().add(theCorePackage.getFeature());

		// Initialize classes, features, and operations; add parameters
		initEClass(scalabilityModelEClass, ScalabilityModel.class, "ScalabilityModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScalabilityModel_Rules(), this.getScalabilityRule(), null, "rules", null, 0, -1, ScalabilityModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getScalabilityModel_Events(), this.getSingleEvent(), null, "events", null, 0, -1, ScalabilityModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getScalabilityModel_EventInstances(), this.getEventInstance(), null, "eventInstances", null, 0, -1, ScalabilityModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getScalabilityModel_Actions(), this.getScalingAction(), null, "actions", null, 0, -1, ScalabilityModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getScalabilityModel_Patterns(), this.getEventPattern(), null, "patterns", null, 0, -1, ScalabilityModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getScalabilityModel_Timers(), this.getTimer(), null, "timers", null, 0, -1, ScalabilityModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(eventEClass, Event.class, "Event", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(eventPatternEClass, EventPattern.class, "EventPattern", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getEventPattern_Timer(), this.getTimer(), null, "timer", null, 0, 1, EventPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getEventPattern__IncludesEvent__SingleEvent(), ecorePackage.getEBoolean(), "includesEvent", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSingleEvent(), "e", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getEventPattern__IncludesLeftEvent__SingleEvent(), ecorePackage.getEBoolean(), "includesLeftEvent", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSingleEvent(), "e", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getEventPattern__IncludesRightEvent__SingleEvent(), ecorePackage.getEBoolean(), "includesRightEvent", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getSingleEvent(), "e", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(binaryEventPatternEClass, BinaryEventPattern.class, "BinaryEventPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getBinaryEventPattern_LeftEvent(), this.getEvent(), null, "leftEvent", null, 0, 1, BinaryEventPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getBinaryEventPattern_RightEvent(), this.getEvent(), null, "rightEvent", null, 0, 1, BinaryEventPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBinaryEventPattern_LowerOccurrenceBound(), ecorePackage.getEDouble(), "lowerOccurrenceBound", null, 1, 1, BinaryEventPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBinaryEventPattern_UpperOccurrenceBound(), ecorePackage.getEDouble(), "upperOccurrenceBound", null, 1, 1, BinaryEventPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBinaryEventPattern_Operator(), this.getBinaryPatternOperatorType(), "operator", null, 1, 1, BinaryEventPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(unaryEventPatternEClass, UnaryEventPattern.class, "UnaryEventPattern", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getUnaryEventPattern_Event(), this.getEvent(), null, "event", null, 1, 1, UnaryEventPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnaryEventPattern_OccurrenceNum(), ecorePackage.getEDouble(), "occurrenceNum", null, 1, 1, UnaryEventPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getUnaryEventPattern_Operator(), this.getUnaryPatternOperatorType(), "operator", null, 1, 1, UnaryEventPattern.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(singleEventEClass, SingleEvent.class, "SingleEvent", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(functionalEventEClass, FunctionalEvent.class, "FunctionalEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunctionalEvent_FunctionalType(), theCorePackage.getAttribute(), null, "functionalType", null, 1, 1, FunctionalEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getFunctionalEvent_Component(), theDeploymentPackage.getComponent(), null, "component", null, 0, 1, FunctionalEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nonFunctionalEventEClass, NonFunctionalEvent.class, "NonFunctionalEvent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNonFunctionalEvent_MetricConstraint(), theConstraintPackage.getMetricConstraint(), null, "metricConstraint", null, 1, 1, NonFunctionalEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getNonFunctionalEvent_IsViolation(), ecorePackage.getEBoolean(), "isViolation", "false", 1, 1, NonFunctionalEvent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventInstanceEClass, EventInstance.class, "EventInstance", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEventInstance_Status(), this.getStatusType(), "status", null, 1, 1, EventInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEventInstance_Layer(), theCorePackage.getLayerType(), "layer", null, 0, 1, EventInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventInstance_Type(), this.getSingleEvent(), null, "type", null, 1, 1, EventInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(functionalEventInstanceEClass, FunctionalEventInstance.class, "FunctionalEventInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getFunctionalEventInstance_ComponentInstance(), theDeploymentPackage.getComponentInstance(), null, "componentInstance", null, 0, 1, FunctionalEventInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(nonFunctionalEventInstanceEClass, NonFunctionalEventInstance.class, "NonFunctionalEventInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getNonFunctionalEventInstance_MetricInstance(), theMetricPackage.getMetricInstance(), null, "metricInstance", null, 0, 1, NonFunctionalEventInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scalabilityRuleEClass, ScalabilityRule.class, "ScalabilityRule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScalabilityRule_Event(), this.getEvent(), null, "event", null, 1, 1, ScalabilityRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getScalabilityRule_Actions(), theCorePackage.getAction(), null, "actions", null, 1, -1, ScalabilityRule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scalingActionEClass, ScalingAction.class, "ScalingAction", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getScalingAction_SoftwareComponent(), theDeploymentPackage.getSoftwareComponent(), null, "softwareComponent", null, 1, 1, ScalingAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(horizontalScalingActionEClass, HorizontalScalingAction.class, "HorizontalScalingAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getHorizontalScalingAction_Count(), ecorePackage.getEInt(), "count", "0", 1, 1, HorizontalScalingAction.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(verticalScalingActionEClass, VerticalScalingAction.class, "VerticalScalingAction", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(timerEClass, Timer.class, "Timer", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getTimer_Type(), this.getTimerType(), "type", null, 1, 1, Timer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimer_TimeValue(), ecorePackage.getEInt(), "timeValue", null, 1, 1, Timer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getTimer_MaxOccurrenceNum(), ecorePackage.getEInt(), "maxOccurrenceNum", null, 1, 1, Timer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTimer_Unit(), theUnitPackage.getUnit(), null, "unit", null, 1, 1, Timer.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(binaryPatternOperatorTypeEEnum, BinaryPatternOperatorType.class, "BinaryPatternOperatorType");
		addEEnumLiteral(binaryPatternOperatorTypeEEnum, BinaryPatternOperatorType.AND);
		addEEnumLiteral(binaryPatternOperatorTypeEEnum, BinaryPatternOperatorType.OR);
		addEEnumLiteral(binaryPatternOperatorTypeEEnum, BinaryPatternOperatorType.XOR);
		addEEnumLiteral(binaryPatternOperatorTypeEEnum, BinaryPatternOperatorType.PRECEDES);
		addEEnumLiteral(binaryPatternOperatorTypeEEnum, BinaryPatternOperatorType.REPEAT_UNTIL);

		initEEnum(timerTypeEEnum, TimerType.class, "TimerType");
		addEEnumLiteral(timerTypeEEnum, TimerType.WITHIN);
		addEEnumLiteral(timerTypeEEnum, TimerType.WITHIN_MAX);
		addEEnumLiteral(timerTypeEEnum, TimerType.INTERVAL);

		initEEnum(unaryPatternOperatorTypeEEnum, UnaryPatternOperatorType.class, "UnaryPatternOperatorType");
		addEEnumLiteral(unaryPatternOperatorTypeEEnum, UnaryPatternOperatorType.EVERY);
		addEEnumLiteral(unaryPatternOperatorTypeEEnum, UnaryPatternOperatorType.NOT);
		addEEnumLiteral(unaryPatternOperatorTypeEEnum, UnaryPatternOperatorType.REPEAT);
		addEEnumLiteral(unaryPatternOperatorTypeEEnum, UnaryPatternOperatorType.WHEN);

		initEEnum(statusTypeEEnum, StatusType.class, "StatusType");
		addEEnumLiteral(statusTypeEEnum, StatusType.CRITICAL);
		addEEnumLiteral(statusTypeEEnum, StatusType.WARNING);
		addEEnumLiteral(statusTypeEEnum, StatusType.SUCCESS);
		addEEnumLiteral(statusTypeEEnum, StatusType.FATAL);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
		   });	
		addAnnotation
		  (binaryEventPatternEClass, 
		   source, 
		   new String[] {
			 "constraints", "binary_event_pattern_at_least_left_right no_timer_both_events_in_pattern no_same_left_right_event_binary_pattern binary_event_pattern_timer_one_event binary_event_pattern_occur_bounds"
		   });	
		addAnnotation
		  (unaryEventPatternEClass, 
		   source, 
		   new String[] {
			 "constraints", "unary_event_pattern_correct_values_per_operator unary_event_pattern_correct_occur_num"
		   });	
		addAnnotation
		  (functionalEventInstanceEClass, 
		   source, 
		   new String[] {
			 "constraints", "functional_event_instance_correct_type functional_event_instance_correct_component"
		   });	
		addAnnotation
		  (nonFunctionalEventInstanceEClass, 
		   source, 
		   new String[] {
			 "constraints", "nonfunctional_event_instance_correct_type event_instance_metric_in_event"
		   });	
		addAnnotation
		  (horizontalScalingActionEClass, 
		   source, 
		   new String[] {
			 "constraints", "horizontal_scale_action_correct_count horizontal_scale_action_req_align"
		   });	
		addAnnotation
		  (verticalScalingActionEClass, 
		   source, 
		   new String[] {
			 "constraints", "vertical_scaling_action"
		   });	
		addAnnotation
		  (timerEClass, 
		   source, 
		   new String[] {
			 "constraints", "timer_correct_values"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";	
		addAnnotation
		  (getEventPattern__IncludesEvent__SingleEvent(), 
		   source, 
		   new String[] {
			 "body", "if (self.oclIsTypeOf(UnaryEventPattern))\n\t\t\t\t\t\tthen self.oclAsType(UnaryEventPattern).event = e\n\t\t\t\t\t\telse (includesLeftEvent(e) or includesRightEvent(e))\n\t\t\t\t\t\tendif"
		   });	
		addAnnotation
		  (getEventPattern__IncludesLeftEvent__SingleEvent(), 
		   source, 
		   new String[] {
			 "body", "if (self.oclAsType(BinaryEventPattern).leftEvent.oclIsKindOf(EventPattern))\n\t\t\t\t\t\tthen self.oclAsType(BinaryEventPattern).leftEvent.oclAsType(EventPattern).includesEvent(e)\n\t\t\t\t\t\telse (if (self.oclAsType(BinaryEventPattern).leftEvent.oclIsKindOf(SingleEvent))\n\t\t\t\t\t\t\tthen self.oclAsType(BinaryEventPattern).leftEvent.oclAsType(SingleEvent) = e\n\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\tendif)\n\t\t\t\t\t\tendif"
		   });	
		addAnnotation
		  (getEventPattern__IncludesRightEvent__SingleEvent(), 
		   source, 
		   new String[] {
			 "body", "if (self.oclAsType(BinaryEventPattern).rightEvent.oclIsKindOf(EventPattern))\n\t\t\t\t\t\tthen self.oclAsType(BinaryEventPattern).rightEvent.oclAsType(EventPattern).includesEvent(e)\n\t\t\t\t\t\telse (if (self.oclAsType(BinaryEventPattern).rightEvent.oclIsKindOf(SingleEvent))\n\t\t\t\t\t\t\tthen self.oclAsType(BinaryEventPattern).rightEvent.oclAsType(SingleEvent) = e\n\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\tendif)\n\t\t\t\t\t\tendif"
		   });	
		addAnnotation
		  (binaryEventPatternEClass, 
		   source, 
		   new String[] {
			 "binary_event_pattern_at_least_left_right", "Tuple {\n\tmessage : String = \'In BinaryEventPattern: \' + self.name + \' no left or right event has been specified\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.leftEvent <> null or self.rightEvent <> null)\n}.status",
			 "no_timer_both_events_in_pattern", "Tuple {\n\tmessage : String = \'In BinaryEventPattern: \' + self.name + \' only one or zero events have been specified with no timer at all\'\n\t\t\t,\n\tstatus : Boolean = asError(self.oclAsType(EventPattern).timer = null implies (self.leftEvent <> null and self.rightEvent <> null))\n}.status",
			 "no_same_left_right_event_binary_pattern", "Tuple {\n\tmessage : String = \'In BinaryEventPattern: \' + self.name + \' both the left and right events are the same\'\n\t\t\t,\n\tstatus : Boolean = asError(leftEvent <> null and rightEvent <> null implies leftEvent <> rightEvent)\n}.status",
			 "binary_event_pattern_timer_one_event", "Tuple {\n\tmessage : String = \'In BinaryEventPattern: \' + self.name + \' both a timer as well as left and right events have been specified or only one \',\n\tstatus : Boolean = \n\t\t\t\t\tasError(self.oclAsType(EventPattern).timer <> null implies (self.leftEvent = null or self.rightEvent = null))\n}.status",
			 "binary_event_pattern_occur_bounds", "Tuple {\n\tmessage : String = \'In BinaryEventPattern: \' + self.name + \' incorrect values were given for the lowerOccurrenceBound and upperOccurrenceBound attributes in conjunction with the use of the respective pattern operator: \' + operator.toString(),\n\tstatus : Boolean = \n\t\t\t\tasError((self.operator <> BinaryPatternOperatorType::REPEAT_UNTIL implies\n\t\t\t\t\t(self.lowerOccurrenceBound = 0 and self.upperOccurrenceBound = 0)) \n\t\t\t\t\tand (self.operator = BinaryPatternOperatorType::REPEAT_UNTIL \n\t\t\t\t\t\timplies (not (self.lowerOccurrenceBound = 0 and self.upperOccurrenceBound = 0) \n\t\t\t\t\t\t\tand (self.upperOccurrenceBound > 0 implies self.lowerOccurrenceBound <= upperOccurrenceBound)\n\t\t\t\t\t\t\tand \n\t\t\t\t\t\t\tif (self.lowerOccurrenceBound >= 1.0) then\n\t\t\t\t\t\t\t\t(self.lowerOccurrenceBound / self.lowerOccurrenceBound.round()) = 1 and \n\t\t\t\t\t\t\t\t\t(if (self.upperOccurrenceBound > 0) then (self.upperOccurrenceBound / self.upperOccurrenceBound.round()) = 1\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif)\n\t\t\t\t\t\t\telse \n\t\t\t\t\t\t\t\tif (self.lowerOccurrenceBound <> 0.0) then \n\t\t\t\t\t\t\t\t\tif (self.upperOccurrenceBound <= 1.0) then true\n\t\t\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\telse\n\t\t\t\t\t\t\t\t\tif (self.upperOccurrenceBound >= 1.0) then (self.upperOccurrenceBound / self.upperOccurrenceBound.round()) = 1\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t)\n\t\t\t\t\t))\n}.status"
		   });	
		addAnnotation
		  (unaryEventPatternEClass, 
		   source, 
		   new String[] {
			 "unary_event_pattern_correct_values_per_operator", "Tuple {\n\tmessage : String = \'In UnaryEventPattern: \' + self.name + \' either a REPEAT operator there is an incorrect combination of values of the operator and occurrenceNum attributes and of the timer association. When operator is REPEAT, occurrenceNum should be positive; otherwise, equal to zero. When operator is WHERE, then a timer must be specified; otherwise, no timer should be specified\',\n\tstatus : Boolean = \n\t\t\t\tasError((self.operator\n\t\t\t\t\t= UnaryPatternOperatorType::REPEAT implies occurrenceNum > 0) and (self.operator <>\n\t\t\t\t\tUnaryPatternOperatorType::REPEAT implies occurrenceNum = 0) and (self.operator = UnaryPatternOperatorType::WHEN\n\t\t\t\t\timplies self.oclAsType(EventPattern).timer <> null) and (self.operator <> UnaryPatternOperatorType::WHEN implies\n\t\t\t\t\tself.oclAsType(EventPattern).timer = null))\n}.status",
			 "unary_event_pattern_correct_occur_num", "Tuple {\n\tmessage : String = \'In UnaryEventPattern: \' + self.name + \' the occurrence num of an event should be either a positive integer or a real in [0.0,1.0]\',\n\tstatus : Boolean = \n\t\t\t\tasError(\n\t\t\t\t\tif (occurrenceNum >= 1.0) then (occurrenceNum / occurrenceNum.round()) = 1\n\t\t\t\t\telse true\n\t\t\t\t\tendif\n\t\t\t\t)\n}.status"
		   });	
		addAnnotation
		  (functionalEventInstanceEClass, 
		   source, 
		   new String[] {
			 "functional_event_instance_correct_type", "Tuple {\n\tmessage : String = \'FunctionalEventInstance: \' + self.name + \' should have a functional event as type\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.type.oclIsTypeOf(FunctionalEvent))\n}.status",
			 "functional_event_instance_correct_component", "Tuple {\n\tmessage : String = \'FunctionalEventInstance: \' + self.name + \' should refer to an instance of a component, which is referred to by its type\',\n\tstatus : Boolean = \n\t\t\t\tasError(componentInstance <> null implies (if type.oclIsTypeOf(FunctionalEvent) and type.oclAsType(FunctionalEvent).component <> null then type.oclAsType(FunctionalEvent).component = componentInstance.type else false endif))\n}.status"
		   });	
		addAnnotation
		  (nonFunctionalEventInstanceEClass, 
		   source, 
		   new String[] {
			 "nonfunctional_event_instance_correct_type", "Tuple {\n\tmessage : String = \'NonFunctionalEventInstance: \' + self.name + \' should have a nonfunctional event as type\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.type.oclIsTypeOf(NonFunctionalEvent))\n}.status",
			 "event_instance_metric_in_event", "Tuple {\n\tmessage : String = \'EventInstance: \' + self.name + \' has a metric instance with a metric which is not identical to the one associated to the event instance\\\'s event: \' + self.type.name,\n\tstatus : Boolean = \n\t\t\t\tasError(if (self.type.oclIsTypeOf(NonFunctionalEvent))\n\t\t\t\t\tthen metricInstance.metricContext.metric = type.oclAsType(NonFunctionalEvent).metricConstraint.metricContext.metric\n\t\t\t\t\telse true\n\t\t\t\t\tendif)\n}.status"
		   });	
		addAnnotation
		  (horizontalScalingActionEClass, 
		   source, 
		   new String[] {
			 "horizontal_scale_action_correct_count", "Tuple {\n\tmessage : String = \'HorizontalScalingAction: \' + self.name + \' has a wrong (negative) value for the count attribute\',\n\tstatus : Boolean = \n\t\t\t\tasError(count <> 0)\n}.status",
			 "horizontal_scale_action_req_align", "Tuple {\n\tmessage : String = \'HorizontalScalingAction: \' + self.name + \' has a count which along with the minimum instance number of the respective sw component exceeds the maximum instance number of that component\',\n\tstatus : Boolean = \n\t\t\t\tasError(softwareComponent.requirementSet <> null and softwareComponent.requirementSet.horizontalScaleRequirement <> null implies \n\t\t\t\t(softwareComponent.requirementSet.horizontalScaleRequirement.maxInstances <> -1 implies  \n\t\t\t\t\tsoftwareComponent.requirementSet.horizontalScaleRequirement.maxInstances >= softwareComponent.requirementSet.horizontalScaleRequirement.minInstances + count\n\t\t\t\t\tand softwareComponent.requirementSet.horizontalScaleRequirement.minInstances <= softwareComponent.requirementSet.horizontalScaleRequirement.maxInstances + count\n\t\t\t\t))\n}.status"
		   });	
		addAnnotation
		  (verticalScalingActionEClass, 
		   source, 
		   new String[] {
			 "vertical_scaling_action", "Tuple {\n\tmessage : String = \'VerticalScalingAction: \' + self.name + \' should have at least one feature or attribute being specified\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.subFeatures->size() <> 0 xor self.attributes->size() <> 0)\n}.status"
		   });	
		addAnnotation
		  (timerEClass, 
		   source, 
		   new String[] {
			 "timer_correct_values", "Tuple {\n\tmessage : String = \'Timer: \' + self.toString() + \' as wrong value combinations for its attributes. This means that either the timeValue is non-positive or the time type is WITHIN_MAX and a non-positive maxOccurrenceNum has been provided or that the time type is not WITHIN_MAX and a positive maxOccurrenceNum has been given\',\n\tstatus : Boolean = \n\t\t\t\tasError(timeValue > 0 and (self.type = TimerType::WITHIN_MAX implies self.maxOccurrenceNum > 0) and (self.type <>\n\t\t\t\t\tTimerType::WITHIN_MAX implies self.maxOccurrenceNum = 0))\n}.status"
		   });
	}

} //ScalabilityPackageImpl
