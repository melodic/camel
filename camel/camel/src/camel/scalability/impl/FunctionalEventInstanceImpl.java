/**
 */
package camel.scalability.impl;

import camel.deployment.ComponentInstance;

import camel.scalability.FunctionalEventInstance;
import camel.scalability.ScalabilityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Functional Event Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.impl.FunctionalEventInstanceImpl#getComponentInstance <em>Component Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class FunctionalEventInstanceImpl extends EventInstanceImpl implements FunctionalEventInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FunctionalEventInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.FUNCTIONAL_EVENT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getComponentInstance() {
		return (ComponentInstance)eGet(ScalabilityPackage.Literals.FUNCTIONAL_EVENT_INSTANCE__COMPONENT_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentInstance(ComponentInstance newComponentInstance) {
		eSet(ScalabilityPackage.Literals.FUNCTIONAL_EVENT_INSTANCE__COMPONENT_INSTANCE, newComponentInstance);
	}

} //FunctionalEventInstanceImpl
