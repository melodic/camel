/**
 */
package camel.scalability.impl;

import camel.scalability.EventPattern;
import camel.scalability.ScalabilityPackage;
import camel.scalability.SingleEvent;
import camel.scalability.Timer;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.WrappedException;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Pattern</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.impl.EventPatternImpl#getTimer <em>Timer</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class EventPatternImpl extends EventImpl implements EventPattern {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventPatternImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.EVENT_PATTERN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Timer getTimer() {
		return (Timer)eGet(ScalabilityPackage.Literals.EVENT_PATTERN__TIMER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimer(Timer newTimer) {
		eSet(ScalabilityPackage.Literals.EVENT_PATTERN__TIMER, newTimer);
	}

	/**
	 * The cached invocation delegate for the '{@link #includesEvent(camel.scalability.SingleEvent) <em>Includes Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #includesEvent(camel.scalability.SingleEvent)
	 * @generated
	 * @ordered
	 */
	protected static final EOperation.Internal.InvocationDelegate INCLUDES_EVENT_SINGLE_EVENT__EINVOCATION_DELEGATE = ((EOperation.Internal)ScalabilityPackage.Literals.EVENT_PATTERN___INCLUDES_EVENT__SINGLEEVENT).getInvocationDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean includesEvent(SingleEvent e) {
		try {
			return (Boolean)INCLUDES_EVENT_SINGLE_EVENT__EINVOCATION_DELEGATE.dynamicInvoke(this, new BasicEList.UnmodifiableEList<Object>(1, new Object[]{e}));
		}
		catch (InvocationTargetException ite) {
			throw new WrappedException(ite);
		}
	}

	/**
	 * The cached invocation delegate for the '{@link #includesLeftEvent(camel.scalability.SingleEvent) <em>Includes Left Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #includesLeftEvent(camel.scalability.SingleEvent)
	 * @generated
	 * @ordered
	 */
	protected static final EOperation.Internal.InvocationDelegate INCLUDES_LEFT_EVENT_SINGLE_EVENT__EINVOCATION_DELEGATE = ((EOperation.Internal)ScalabilityPackage.Literals.EVENT_PATTERN___INCLUDES_LEFT_EVENT__SINGLEEVENT).getInvocationDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean includesLeftEvent(SingleEvent e) {
		try {
			return (Boolean)INCLUDES_LEFT_EVENT_SINGLE_EVENT__EINVOCATION_DELEGATE.dynamicInvoke(this, new BasicEList.UnmodifiableEList<Object>(1, new Object[]{e}));
		}
		catch (InvocationTargetException ite) {
			throw new WrappedException(ite);
		}
	}

	/**
	 * The cached invocation delegate for the '{@link #includesRightEvent(camel.scalability.SingleEvent) <em>Includes Right Event</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #includesRightEvent(camel.scalability.SingleEvent)
	 * @generated
	 * @ordered
	 */
	protected static final EOperation.Internal.InvocationDelegate INCLUDES_RIGHT_EVENT_SINGLE_EVENT__EINVOCATION_DELEGATE = ((EOperation.Internal)ScalabilityPackage.Literals.EVENT_PATTERN___INCLUDES_RIGHT_EVENT__SINGLEEVENT).getInvocationDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean includesRightEvent(SingleEvent e) {
		try {
			return (Boolean)INCLUDES_RIGHT_EVENT_SINGLE_EVENT__EINVOCATION_DELEGATE.dynamicInvoke(this, new BasicEList.UnmodifiableEList<Object>(1, new Object[]{e}));
		}
		catch (InvocationTargetException ite) {
			throw new WrappedException(ite);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ScalabilityPackage.EVENT_PATTERN___INCLUDES_EVENT__SINGLEEVENT:
				return includesEvent((SingleEvent)arguments.get(0));
			case ScalabilityPackage.EVENT_PATTERN___INCLUDES_LEFT_EVENT__SINGLEEVENT:
				return includesLeftEvent((SingleEvent)arguments.get(0));
			case ScalabilityPackage.EVENT_PATTERN___INCLUDES_RIGHT_EVENT__SINGLEEVENT:
				return includesRightEvent((SingleEvent)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //EventPatternImpl
