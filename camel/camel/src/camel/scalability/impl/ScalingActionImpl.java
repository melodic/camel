/**
 */
package camel.scalability.impl;

import camel.core.impl.ActionImpl;

import camel.deployment.SoftwareComponent;
import camel.scalability.ScalabilityPackage;
import camel.scalability.ScalingAction;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Scaling Action</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.scalability.impl.ScalingActionImpl#getSoftwareComponent <em>Software Component</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ScalingActionImpl extends ActionImpl implements ScalingAction {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScalingActionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ScalabilityPackage.Literals.SCALING_ACTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareComponent getSoftwareComponent() {
		return (SoftwareComponent)eGet(ScalabilityPackage.Literals.SCALING_ACTION__SOFTWARE_COMPONENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSoftwareComponent(SoftwareComponent newSoftwareComponent) {
		eSet(ScalabilityPackage.Literals.SCALING_ACTION__SOFTWARE_COMPONENT, newSoftwareComponent);
	}

} //ScalingActionImpl
