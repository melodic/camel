/**
 */
package camel.type;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Numeric Value</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.type.TypePackage#getNumericValue()
 * @model abstract="true"
 * @generated
 */
public interface NumericValue extends Value {
} // NumericValue
