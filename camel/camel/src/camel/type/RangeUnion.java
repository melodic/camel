/**
 */
package camel.type;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range Union</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.type.RangeUnion#getRanges <em>Ranges</em>}</li>
 * </ul>
 *
 * @see camel.type.TypePackage#getRangeUnion()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='same_primitive_types_in_union_ranges correct_range_union_sequence'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot same_primitive_types_in_union_ranges='Tuple {\n\tmessage : String = \'RangeUnion: \' + self.name + \' has one or more ranges with a different primitive type\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.ranges-&gt;forAll(p | p.primitiveType = self.primitiveType))\n}.status' correct_range_union_sequence='Tuple {\n\tmessage : String = \'RangeUnion: \' + self.name + \' has a wrong sequence of ranges. This means that two or more ranges are conflicting\',\n\tstatus : Boolean = \n\t\t\t\tasError(not (invalidRangeSequence(self)))\n}.status'"
 * @generated
 */
public interface RangeUnion extends ValueType {
	/**
	 * Returns the value of the '<em><b>Ranges</b></em>' containment reference list.
	 * The list contents are of type {@link camel.type.Range}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Ranges</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Ranges</em>' containment reference list.
	 * @see camel.type.TypePackage#getRangeUnion_Ranges()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Range> getRanges();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" nRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='ranges\n\t\t\t\t\t\t-&gt;exists(p | p.includesValue(n))'"
	 * @generated
	 */
	boolean includesValue(double n);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" ruRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='EList&lt;Range&gt; ranges = ru.getRanges();\n\t\t\t\tRange prev = ranges.get(0);\n\t\t\t\tfor (int i = 1; i &lt; ranges.size(); i++){\n\t\t\t\t\tRange next = ranges.get(i);\n\t\t\t\t\tcamel.type.Limit lowerLimit = prev.getUpperLimit();\n\t\t\t\t\tcamel.type.Limit upperLimit = next.getLowerLimit();\n\t\t\t\t\tif (lowerLimit == null || upperLimit == null) return Boolean.TRUE;\n\t\t\t\t\tboolean lowerInclusive = lowerLimit.isIncluded();\n\t\t\t\t\tboolean upperInclusive = upperLimit.isIncluded();\n\t\t\t\t\tdouble low = 0.0, upper = 0.0;\n\t\t\t\t\t//Checking if already at end (positive infinity or next range starts with negative infinity\n\t\t\t\t\tcamel.type.NumericValue prevVal = lowerLimit.getValue();\n\t\t\t\t\tcamel.type.NumericValue nextVal = upperLimit.getValue();\n\t\t\t\t\t//Checking now that low is less or equal to upper\n\t\t\t\t\tif (prevVal instanceof camel.type.IntValue){\n\t\t\t\t\t\tlow = ((camel.type.IntValue)prevVal).getValue();\n\t\t\t\t\t\tif (!lowerInclusive){\n\t\t\t\t\t\t\tlow = low -1;\n\t\t\t\t\t\t\tlowerInclusive = true;\n\t\t\t\t\t\t}\n\t\t\t\t\t}\n\t\t\t\t\telse if (prevVal instanceof camel.type.FloatValue) low = ((camel.type.FloatValue)prevVal).getValue();\n\t\t\t\t\telse low = ((camel.type.DoubleValue)prevVal).getValue();\n\t\t\t\t\tif (nextVal instanceof camel.type.IntValue){\n\t\t\t\t\t\tupper = ((camel.type.IntValue)nextVal).getValue();\n\t\t\t\t\t\tif (!upperInclusive){\n\t\t\t\t\t\t\tupper = upper + 1;\n\t\t\t\t\t\t\tupperInclusive = true;\n\t\t\t\t\t\t}\n\t\t\t\t\t}\n\t\t\t\t\telse if (nextVal instanceof camel.type.FloatValue) upper = ((camel.type.FloatValue)nextVal).getValue();\n\t\t\t\t\telse upper = ((camel.type.DoubleValue)nextVal).getValue();\n\t\t\t\t\tSystem.out.println(\"Low is: \" + low + \" upper is: \" + upper);\n\t\t\t\t\tif (low &gt; upper || (low == upper &amp;&amp; lowerInclusive == true )) return Boolean.TRUE;\n\t\t\t\t\tprev = next;\n\t\t\t\t}\n\t\t\t\treturn Boolean.FALSE;'"
	 * @generated
	 */
	boolean invalidRangeSequence(RangeUnion ru);

} // RangeUnion
