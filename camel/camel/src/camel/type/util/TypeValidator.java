/**
 */
package camel.type.util;

import camel.type.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.type.TypePackage
 * @generated
 */
public class TypeValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final TypeValidator INSTANCE = new TypeValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.type";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return TypePackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case TypePackage.TYPE_MODEL:
				return validateTypeModel((TypeModel)value, diagnostics, context);
			case TypePackage.LIMIT:
				return validateLimit((Limit)value, diagnostics, context);
			case TypePackage.VALUE:
				return validateValue((Value)value, diagnostics, context);
			case TypePackage.BOOLEAN_VALUE:
				return validateBooleanValue((BooleanValue)value, diagnostics, context);
			case TypePackage.NUMERIC_VALUE:
				return validateNumericValue((NumericValue)value, diagnostics, context);
			case TypePackage.INT_VALUE:
				return validateIntValue((IntValue)value, diagnostics, context);
			case TypePackage.FLOAT_VALUE:
				return validateFloatValue((FloatValue)value, diagnostics, context);
			case TypePackage.DOUBLE_VALUE:
				return validateDoubleValue((DoubleValue)value, diagnostics, context);
			case TypePackage.STRING_VALUE:
				return validateStringValue((StringValue)value, diagnostics, context);
			case TypePackage.VALUE_TYPE:
				return validateValueType((ValueType)value, diagnostics, context);
			case TypePackage.BOOLEAN_VALUE_TYPE:
				return validateBooleanValueType((BooleanValueType)value, diagnostics, context);
			case TypePackage.LIST:
				return validateList((List)value, diagnostics, context);
			case TypePackage.RANGE:
				return validateRange((Range)value, diagnostics, context);
			case TypePackage.RANGE_UNION:
				return validateRangeUnion((RangeUnion)value, diagnostics, context);
			case TypePackage.STRING_VALUE_TYPE:
				return validateStringValueType((StringValueType)value, diagnostics, context);
			case TypePackage.PRIMITIVE_TYPE:
				return validatePrimitiveType((PrimitiveType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateTypeModel(TypeModel typeModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)typeModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLimit(Limit limit, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)limit, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateValue(Value value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)value, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBooleanValue(BooleanValue booleanValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)booleanValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNumericValue(NumericValue numericValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)numericValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIntValue(IntValue intValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)intValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFloatValue(FloatValue floatValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)floatValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDoubleValue(DoubleValue doubleValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)doubleValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStringValue(StringValue stringValue, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)stringValue, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateValueType(ValueType valueType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)valueType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBooleanValueType(BooleanValueType booleanValueType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)booleanValueType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateList(List list, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)list, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)list, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)list, diagnostics, context);
		if (result || diagnostics != null) result &= validateList_list_must_have_type(list, diagnostics, context);
		if (result || diagnostics != null) result &= validateList_all_list_values_correct_type(list, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the list_must_have_type constraint of '<em>List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String LIST__LIST_MUST_HAVE_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'List:' + self.toString() + ' should either have a primitive or a normal type of values',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(primitiveType <> null xor type <> null)\n" +
		"}.status";

	/**
	 * Validates the list_must_have_type constraint of '<em>List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateList_list_must_have_type(List list, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TypePackage.Literals.LIST,
				 (EObject)list,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "list_must_have_type",
				 LIST__LIST_MUST_HAVE_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the all_list_values_correct_type constraint of '<em>List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String LIST__ALL_LIST_VALUES_CORRECT_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'List: ' + self.toString() + ' has one or more values that do not conform to its value type',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(values->forAll(p | self.checkValueType(p)))\n" +
		"}.status";

	/**
	 * Validates the all_list_values_correct_type constraint of '<em>List</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateList_all_list_values_correct_type(List list, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TypePackage.Literals.LIST,
				 (EObject)list,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "all_list_values_correct_type",
				 LIST__ALL_LIST_VALUES_CORRECT_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRange(Range range, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)range, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)range, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)range, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)range, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)range, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)range, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)range, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)range, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)range, diagnostics, context);
		if (result || diagnostics != null) result &= validateRange_correct_range_type(range, diagnostics, context);
		if (result || diagnostics != null) result &= validateRange_enforce_correct_range_type(range, diagnostics, context);
		if (result || diagnostics != null) result &= validateRange_range_low_less_than_upper(range, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the correct_range_type constraint of '<em>Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String RANGE__CORRECT_RANGE_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Range: ' + self.toString() + ' has a non-numeric primitiveType: ' + primitiveType.toString(),\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((primitiveType = PrimitiveType::IntType) or (primitiveType = PrimitiveType::FloatType) or\n" +
		"\t\t\t\t\t(primitiveType = PrimitiveType::DoubleType))\n" +
		"}.status";

	/**
	 * Validates the correct_range_type constraint of '<em>Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRange_correct_range_type(Range range, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TypePackage.Literals.RANGE,
				 (EObject)range,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_range_type",
				 RANGE__CORRECT_RANGE_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the enforce_correct_range_type constraint of '<em>Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String RANGE__ENFORCE_CORRECT_RANGE_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Range: ' + self.toString() + ' has either its low or upper limit not conforming to its primitive type',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((self.lowerLimit = null or self.checkType(self.lowerLimit,\n" +
		"\t\t\t\t\t\tprimitiveType)) and (self.upperLimit = null or self.checkType(self.upperLimit, primitiveType)))\n" +
		"}.status";

	/**
	 * Validates the enforce_correct_range_type constraint of '<em>Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRange_enforce_correct_range_type(Range range, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TypePackage.Literals.RANGE,
				 (EObject)range,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "enforce_correct_range_type",
				 RANGE__ENFORCE_CORRECT_RANGE_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the range_low_less_than_upper constraint of '<em>Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String RANGE__RANGE_LOW_LESS_THAN_UPPER__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Range: ' + self.toString() + ' has its lower limit greater than the upper',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(if (lowerLimit <> null) and\n" +
		"\t\t\t\t\t\t(upperLimit <> null)\n" +
		"\t\t\t\t\tthen if (primitiveType = PrimitiveType::IntType)\n" +
		"\t\t\t\t\t\tthen if (not (upperLimit.included) and not (lowerLimit.included))\n" +
		"\t\t\t\t\t\t\tthen (upperLimit.value.oclAsType(IntValue).value -\n" +
		"\t\t\t\t\t\t\t\tlowerLimit.value.oclAsType(IntValue).value.oclAsType(ecore::EInt)) >= 2\n" +
		"\t\t\t\t\t\t\telse if (upperLimit.included)\n" +
		"\t\t\t\t\t\t\t\tthen lowerLimit.value.oclAsType(IntValue).value.oclAsType(ecore::EInt) <=\n" +
		"\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(IntValue).value\n" +
		"\t\t\t\t\t\t\t\telse lowerLimit.value.oclAsType(IntValue).value.oclAsType(ecore::EInt) <\n" +
		"\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(IntValue).value\n" +
		"\t\t\t\t\t\t\t\tendif\n" +
		"\t\t\t\t\t\t\tendif\n" +
		"\t\t\t\t\t\telse (if (primitiveType = PrimitiveType::FloatType)\n" +
		"\t\t\t\t\t\t\tthen if (upperLimit.included)\n" +
		"\t\t\t\t\t\t\t\tthen lowerLimit.value.oclAsType(FloatValue).value.oclAsType(ecore::EFloat) <=\n" +
		"\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(FloatValue).value\n" +
		"\t\t\t\t\t\t\t\telse lowerLimit.value.oclAsType(FloatValue).value.oclAsType(ecore::EFloat) <\n" +
		"\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(FloatValue).value\n" +
		"\t\t\t\t\t\t\t\tendif\n" +
		"\t\t\t\t\t\t\telse if (upperLimit.included)\n" +
		"\t\t\t\t\t\t\t\tthen lowerLimit.value.oclAsType(DoubleValue).value.oclAsType(ecore::EDouble) <=\n" +
		"\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(DoubleValue).value\n" +
		"\t\t\t\t\t\t\t\telse lowerLimit.value.oclAsType(DoubleValue).value.oclAsType(ecore::EDouble) <\n" +
		"\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(DoubleValue).value\n" +
		"\t\t\t\t\t\t\t\tendif\n" +
		"\t\t\t\t\t\t\tendif)\n" +
		"\t\t\t\t\t\tendif\n" +
		"\t\t\t\t\telse true\n" +
		"\t\t\t\t\tendif)\n" +
		"}.status";

	/**
	 * Validates the range_low_less_than_upper constraint of '<em>Range</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRange_range_low_less_than_upper(Range range, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TypePackage.Literals.RANGE,
				 (EObject)range,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "range_low_less_than_upper",
				 RANGE__RANGE_LOW_LESS_THAN_UPPER__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRangeUnion(RangeUnion rangeUnion, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)rangeUnion, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)rangeUnion, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)rangeUnion, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)rangeUnion, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)rangeUnion, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)rangeUnion, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)rangeUnion, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)rangeUnion, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)rangeUnion, diagnostics, context);
		if (result || diagnostics != null) result &= validateRangeUnion_same_primitive_types_in_union_ranges(rangeUnion, diagnostics, context);
		if (result || diagnostics != null) result &= validateRangeUnion_correct_range_union_sequence(rangeUnion, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the same_primitive_types_in_union_ranges constraint of '<em>Range Union</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String RANGE_UNION__SAME_PRIMITIVE_TYPES_IN_UNION_RANGES__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'RangeUnion: ' + self.name + ' has one or more ranges with a different primitive type',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.ranges->forAll(p | p.primitiveType = self.primitiveType))\n" +
		"}.status";

	/**
	 * Validates the same_primitive_types_in_union_ranges constraint of '<em>Range Union</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRangeUnion_same_primitive_types_in_union_ranges(RangeUnion rangeUnion, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TypePackage.Literals.RANGE_UNION,
				 (EObject)rangeUnion,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "same_primitive_types_in_union_ranges",
				 RANGE_UNION__SAME_PRIMITIVE_TYPES_IN_UNION_RANGES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the correct_range_union_sequence constraint of '<em>Range Union</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String RANGE_UNION__CORRECT_RANGE_UNION_SEQUENCE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'RangeUnion: ' + self.name + ' has a wrong sequence of ranges. This means that two or more ranges are conflicting',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(not (invalidRangeSequence(self)))\n" +
		"}.status";

	/**
	 * Validates the correct_range_union_sequence constraint of '<em>Range Union</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRangeUnion_correct_range_union_sequence(RangeUnion rangeUnion, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(TypePackage.Literals.RANGE_UNION,
				 (EObject)rangeUnion,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_range_union_sequence",
				 RANGE_UNION__CORRECT_RANGE_UNION_SEQUENCE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateStringValueType(StringValueType stringValueType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)stringValueType, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePrimitiveType(PrimitiveType primitiveType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //TypeValidator
