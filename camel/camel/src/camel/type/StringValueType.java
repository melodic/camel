/**
 */
package camel.type;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>String Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.type.TypePackage#getStringValueType()
 * @model
 * @generated
 */
public interface StringValueType extends ValueType {
} // StringValueType
