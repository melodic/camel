/**
 */
package camel.type;

import camel.core.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.type.TypeModel#getValueTypes <em>Value Types</em>}</li>
 *   <li>{@link camel.type.TypeModel#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @see camel.type.TypePackage#getTypeModel()
 * @model
 * @generated
 */
public interface TypeModel extends Model {
	/**
	 * Returns the value of the '<em><b>Value Types</b></em>' containment reference list.
	 * The list contents are of type {@link camel.type.ValueType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Types</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Types</em>' containment reference list.
	 * @see camel.type.TypePackage#getTypeModel_ValueTypes()
	 * @model containment="true"
	 * @generated
	 */
	EList<ValueType> getValueTypes();

	/**
	 * Returns the value of the '<em><b>Values</b></em>' containment reference list.
	 * The list contents are of type {@link camel.type.Value}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' containment reference list.
	 * @see camel.type.TypePackage#getTypeModel_Values()
	 * @model containment="true"
	 * @generated
	 */
	EList<Value> getValues();

} // TypeModel
