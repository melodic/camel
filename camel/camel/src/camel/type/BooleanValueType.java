/**
 */
package camel.type;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Boolean Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.type.TypePackage#getBooleanValueType()
 * @model
 * @generated
 */
public interface BooleanValueType extends ValueType {
} // BooleanValueType
