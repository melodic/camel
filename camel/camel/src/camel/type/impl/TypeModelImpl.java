/**
 */
package camel.type.impl;

import camel.core.impl.ModelImpl;

import camel.type.TypeModel;
import camel.type.TypePackage;
import camel.type.Value;
import camel.type.ValueType;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.type.impl.TypeModelImpl#getValueTypes <em>Value Types</em>}</li>
 *   <li>{@link camel.type.impl.TypeModelImpl#getValues <em>Values</em>}</li>
 * </ul>
 *
 * @generated
 */
public class TypeModelImpl extends ModelImpl implements TypeModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TypeModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypePackage.Literals.TYPE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ValueType> getValueTypes() {
		return (EList<ValueType>)eGet(TypePackage.Literals.TYPE_MODEL__VALUE_TYPES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Value> getValues() {
		return (EList<Value>)eGet(TypePackage.Literals.TYPE_MODEL__VALUES, true);
	}

} //TypeModelImpl
