/**
 */
package camel.type.impl;

import camel.constraint.ConstraintPackage;

import camel.constraint.impl.ConstraintPackageImpl;

import camel.core.CorePackage;

import camel.core.impl.CorePackageImpl;

import camel.data.DataPackage;

import camel.data.impl.DataPackageImpl;

import camel.deployment.DeploymentPackage;

import camel.deployment.impl.DeploymentPackageImpl;

import camel.execution.ExecutionPackage;

import camel.execution.impl.ExecutionPackageImpl;

import camel.location.LocationPackage;

import camel.location.impl.LocationPackageImpl;

import camel.metric.MetricPackage;

import camel.metric.impl.MetricPackageImpl;

import camel.mms.MmsPackage;

import camel.mms.impl.MmsPackageImpl;

import camel.organisation.OrganisationPackage;

import camel.organisation.impl.OrganisationPackageImpl;

import camel.requirement.RequirementPackage;

import camel.requirement.impl.RequirementPackageImpl;

import camel.scalability.ScalabilityPackage;

import camel.scalability.impl.ScalabilityPackageImpl;

import camel.security.SecurityPackage;

import camel.security.impl.SecurityPackageImpl;

import camel.type.BooleanValue;
import camel.type.BooleanValueType;
import camel.type.DoubleValue;
import camel.type.FloatValue;
import camel.type.IntValue;
import camel.type.Limit;
import camel.type.List;
import camel.type.NumericValue;
import camel.type.PrimitiveType;
import camel.type.Range;
import camel.type.RangeUnion;
import camel.type.StringValue;
import camel.type.StringValueType;
import camel.type.TypeFactory;
import camel.type.TypeModel;
import camel.type.TypePackage;
import camel.type.Value;
import camel.type.ValueType;

import camel.type.util.TypeValidator;

import camel.unit.UnitPackage;

import camel.unit.impl.UnitPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class TypePackageImpl extends EPackageImpl implements TypePackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass typeModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass limitEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass numericValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass intValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass floatValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass doubleValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringValueEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass valueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass booleanValueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass listEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rangeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rangeUnionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass stringValueTypeEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum primitiveTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see camel.type.TypePackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private TypePackageImpl() {
		super(eNS_URI, TypeFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link TypePackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static TypePackage init() {
		if (isInited) return (TypePackage)EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI);

		// Obtain or create and register package
		TypePackageImpl theTypePackage = (TypePackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof TypePackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new TypePackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		MmsPackageImpl theMmsPackage = (MmsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI) instanceof MmsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI) : MmsPackage.eINSTANCE);
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		DeploymentPackageImpl theDeploymentPackage = (DeploymentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI) instanceof DeploymentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI) : DeploymentPackage.eINSTANCE);
		ExecutionPackageImpl theExecutionPackage = (ExecutionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI) instanceof ExecutionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI) : ExecutionPackage.eINSTANCE);
		LocationPackageImpl theLocationPackage = (LocationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) instanceof LocationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) : LocationPackage.eINSTANCE);
		MetricPackageImpl theMetricPackage = (MetricPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI) instanceof MetricPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI) : MetricPackage.eINSTANCE);
		OrganisationPackageImpl theOrganisationPackage = (OrganisationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) instanceof OrganisationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) : OrganisationPackage.eINSTANCE);
		RequirementPackageImpl theRequirementPackage = (RequirementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) instanceof RequirementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) : RequirementPackage.eINSTANCE);
		ScalabilityPackageImpl theScalabilityPackage = (ScalabilityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI) instanceof ScalabilityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI) : ScalabilityPackage.eINSTANCE);
		SecurityPackageImpl theSecurityPackage = (SecurityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) instanceof SecurityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) : SecurityPackage.eINSTANCE);
		UnitPackageImpl theUnitPackage = (UnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) instanceof UnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) : UnitPackage.eINSTANCE);
		ConstraintPackageImpl theConstraintPackage = (ConstraintPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) instanceof ConstraintPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) : ConstraintPackage.eINSTANCE);
		DataPackageImpl theDataPackage = (DataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) instanceof DataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) : DataPackage.eINSTANCE);

		// Create package meta-data objects
		theTypePackage.createPackageContents();
		theMmsPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theDeploymentPackage.createPackageContents();
		theExecutionPackage.createPackageContents();
		theLocationPackage.createPackageContents();
		theMetricPackage.createPackageContents();
		theOrganisationPackage.createPackageContents();
		theRequirementPackage.createPackageContents();
		theScalabilityPackage.createPackageContents();
		theSecurityPackage.createPackageContents();
		theUnitPackage.createPackageContents();
		theConstraintPackage.createPackageContents();
		theDataPackage.createPackageContents();

		// Initialize created meta-data
		theTypePackage.initializePackageContents();
		theMmsPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theDeploymentPackage.initializePackageContents();
		theExecutionPackage.initializePackageContents();
		theLocationPackage.initializePackageContents();
		theMetricPackage.initializePackageContents();
		theOrganisationPackage.initializePackageContents();
		theRequirementPackage.initializePackageContents();
		theScalabilityPackage.initializePackageContents();
		theSecurityPackage.initializePackageContents();
		theUnitPackage.initializePackageContents();
		theConstraintPackage.initializePackageContents();
		theDataPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theTypePackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return TypeValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theTypePackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(TypePackage.eNS_URI, theTypePackage);
		return theTypePackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getTypeModel() {
		return typeModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeModel_ValueTypes() {
		return (EReference)typeModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getTypeModel_Values() {
		return (EReference)typeModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLimit() {
		return limitEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLimit_Included() {
		return (EAttribute)limitEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLimit_Value() {
		return (EReference)limitEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValue() {
		return valueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getValue__ValueEquals__Value() {
		return valueEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanValue() {
		return booleanValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBooleanValue_Value() {
		return (EAttribute)booleanValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getNumericValue() {
		return numericValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getIntValue() {
		return intValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getIntValue_Value() {
		return (EAttribute)intValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFloatValue() {
		return floatValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFloatValue_Value() {
		return (EAttribute)floatValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDoubleValue() {
		return doubleValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getDoubleValue_Value() {
		return (EAttribute)doubleValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringValue() {
		return stringValueEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getStringValue_Value() {
		return (EAttribute)stringValueEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getValueType() {
		return valueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getValueType_PrimitiveType() {
		return (EAttribute)valueTypeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBooleanValueType() {
		return booleanValueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getList() {
		return listEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getList_Values() {
		return (EReference)listEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getList_Type() {
		return (EReference)listEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getList__IncludesValue__Value() {
		return listEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getList__CheckValueType__Value() {
		return listEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRange() {
		return rangeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRange_LowerLimit() {
		return (EReference)rangeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRange_UpperLimit() {
		return (EReference)rangeEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRange__CheckType__Limit_PrimitiveType() {
		return rangeEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRange__IncludesValue__double() {
		return rangeEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRangeUnion() {
		return rangeUnionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRangeUnion_Ranges() {
		return (EReference)rangeUnionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRangeUnion__IncludesValue__double() {
		return rangeUnionEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getRangeUnion__InvalidRangeSequence__RangeUnion() {
		return rangeUnionEClass.getEOperations().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getStringValueType() {
		return stringValueTypeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getPrimitiveType() {
		return primitiveTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TypeFactory getTypeFactory() {
		return (TypeFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		typeModelEClass = createEClass(TYPE_MODEL);
		createEReference(typeModelEClass, TYPE_MODEL__VALUE_TYPES);
		createEReference(typeModelEClass, TYPE_MODEL__VALUES);

		limitEClass = createEClass(LIMIT);
		createEAttribute(limitEClass, LIMIT__INCLUDED);
		createEReference(limitEClass, LIMIT__VALUE);

		valueEClass = createEClass(VALUE);
		createEOperation(valueEClass, VALUE___VALUE_EQUALS__VALUE);

		booleanValueEClass = createEClass(BOOLEAN_VALUE);
		createEAttribute(booleanValueEClass, BOOLEAN_VALUE__VALUE);

		numericValueEClass = createEClass(NUMERIC_VALUE);

		intValueEClass = createEClass(INT_VALUE);
		createEAttribute(intValueEClass, INT_VALUE__VALUE);

		floatValueEClass = createEClass(FLOAT_VALUE);
		createEAttribute(floatValueEClass, FLOAT_VALUE__VALUE);

		doubleValueEClass = createEClass(DOUBLE_VALUE);
		createEAttribute(doubleValueEClass, DOUBLE_VALUE__VALUE);

		stringValueEClass = createEClass(STRING_VALUE);
		createEAttribute(stringValueEClass, STRING_VALUE__VALUE);

		valueTypeEClass = createEClass(VALUE_TYPE);
		createEAttribute(valueTypeEClass, VALUE_TYPE__PRIMITIVE_TYPE);

		booleanValueTypeEClass = createEClass(BOOLEAN_VALUE_TYPE);

		listEClass = createEClass(LIST);
		createEReference(listEClass, LIST__VALUES);
		createEReference(listEClass, LIST__TYPE);
		createEOperation(listEClass, LIST___INCLUDES_VALUE__VALUE);
		createEOperation(listEClass, LIST___CHECK_VALUE_TYPE__VALUE);

		rangeEClass = createEClass(RANGE);
		createEReference(rangeEClass, RANGE__LOWER_LIMIT);
		createEReference(rangeEClass, RANGE__UPPER_LIMIT);
		createEOperation(rangeEClass, RANGE___CHECK_TYPE__LIMIT_PRIMITIVETYPE);
		createEOperation(rangeEClass, RANGE___INCLUDES_VALUE__DOUBLE);

		rangeUnionEClass = createEClass(RANGE_UNION);
		createEReference(rangeUnionEClass, RANGE_UNION__RANGES);
		createEOperation(rangeUnionEClass, RANGE_UNION___INCLUDES_VALUE__DOUBLE);
		createEOperation(rangeUnionEClass, RANGE_UNION___INVALID_RANGE_SEQUENCE__RANGEUNION);

		stringValueTypeEClass = createEClass(STRING_VALUE_TYPE);

		// Create enums
		primitiveTypeEEnum = createEEnum(PRIMITIVE_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		typeModelEClass.getESuperTypes().add(theCorePackage.getModel());
		booleanValueEClass.getESuperTypes().add(this.getValue());
		numericValueEClass.getESuperTypes().add(this.getValue());
		intValueEClass.getESuperTypes().add(this.getNumericValue());
		floatValueEClass.getESuperTypes().add(this.getNumericValue());
		doubleValueEClass.getESuperTypes().add(this.getNumericValue());
		stringValueEClass.getESuperTypes().add(this.getValue());
		valueTypeEClass.getESuperTypes().add(theCorePackage.getNamedElement());
		booleanValueTypeEClass.getESuperTypes().add(this.getValueType());
		listEClass.getESuperTypes().add(this.getValueType());
		rangeEClass.getESuperTypes().add(this.getValueType());
		rangeUnionEClass.getESuperTypes().add(this.getValueType());
		stringValueTypeEClass.getESuperTypes().add(this.getValueType());

		// Initialize classes, features, and operations; add parameters
		initEClass(typeModelEClass, TypeModel.class, "TypeModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getTypeModel_ValueTypes(), this.getValueType(), null, "valueTypes", null, 0, -1, TypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getTypeModel_Values(), this.getValue(), null, "values", null, 0, -1, TypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(limitEClass, Limit.class, "Limit", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getLimit_Included(), ecorePackage.getEBoolean(), "included", "false", 1, 1, Limit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getLimit_Value(), this.getNumericValue(), null, "value", null, 1, 1, Limit.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueEClass, Value.class, "Value", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		EOperation op = initEOperation(getValue__ValueEquals__Value(), ecorePackage.getEBoolean(), "valueEquals", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getValue(), "v", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(booleanValueEClass, BooleanValue.class, "BooleanValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBooleanValue_Value(), ecorePackage.getEBoolean(), "value", null, 1, 1, BooleanValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(numericValueEClass, NumericValue.class, "NumericValue", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(intValueEClass, IntValue.class, "IntValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getIntValue_Value(), ecorePackage.getEInt(), "value", null, 1, 1, IntValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(floatValueEClass, FloatValue.class, "FloatValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFloatValue_Value(), ecorePackage.getEFloat(), "value", null, 1, 1, FloatValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(doubleValueEClass, DoubleValue.class, "DoubleValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getDoubleValue_Value(), ecorePackage.getEDouble(), "value", null, 1, 1, DoubleValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(stringValueEClass, StringValue.class, "StringValue", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getStringValue_Value(), ecorePackage.getEString(), "value", null, 1, 1, StringValue.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(valueTypeEClass, ValueType.class, "ValueType", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getValueType_PrimitiveType(), this.getPrimitiveType(), "primitiveType", null, 0, 1, ValueType.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(booleanValueTypeEClass, BooleanValueType.class, "BooleanValueType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(listEClass, List.class, "List", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getList_Values(), this.getValue(), null, "values", null, 1, -1, List.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getList_Type(), this.getValueType(), null, "type", null, 0, 1, List.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getList__IncludesValue__Value(), ecorePackage.getEBoolean(), "includesValue", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getValue(), "v", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getList__CheckValueType__Value(), ecorePackage.getEBoolean(), "checkValueType", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getValue(), "p", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(rangeEClass, Range.class, "Range", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRange_LowerLimit(), this.getLimit(), null, "lowerLimit", null, 0, 1, Range.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRange_UpperLimit(), this.getLimit(), null, "upperLimit", null, 0, 1, Range.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getRange__CheckType__Limit_PrimitiveType(), ecorePackage.getEBoolean(), "checkType", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getLimit(), "l", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getPrimitiveType(), "type", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRange__IncludesValue__double(), ecorePackage.getEBoolean(), "includesValue", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "n", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(rangeUnionEClass, RangeUnion.class, "RangeUnion", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRangeUnion_Ranges(), this.getRange(), null, "ranges", null, 1, -1, RangeUnion.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getRangeUnion__IncludesValue__double(), ecorePackage.getEBoolean(), "includesValue", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, ecorePackage.getEDouble(), "n", 1, 1, IS_UNIQUE, IS_ORDERED);

		op = initEOperation(getRangeUnion__InvalidRangeSequence__RangeUnion(), ecorePackage.getEBoolean(), "invalidRangeSequence", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getRangeUnion(), "ru", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(stringValueTypeEClass, StringValueType.class, "StringValueType", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		// Initialize enums and add enum literals
		initEEnum(primitiveTypeEEnum, PrimitiveType.class, "PrimitiveType");
		addEEnumLiteral(primitiveTypeEEnum, PrimitiveType.INT_TYPE);
		addEEnumLiteral(primitiveTypeEEnum, PrimitiveType.STRING_TYPE);
		addEEnumLiteral(primitiveTypeEEnum, PrimitiveType.BOOLEAN_TYPE);
		addEEnumLiteral(primitiveTypeEEnum, PrimitiveType.FLOAT_TYPE);
		addEEnumLiteral(primitiveTypeEEnum, PrimitiveType.DOUBLE_TYPE);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
		   });	
		addAnnotation
		  (listEClass, 
		   source, 
		   new String[] {
			 "constraints", "list_must_have_type all_list_values_correct_type"
		   });	
		addAnnotation
		  (rangeEClass, 
		   source, 
		   new String[] {
			 "constraints", "correct_range_type enforce_correct_range_type range_low_less_than_upper"
		   });	
		addAnnotation
		  (rangeUnionEClass, 
		   source, 
		   new String[] {
			 "constraints", "same_primitive_types_in_union_ranges correct_range_union_sequence"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";	
		addAnnotation
		  (getValue__ValueEquals__Value(), 
		   source, 
		   new String[] {
			 "body", "if (self.oclIsTypeOf(IntValue) and v.oclIsTypeOf(IntValue))\n\t\t\t\t\t\tthen self.oclAsType(IntValue).value = v.oclAsType(IntValue).value\n\t\t\t\t\t\telse if (self.oclIsTypeOf(FloatValue) and v.oclIsTypeOf(FloatValue))\n\t\t\t\t\t\t\tthen self.oclAsType(FloatValue).value = v.oclAsType(FloatValue).value\n\t\t\t\t\t\t\telse if (self.oclIsTypeOf(DoubleValue) and v.oclIsTypeOf(DoubleValue))\n\t\t\t\t\t\t\t\tthen self.oclAsType(DoubleValue).value = v.oclAsType(DoubleValue).value\n\t\t\t\t\t\t\t\telse if (self.oclIsTypeOf(StringValue) and v.oclIsTypeOf(StringValue))\n\t\t\t\t\t\t\t\t\tthen self.oclAsType(StringValue).value = v.oclAsType(StringValue).value\n\t\t\t\t\t\t\t\t\telse if (self.oclIsTypeOf(BooleanValue) and v.oclIsTypeOf(BooleanValue))\n\t\t\t\t\t\t\t\t\t\tthen self.oclAsType(BooleanValue).value = v.oclAsType(BooleanValue).value\n\t\t\t\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\tendif"
		   });	
		addAnnotation
		  (listEClass, 
		   source, 
		   new String[] {
			 "list_must_have_type", "Tuple {\n\tmessage : String = \'List:\' + self.toString() + \' should either have a primitive or a normal type of values\',\n\tstatus : Boolean = \n\t\t\t\tasError(primitiveType <> null xor type <> null)\n}.status",
			 "all_list_values_correct_type", "Tuple {\n\tmessage : String = \'List: \' + self.toString() + \' has one or more values that do not conform to its value type\',\n\tstatus : Boolean = \n\t\t\t\tasError(values->forAll(p | self.checkValueType(p)))\n}.status"
		   });	
		addAnnotation
		  (getList__IncludesValue__Value(), 
		   source, 
		   new String[] {
			 "body", "if (type <> null)\n\t\t\t\t\t\tthen if (type.oclIsTypeOf(Range))\n\t\t\t\t\t\t\tthen if (type.oclAsType(Range).primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(IntValue).value = v.oclAsType(IntValue).value)\n\t\t\t\t\t\t\t\telse if (type.oclAsType(Range).primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(FloatValue).value = v.oclAsType(FloatValue).value)\n\t\t\t\t\t\t\t\t\telse values\n\t\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(DoubleValue).value = v.oclAsType(DoubleValue).value)\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\telse if (type.oclIsTypeOf(RangeUnion))\n\t\t\t\t\t\t\t\tthen if (type.oclAsType(RangeUnion).primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(IntValue).value = v.oclAsType(IntValue).value)\n\t\t\t\t\t\t\t\t\telse if (type.oclAsType(RangeUnion).primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(FloatValue).value = v.oclAsType(FloatValue).value)\n\t\t\t\t\t\t\t\t\t\telse values\n\t\t\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(DoubleValue).value = v.oclAsType(DoubleValue).value)\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\telse if (type.oclIsTypeOf(StringValueType))\n\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(StringValue).value = v.oclAsType(StringValue).value)\n\t\t\t\t\t\t\t\t\telse if (type.oclIsTypeOf(BooleanValueType))\n\t\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(BooleanValue).value = v.oclAsType(BooleanValue).value)\n\t\t\t\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\telse if (primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(IntValue).value = v.oclAsType(IntValue).value)\n\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(FloatValue).value = v.oclAsType(FloatValue).value)\n\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::DoubleType)\n\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(DoubleValue).value = v.oclAsType(DoubleValue).value)\n\t\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::StringType)\n\t\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(StringValue).value = v.oclAsType(StringValue).value)\n\t\t\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::BooleanType)\n\t\t\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t\t\t->exists(p | p.oclAsType(BooleanValue).value = v.oclAsType(BooleanValue).value)\n\t\t\t\t\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\tendif"
		   });	
		addAnnotation
		  (getList__CheckValueType__Value(), 
		   source, 
		   new String[] {
			 "body", "if (type <> null)\n\t\t\t\t\t\tthen if (type.oclIsTypeOf(Range))\n\t\t\t\t\t\t\tthen if (type.oclAsType(Range).primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(IntValue) and\n\t\t\t\t\t\t\t\t\ttype.oclAsType(Range).includesValue(p.oclAsType(IntValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\telse if (type.oclAsType(Range).primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(FloatValue) and\n\t\t\t\t\t\t\t\t\t\ttype.oclAsType(Range).includesValue(p.oclAsType(FloatValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\telse p.oclIsTypeOf(DoubleValue) and\n\t\t\t\t\t\t\t\t\t\ttype.oclAsType(Range).includesValue(p.oclAsType(DoubleValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\telse if (type.oclIsTypeOf(RangeUnion))\n\t\t\t\t\t\t\t\tthen if (type.oclAsType(RangeUnion).primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(IntValue) and\n\t\t\t\t\t\t\t\t\t\ttype.oclAsType(RangeUnion).includesValue(p.oclAsType(IntValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\telse if (type.oclAsType(RangeUnion).primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(FloatValue) and\n\t\t\t\t\t\t\t\t\t\t\ttype.oclAsType(RangeUnion).includesValue(p.oclAsType(FloatValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\t\telse p.oclIsTypeOf(DoubleValue) and\n\t\t\t\t\t\t\t\t\t\t\ttype.oclAsType(RangeUnion).includesValue(p.oclAsType(DoubleValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\telse if (type.oclIsTypeOf(StringValueType))\n\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(StringValue)\n\t\t\t\t\t\t\t\t\telse if (type.oclIsTypeOf(BooleanValueType))\n\t\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(BooleanValue)\n\t\t\t\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\telse if (primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\tthen p.oclIsTypeOf(IntValue)\n\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::StringType)\n\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(StringValue)\n\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::BooleanType)\n\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(BooleanValue)\n\t\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(FloatValue)\n\t\t\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::DoubleType)\n\t\t\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(DoubleValue)\n\t\t\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\tendif"
		   });	
		addAnnotation
		  (rangeEClass, 
		   source, 
		   new String[] {
			 "correct_range_type", "Tuple {\n\tmessage : String = \'Range: \' + self.toString() + \' has a non-numeric primitiveType: \' + primitiveType.toString(),\n\tstatus : Boolean = \n\t\t\t\tasError((primitiveType = PrimitiveType::IntType) or (primitiveType = PrimitiveType::FloatType) or\n\t\t\t\t\t(primitiveType = PrimitiveType::DoubleType))\n}.status",
			 "enforce_correct_range_type", "Tuple {\n\tmessage : String = \'Range: \' + self.toString() + \' has either its low or upper limit not conforming to its primitive type\',\n\tstatus : Boolean = \n\t\t\t\tasError((self.lowerLimit = null or self.checkType(self.lowerLimit,\n\t\t\t\t\t\tprimitiveType)) and (self.upperLimit = null or self.checkType(self.upperLimit, primitiveType)))\n}.status",
			 "range_low_less_than_upper", "Tuple {\n\tmessage : String = \'Range: \' + self.toString() + \' has its lower limit greater than the upper\',\n\tstatus : Boolean = \n\t\t\t\tasError(if (lowerLimit <> null) and\n\t\t\t\t\t\t(upperLimit <> null)\n\t\t\t\t\tthen if (primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\tthen if (not (upperLimit.included) and not (lowerLimit.included))\n\t\t\t\t\t\t\tthen (upperLimit.value.oclAsType(IntValue).value -\n\t\t\t\t\t\t\t\tlowerLimit.value.oclAsType(IntValue).value.oclAsType(ecore::EInt)) >= 2\n\t\t\t\t\t\t\telse if (upperLimit.included)\n\t\t\t\t\t\t\t\tthen lowerLimit.value.oclAsType(IntValue).value.oclAsType(ecore::EInt) <=\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(IntValue).value\n\t\t\t\t\t\t\t\telse lowerLimit.value.oclAsType(IntValue).value.oclAsType(ecore::EInt) <\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(IntValue).value\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\telse (if (primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\tthen if (upperLimit.included)\n\t\t\t\t\t\t\t\tthen lowerLimit.value.oclAsType(FloatValue).value.oclAsType(ecore::EFloat) <=\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(FloatValue).value\n\t\t\t\t\t\t\t\telse lowerLimit.value.oclAsType(FloatValue).value.oclAsType(ecore::EFloat) <\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(FloatValue).value\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\telse if (upperLimit.included)\n\t\t\t\t\t\t\t\tthen lowerLimit.value.oclAsType(DoubleValue).value.oclAsType(ecore::EDouble) <=\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(DoubleValue).value\n\t\t\t\t\t\t\t\telse lowerLimit.value.oclAsType(DoubleValue).value.oclAsType(ecore::EDouble) <\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(DoubleValue).value\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif)\n\t\t\t\t\t\tendif\n\t\t\t\t\telse true\n\t\t\t\t\tendif)\n}.status"
		   });	
		addAnnotation
		  (getRange__CheckType__Limit_PrimitiveType(), 
		   source, 
		   new String[] {
			 "body", "\n\t\t\t\tif (type = PrimitiveType::IntType) then l.value.oclIsTypeOf(IntValue)\n\t\t\t\telse \n\t\t\t\t\tif (type = PrimitiveType::FloatType) then l.value.oclIsTypeOf(FloatValue)\n\t\t\t\t\telse lowerLimit.value.oclIsTypeOf(DoubleValue)\n\t\t\t\t \tendif\n\t\t\t\tendif"
		   });	
		addAnnotation
		  (getRange__IncludesValue__double(), 
		   source, 
		   new String[] {
			 "body", "if (primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\tthen if (lowerLimit <> null)\n\t\t\t\t\t\t\tthen ((lowerLimit.included and lowerLimit.value.oclAsType(IntValue).value <= n) or (not\n\t\t\t\t\t\t\t\t(lowerLimit.included) and lowerLimit.value.oclAsType(IntValue).value < n)) and if (upperLimit <> null)\n\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(IntValue).value >= n) or (not\n\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(IntValue).value > n)\n\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\telse if (upperLimit <> null)\n\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(IntValue).value >= n) or (not\n\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(IntValue).value > n)\n\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\telse if (primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\tthen if (lowerLimit<> null)\n\t\t\t\t\t\t\t\tthen ((lowerLimit.included and lowerLimit.value.oclAsType(FloatValue).value <= n) or (not\n\t\t\t\t\t\t\t\t\t(lowerLimit.included) and lowerLimit.value.oclAsType(FloatValue).value < n)) and if (upperLimit <> null)\n\t\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(FloatValue).value >= n) or (not\n\t\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(FloatValue).value > n)\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\telse if (upperLimit <> null)\n\t\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(FloatValue).value >= n) or (not\n\t\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(FloatValue).value > n)\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\telse if (lowerLimit <> null)\n\t\t\t\t\t\t\t\tthen ((lowerLimit.included and lowerLimit.value.oclAsType(DoubleValue).value <= n) or (not\n\t\t\t\t\t\t\t\t\t(lowerLimit.included) and lowerLimit.value.oclAsType(DoubleValue).value < n)) and if (upperLimit <> null)\n\t\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(DoubleValue).value >= n) or (not\n\t\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(DoubleValue).value > n)\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\telse if (upperLimit <> null)\n\t\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(DoubleValue).value >= n) or (not\n\t\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(DoubleValue).value > n)\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\tendif"
		   });	
		addAnnotation
		  (rangeUnionEClass, 
		   source, 
		   new String[] {
			 "same_primitive_types_in_union_ranges", "Tuple {\n\tmessage : String = \'RangeUnion: \' + self.name + \' has one or more ranges with a different primitive type\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.ranges->forAll(p | p.primitiveType = self.primitiveType))\n}.status",
			 "correct_range_union_sequence", "Tuple {\n\tmessage : String = \'RangeUnion: \' + self.name + \' has a wrong sequence of ranges. This means that two or more ranges are conflicting\',\n\tstatus : Boolean = \n\t\t\t\tasError(not (invalidRangeSequence(self)))\n}.status"
		   });	
		addAnnotation
		  (getRangeUnion__IncludesValue__double(), 
		   source, 
		   new String[] {
			 "body", "ranges\n\t\t\t\t\t\t->exists(p | p.includesValue(n))"
		   });
	}

} //TypePackageImpl
