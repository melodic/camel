/**
 */
package camel.type.impl;

import camel.type.StringValueType;
import camel.type.TypePackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>String Value Type</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class StringValueTypeImpl extends ValueTypeImpl implements StringValueType {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StringValueTypeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TypePackage.Literals.STRING_VALUE_TYPE;
	}

} //StringValueTypeImpl
