/**
 */
package camel.type;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Range</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.type.Range#getLowerLimit <em>Lower Limit</em>}</li>
 *   <li>{@link camel.type.Range#getUpperLimit <em>Upper Limit</em>}</li>
 * </ul>
 *
 * @see camel.type.TypePackage#getRange()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correct_range_type enforce_correct_range_type range_low_less_than_upper'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot correct_range_type='Tuple {\n\tmessage : String = \'Range: \' + self.toString() + \' has a non-numeric primitiveType: \' + primitiveType.toString(),\n\tstatus : Boolean = \n\t\t\t\tasError((primitiveType = PrimitiveType::IntType) or (primitiveType = PrimitiveType::FloatType) or\n\t\t\t\t\t(primitiveType = PrimitiveType::DoubleType))\n}.status' enforce_correct_range_type='Tuple {\n\tmessage : String = \'Range: \' + self.toString() + \' has either its low or upper limit not conforming to its primitive type\',\n\tstatus : Boolean = \n\t\t\t\tasError((self.lowerLimit = null or self.checkType(self.lowerLimit,\n\t\t\t\t\t\tprimitiveType)) and (self.upperLimit = null or self.checkType(self.upperLimit, primitiveType)))\n}.status' range_low_less_than_upper='Tuple {\n\tmessage : String = \'Range: \' + self.toString() + \' has its lower limit greater than the upper\',\n\tstatus : Boolean = \n\t\t\t\tasError(if (lowerLimit &lt;&gt; null) and\n\t\t\t\t\t\t(upperLimit &lt;&gt; null)\n\t\t\t\t\tthen if (primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\tthen if (not (upperLimit.included) and not (lowerLimit.included))\n\t\t\t\t\t\t\tthen (upperLimit.value.oclAsType(IntValue).value -\n\t\t\t\t\t\t\t\tlowerLimit.value.oclAsType(IntValue).value.oclAsType(ecore::EInt)) &gt;= 2\n\t\t\t\t\t\t\telse if (upperLimit.included)\n\t\t\t\t\t\t\t\tthen lowerLimit.value.oclAsType(IntValue).value.oclAsType(ecore::EInt) &lt;=\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(IntValue).value\n\t\t\t\t\t\t\t\telse lowerLimit.value.oclAsType(IntValue).value.oclAsType(ecore::EInt) &lt;\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(IntValue).value\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\telse (if (primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\tthen if (upperLimit.included)\n\t\t\t\t\t\t\t\tthen lowerLimit.value.oclAsType(FloatValue).value.oclAsType(ecore::EFloat) &lt;=\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(FloatValue).value\n\t\t\t\t\t\t\t\telse lowerLimit.value.oclAsType(FloatValue).value.oclAsType(ecore::EFloat) &lt;\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(FloatValue).value\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\telse if (upperLimit.included)\n\t\t\t\t\t\t\t\tthen lowerLimit.value.oclAsType(DoubleValue).value.oclAsType(ecore::EDouble) &lt;=\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(DoubleValue).value\n\t\t\t\t\t\t\t\telse lowerLimit.value.oclAsType(DoubleValue).value.oclAsType(ecore::EDouble) &lt;\n\t\t\t\t\t\t\t\t\tupperLimit.value.oclAsType(DoubleValue).value\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif)\n\t\t\t\t\t\tendif\n\t\t\t\t\telse true\n\t\t\t\t\tendif)\n}.status'"
 * @generated
 */
public interface Range extends ValueType {
	/**
	 * Returns the value of the '<em><b>Lower Limit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Lower Limit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Lower Limit</em>' containment reference.
	 * @see #setLowerLimit(Limit)
	 * @see camel.type.TypePackage#getRange_LowerLimit()
	 * @model containment="true"
	 * @generated
	 */
	Limit getLowerLimit();

	/**
	 * Sets the value of the '{@link camel.type.Range#getLowerLimit <em>Lower Limit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Lower Limit</em>' containment reference.
	 * @see #getLowerLimit()
	 * @generated
	 */
	void setLowerLimit(Limit value);

	/**
	 * Returns the value of the '<em><b>Upper Limit</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upper Limit</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upper Limit</em>' containment reference.
	 * @see #setUpperLimit(Limit)
	 * @see camel.type.TypePackage#getRange_UpperLimit()
	 * @model containment="true"
	 * @generated
	 */
	Limit getUpperLimit();

	/**
	 * Sets the value of the '{@link camel.type.Range#getUpperLimit <em>Upper Limit</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upper Limit</em>' containment reference.
	 * @see #getUpperLimit()
	 * @generated
	 */
	void setUpperLimit(Limit value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" lRequired="true" typeRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='\n\t\t\t\tif (type = PrimitiveType::IntType) then l.value.oclIsTypeOf(IntValue)\n\t\t\t\telse \n\t\t\t\t\tif (type = PrimitiveType::FloatType) then l.value.oclIsTypeOf(FloatValue)\n\t\t\t\t\telse lowerLimit.value.oclIsTypeOf(DoubleValue)\n\t\t\t\t \tendif\n\t\t\t\tendif'"
	 * @generated
	 */
	boolean checkType(Limit l, PrimitiveType type);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" nRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='if (primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\tthen if (lowerLimit &lt;&gt; null)\n\t\t\t\t\t\t\tthen ((lowerLimit.included and lowerLimit.value.oclAsType(IntValue).value &lt;= n) or (not\n\t\t\t\t\t\t\t\t(lowerLimit.included) and lowerLimit.value.oclAsType(IntValue).value &lt; n)) and if (upperLimit &lt;&gt; null)\n\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(IntValue).value &gt;= n) or (not\n\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(IntValue).value &gt; n)\n\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\telse if (upperLimit &lt;&gt; null)\n\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(IntValue).value &gt;= n) or (not\n\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(IntValue).value &gt; n)\n\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\telse if (primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\tthen if (lowerLimit&lt;&gt; null)\n\t\t\t\t\t\t\t\tthen ((lowerLimit.included and lowerLimit.value.oclAsType(FloatValue).value &lt;= n) or (not\n\t\t\t\t\t\t\t\t\t(lowerLimit.included) and lowerLimit.value.oclAsType(FloatValue).value &lt; n)) and if (upperLimit &lt;&gt; null)\n\t\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(FloatValue).value &gt;= n) or (not\n\t\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(FloatValue).value &gt; n)\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\telse if (upperLimit &lt;&gt; null)\n\t\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(FloatValue).value &gt;= n) or (not\n\t\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(FloatValue).value &gt; n)\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\telse if (lowerLimit &lt;&gt; null)\n\t\t\t\t\t\t\t\tthen ((lowerLimit.included and lowerLimit.value.oclAsType(DoubleValue).value &lt;= n) or (not\n\t\t\t\t\t\t\t\t\t(lowerLimit.included) and lowerLimit.value.oclAsType(DoubleValue).value &lt; n)) and if (upperLimit &lt;&gt; null)\n\t\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(DoubleValue).value &gt;= n) or (not\n\t\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(DoubleValue).value &gt; n)\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\telse if (upperLimit &lt;&gt; null)\n\t\t\t\t\t\t\t\t\tthen (upperLimit.included and upperLimit.value.oclAsType(DoubleValue).value &gt;= n) or (not\n\t\t\t\t\t\t\t\t\t\t(upperLimit.included) and upperLimit.value.oclAsType(DoubleValue).value &gt; n)\n\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\tendif'"
	 * @generated
	 */
	boolean includesValue(double n);

} // Range
