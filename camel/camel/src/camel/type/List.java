/**
 */
package camel.type;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>List</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.type.List#getValues <em>Values</em>}</li>
 *   <li>{@link camel.type.List#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see camel.type.TypePackage#getList()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='list_must_have_type all_list_values_correct_type'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot list_must_have_type='Tuple {\n\tmessage : String = \'List:\' + self.toString() + \' should either have a primitive or a normal type of values\',\n\tstatus : Boolean = \n\t\t\t\tasError(primitiveType &lt;&gt; null xor type &lt;&gt; null)\n}.status' all_list_values_correct_type='Tuple {\n\tmessage : String = \'List: \' + self.toString() + \' has one or more values that do not conform to its value type\',\n\tstatus : Boolean = \n\t\t\t\tasError(values-&gt;forAll(p | self.checkValueType(p)))\n}.status'"
 * @generated
 */
public interface List extends ValueType {
	/**
	 * Returns the value of the '<em><b>Values</b></em>' containment reference list.
	 * The list contents are of type {@link camel.type.Value}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Values</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Values</em>' containment reference list.
	 * @see camel.type.TypePackage#getList_Values()
	 * @model containment="true" required="true"
	 * @generated
	 */
	EList<Value> getValues();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(ValueType)
	 * @see camel.type.TypePackage#getList_Type()
	 * @model
	 * @generated
	 */
	ValueType getType();

	/**
	 * Sets the value of the '{@link camel.type.List#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(ValueType value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" vRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='if (type &lt;&gt; null)\n\t\t\t\t\t\tthen if (type.oclIsTypeOf(Range))\n\t\t\t\t\t\t\tthen if (type.oclAsType(Range).primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(IntValue).value = v.oclAsType(IntValue).value)\n\t\t\t\t\t\t\t\telse if (type.oclAsType(Range).primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(FloatValue).value = v.oclAsType(FloatValue).value)\n\t\t\t\t\t\t\t\t\telse values\n\t\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(DoubleValue).value = v.oclAsType(DoubleValue).value)\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\telse if (type.oclIsTypeOf(RangeUnion))\n\t\t\t\t\t\t\t\tthen if (type.oclAsType(RangeUnion).primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(IntValue).value = v.oclAsType(IntValue).value)\n\t\t\t\t\t\t\t\t\telse if (type.oclAsType(RangeUnion).primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(FloatValue).value = v.oclAsType(FloatValue).value)\n\t\t\t\t\t\t\t\t\t\telse values\n\t\t\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(DoubleValue).value = v.oclAsType(DoubleValue).value)\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\telse if (type.oclIsTypeOf(StringValueType))\n\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(StringValue).value = v.oclAsType(StringValue).value)\n\t\t\t\t\t\t\t\t\telse if (type.oclIsTypeOf(BooleanValueType))\n\t\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(BooleanValue).value = v.oclAsType(BooleanValue).value)\n\t\t\t\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\telse if (primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(IntValue).value = v.oclAsType(IntValue).value)\n\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(FloatValue).value = v.oclAsType(FloatValue).value)\n\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::DoubleType)\n\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(DoubleValue).value = v.oclAsType(DoubleValue).value)\n\t\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::StringType)\n\t\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(StringValue).value = v.oclAsType(StringValue).value)\n\t\t\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::BooleanType)\n\t\t\t\t\t\t\t\t\t\t\tthen values\n\t\t\t\t\t\t\t\t\t\t\t\t-&gt;exists(p | p.oclAsType(BooleanValue).value = v.oclAsType(BooleanValue).value)\n\t\t\t\t\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\tendif'"
	 * @generated
	 */
	boolean includesValue(Value v);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" pRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='if (type &lt;&gt; null)\n\t\t\t\t\t\tthen if (type.oclIsTypeOf(Range))\n\t\t\t\t\t\t\tthen if (type.oclAsType(Range).primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(IntValue) and\n\t\t\t\t\t\t\t\t\ttype.oclAsType(Range).includesValue(p.oclAsType(IntValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\telse if (type.oclAsType(Range).primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(FloatValue) and\n\t\t\t\t\t\t\t\t\t\ttype.oclAsType(Range).includesValue(p.oclAsType(FloatValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\telse p.oclIsTypeOf(DoubleValue) and\n\t\t\t\t\t\t\t\t\t\ttype.oclAsType(Range).includesValue(p.oclAsType(DoubleValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\telse if (type.oclIsTypeOf(RangeUnion))\n\t\t\t\t\t\t\t\tthen if (type.oclAsType(RangeUnion).primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(IntValue) and\n\t\t\t\t\t\t\t\t\t\ttype.oclAsType(RangeUnion).includesValue(p.oclAsType(IntValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\telse if (type.oclAsType(RangeUnion).primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(FloatValue) and\n\t\t\t\t\t\t\t\t\t\t\ttype.oclAsType(RangeUnion).includesValue(p.oclAsType(FloatValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\t\telse p.oclIsTypeOf(DoubleValue) and\n\t\t\t\t\t\t\t\t\t\t\ttype.oclAsType(RangeUnion).includesValue(p.oclAsType(DoubleValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\telse if (type.oclIsTypeOf(StringValueType))\n\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(StringValue)\n\t\t\t\t\t\t\t\t\telse if (type.oclIsTypeOf(BooleanValueType))\n\t\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(BooleanValue)\n\t\t\t\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\telse if (primitiveType = PrimitiveType::IntType)\n\t\t\t\t\t\t\tthen p.oclIsTypeOf(IntValue)\n\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::StringType)\n\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(StringValue)\n\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::BooleanType)\n\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(BooleanValue)\n\t\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::FloatType)\n\t\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(FloatValue)\n\t\t\t\t\t\t\t\t\t\telse if (primitiveType = PrimitiveType::DoubleType)\n\t\t\t\t\t\t\t\t\t\t\tthen p.oclIsTypeOf(DoubleValue)\n\t\t\t\t\t\t\t\t\t\t\telse true\n\t\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\tendif'"
	 * @generated
	 */
	boolean checkValueType(Value p);

} // List
