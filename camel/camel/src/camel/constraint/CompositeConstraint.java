/**
 */
package camel.constraint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.constraint.ConstraintPackage#getCompositeConstraint()
 * @model abstract="true"
 * @generated
 */
public interface CompositeConstraint extends Constraint {
} // CompositeConstraint
