/**
 */
package camel.constraint;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Logical Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.LogicalConstraint#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link camel.constraint.LogicalConstraint#getLogicalOperator <em>Logical Operator</em>}</li>
 * </ul>
 *
 * @see camel.constraint.ConstraintPackage#getLogicalConstraint()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='logical_constraint_not_recursiv_itself'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot logical_constraint_not_recursiv_itself='Tuple {\n\tmessage : String = \'LogicalConstraint: \' + self.name + \' cannot contain recursively itself\'\n\t\t\t,\n\tstatus : Boolean = asError(not self.containsConstraint(self))\n}.status'"
 * @generated
 */
public interface LogicalConstraint extends CompositeConstraint {
	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' reference list.
	 * The list contents are of type {@link camel.constraint.Constraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' reference list.
	 * @see camel.constraint.ConstraintPackage#getLogicalConstraint_Constraints()
	 * @model
	 * @generated
	 */
	EList<Constraint> getConstraints();

	/**
	 * Returns the value of the '<em><b>Logical Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.constraint.LogicalOperatorType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Logical Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Logical Operator</em>' attribute.
	 * @see camel.constraint.LogicalOperatorType
	 * @see #setLogicalOperator(LogicalOperatorType)
	 * @see camel.constraint.ConstraintPackage#getLogicalConstraint_LogicalOperator()
	 * @model required="true"
	 * @generated
	 */
	LogicalOperatorType getLogicalOperator();

	/**
	 * Sets the value of the '{@link camel.constraint.LogicalConstraint#getLogicalOperator <em>Logical Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Logical Operator</em>' attribute.
	 * @see camel.constraint.LogicalOperatorType
	 * @see #getLogicalOperator()
	 * @generated
	 */
	void setLogicalOperator(LogicalOperatorType value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" lcRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='\n\t\t\t\t\tself.constraints-&gt;includes(lc) or self.constraints-&gt;exists(con | con.oclIsTypeOf(LogicalConstraint) and con.oclAsType(LogicalConstraint).containsConstraint(lc))'"
	 * @generated
	 */
	boolean containsConstraint(LogicalConstraint lc);

} // LogicalConstraint
