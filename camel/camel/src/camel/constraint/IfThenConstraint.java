/**
 */
package camel.constraint;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>If Then Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.IfThenConstraint#getIf <em>If</em>}</li>
 *   <li>{@link camel.constraint.IfThenConstraint#getThen <em>Then</em>}</li>
 *   <li>{@link camel.constraint.IfThenConstraint#getElse <em>Else</em>}</li>
 * </ul>
 *
 * @see camel.constraint.ConstraintPackage#getIfThenConstraint()
 * @model
 * @generated
 */
public interface IfThenConstraint extends CompositeConstraint {
	/**
	 * Returns the value of the '<em><b>If</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>If</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>If</em>' containment reference.
	 * @see #setIf(Constraint)
	 * @see camel.constraint.ConstraintPackage#getIfThenConstraint_If()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Constraint getIf();

	/**
	 * Sets the value of the '{@link camel.constraint.IfThenConstraint#getIf <em>If</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>If</em>' containment reference.
	 * @see #getIf()
	 * @generated
	 */
	void setIf(Constraint value);

	/**
	 * Returns the value of the '<em><b>Then</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Then</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Then</em>' containment reference.
	 * @see #setThen(Constraint)
	 * @see camel.constraint.ConstraintPackage#getIfThenConstraint_Then()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Constraint getThen();

	/**
	 * Sets the value of the '{@link camel.constraint.IfThenConstraint#getThen <em>Then</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Then</em>' containment reference.
	 * @see #getThen()
	 * @generated
	 */
	void setThen(Constraint value);

	/**
	 * Returns the value of the '<em><b>Else</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Else</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Else</em>' containment reference.
	 * @see #setElse(Constraint)
	 * @see camel.constraint.ConstraintPackage#getIfThenConstraint_Else()
	 * @model containment="true"
	 * @generated
	 */
	Constraint getElse();

	/**
	 * Sets the value of the '{@link camel.constraint.IfThenConstraint#getElse <em>Else</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Else</em>' containment reference.
	 * @see #getElse()
	 * @generated
	 */
	void setElse(Constraint value);

} // IfThenConstraint
