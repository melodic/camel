/**
 */
package camel.constraint;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.ConstraintContext#getQuantifier <em>Quantifier</em>}</li>
 *   <li>{@link camel.constraint.ConstraintContext#getMinQuantity <em>Min Quantity</em>}</li>
 *   <li>{@link camel.constraint.ConstraintContext#getMaxQuantity <em>Max Quantity</em>}</li>
 *   <li>{@link camel.constraint.ConstraintContext#isIsRelative <em>Is Relative</em>}</li>
 * </ul>
 *
 * @see camel.constraint.ConstraintPackage#getConstraintContext()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='context_right_params'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot context_right_params='Tuple {\n\tmessage : String = \'Context: \' + self.name + \' has wrong value combinations for the quantifier and quantity attributes. When quantifier equals to SOME, then we have the two following cases: (a) relative values: minQuantity should be greater than 0.0 and maxQuantity less or equal to 1.0 and minQuantity less or equal to maxQuantity; (b) absolute values: minQuantity should be greater or equal to 1 and maxQuantity either -1 (inf) or greater or equal to minQuantity and both quantities should be integer\',\n\tstatus : Boolean = \n\t\t\t\tasError(((self.quantifier\n\t\t\t\t\t= QuantifierType::SOME and self.isRelative = true) implies (minQuantity > 0.0 and maxQuantity <= 1.0 and\n\t\t\t\t\tmaxQuantity >= minQuantity)) and ((self.quantifier = QuantifierType::SOME and self.isRelative = false) implies\n\t\t\t\t\t(minQuantity >= 1.0 and ((maxQuantity >= 1.0 and maxQuantity >= minQuantity) or maxQuantity = - 1.0)) and\n\t\t\t\t\t(minQuantity / minQuantity.round()) = 1 and (maxQuantity / maxQuantity.round()) = 1))\n}.status'"
 * @generated
 */
public interface ConstraintContext extends Feature {
	/**
	 * Returns the value of the '<em><b>Quantifier</b></em>' attribute.
	 * The default value is <code>"ANY"</code>.
	 * The literals are from the enumeration {@link camel.constraint.QuantifierType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Quantifier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Quantifier</em>' attribute.
	 * @see camel.constraint.QuantifierType
	 * @see #setQuantifier(QuantifierType)
	 * @see camel.constraint.ConstraintPackage#getConstraintContext_Quantifier()
	 * @model default="ANY" required="true"
	 * @generated
	 */
	QuantifierType getQuantifier();

	/**
	 * Sets the value of the '{@link camel.constraint.ConstraintContext#getQuantifier <em>Quantifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Quantifier</em>' attribute.
	 * @see camel.constraint.QuantifierType
	 * @see #getQuantifier()
	 * @generated
	 */
	void setQuantifier(QuantifierType value);

	/**
	 * Returns the value of the '<em><b>Min Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Quantity</em>' attribute.
	 * @see #setMinQuantity(double)
	 * @see camel.constraint.ConstraintPackage#getConstraintContext_MinQuantity()
	 * @model required="true"
	 * @generated
	 */
	double getMinQuantity();

	/**
	 * Sets the value of the '{@link camel.constraint.ConstraintContext#getMinQuantity <em>Min Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Quantity</em>' attribute.
	 * @see #getMinQuantity()
	 * @generated
	 */
	void setMinQuantity(double value);

	/**
	 * Returns the value of the '<em><b>Max Quantity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Quantity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Quantity</em>' attribute.
	 * @see #setMaxQuantity(double)
	 * @see camel.constraint.ConstraintPackage#getConstraintContext_MaxQuantity()
	 * @model required="true"
	 * @generated
	 */
	double getMaxQuantity();

	/**
	 * Sets the value of the '{@link camel.constraint.ConstraintContext#getMaxQuantity <em>Max Quantity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Quantity</em>' attribute.
	 * @see #getMaxQuantity()
	 * @generated
	 */
	void setMaxQuantity(double value);

	/**
	 * Returns the value of the '<em><b>Is Relative</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Relative</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Relative</em>' attribute.
	 * @see #setIsRelative(boolean)
	 * @see camel.constraint.ConstraintPackage#getConstraintContext_IsRelative()
	 * @model required="true"
	 * @generated
	 */
	boolean isIsRelative();

	/**
	 * Sets the value of the '{@link camel.constraint.ConstraintContext#isIsRelative <em>Is Relative</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Relative</em>' attribute.
	 * @see #isIsRelative()
	 * @generated
	 */
	void setIsRelative(boolean value);

} // ConstraintContext
