/**
 */
package camel.constraint.util;

import camel.constraint.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.constraint.ConstraintPackage
 * @generated
 */
public class ConstraintValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final ConstraintValidator INSTANCE = new ConstraintValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.constraint";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstraintValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return ConstraintPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case ConstraintPackage.CONSTRAINT_MODEL:
				return validateConstraintModel((ConstraintModel)value, diagnostics, context);
			case ConstraintPackage.CONSTRAINT:
				return validateConstraint((Constraint)value, diagnostics, context);
			case ConstraintPackage.UNARY_CONSTRAINT:
				return validateUnaryConstraint((UnaryConstraint)value, diagnostics, context);
			case ConstraintPackage.METRIC_CONSTRAINT:
				return validateMetricConstraint((MetricConstraint)value, diagnostics, context);
			case ConstraintPackage.ATTRIBUTE_CONSTRAINT:
				return validateAttributeConstraint((AttributeConstraint)value, diagnostics, context);
			case ConstraintPackage.COMPOSITE_CONSTRAINT:
				return validateCompositeConstraint((CompositeConstraint)value, diagnostics, context);
			case ConstraintPackage.IF_THEN_CONSTRAINT:
				return validateIfThenConstraint((IfThenConstraint)value, diagnostics, context);
			case ConstraintPackage.METRIC_VARIABLE_CONSTRAINT:
				return validateMetricVariableConstraint((MetricVariableConstraint)value, diagnostics, context);
			case ConstraintPackage.LOGICAL_CONSTRAINT:
				return validateLogicalConstraint((LogicalConstraint)value, diagnostics, context);
			case ConstraintPackage.COMPARISON_OPERATOR_TYPE:
				return validateComparisonOperatorType((ComparisonOperatorType)value, diagnostics, context);
			case ConstraintPackage.LOGICAL_OPERATOR_TYPE:
				return validateLogicalOperatorType((LogicalOperatorType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConstraintModel(ConstraintModel constraintModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)constraintModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConstraint(Constraint constraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)constraint, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnaryConstraint(UnaryConstraint unaryConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)unaryConstraint, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricConstraint(MetricConstraint metricConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)metricConstraint, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAttributeConstraint(AttributeConstraint attributeConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)attributeConstraint, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeConstraint(CompositeConstraint compositeConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)compositeConstraint, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateIfThenConstraint(IfThenConstraint ifThenConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)ifThenConstraint, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricVariableConstraint(MetricVariableConstraint metricVariableConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)metricVariableConstraint, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLogicalConstraint(LogicalConstraint logicalConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)logicalConstraint, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)logicalConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)logicalConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)logicalConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)logicalConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)logicalConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)logicalConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)logicalConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)logicalConstraint, diagnostics, context);
		if (result || diagnostics != null) result &= validateLogicalConstraint_logical_constraint_not_recursiv_itself(logicalConstraint, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the logical_constraint_not_recursiv_itself constraint of '<em>Logical Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String LOGICAL_CONSTRAINT__LOGICAL_CONSTRAINT_NOT_RECURSIV_ITSELF__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'LogicalConstraint: ' + self.name + ' cannot contain recursively itself'\n" +
		"\t\t\t,\n" +
		"\tstatus : Boolean = asError(not self.containsConstraint(self))\n" +
		"}.status";

	/**
	 * Validates the logical_constraint_not_recursiv_itself constraint of '<em>Logical Constraint</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLogicalConstraint_logical_constraint_not_recursiv_itself(LogicalConstraint logicalConstraint, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ConstraintPackage.Literals.LOGICAL_CONSTRAINT,
				 (EObject)logicalConstraint,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "logical_constraint_not_recursiv_itself",
				 LOGICAL_CONSTRAINT__LOGICAL_CONSTRAINT_NOT_RECURSIV_ITSELF__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComparisonOperatorType(ComparisonOperatorType comparisonOperatorType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLogicalOperatorType(LogicalOperatorType logicalOperatorType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //ConstraintValidator
