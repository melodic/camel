/**
 */
package camel.constraint;

import camel.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see camel.constraint.ConstraintFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface ConstraintPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "constraint";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.camel-dsl.org/camel/constraint";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "constraint";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ConstraintPackage eINSTANCE = camel.constraint.impl.ConstraintPackageImpl.init();

	/**
	 * The meta object id for the '{@link camel.constraint.impl.ConstraintModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.constraint.impl.ConstraintModelImpl
	 * @see camel.constraint.impl.ConstraintPackageImpl#getConstraintModel()
	 * @generated
	 */
	int CONSTRAINT_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_MODEL__NAME = CorePackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_MODEL__DESCRIPTION = CorePackage.MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_MODEL__ANNOTATIONS = CorePackage.MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_MODEL__ATTRIBUTES = CorePackage.MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_MODEL__SUB_FEATURES = CorePackage.MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_MODEL__IMPORT_URI = CorePackage.MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_MODEL__CONSTRAINTS = CorePackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_MODEL_FEATURE_COUNT = CorePackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_MODEL___AS_ERROR__BOOLEAN = CorePackage.MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_MODEL_OPERATION_COUNT = CorePackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.constraint.impl.ConstraintImpl <em>Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.constraint.impl.ConstraintImpl
	 * @see camel.constraint.impl.ConstraintPackageImpl#getConstraint()
	 * @generated
	 */
	int CONSTRAINT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__NAME = CorePackage.NAMED_ELEMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__DESCRIPTION = CorePackage.NAMED_ELEMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT__ANNOTATIONS = CorePackage.NAMED_ELEMENT__ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_FEATURE_COUNT = CorePackage.NAMED_ELEMENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT___AS_ERROR__BOOLEAN = CorePackage.NAMED_ELEMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTRAINT_OPERATION_COUNT = CorePackage.NAMED_ELEMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.constraint.impl.UnaryConstraintImpl <em>Unary Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.constraint.impl.UnaryConstraintImpl
	 * @see camel.constraint.impl.ConstraintPackageImpl#getUnaryConstraint()
	 * @generated
	 */
	int UNARY_CONSTRAINT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT__NAME = CONSTRAINT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT__DESCRIPTION = CONSTRAINT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT__ANNOTATIONS = CONSTRAINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT__VALIDITY = CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Comparison Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT__COMPARISON_OPERATOR = CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT__THRESHOLD = CONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Unary Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT_FEATURE_COUNT = CONSTRAINT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT___AS_ERROR__BOOLEAN = CONSTRAINT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Unary Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_CONSTRAINT_OPERATION_COUNT = CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.constraint.impl.MetricConstraintImpl <em>Metric Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.constraint.impl.MetricConstraintImpl
	 * @see camel.constraint.impl.ConstraintPackageImpl#getMetricConstraint()
	 * @generated
	 */
	int METRIC_CONSTRAINT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONSTRAINT__NAME = UNARY_CONSTRAINT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONSTRAINT__DESCRIPTION = UNARY_CONSTRAINT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONSTRAINT__ANNOTATIONS = UNARY_CONSTRAINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONSTRAINT__VALIDITY = UNARY_CONSTRAINT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Comparison Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONSTRAINT__COMPARISON_OPERATOR = UNARY_CONSTRAINT__COMPARISON_OPERATOR;

	/**
	 * The feature id for the '<em><b>Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONSTRAINT__THRESHOLD = UNARY_CONSTRAINT__THRESHOLD;

	/**
	 * The feature id for the '<em><b>Metric Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONSTRAINT__METRIC_CONTEXT = UNARY_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Metric Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONSTRAINT_FEATURE_COUNT = UNARY_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONSTRAINT___AS_ERROR__BOOLEAN = UNARY_CONSTRAINT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Metric Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONSTRAINT_OPERATION_COUNT = UNARY_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.constraint.impl.AttributeConstraintImpl <em>Attribute Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.constraint.impl.AttributeConstraintImpl
	 * @see camel.constraint.impl.ConstraintPackageImpl#getAttributeConstraint()
	 * @generated
	 */
	int ATTRIBUTE_CONSTRAINT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONSTRAINT__NAME = UNARY_CONSTRAINT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONSTRAINT__DESCRIPTION = UNARY_CONSTRAINT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONSTRAINT__ANNOTATIONS = UNARY_CONSTRAINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONSTRAINT__VALIDITY = UNARY_CONSTRAINT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Comparison Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONSTRAINT__COMPARISON_OPERATOR = UNARY_CONSTRAINT__COMPARISON_OPERATOR;

	/**
	 * The feature id for the '<em><b>Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONSTRAINT__THRESHOLD = UNARY_CONSTRAINT__THRESHOLD;

	/**
	 * The feature id for the '<em><b>Attribute Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONSTRAINT__ATTRIBUTE_CONTEXT = UNARY_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attribute Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONSTRAINT_FEATURE_COUNT = UNARY_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONSTRAINT___AS_ERROR__BOOLEAN = UNARY_CONSTRAINT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Attribute Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONSTRAINT_OPERATION_COUNT = UNARY_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.constraint.impl.CompositeConstraintImpl <em>Composite Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.constraint.impl.CompositeConstraintImpl
	 * @see camel.constraint.impl.ConstraintPackageImpl#getCompositeConstraint()
	 * @generated
	 */
	int COMPOSITE_CONSTRAINT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_CONSTRAINT__NAME = CONSTRAINT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_CONSTRAINT__DESCRIPTION = CONSTRAINT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_CONSTRAINT__ANNOTATIONS = CONSTRAINT__ANNOTATIONS;

	/**
	 * The number of structural features of the '<em>Composite Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_CONSTRAINT_FEATURE_COUNT = CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_CONSTRAINT___AS_ERROR__BOOLEAN = CONSTRAINT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Composite Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_CONSTRAINT_OPERATION_COUNT = CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.constraint.impl.IfThenConstraintImpl <em>If Then Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.constraint.impl.IfThenConstraintImpl
	 * @see camel.constraint.impl.ConstraintPackageImpl#getIfThenConstraint()
	 * @generated
	 */
	int IF_THEN_CONSTRAINT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_CONSTRAINT__NAME = COMPOSITE_CONSTRAINT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_CONSTRAINT__DESCRIPTION = COMPOSITE_CONSTRAINT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_CONSTRAINT__ANNOTATIONS = COMPOSITE_CONSTRAINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>If</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_CONSTRAINT__IF = COMPOSITE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Then</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_CONSTRAINT__THEN = COMPOSITE_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Else</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_CONSTRAINT__ELSE = COMPOSITE_CONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>If Then Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_CONSTRAINT_FEATURE_COUNT = COMPOSITE_CONSTRAINT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_CONSTRAINT___AS_ERROR__BOOLEAN = COMPOSITE_CONSTRAINT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>If Then Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IF_THEN_CONSTRAINT_OPERATION_COUNT = COMPOSITE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.constraint.impl.MetricVariableConstraintImpl <em>Metric Variable Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.constraint.impl.MetricVariableConstraintImpl
	 * @see camel.constraint.impl.ConstraintPackageImpl#getMetricVariableConstraint()
	 * @generated
	 */
	int METRIC_VARIABLE_CONSTRAINT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_CONSTRAINT__NAME = UNARY_CONSTRAINT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_CONSTRAINT__DESCRIPTION = UNARY_CONSTRAINT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_CONSTRAINT__ANNOTATIONS = UNARY_CONSTRAINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Validity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_CONSTRAINT__VALIDITY = UNARY_CONSTRAINT__VALIDITY;

	/**
	 * The feature id for the '<em><b>Comparison Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_CONSTRAINT__COMPARISON_OPERATOR = UNARY_CONSTRAINT__COMPARISON_OPERATOR;

	/**
	 * The feature id for the '<em><b>Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_CONSTRAINT__THRESHOLD = UNARY_CONSTRAINT__THRESHOLD;

	/**
	 * The feature id for the '<em><b>Metric Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_CONSTRAINT__METRIC_VARIABLE = UNARY_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Metric Variable Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_CONSTRAINT_FEATURE_COUNT = UNARY_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_CONSTRAINT___AS_ERROR__BOOLEAN = UNARY_CONSTRAINT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Metric Variable Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_CONSTRAINT_OPERATION_COUNT = UNARY_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.constraint.impl.LogicalConstraintImpl <em>Logical Constraint</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.constraint.impl.LogicalConstraintImpl
	 * @see camel.constraint.impl.ConstraintPackageImpl#getLogicalConstraint()
	 * @generated
	 */
	int LOGICAL_CONSTRAINT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT__NAME = COMPOSITE_CONSTRAINT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT__DESCRIPTION = COMPOSITE_CONSTRAINT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT__ANNOTATIONS = COMPOSITE_CONSTRAINT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Constraints</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT__CONSTRAINTS = COMPOSITE_CONSTRAINT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Logical Operator</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT__LOGICAL_OPERATOR = COMPOSITE_CONSTRAINT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Logical Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT_FEATURE_COUNT = COMPOSITE_CONSTRAINT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT___AS_ERROR__BOOLEAN = COMPOSITE_CONSTRAINT___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Contains Constraint</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT___CONTAINS_CONSTRAINT__LOGICALCONSTRAINT = COMPOSITE_CONSTRAINT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Logical Constraint</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOGICAL_CONSTRAINT_OPERATION_COUNT = COMPOSITE_CONSTRAINT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link camel.constraint.ComparisonOperatorType <em>Comparison Operator Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.constraint.ComparisonOperatorType
	 * @see camel.constraint.impl.ConstraintPackageImpl#getComparisonOperatorType()
	 * @generated
	 */
	int COMPARISON_OPERATOR_TYPE = 9;

	/**
	 * The meta object id for the '{@link camel.constraint.LogicalOperatorType <em>Logical Operator Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.constraint.LogicalOperatorType
	 * @see camel.constraint.impl.ConstraintPackageImpl#getLogicalOperatorType()
	 * @generated
	 */
	int LOGICAL_OPERATOR_TYPE = 10;


	/**
	 * Returns the meta object for class '{@link camel.constraint.ConstraintModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see camel.constraint.ConstraintModel
	 * @generated
	 */
	EClass getConstraintModel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.constraint.ConstraintModel#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Constraints</em>'.
	 * @see camel.constraint.ConstraintModel#getConstraints()
	 * @see #getConstraintModel()
	 * @generated
	 */
	EReference getConstraintModel_Constraints();

	/**
	 * Returns the meta object for class '{@link camel.constraint.Constraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constraint</em>'.
	 * @see camel.constraint.Constraint
	 * @generated
	 */
	EClass getConstraint();

	/**
	 * Returns the meta object for class '{@link camel.constraint.UnaryConstraint <em>Unary Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Constraint</em>'.
	 * @see camel.constraint.UnaryConstraint
	 * @generated
	 */
	EClass getUnaryConstraint();

	/**
	 * Returns the meta object for the attribute '{@link camel.constraint.UnaryConstraint#getValidity <em>Validity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Validity</em>'.
	 * @see camel.constraint.UnaryConstraint#getValidity()
	 * @see #getUnaryConstraint()
	 * @generated
	 */
	EAttribute getUnaryConstraint_Validity();

	/**
	 * Returns the meta object for the attribute '{@link camel.constraint.UnaryConstraint#getComparisonOperator <em>Comparison Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Comparison Operator</em>'.
	 * @see camel.constraint.UnaryConstraint#getComparisonOperator()
	 * @see #getUnaryConstraint()
	 * @generated
	 */
	EAttribute getUnaryConstraint_ComparisonOperator();

	/**
	 * Returns the meta object for the attribute '{@link camel.constraint.UnaryConstraint#getThreshold <em>Threshold</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Threshold</em>'.
	 * @see camel.constraint.UnaryConstraint#getThreshold()
	 * @see #getUnaryConstraint()
	 * @generated
	 */
	EAttribute getUnaryConstraint_Threshold();

	/**
	 * Returns the meta object for class '{@link camel.constraint.MetricConstraint <em>Metric Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Metric Constraint</em>'.
	 * @see camel.constraint.MetricConstraint
	 * @generated
	 */
	EClass getMetricConstraint();

	/**
	 * Returns the meta object for the reference '{@link camel.constraint.MetricConstraint#getMetricContext <em>Metric Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metric Context</em>'.
	 * @see camel.constraint.MetricConstraint#getMetricContext()
	 * @see #getMetricConstraint()
	 * @generated
	 */
	EReference getMetricConstraint_MetricContext();

	/**
	 * Returns the meta object for class '{@link camel.constraint.AttributeConstraint <em>Attribute Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Constraint</em>'.
	 * @see camel.constraint.AttributeConstraint
	 * @generated
	 */
	EClass getAttributeConstraint();

	/**
	 * Returns the meta object for the reference '{@link camel.constraint.AttributeConstraint#getAttributeContext <em>Attribute Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute Context</em>'.
	 * @see camel.constraint.AttributeConstraint#getAttributeContext()
	 * @see #getAttributeConstraint()
	 * @generated
	 */
	EReference getAttributeConstraint_AttributeContext();

	/**
	 * Returns the meta object for class '{@link camel.constraint.CompositeConstraint <em>Composite Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Constraint</em>'.
	 * @see camel.constraint.CompositeConstraint
	 * @generated
	 */
	EClass getCompositeConstraint();

	/**
	 * Returns the meta object for class '{@link camel.constraint.IfThenConstraint <em>If Then Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>If Then Constraint</em>'.
	 * @see camel.constraint.IfThenConstraint
	 * @generated
	 */
	EClass getIfThenConstraint();

	/**
	 * Returns the meta object for the containment reference '{@link camel.constraint.IfThenConstraint#getIf <em>If</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>If</em>'.
	 * @see camel.constraint.IfThenConstraint#getIf()
	 * @see #getIfThenConstraint()
	 * @generated
	 */
	EReference getIfThenConstraint_If();

	/**
	 * Returns the meta object for the containment reference '{@link camel.constraint.IfThenConstraint#getThen <em>Then</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Then</em>'.
	 * @see camel.constraint.IfThenConstraint#getThen()
	 * @see #getIfThenConstraint()
	 * @generated
	 */
	EReference getIfThenConstraint_Then();

	/**
	 * Returns the meta object for the containment reference '{@link camel.constraint.IfThenConstraint#getElse <em>Else</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Else</em>'.
	 * @see camel.constraint.IfThenConstraint#getElse()
	 * @see #getIfThenConstraint()
	 * @generated
	 */
	EReference getIfThenConstraint_Else();

	/**
	 * Returns the meta object for class '{@link camel.constraint.MetricVariableConstraint <em>Metric Variable Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Metric Variable Constraint</em>'.
	 * @see camel.constraint.MetricVariableConstraint
	 * @generated
	 */
	EClass getMetricVariableConstraint();

	/**
	 * Returns the meta object for the reference '{@link camel.constraint.MetricVariableConstraint#getMetricVariable <em>Metric Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metric Variable</em>'.
	 * @see camel.constraint.MetricVariableConstraint#getMetricVariable()
	 * @see #getMetricVariableConstraint()
	 * @generated
	 */
	EReference getMetricVariableConstraint_MetricVariable();

	/**
	 * Returns the meta object for class '{@link camel.constraint.LogicalConstraint <em>Logical Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Logical Constraint</em>'.
	 * @see camel.constraint.LogicalConstraint
	 * @generated
	 */
	EClass getLogicalConstraint();

	/**
	 * Returns the meta object for the reference list '{@link camel.constraint.LogicalConstraint#getConstraints <em>Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Constraints</em>'.
	 * @see camel.constraint.LogicalConstraint#getConstraints()
	 * @see #getLogicalConstraint()
	 * @generated
	 */
	EReference getLogicalConstraint_Constraints();

	/**
	 * Returns the meta object for the attribute '{@link camel.constraint.LogicalConstraint#getLogicalOperator <em>Logical Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Logical Operator</em>'.
	 * @see camel.constraint.LogicalConstraint#getLogicalOperator()
	 * @see #getLogicalConstraint()
	 * @generated
	 */
	EAttribute getLogicalConstraint_LogicalOperator();

	/**
	 * Returns the meta object for the '{@link camel.constraint.LogicalConstraint#containsConstraint(camel.constraint.LogicalConstraint) <em>Contains Constraint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Contains Constraint</em>' operation.
	 * @see camel.constraint.LogicalConstraint#containsConstraint(camel.constraint.LogicalConstraint)
	 * @generated
	 */
	EOperation getLogicalConstraint__ContainsConstraint__LogicalConstraint();

	/**
	 * Returns the meta object for enum '{@link camel.constraint.ComparisonOperatorType <em>Comparison Operator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Comparison Operator Type</em>'.
	 * @see camel.constraint.ComparisonOperatorType
	 * @generated
	 */
	EEnum getComparisonOperatorType();

	/**
	 * Returns the meta object for enum '{@link camel.constraint.LogicalOperatorType <em>Logical Operator Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Logical Operator Type</em>'.
	 * @see camel.constraint.LogicalOperatorType
	 * @generated
	 */
	EEnum getLogicalOperatorType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ConstraintFactory getConstraintFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link camel.constraint.impl.ConstraintModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.constraint.impl.ConstraintModelImpl
		 * @see camel.constraint.impl.ConstraintPackageImpl#getConstraintModel()
		 * @generated
		 */
		EClass CONSTRAINT_MODEL = eINSTANCE.getConstraintModel();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONSTRAINT_MODEL__CONSTRAINTS = eINSTANCE.getConstraintModel_Constraints();

		/**
		 * The meta object literal for the '{@link camel.constraint.impl.ConstraintImpl <em>Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.constraint.impl.ConstraintImpl
		 * @see camel.constraint.impl.ConstraintPackageImpl#getConstraint()
		 * @generated
		 */
		EClass CONSTRAINT = eINSTANCE.getConstraint();

		/**
		 * The meta object literal for the '{@link camel.constraint.impl.UnaryConstraintImpl <em>Unary Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.constraint.impl.UnaryConstraintImpl
		 * @see camel.constraint.impl.ConstraintPackageImpl#getUnaryConstraint()
		 * @generated
		 */
		EClass UNARY_CONSTRAINT = eINSTANCE.getUnaryConstraint();

		/**
		 * The meta object literal for the '<em><b>Validity</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_CONSTRAINT__VALIDITY = eINSTANCE.getUnaryConstraint_Validity();

		/**
		 * The meta object literal for the '<em><b>Comparison Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_CONSTRAINT__COMPARISON_OPERATOR = eINSTANCE.getUnaryConstraint_ComparisonOperator();

		/**
		 * The meta object literal for the '<em><b>Threshold</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute UNARY_CONSTRAINT__THRESHOLD = eINSTANCE.getUnaryConstraint_Threshold();

		/**
		 * The meta object literal for the '{@link camel.constraint.impl.MetricConstraintImpl <em>Metric Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.constraint.impl.MetricConstraintImpl
		 * @see camel.constraint.impl.ConstraintPackageImpl#getMetricConstraint()
		 * @generated
		 */
		EClass METRIC_CONSTRAINT = eINSTANCE.getMetricConstraint();

		/**
		 * The meta object literal for the '<em><b>Metric Context</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_CONSTRAINT__METRIC_CONTEXT = eINSTANCE.getMetricConstraint_MetricContext();

		/**
		 * The meta object literal for the '{@link camel.constraint.impl.AttributeConstraintImpl <em>Attribute Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.constraint.impl.AttributeConstraintImpl
		 * @see camel.constraint.impl.ConstraintPackageImpl#getAttributeConstraint()
		 * @generated
		 */
		EClass ATTRIBUTE_CONSTRAINT = eINSTANCE.getAttributeConstraint();

		/**
		 * The meta object literal for the '<em><b>Attribute Context</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_CONSTRAINT__ATTRIBUTE_CONTEXT = eINSTANCE.getAttributeConstraint_AttributeContext();

		/**
		 * The meta object literal for the '{@link camel.constraint.impl.CompositeConstraintImpl <em>Composite Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.constraint.impl.CompositeConstraintImpl
		 * @see camel.constraint.impl.ConstraintPackageImpl#getCompositeConstraint()
		 * @generated
		 */
		EClass COMPOSITE_CONSTRAINT = eINSTANCE.getCompositeConstraint();

		/**
		 * The meta object literal for the '{@link camel.constraint.impl.IfThenConstraintImpl <em>If Then Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.constraint.impl.IfThenConstraintImpl
		 * @see camel.constraint.impl.ConstraintPackageImpl#getIfThenConstraint()
		 * @generated
		 */
		EClass IF_THEN_CONSTRAINT = eINSTANCE.getIfThenConstraint();

		/**
		 * The meta object literal for the '<em><b>If</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_THEN_CONSTRAINT__IF = eINSTANCE.getIfThenConstraint_If();

		/**
		 * The meta object literal for the '<em><b>Then</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_THEN_CONSTRAINT__THEN = eINSTANCE.getIfThenConstraint_Then();

		/**
		 * The meta object literal for the '<em><b>Else</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference IF_THEN_CONSTRAINT__ELSE = eINSTANCE.getIfThenConstraint_Else();

		/**
		 * The meta object literal for the '{@link camel.constraint.impl.MetricVariableConstraintImpl <em>Metric Variable Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.constraint.impl.MetricVariableConstraintImpl
		 * @see camel.constraint.impl.ConstraintPackageImpl#getMetricVariableConstraint()
		 * @generated
		 */
		EClass METRIC_VARIABLE_CONSTRAINT = eINSTANCE.getMetricVariableConstraint();

		/**
		 * The meta object literal for the '<em><b>Metric Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_VARIABLE_CONSTRAINT__METRIC_VARIABLE = eINSTANCE.getMetricVariableConstraint_MetricVariable();

		/**
		 * The meta object literal for the '{@link camel.constraint.impl.LogicalConstraintImpl <em>Logical Constraint</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.constraint.impl.LogicalConstraintImpl
		 * @see camel.constraint.impl.ConstraintPackageImpl#getLogicalConstraint()
		 * @generated
		 */
		EClass LOGICAL_CONSTRAINT = eINSTANCE.getLogicalConstraint();

		/**
		 * The meta object literal for the '<em><b>Constraints</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOGICAL_CONSTRAINT__CONSTRAINTS = eINSTANCE.getLogicalConstraint_Constraints();

		/**
		 * The meta object literal for the '<em><b>Logical Operator</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOGICAL_CONSTRAINT__LOGICAL_OPERATOR = eINSTANCE.getLogicalConstraint_LogicalOperator();

		/**
		 * The meta object literal for the '<em><b>Contains Constraint</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation LOGICAL_CONSTRAINT___CONTAINS_CONSTRAINT__LOGICALCONSTRAINT = eINSTANCE.getLogicalConstraint__ContainsConstraint__LogicalConstraint();

		/**
		 * The meta object literal for the '{@link camel.constraint.ComparisonOperatorType <em>Comparison Operator Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.constraint.ComparisonOperatorType
		 * @see camel.constraint.impl.ConstraintPackageImpl#getComparisonOperatorType()
		 * @generated
		 */
		EEnum COMPARISON_OPERATOR_TYPE = eINSTANCE.getComparisonOperatorType();

		/**
		 * The meta object literal for the '{@link camel.constraint.LogicalOperatorType <em>Logical Operator Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.constraint.LogicalOperatorType
		 * @see camel.constraint.impl.ConstraintPackageImpl#getLogicalOperatorType()
		 * @generated
		 */
		EEnum LOGICAL_OPERATOR_TYPE = eINSTANCE.getLogicalOperatorType();

	}

} //ConstraintPackage
