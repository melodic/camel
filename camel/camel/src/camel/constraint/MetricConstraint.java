/**
 */
package camel.constraint;

import camel.metric.MetricContext;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Metric Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.MetricConstraint#getMetricContext <em>Metric Context</em>}</li>
 * </ul>
 *
 * @see camel.constraint.ConstraintPackage#getMetricConstraint()
 * @model
 * @generated
 */
public interface MetricConstraint extends UnaryConstraint {
	/**
	 * Returns the value of the '<em><b>Metric Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Context</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Context</em>' reference.
	 * @see #setMetricContext(MetricContext)
	 * @see camel.constraint.ConstraintPackage#getMetricConstraint_MetricContext()
	 * @model required="true"
	 * @generated
	 */
	MetricContext getMetricContext();

	/**
	 * Sets the value of the '{@link camel.constraint.MetricConstraint#getMetricContext <em>Metric Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metric Context</em>' reference.
	 * @see #getMetricContext()
	 * @generated
	 */
	void setMetricContext(MetricContext value);

} // MetricConstraint
