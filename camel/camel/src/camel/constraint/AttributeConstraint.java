/**
 */
package camel.constraint;

import camel.metric.AttributeContext;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.AttributeConstraint#getAttributeContext <em>Attribute Context</em>}</li>
 * </ul>
 *
 * @see camel.constraint.ConstraintPackage#getAttributeConstraint()
 * @model abstract="true"
 * @generated
 */
public interface AttributeConstraint extends UnaryConstraint {
	/**
	 * Returns the value of the '<em><b>Attribute Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Context</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Context</em>' reference.
	 * @see #setAttributeContext(AttributeContext)
	 * @see camel.constraint.ConstraintPackage#getAttributeConstraint_AttributeContext()
	 * @model required="true"
	 * @generated
	 */
	AttributeContext getAttributeContext();

	/**
	 * Sets the value of the '{@link camel.constraint.AttributeConstraint#getAttributeContext <em>Attribute Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute Context</em>' reference.
	 * @see #getAttributeContext()
	 * @generated
	 */
	void setAttributeContext(AttributeContext value);

} // AttributeConstraint
