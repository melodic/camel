/**
 */
package camel.constraint;

import camel.core.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.ConstraintModel#getConstraints <em>Constraints</em>}</li>
 * </ul>
 *
 * @see camel.constraint.ConstraintPackage#getConstraintModel()
 * @model
 * @generated
 */
public interface ConstraintModel extends Model {
	/**
	 * Returns the value of the '<em><b>Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link camel.constraint.Constraint}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraints</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraints</em>' containment reference list.
	 * @see camel.constraint.ConstraintPackage#getConstraintModel_Constraints()
	 * @model containment="true"
	 * @generated
	 */
	EList<Constraint> getConstraints();

} // ConstraintModel
