/**
 */
package camel.constraint;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.UnaryConstraint#getValidity <em>Validity</em>}</li>
 *   <li>{@link camel.constraint.UnaryConstraint#getComparisonOperator <em>Comparison Operator</em>}</li>
 *   <li>{@link camel.constraint.UnaryConstraint#getThreshold <em>Threshold</em>}</li>
 * </ul>
 *
 * @see camel.constraint.ConstraintPackage#getUnaryConstraint()
 * @model abstract="true"
 * @generated
 */
public interface UnaryConstraint extends Constraint {
	/**
	 * Returns the value of the '<em><b>Validity</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Validity</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Validity</em>' attribute.
	 * @see #setValidity(Date)
	 * @see camel.constraint.ConstraintPackage#getUnaryConstraint_Validity()
	 * @model
	 * @generated
	 */
	Date getValidity();

	/**
	 * Sets the value of the '{@link camel.constraint.UnaryConstraint#getValidity <em>Validity</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Validity</em>' attribute.
	 * @see #getValidity()
	 * @generated
	 */
	void setValidity(Date value);

	/**
	 * Returns the value of the '<em><b>Comparison Operator</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.constraint.ComparisonOperatorType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Comparison Operator</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Comparison Operator</em>' attribute.
	 * @see camel.constraint.ComparisonOperatorType
	 * @see #setComparisonOperator(ComparisonOperatorType)
	 * @see camel.constraint.ConstraintPackage#getUnaryConstraint_ComparisonOperator()
	 * @model required="true"
	 * @generated
	 */
	ComparisonOperatorType getComparisonOperator();

	/**
	 * Sets the value of the '{@link camel.constraint.UnaryConstraint#getComparisonOperator <em>Comparison Operator</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Comparison Operator</em>' attribute.
	 * @see camel.constraint.ComparisonOperatorType
	 * @see #getComparisonOperator()
	 * @generated
	 */
	void setComparisonOperator(ComparisonOperatorType value);

	/**
	 * Returns the value of the '<em><b>Threshold</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Threshold</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Threshold</em>' attribute.
	 * @see #setThreshold(double)
	 * @see camel.constraint.ConstraintPackage#getUnaryConstraint_Threshold()
	 * @model required="true"
	 * @generated
	 */
	double getThreshold();

	/**
	 * Sets the value of the '{@link camel.constraint.UnaryConstraint#getThreshold <em>Threshold</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Threshold</em>' attribute.
	 * @see #getThreshold()
	 * @generated
	 */
	void setThreshold(double value);

} // UnaryConstraint
