/**
 */
package camel.constraint.impl;

import camel.constraint.ConstraintPackage;
import camel.constraint.MetricConstraint;

import camel.metric.MetricContext;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Metric Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.impl.MetricConstraintImpl#getMetricContext <em>Metric Context</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetricConstraintImpl extends UnaryConstraintImpl implements MetricConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintPackage.Literals.METRIC_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricContext getMetricContext() {
		return (MetricContext)eGet(ConstraintPackage.Literals.METRIC_CONSTRAINT__METRIC_CONTEXT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetricContext(MetricContext newMetricContext) {
		eSet(ConstraintPackage.Literals.METRIC_CONSTRAINT__METRIC_CONTEXT, newMetricContext);
	}

} //MetricConstraintImpl
