/**
 */
package camel.constraint.impl;

import camel.constraint.Constraint;
import camel.constraint.ConstraintPackage;
import camel.constraint.IfThenConstraint;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>If Then Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.impl.IfThenConstraintImpl#getIf <em>If</em>}</li>
 *   <li>{@link camel.constraint.impl.IfThenConstraintImpl#getThen <em>Then</em>}</li>
 *   <li>{@link camel.constraint.impl.IfThenConstraintImpl#getElse <em>Else</em>}</li>
 * </ul>
 *
 * @generated
 */
public class IfThenConstraintImpl extends CompositeConstraintImpl implements IfThenConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected IfThenConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintPackage.Literals.IF_THEN_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint getIf() {
		return (Constraint)eGet(ConstraintPackage.Literals.IF_THEN_CONSTRAINT__IF, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIf(Constraint newIf) {
		eSet(ConstraintPackage.Literals.IF_THEN_CONSTRAINT__IF, newIf);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint getThen() {
		return (Constraint)eGet(ConstraintPackage.Literals.IF_THEN_CONSTRAINT__THEN, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThen(Constraint newThen) {
		eSet(ConstraintPackage.Literals.IF_THEN_CONSTRAINT__THEN, newThen);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint getElse() {
		return (Constraint)eGet(ConstraintPackage.Literals.IF_THEN_CONSTRAINT__ELSE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setElse(Constraint newElse) {
		eSet(ConstraintPackage.Literals.IF_THEN_CONSTRAINT__ELSE, newElse);
	}

} //IfThenConstraintImpl
