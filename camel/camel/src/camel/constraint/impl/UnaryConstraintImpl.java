/**
 */
package camel.constraint.impl;

import camel.constraint.ComparisonOperatorType;
import camel.constraint.ConstraintPackage;
import camel.constraint.UnaryConstraint;
import java.util.Date;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unary Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.impl.UnaryConstraintImpl#getValidity <em>Validity</em>}</li>
 *   <li>{@link camel.constraint.impl.UnaryConstraintImpl#getComparisonOperator <em>Comparison Operator</em>}</li>
 *   <li>{@link camel.constraint.impl.UnaryConstraintImpl#getThreshold <em>Threshold</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class UnaryConstraintImpl extends ConstraintImpl implements UnaryConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnaryConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintPackage.Literals.UNARY_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getValidity() {
		return (Date)eGet(ConstraintPackage.Literals.UNARY_CONSTRAINT__VALIDITY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValidity(Date newValidity) {
		eSet(ConstraintPackage.Literals.UNARY_CONSTRAINT__VALIDITY, newValidity);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComparisonOperatorType getComparisonOperator() {
		return (ComparisonOperatorType)eGet(ConstraintPackage.Literals.UNARY_CONSTRAINT__COMPARISON_OPERATOR, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComparisonOperator(ComparisonOperatorType newComparisonOperator) {
		eSet(ConstraintPackage.Literals.UNARY_CONSTRAINT__COMPARISON_OPERATOR, newComparisonOperator);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getThreshold() {
		return (Double)eGet(ConstraintPackage.Literals.UNARY_CONSTRAINT__THRESHOLD, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setThreshold(double newThreshold) {
		eSet(ConstraintPackage.Literals.UNARY_CONSTRAINT__THRESHOLD, newThreshold);
	}

} //UnaryConstraintImpl
