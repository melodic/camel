/**
 */
package camel.constraint.impl;

import camel.constraint.ConstraintPackage;
import camel.constraint.MetricVariableConstraint;

import camel.metric.MetricVariable;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Metric Variable Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.impl.MetricVariableConstraintImpl#getMetricVariable <em>Metric Variable</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetricVariableConstraintImpl extends UnaryConstraintImpl implements MetricVariableConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricVariableConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintPackage.Literals.METRIC_VARIABLE_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricVariable getMetricVariable() {
		return (MetricVariable)eGet(ConstraintPackage.Literals.METRIC_VARIABLE_CONSTRAINT__METRIC_VARIABLE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetricVariable(MetricVariable newMetricVariable) {
		eSet(ConstraintPackage.Literals.METRIC_VARIABLE_CONSTRAINT__METRIC_VARIABLE, newMetricVariable);
	}

} //MetricVariableConstraintImpl
