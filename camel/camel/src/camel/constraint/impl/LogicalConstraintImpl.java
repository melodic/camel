/**
 */
package camel.constraint.impl;

import camel.constraint.Constraint;
import camel.constraint.ConstraintPackage;
import camel.constraint.LogicalConstraint;
import camel.constraint.LogicalOperatorType;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Logical Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.impl.LogicalConstraintImpl#getConstraints <em>Constraints</em>}</li>
 *   <li>{@link camel.constraint.impl.LogicalConstraintImpl#getLogicalOperator <em>Logical Operator</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LogicalConstraintImpl extends CompositeConstraintImpl implements LogicalConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LogicalConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintPackage.Literals.LOGICAL_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Constraint> getConstraints() {
		return (EList<Constraint>)eGet(ConstraintPackage.Literals.LOGICAL_CONSTRAINT__CONSTRAINTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LogicalOperatorType getLogicalOperator() {
		return (LogicalOperatorType)eGet(ConstraintPackage.Literals.LOGICAL_CONSTRAINT__LOGICAL_OPERATOR, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLogicalOperator(LogicalOperatorType newLogicalOperator) {
		eSet(ConstraintPackage.Literals.LOGICAL_CONSTRAINT__LOGICAL_OPERATOR, newLogicalOperator);
	}

	/**
	 * The cached invocation delegate for the '{@link #containsConstraint(camel.constraint.LogicalConstraint) <em>Contains Constraint</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #containsConstraint(camel.constraint.LogicalConstraint)
	 * @generated
	 * @ordered
	 */
	protected static final EOperation.Internal.InvocationDelegate CONTAINS_CONSTRAINT_LOGICAL_CONSTRAINT__EINVOCATION_DELEGATE = ((EOperation.Internal)ConstraintPackage.Literals.LOGICAL_CONSTRAINT___CONTAINS_CONSTRAINT__LOGICALCONSTRAINT).getInvocationDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean containsConstraint(LogicalConstraint lc) {
		try {
			return (Boolean)CONTAINS_CONSTRAINT_LOGICAL_CONSTRAINT__EINVOCATION_DELEGATE.dynamicInvoke(this, new BasicEList.UnmodifiableEList<Object>(1, new Object[]{lc}));
		}
		catch (InvocationTargetException ite) {
			throw new WrappedException(ite);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case ConstraintPackage.LOGICAL_CONSTRAINT___CONTAINS_CONSTRAINT__LOGICALCONSTRAINT:
				return containsConstraint((LogicalConstraint)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //LogicalConstraintImpl
