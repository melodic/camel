/**
 */
package camel.constraint.impl;

import camel.constraint.AttributeConstraint;
import camel.constraint.ConstraintPackage;

import camel.metric.AttributeContext;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.impl.AttributeConstraintImpl#getAttributeContext <em>Attribute Context</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class AttributeConstraintImpl extends UnaryConstraintImpl implements AttributeConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintPackage.Literals.ATTRIBUTE_CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeContext getAttributeContext() {
		return (AttributeContext)eGet(ConstraintPackage.Literals.ATTRIBUTE_CONSTRAINT__ATTRIBUTE_CONTEXT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttributeContext(AttributeContext newAttributeContext) {
		eSet(ConstraintPackage.Literals.ATTRIBUTE_CONSTRAINT__ATTRIBUTE_CONTEXT, newAttributeContext);
	}

} //AttributeConstraintImpl
