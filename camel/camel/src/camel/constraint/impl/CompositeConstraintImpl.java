/**
 */
package camel.constraint.impl;

import camel.constraint.CompositeConstraint;
import camel.constraint.ConstraintPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class CompositeConstraintImpl extends ConstraintImpl implements CompositeConstraint {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ConstraintPackage.Literals.COMPOSITE_CONSTRAINT;
	}

} //CompositeConstraintImpl
