/**
 */
package camel.constraint;

import camel.metric.MetricVariable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Metric Variable Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.constraint.MetricVariableConstraint#getMetricVariable <em>Metric Variable</em>}</li>
 * </ul>
 *
 * @see camel.constraint.ConstraintPackage#getMetricVariableConstraint()
 * @model
 * @generated
 */
public interface MetricVariableConstraint extends UnaryConstraint {
	/**
	 * Returns the value of the '<em><b>Metric Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Variable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Variable</em>' reference.
	 * @see #setMetricVariable(MetricVariable)
	 * @see camel.constraint.ConstraintPackage#getMetricVariableConstraint_MetricVariable()
	 * @model required="true"
	 * @generated
	 */
	MetricVariable getMetricVariable();

	/**
	 * Sets the value of the '{@link camel.constraint.MetricVariableConstraint#getMetricVariable <em>Metric Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metric Variable</em>' reference.
	 * @see #getMetricVariable()
	 * @generated
	 */
	void setMetricVariable(MetricVariable value);

} // MetricVariableConstraint
