/**
 */
package camel.data;

import camel.core.Feature;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.data.Data#getDataSource <em>Data Source</em>}</li>
 *   <li>{@link camel.data.Data#getIncludesData <em>Includes Data</em>}</li>
 * </ul>
 *
 * @see camel.data.DataPackage#getData()
 * @model
 * @generated
 */
public interface Data extends Feature {
	/**
	 * Returns the value of the '<em><b>Data Source</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Source</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Source</em>' reference.
	 * @see #setDataSource(DataSource)
	 * @see camel.data.DataPackage#getData_DataSource()
	 * @model
	 * @generated
	 */
	DataSource getDataSource();

	/**
	 * Sets the value of the '{@link camel.data.Data#getDataSource <em>Data Source</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Source</em>' reference.
	 * @see #getDataSource()
	 * @generated
	 */
	void setDataSource(DataSource value);

	/**
	 * Returns the value of the '<em><b>Includes Data</b></em>' containment reference list.
	 * The list contents are of type {@link camel.data.Data}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Includes Data</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Includes Data</em>' containment reference list.
	 * @see camel.data.DataPackage#getData_IncludesData()
	 * @model containment="true"
	 * @generated
	 */
	EList<Data> getIncludesData();

} // Data
