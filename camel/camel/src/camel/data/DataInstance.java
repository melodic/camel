/**
 */
package camel.data;

import camel.core.Feature;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.data.DataInstance#getType <em>Type</em>}</li>
 *   <li>{@link camel.data.DataInstance#getIncludesDataInstance <em>Includes Data Instance</em>}</li>
 *   <li>{@link camel.data.DataInstance#getDataSourceInstance <em>Data Source Instance</em>}</li>
 * </ul>
 *
 * @see camel.data.DataPackage#getDataInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='data_instance_includes_correct_data_instances data_instance_correct_ds_instance'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot data_instance_includes_correct_data_instances='Tuple {\n\tmessage : String = \'DataInstance: \' + self.name + \' contains data instances which have a type which is not contained in the type of this data instance\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.includesDataInstance-&gt;forAll(di | self.type.includesData-&gt;includes(di.type)))\n}.status' data_instance_correct_ds_instance='Tuple {\n\tmessage : String = \'DataInstance: \' + self.name + \' should contain a data source instance whose type is associated with the type of this data instance\'\n\t\t\t,\n\tstatus : Boolean = asError(dataSourceInstance &lt;&gt; null implies (dataSourceInstance.type = self.type.dataSource))\n}.status'"
 * @generated
 */
public interface DataInstance extends Feature {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(Data)
	 * @see camel.data.DataPackage#getDataInstance_Type()
	 * @model required="true"
	 * @generated
	 */
	Data getType();

	/**
	 * Sets the value of the '{@link camel.data.DataInstance#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(Data value);

	/**
	 * Returns the value of the '<em><b>Includes Data Instance</b></em>' containment reference list.
	 * The list contents are of type {@link camel.data.DataInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Includes Data Instance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Includes Data Instance</em>' containment reference list.
	 * @see camel.data.DataPackage#getDataInstance_IncludesDataInstance()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataInstance> getIncludesDataInstance();

	/**
	 * Returns the value of the '<em><b>Data Source Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Source Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Source Instance</em>' reference.
	 * @see #setDataSourceInstance(DataSourceInstance)
	 * @see camel.data.DataPackage#getDataInstance_DataSourceInstance()
	 * @model
	 * @generated
	 */
	DataSourceInstance getDataSourceInstance();

	/**
	 * Sets the value of the '{@link camel.data.DataInstance#getDataSourceInstance <em>Data Source Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Source Instance</em>' reference.
	 * @see #getDataSourceInstance()
	 * @generated
	 */
	void setDataSourceInstance(DataSourceInstance value);

} // DataInstance
