/**
 */
package camel.data;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.data.DataInstanceModel#getDataInstances <em>Data Instances</em>}</li>
 *   <li>{@link camel.data.DataInstanceModel#getType <em>Type</em>}</li>
 *   <li>{@link camel.data.DataInstanceModel#getDataSourceInstances <em>Data Source Instances</em>}</li>
 * </ul>
 *
 * @see camel.data.DataPackage#getDataInstanceModel()
 * @model
 * @generated
 */
public interface DataInstanceModel extends DataModel {
	/**
	 * Returns the value of the '<em><b>Data Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.data.DataInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Instances</em>' containment reference list.
	 * @see camel.data.DataPackage#getDataInstanceModel_DataInstances()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataInstance> getDataInstances();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(DataTypeModel)
	 * @see camel.data.DataPackage#getDataInstanceModel_Type()
	 * @model required="true"
	 * @generated
	 */
	DataTypeModel getType();

	/**
	 * Sets the value of the '{@link camel.data.DataInstanceModel#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(DataTypeModel value);

	/**
	 * Returns the value of the '<em><b>Data Source Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.data.DataSourceInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Source Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Source Instances</em>' containment reference list.
	 * @see camel.data.DataPackage#getDataInstanceModel_DataSourceInstances()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataSourceInstance> getDataSourceInstances();

} // DataInstanceModel
