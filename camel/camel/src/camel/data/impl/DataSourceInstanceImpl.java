/**
 */
package camel.data.impl;

import camel.core.impl.FeatureImpl;

import camel.data.DataPackage;
import camel.data.DataSource;
import camel.data.DataSourceInstance;

import camel.deployment.SoftwareComponentInstance;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Source Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.data.impl.DataSourceInstanceImpl#getType <em>Type</em>}</li>
 *   <li>{@link camel.data.impl.DataSourceInstanceImpl#getSoftwareComponentInstance <em>Software Component Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataSourceInstanceImpl extends FeatureImpl implements DataSourceInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataSourceInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.DATA_SOURCE_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSource getType() {
		return (DataSource)eGet(DataPackage.Literals.DATA_SOURCE_INSTANCE__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(DataSource newType) {
		eSet(DataPackage.Literals.DATA_SOURCE_INSTANCE__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareComponentInstance getSoftwareComponentInstance() {
		return (SoftwareComponentInstance)eGet(DataPackage.Literals.DATA_SOURCE_INSTANCE__SOFTWARE_COMPONENT_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSoftwareComponentInstance(SoftwareComponentInstance newSoftwareComponentInstance) {
		eSet(DataPackage.Literals.DATA_SOURCE_INSTANCE__SOFTWARE_COMPONENT_INSTANCE, newSoftwareComponentInstance);
	}

} //DataSourceInstanceImpl
