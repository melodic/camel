/**
 */
package camel.data.impl;

import camel.data.DataInstance;
import camel.data.DataInstanceModel;
import camel.data.DataPackage;
import camel.data.DataSourceInstance;
import camel.data.DataTypeModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instance Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.data.impl.DataInstanceModelImpl#getDataInstances <em>Data Instances</em>}</li>
 *   <li>{@link camel.data.impl.DataInstanceModelImpl#getType <em>Type</em>}</li>
 *   <li>{@link camel.data.impl.DataInstanceModelImpl#getDataSourceInstances <em>Data Source Instances</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataInstanceModelImpl extends DataModelImpl implements DataInstanceModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataInstanceModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.DATA_INSTANCE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DataInstance> getDataInstances() {
		return (EList<DataInstance>)eGet(DataPackage.Literals.DATA_INSTANCE_MODEL__DATA_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataTypeModel getType() {
		return (DataTypeModel)eGet(DataPackage.Literals.DATA_INSTANCE_MODEL__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(DataTypeModel newType) {
		eSet(DataPackage.Literals.DATA_INSTANCE_MODEL__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DataSourceInstance> getDataSourceInstances() {
		return (EList<DataSourceInstance>)eGet(DataPackage.Literals.DATA_INSTANCE_MODEL__DATA_SOURCE_INSTANCES, true);
	}

} //DataInstanceModelImpl
