/**
 */
package camel.data.impl;

import camel.data.Data;
import camel.data.DataPackage;
import camel.data.DataSource;
import camel.data.DataTypeModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.data.impl.DataTypeModelImpl#getData <em>Data</em>}</li>
 *   <li>{@link camel.data.impl.DataTypeModelImpl#getDataSources <em>Data Sources</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataTypeModelImpl extends DataModelImpl implements DataTypeModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataTypeModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.DATA_TYPE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Data> getData() {
		return (EList<Data>)eGet(DataPackage.Literals.DATA_TYPE_MODEL__DATA, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DataSource> getDataSources() {
		return (EList<DataSource>)eGet(DataPackage.Literals.DATA_TYPE_MODEL__DATA_SOURCES, true);
	}

} //DataTypeModelImpl
