/**
 */
package camel.data.impl;

import camel.core.impl.FeatureImpl;

import camel.data.DataPackage;
import camel.data.DataSource;

import camel.deployment.SoftwareComponent;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Source</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.data.impl.DataSourceImpl#isExternal <em>External</em>}</li>
 *   <li>{@link camel.data.impl.DataSourceImpl#getComponent <em>Component</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataSourceImpl extends FeatureImpl implements DataSource {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataSourceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.DATA_SOURCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isExternal() {
		return (Boolean)eGet(DataPackage.Literals.DATA_SOURCE__EXTERNAL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExternal(boolean newExternal) {
		eSet(DataPackage.Literals.DATA_SOURCE__EXTERNAL, newExternal);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareComponent getComponent() {
		return (SoftwareComponent)eGet(DataPackage.Literals.DATA_SOURCE__COMPONENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponent(SoftwareComponent newComponent) {
		eSet(DataPackage.Literals.DATA_SOURCE__COMPONENT, newComponent);
	}

} //DataSourceImpl
