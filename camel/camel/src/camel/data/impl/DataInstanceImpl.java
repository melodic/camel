/**
 */
package camel.data.impl;

import camel.core.impl.FeatureImpl;

import camel.data.Data;
import camel.data.DataInstance;
import camel.data.DataPackage;
import camel.data.DataSourceInstance;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.data.impl.DataInstanceImpl#getType <em>Type</em>}</li>
 *   <li>{@link camel.data.impl.DataInstanceImpl#getIncludesDataInstance <em>Includes Data Instance</em>}</li>
 *   <li>{@link camel.data.impl.DataInstanceImpl#getDataSourceInstance <em>Data Source Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataInstanceImpl extends FeatureImpl implements DataInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.DATA_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Data getType() {
		return (Data)eGet(DataPackage.Literals.DATA_INSTANCE__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(Data newType) {
		eSet(DataPackage.Literals.DATA_INSTANCE__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DataInstance> getIncludesDataInstance() {
		return (EList<DataInstance>)eGet(DataPackage.Literals.DATA_INSTANCE__INCLUDES_DATA_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSourceInstance getDataSourceInstance() {
		return (DataSourceInstance)eGet(DataPackage.Literals.DATA_INSTANCE__DATA_SOURCE_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataSourceInstance(DataSourceInstance newDataSourceInstance) {
		eSet(DataPackage.Literals.DATA_INSTANCE__DATA_SOURCE_INSTANCE, newDataSourceInstance);
	}

} //DataInstanceImpl
