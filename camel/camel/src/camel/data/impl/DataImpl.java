/**
 */
package camel.data.impl;

import camel.core.impl.FeatureImpl;

import camel.data.Data;
import camel.data.DataPackage;
import camel.data.DataSource;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.data.impl.DataImpl#getDataSource <em>Data Source</em>}</li>
 *   <li>{@link camel.data.impl.DataImpl#getIncludesData <em>Includes Data</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataImpl extends FeatureImpl implements Data {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DataPackage.Literals.DATA;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataSource getDataSource() {
		return (DataSource)eGet(DataPackage.Literals.DATA__DATA_SOURCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataSource(DataSource newDataSource) {
		eSet(DataPackage.Literals.DATA__DATA_SOURCE, newDataSource);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Data> getIncludesData() {
		return (EList<Data>)eGet(DataPackage.Literals.DATA__INCLUDES_DATA, true);
	}

} //DataImpl
