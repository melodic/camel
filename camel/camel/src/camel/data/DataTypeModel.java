/**
 */
package camel.data;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.data.DataTypeModel#getData <em>Data</em>}</li>
 *   <li>{@link camel.data.DataTypeModel#getDataSources <em>Data Sources</em>}</li>
 * </ul>
 *
 * @see camel.data.DataPackage#getDataTypeModel()
 * @model
 * @generated
 */
public interface DataTypeModel extends DataModel {
	/**
	 * Returns the value of the '<em><b>Data</b></em>' containment reference list.
	 * The list contents are of type {@link camel.data.Data}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data</em>' containment reference list.
	 * @see camel.data.DataPackage#getDataTypeModel_Data()
	 * @model containment="true"
	 * @generated
	 */
	EList<Data> getData();

	/**
	 * Returns the value of the '<em><b>Data Sources</b></em>' containment reference list.
	 * The list contents are of type {@link camel.data.DataSource}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Sources</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Sources</em>' containment reference list.
	 * @see camel.data.DataPackage#getDataTypeModel_DataSources()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataSource> getDataSources();

} // DataTypeModel
