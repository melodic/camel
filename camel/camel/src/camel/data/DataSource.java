/**
 */
package camel.data;

import camel.core.Feature;

import camel.deployment.SoftwareComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.data.DataSource#isExternal <em>External</em>}</li>
 *   <li>{@link camel.data.DataSource#getComponent <em>Component</em>}</li>
 * </ul>
 *
 * @see camel.data.DataPackage#getDataSource()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='valid_data_source'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot valid_data_source='Tuple {\n\tmessage : String = \'If DataSource: \' + self.name + \' is external, it should not be associated with a sw component; otherwise, it should\'\n\t\t\t,\n\tstatus : Boolean = asError((external = true) xor (component &lt;&gt; null))\n}.status'"
 * @generated
 */
public interface DataSource extends Feature {
	/**
	 * Returns the value of the '<em><b>External</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External</em>' attribute.
	 * @see #setExternal(boolean)
	 * @see camel.data.DataPackage#getDataSource_External()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isExternal();

	/**
	 * Sets the value of the '{@link camel.data.DataSource#isExternal <em>External</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>External</em>' attribute.
	 * @see #isExternal()
	 * @generated
	 */
	void setExternal(boolean value);

	/**
	 * Returns the value of the '<em><b>Component</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link camel.deployment.SoftwareComponent#getManagesDataSource <em>Manages Data Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component</em>' reference.
	 * @see #setComponent(SoftwareComponent)
	 * @see camel.data.DataPackage#getDataSource_Component()
	 * @see camel.deployment.SoftwareComponent#getManagesDataSource
	 * @model opposite="managesDataSource"
	 * @generated
	 */
	SoftwareComponent getComponent();

	/**
	 * Sets the value of the '{@link camel.data.DataSource#getComponent <em>Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component</em>' reference.
	 * @see #getComponent()
	 * @generated
	 */
	void setComponent(SoftwareComponent value);

} // DataSource
