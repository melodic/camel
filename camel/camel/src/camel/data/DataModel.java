/**
 */
package camel.data;

import camel.core.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.data.DataPackage#getDataModel()
 * @model abstract="true"
 * @generated
 */
public interface DataModel extends Model {
} // DataModel
