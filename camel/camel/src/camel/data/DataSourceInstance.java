/**
 */
package camel.data;

import camel.core.Feature;

import camel.deployment.SoftwareComponentInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Source Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.data.DataSourceInstance#getType <em>Type</em>}</li>
 *   <li>{@link camel.data.DataSourceInstance#getSoftwareComponentInstance <em>Software Component Instance</em>}</li>
 * </ul>
 *
 * @see camel.data.DataPackage#getDataSourceInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='data_source_instance_correct_sw_component_inst'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot data_source_instance_correct_sw_component_inst='Tuple {\n\tmessage : String = \'DataSourceInstance: \' + self.name + \' should be associated with a sw component instance whose type is associated with the type of this data source instance -- please note that this type should be internal\'\n\t\t\t,\n\tstatus : Boolean = asError(softwareComponentInstance &lt;&gt; null implies softwareComponentInstance.type = self.type.component)\n}.status'"
 * @generated
 */
public interface DataSourceInstance extends Feature {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(DataSource)
	 * @see camel.data.DataPackage#getDataSourceInstance_Type()
	 * @model required="true"
	 * @generated
	 */
	DataSource getType();

	/**
	 * Sets the value of the '{@link camel.data.DataSourceInstance#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(DataSource value);

	/**
	 * Returns the value of the '<em><b>Software Component Instance</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link camel.deployment.SoftwareComponentInstance#getManagesDataSourceInstance <em>Manages Data Source Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software Component Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software Component Instance</em>' reference.
	 * @see #setSoftwareComponentInstance(SoftwareComponentInstance)
	 * @see camel.data.DataPackage#getDataSourceInstance_SoftwareComponentInstance()
	 * @see camel.deployment.SoftwareComponentInstance#getManagesDataSourceInstance
	 * @model opposite="managesDataSourceInstance"
	 * @generated
	 */
	SoftwareComponentInstance getSoftwareComponentInstance();

	/**
	 * Sets the value of the '{@link camel.data.DataSourceInstance#getSoftwareComponentInstance <em>Software Component Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Software Component Instance</em>' reference.
	 * @see #getSoftwareComponentInstance()
	 * @generated
	 */
	void setSoftwareComponentInstance(SoftwareComponentInstance value);

} // DataSourceInstance
