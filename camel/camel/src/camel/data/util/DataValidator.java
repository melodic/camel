/**
 */
package camel.data.util;

import camel.data.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.data.DataPackage
 * @generated
 */
public class DataValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final DataValidator INSTANCE = new DataValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.data";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return DataPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case DataPackage.DATA_MODEL:
				return validateDataModel((DataModel)value, diagnostics, context);
			case DataPackage.DATA:
				return validateData((Data)value, diagnostics, context);
			case DataPackage.DATA_INSTANCE:
				return validateDataInstance((DataInstance)value, diagnostics, context);
			case DataPackage.DATA_TYPE_MODEL:
				return validateDataTypeModel((DataTypeModel)value, diagnostics, context);
			case DataPackage.DATA_INSTANCE_MODEL:
				return validateDataInstanceModel((DataInstanceModel)value, diagnostics, context);
			case DataPackage.DATA_SOURCE:
				return validateDataSource((DataSource)value, diagnostics, context);
			case DataPackage.DATA_SOURCE_INSTANCE:
				return validateDataSourceInstance((DataSourceInstance)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataModel(DataModel dataModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)dataModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateData(Data data, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)data, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataInstance(DataInstance dataInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)dataInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)dataInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)dataInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)dataInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)dataInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)dataInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)dataInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)dataInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)dataInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateDataInstance_data_instance_includes_correct_data_instances(dataInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateDataInstance_data_instance_correct_ds_instance(dataInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the data_instance_includes_correct_data_instances constraint of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String DATA_INSTANCE__DATA_INSTANCE_INCLUDES_CORRECT_DATA_INSTANCES__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'DataInstance: ' + self.name + ' contains data instances which have a type which is not contained in the type of this data instance',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.includesDataInstance->forAll(di | self.type.includesData->includes(di.type)))\n" +
		"}.status";

	/**
	 * Validates the data_instance_includes_correct_data_instances constraint of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataInstance_data_instance_includes_correct_data_instances(DataInstance dataInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DataPackage.Literals.DATA_INSTANCE,
				 (EObject)dataInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "data_instance_includes_correct_data_instances",
				 DATA_INSTANCE__DATA_INSTANCE_INCLUDES_CORRECT_DATA_INSTANCES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the data_instance_correct_ds_instance constraint of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String DATA_INSTANCE__DATA_INSTANCE_CORRECT_DS_INSTANCE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'DataInstance: ' + self.name + ' should contain a data source instance whose type is associated with the type of this data instance'\n" +
		"\t\t\t,\n" +
		"\tstatus : Boolean = asError(dataSourceInstance <> null implies (dataSourceInstance.type = self.type.dataSource))\n" +
		"}.status";

	/**
	 * Validates the data_instance_correct_ds_instance constraint of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataInstance_data_instance_correct_ds_instance(DataInstance dataInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DataPackage.Literals.DATA_INSTANCE,
				 (EObject)dataInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "data_instance_correct_ds_instance",
				 DATA_INSTANCE__DATA_INSTANCE_CORRECT_DS_INSTANCE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataTypeModel(DataTypeModel dataTypeModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)dataTypeModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataInstanceModel(DataInstanceModel dataInstanceModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)dataInstanceModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataSource(DataSource dataSource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)dataSource, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)dataSource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)dataSource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)dataSource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)dataSource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)dataSource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)dataSource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)dataSource, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)dataSource, diagnostics, context);
		if (result || diagnostics != null) result &= validateDataSource_valid_data_source(dataSource, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the valid_data_source constraint of '<em>Source</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String DATA_SOURCE__VALID_DATA_SOURCE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'If DataSource: ' + self.name + ' is external, it should not be associated with a sw component; otherwise, it should'\n" +
		"\t\t\t,\n" +
		"\tstatus : Boolean = asError((external = true) xor (component <> null))\n" +
		"}.status";

	/**
	 * Validates the valid_data_source constraint of '<em>Source</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataSource_valid_data_source(DataSource dataSource, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DataPackage.Literals.DATA_SOURCE,
				 (EObject)dataSource,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "valid_data_source",
				 DATA_SOURCE__VALID_DATA_SOURCE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataSourceInstance(DataSourceInstance dataSourceInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)dataSourceInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)dataSourceInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)dataSourceInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)dataSourceInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)dataSourceInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)dataSourceInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)dataSourceInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)dataSourceInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)dataSourceInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateDataSourceInstance_data_source_instance_correct_sw_component_inst(dataSourceInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the data_source_instance_correct_sw_component_inst constraint of '<em>Source Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String DATA_SOURCE_INSTANCE__DATA_SOURCE_INSTANCE_CORRECT_SW_COMPONENT_INST__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'DataSourceInstance: ' + self.name + ' should be associated with a sw component instance whose type is associated with the type of this data source instance -- please note that this type should be internal'\n" +
		"\t\t\t,\n" +
		"\tstatus : Boolean = asError(softwareComponentInstance <> null implies softwareComponentInstance.type = self.type.component)\n" +
		"}.status";

	/**
	 * Validates the data_source_instance_correct_sw_component_inst constraint of '<em>Source Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataSourceInstance_data_source_instance_correct_sw_component_inst(DataSourceInstance dataSourceInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DataPackage.Literals.DATA_SOURCE_INSTANCE,
				 (EObject)dataSourceInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "data_source_instance_correct_sw_component_inst",
				 DATA_SOURCE_INSTANCE__DATA_SOURCE_INSTANCE_CORRECT_SW_COMPONENT_INST__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //DataValidator
