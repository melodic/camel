/**
 */
package camel.deployment;

import camel.core.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.deployment.DeploymentPackage#getDeploymentModel()
 * @model abstract="true"
 * @generated
 */
public interface DeploymentModel extends Model {
} // DeploymentModel
