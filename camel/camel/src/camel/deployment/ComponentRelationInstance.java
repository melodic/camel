/**
 */
package camel.deployment;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Relation Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.deployment.DeploymentPackage#getComponentRelationInstance()
 * @model abstract="true"
 * @generated
 */
public interface ComponentRelationInstance extends Feature {
} // ComponentRelationInstance
