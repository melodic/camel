/**
 */
package camel.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Paa SInstance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.deployment.DeploymentPackage#getPaaSInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correct_type_for_paas_instance'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot correct_type_for_paas_instance='Tuple {\n\tmessage : String = \'PaaS Instance: \' + self.name + \' should have as type a PaaS\',\n\tstatus : Boolean = asError(type.oclIsTypeOf(PaaS))\n}.status'"
 * @generated
 */
public interface PaaSInstance extends ComponentInstance {
} // PaaSInstance
