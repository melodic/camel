/**
 */
package camel.deployment;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hosting Port</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.deployment.DeploymentPackage#getHostingPort()
 * @model abstract="true"
 * @generated
 */
public interface HostingPort extends Feature {
} // HostingPort
