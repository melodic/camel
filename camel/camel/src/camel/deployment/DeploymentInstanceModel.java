/**
 */
package camel.deployment;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.DeploymentInstanceModel#getSoftwareComponentInstances <em>Software Component Instances</em>}</li>
 *   <li>{@link camel.deployment.DeploymentInstanceModel#getVmInstances <em>Vm Instances</em>}</li>
 *   <li>{@link camel.deployment.DeploymentInstanceModel#getCommunicationInstances <em>Communication Instances</em>}</li>
 *   <li>{@link camel.deployment.DeploymentInstanceModel#getHostingInstances <em>Hosting Instances</em>}</li>
 *   <li>{@link camel.deployment.DeploymentInstanceModel#getType <em>Type</em>}</li>
 *   <li>{@link camel.deployment.DeploymentInstanceModel#getContainerInstances <em>Container Instances</em>}</li>
 *   <li>{@link camel.deployment.DeploymentInstanceModel#getPaasInstances <em>Paas Instances</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getDeploymentInstanceModel()
 * @model
 * @generated
 */
public interface DeploymentInstanceModel extends DeploymentModel {
	/**
	 * Returns the value of the '<em><b>Software Component Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.SoftwareComponentInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software Component Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software Component Instances</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentInstanceModel_SoftwareComponentInstances()
	 * @model containment="true"
	 * @generated
	 */
	EList<SoftwareComponentInstance> getSoftwareComponentInstances();

	/**
	 * Returns the value of the '<em><b>Vm Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.VMInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vm Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vm Instances</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentInstanceModel_VmInstances()
	 * @model containment="true"
	 * @generated
	 */
	EList<VMInstance> getVmInstances();

	/**
	 * Returns the value of the '<em><b>Communication Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.CommunicationInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communication Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communication Instances</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentInstanceModel_CommunicationInstances()
	 * @model containment="true"
	 * @generated
	 */
	EList<CommunicationInstance> getCommunicationInstances();

	/**
	 * Returns the value of the '<em><b>Hosting Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.HostingInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hosting Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hosting Instances</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentInstanceModel_HostingInstances()
	 * @model containment="true"
	 * @generated
	 */
	EList<HostingInstance> getHostingInstances();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(DeploymentTypeModel)
	 * @see camel.deployment.DeploymentPackage#getDeploymentInstanceModel_Type()
	 * @model required="true"
	 * @generated
	 */
	DeploymentTypeModel getType();

	/**
	 * Sets the value of the '{@link camel.deployment.DeploymentInstanceModel#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(DeploymentTypeModel value);

	/**
	 * Returns the value of the '<em><b>Container Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.ContainerInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container Instances</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentInstanceModel_ContainerInstances()
	 * @model containment="true"
	 * @generated
	 */
	EList<ContainerInstance> getContainerInstances();

	/**
	 * Returns the value of the '<em><b>Paas Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.PaaSInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Paas Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Paas Instances</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentInstanceModel_PaasInstances()
	 * @model containment="true"
	 * @generated
	 */
	EList<PaaSInstance> getPaasInstances();

} // DeploymentInstanceModel
