/**
 */
package camel.deployment;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Build Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.BuildConfiguration#getArtifactId <em>Artifact Id</em>}</li>
 *   <li>{@link camel.deployment.BuildConfiguration#getBuildFramework <em>Build Framework</em>}</li>
 *   <li>{@link camel.deployment.BuildConfiguration#getSourceCodeURL <em>Source Code URL</em>}</li>
 *   <li>{@link camel.deployment.BuildConfiguration#getInclude <em>Include</em>}</li>
 *   <li>{@link camel.deployment.BuildConfiguration#getExclude <em>Exclude</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getBuildConfiguration()
 * @model
 * @generated
 */
public interface BuildConfiguration extends Feature {
	/**
	 * Returns the value of the '<em><b>Artifact Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Artifact Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Artifact Id</em>' attribute.
	 * @see #setArtifactId(String)
	 * @see camel.deployment.DeploymentPackage#getBuildConfiguration_ArtifactId()
	 * @model required="true"
	 * @generated
	 */
	String getArtifactId();

	/**
	 * Sets the value of the '{@link camel.deployment.BuildConfiguration#getArtifactId <em>Artifact Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Artifact Id</em>' attribute.
	 * @see #getArtifactId()
	 * @generated
	 */
	void setArtifactId(String value);

	/**
	 * Returns the value of the '<em><b>Build Framework</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Build Framework</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Build Framework</em>' attribute.
	 * @see #setBuildFramework(String)
	 * @see camel.deployment.DeploymentPackage#getBuildConfiguration_BuildFramework()
	 * @model required="true"
	 * @generated
	 */
	String getBuildFramework();

	/**
	 * Sets the value of the '{@link camel.deployment.BuildConfiguration#getBuildFramework <em>Build Framework</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Build Framework</em>' attribute.
	 * @see #getBuildFramework()
	 * @generated
	 */
	void setBuildFramework(String value);

	/**
	 * Returns the value of the '<em><b>Source Code URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source Code URL</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source Code URL</em>' attribute.
	 * @see #setSourceCodeURL(String)
	 * @see camel.deployment.DeploymentPackage#getBuildConfiguration_SourceCodeURL()
	 * @model required="true"
	 * @generated
	 */
	String getSourceCodeURL();

	/**
	 * Sets the value of the '{@link camel.deployment.BuildConfiguration#getSourceCodeURL <em>Source Code URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source Code URL</em>' attribute.
	 * @see #getSourceCodeURL()
	 * @generated
	 */
	void setSourceCodeURL(String value);

	/**
	 * Returns the value of the '<em><b>Include</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Include</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Include</em>' attribute.
	 * @see #setInclude(String)
	 * @see camel.deployment.DeploymentPackage#getBuildConfiguration_Include()
	 * @model
	 * @generated
	 */
	String getInclude();

	/**
	 * Sets the value of the '{@link camel.deployment.BuildConfiguration#getInclude <em>Include</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Include</em>' attribute.
	 * @see #getInclude()
	 * @generated
	 */
	void setInclude(String value);

	/**
	 * Returns the value of the '<em><b>Exclude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Exclude</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Exclude</em>' attribute.
	 * @see #setExclude(String)
	 * @see camel.deployment.DeploymentPackage#getBuildConfiguration_Exclude()
	 * @model
	 * @generated
	 */
	String getExclude();

	/**
	 * Sets the value of the '{@link camel.deployment.BuildConfiguration#getExclude <em>Exclude</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Exclude</em>' attribute.
	 * @see #getExclude()
	 * @generated
	 */
	void setExclude(String value);

} // BuildConfiguration
