/**
 */
package camel.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provided Host Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.deployment.DeploymentPackage#getProvidedHostInstance()
 * @model
 * @generated
 */
public interface ProvidedHostInstance extends HostingPortInstance {
} // ProvidedHostInstance
