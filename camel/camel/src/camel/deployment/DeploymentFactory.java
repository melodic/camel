/**
 */
package camel.deployment;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see camel.deployment.DeploymentPackage
 * @generated
 */
public interface DeploymentFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DeploymentFactory eINSTANCE = camel.deployment.impl.DeploymentFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Instance Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Instance Model</em>'.
	 * @generated
	 */
	DeploymentInstanceModel createDeploymentInstanceModel();

	/**
	 * Returns a new object of class '<em>Type Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Type Model</em>'.
	 * @generated
	 */
	DeploymentTypeModel createDeploymentTypeModel();

	/**
	 * Returns a new object of class '<em>Software Component</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Software Component</em>'.
	 * @generated
	 */
	SoftwareComponent createSoftwareComponent();

	/**
	 * Returns a new object of class '<em>VM</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VM</em>'.
	 * @generated
	 */
	VM createVM();

	/**
	 * Returns a new object of class '<em>Requirement Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Requirement Set</em>'.
	 * @generated
	 */
	RequirementSet createRequirementSet();

	/**
	 * Returns a new object of class '<em>Communication</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication</em>'.
	 * @generated
	 */
	Communication createCommunication();

	/**
	 * Returns a new object of class '<em>Provided Communication</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Provided Communication</em>'.
	 * @generated
	 */
	ProvidedCommunication createProvidedCommunication();

	/**
	 * Returns a new object of class '<em>Required Communication</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Required Communication</em>'.
	 * @generated
	 */
	RequiredCommunication createRequiredCommunication();

	/**
	 * Returns a new object of class '<em>Hosting</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hosting</em>'.
	 * @generated
	 */
	Hosting createHosting();

	/**
	 * Returns a new object of class '<em>Provided Host</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Provided Host</em>'.
	 * @generated
	 */
	ProvidedHost createProvidedHost();

	/**
	 * Returns a new object of class '<em>Required Host</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Required Host</em>'.
	 * @generated
	 */
	RequiredHost createRequiredHost();

	/**
	 * Returns a new object of class '<em>Software Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Software Component Instance</em>'.
	 * @generated
	 */
	SoftwareComponentInstance createSoftwareComponentInstance();

	/**
	 * Returns a new object of class '<em>VM Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VM Instance</em>'.
	 * @generated
	 */
	VMInstance createVMInstance();

	/**
	 * Returns a new object of class '<em>Communication Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Instance</em>'.
	 * @generated
	 */
	CommunicationInstance createCommunicationInstance();

	/**
	 * Returns a new object of class '<em>Communication Port Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Port Instance</em>'.
	 * @generated
	 */
	CommunicationPortInstance createCommunicationPortInstance();

	/**
	 * Returns a new object of class '<em>Provided Communication Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Provided Communication Instance</em>'.
	 * @generated
	 */
	ProvidedCommunicationInstance createProvidedCommunicationInstance();

	/**
	 * Returns a new object of class '<em>Required Communication Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Required Communication Instance</em>'.
	 * @generated
	 */
	RequiredCommunicationInstance createRequiredCommunicationInstance();

	/**
	 * Returns a new object of class '<em>Hosting Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Hosting Instance</em>'.
	 * @generated
	 */
	HostingInstance createHostingInstance();

	/**
	 * Returns a new object of class '<em>Provided Host Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Provided Host Instance</em>'.
	 * @generated
	 */
	ProvidedHostInstance createProvidedHostInstance();

	/**
	 * Returns a new object of class '<em>Required Host Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Required Host Instance</em>'.
	 * @generated
	 */
	RequiredHostInstance createRequiredHostInstance();

	/**
	 * Returns a new object of class '<em>Script Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Script Configuration</em>'.
	 * @generated
	 */
	ScriptConfiguration createScriptConfiguration();

	/**
	 * Returns a new object of class '<em>Cluster Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cluster Configuration</em>'.
	 * @generated
	 */
	ClusterConfiguration createClusterConfiguration();

	/**
	 * Returns a new object of class '<em>Location Coupling</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Location Coupling</em>'.
	 * @generated
	 */
	LocationCoupling createLocationCoupling();

	/**
	 * Returns a new object of class '<em>Container</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Container</em>'.
	 * @generated
	 */
	Container createContainer();

	/**
	 * Returns a new object of class '<em>Container Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Container Instance</em>'.
	 * @generated
	 */
	ContainerInstance createContainerInstance();

	/**
	 * Returns a new object of class '<em>Paa S</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Paa S</em>'.
	 * @generated
	 */
	PaaS createPaaS();

	/**
	 * Returns a new object of class '<em>Paa SInstance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Paa SInstance</em>'.
	 * @generated
	 */
	PaaSInstance createPaaSInstance();

	/**
	 * Returns a new object of class '<em>Paa SConfiguration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Paa SConfiguration</em>'.
	 * @generated
	 */
	PaaSConfiguration createPaaSConfiguration();

	/**
	 * Returns a new object of class '<em>Serverless Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Serverless Configuration</em>'.
	 * @generated
	 */
	ServerlessConfiguration createServerlessConfiguration();

	/**
	 * Returns a new object of class '<em>Build Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Build Configuration</em>'.
	 * @generated
	 */
	BuildConfiguration createBuildConfiguration();

	/**
	 * Returns a new object of class '<em>Event Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Event Configuration</em>'.
	 * @generated
	 */
	EventConfiguration createEventConfiguration();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	DeploymentPackage getDeploymentPackage();

} //DeploymentFactory
