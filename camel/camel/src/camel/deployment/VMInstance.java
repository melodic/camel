/**
 */
package camel.deployment;

import camel.core.Attribute;

import camel.location.Location;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VM Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.VMInstance#getLocation <em>Location</em>}</li>
 *   <li>{@link camel.deployment.VMInstance#getOs <em>Os</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getVMInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correct_type_for_vm_instance'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot correct_type_for_vm_instance='Tuple {\n\tmessage : String = \'VM Instance: \' + self.name + \' should have as type a VM\',\n\tstatus : Boolean = asError(type.oclIsTypeOf(VM))\n}.status'"
 * @generated
 */
public interface VMInstance extends ComponentInstance {
	/**
	 * Returns the value of the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location</em>' reference.
	 * @see #setLocation(Location)
	 * @see camel.deployment.DeploymentPackage#getVMInstance_Location()
	 * @model required="true"
	 * @generated
	 */
	Location getLocation();

	/**
	 * Sets the value of the '{@link camel.deployment.VMInstance#getLocation <em>Location</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location</em>' reference.
	 * @see #getLocation()
	 * @generated
	 */
	void setLocation(Location value);

	/**
	 * Returns the value of the '<em><b>Os</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Os</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Os</em>' reference.
	 * @see #setOs(Attribute)
	 * @see camel.deployment.DeploymentPackage#getVMInstance_Os()
	 * @model
	 * @generated
	 */
	Attribute getOs();

	/**
	 * Sets the value of the '{@link camel.deployment.VMInstance#getOs <em>Os</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Os</em>' reference.
	 * @see #getOs()
	 * @generated
	 */
	void setOs(Attribute value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='System.out.println(\"Checking dates for VMInstance: \" + vm);\n\t\tjava.util.Date createdOn = vm.getInstantiatedOn();\n\t\tjava.util.Date destroyedOn = vm.getDestroyedOn();\n\t\tif (createdOn != null &amp;&amp; destroyedOn != null &amp;&amp; destroyedOn.before(createdOn)) return Boolean.FALSE;\n\t\treturn Boolean.TRUE;'"
	 * @generated
	 */
	boolean checkDates(VMInstance vm);

} // VMInstance
