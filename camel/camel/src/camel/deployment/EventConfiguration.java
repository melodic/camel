/**
 */
package camel.deployment;

import camel.core.Feature;

import camel.metric.Schedule;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Event Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.EventConfiguration#getHttpMethodName <em>Http Method Name</em>}</li>
 *   <li>{@link camel.deployment.EventConfiguration#getHttpMethodType <em>Http Method Type</em>}</li>
 *   <li>{@link camel.deployment.EventConfiguration#getExecutionSchedule <em>Execution Schedule</em>}</li>
 *   <li>{@link camel.deployment.EventConfiguration#getScheduledExecutionConfig <em>Scheduled Execution Config</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getEventConfiguration()
 * @model
 * @generated
 */
public interface EventConfiguration extends Feature {
	/**
	 * Returns the value of the '<em><b>Http Method Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Http Method Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Http Method Name</em>' attribute.
	 * @see #setHttpMethodName(String)
	 * @see camel.deployment.DeploymentPackage#getEventConfiguration_HttpMethodName()
	 * @model
	 * @generated
	 */
	String getHttpMethodName();

	/**
	 * Sets the value of the '{@link camel.deployment.EventConfiguration#getHttpMethodName <em>Http Method Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Http Method Name</em>' attribute.
	 * @see #getHttpMethodName()
	 * @generated
	 */
	void setHttpMethodName(String value);

	/**
	 * Returns the value of the '<em><b>Http Method Type</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.deployment.HTTPMethodType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Http Method Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Http Method Type</em>' attribute.
	 * @see camel.deployment.HTTPMethodType
	 * @see #setHttpMethodType(HTTPMethodType)
	 * @see camel.deployment.DeploymentPackage#getEventConfiguration_HttpMethodType()
	 * @model
	 * @generated
	 */
	HTTPMethodType getHttpMethodType();

	/**
	 * Sets the value of the '{@link camel.deployment.EventConfiguration#getHttpMethodType <em>Http Method Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Http Method Type</em>' attribute.
	 * @see camel.deployment.HTTPMethodType
	 * @see #getHttpMethodType()
	 * @generated
	 */
	void setHttpMethodType(HTTPMethodType value);

	/**
	 * Returns the value of the '<em><b>Execution Schedule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Schedule</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Schedule</em>' reference.
	 * @see #setExecutionSchedule(Schedule)
	 * @see camel.deployment.DeploymentPackage#getEventConfiguration_ExecutionSchedule()
	 * @model
	 * @generated
	 */
	Schedule getExecutionSchedule();

	/**
	 * Sets the value of the '{@link camel.deployment.EventConfiguration#getExecutionSchedule <em>Execution Schedule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Schedule</em>' reference.
	 * @see #getExecutionSchedule()
	 * @generated
	 */
	void setExecutionSchedule(Schedule value);

	/**
	 * Returns the value of the '<em><b>Scheduled Execution Config</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scheduled Execution Config</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scheduled Execution Config</em>' containment reference.
	 * @see #setScheduledExecutionConfig(Feature)
	 * @see camel.deployment.DeploymentPackage#getEventConfiguration_ScheduledExecutionConfig()
	 * @model containment="true"
	 * @generated
	 */
	Feature getScheduledExecutionConfig();

	/**
	 * Sets the value of the '{@link camel.deployment.EventConfiguration#getScheduledExecutionConfig <em>Scheduled Execution Config</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scheduled Execution Config</em>' containment reference.
	 * @see #getScheduledExecutionConfig()
	 * @generated
	 */
	void setScheduledExecutionConfig(Feature value);

} // EventConfiguration
