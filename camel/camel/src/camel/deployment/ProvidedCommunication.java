/**
 */
package camel.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provided Communication</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.deployment.DeploymentPackage#getProvidedCommunication()
 * @model
 * @generated
 */
public interface ProvidedCommunication extends CommunicationPort {
} // ProvidedCommunication
