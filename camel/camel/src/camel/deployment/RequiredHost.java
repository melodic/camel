/**
 */
package camel.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Required Host</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.deployment.DeploymentPackage#getRequiredHost()
 * @model
 * @generated
 */
public interface RequiredHost extends HostingPort {
} // RequiredHost
