/**
 */
package camel.deployment.impl;

import camel.deployment.DeploymentPackage;
import camel.deployment.PaaSConfiguration;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Paa SConfiguration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.PaaSConfigurationImpl#getApi <em>Api</em>}</li>
 *   <li>{@link camel.deployment.impl.PaaSConfigurationImpl#getVersion <em>Version</em>}</li>
 *   <li>{@link camel.deployment.impl.PaaSConfigurationImpl#getEndpoint <em>Endpoint</em>}</li>
 *   <li>{@link camel.deployment.impl.PaaSConfigurationImpl#getDownloadURL <em>Download URL</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PaaSConfigurationImpl extends ConfigurationImpl implements PaaSConfiguration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PaaSConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.PAA_SCONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getApi() {
		return (String)eGet(DeploymentPackage.Literals.PAA_SCONFIGURATION__API, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApi(String newApi) {
		eSet(DeploymentPackage.Literals.PAA_SCONFIGURATION__API, newApi);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getVersion() {
		return (String)eGet(DeploymentPackage.Literals.PAA_SCONFIGURATION__VERSION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVersion(String newVersion) {
		eSet(DeploymentPackage.Literals.PAA_SCONFIGURATION__VERSION, newVersion);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEndpoint() {
		return (String)eGet(DeploymentPackage.Literals.PAA_SCONFIGURATION__ENDPOINT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndpoint(String newEndpoint) {
		eSet(DeploymentPackage.Literals.PAA_SCONFIGURATION__ENDPOINT, newEndpoint);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDownloadURL() {
		return (String)eGet(DeploymentPackage.Literals.PAA_SCONFIGURATION__DOWNLOAD_URL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDownloadURL(String newDownloadURL) {
		eSet(DeploymentPackage.Literals.PAA_SCONFIGURATION__DOWNLOAD_URL, newDownloadURL);
	}

} //PaaSConfigurationImpl
