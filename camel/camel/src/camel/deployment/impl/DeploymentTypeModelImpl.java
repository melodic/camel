/**
 */
package camel.deployment.impl;

import camel.deployment.Communication;
import camel.deployment.DeploymentPackage;
import camel.deployment.DeploymentTypeModel;
import camel.deployment.Hosting;
import camel.deployment.LocationCoupling;
import camel.deployment.PaaS;
import camel.deployment.RequirementSet;
import camel.deployment.SoftwareComponent;
import camel.deployment.VM;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.DeploymentTypeModelImpl#getSoftwareComponents <em>Software Components</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentTypeModelImpl#getVms <em>Vms</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentTypeModelImpl#getPaases <em>Paases</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentTypeModelImpl#getCommunications <em>Communications</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentTypeModelImpl#getHostings <em>Hostings</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentTypeModelImpl#getRequirementSets <em>Requirement Sets</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentTypeModelImpl#getGlobalRequirementSet <em>Global Requirement Set</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentTypeModelImpl#getLocationCouplings <em>Location Couplings</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentTypeModelImpl#getContainers <em>Containers</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeploymentTypeModelImpl extends DeploymentModelImpl implements DeploymentTypeModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeploymentTypeModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.DEPLOYMENT_TYPE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SoftwareComponent> getSoftwareComponents() {
		return (EList<SoftwareComponent>)eGet(DeploymentPackage.Literals.DEPLOYMENT_TYPE_MODEL__SOFTWARE_COMPONENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<VM> getVms() {
		return (EList<VM>)eGet(DeploymentPackage.Literals.DEPLOYMENT_TYPE_MODEL__VMS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<PaaS> getPaases() {
		return (EList<PaaS>)eGet(DeploymentPackage.Literals.DEPLOYMENT_TYPE_MODEL__PAASES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Communication> getCommunications() {
		return (EList<Communication>)eGet(DeploymentPackage.Literals.DEPLOYMENT_TYPE_MODEL__COMMUNICATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Hosting> getHostings() {
		return (EList<Hosting>)eGet(DeploymentPackage.Literals.DEPLOYMENT_TYPE_MODEL__HOSTINGS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RequirementSet> getRequirementSets() {
		return (EList<RequirementSet>)eGet(DeploymentPackage.Literals.DEPLOYMENT_TYPE_MODEL__REQUIREMENT_SETS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementSet getGlobalRequirementSet() {
		return (RequirementSet)eGet(DeploymentPackage.Literals.DEPLOYMENT_TYPE_MODEL__GLOBAL_REQUIREMENT_SET, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGlobalRequirementSet(RequirementSet newGlobalRequirementSet) {
		eSet(DeploymentPackage.Literals.DEPLOYMENT_TYPE_MODEL__GLOBAL_REQUIREMENT_SET, newGlobalRequirementSet);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<LocationCoupling> getLocationCouplings() {
		return (EList<LocationCoupling>)eGet(DeploymentPackage.Literals.DEPLOYMENT_TYPE_MODEL__LOCATION_COUPLINGS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<camel.deployment.Container> getContainers() {
		return (EList<camel.deployment.Container>)eGet(DeploymentPackage.Literals.DEPLOYMENT_TYPE_MODEL__CONTAINERS, true);
	}

} //DeploymentTypeModelImpl
