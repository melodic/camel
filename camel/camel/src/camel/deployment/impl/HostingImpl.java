/**
 */
package camel.deployment.impl;

import camel.deployment.Configuration;
import camel.deployment.DeploymentPackage;
import camel.deployment.Hosting;
import camel.deployment.ProvidedHost;
import camel.deployment.RequiredHost;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Hosting</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.HostingImpl#getProvidedHost <em>Provided Host</em>}</li>
 *   <li>{@link camel.deployment.impl.HostingImpl#getRequiredHosts <em>Required Hosts</em>}</li>
 *   <li>{@link camel.deployment.impl.HostingImpl#getProvidedHostConfiguration <em>Provided Host Configuration</em>}</li>
 *   <li>{@link camel.deployment.impl.HostingImpl#getRequiredHostsConfiguration <em>Required Hosts Configuration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HostingImpl extends ComponentRelationImpl implements Hosting {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HostingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.HOSTING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedHost getProvidedHost() {
		return (ProvidedHost)eGet(DeploymentPackage.Literals.HOSTING__PROVIDED_HOST, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvidedHost(ProvidedHost newProvidedHost) {
		eSet(DeploymentPackage.Literals.HOSTING__PROVIDED_HOST, newProvidedHost);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RequiredHost> getRequiredHosts() {
		return (EList<RequiredHost>)eGet(DeploymentPackage.Literals.HOSTING__REQUIRED_HOSTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Configuration getProvidedHostConfiguration() {
		return (Configuration)eGet(DeploymentPackage.Literals.HOSTING__PROVIDED_HOST_CONFIGURATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProvidedHostConfiguration(Configuration newProvidedHostConfiguration) {
		eSet(DeploymentPackage.Literals.HOSTING__PROVIDED_HOST_CONFIGURATION, newProvidedHostConfiguration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Configuration> getRequiredHostsConfiguration() {
		return (EList<Configuration>)eGet(DeploymentPackage.Literals.HOSTING__REQUIRED_HOSTS_CONFIGURATION, true);
	}

} //HostingImpl
