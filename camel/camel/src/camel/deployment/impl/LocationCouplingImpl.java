/**
 */
package camel.deployment.impl;

import camel.deployment.DeploymentPackage;
import camel.deployment.LocationCoupling;
import camel.deployment.LocationCouplingType;
import camel.deployment.SoftwareComponent;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Location Coupling</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.LocationCouplingImpl#getSoftwareComponents <em>Software Components</em>}</li>
 *   <li>{@link camel.deployment.impl.LocationCouplingImpl#getCouplingType <em>Coupling Type</em>}</li>
 *   <li>{@link camel.deployment.impl.LocationCouplingImpl#isRelaxed <em>Relaxed</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocationCouplingImpl extends ComponentRelationImpl implements LocationCoupling {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocationCouplingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.LOCATION_COUPLING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SoftwareComponent> getSoftwareComponents() {
		return (EList<SoftwareComponent>)eGet(DeploymentPackage.Literals.LOCATION_COUPLING__SOFTWARE_COMPONENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationCouplingType getCouplingType() {
		return (LocationCouplingType)eGet(DeploymentPackage.Literals.LOCATION_COUPLING__COUPLING_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCouplingType(LocationCouplingType newCouplingType) {
		eSet(DeploymentPackage.Literals.LOCATION_COUPLING__COUPLING_TYPE, newCouplingType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isRelaxed() {
		return (Boolean)eGet(DeploymentPackage.Literals.LOCATION_COUPLING__RELAXED, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRelaxed(boolean newRelaxed) {
		eSet(DeploymentPackage.Literals.LOCATION_COUPLING__RELAXED, newRelaxed);
	}

} //LocationCouplingImpl
