/**
 */
package camel.deployment.impl;

import camel.data.DataInstance;
import camel.data.DataSourceInstance;

import camel.deployment.DeploymentPackage;
import camel.deployment.RequiredCommunicationInstance;
import camel.deployment.RequiredHostInstance;
import camel.deployment.SoftwareComponentInstance;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Software Component Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.SoftwareComponentInstanceImpl#getRequiredCommunicationInstances <em>Required Communication Instances</em>}</li>
 *   <li>{@link camel.deployment.impl.SoftwareComponentInstanceImpl#getRequiredHostInstance <em>Required Host Instance</em>}</li>
 *   <li>{@link camel.deployment.impl.SoftwareComponentInstanceImpl#getConsumesDataInstance <em>Consumes Data Instance</em>}</li>
 *   <li>{@link camel.deployment.impl.SoftwareComponentInstanceImpl#getGeneratesDataInstance <em>Generates Data Instance</em>}</li>
 *   <li>{@link camel.deployment.impl.SoftwareComponentInstanceImpl#getManagesDataSourceInstance <em>Manages Data Source Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SoftwareComponentInstanceImpl extends ComponentInstanceImpl implements SoftwareComponentInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SoftwareComponentInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.SOFTWARE_COMPONENT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RequiredCommunicationInstance> getRequiredCommunicationInstances() {
		return (EList<RequiredCommunicationInstance>)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT_INSTANCE__REQUIRED_COMMUNICATION_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredHostInstance getRequiredHostInstance() {
		return (RequiredHostInstance)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT_INSTANCE__REQUIRED_HOST_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredHostInstance(RequiredHostInstance newRequiredHostInstance) {
		eSet(DeploymentPackage.Literals.SOFTWARE_COMPONENT_INSTANCE__REQUIRED_HOST_INSTANCE, newRequiredHostInstance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DataInstance> getConsumesDataInstance() {
		return (EList<DataInstance>)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT_INSTANCE__CONSUMES_DATA_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DataInstance> getGeneratesDataInstance() {
		return (EList<DataInstance>)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT_INSTANCE__GENERATES_DATA_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DataSourceInstance> getManagesDataSourceInstance() {
		return (EList<DataSourceInstance>)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT_INSTANCE__MANAGES_DATA_SOURCE_INSTANCE, true);
	}

} //SoftwareComponentInstanceImpl
