/**
 */
package camel.deployment.impl;

import camel.data.Data;
import camel.data.DataSource;

import camel.deployment.DeploymentPackage;
import camel.deployment.RequiredCommunication;
import camel.deployment.RequiredHost;
import camel.deployment.RequirementSet;
import camel.deployment.SoftwareComponent;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Software Component</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.SoftwareComponentImpl#getRequiredCommunications <em>Required Communications</em>}</li>
 *   <li>{@link camel.deployment.impl.SoftwareComponentImpl#getRequiredHost <em>Required Host</em>}</li>
 *   <li>{@link camel.deployment.impl.SoftwareComponentImpl#getRequirementSet <em>Requirement Set</em>}</li>
 *   <li>{@link camel.deployment.impl.SoftwareComponentImpl#getGeneratesData <em>Generates Data</em>}</li>
 *   <li>{@link camel.deployment.impl.SoftwareComponentImpl#getConsumesData <em>Consumes Data</em>}</li>
 *   <li>{@link camel.deployment.impl.SoftwareComponentImpl#getManagesDataSource <em>Manages Data Source</em>}</li>
 *   <li>{@link camel.deployment.impl.SoftwareComponentImpl#isLongLived <em>Long Lived</em>}</li>
 *   <li>{@link camel.deployment.impl.SoftwareComponentImpl#isCoInstanceHosting <em>Co Instance Hosting</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SoftwareComponentImpl extends ComponentImpl implements SoftwareComponent {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SoftwareComponentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.SOFTWARE_COMPONENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RequiredCommunication> getRequiredCommunications() {
		return (EList<RequiredCommunication>)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__REQUIRED_COMMUNICATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredHost getRequiredHost() {
		return (RequiredHost)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__REQUIRED_HOST, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredHost(RequiredHost newRequiredHost) {
		eSet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__REQUIRED_HOST, newRequiredHost);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementSet getRequirementSet() {
		return (RequirementSet)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__REQUIREMENT_SET, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequirementSet(RequirementSet newRequirementSet) {
		eSet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__REQUIREMENT_SET, newRequirementSet);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Data> getGeneratesData() {
		return (EList<Data>)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__GENERATES_DATA, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Data> getConsumesData() {
		return (EList<Data>)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__CONSUMES_DATA, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DataSource> getManagesDataSource() {
		return (EList<DataSource>)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__MANAGES_DATA_SOURCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isLongLived() {
		return (Boolean)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__LONG_LIVED, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLongLived(boolean newLongLived) {
		eSet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__LONG_LIVED, newLongLived);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCoInstanceHosting() {
		return (Boolean)eGet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__CO_INSTANCE_HOSTING, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCoInstanceHosting(boolean newCoInstanceHosting) {
		eSet(DeploymentPackage.Literals.SOFTWARE_COMPONENT__CO_INSTANCE_HOSTING, newCoInstanceHosting);
	}

} //SoftwareComponentImpl
