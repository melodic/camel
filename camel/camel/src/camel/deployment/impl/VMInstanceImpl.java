/**
 */
package camel.deployment.impl;

import camel.core.Attribute;

import camel.deployment.DeploymentPackage;
import camel.deployment.VMInstance;

import camel.location.Location;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VM Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.VMInstanceImpl#getLocation <em>Location</em>}</li>
 *   <li>{@link camel.deployment.impl.VMInstanceImpl#getOs <em>Os</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VMInstanceImpl extends ComponentInstanceImpl implements VMInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VMInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.VM_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Location getLocation() {
		return (Location)eGet(DeploymentPackage.Literals.VM_INSTANCE__LOCATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocation(Location newLocation) {
		eSet(DeploymentPackage.Literals.VM_INSTANCE__LOCATION, newLocation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getOs() {
		return (Attribute)eGet(DeploymentPackage.Literals.VM_INSTANCE__OS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOs(Attribute newOs) {
		eSet(DeploymentPackage.Literals.VM_INSTANCE__OS, newOs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean checkDates(final VMInstance vm) {
		System.out.println("Checking dates for VMInstance: " + vm);
				java.util.Date createdOn = vm.getInstantiatedOn();
				java.util.Date destroyedOn = vm.getDestroyedOn();
				if (createdOn != null && destroyedOn != null && destroyedOn.before(createdOn)) return Boolean.FALSE;
				return Boolean.TRUE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case DeploymentPackage.VM_INSTANCE___CHECK_DATES__VMINSTANCE:
				return checkDates((VMInstance)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //VMInstanceImpl
