/**
 */
package camel.deployment.impl;

import camel.constraint.ConstraintPackage;

import camel.constraint.impl.ConstraintPackageImpl;

import camel.core.CorePackage;

import camel.core.impl.CorePackageImpl;

import camel.data.DataPackage;

import camel.data.impl.DataPackageImpl;

import camel.deployment.BuildConfiguration;
import camel.deployment.ClusterConfiguration;
import camel.deployment.Communication;
import camel.deployment.CommunicationInstance;
import camel.deployment.CommunicationPort;
import camel.deployment.CommunicationPortInstance;
import camel.deployment.Component;
import camel.deployment.ComponentInstance;
import camel.deployment.ComponentRelation;
import camel.deployment.ComponentRelationInstance;
import camel.deployment.Configuration;
import camel.deployment.ContainerInstance;
import camel.deployment.DeploymentFactory;
import camel.deployment.DeploymentInstanceModel;
import camel.deployment.DeploymentModel;
import camel.deployment.DeploymentPackage;
import camel.deployment.DeploymentTypeModel;
import camel.deployment.EventConfiguration;
import camel.deployment.HTTPMethodType;
import camel.deployment.Hosting;
import camel.deployment.HostingInstance;
import camel.deployment.HostingPort;
import camel.deployment.HostingPortInstance;
import camel.deployment.LocationCoupling;
import camel.deployment.LocationCouplingType;
import camel.deployment.PaaS;
import camel.deployment.PaaSConfiguration;
import camel.deployment.PaaSInstance;
import camel.deployment.ProvidedCommunication;
import camel.deployment.ProvidedCommunicationInstance;
import camel.deployment.ProvidedHost;
import camel.deployment.ProvidedHostInstance;
import camel.deployment.RequiredCommunication;
import camel.deployment.RequiredCommunicationInstance;
import camel.deployment.RequiredHost;
import camel.deployment.RequiredHostInstance;
import camel.deployment.RequirementSet;
import camel.deployment.ScriptConfiguration;
import camel.deployment.ServerlessConfiguration;
import camel.deployment.SoftwareComponent;
import camel.deployment.SoftwareComponentInstance;
import camel.deployment.VMInstance;

import camel.deployment.util.DeploymentValidator;

import camel.execution.ExecutionPackage;

import camel.execution.impl.ExecutionPackageImpl;

import camel.location.LocationPackage;

import camel.location.impl.LocationPackageImpl;

import camel.metric.MetricPackage;

import camel.metric.impl.MetricPackageImpl;

import camel.mms.MmsPackage;

import camel.mms.impl.MmsPackageImpl;

import camel.organisation.OrganisationPackage;

import camel.organisation.impl.OrganisationPackageImpl;

import camel.requirement.RequirementPackage;

import camel.requirement.impl.RequirementPackageImpl;

import camel.scalability.ScalabilityPackage;

import camel.scalability.impl.ScalabilityPackageImpl;

import camel.security.SecurityPackage;

import camel.security.impl.SecurityPackageImpl;

import camel.type.TypePackage;

import camel.type.impl.TypePackageImpl;

import camel.unit.UnitPackage;

import camel.unit.impl.UnitPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DeploymentPackageImpl extends EPackageImpl implements DeploymentPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deploymentInstanceModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deploymentModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass deploymentTypeModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass softwareComponentEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vmEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requirementSetEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass configurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass providedCommunicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiredCommunicationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hostingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hostingPortEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass providedHostEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiredHostEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass softwareComponentInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vmInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationPortInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass providedCommunicationInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiredCommunicationInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hostingInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass hostingPortInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass providedHostInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass requiredHostInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scriptConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass clusterConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass locationCouplingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass containerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass containerInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentRelationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass componentRelationInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paaSEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paaSInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paaSConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass serverlessConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass buildConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass eventConfigurationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum locationCouplingTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum httpMethodTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see camel.deployment.DeploymentPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private DeploymentPackageImpl() {
		super(eNS_URI, DeploymentFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link DeploymentPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static DeploymentPackage init() {
		if (isInited) return (DeploymentPackage)EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI);

		// Obtain or create and register package
		DeploymentPackageImpl theDeploymentPackage = (DeploymentPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof DeploymentPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new DeploymentPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		MmsPackageImpl theMmsPackage = (MmsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI) instanceof MmsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI) : MmsPackage.eINSTANCE);
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		ExecutionPackageImpl theExecutionPackage = (ExecutionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI) instanceof ExecutionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI) : ExecutionPackage.eINSTANCE);
		LocationPackageImpl theLocationPackage = (LocationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) instanceof LocationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) : LocationPackage.eINSTANCE);
		MetricPackageImpl theMetricPackage = (MetricPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI) instanceof MetricPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI) : MetricPackage.eINSTANCE);
		OrganisationPackageImpl theOrganisationPackage = (OrganisationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) instanceof OrganisationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) : OrganisationPackage.eINSTANCE);
		RequirementPackageImpl theRequirementPackage = (RequirementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) instanceof RequirementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) : RequirementPackage.eINSTANCE);
		ScalabilityPackageImpl theScalabilityPackage = (ScalabilityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI) instanceof ScalabilityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI) : ScalabilityPackage.eINSTANCE);
		SecurityPackageImpl theSecurityPackage = (SecurityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) instanceof SecurityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) : SecurityPackage.eINSTANCE);
		TypePackageImpl theTypePackage = (TypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI) instanceof TypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI) : TypePackage.eINSTANCE);
		UnitPackageImpl theUnitPackage = (UnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) instanceof UnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) : UnitPackage.eINSTANCE);
		ConstraintPackageImpl theConstraintPackage = (ConstraintPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) instanceof ConstraintPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) : ConstraintPackage.eINSTANCE);
		DataPackageImpl theDataPackage = (DataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) instanceof DataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) : DataPackage.eINSTANCE);

		// Create package meta-data objects
		theDeploymentPackage.createPackageContents();
		theMmsPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theExecutionPackage.createPackageContents();
		theLocationPackage.createPackageContents();
		theMetricPackage.createPackageContents();
		theOrganisationPackage.createPackageContents();
		theRequirementPackage.createPackageContents();
		theScalabilityPackage.createPackageContents();
		theSecurityPackage.createPackageContents();
		theTypePackage.createPackageContents();
		theUnitPackage.createPackageContents();
		theConstraintPackage.createPackageContents();
		theDataPackage.createPackageContents();

		// Initialize created meta-data
		theDeploymentPackage.initializePackageContents();
		theMmsPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theExecutionPackage.initializePackageContents();
		theLocationPackage.initializePackageContents();
		theMetricPackage.initializePackageContents();
		theOrganisationPackage.initializePackageContents();
		theRequirementPackage.initializePackageContents();
		theScalabilityPackage.initializePackageContents();
		theSecurityPackage.initializePackageContents();
		theTypePackage.initializePackageContents();
		theUnitPackage.initializePackageContents();
		theConstraintPackage.initializePackageContents();
		theDataPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theDeploymentPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return DeploymentValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theDeploymentPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(DeploymentPackage.eNS_URI, theDeploymentPackage);
		return theDeploymentPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeploymentInstanceModel() {
		return deploymentInstanceModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentInstanceModel_SoftwareComponentInstances() {
		return (EReference)deploymentInstanceModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentInstanceModel_VmInstances() {
		return (EReference)deploymentInstanceModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentInstanceModel_CommunicationInstances() {
		return (EReference)deploymentInstanceModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentInstanceModel_HostingInstances() {
		return (EReference)deploymentInstanceModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentInstanceModel_Type() {
		return (EReference)deploymentInstanceModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentInstanceModel_ContainerInstances() {
		return (EReference)deploymentInstanceModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentInstanceModel_PaasInstances() {
		return (EReference)deploymentInstanceModelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeploymentModel() {
		return deploymentModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDeploymentTypeModel() {
		return deploymentTypeModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentTypeModel_SoftwareComponents() {
		return (EReference)deploymentTypeModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentTypeModel_Vms() {
		return (EReference)deploymentTypeModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentTypeModel_Paases() {
		return (EReference)deploymentTypeModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentTypeModel_Communications() {
		return (EReference)deploymentTypeModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentTypeModel_Hostings() {
		return (EReference)deploymentTypeModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentTypeModel_RequirementSets() {
		return (EReference)deploymentTypeModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentTypeModel_GlobalRequirementSet() {
		return (EReference)deploymentTypeModelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentTypeModel_LocationCouplings() {
		return (EReference)deploymentTypeModelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDeploymentTypeModel_Containers() {
		return (EReference)deploymentTypeModelEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponent() {
		return componentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponent_ProvidedCommunications() {
		return (EReference)componentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponent_ProvidedHosts() {
		return (EReference)componentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponent_Configurations() {
		return (EReference)componentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSoftwareComponent() {
		return softwareComponentEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponent_RequiredCommunications() {
		return (EReference)softwareComponentEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponent_RequiredHost() {
		return (EReference)softwareComponentEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponent_RequirementSet() {
		return (EReference)softwareComponentEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponent_GeneratesData() {
		return (EReference)softwareComponentEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponent_ConsumesData() {
		return (EReference)softwareComponentEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponent_ManagesDataSource() {
		return (EReference)softwareComponentEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoftwareComponent_LongLived() {
		return (EAttribute)softwareComponentEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSoftwareComponent_CoInstanceHosting() {
		return (EAttribute)softwareComponentEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVM() {
		return vmEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequirementSet() {
		return requirementSetEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirementSet_ResourceRequirement() {
		return (EReference)requirementSetEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirementSet_PaasRequirement() {
		return (EReference)requirementSetEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirementSet_LocationRequirement() {
		return (EReference)requirementSetEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirementSet_ProviderRequirement() {
		return (EReference)requirementSetEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirementSet_VerticalScaleRequirement() {
		return (EReference)requirementSetEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirementSet_HorizontalScaleRequirement() {
		return (EReference)requirementSetEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirementSet_SecurityRequirement() {
		return (EReference)requirementSetEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirementSet_OsRequirement() {
		return (EReference)requirementSetEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRequirementSet_ImageRequirement() {
		return (EReference)requirementSetEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getConfiguration() {
		return configurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunication() {
		return communicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunication_ProvidedCommunication() {
		return (EReference)communicationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunication_RequiredCommunication() {
		return (EReference)communicationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunication_ProvidedPortConfiguration() {
		return (EReference)communicationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunication_RequiredPortConfiguration() {
		return (EReference)communicationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunicationPort() {
		return communicationPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCommunicationPort_PortNumber() {
		return (EAttribute)communicationPortEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCommunicationPort_LowPortNumber() {
		return (EAttribute)communicationPortEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCommunicationPort_HighPortNumber() {
		return (EAttribute)communicationPortEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProvidedCommunication() {
		return providedCommunicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequiredCommunication() {
		return requiredCommunicationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRequiredCommunication_IsMandatory() {
		return (EAttribute)requiredCommunicationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHosting() {
		return hostingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHosting_ProvidedHost() {
		return (EReference)hostingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHosting_RequiredHosts() {
		return (EReference)hostingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHosting_ProvidedHostConfiguration() {
		return (EReference)hostingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHosting_RequiredHostsConfiguration() {
		return (EReference)hostingEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHostingPort() {
		return hostingPortEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProvidedHost() {
		return providedHostEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequiredHost() {
		return requiredHostEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentInstance() {
		return componentInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstance_Type() {
		return (EReference)componentInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstance_ProvidedCommunicationInstances() {
		return (EReference)componentInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getComponentInstance_ProvidedHostInstances() {
		return (EReference)componentInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComponentInstance_InstantiatedOn() {
		return (EAttribute)componentInstanceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getComponentInstance_DestroyedOn() {
		return (EAttribute)componentInstanceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSoftwareComponentInstance() {
		return softwareComponentInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponentInstance_RequiredCommunicationInstances() {
		return (EReference)softwareComponentInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponentInstance_RequiredHostInstance() {
		return (EReference)softwareComponentInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponentInstance_ConsumesDataInstance() {
		return (EReference)softwareComponentInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponentInstance_GeneratesDataInstance() {
		return (EReference)softwareComponentInstanceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponentInstance_ManagesDataSourceInstance() {
		return (EReference)softwareComponentInstanceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVMInstance() {
		return vmInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVMInstance_Location() {
		return (EReference)vmInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVMInstance_Os() {
		return (EReference)vmInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getVMInstance__CheckDates__VMInstance() {
		return vmInstanceEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunicationInstance() {
		return communicationInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationInstance_Type() {
		return (EReference)communicationInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationInstance_ProvidedCommunicationInstance() {
		return (EReference)communicationInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationInstance_RequiredCommunicationInstance() {
		return (EReference)communicationInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunicationPortInstance() {
		return communicationPortInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationPortInstance_Type() {
		return (EReference)communicationPortInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProvidedCommunicationInstance() {
		return providedCommunicationInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequiredCommunicationInstance() {
		return requiredCommunicationInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHostingInstance() {
		return hostingInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHostingInstance_Type() {
		return (EReference)hostingInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHostingInstance_ProvidedHostInstance() {
		return (EReference)hostingInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHostingInstance_RequiredHostInstances() {
		return (EReference)hostingInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHostingPortInstance() {
		return hostingPortInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHostingPortInstance_Type() {
		return (EReference)hostingPortInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getProvidedHostInstance() {
		return providedHostInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRequiredHostInstance() {
		return requiredHostInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getScriptConfiguration() {
		return scriptConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScriptConfiguration_UploadCommand() {
		return (EAttribute)scriptConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScriptConfiguration_InstallCommand() {
		return (EAttribute)scriptConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScriptConfiguration_StartCommand() {
		return (EAttribute)scriptConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScriptConfiguration_StopCommand() {
		return (EAttribute)scriptConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScriptConfiguration_DownloadCommand() {
		return (EAttribute)scriptConfigurationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScriptConfiguration_ConfigureCommand() {
		return (EAttribute)scriptConfigurationEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScriptConfiguration_UpdateCommand() {
		return (EAttribute)scriptConfigurationEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScriptConfiguration_DevopsTool() {
		return (EAttribute)scriptConfigurationEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScriptConfiguration_Os() {
		return (EAttribute)scriptConfigurationEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getScriptConfiguration_ImageId() {
		return (EAttribute)scriptConfigurationEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getClusterConfiguration() {
		return clusterConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getClusterConfiguration_DownloadURL() {
		return (EAttribute)clusterConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getClusterConfiguration_ConfigParameters() {
		return (EReference)clusterConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getLocationCoupling() {
		return locationCouplingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getLocationCoupling_SoftwareComponents() {
		return (EReference)locationCouplingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLocationCoupling_CouplingType() {
		return (EAttribute)locationCouplingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getLocationCoupling_Relaxed() {
		return (EAttribute)locationCouplingEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContainer() {
		return containerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContainer_RequiredCommunications() {
		return (EReference)containerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContainer_RequiredHost() {
		return (EReference)containerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContainerInstance() {
		return containerInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContainerInstance_RequiredCommunicationInstances() {
		return (EReference)containerInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContainerInstance_RequiredHostInstance() {
		return (EReference)containerInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentRelation() {
		return componentRelationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getComponentRelationInstance() {
		return componentRelationInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPaaS() {
		return paaSEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPaaSInstance() {
		return paaSInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPaaSConfiguration() {
		return paaSConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPaaSConfiguration_Api() {
		return (EAttribute)paaSConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPaaSConfiguration_Version() {
		return (EAttribute)paaSConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPaaSConfiguration_Endpoint() {
		return (EAttribute)paaSConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getPaaSConfiguration_DownloadURL() {
		return (EAttribute)paaSConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getServerlessConfiguration() {
		return serverlessConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServerlessConfiguration_BinaryCodeURL() {
		return (EAttribute)serverlessConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getServerlessConfiguration_ContinuousDeployment() {
		return (EAttribute)serverlessConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServerlessConfiguration_EnvironmentConfigParams() {
		return (EReference)serverlessConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServerlessConfiguration_BuildConfiguration() {
		return (EReference)serverlessConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getServerlessConfiguration_EventConfiguration() {
		return (EReference)serverlessConfigurationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getBuildConfiguration() {
		return buildConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBuildConfiguration_ArtifactId() {
		return (EAttribute)buildConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBuildConfiguration_BuildFramework() {
		return (EAttribute)buildConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBuildConfiguration_SourceCodeURL() {
		return (EAttribute)buildConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBuildConfiguration_Include() {
		return (EAttribute)buildConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getBuildConfiguration_Exclude() {
		return (EAttribute)buildConfigurationEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getEventConfiguration() {
		return eventConfigurationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventConfiguration_HttpMethodName() {
		return (EAttribute)eventConfigurationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getEventConfiguration_HttpMethodType() {
		return (EAttribute)eventConfigurationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventConfiguration_ExecutionSchedule() {
		return (EReference)eventConfigurationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getEventConfiguration_ScheduledExecutionConfig() {
		return (EReference)eventConfigurationEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getLocationCouplingType() {
		return locationCouplingTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getHTTPMethodType() {
		return httpMethodTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentFactory getDeploymentFactory() {
		return (DeploymentFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		deploymentInstanceModelEClass = createEClass(DEPLOYMENT_INSTANCE_MODEL);
		createEReference(deploymentInstanceModelEClass, DEPLOYMENT_INSTANCE_MODEL__SOFTWARE_COMPONENT_INSTANCES);
		createEReference(deploymentInstanceModelEClass, DEPLOYMENT_INSTANCE_MODEL__VM_INSTANCES);
		createEReference(deploymentInstanceModelEClass, DEPLOYMENT_INSTANCE_MODEL__COMMUNICATION_INSTANCES);
		createEReference(deploymentInstanceModelEClass, DEPLOYMENT_INSTANCE_MODEL__HOSTING_INSTANCES);
		createEReference(deploymentInstanceModelEClass, DEPLOYMENT_INSTANCE_MODEL__TYPE);
		createEReference(deploymentInstanceModelEClass, DEPLOYMENT_INSTANCE_MODEL__CONTAINER_INSTANCES);
		createEReference(deploymentInstanceModelEClass, DEPLOYMENT_INSTANCE_MODEL__PAAS_INSTANCES);

		deploymentModelEClass = createEClass(DEPLOYMENT_MODEL);

		deploymentTypeModelEClass = createEClass(DEPLOYMENT_TYPE_MODEL);
		createEReference(deploymentTypeModelEClass, DEPLOYMENT_TYPE_MODEL__SOFTWARE_COMPONENTS);
		createEReference(deploymentTypeModelEClass, DEPLOYMENT_TYPE_MODEL__VMS);
		createEReference(deploymentTypeModelEClass, DEPLOYMENT_TYPE_MODEL__PAASES);
		createEReference(deploymentTypeModelEClass, DEPLOYMENT_TYPE_MODEL__COMMUNICATIONS);
		createEReference(deploymentTypeModelEClass, DEPLOYMENT_TYPE_MODEL__HOSTINGS);
		createEReference(deploymentTypeModelEClass, DEPLOYMENT_TYPE_MODEL__REQUIREMENT_SETS);
		createEReference(deploymentTypeModelEClass, DEPLOYMENT_TYPE_MODEL__GLOBAL_REQUIREMENT_SET);
		createEReference(deploymentTypeModelEClass, DEPLOYMENT_TYPE_MODEL__LOCATION_COUPLINGS);
		createEReference(deploymentTypeModelEClass, DEPLOYMENT_TYPE_MODEL__CONTAINERS);

		componentEClass = createEClass(COMPONENT);
		createEReference(componentEClass, COMPONENT__PROVIDED_COMMUNICATIONS);
		createEReference(componentEClass, COMPONENT__PROVIDED_HOSTS);
		createEReference(componentEClass, COMPONENT__CONFIGURATIONS);

		softwareComponentEClass = createEClass(SOFTWARE_COMPONENT);
		createEReference(softwareComponentEClass, SOFTWARE_COMPONENT__REQUIRED_COMMUNICATIONS);
		createEReference(softwareComponentEClass, SOFTWARE_COMPONENT__REQUIRED_HOST);
		createEReference(softwareComponentEClass, SOFTWARE_COMPONENT__REQUIREMENT_SET);
		createEReference(softwareComponentEClass, SOFTWARE_COMPONENT__GENERATES_DATA);
		createEReference(softwareComponentEClass, SOFTWARE_COMPONENT__CONSUMES_DATA);
		createEReference(softwareComponentEClass, SOFTWARE_COMPONENT__MANAGES_DATA_SOURCE);
		createEAttribute(softwareComponentEClass, SOFTWARE_COMPONENT__LONG_LIVED);
		createEAttribute(softwareComponentEClass, SOFTWARE_COMPONENT__CO_INSTANCE_HOSTING);

		vmEClass = createEClass(VM);

		requirementSetEClass = createEClass(REQUIREMENT_SET);
		createEReference(requirementSetEClass, REQUIREMENT_SET__RESOURCE_REQUIREMENT);
		createEReference(requirementSetEClass, REQUIREMENT_SET__PAAS_REQUIREMENT);
		createEReference(requirementSetEClass, REQUIREMENT_SET__LOCATION_REQUIREMENT);
		createEReference(requirementSetEClass, REQUIREMENT_SET__PROVIDER_REQUIREMENT);
		createEReference(requirementSetEClass, REQUIREMENT_SET__VERTICAL_SCALE_REQUIREMENT);
		createEReference(requirementSetEClass, REQUIREMENT_SET__HORIZONTAL_SCALE_REQUIREMENT);
		createEReference(requirementSetEClass, REQUIREMENT_SET__SECURITY_REQUIREMENT);
		createEReference(requirementSetEClass, REQUIREMENT_SET__OS_REQUIREMENT);
		createEReference(requirementSetEClass, REQUIREMENT_SET__IMAGE_REQUIREMENT);

		configurationEClass = createEClass(CONFIGURATION);

		communicationEClass = createEClass(COMMUNICATION);
		createEReference(communicationEClass, COMMUNICATION__PROVIDED_COMMUNICATION);
		createEReference(communicationEClass, COMMUNICATION__REQUIRED_COMMUNICATION);
		createEReference(communicationEClass, COMMUNICATION__PROVIDED_PORT_CONFIGURATION);
		createEReference(communicationEClass, COMMUNICATION__REQUIRED_PORT_CONFIGURATION);

		communicationPortEClass = createEClass(COMMUNICATION_PORT);
		createEAttribute(communicationPortEClass, COMMUNICATION_PORT__PORT_NUMBER);
		createEAttribute(communicationPortEClass, COMMUNICATION_PORT__LOW_PORT_NUMBER);
		createEAttribute(communicationPortEClass, COMMUNICATION_PORT__HIGH_PORT_NUMBER);

		providedCommunicationEClass = createEClass(PROVIDED_COMMUNICATION);

		requiredCommunicationEClass = createEClass(REQUIRED_COMMUNICATION);
		createEAttribute(requiredCommunicationEClass, REQUIRED_COMMUNICATION__IS_MANDATORY);

		hostingEClass = createEClass(HOSTING);
		createEReference(hostingEClass, HOSTING__PROVIDED_HOST);
		createEReference(hostingEClass, HOSTING__REQUIRED_HOSTS);
		createEReference(hostingEClass, HOSTING__PROVIDED_HOST_CONFIGURATION);
		createEReference(hostingEClass, HOSTING__REQUIRED_HOSTS_CONFIGURATION);

		hostingPortEClass = createEClass(HOSTING_PORT);

		providedHostEClass = createEClass(PROVIDED_HOST);

		requiredHostEClass = createEClass(REQUIRED_HOST);

		componentInstanceEClass = createEClass(COMPONENT_INSTANCE);
		createEReference(componentInstanceEClass, COMPONENT_INSTANCE__TYPE);
		createEReference(componentInstanceEClass, COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES);
		createEReference(componentInstanceEClass, COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES);
		createEAttribute(componentInstanceEClass, COMPONENT_INSTANCE__INSTANTIATED_ON);
		createEAttribute(componentInstanceEClass, COMPONENT_INSTANCE__DESTROYED_ON);

		softwareComponentInstanceEClass = createEClass(SOFTWARE_COMPONENT_INSTANCE);
		createEReference(softwareComponentInstanceEClass, SOFTWARE_COMPONENT_INSTANCE__REQUIRED_COMMUNICATION_INSTANCES);
		createEReference(softwareComponentInstanceEClass, SOFTWARE_COMPONENT_INSTANCE__REQUIRED_HOST_INSTANCE);
		createEReference(softwareComponentInstanceEClass, SOFTWARE_COMPONENT_INSTANCE__CONSUMES_DATA_INSTANCE);
		createEReference(softwareComponentInstanceEClass, SOFTWARE_COMPONENT_INSTANCE__GENERATES_DATA_INSTANCE);
		createEReference(softwareComponentInstanceEClass, SOFTWARE_COMPONENT_INSTANCE__MANAGES_DATA_SOURCE_INSTANCE);

		vmInstanceEClass = createEClass(VM_INSTANCE);
		createEReference(vmInstanceEClass, VM_INSTANCE__LOCATION);
		createEReference(vmInstanceEClass, VM_INSTANCE__OS);
		createEOperation(vmInstanceEClass, VM_INSTANCE___CHECK_DATES__VMINSTANCE);

		communicationInstanceEClass = createEClass(COMMUNICATION_INSTANCE);
		createEReference(communicationInstanceEClass, COMMUNICATION_INSTANCE__TYPE);
		createEReference(communicationInstanceEClass, COMMUNICATION_INSTANCE__PROVIDED_COMMUNICATION_INSTANCE);
		createEReference(communicationInstanceEClass, COMMUNICATION_INSTANCE__REQUIRED_COMMUNICATION_INSTANCE);

		communicationPortInstanceEClass = createEClass(COMMUNICATION_PORT_INSTANCE);
		createEReference(communicationPortInstanceEClass, COMMUNICATION_PORT_INSTANCE__TYPE);

		providedCommunicationInstanceEClass = createEClass(PROVIDED_COMMUNICATION_INSTANCE);

		requiredCommunicationInstanceEClass = createEClass(REQUIRED_COMMUNICATION_INSTANCE);

		hostingInstanceEClass = createEClass(HOSTING_INSTANCE);
		createEReference(hostingInstanceEClass, HOSTING_INSTANCE__TYPE);
		createEReference(hostingInstanceEClass, HOSTING_INSTANCE__PROVIDED_HOST_INSTANCE);
		createEReference(hostingInstanceEClass, HOSTING_INSTANCE__REQUIRED_HOST_INSTANCES);

		hostingPortInstanceEClass = createEClass(HOSTING_PORT_INSTANCE);
		createEReference(hostingPortInstanceEClass, HOSTING_PORT_INSTANCE__TYPE);

		providedHostInstanceEClass = createEClass(PROVIDED_HOST_INSTANCE);

		requiredHostInstanceEClass = createEClass(REQUIRED_HOST_INSTANCE);

		scriptConfigurationEClass = createEClass(SCRIPT_CONFIGURATION);
		createEAttribute(scriptConfigurationEClass, SCRIPT_CONFIGURATION__UPLOAD_COMMAND);
		createEAttribute(scriptConfigurationEClass, SCRIPT_CONFIGURATION__INSTALL_COMMAND);
		createEAttribute(scriptConfigurationEClass, SCRIPT_CONFIGURATION__START_COMMAND);
		createEAttribute(scriptConfigurationEClass, SCRIPT_CONFIGURATION__STOP_COMMAND);
		createEAttribute(scriptConfigurationEClass, SCRIPT_CONFIGURATION__DOWNLOAD_COMMAND);
		createEAttribute(scriptConfigurationEClass, SCRIPT_CONFIGURATION__CONFIGURE_COMMAND);
		createEAttribute(scriptConfigurationEClass, SCRIPT_CONFIGURATION__UPDATE_COMMAND);
		createEAttribute(scriptConfigurationEClass, SCRIPT_CONFIGURATION__DEVOPS_TOOL);
		createEAttribute(scriptConfigurationEClass, SCRIPT_CONFIGURATION__OS);
		createEAttribute(scriptConfigurationEClass, SCRIPT_CONFIGURATION__IMAGE_ID);

		clusterConfigurationEClass = createEClass(CLUSTER_CONFIGURATION);
		createEAttribute(clusterConfigurationEClass, CLUSTER_CONFIGURATION__DOWNLOAD_URL);
		createEReference(clusterConfigurationEClass, CLUSTER_CONFIGURATION__CONFIG_PARAMETERS);

		locationCouplingEClass = createEClass(LOCATION_COUPLING);
		createEReference(locationCouplingEClass, LOCATION_COUPLING__SOFTWARE_COMPONENTS);
		createEAttribute(locationCouplingEClass, LOCATION_COUPLING__COUPLING_TYPE);
		createEAttribute(locationCouplingEClass, LOCATION_COUPLING__RELAXED);

		containerEClass = createEClass(CONTAINER);
		createEReference(containerEClass, CONTAINER__REQUIRED_COMMUNICATIONS);
		createEReference(containerEClass, CONTAINER__REQUIRED_HOST);

		containerInstanceEClass = createEClass(CONTAINER_INSTANCE);
		createEReference(containerInstanceEClass, CONTAINER_INSTANCE__REQUIRED_COMMUNICATION_INSTANCES);
		createEReference(containerInstanceEClass, CONTAINER_INSTANCE__REQUIRED_HOST_INSTANCE);

		componentRelationEClass = createEClass(COMPONENT_RELATION);

		componentRelationInstanceEClass = createEClass(COMPONENT_RELATION_INSTANCE);

		paaSEClass = createEClass(PAA_S);

		paaSInstanceEClass = createEClass(PAA_SINSTANCE);

		paaSConfigurationEClass = createEClass(PAA_SCONFIGURATION);
		createEAttribute(paaSConfigurationEClass, PAA_SCONFIGURATION__API);
		createEAttribute(paaSConfigurationEClass, PAA_SCONFIGURATION__VERSION);
		createEAttribute(paaSConfigurationEClass, PAA_SCONFIGURATION__ENDPOINT);
		createEAttribute(paaSConfigurationEClass, PAA_SCONFIGURATION__DOWNLOAD_URL);

		serverlessConfigurationEClass = createEClass(SERVERLESS_CONFIGURATION);
		createEAttribute(serverlessConfigurationEClass, SERVERLESS_CONFIGURATION__BINARY_CODE_URL);
		createEAttribute(serverlessConfigurationEClass, SERVERLESS_CONFIGURATION__CONTINUOUS_DEPLOYMENT);
		createEReference(serverlessConfigurationEClass, SERVERLESS_CONFIGURATION__ENVIRONMENT_CONFIG_PARAMS);
		createEReference(serverlessConfigurationEClass, SERVERLESS_CONFIGURATION__BUILD_CONFIGURATION);
		createEReference(serverlessConfigurationEClass, SERVERLESS_CONFIGURATION__EVENT_CONFIGURATION);

		buildConfigurationEClass = createEClass(BUILD_CONFIGURATION);
		createEAttribute(buildConfigurationEClass, BUILD_CONFIGURATION__ARTIFACT_ID);
		createEAttribute(buildConfigurationEClass, BUILD_CONFIGURATION__BUILD_FRAMEWORK);
		createEAttribute(buildConfigurationEClass, BUILD_CONFIGURATION__SOURCE_CODE_URL);
		createEAttribute(buildConfigurationEClass, BUILD_CONFIGURATION__INCLUDE);
		createEAttribute(buildConfigurationEClass, BUILD_CONFIGURATION__EXCLUDE);

		eventConfigurationEClass = createEClass(EVENT_CONFIGURATION);
		createEAttribute(eventConfigurationEClass, EVENT_CONFIGURATION__HTTP_METHOD_NAME);
		createEAttribute(eventConfigurationEClass, EVENT_CONFIGURATION__HTTP_METHOD_TYPE);
		createEReference(eventConfigurationEClass, EVENT_CONFIGURATION__EXECUTION_SCHEDULE);
		createEReference(eventConfigurationEClass, EVENT_CONFIGURATION__SCHEDULED_EXECUTION_CONFIG);

		// Create enums
		locationCouplingTypeEEnum = createEEnum(LOCATION_COUPLING_TYPE);
		httpMethodTypeEEnum = createEEnum(HTTP_METHOD_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		DataPackage theDataPackage = (DataPackage)EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI);
		RequirementPackage theRequirementPackage = (RequirementPackage)EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI);
		LocationPackage theLocationPackage = (LocationPackage)EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI);
		MetricPackage theMetricPackage = (MetricPackage)EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		deploymentInstanceModelEClass.getESuperTypes().add(this.getDeploymentModel());
		deploymentModelEClass.getESuperTypes().add(theCorePackage.getModel());
		deploymentTypeModelEClass.getESuperTypes().add(this.getDeploymentModel());
		componentEClass.getESuperTypes().add(theCorePackage.getFeature());
		softwareComponentEClass.getESuperTypes().add(this.getComponent());
		vmEClass.getESuperTypes().add(this.getComponent());
		requirementSetEClass.getESuperTypes().add(theCorePackage.getFeature());
		configurationEClass.getESuperTypes().add(theCorePackage.getFeature());
		communicationEClass.getESuperTypes().add(this.getComponentRelation());
		communicationPortEClass.getESuperTypes().add(theCorePackage.getFeature());
		providedCommunicationEClass.getESuperTypes().add(this.getCommunicationPort());
		requiredCommunicationEClass.getESuperTypes().add(this.getCommunicationPort());
		hostingEClass.getESuperTypes().add(this.getComponentRelation());
		hostingPortEClass.getESuperTypes().add(theCorePackage.getFeature());
		providedHostEClass.getESuperTypes().add(this.getHostingPort());
		requiredHostEClass.getESuperTypes().add(this.getHostingPort());
		componentInstanceEClass.getESuperTypes().add(theCorePackage.getFeature());
		softwareComponentInstanceEClass.getESuperTypes().add(this.getComponentInstance());
		vmInstanceEClass.getESuperTypes().add(this.getComponentInstance());
		communicationInstanceEClass.getESuperTypes().add(this.getComponentRelationInstance());
		communicationPortInstanceEClass.getESuperTypes().add(theCorePackage.getFeature());
		providedCommunicationInstanceEClass.getESuperTypes().add(this.getCommunicationPortInstance());
		requiredCommunicationInstanceEClass.getESuperTypes().add(this.getCommunicationPortInstance());
		hostingInstanceEClass.getESuperTypes().add(this.getComponentRelationInstance());
		hostingPortInstanceEClass.getESuperTypes().add(theCorePackage.getFeature());
		providedHostInstanceEClass.getESuperTypes().add(this.getHostingPortInstance());
		requiredHostInstanceEClass.getESuperTypes().add(this.getHostingPortInstance());
		scriptConfigurationEClass.getESuperTypes().add(this.getConfiguration());
		clusterConfigurationEClass.getESuperTypes().add(this.getConfiguration());
		locationCouplingEClass.getESuperTypes().add(this.getComponentRelation());
		containerEClass.getESuperTypes().add(this.getComponent());
		containerInstanceEClass.getESuperTypes().add(this.getComponentInstance());
		componentRelationEClass.getESuperTypes().add(theCorePackage.getFeature());
		componentRelationInstanceEClass.getESuperTypes().add(theCorePackage.getFeature());
		paaSEClass.getESuperTypes().add(this.getComponent());
		paaSInstanceEClass.getESuperTypes().add(this.getComponentInstance());
		paaSConfigurationEClass.getESuperTypes().add(this.getConfiguration());
		serverlessConfigurationEClass.getESuperTypes().add(this.getConfiguration());
		buildConfigurationEClass.getESuperTypes().add(theCorePackage.getFeature());
		eventConfigurationEClass.getESuperTypes().add(theCorePackage.getFeature());

		// Initialize classes, features, and operations; add parameters
		initEClass(deploymentInstanceModelEClass, DeploymentInstanceModel.class, "DeploymentInstanceModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeploymentInstanceModel_SoftwareComponentInstances(), this.getSoftwareComponentInstance(), null, "softwareComponentInstances", null, 0, -1, DeploymentInstanceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentInstanceModel_VmInstances(), this.getVMInstance(), null, "vmInstances", null, 0, -1, DeploymentInstanceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentInstanceModel_CommunicationInstances(), this.getCommunicationInstance(), null, "communicationInstances", null, 0, -1, DeploymentInstanceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentInstanceModel_HostingInstances(), this.getHostingInstance(), null, "hostingInstances", null, 0, -1, DeploymentInstanceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentInstanceModel_Type(), this.getDeploymentTypeModel(), null, "type", null, 1, 1, DeploymentInstanceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentInstanceModel_ContainerInstances(), this.getContainerInstance(), null, "containerInstances", null, 0, -1, DeploymentInstanceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentInstanceModel_PaasInstances(), this.getPaaSInstance(), null, "paasInstances", null, 0, -1, DeploymentInstanceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(deploymentModelEClass, DeploymentModel.class, "DeploymentModel", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(deploymentTypeModelEClass, DeploymentTypeModel.class, "DeploymentTypeModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDeploymentTypeModel_SoftwareComponents(), this.getSoftwareComponent(), null, "softwareComponents", null, 0, -1, DeploymentTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentTypeModel_Vms(), this.getVM(), null, "vms", null, 0, -1, DeploymentTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentTypeModel_Paases(), this.getPaaS(), null, "paases", null, 0, -1, DeploymentTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentTypeModel_Communications(), this.getCommunication(), null, "communications", null, 0, -1, DeploymentTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentTypeModel_Hostings(), this.getHosting(), null, "hostings", null, 0, -1, DeploymentTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentTypeModel_RequirementSets(), this.getRequirementSet(), null, "requirementSets", null, 0, -1, DeploymentTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentTypeModel_GlobalRequirementSet(), this.getRequirementSet(), null, "globalRequirementSet", null, 0, 1, DeploymentTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentTypeModel_LocationCouplings(), this.getLocationCoupling(), null, "locationCouplings", null, 0, -1, DeploymentTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDeploymentTypeModel_Containers(), this.getContainer(), null, "containers", null, 0, -1, DeploymentTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentEClass, Component.class, "Component", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponent_ProvidedCommunications(), this.getProvidedCommunication(), null, "providedCommunications", null, 0, -1, Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponent_ProvidedHosts(), this.getProvidedHost(), null, "providedHosts", null, 0, -1, Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponent_Configurations(), this.getConfiguration(), null, "configurations", null, 0, -1, Component.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(softwareComponentEClass, SoftwareComponent.class, "SoftwareComponent", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSoftwareComponent_RequiredCommunications(), this.getRequiredCommunication(), null, "requiredCommunications", null, 0, -1, SoftwareComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSoftwareComponent_RequiredHost(), this.getRequiredHost(), null, "requiredHost", null, 0, 1, SoftwareComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSoftwareComponent_RequirementSet(), this.getRequirementSet(), null, "requirementSet", null, 0, 1, SoftwareComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSoftwareComponent_GeneratesData(), theDataPackage.getData(), null, "generatesData", null, 0, -1, SoftwareComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSoftwareComponent_ConsumesData(), theDataPackage.getData(), null, "consumesData", null, 0, -1, SoftwareComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSoftwareComponent_ManagesDataSource(), theDataPackage.getDataSource(), theDataPackage.getDataSource_Component(), "managesDataSource", null, 0, -1, SoftwareComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSoftwareComponent_LongLived(), ecorePackage.getEBoolean(), "longLived", "false", 1, 1, SoftwareComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSoftwareComponent_CoInstanceHosting(), ecorePackage.getEBoolean(), "coInstanceHosting", "false", 1, 1, SoftwareComponent.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vmEClass, camel.deployment.VM.class, "VM", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(requirementSetEClass, RequirementSet.class, "RequirementSet", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRequirementSet_ResourceRequirement(), theRequirementPackage.getResourceRequirement(), null, "resourceRequirement", null, 0, 1, RequirementSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequirementSet_PaasRequirement(), theRequirementPackage.getPaaSRequirement(), null, "paasRequirement", null, 0, 1, RequirementSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequirementSet_LocationRequirement(), theRequirementPackage.getLocationRequirement(), null, "locationRequirement", null, 0, 1, RequirementSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequirementSet_ProviderRequirement(), theRequirementPackage.getProviderRequirement(), null, "providerRequirement", null, 0, 1, RequirementSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequirementSet_VerticalScaleRequirement(), theRequirementPackage.getVerticalScaleRequirement(), null, "verticalScaleRequirement", null, 0, 1, RequirementSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequirementSet_HorizontalScaleRequirement(), theRequirementPackage.getHorizontalScaleRequirement(), null, "horizontalScaleRequirement", null, 0, 1, RequirementSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequirementSet_SecurityRequirement(), theRequirementPackage.getSecurityRequirement(), null, "securityRequirement", null, 0, 1, RequirementSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequirementSet_OsRequirement(), theRequirementPackage.getOSRequirement(), null, "osRequirement", null, 0, 1, RequirementSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getRequirementSet_ImageRequirement(), theRequirementPackage.getImageRequirement(), null, "imageRequirement", null, 0, 1, RequirementSet.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(configurationEClass, Configuration.class, "Configuration", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(communicationEClass, Communication.class, "Communication", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommunication_ProvidedCommunication(), this.getProvidedCommunication(), null, "providedCommunication", null, 1, 1, Communication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunication_RequiredCommunication(), this.getRequiredCommunication(), null, "requiredCommunication", null, 1, 1, Communication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunication_ProvidedPortConfiguration(), this.getConfiguration(), null, "providedPortConfiguration", null, 0, 1, Communication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunication_RequiredPortConfiguration(), this.getConfiguration(), null, "requiredPortConfiguration", null, 0, 1, Communication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(communicationPortEClass, CommunicationPort.class, "CommunicationPort", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCommunicationPort_PortNumber(), ecorePackage.getEInt(), "portNumber", null, 1, 1, CommunicationPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCommunicationPort_LowPortNumber(), ecorePackage.getEInt(), "lowPortNumber", null, 1, 1, CommunicationPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getCommunicationPort_HighPortNumber(), ecorePackage.getEInt(), "highPortNumber", null, 1, 1, CommunicationPort.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(providedCommunicationEClass, ProvidedCommunication.class, "ProvidedCommunication", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(requiredCommunicationEClass, RequiredCommunication.class, "RequiredCommunication", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getRequiredCommunication_IsMandatory(), ecorePackage.getEBoolean(), "isMandatory", "false", 1, 1, RequiredCommunication.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hostingEClass, Hosting.class, "Hosting", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHosting_ProvidedHost(), this.getProvidedHost(), null, "providedHost", null, 1, 1, Hosting.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHosting_RequiredHosts(), this.getRequiredHost(), null, "requiredHosts", null, 1, -1, Hosting.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHosting_ProvidedHostConfiguration(), this.getConfiguration(), null, "providedHostConfiguration", null, 0, 1, Hosting.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHosting_RequiredHostsConfiguration(), this.getConfiguration(), null, "requiredHostsConfiguration", null, 0, -1, Hosting.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hostingPortEClass, HostingPort.class, "HostingPort", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(providedHostEClass, ProvidedHost.class, "ProvidedHost", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(requiredHostEClass, RequiredHost.class, "RequiredHost", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(componentInstanceEClass, ComponentInstance.class, "ComponentInstance", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getComponentInstance_Type(), this.getComponent(), null, "type", null, 1, 1, ComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentInstance_ProvidedCommunicationInstances(), this.getProvidedCommunicationInstance(), null, "providedCommunicationInstances", null, 0, -1, ComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getComponentInstance_ProvidedHostInstances(), this.getProvidedHostInstance(), null, "providedHostInstances", null, 0, -1, ComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComponentInstance_InstantiatedOn(), ecorePackage.getEDate(), "instantiatedOn", null, 0, 1, ComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getComponentInstance_DestroyedOn(), ecorePackage.getEDate(), "destroyedOn", null, 0, 1, ComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(softwareComponentInstanceEClass, SoftwareComponentInstance.class, "SoftwareComponentInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSoftwareComponentInstance_RequiredCommunicationInstances(), this.getRequiredCommunicationInstance(), null, "requiredCommunicationInstances", null, 0, -1, SoftwareComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSoftwareComponentInstance_RequiredHostInstance(), this.getRequiredHostInstance(), null, "requiredHostInstance", null, 0, 1, SoftwareComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSoftwareComponentInstance_ConsumesDataInstance(), theDataPackage.getDataInstance(), null, "consumesDataInstance", null, 0, -1, SoftwareComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSoftwareComponentInstance_GeneratesDataInstance(), theDataPackage.getDataInstance(), null, "generatesDataInstance", null, 0, -1, SoftwareComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSoftwareComponentInstance_ManagesDataSourceInstance(), theDataPackage.getDataSourceInstance(), theDataPackage.getDataSourceInstance_SoftwareComponentInstance(), "managesDataSourceInstance", null, 0, -1, SoftwareComponentInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vmInstanceEClass, VMInstance.class, "VMInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVMInstance_Location(), theLocationPackage.getLocation(), null, "location", null, 1, 1, VMInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVMInstance_Os(), theCorePackage.getAttribute(), null, "os", null, 0, 1, VMInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getVMInstance__CheckDates__VMInstance(), ecorePackage.getEBoolean(), "checkDates", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getVMInstance(), "vm", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(communicationInstanceEClass, CommunicationInstance.class, "CommunicationInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommunicationInstance_Type(), this.getCommunication(), null, "type", null, 1, 1, CommunicationInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationInstance_ProvidedCommunicationInstance(), this.getProvidedCommunicationInstance(), null, "providedCommunicationInstance", null, 1, 1, CommunicationInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationInstance_RequiredCommunicationInstance(), this.getRequiredCommunicationInstance(), null, "requiredCommunicationInstance", null, 1, 1, CommunicationInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(communicationPortInstanceEClass, CommunicationPortInstance.class, "CommunicationPortInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommunicationPortInstance_Type(), this.getCommunicationPort(), null, "type", null, 1, 1, CommunicationPortInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(providedCommunicationInstanceEClass, ProvidedCommunicationInstance.class, "ProvidedCommunicationInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(requiredCommunicationInstanceEClass, RequiredCommunicationInstance.class, "RequiredCommunicationInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(hostingInstanceEClass, HostingInstance.class, "HostingInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHostingInstance_Type(), this.getHosting(), null, "type", null, 1, 1, HostingInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHostingInstance_ProvidedHostInstance(), this.getProvidedHostInstance(), null, "providedHostInstance", null, 1, 1, HostingInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHostingInstance_RequiredHostInstances(), this.getRequiredHostInstance(), null, "requiredHostInstances", null, 1, -1, HostingInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(hostingPortInstanceEClass, HostingPortInstance.class, "HostingPortInstance", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHostingPortInstance_Type(), this.getHostingPort(), null, "type", null, 1, 1, HostingPortInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(providedHostInstanceEClass, ProvidedHostInstance.class, "ProvidedHostInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(requiredHostInstanceEClass, RequiredHostInstance.class, "RequiredHostInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(scriptConfigurationEClass, ScriptConfiguration.class, "ScriptConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getScriptConfiguration_UploadCommand(), ecorePackage.getEString(), "uploadCommand", null, 0, 1, ScriptConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScriptConfiguration_InstallCommand(), ecorePackage.getEString(), "installCommand", null, 0, 1, ScriptConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScriptConfiguration_StartCommand(), ecorePackage.getEString(), "startCommand", null, 0, 1, ScriptConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScriptConfiguration_StopCommand(), ecorePackage.getEString(), "stopCommand", null, 0, 1, ScriptConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScriptConfiguration_DownloadCommand(), ecorePackage.getEString(), "downloadCommand", null, 0, 1, ScriptConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScriptConfiguration_ConfigureCommand(), ecorePackage.getEString(), "configureCommand", null, 0, 1, ScriptConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScriptConfiguration_UpdateCommand(), ecorePackage.getEString(), "updateCommand", null, 0, 1, ScriptConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScriptConfiguration_DevopsTool(), ecorePackage.getEString(), "devopsTool", null, 0, 1, ScriptConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScriptConfiguration_Os(), ecorePackage.getEString(), "os", null, 0, 1, ScriptConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getScriptConfiguration_ImageId(), ecorePackage.getEString(), "imageId", null, 0, 1, ScriptConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(clusterConfigurationEClass, ClusterConfiguration.class, "ClusterConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getClusterConfiguration_DownloadURL(), ecorePackage.getEString(), "downloadURL", null, 0, 1, ClusterConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getClusterConfiguration_ConfigParameters(), theCorePackage.getAttribute(), null, "configParameters", null, 0, -1, ClusterConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(locationCouplingEClass, LocationCoupling.class, "LocationCoupling", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getLocationCoupling_SoftwareComponents(), this.getSoftwareComponent(), null, "softwareComponents", null, 2, -1, LocationCoupling.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLocationCoupling_CouplingType(), this.getLocationCouplingType(), "couplingType", "SAME_HOST", 1, 1, LocationCoupling.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getLocationCoupling_Relaxed(), ecorePackage.getEBoolean(), "relaxed", "false", 1, 1, LocationCoupling.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(containerEClass, camel.deployment.Container.class, "Container", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getContainer_RequiredCommunications(), this.getRequiredCommunication(), null, "requiredCommunications", null, 0, -1, camel.deployment.Container.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getContainer_RequiredHost(), this.getRequiredHost(), null, "requiredHost", null, 0, 1, camel.deployment.Container.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(containerInstanceEClass, ContainerInstance.class, "ContainerInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getContainerInstance_RequiredCommunicationInstances(), this.getRequiredCommunicationInstance(), null, "requiredCommunicationInstances", null, 0, -1, ContainerInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getContainerInstance_RequiredHostInstance(), this.getRequiredHostInstance(), null, "requiredHostInstance", null, 0, 1, ContainerInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(componentRelationEClass, ComponentRelation.class, "ComponentRelation", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(componentRelationInstanceEClass, ComponentRelationInstance.class, "ComponentRelationInstance", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(paaSEClass, PaaS.class, "PaaS", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(paaSInstanceEClass, PaaSInstance.class, "PaaSInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(paaSConfigurationEClass, PaaSConfiguration.class, "PaaSConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getPaaSConfiguration_Api(), ecorePackage.getEString(), "api", null, 0, 1, PaaSConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPaaSConfiguration_Version(), ecorePackage.getEString(), "version", null, 0, 1, PaaSConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPaaSConfiguration_Endpoint(), ecorePackage.getEString(), "endpoint", null, 0, 1, PaaSConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getPaaSConfiguration_DownloadURL(), ecorePackage.getEString(), "downloadURL", null, 0, 1, PaaSConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(serverlessConfigurationEClass, ServerlessConfiguration.class, "ServerlessConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getServerlessConfiguration_BinaryCodeURL(), ecorePackage.getEString(), "binaryCodeURL", null, 0, 1, ServerlessConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getServerlessConfiguration_ContinuousDeployment(), ecorePackage.getEBoolean(), "continuousDeployment", "false", 1, 1, ServerlessConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServerlessConfiguration_EnvironmentConfigParams(), theCorePackage.getAttribute(), null, "environmentConfigParams", null, 0, -1, ServerlessConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServerlessConfiguration_BuildConfiguration(), this.getBuildConfiguration(), null, "buildConfiguration", null, 0, 1, ServerlessConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getServerlessConfiguration_EventConfiguration(), this.getEventConfiguration(), null, "eventConfiguration", null, 0, 1, ServerlessConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(buildConfigurationEClass, BuildConfiguration.class, "BuildConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getBuildConfiguration_ArtifactId(), ecorePackage.getEString(), "artifactId", null, 1, 1, BuildConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBuildConfiguration_BuildFramework(), ecorePackage.getEString(), "buildFramework", null, 1, 1, BuildConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBuildConfiguration_SourceCodeURL(), ecorePackage.getEString(), "sourceCodeURL", null, 1, 1, BuildConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBuildConfiguration_Include(), ecorePackage.getEString(), "include", null, 0, 1, BuildConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getBuildConfiguration_Exclude(), ecorePackage.getEString(), "exclude", null, 0, 1, BuildConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(eventConfigurationEClass, EventConfiguration.class, "EventConfiguration", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getEventConfiguration_HttpMethodName(), ecorePackage.getEString(), "httpMethodName", null, 0, 1, EventConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getEventConfiguration_HttpMethodType(), this.getHTTPMethodType(), "httpMethodType", null, 0, 1, EventConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventConfiguration_ExecutionSchedule(), theMetricPackage.getSchedule(), null, "executionSchedule", null, 0, 1, EventConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getEventConfiguration_ScheduledExecutionConfig(), theCorePackage.getFeature(), null, "scheduledExecutionConfig", null, 0, 1, EventConfiguration.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(locationCouplingTypeEEnum, LocationCouplingType.class, "LocationCouplingType");
		addEEnumLiteral(locationCouplingTypeEEnum, LocationCouplingType.SAME_HOST);
		addEEnumLiteral(locationCouplingTypeEEnum, LocationCouplingType.SAME_ZONE);
		addEEnumLiteral(locationCouplingTypeEEnum, LocationCouplingType.SAME_REGION);
		addEEnumLiteral(locationCouplingTypeEEnum, LocationCouplingType.SAME_CLOUD);

		initEEnum(httpMethodTypeEEnum, HTTPMethodType.class, "HTTPMethodType");
		addEEnumLiteral(httpMethodTypeEEnum, HTTPMethodType.GET);
		addEEnumLiteral(httpMethodTypeEEnum, HTTPMethodType.POST);
		addEEnumLiteral(httpMethodTypeEEnum, HTTPMethodType.PUT);
		addEEnumLiteral(httpMethodTypeEEnum, HTTPMethodType.DELETE);
		addEEnumLiteral(httpMethodTypeEEnum, HTTPMethodType.HEAD);
		addEEnumLiteral(httpMethodTypeEEnum, HTTPMethodType.PATCH);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
		   });	
		addAnnotation
		  (requirementSetEClass, 
		   source, 
		   new String[] {
			 "constraints", "at_least_one_req_in_req_set osOrImageRequrement"
		   });	
		addAnnotation
		  (communicationEClass, 
		   source, 
		   new String[] {
			 "constraints", "required_port_in_provided_port"
		   });	
		addAnnotation
		  (communicationPortEClass, 
		   source, 
		   new String[] {
			 "constraints", "at_least_one_port_number"
		   });	
		addAnnotation
		  (componentInstanceEClass, 
		   source, 
		   new String[] {
			 "constraints", "component_port_instances_of_correct_type"
		   });	
		addAnnotation
		  (softwareComponentInstanceEClass, 
		   source, 
		   new String[] {
			 "constraints", "sw_component_port_instances_of_correct_type correct_data_instances_sw_component"
		   });	
		addAnnotation
		  (vmInstanceEClass, 
		   source, 
		   new String[] {
			 "constraints", "correct_type_for_vm_instance"
		   });	
		addAnnotation
		  (communicationInstanceEClass, 
		   source, 
		   new String[] {
			 "constraints", "communication_instance_correct_port_instances"
		   });	
		addAnnotation
		  (scriptConfigurationEClass, 
		   source, 
		   new String[] {
			 "constraints", "script_config_at_least_one_cmd script_config_image_or_os"
		   });	
		addAnnotation
		  (locationCouplingEClass, 
		   source, 
		   new String[] {
			 "constraints", "coupled_components_same_host"
		   });	
		addAnnotation
		  (containerInstanceEClass, 
		   source, 
		   new String[] {
			 "constraints", "container_port_instances_of_correct_type"
		   });	
		addAnnotation
		  (paaSInstanceEClass, 
		   source, 
		   new String[] {
			 "constraints", "correct_type_for_paas_instance"
		   });	
		addAnnotation
		  (serverlessConfigurationEClass, 
		   source, 
		   new String[] {
			 "constraints", "correct_serverless_config"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";	
		addAnnotation
		  (requirementSetEClass, 
		   source, 
		   new String[] {
			 "at_least_one_req_in_req_set", "Tuple {\n\tmessage : String = \'RequirementSet: \' + self.name + \' should have at least one requirement referenced\',\n\tstatus : Boolean = \n\t\t\t\tasError(resourceRequirement <> null or locationRequirement <> null or providerRequirement <> null or verticalScaleRequirement <> null \n\t\t\t\t\tor horizontalScaleRequirement <> null or securityRequirement <> null or osRequirement <> null or imageRequirement <> null or paasRequirement <> null\n\t\t\t\t)\n}.status",
			 "osOrImageRequrement", "Tuple {\n\tmessage : String = \'RequirementSet: \' + self.name + \'cannot have both an os and image requirement referenced\',\n\tstatus : Boolean = \n\t\t\t\tasError(not(osRequirement <> null and imageRequirement <> null))\n}.status"
		   });	
		addAnnotation
		  (communicationEClass, 
		   source, 
		   new String[] {
			 "required_port_in_provided_port", "Tuple {\n\tmessage : String = \'Communication: \' + self.name + \' should have the required port (range) to be equal or included in the provided port (range)\',\n\tstatus : Boolean = \n\t\t\t\tasError(\n\t\t\t\t\t((requiredCommunication.portNumber > 0 and providedCommunication.portNumber > 0) implies requiredCommunication.portNumber = providedCommunication.portNumber)\n\t\t\t\t\tand ((requiredCommunication.portNumber > 0 and providedCommunication.portNumber = 0) implies (requiredCommunication.portNumber >= providedCommunication.lowPortNumber and requiredCommunication.portNumber <= providedCommunication.highPortNumber))\n\t\t\t\t\tand (requiredCommunication.portNumber = 0 implies (providedCommunication.portNumber = 0 and requiredCommunication.lowPortNumber >= providedCommunication.lowPortNumber and requiredCommunication.highPortNumber <= providedCommunication.highPortNumber))\n\t\t\t\t)\n}.status"
		   });	
		addAnnotation
		  (communicationPortEClass, 
		   source, 
		   new String[] {
			 "at_least_one_port_number", "Tuple {\n\tmessage : String = \'CommunicationPort: \' + self.name + \' should have either a valid port number or a port range specified\',\n\tstatus : Boolean = \n\t\t\t\tasError(\n\t\t\t\t\t(portNumber > 0 implies (lowPortNumber = 0 and highPortNumber = 0)) and ((lowPortNumber > 0 and highPortNumber > 0) implies portNumber = 0) and ((portNumber > 0 and portNumber <= 65535) or (lowPortNumber > 0 and highPortNumber > 0 and lowPortNumber < highPortNumber and highPortNumber <= 65535)) \n\t\t\t\t)\n}.status"
		   });	
		addAnnotation
		  (componentInstanceEClass, 
		   source, 
		   new String[] {
			 "component_port_instances_of_correct_type", "Tuple {\n\tmessage : String = \'The type of component instance:\' + self.name + \' should have as provided communication/hosts the types of the component instance\\\'s provided communication/host instances\',\n\tstatus : Boolean = \n\t\t\tasError(providedCommunicationInstances\n\t\t\t\t->forAll(p | type.providedCommunications\n\t\t\t\t\t->includes(p.type)) and providedHostInstances\n\t\t\t\t->forAll(p | type.providedHosts\n\t\t\t\t\t->includes(p.type)))\n}.status"
		   });	
		addAnnotation
		  (softwareComponentInstanceEClass, 
		   source, 
		   new String[] {
			 "sw_component_port_instances_of_correct_type", "Tuple {\n\tmessage : String = \'The type of sw component instance:\' + self.name + \' should have as required communication/hosts the types of the component instance\\\'s required communication/host instances\',\n\tstatus : Boolean = \n\t\t\t\tasError(type.oclIsTypeOf(SoftwareComponent) and requiredCommunicationInstances->forAll(p | type.oclAsType(SoftwareComponent).requiredCommunications->includes(p.type)) \n\t\t\t\t\tand (requiredHostInstance <> null implies requiredHostInstance.type = type.oclAsType(SoftwareComponent).requiredHost))\n}.status",
			 "correct_data_instances_sw_component", "Tuple {\n\tmessage : String = \'SoftwareComponentInstance: \' + self.name + \' should map to instances of consumed, generated or managed instances of data which have a type which is consumed, generated or managed by the type of this instance\',\n\tstatus : Boolean = \n\t\t\t\tasError(consumesDataInstance->forAll(di | self.type.oclAsType(SoftwareComponent).consumesData->exists(d | di.type=d)) and \n\t\t\t\tgeneratesDataInstance->forAll(di | self.type.oclAsType(SoftwareComponent).generatesData->exists(d | di.type=d)) and\n\t\t\t\tmanagesDataSourceInstance->forAll(dsi | self.type.oclAsType(SoftwareComponent).managesDataSource->exists(ds | dsi.type=ds)))\n}.status"
		   });	
		addAnnotation
		  (vmInstanceEClass, 
		   source, 
		   new String[] {
			 "correct_type_for_vm_instance", "Tuple {\n\tmessage : String = \'VM Instance: \' + self.name + \' should have as type a VM\',\n\tstatus : Boolean = asError(type.oclIsTypeOf(VM))\n}.status"
		   });	
		addAnnotation
		  (communicationInstanceEClass, 
		   source, 
		   new String[] {
			 "communication_instance_correct_port_instances", "Tuple {\n\tmessage : String = \'CommunicationInstance: \' + self.name + \' should have its provided and required communication instances\\\' types mapping to its required communication/host type, respectively\',\n\tstatus : Boolean = \n\t\t\tasError(requiredCommunicationInstance.type = type.requiredCommunication and providedCommunicationInstance.type = type.providedCommunication)\n}.status"
		   });	
		addAnnotation
		  (scriptConfigurationEClass, 
		   source, 
		   new String[] {
			 "script_config_at_least_one_cmd", "Tuple {\n\tmessage : String = \'ScriptConfiguration: \' + self.name + \' should have at least one command specified\',\n\tstatus : Boolean = \n\t\t\t\tasError(imageId = null implies (uploadCommand <> null or installCommand <> null or startCommand <> null or stopCommand <> null\n\t\t\t\t\tor downloadCommand <> null or configureCommand <> null or updateCommand <> null)\n\t\t\t\t)\n}.status",
			 "script_config_image_or_os", "Tuple {\n\tmessage : String = \'Script Configuration: \' + self.name + \' cannot have both the os and imageId supplied\',\n\tstatus : Boolean = \n\t\t\t\tasError(os = null or imageId = null)\n}.status"
		   });	
		addAnnotation
		  (locationCouplingEClass, 
		   source, 
		   new String[] {
			 "coupled_components_same_host", "Tuple {\n\tmessage : String = \'In this coupling:\' + self.name + \' all components should be mapped to the same host\',\n\tstatus : Boolean = \n\t\t\t\tasError(\n\t\t\t\tlet hostings : Hosting[*] = self.oclContainer().oclAsType(DeploymentTypeModel).hostings->select(p | softwareComponents->exists(s | p.requiredHosts->includes(s.requiredHost)))\n\t\t\t\tin \n\t\t\t\t\tif (couplingType = LocationCouplingType::SAME_HOST and hostings->size() > 0) then softwareComponents->forAll(p1,p2 | \n\t\t\t\t\t\tif (p1 <> p2) then (hostings->exists(h1,h2 | h1.requiredHosts->includes(p1.requiredHost) and h2.requiredHosts->includes(p2.requiredHost) \n\t\t\t\t\t\t\tand h1.providedHost = h2.providedHost))\n\t\t\t\t\t\telse true\n\t\t\t\t\t\tendif\n\t\t\t\t\t\t)\t\n\t\t\t\t\telse true\n\t\t\t\t\tendif)\n}.status"
		   });	
		addAnnotation
		  (containerInstanceEClass, 
		   source, 
		   new String[] {
			 "container_port_instances_of_correct_type", "Tuple {\n\tmessage : String = \'The type of container instance:\' + self.name + \' should have as required communication/hosts the types of the container instance\\\'s required communication/host instances\',\n\tstatus : Boolean = \n\t\t\t\tasError(type.oclIsTypeOf(Container) and requiredCommunicationInstances->forAll(p | type.oclAsType(Container).requiredCommunications->includes(p.type)) \n\t\t\t\t\tand (requiredHostInstance <> null implies requiredHostInstance.type = type.oclAsType(Container).requiredHost))\n}.status"
		   });	
		addAnnotation
		  (paaSInstanceEClass, 
		   source, 
		   new String[] {
			 "correct_type_for_paas_instance", "Tuple {\n\tmessage : String = \'PaaS Instance: \' + self.name + \' should have as type a PaaS\',\n\tstatus : Boolean = asError(type.oclIsTypeOf(PaaS))\n}.status"
		   });	
		addAnnotation
		  (serverlessConfigurationEClass, 
		   source, 
		   new String[] {
			 "correct_serverless_config", "Tuple {\n\tmessage : String = \'ServerlessConfiguration: \' + self.name + \' should have either binary code URL given or a build configuration\',\n\tstatus : Boolean = \n\t\t\t\tasError(\n\t\t\t\t\tbinaryCodeURL <> null xor buildConfiguration <> null\n\t\t\t\t)\n}.status"
		   });
	}

} //DeploymentPackageImpl
