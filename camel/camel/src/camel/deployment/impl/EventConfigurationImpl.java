/**
 */
package camel.deployment.impl;

import camel.core.Feature;

import camel.core.impl.FeatureImpl;

import camel.deployment.DeploymentPackage;
import camel.deployment.EventConfiguration;
import camel.deployment.HTTPMethodType;

import camel.metric.Schedule;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Event Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.EventConfigurationImpl#getHttpMethodName <em>Http Method Name</em>}</li>
 *   <li>{@link camel.deployment.impl.EventConfigurationImpl#getHttpMethodType <em>Http Method Type</em>}</li>
 *   <li>{@link camel.deployment.impl.EventConfigurationImpl#getExecutionSchedule <em>Execution Schedule</em>}</li>
 *   <li>{@link camel.deployment.impl.EventConfigurationImpl#getScheduledExecutionConfig <em>Scheduled Execution Config</em>}</li>
 * </ul>
 *
 * @generated
 */
public class EventConfigurationImpl extends FeatureImpl implements EventConfiguration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected EventConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.EVENT_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getHttpMethodName() {
		return (String)eGet(DeploymentPackage.Literals.EVENT_CONFIGURATION__HTTP_METHOD_NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHttpMethodName(String newHttpMethodName) {
		eSet(DeploymentPackage.Literals.EVENT_CONFIGURATION__HTTP_METHOD_NAME, newHttpMethodName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HTTPMethodType getHttpMethodType() {
		return (HTTPMethodType)eGet(DeploymentPackage.Literals.EVENT_CONFIGURATION__HTTP_METHOD_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHttpMethodType(HTTPMethodType newHttpMethodType) {
		eSet(DeploymentPackage.Literals.EVENT_CONFIGURATION__HTTP_METHOD_TYPE, newHttpMethodType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Schedule getExecutionSchedule() {
		return (Schedule)eGet(DeploymentPackage.Literals.EVENT_CONFIGURATION__EXECUTION_SCHEDULE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionSchedule(Schedule newExecutionSchedule) {
		eSet(DeploymentPackage.Literals.EVENT_CONFIGURATION__EXECUTION_SCHEDULE, newExecutionSchedule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Feature getScheduledExecutionConfig() {
		return (Feature)eGet(DeploymentPackage.Literals.EVENT_CONFIGURATION__SCHEDULED_EXECUTION_CONFIG, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScheduledExecutionConfig(Feature newScheduledExecutionConfig) {
		eSet(DeploymentPackage.Literals.EVENT_CONFIGURATION__SCHEDULED_EXECUTION_CONFIG, newScheduledExecutionConfig);
	}

} //EventConfigurationImpl
