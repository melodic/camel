/**
 */
package camel.deployment.impl;

import camel.deployment.ContainerInstance;
import camel.deployment.DeploymentPackage;
import camel.deployment.RequiredCommunicationInstance;
import camel.deployment.RequiredHostInstance;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Container Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.ContainerInstanceImpl#getRequiredCommunicationInstances <em>Required Communication Instances</em>}</li>
 *   <li>{@link camel.deployment.impl.ContainerInstanceImpl#getRequiredHostInstance <em>Required Host Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ContainerInstanceImpl extends ComponentInstanceImpl implements ContainerInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContainerInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.CONTAINER_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RequiredCommunicationInstance> getRequiredCommunicationInstances() {
		return (EList<RequiredCommunicationInstance>)eGet(DeploymentPackage.Literals.CONTAINER_INSTANCE__REQUIRED_COMMUNICATION_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredHostInstance getRequiredHostInstance() {
		return (RequiredHostInstance)eGet(DeploymentPackage.Literals.CONTAINER_INSTANCE__REQUIRED_HOST_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequiredHostInstance(RequiredHostInstance newRequiredHostInstance) {
		eSet(DeploymentPackage.Literals.CONTAINER_INSTANCE__REQUIRED_HOST_INSTANCE, newRequiredHostInstance);
	}

} //ContainerInstanceImpl
