/**
 */
package camel.deployment.impl;

import camel.core.Attribute;

import camel.deployment.ClusterConfiguration;
import camel.deployment.DeploymentPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cluster Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.ClusterConfigurationImpl#getDownloadURL <em>Download URL</em>}</li>
 *   <li>{@link camel.deployment.impl.ClusterConfigurationImpl#getConfigParameters <em>Config Parameters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ClusterConfigurationImpl extends ConfigurationImpl implements ClusterConfiguration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ClusterConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.CLUSTER_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDownloadURL() {
		return (String)eGet(DeploymentPackage.Literals.CLUSTER_CONFIGURATION__DOWNLOAD_URL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDownloadURL(String newDownloadURL) {
		eSet(DeploymentPackage.Literals.CLUSTER_CONFIGURATION__DOWNLOAD_URL, newDownloadURL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Attribute> getConfigParameters() {
		return (EList<Attribute>)eGet(DeploymentPackage.Literals.CLUSTER_CONFIGURATION__CONFIG_PARAMETERS, true);
	}

} //ClusterConfigurationImpl
