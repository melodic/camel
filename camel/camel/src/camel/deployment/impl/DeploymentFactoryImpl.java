/**
 */
package camel.deployment.impl;

import camel.deployment.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class DeploymentFactoryImpl extends EFactoryImpl implements DeploymentFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static DeploymentFactory init() {
		try {
			DeploymentFactory theDeploymentFactory = (DeploymentFactory)EPackage.Registry.INSTANCE.getEFactory(DeploymentPackage.eNS_URI);
			if (theDeploymentFactory != null) {
				return theDeploymentFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new DeploymentFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case DeploymentPackage.DEPLOYMENT_INSTANCE_MODEL: return (EObject)createDeploymentInstanceModel();
			case DeploymentPackage.DEPLOYMENT_TYPE_MODEL: return (EObject)createDeploymentTypeModel();
			case DeploymentPackage.SOFTWARE_COMPONENT: return (EObject)createSoftwareComponent();
			case DeploymentPackage.VM: return (EObject)createVM();
			case DeploymentPackage.REQUIREMENT_SET: return (EObject)createRequirementSet();
			case DeploymentPackage.COMMUNICATION: return (EObject)createCommunication();
			case DeploymentPackage.PROVIDED_COMMUNICATION: return (EObject)createProvidedCommunication();
			case DeploymentPackage.REQUIRED_COMMUNICATION: return (EObject)createRequiredCommunication();
			case DeploymentPackage.HOSTING: return (EObject)createHosting();
			case DeploymentPackage.PROVIDED_HOST: return (EObject)createProvidedHost();
			case DeploymentPackage.REQUIRED_HOST: return (EObject)createRequiredHost();
			case DeploymentPackage.SOFTWARE_COMPONENT_INSTANCE: return (EObject)createSoftwareComponentInstance();
			case DeploymentPackage.VM_INSTANCE: return (EObject)createVMInstance();
			case DeploymentPackage.COMMUNICATION_INSTANCE: return (EObject)createCommunicationInstance();
			case DeploymentPackage.COMMUNICATION_PORT_INSTANCE: return (EObject)createCommunicationPortInstance();
			case DeploymentPackage.PROVIDED_COMMUNICATION_INSTANCE: return (EObject)createProvidedCommunicationInstance();
			case DeploymentPackage.REQUIRED_COMMUNICATION_INSTANCE: return (EObject)createRequiredCommunicationInstance();
			case DeploymentPackage.HOSTING_INSTANCE: return (EObject)createHostingInstance();
			case DeploymentPackage.PROVIDED_HOST_INSTANCE: return (EObject)createProvidedHostInstance();
			case DeploymentPackage.REQUIRED_HOST_INSTANCE: return (EObject)createRequiredHostInstance();
			case DeploymentPackage.SCRIPT_CONFIGURATION: return (EObject)createScriptConfiguration();
			case DeploymentPackage.CLUSTER_CONFIGURATION: return (EObject)createClusterConfiguration();
			case DeploymentPackage.LOCATION_COUPLING: return (EObject)createLocationCoupling();
			case DeploymentPackage.CONTAINER: return (EObject)createContainer();
			case DeploymentPackage.CONTAINER_INSTANCE: return (EObject)createContainerInstance();
			case DeploymentPackage.PAA_S: return (EObject)createPaaS();
			case DeploymentPackage.PAA_SINSTANCE: return (EObject)createPaaSInstance();
			case DeploymentPackage.PAA_SCONFIGURATION: return (EObject)createPaaSConfiguration();
			case DeploymentPackage.SERVERLESS_CONFIGURATION: return (EObject)createServerlessConfiguration();
			case DeploymentPackage.BUILD_CONFIGURATION: return (EObject)createBuildConfiguration();
			case DeploymentPackage.EVENT_CONFIGURATION: return (EObject)createEventConfiguration();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case DeploymentPackage.LOCATION_COUPLING_TYPE:
				return createLocationCouplingTypeFromString(eDataType, initialValue);
			case DeploymentPackage.HTTP_METHOD_TYPE:
				return createHTTPMethodTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case DeploymentPackage.LOCATION_COUPLING_TYPE:
				return convertLocationCouplingTypeToString(eDataType, instanceValue);
			case DeploymentPackage.HTTP_METHOD_TYPE:
				return convertHTTPMethodTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentInstanceModel createDeploymentInstanceModel() {
		DeploymentInstanceModelImpl deploymentInstanceModel = new DeploymentInstanceModelImpl();
		return deploymentInstanceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentTypeModel createDeploymentTypeModel() {
		DeploymentTypeModelImpl deploymentTypeModel = new DeploymentTypeModelImpl();
		return deploymentTypeModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareComponent createSoftwareComponent() {
		SoftwareComponentImpl softwareComponent = new SoftwareComponentImpl();
		return softwareComponent;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VM createVM() {
		VMImpl vm = new VMImpl();
		return vm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementSet createRequirementSet() {
		RequirementSetImpl requirementSet = new RequirementSetImpl();
		return requirementSet;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Communication createCommunication() {
		CommunicationImpl communication = new CommunicationImpl();
		return communication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedCommunication createProvidedCommunication() {
		ProvidedCommunicationImpl providedCommunication = new ProvidedCommunicationImpl();
		return providedCommunication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredCommunication createRequiredCommunication() {
		RequiredCommunicationImpl requiredCommunication = new RequiredCommunicationImpl();
		return requiredCommunication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Hosting createHosting() {
		HostingImpl hosting = new HostingImpl();
		return hosting;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedHost createProvidedHost() {
		ProvidedHostImpl providedHost = new ProvidedHostImpl();
		return providedHost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredHost createRequiredHost() {
		RequiredHostImpl requiredHost = new RequiredHostImpl();
		return requiredHost;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareComponentInstance createSoftwareComponentInstance() {
		SoftwareComponentInstanceImpl softwareComponentInstance = new SoftwareComponentInstanceImpl();
		return softwareComponentInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VMInstance createVMInstance() {
		VMInstanceImpl vmInstance = new VMInstanceImpl();
		return vmInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationInstance createCommunicationInstance() {
		CommunicationInstanceImpl communicationInstance = new CommunicationInstanceImpl();
		return communicationInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationPortInstance createCommunicationPortInstance() {
		CommunicationPortInstanceImpl communicationPortInstance = new CommunicationPortInstanceImpl();
		return communicationPortInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedCommunicationInstance createProvidedCommunicationInstance() {
		ProvidedCommunicationInstanceImpl providedCommunicationInstance = new ProvidedCommunicationInstanceImpl();
		return providedCommunicationInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredCommunicationInstance createRequiredCommunicationInstance() {
		RequiredCommunicationInstanceImpl requiredCommunicationInstance = new RequiredCommunicationInstanceImpl();
		return requiredCommunicationInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HostingInstance createHostingInstance() {
		HostingInstanceImpl hostingInstance = new HostingInstanceImpl();
		return hostingInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProvidedHostInstance createProvidedHostInstance() {
		ProvidedHostInstanceImpl providedHostInstance = new ProvidedHostInstanceImpl();
		return providedHostInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequiredHostInstance createRequiredHostInstance() {
		RequiredHostInstanceImpl requiredHostInstance = new RequiredHostInstanceImpl();
		return requiredHostInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScriptConfiguration createScriptConfiguration() {
		ScriptConfigurationImpl scriptConfiguration = new ScriptConfigurationImpl();
		return scriptConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ClusterConfiguration createClusterConfiguration() {
		ClusterConfigurationImpl clusterConfiguration = new ClusterConfigurationImpl();
		return clusterConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationCoupling createLocationCoupling() {
		LocationCouplingImpl locationCoupling = new LocationCouplingImpl();
		return locationCoupling;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public camel.deployment.Container createContainer() {
		ContainerImpl container = new ContainerImpl();
		return container;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContainerInstance createContainerInstance() {
		ContainerInstanceImpl containerInstance = new ContainerInstanceImpl();
		return containerInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaaS createPaaS() {
		PaaSImpl paaS = new PaaSImpl();
		return paaS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaaSInstance createPaaSInstance() {
		PaaSInstanceImpl paaSInstance = new PaaSInstanceImpl();
		return paaSInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaaSConfiguration createPaaSConfiguration() {
		PaaSConfigurationImpl paaSConfiguration = new PaaSConfigurationImpl();
		return paaSConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServerlessConfiguration createServerlessConfiguration() {
		ServerlessConfigurationImpl serverlessConfiguration = new ServerlessConfigurationImpl();
		return serverlessConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BuildConfiguration createBuildConfiguration() {
		BuildConfigurationImpl buildConfiguration = new BuildConfigurationImpl();
		return buildConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventConfiguration createEventConfiguration() {
		EventConfigurationImpl eventConfiguration = new EventConfigurationImpl();
		return eventConfiguration;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationCouplingType createLocationCouplingTypeFromString(EDataType eDataType, String initialValue) {
		LocationCouplingType result = LocationCouplingType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertLocationCouplingTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HTTPMethodType createHTTPMethodTypeFromString(EDataType eDataType, String initialValue) {
		HTTPMethodType result = HTTPMethodType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertHTTPMethodTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentPackage getDeploymentPackage() {
		return (DeploymentPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static DeploymentPackage getPackage() {
		return DeploymentPackage.eINSTANCE;
	}

} //DeploymentFactoryImpl
