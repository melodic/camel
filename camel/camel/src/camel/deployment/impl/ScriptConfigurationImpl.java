/**
 */
package camel.deployment.impl;

import camel.deployment.DeploymentPackage;
import camel.deployment.ScriptConfiguration;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Script Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.ScriptConfigurationImpl#getUploadCommand <em>Upload Command</em>}</li>
 *   <li>{@link camel.deployment.impl.ScriptConfigurationImpl#getInstallCommand <em>Install Command</em>}</li>
 *   <li>{@link camel.deployment.impl.ScriptConfigurationImpl#getStartCommand <em>Start Command</em>}</li>
 *   <li>{@link camel.deployment.impl.ScriptConfigurationImpl#getStopCommand <em>Stop Command</em>}</li>
 *   <li>{@link camel.deployment.impl.ScriptConfigurationImpl#getDownloadCommand <em>Download Command</em>}</li>
 *   <li>{@link camel.deployment.impl.ScriptConfigurationImpl#getConfigureCommand <em>Configure Command</em>}</li>
 *   <li>{@link camel.deployment.impl.ScriptConfigurationImpl#getUpdateCommand <em>Update Command</em>}</li>
 *   <li>{@link camel.deployment.impl.ScriptConfigurationImpl#getDevopsTool <em>Devops Tool</em>}</li>
 *   <li>{@link camel.deployment.impl.ScriptConfigurationImpl#getOs <em>Os</em>}</li>
 *   <li>{@link camel.deployment.impl.ScriptConfigurationImpl#getImageId <em>Image Id</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScriptConfigurationImpl extends ConfigurationImpl implements ScriptConfiguration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScriptConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.SCRIPT_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUploadCommand() {
		return (String)eGet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__UPLOAD_COMMAND, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUploadCommand(String newUploadCommand) {
		eSet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__UPLOAD_COMMAND, newUploadCommand);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInstallCommand() {
		return (String)eGet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__INSTALL_COMMAND, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInstallCommand(String newInstallCommand) {
		eSet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__INSTALL_COMMAND, newInstallCommand);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStartCommand() {
		return (String)eGet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__START_COMMAND, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartCommand(String newStartCommand) {
		eSet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__START_COMMAND, newStartCommand);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStopCommand() {
		return (String)eGet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__STOP_COMMAND, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStopCommand(String newStopCommand) {
		eSet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__STOP_COMMAND, newStopCommand);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDownloadCommand() {
		return (String)eGet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__DOWNLOAD_COMMAND, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDownloadCommand(String newDownloadCommand) {
		eSet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__DOWNLOAD_COMMAND, newDownloadCommand);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConfigureCommand() {
		return (String)eGet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__CONFIGURE_COMMAND, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfigureCommand(String newConfigureCommand) {
		eSet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__CONFIGURE_COMMAND, newConfigureCommand);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getUpdateCommand() {
		return (String)eGet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__UPDATE_COMMAND, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUpdateCommand(String newUpdateCommand) {
		eSet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__UPDATE_COMMAND, newUpdateCommand);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDevopsTool() {
		return (String)eGet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__DEVOPS_TOOL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDevopsTool(String newDevopsTool) {
		eSet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__DEVOPS_TOOL, newDevopsTool);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getOs() {
		return (String)eGet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__OS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOs(String newOs) {
		eSet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__OS, newOs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getImageId() {
		return (String)eGet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__IMAGE_ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImageId(String newImageId) {
		eSet(DeploymentPackage.Literals.SCRIPT_CONFIGURATION__IMAGE_ID, newImageId);
	}

} //ScriptConfigurationImpl
