/**
 */
package camel.deployment.impl;

import camel.deployment.CommunicationInstance;
import camel.deployment.ContainerInstance;
import camel.deployment.DeploymentInstanceModel;
import camel.deployment.DeploymentPackage;
import camel.deployment.DeploymentTypeModel;
import camel.deployment.HostingInstance;
import camel.deployment.PaaSInstance;
import camel.deployment.SoftwareComponentInstance;
import camel.deployment.VMInstance;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instance Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.DeploymentInstanceModelImpl#getSoftwareComponentInstances <em>Software Component Instances</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentInstanceModelImpl#getVmInstances <em>Vm Instances</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentInstanceModelImpl#getCommunicationInstances <em>Communication Instances</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentInstanceModelImpl#getHostingInstances <em>Hosting Instances</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentInstanceModelImpl#getType <em>Type</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentInstanceModelImpl#getContainerInstances <em>Container Instances</em>}</li>
 *   <li>{@link camel.deployment.impl.DeploymentInstanceModelImpl#getPaasInstances <em>Paas Instances</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DeploymentInstanceModelImpl extends DeploymentModelImpl implements DeploymentInstanceModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeploymentInstanceModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.DEPLOYMENT_INSTANCE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SoftwareComponentInstance> getSoftwareComponentInstances() {
		return (EList<SoftwareComponentInstance>)eGet(DeploymentPackage.Literals.DEPLOYMENT_INSTANCE_MODEL__SOFTWARE_COMPONENT_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<VMInstance> getVmInstances() {
		return (EList<VMInstance>)eGet(DeploymentPackage.Literals.DEPLOYMENT_INSTANCE_MODEL__VM_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<CommunicationInstance> getCommunicationInstances() {
		return (EList<CommunicationInstance>)eGet(DeploymentPackage.Literals.DEPLOYMENT_INSTANCE_MODEL__COMMUNICATION_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<HostingInstance> getHostingInstances() {
		return (EList<HostingInstance>)eGet(DeploymentPackage.Literals.DEPLOYMENT_INSTANCE_MODEL__HOSTING_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentTypeModel getType() {
		return (DeploymentTypeModel)eGet(DeploymentPackage.Literals.DEPLOYMENT_INSTANCE_MODEL__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(DeploymentTypeModel newType) {
		eSet(DeploymentPackage.Literals.DEPLOYMENT_INSTANCE_MODEL__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ContainerInstance> getContainerInstances() {
		return (EList<ContainerInstance>)eGet(DeploymentPackage.Literals.DEPLOYMENT_INSTANCE_MODEL__CONTAINER_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<PaaSInstance> getPaasInstances() {
		return (EList<PaaSInstance>)eGet(DeploymentPackage.Literals.DEPLOYMENT_INSTANCE_MODEL__PAAS_INSTANCES, true);
	}

} //DeploymentInstanceModelImpl
