/**
 */
package camel.deployment.impl;

import camel.core.impl.FeatureImpl;

import camel.deployment.CommunicationPort;
import camel.deployment.DeploymentPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Port</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.CommunicationPortImpl#getPortNumber <em>Port Number</em>}</li>
 *   <li>{@link camel.deployment.impl.CommunicationPortImpl#getLowPortNumber <em>Low Port Number</em>}</li>
 *   <li>{@link camel.deployment.impl.CommunicationPortImpl#getHighPortNumber <em>High Port Number</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class CommunicationPortImpl extends FeatureImpl implements CommunicationPort {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationPortImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.COMMUNICATION_PORT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getPortNumber() {
		return (Integer)eGet(DeploymentPackage.Literals.COMMUNICATION_PORT__PORT_NUMBER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPortNumber(int newPortNumber) {
		eSet(DeploymentPackage.Literals.COMMUNICATION_PORT__PORT_NUMBER, newPortNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getLowPortNumber() {
		return (Integer)eGet(DeploymentPackage.Literals.COMMUNICATION_PORT__LOW_PORT_NUMBER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLowPortNumber(int newLowPortNumber) {
		eSet(DeploymentPackage.Literals.COMMUNICATION_PORT__LOW_PORT_NUMBER, newLowPortNumber);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getHighPortNumber() {
		return (Integer)eGet(DeploymentPackage.Literals.COMMUNICATION_PORT__HIGH_PORT_NUMBER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHighPortNumber(int newHighPortNumber) {
		eSet(DeploymentPackage.Literals.COMMUNICATION_PORT__HIGH_PORT_NUMBER, newHighPortNumber);
	}

} //CommunicationPortImpl
