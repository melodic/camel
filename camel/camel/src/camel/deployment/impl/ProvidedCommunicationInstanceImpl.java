/**
 */
package camel.deployment.impl;

import camel.deployment.DeploymentPackage;
import camel.deployment.ProvidedCommunicationInstance;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provided Communication Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ProvidedCommunicationInstanceImpl extends CommunicationPortInstanceImpl implements ProvidedCommunicationInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProvidedCommunicationInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.PROVIDED_COMMUNICATION_INSTANCE;
	}

} //ProvidedCommunicationInstanceImpl
