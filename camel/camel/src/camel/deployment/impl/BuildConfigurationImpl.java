/**
 */
package camel.deployment.impl;

import camel.core.impl.FeatureImpl;

import camel.deployment.BuildConfiguration;
import camel.deployment.DeploymentPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Build Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.BuildConfigurationImpl#getArtifactId <em>Artifact Id</em>}</li>
 *   <li>{@link camel.deployment.impl.BuildConfigurationImpl#getBuildFramework <em>Build Framework</em>}</li>
 *   <li>{@link camel.deployment.impl.BuildConfigurationImpl#getSourceCodeURL <em>Source Code URL</em>}</li>
 *   <li>{@link camel.deployment.impl.BuildConfigurationImpl#getInclude <em>Include</em>}</li>
 *   <li>{@link camel.deployment.impl.BuildConfigurationImpl#getExclude <em>Exclude</em>}</li>
 * </ul>
 *
 * @generated
 */
public class BuildConfigurationImpl extends FeatureImpl implements BuildConfiguration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected BuildConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.BUILD_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getArtifactId() {
		return (String)eGet(DeploymentPackage.Literals.BUILD_CONFIGURATION__ARTIFACT_ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setArtifactId(String newArtifactId) {
		eSet(DeploymentPackage.Literals.BUILD_CONFIGURATION__ARTIFACT_ID, newArtifactId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBuildFramework() {
		return (String)eGet(DeploymentPackage.Literals.BUILD_CONFIGURATION__BUILD_FRAMEWORK, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBuildFramework(String newBuildFramework) {
		eSet(DeploymentPackage.Literals.BUILD_CONFIGURATION__BUILD_FRAMEWORK, newBuildFramework);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getSourceCodeURL() {
		return (String)eGet(DeploymentPackage.Literals.BUILD_CONFIGURATION__SOURCE_CODE_URL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceCodeURL(String newSourceCodeURL) {
		eSet(DeploymentPackage.Literals.BUILD_CONFIGURATION__SOURCE_CODE_URL, newSourceCodeURL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInclude() {
		return (String)eGet(DeploymentPackage.Literals.BUILD_CONFIGURATION__INCLUDE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInclude(String newInclude) {
		eSet(DeploymentPackage.Literals.BUILD_CONFIGURATION__INCLUDE, newInclude);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getExclude() {
		return (String)eGet(DeploymentPackage.Literals.BUILD_CONFIGURATION__EXCLUDE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExclude(String newExclude) {
		eSet(DeploymentPackage.Literals.BUILD_CONFIGURATION__EXCLUDE, newExclude);
	}

} //BuildConfigurationImpl
