/**
 */
package camel.deployment.impl;

import camel.core.impl.FeatureImpl;

import camel.deployment.DeploymentPackage;
import camel.deployment.RequirementSet;

import camel.requirement.HorizontalScaleRequirement;
import camel.requirement.ImageRequirement;
import camel.requirement.LocationRequirement;
import camel.requirement.OSRequirement;
import camel.requirement.PaaSRequirement;
import camel.requirement.ProviderRequirement;
import camel.requirement.ResourceRequirement;
import camel.requirement.SecurityRequirement;
import camel.requirement.VerticalScaleRequirement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Requirement Set</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.RequirementSetImpl#getResourceRequirement <em>Resource Requirement</em>}</li>
 *   <li>{@link camel.deployment.impl.RequirementSetImpl#getPaasRequirement <em>Paas Requirement</em>}</li>
 *   <li>{@link camel.deployment.impl.RequirementSetImpl#getLocationRequirement <em>Location Requirement</em>}</li>
 *   <li>{@link camel.deployment.impl.RequirementSetImpl#getProviderRequirement <em>Provider Requirement</em>}</li>
 *   <li>{@link camel.deployment.impl.RequirementSetImpl#getVerticalScaleRequirement <em>Vertical Scale Requirement</em>}</li>
 *   <li>{@link camel.deployment.impl.RequirementSetImpl#getHorizontalScaleRequirement <em>Horizontal Scale Requirement</em>}</li>
 *   <li>{@link camel.deployment.impl.RequirementSetImpl#getSecurityRequirement <em>Security Requirement</em>}</li>
 *   <li>{@link camel.deployment.impl.RequirementSetImpl#getOsRequirement <em>Os Requirement</em>}</li>
 *   <li>{@link camel.deployment.impl.RequirementSetImpl#getImageRequirement <em>Image Requirement</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RequirementSetImpl extends FeatureImpl implements RequirementSet {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequirementSetImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.REQUIREMENT_SET;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceRequirement getResourceRequirement() {
		return (ResourceRequirement)eGet(DeploymentPackage.Literals.REQUIREMENT_SET__RESOURCE_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourceRequirement(ResourceRequirement newResourceRequirement) {
		eSet(DeploymentPackage.Literals.REQUIREMENT_SET__RESOURCE_REQUIREMENT, newResourceRequirement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaaSRequirement getPaasRequirement() {
		return (PaaSRequirement)eGet(DeploymentPackage.Literals.REQUIREMENT_SET__PAAS_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPaasRequirement(PaaSRequirement newPaasRequirement) {
		eSet(DeploymentPackage.Literals.REQUIREMENT_SET__PAAS_REQUIREMENT, newPaasRequirement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationRequirement getLocationRequirement() {
		return (LocationRequirement)eGet(DeploymentPackage.Literals.REQUIREMENT_SET__LOCATION_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLocationRequirement(LocationRequirement newLocationRequirement) {
		eSet(DeploymentPackage.Literals.REQUIREMENT_SET__LOCATION_REQUIREMENT, newLocationRequirement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProviderRequirement getProviderRequirement() {
		return (ProviderRequirement)eGet(DeploymentPackage.Literals.REQUIREMENT_SET__PROVIDER_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setProviderRequirement(ProviderRequirement newProviderRequirement) {
		eSet(DeploymentPackage.Literals.REQUIREMENT_SET__PROVIDER_REQUIREMENT, newProviderRequirement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VerticalScaleRequirement getVerticalScaleRequirement() {
		return (VerticalScaleRequirement)eGet(DeploymentPackage.Literals.REQUIREMENT_SET__VERTICAL_SCALE_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVerticalScaleRequirement(VerticalScaleRequirement newVerticalScaleRequirement) {
		eSet(DeploymentPackage.Literals.REQUIREMENT_SET__VERTICAL_SCALE_REQUIREMENT, newVerticalScaleRequirement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HorizontalScaleRequirement getHorizontalScaleRequirement() {
		return (HorizontalScaleRequirement)eGet(DeploymentPackage.Literals.REQUIREMENT_SET__HORIZONTAL_SCALE_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setHorizontalScaleRequirement(HorizontalScaleRequirement newHorizontalScaleRequirement) {
		eSet(DeploymentPackage.Literals.REQUIREMENT_SET__HORIZONTAL_SCALE_REQUIREMENT, newHorizontalScaleRequirement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityRequirement getSecurityRequirement() {
		return (SecurityRequirement)eGet(DeploymentPackage.Literals.REQUIREMENT_SET__SECURITY_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecurityRequirement(SecurityRequirement newSecurityRequirement) {
		eSet(DeploymentPackage.Literals.REQUIREMENT_SET__SECURITY_REQUIREMENT, newSecurityRequirement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSRequirement getOsRequirement() {
		return (OSRequirement)eGet(DeploymentPackage.Literals.REQUIREMENT_SET__OS_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOsRequirement(OSRequirement newOsRequirement) {
		eSet(DeploymentPackage.Literals.REQUIREMENT_SET__OS_REQUIREMENT, newOsRequirement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImageRequirement getImageRequirement() {
		return (ImageRequirement)eGet(DeploymentPackage.Literals.REQUIREMENT_SET__IMAGE_REQUIREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setImageRequirement(ImageRequirement newImageRequirement) {
		eSet(DeploymentPackage.Literals.REQUIREMENT_SET__IMAGE_REQUIREMENT, newImageRequirement);
	}

} //RequirementSetImpl
