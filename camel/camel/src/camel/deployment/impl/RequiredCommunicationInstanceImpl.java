/**
 */
package camel.deployment.impl;

import camel.deployment.DeploymentPackage;
import camel.deployment.RequiredCommunicationInstance;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Required Communication Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RequiredCommunicationInstanceImpl extends CommunicationPortInstanceImpl implements RequiredCommunicationInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequiredCommunicationInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.REQUIRED_COMMUNICATION_INSTANCE;
	}

} //RequiredCommunicationInstanceImpl
