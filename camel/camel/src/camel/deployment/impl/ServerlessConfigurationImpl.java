/**
 */
package camel.deployment.impl;

import camel.core.Attribute;

import camel.deployment.BuildConfiguration;
import camel.deployment.DeploymentPackage;
import camel.deployment.EventConfiguration;
import camel.deployment.ServerlessConfiguration;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Serverless Configuration</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.impl.ServerlessConfigurationImpl#getBinaryCodeURL <em>Binary Code URL</em>}</li>
 *   <li>{@link camel.deployment.impl.ServerlessConfigurationImpl#isContinuousDeployment <em>Continuous Deployment</em>}</li>
 *   <li>{@link camel.deployment.impl.ServerlessConfigurationImpl#getEnvironmentConfigParams <em>Environment Config Params</em>}</li>
 *   <li>{@link camel.deployment.impl.ServerlessConfigurationImpl#getBuildConfiguration <em>Build Configuration</em>}</li>
 *   <li>{@link camel.deployment.impl.ServerlessConfigurationImpl#getEventConfiguration <em>Event Configuration</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ServerlessConfigurationImpl extends ConfigurationImpl implements ServerlessConfiguration {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServerlessConfigurationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return DeploymentPackage.Literals.SERVERLESS_CONFIGURATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getBinaryCodeURL() {
		return (String)eGet(DeploymentPackage.Literals.SERVERLESS_CONFIGURATION__BINARY_CODE_URL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBinaryCodeURL(String newBinaryCodeURL) {
		eSet(DeploymentPackage.Literals.SERVERLESS_CONFIGURATION__BINARY_CODE_URL, newBinaryCodeURL);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isContinuousDeployment() {
		return (Boolean)eGet(DeploymentPackage.Literals.SERVERLESS_CONFIGURATION__CONTINUOUS_DEPLOYMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContinuousDeployment(boolean newContinuousDeployment) {
		eSet(DeploymentPackage.Literals.SERVERLESS_CONFIGURATION__CONTINUOUS_DEPLOYMENT, newContinuousDeployment);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Attribute> getEnvironmentConfigParams() {
		return (EList<Attribute>)eGet(DeploymentPackage.Literals.SERVERLESS_CONFIGURATION__ENVIRONMENT_CONFIG_PARAMS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BuildConfiguration getBuildConfiguration() {
		return (BuildConfiguration)eGet(DeploymentPackage.Literals.SERVERLESS_CONFIGURATION__BUILD_CONFIGURATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setBuildConfiguration(BuildConfiguration newBuildConfiguration) {
		eSet(DeploymentPackage.Literals.SERVERLESS_CONFIGURATION__BUILD_CONFIGURATION, newBuildConfiguration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventConfiguration getEventConfiguration() {
		return (EventConfiguration)eGet(DeploymentPackage.Literals.SERVERLESS_CONFIGURATION__EVENT_CONFIGURATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEventConfiguration(EventConfiguration newEventConfiguration) {
		eSet(DeploymentPackage.Literals.SERVERLESS_CONFIGURATION__EVENT_CONFIGURATION, newEventConfiguration);
	}

} //ServerlessConfigurationImpl
