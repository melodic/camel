/**
 */
package camel.deployment;

import camel.data.Data;
import camel.data.DataSource;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Software Component</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.SoftwareComponent#getRequiredCommunications <em>Required Communications</em>}</li>
 *   <li>{@link camel.deployment.SoftwareComponent#getRequiredHost <em>Required Host</em>}</li>
 *   <li>{@link camel.deployment.SoftwareComponent#getRequirementSet <em>Requirement Set</em>}</li>
 *   <li>{@link camel.deployment.SoftwareComponent#getGeneratesData <em>Generates Data</em>}</li>
 *   <li>{@link camel.deployment.SoftwareComponent#getConsumesData <em>Consumes Data</em>}</li>
 *   <li>{@link camel.deployment.SoftwareComponent#getManagesDataSource <em>Manages Data Source</em>}</li>
 *   <li>{@link camel.deployment.SoftwareComponent#isLongLived <em>Long Lived</em>}</li>
 *   <li>{@link camel.deployment.SoftwareComponent#isCoInstanceHosting <em>Co Instance Hosting</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getSoftwareComponent()
 * @model
 * @generated
 */
public interface SoftwareComponent extends Component {
	/**
	 * Returns the value of the '<em><b>Required Communications</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.RequiredCommunication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Communications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Communications</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponent_RequiredCommunications()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequiredCommunication> getRequiredCommunications();

	/**
	 * Returns the value of the '<em><b>Required Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Host</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Host</em>' containment reference.
	 * @see #setRequiredHost(RequiredHost)
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponent_RequiredHost()
	 * @model containment="true"
	 * @generated
	 */
	RequiredHost getRequiredHost();

	/**
	 * Sets the value of the '{@link camel.deployment.SoftwareComponent#getRequiredHost <em>Required Host</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Host</em>' containment reference.
	 * @see #getRequiredHost()
	 * @generated
	 */
	void setRequiredHost(RequiredHost value);

	/**
	 * Returns the value of the '<em><b>Requirement Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirement Set</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirement Set</em>' reference.
	 * @see #setRequirementSet(RequirementSet)
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponent_RequirementSet()
	 * @model
	 * @generated
	 */
	RequirementSet getRequirementSet();

	/**
	 * Sets the value of the '{@link camel.deployment.SoftwareComponent#getRequirementSet <em>Requirement Set</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requirement Set</em>' reference.
	 * @see #getRequirementSet()
	 * @generated
	 */
	void setRequirementSet(RequirementSet value);

	/**
	 * Returns the value of the '<em><b>Generates Data</b></em>' reference list.
	 * The list contents are of type {@link camel.data.Data}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generates Data</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generates Data</em>' reference list.
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponent_GeneratesData()
	 * @model
	 * @generated
	 */
	EList<Data> getGeneratesData();

	/**
	 * Returns the value of the '<em><b>Consumes Data</b></em>' reference list.
	 * The list contents are of type {@link camel.data.Data}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Consumes Data</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consumes Data</em>' reference list.
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponent_ConsumesData()
	 * @model
	 * @generated
	 */
	EList<Data> getConsumesData();

	/**
	 * Returns the value of the '<em><b>Manages Data Source</b></em>' reference list.
	 * The list contents are of type {@link camel.data.DataSource}.
	 * It is bidirectional and its opposite is '{@link camel.data.DataSource#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Manages Data Source</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manages Data Source</em>' reference list.
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponent_ManagesDataSource()
	 * @see camel.data.DataSource#getComponent
	 * @model opposite="component"
	 * @generated
	 */
	EList<DataSource> getManagesDataSource();

	/**
	 * Returns the value of the '<em><b>Long Lived</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Long Lived</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Long Lived</em>' attribute.
	 * @see #setLongLived(boolean)
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponent_LongLived()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isLongLived();

	/**
	 * Sets the value of the '{@link camel.deployment.SoftwareComponent#isLongLived <em>Long Lived</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Long Lived</em>' attribute.
	 * @see #isLongLived()
	 * @generated
	 */
	void setLongLived(boolean value);

	/**
	 * Returns the value of the '<em><b>Co Instance Hosting</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Co Instance Hosting</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Co Instance Hosting</em>' attribute.
	 * @see #setCoInstanceHosting(boolean)
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponent_CoInstanceHosting()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isCoInstanceHosting();

	/**
	 * Sets the value of the '{@link camel.deployment.SoftwareComponent#isCoInstanceHosting <em>Co Instance Hosting</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Co Instance Hosting</em>' attribute.
	 * @see #isCoInstanceHosting()
	 * @generated
	 */
	void setCoInstanceHosting(boolean value);

} // SoftwareComponent
