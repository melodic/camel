/**
 */
package camel.deployment;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Hosting</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.Hosting#getProvidedHost <em>Provided Host</em>}</li>
 *   <li>{@link camel.deployment.Hosting#getRequiredHosts <em>Required Hosts</em>}</li>
 *   <li>{@link camel.deployment.Hosting#getProvidedHostConfiguration <em>Provided Host Configuration</em>}</li>
 *   <li>{@link camel.deployment.Hosting#getRequiredHostsConfiguration <em>Required Hosts Configuration</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getHosting()
 * @model
 * @generated
 */
public interface Hosting extends ComponentRelation {
	/**
	 * Returns the value of the '<em><b>Provided Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Host</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Host</em>' reference.
	 * @see #setProvidedHost(ProvidedHost)
	 * @see camel.deployment.DeploymentPackage#getHosting_ProvidedHost()
	 * @model required="true"
	 * @generated
	 */
	ProvidedHost getProvidedHost();

	/**
	 * Sets the value of the '{@link camel.deployment.Hosting#getProvidedHost <em>Provided Host</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provided Host</em>' reference.
	 * @see #getProvidedHost()
	 * @generated
	 */
	void setProvidedHost(ProvidedHost value);

	/**
	 * Returns the value of the '<em><b>Required Hosts</b></em>' reference list.
	 * The list contents are of type {@link camel.deployment.RequiredHost}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Hosts</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Hosts</em>' reference list.
	 * @see camel.deployment.DeploymentPackage#getHosting_RequiredHosts()
	 * @model required="true"
	 * @generated
	 */
	EList<RequiredHost> getRequiredHosts();

	/**
	 * Returns the value of the '<em><b>Provided Host Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Host Configuration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Host Configuration</em>' containment reference.
	 * @see #setProvidedHostConfiguration(Configuration)
	 * @see camel.deployment.DeploymentPackage#getHosting_ProvidedHostConfiguration()
	 * @model containment="true"
	 * @generated
	 */
	Configuration getProvidedHostConfiguration();

	/**
	 * Sets the value of the '{@link camel.deployment.Hosting#getProvidedHostConfiguration <em>Provided Host Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provided Host Configuration</em>' containment reference.
	 * @see #getProvidedHostConfiguration()
	 * @generated
	 */
	void setProvidedHostConfiguration(Configuration value);

	/**
	 * Returns the value of the '<em><b>Required Hosts Configuration</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.Configuration}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Hosts Configuration</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Hosts Configuration</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getHosting_RequiredHostsConfiguration()
	 * @model containment="true"
	 * @generated
	 */
	EList<Configuration> getRequiredHostsConfiguration();

} // Hosting
