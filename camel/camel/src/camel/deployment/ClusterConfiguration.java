/**
 */
package camel.deployment;

import camel.core.Attribute;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cluster Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.ClusterConfiguration#getDownloadURL <em>Download URL</em>}</li>
 *   <li>{@link camel.deployment.ClusterConfiguration#getConfigParameters <em>Config Parameters</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getClusterConfiguration()
 * @model
 * @generated
 */
public interface ClusterConfiguration extends Configuration {
	/**
	 * Returns the value of the '<em><b>Download URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Download URL</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Download URL</em>' attribute.
	 * @see #setDownloadURL(String)
	 * @see camel.deployment.DeploymentPackage#getClusterConfiguration_DownloadURL()
	 * @model
	 * @generated
	 */
	String getDownloadURL();

	/**
	 * Sets the value of the '{@link camel.deployment.ClusterConfiguration#getDownloadURL <em>Download URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Download URL</em>' attribute.
	 * @see #getDownloadURL()
	 * @generated
	 */
	void setDownloadURL(String value);

	/**
	 * Returns the value of the '<em><b>Config Parameters</b></em>' containment reference list.
	 * The list contents are of type {@link camel.core.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Config Parameters</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Config Parameters</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getClusterConfiguration_ConfigParameters()
	 * @model containment="true"
	 * @generated
	 */
	EList<Attribute> getConfigParameters();

} // ClusterConfiguration
