/**
 */
package camel.deployment;

import camel.data.DataInstance;
import camel.data.DataSourceInstance;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Software Component Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.SoftwareComponentInstance#getRequiredCommunicationInstances <em>Required Communication Instances</em>}</li>
 *   <li>{@link camel.deployment.SoftwareComponentInstance#getRequiredHostInstance <em>Required Host Instance</em>}</li>
 *   <li>{@link camel.deployment.SoftwareComponentInstance#getConsumesDataInstance <em>Consumes Data Instance</em>}</li>
 *   <li>{@link camel.deployment.SoftwareComponentInstance#getGeneratesDataInstance <em>Generates Data Instance</em>}</li>
 *   <li>{@link camel.deployment.SoftwareComponentInstance#getManagesDataSourceInstance <em>Manages Data Source Instance</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getSoftwareComponentInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='sw_component_port_instances_of_correct_type correct_data_instances_sw_component'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot sw_component_port_instances_of_correct_type='Tuple {\n\tmessage : String = \'The type of sw component instance:\' + self.name + \' should have as required communication/hosts the types of the component instance\\\'s required communication/host instances\',\n\tstatus : Boolean = \n\t\t\t\tasError(type.oclIsTypeOf(SoftwareComponent) and requiredCommunicationInstances-&gt;forAll(p | type.oclAsType(SoftwareComponent).requiredCommunications-&gt;includes(p.type)) \n\t\t\t\t\tand (requiredHostInstance &lt;&gt; null implies requiredHostInstance.type = type.oclAsType(SoftwareComponent).requiredHost))\n}.status' correct_data_instances_sw_component='Tuple {\n\tmessage : String = \'SoftwareComponentInstance: \' + self.name + \' should map to instances of consumed, generated or managed instances of data which have a type which is consumed, generated or managed by the type of this instance\',\n\tstatus : Boolean = \n\t\t\t\tasError(consumesDataInstance-&gt;forAll(di | self.type.oclAsType(SoftwareComponent).consumesData-&gt;exists(d | di.type=d)) and \n\t\t\t\tgeneratesDataInstance-&gt;forAll(di | self.type.oclAsType(SoftwareComponent).generatesData-&gt;exists(d | di.type=d)) and\n\t\t\t\tmanagesDataSourceInstance-&gt;forAll(dsi | self.type.oclAsType(SoftwareComponent).managesDataSource-&gt;exists(ds | dsi.type=ds)))\n}.status'"
 * @generated
 */
public interface SoftwareComponentInstance extends ComponentInstance {
	/**
	 * Returns the value of the '<em><b>Required Communication Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.RequiredCommunicationInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Communication Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Communication Instances</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponentInstance_RequiredCommunicationInstances()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequiredCommunicationInstance> getRequiredCommunicationInstances();

	/**
	 * Returns the value of the '<em><b>Required Host Instance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Host Instance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Host Instance</em>' containment reference.
	 * @see #setRequiredHostInstance(RequiredHostInstance)
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponentInstance_RequiredHostInstance()
	 * @model containment="true"
	 * @generated
	 */
	RequiredHostInstance getRequiredHostInstance();

	/**
	 * Sets the value of the '{@link camel.deployment.SoftwareComponentInstance#getRequiredHostInstance <em>Required Host Instance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Host Instance</em>' containment reference.
	 * @see #getRequiredHostInstance()
	 * @generated
	 */
	void setRequiredHostInstance(RequiredHostInstance value);

	/**
	 * Returns the value of the '<em><b>Consumes Data Instance</b></em>' reference list.
	 * The list contents are of type {@link camel.data.DataInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Consumes Data Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Consumes Data Instance</em>' reference list.
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponentInstance_ConsumesDataInstance()
	 * @model
	 * @generated
	 */
	EList<DataInstance> getConsumesDataInstance();

	/**
	 * Returns the value of the '<em><b>Generates Data Instance</b></em>' reference list.
	 * The list contents are of type {@link camel.data.DataInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Generates Data Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Generates Data Instance</em>' reference list.
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponentInstance_GeneratesDataInstance()
	 * @model
	 * @generated
	 */
	EList<DataInstance> getGeneratesDataInstance();

	/**
	 * Returns the value of the '<em><b>Manages Data Source Instance</b></em>' reference list.
	 * The list contents are of type {@link camel.data.DataSourceInstance}.
	 * It is bidirectional and its opposite is '{@link camel.data.DataSourceInstance#getSoftwareComponentInstance <em>Software Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Manages Data Source Instance</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Manages Data Source Instance</em>' reference list.
	 * @see camel.deployment.DeploymentPackage#getSoftwareComponentInstance_ManagesDataSourceInstance()
	 * @see camel.data.DataSourceInstance#getSoftwareComponentInstance
	 * @model opposite="softwareComponentInstance"
	 * @generated
	 */
	EList<DataSourceInstance> getManagesDataSourceInstance();

} // SoftwareComponentInstance
