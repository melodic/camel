/**
 */
package camel.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provided Communication Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.deployment.DeploymentPackage#getProvidedCommunicationInstance()
 * @model
 * @generated
 */
public interface ProvidedCommunicationInstance extends CommunicationPortInstance {
} // ProvidedCommunicationInstance
