/**
 */
package camel.deployment;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.Communication#getProvidedCommunication <em>Provided Communication</em>}</li>
 *   <li>{@link camel.deployment.Communication#getRequiredCommunication <em>Required Communication</em>}</li>
 *   <li>{@link camel.deployment.Communication#getProvidedPortConfiguration <em>Provided Port Configuration</em>}</li>
 *   <li>{@link camel.deployment.Communication#getRequiredPortConfiguration <em>Required Port Configuration</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getCommunication()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='required_port_in_provided_port'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot required_port_in_provided_port='Tuple {\n\tmessage : String = \'Communication: \' + self.name + \' should have the required port (range) to be equal or included in the provided port (range)\',\n\tstatus : Boolean = \n\t\t\t\tasError(\n\t\t\t\t\t((requiredCommunication.portNumber &gt; 0 and providedCommunication.portNumber &gt; 0) implies requiredCommunication.portNumber = providedCommunication.portNumber)\n\t\t\t\t\tand ((requiredCommunication.portNumber &gt; 0 and providedCommunication.portNumber = 0) implies (requiredCommunication.portNumber &gt;= providedCommunication.lowPortNumber and requiredCommunication.portNumber &lt;= providedCommunication.highPortNumber))\n\t\t\t\t\tand (requiredCommunication.portNumber = 0 implies (providedCommunication.portNumber = 0 and requiredCommunication.lowPortNumber &gt;= providedCommunication.lowPortNumber and requiredCommunication.highPortNumber &lt;= providedCommunication.highPortNumber))\n\t\t\t\t)\n}.status'"
 * @generated
 */
public interface Communication extends ComponentRelation {
	/**
	 * Returns the value of the '<em><b>Provided Communication</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Communication</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Communication</em>' reference.
	 * @see #setProvidedCommunication(ProvidedCommunication)
	 * @see camel.deployment.DeploymentPackage#getCommunication_ProvidedCommunication()
	 * @model required="true"
	 * @generated
	 */
	ProvidedCommunication getProvidedCommunication();

	/**
	 * Sets the value of the '{@link camel.deployment.Communication#getProvidedCommunication <em>Provided Communication</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provided Communication</em>' reference.
	 * @see #getProvidedCommunication()
	 * @generated
	 */
	void setProvidedCommunication(ProvidedCommunication value);

	/**
	 * Returns the value of the '<em><b>Required Communication</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Communication</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Communication</em>' reference.
	 * @see #setRequiredCommunication(RequiredCommunication)
	 * @see camel.deployment.DeploymentPackage#getCommunication_RequiredCommunication()
	 * @model required="true"
	 * @generated
	 */
	RequiredCommunication getRequiredCommunication();

	/**
	 * Sets the value of the '{@link camel.deployment.Communication#getRequiredCommunication <em>Required Communication</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Communication</em>' reference.
	 * @see #getRequiredCommunication()
	 * @generated
	 */
	void setRequiredCommunication(RequiredCommunication value);

	/**
	 * Returns the value of the '<em><b>Provided Port Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provided Port Configuration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provided Port Configuration</em>' containment reference.
	 * @see #setProvidedPortConfiguration(Configuration)
	 * @see camel.deployment.DeploymentPackage#getCommunication_ProvidedPortConfiguration()
	 * @model containment="true"
	 * @generated
	 */
	Configuration getProvidedPortConfiguration();

	/**
	 * Sets the value of the '{@link camel.deployment.Communication#getProvidedPortConfiguration <em>Provided Port Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provided Port Configuration</em>' containment reference.
	 * @see #getProvidedPortConfiguration()
	 * @generated
	 */
	void setProvidedPortConfiguration(Configuration value);

	/**
	 * Returns the value of the '<em><b>Required Port Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Port Configuration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Port Configuration</em>' containment reference.
	 * @see #setRequiredPortConfiguration(Configuration)
	 * @see camel.deployment.DeploymentPackage#getCommunication_RequiredPortConfiguration()
	 * @model containment="true"
	 * @generated
	 */
	Configuration getRequiredPortConfiguration();

	/**
	 * Sets the value of the '{@link camel.deployment.Communication#getRequiredPortConfiguration <em>Required Port Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Port Configuration</em>' containment reference.
	 * @see #getRequiredPortConfiguration()
	 * @generated
	 */
	void setRequiredPortConfiguration(Configuration value);

} // Communication
