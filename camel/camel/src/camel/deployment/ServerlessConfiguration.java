/**
 */
package camel.deployment;

import camel.core.Attribute;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Serverless Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.ServerlessConfiguration#getBinaryCodeURL <em>Binary Code URL</em>}</li>
 *   <li>{@link camel.deployment.ServerlessConfiguration#isContinuousDeployment <em>Continuous Deployment</em>}</li>
 *   <li>{@link camel.deployment.ServerlessConfiguration#getEnvironmentConfigParams <em>Environment Config Params</em>}</li>
 *   <li>{@link camel.deployment.ServerlessConfiguration#getBuildConfiguration <em>Build Configuration</em>}</li>
 *   <li>{@link camel.deployment.ServerlessConfiguration#getEventConfiguration <em>Event Configuration</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getServerlessConfiguration()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correct_serverless_config'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot correct_serverless_config='Tuple {\n\tmessage : String = \'ServerlessConfiguration: \' + self.name + \' should have either binary code URL given or a build configuration\',\n\tstatus : Boolean = \n\t\t\t\tasError(\n\t\t\t\t\tbinaryCodeURL &lt;&gt; null xor buildConfiguration &lt;&gt; null\n\t\t\t\t)\n}.status'"
 * @generated
 */
public interface ServerlessConfiguration extends Configuration {
	/**
	 * Returns the value of the '<em><b>Binary Code URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Binary Code URL</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Binary Code URL</em>' attribute.
	 * @see #setBinaryCodeURL(String)
	 * @see camel.deployment.DeploymentPackage#getServerlessConfiguration_BinaryCodeURL()
	 * @model
	 * @generated
	 */
	String getBinaryCodeURL();

	/**
	 * Sets the value of the '{@link camel.deployment.ServerlessConfiguration#getBinaryCodeURL <em>Binary Code URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Binary Code URL</em>' attribute.
	 * @see #getBinaryCodeURL()
	 * @generated
	 */
	void setBinaryCodeURL(String value);

	/**
	 * Returns the value of the '<em><b>Continuous Deployment</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Continuous Deployment</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Continuous Deployment</em>' attribute.
	 * @see #setContinuousDeployment(boolean)
	 * @see camel.deployment.DeploymentPackage#getServerlessConfiguration_ContinuousDeployment()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isContinuousDeployment();

	/**
	 * Sets the value of the '{@link camel.deployment.ServerlessConfiguration#isContinuousDeployment <em>Continuous Deployment</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Continuous Deployment</em>' attribute.
	 * @see #isContinuousDeployment()
	 * @generated
	 */
	void setContinuousDeployment(boolean value);

	/**
	 * Returns the value of the '<em><b>Environment Config Params</b></em>' containment reference list.
	 * The list contents are of type {@link camel.core.Attribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Environment Config Params</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Environment Config Params</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getServerlessConfiguration_EnvironmentConfigParams()
	 * @model containment="true"
	 * @generated
	 */
	EList<Attribute> getEnvironmentConfigParams();

	/**
	 * Returns the value of the '<em><b>Build Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Build Configuration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Build Configuration</em>' containment reference.
	 * @see #setBuildConfiguration(BuildConfiguration)
	 * @see camel.deployment.DeploymentPackage#getServerlessConfiguration_BuildConfiguration()
	 * @model containment="true"
	 * @generated
	 */
	BuildConfiguration getBuildConfiguration();

	/**
	 * Sets the value of the '{@link camel.deployment.ServerlessConfiguration#getBuildConfiguration <em>Build Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Build Configuration</em>' containment reference.
	 * @see #getBuildConfiguration()
	 * @generated
	 */
	void setBuildConfiguration(BuildConfiguration value);

	/**
	 * Returns the value of the '<em><b>Event Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Configuration</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Configuration</em>' containment reference.
	 * @see #setEventConfiguration(EventConfiguration)
	 * @see camel.deployment.DeploymentPackage#getServerlessConfiguration_EventConfiguration()
	 * @model containment="true"
	 * @generated
	 */
	EventConfiguration getEventConfiguration();

	/**
	 * Sets the value of the '{@link camel.deployment.ServerlessConfiguration#getEventConfiguration <em>Event Configuration</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Configuration</em>' containment reference.
	 * @see #getEventConfiguration()
	 * @generated
	 */
	void setEventConfiguration(EventConfiguration value);

} // ServerlessConfiguration
