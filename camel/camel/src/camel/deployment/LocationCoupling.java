/**
 */
package camel.deployment;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Location Coupling</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.LocationCoupling#getSoftwareComponents <em>Software Components</em>}</li>
 *   <li>{@link camel.deployment.LocationCoupling#getCouplingType <em>Coupling Type</em>}</li>
 *   <li>{@link camel.deployment.LocationCoupling#isRelaxed <em>Relaxed</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getLocationCoupling()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='coupled_components_same_host'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot coupled_components_same_host='Tuple {\n\tmessage : String = \'In this coupling:\' + self.name + \' all components should be mapped to the same host\',\n\tstatus : Boolean = \n\t\t\t\tasError(\n\t\t\t\tlet hostings : Hosting[*] = self.oclContainer().oclAsType(DeploymentTypeModel).hostings-&gt;select(p | softwareComponents-&gt;exists(s | p.requiredHosts-&gt;includes(s.requiredHost)))\n\t\t\t\tin \n\t\t\t\t\tif (couplingType = LocationCouplingType::SAME_HOST and hostings-&gt;size() &gt; 0) then softwareComponents-&gt;forAll(p1,p2 | \n\t\t\t\t\t\tif (p1 &lt;&gt; p2) then (hostings-&gt;exists(h1,h2 | h1.requiredHosts-&gt;includes(p1.requiredHost) and h2.requiredHosts-&gt;includes(p2.requiredHost) \n\t\t\t\t\t\t\tand h1.providedHost = h2.providedHost))\n\t\t\t\t\t\telse true\n\t\t\t\t\t\tendif\n\t\t\t\t\t\t)\t\n\t\t\t\t\telse true\n\t\t\t\t\tendif)\n}.status'"
 * @generated
 */
public interface LocationCoupling extends ComponentRelation {
	/**
	 * Returns the value of the '<em><b>Software Components</b></em>' reference list.
	 * The list contents are of type {@link camel.deployment.SoftwareComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software Components</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software Components</em>' reference list.
	 * @see camel.deployment.DeploymentPackage#getLocationCoupling_SoftwareComponents()
	 * @model lower="2"
	 * @generated
	 */
	EList<SoftwareComponent> getSoftwareComponents();

	/**
	 * Returns the value of the '<em><b>Coupling Type</b></em>' attribute.
	 * The default value is <code>"SAME_HOST"</code>.
	 * The literals are from the enumeration {@link camel.deployment.LocationCouplingType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Coupling Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Coupling Type</em>' attribute.
	 * @see camel.deployment.LocationCouplingType
	 * @see #setCouplingType(LocationCouplingType)
	 * @see camel.deployment.DeploymentPackage#getLocationCoupling_CouplingType()
	 * @model default="SAME_HOST" required="true"
	 * @generated
	 */
	LocationCouplingType getCouplingType();

	/**
	 * Sets the value of the '{@link camel.deployment.LocationCoupling#getCouplingType <em>Coupling Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Coupling Type</em>' attribute.
	 * @see camel.deployment.LocationCouplingType
	 * @see #getCouplingType()
	 * @generated
	 */
	void setCouplingType(LocationCouplingType value);

	/**
	 * Returns the value of the '<em><b>Relaxed</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Relaxed</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Relaxed</em>' attribute.
	 * @see #setRelaxed(boolean)
	 * @see camel.deployment.DeploymentPackage#getLocationCoupling_Relaxed()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isRelaxed();

	/**
	 * Sets the value of the '{@link camel.deployment.LocationCoupling#isRelaxed <em>Relaxed</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Relaxed</em>' attribute.
	 * @see #isRelaxed()
	 * @generated
	 */
	void setRelaxed(boolean value);

} // LocationCoupling
