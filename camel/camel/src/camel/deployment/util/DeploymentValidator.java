/**
 */
package camel.deployment.util;

import camel.deployment.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.deployment.DeploymentPackage
 * @generated
 */
public class DeploymentValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final DeploymentValidator INSTANCE = new DeploymentValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.deployment";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return DeploymentPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case DeploymentPackage.DEPLOYMENT_INSTANCE_MODEL:
				return validateDeploymentInstanceModel((DeploymentInstanceModel)value, diagnostics, context);
			case DeploymentPackage.DEPLOYMENT_MODEL:
				return validateDeploymentModel((DeploymentModel)value, diagnostics, context);
			case DeploymentPackage.DEPLOYMENT_TYPE_MODEL:
				return validateDeploymentTypeModel((DeploymentTypeModel)value, diagnostics, context);
			case DeploymentPackage.COMPONENT:
				return validateComponent((Component)value, diagnostics, context);
			case DeploymentPackage.SOFTWARE_COMPONENT:
				return validateSoftwareComponent((SoftwareComponent)value, diagnostics, context);
			case DeploymentPackage.VM:
				return validateVM((VM)value, diagnostics, context);
			case DeploymentPackage.REQUIREMENT_SET:
				return validateRequirementSet((RequirementSet)value, diagnostics, context);
			case DeploymentPackage.CONFIGURATION:
				return validateConfiguration((Configuration)value, diagnostics, context);
			case DeploymentPackage.COMMUNICATION:
				return validateCommunication((Communication)value, diagnostics, context);
			case DeploymentPackage.COMMUNICATION_PORT:
				return validateCommunicationPort((CommunicationPort)value, diagnostics, context);
			case DeploymentPackage.PROVIDED_COMMUNICATION:
				return validateProvidedCommunication((ProvidedCommunication)value, diagnostics, context);
			case DeploymentPackage.REQUIRED_COMMUNICATION:
				return validateRequiredCommunication((RequiredCommunication)value, diagnostics, context);
			case DeploymentPackage.HOSTING:
				return validateHosting((Hosting)value, diagnostics, context);
			case DeploymentPackage.HOSTING_PORT:
				return validateHostingPort((HostingPort)value, diagnostics, context);
			case DeploymentPackage.PROVIDED_HOST:
				return validateProvidedHost((ProvidedHost)value, diagnostics, context);
			case DeploymentPackage.REQUIRED_HOST:
				return validateRequiredHost((RequiredHost)value, diagnostics, context);
			case DeploymentPackage.COMPONENT_INSTANCE:
				return validateComponentInstance((ComponentInstance)value, diagnostics, context);
			case DeploymentPackage.SOFTWARE_COMPONENT_INSTANCE:
				return validateSoftwareComponentInstance((SoftwareComponentInstance)value, diagnostics, context);
			case DeploymentPackage.VM_INSTANCE:
				return validateVMInstance((VMInstance)value, diagnostics, context);
			case DeploymentPackage.COMMUNICATION_INSTANCE:
				return validateCommunicationInstance((CommunicationInstance)value, diagnostics, context);
			case DeploymentPackage.COMMUNICATION_PORT_INSTANCE:
				return validateCommunicationPortInstance((CommunicationPortInstance)value, diagnostics, context);
			case DeploymentPackage.PROVIDED_COMMUNICATION_INSTANCE:
				return validateProvidedCommunicationInstance((ProvidedCommunicationInstance)value, diagnostics, context);
			case DeploymentPackage.REQUIRED_COMMUNICATION_INSTANCE:
				return validateRequiredCommunicationInstance((RequiredCommunicationInstance)value, diagnostics, context);
			case DeploymentPackage.HOSTING_INSTANCE:
				return validateHostingInstance((HostingInstance)value, diagnostics, context);
			case DeploymentPackage.HOSTING_PORT_INSTANCE:
				return validateHostingPortInstance((HostingPortInstance)value, diagnostics, context);
			case DeploymentPackage.PROVIDED_HOST_INSTANCE:
				return validateProvidedHostInstance((ProvidedHostInstance)value, diagnostics, context);
			case DeploymentPackage.REQUIRED_HOST_INSTANCE:
				return validateRequiredHostInstance((RequiredHostInstance)value, diagnostics, context);
			case DeploymentPackage.SCRIPT_CONFIGURATION:
				return validateScriptConfiguration((ScriptConfiguration)value, diagnostics, context);
			case DeploymentPackage.CLUSTER_CONFIGURATION:
				return validateClusterConfiguration((ClusterConfiguration)value, diagnostics, context);
			case DeploymentPackage.LOCATION_COUPLING:
				return validateLocationCoupling((LocationCoupling)value, diagnostics, context);
			case DeploymentPackage.CONTAINER:
				return validateContainer((Container)value, diagnostics, context);
			case DeploymentPackage.CONTAINER_INSTANCE:
				return validateContainerInstance((ContainerInstance)value, diagnostics, context);
			case DeploymentPackage.COMPONENT_RELATION:
				return validateComponentRelation((ComponentRelation)value, diagnostics, context);
			case DeploymentPackage.COMPONENT_RELATION_INSTANCE:
				return validateComponentRelationInstance((ComponentRelationInstance)value, diagnostics, context);
			case DeploymentPackage.PAA_S:
				return validatePaaS((PaaS)value, diagnostics, context);
			case DeploymentPackage.PAA_SINSTANCE:
				return validatePaaSInstance((PaaSInstance)value, diagnostics, context);
			case DeploymentPackage.PAA_SCONFIGURATION:
				return validatePaaSConfiguration((PaaSConfiguration)value, diagnostics, context);
			case DeploymentPackage.SERVERLESS_CONFIGURATION:
				return validateServerlessConfiguration((ServerlessConfiguration)value, diagnostics, context);
			case DeploymentPackage.BUILD_CONFIGURATION:
				return validateBuildConfiguration((BuildConfiguration)value, diagnostics, context);
			case DeploymentPackage.EVENT_CONFIGURATION:
				return validateEventConfiguration((EventConfiguration)value, diagnostics, context);
			case DeploymentPackage.LOCATION_COUPLING_TYPE:
				return validateLocationCouplingType((LocationCouplingType)value, diagnostics, context);
			case DeploymentPackage.HTTP_METHOD_TYPE:
				return validateHTTPMethodType((HTTPMethodType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDeploymentInstanceModel(DeploymentInstanceModel deploymentInstanceModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)deploymentInstanceModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDeploymentModel(DeploymentModel deploymentModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)deploymentModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDeploymentTypeModel(DeploymentTypeModel deploymentTypeModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)deploymentTypeModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponent(Component component, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)component, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoftwareComponent(SoftwareComponent softwareComponent, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)softwareComponent, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVM(VM vm, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)vm, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirementSet(RequirementSet requirementSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)requirementSet, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)requirementSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)requirementSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)requirementSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)requirementSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)requirementSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)requirementSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)requirementSet, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)requirementSet, diagnostics, context);
		if (result || diagnostics != null) result &= validateRequirementSet_at_least_one_req_in_req_set(requirementSet, diagnostics, context);
		if (result || diagnostics != null) result &= validateRequirementSet_osOrImageRequrement(requirementSet, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the at_least_one_req_in_req_set constraint of '<em>Requirement Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT_SET__AT_LEAST_ONE_REQ_IN_REQ_SET__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'RequirementSet: ' + self.name + ' should have at least one requirement referenced',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(resourceRequirement <> null or locationRequirement <> null or providerRequirement <> null or verticalScaleRequirement <> null \n" +
		"\t\t\t\t\tor horizontalScaleRequirement <> null or securityRequirement <> null or osRequirement <> null or imageRequirement <> null or paasRequirement <> null\n" +
		"\t\t\t\t)\n" +
		"}.status";

	/**
	 * Validates the at_least_one_req_in_req_set constraint of '<em>Requirement Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirementSet_at_least_one_req_in_req_set(RequirementSet requirementSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.REQUIREMENT_SET,
				 (EObject)requirementSet,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "at_least_one_req_in_req_set",
				 REQUIREMENT_SET__AT_LEAST_ONE_REQ_IN_REQ_SET__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the osOrImageRequrement constraint of '<em>Requirement Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String REQUIREMENT_SET__OS_OR_IMAGE_REQUREMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'RequirementSet: ' + self.name + 'cannot have both an os and image requirement referenced',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(not(osRequirement <> null and imageRequirement <> null))\n" +
		"}.status";

	/**
	 * Validates the osOrImageRequrement constraint of '<em>Requirement Set</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirementSet_osOrImageRequrement(RequirementSet requirementSet, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.REQUIREMENT_SET,
				 (EObject)requirementSet,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "osOrImageRequrement",
				 REQUIREMENT_SET__OS_OR_IMAGE_REQUREMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateConfiguration(Configuration configuration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)configuration, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunication(Communication communication, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)communication, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)communication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)communication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)communication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)communication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)communication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)communication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)communication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)communication, diagnostics, context);
		if (result || diagnostics != null) result &= validateCommunication_required_port_in_provided_port(communication, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the required_port_in_provided_port constraint of '<em>Communication</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMMUNICATION__REQUIRED_PORT_IN_PROVIDED_PORT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Communication: ' + self.name + ' should have the required port (range) to be equal or included in the provided port (range)',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(\n" +
		"\t\t\t\t\t((requiredCommunication.portNumber > 0 and providedCommunication.portNumber > 0) implies requiredCommunication.portNumber = providedCommunication.portNumber)\n" +
		"\t\t\t\t\tand ((requiredCommunication.portNumber > 0 and providedCommunication.portNumber = 0) implies (requiredCommunication.portNumber >= providedCommunication.lowPortNumber and requiredCommunication.portNumber <= providedCommunication.highPortNumber))\n" +
		"\t\t\t\t\tand (requiredCommunication.portNumber = 0 implies (providedCommunication.portNumber = 0 and requiredCommunication.lowPortNumber >= providedCommunication.lowPortNumber and requiredCommunication.highPortNumber <= providedCommunication.highPortNumber))\n" +
		"\t\t\t\t)\n" +
		"}.status";

	/**
	 * Validates the required_port_in_provided_port constraint of '<em>Communication</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunication_required_port_in_provided_port(Communication communication, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.COMMUNICATION,
				 (EObject)communication,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "required_port_in_provided_port",
				 COMMUNICATION__REQUIRED_PORT_IN_PROVIDED_PORT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationPort(CommunicationPort communicationPort, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)communicationPort, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)communicationPort, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)communicationPort, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)communicationPort, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)communicationPort, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)communicationPort, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)communicationPort, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)communicationPort, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)communicationPort, diagnostics, context);
		if (result || diagnostics != null) result &= validateCommunicationPort_at_least_one_port_number(communicationPort, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the at_least_one_port_number constraint of '<em>Communication Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMMUNICATION_PORT__AT_LEAST_ONE_PORT_NUMBER__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'CommunicationPort: ' + self.name + ' should have either a valid port number or a port range specified',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(\n" +
		"\t\t\t\t\t(portNumber > 0 implies (lowPortNumber = 0 and highPortNumber = 0)) and ((lowPortNumber > 0 and highPortNumber > 0) implies portNumber = 0) and ((portNumber > 0 and portNumber <= 65535) or (lowPortNumber > 0 and highPortNumber > 0 and lowPortNumber < highPortNumber and highPortNumber <= 65535)) \n" +
		"\t\t\t\t)\n" +
		"}.status";

	/**
	 * Validates the at_least_one_port_number constraint of '<em>Communication Port</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationPort_at_least_one_port_number(CommunicationPort communicationPort, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.COMMUNICATION_PORT,
				 (EObject)communicationPort,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "at_least_one_port_number",
				 COMMUNICATION_PORT__AT_LEAST_ONE_PORT_NUMBER__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProvidedCommunication(ProvidedCommunication providedCommunication, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)providedCommunication, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)providedCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)providedCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)providedCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)providedCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)providedCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)providedCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)providedCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)providedCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validateCommunicationPort_at_least_one_port_number(providedCommunication, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequiredCommunication(RequiredCommunication requiredCommunication, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)requiredCommunication, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)requiredCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)requiredCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)requiredCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)requiredCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)requiredCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)requiredCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)requiredCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)requiredCommunication, diagnostics, context);
		if (result || diagnostics != null) result &= validateCommunicationPort_at_least_one_port_number(requiredCommunication, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHosting(Hosting hosting, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)hosting, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHostingPort(HostingPort hostingPort, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)hostingPort, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProvidedHost(ProvidedHost providedHost, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)providedHost, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequiredHost(RequiredHost requiredHost, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)requiredHost, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentInstance(ComponentInstance componentInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)componentInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)componentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)componentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)componentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)componentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)componentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)componentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)componentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)componentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateComponentInstance_component_port_instances_of_correct_type(componentInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the component_port_instances_of_correct_type constraint of '<em>Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMPONENT_INSTANCE__COMPONENT_PORT_INSTANCES_OF_CORRECT_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'The type of component instance:' + self.name + ' should have as provided communication/hosts the types of the component instance\\'s provided communication/host instances',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\tasError(providedCommunicationInstances\n" +
		"\t\t\t\t->forAll(p | type.providedCommunications\n" +
		"\t\t\t\t\t->includes(p.type)) and providedHostInstances\n" +
		"\t\t\t\t->forAll(p | type.providedHosts\n" +
		"\t\t\t\t\t->includes(p.type)))\n" +
		"}.status";

	/**
	 * Validates the component_port_instances_of_correct_type constraint of '<em>Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentInstance_component_port_instances_of_correct_type(ComponentInstance componentInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.COMPONENT_INSTANCE,
				 (EObject)componentInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "component_port_instances_of_correct_type",
				 COMPONENT_INSTANCE__COMPONENT_PORT_INSTANCES_OF_CORRECT_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoftwareComponentInstance(SoftwareComponentInstance softwareComponentInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)softwareComponentInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)softwareComponentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)softwareComponentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)softwareComponentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)softwareComponentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)softwareComponentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)softwareComponentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)softwareComponentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)softwareComponentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateComponentInstance_component_port_instances_of_correct_type(softwareComponentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateSoftwareComponentInstance_sw_component_port_instances_of_correct_type(softwareComponentInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateSoftwareComponentInstance_correct_data_instances_sw_component(softwareComponentInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the sw_component_port_instances_of_correct_type constraint of '<em>Software Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SOFTWARE_COMPONENT_INSTANCE__SW_COMPONENT_PORT_INSTANCES_OF_CORRECT_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'The type of sw component instance:' + self.name + ' should have as required communication/hosts the types of the component instance\\'s required communication/host instances',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(type.oclIsTypeOf(SoftwareComponent) and requiredCommunicationInstances->forAll(p | type.oclAsType(SoftwareComponent).requiredCommunications->includes(p.type)) \n" +
		"\t\t\t\t\tand (requiredHostInstance <> null implies requiredHostInstance.type = type.oclAsType(SoftwareComponent).requiredHost))\n" +
		"}.status";

	/**
	 * Validates the sw_component_port_instances_of_correct_type constraint of '<em>Software Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoftwareComponentInstance_sw_component_port_instances_of_correct_type(SoftwareComponentInstance softwareComponentInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.SOFTWARE_COMPONENT_INSTANCE,
				 (EObject)softwareComponentInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "sw_component_port_instances_of_correct_type",
				 SOFTWARE_COMPONENT_INSTANCE__SW_COMPONENT_PORT_INSTANCES_OF_CORRECT_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the correct_data_instances_sw_component constraint of '<em>Software Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SOFTWARE_COMPONENT_INSTANCE__CORRECT_DATA_INSTANCES_SW_COMPONENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'SoftwareComponentInstance: ' + self.name + ' should map to instances of consumed, generated or managed instances of data which have a type which is consumed, generated or managed by the type of this instance',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(consumesDataInstance->forAll(di | self.type.oclAsType(SoftwareComponent).consumesData->exists(d | di.type=d)) and \n" +
		"\t\t\t\tgeneratesDataInstance->forAll(di | self.type.oclAsType(SoftwareComponent).generatesData->exists(d | di.type=d)) and\n" +
		"\t\t\t\tmanagesDataSourceInstance->forAll(dsi | self.type.oclAsType(SoftwareComponent).managesDataSource->exists(ds | dsi.type=ds)))\n" +
		"}.status";

	/**
	 * Validates the correct_data_instances_sw_component constraint of '<em>Software Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoftwareComponentInstance_correct_data_instances_sw_component(SoftwareComponentInstance softwareComponentInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.SOFTWARE_COMPONENT_INSTANCE,
				 (EObject)softwareComponentInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_data_instances_sw_component",
				 SOFTWARE_COMPONENT_INSTANCE__CORRECT_DATA_INSTANCES_SW_COMPONENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVMInstance(VMInstance vmInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)vmInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)vmInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)vmInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)vmInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)vmInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)vmInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)vmInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)vmInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)vmInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateComponentInstance_component_port_instances_of_correct_type(vmInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateVMInstance_correct_type_for_vm_instance(vmInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the correct_type_for_vm_instance constraint of '<em>VM Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String VM_INSTANCE__CORRECT_TYPE_FOR_VM_INSTANCE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'VM Instance: ' + self.name + ' should have as type a VM',\n" +
		"\tstatus : Boolean = asError(type.oclIsTypeOf(VM))\n" +
		"}.status";

	/**
	 * Validates the correct_type_for_vm_instance constraint of '<em>VM Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVMInstance_correct_type_for_vm_instance(VMInstance vmInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.VM_INSTANCE,
				 (EObject)vmInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_type_for_vm_instance",
				 VM_INSTANCE__CORRECT_TYPE_FOR_VM_INSTANCE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationInstance(CommunicationInstance communicationInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)communicationInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)communicationInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)communicationInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)communicationInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)communicationInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)communicationInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)communicationInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)communicationInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)communicationInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateCommunicationInstance_communication_instance_correct_port_instances(communicationInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the communication_instance_correct_port_instances constraint of '<em>Communication Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMMUNICATION_INSTANCE__COMMUNICATION_INSTANCE_CORRECT_PORT_INSTANCES__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'CommunicationInstance: ' + self.name + ' should have its provided and required communication instances\\' types mapping to its required communication/host type, respectively',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\tasError(requiredCommunicationInstance.type = type.requiredCommunication and providedCommunicationInstance.type = type.providedCommunication)\n" +
		"}.status";

	/**
	 * Validates the communication_instance_correct_port_instances constraint of '<em>Communication Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationInstance_communication_instance_correct_port_instances(CommunicationInstance communicationInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.COMMUNICATION_INSTANCE,
				 (EObject)communicationInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "communication_instance_correct_port_instances",
				 COMMUNICATION_INSTANCE__COMMUNICATION_INSTANCE_CORRECT_PORT_INSTANCES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationPortInstance(CommunicationPortInstance communicationPortInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)communicationPortInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProvidedCommunicationInstance(ProvidedCommunicationInstance providedCommunicationInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)providedCommunicationInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequiredCommunicationInstance(RequiredCommunicationInstance requiredCommunicationInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)requiredCommunicationInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHostingInstance(HostingInstance hostingInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)hostingInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHostingPortInstance(HostingPortInstance hostingPortInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)hostingPortInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProvidedHostInstance(ProvidedHostInstance providedHostInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)providedHostInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequiredHostInstance(RequiredHostInstance requiredHostInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)requiredHostInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScriptConfiguration(ScriptConfiguration scriptConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)scriptConfiguration, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)scriptConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)scriptConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)scriptConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)scriptConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)scriptConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)scriptConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)scriptConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)scriptConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validateScriptConfiguration_script_config_at_least_one_cmd(scriptConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validateScriptConfiguration_script_config_image_or_os(scriptConfiguration, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the script_config_at_least_one_cmd constraint of '<em>Script Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SCRIPT_CONFIGURATION__SCRIPT_CONFIG_AT_LEAST_ONE_CMD__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'ScriptConfiguration: ' + self.name + ' should have at least one command specified',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(imageId = null implies (uploadCommand <> null or installCommand <> null or startCommand <> null or stopCommand <> null\n" +
		"\t\t\t\t\tor downloadCommand <> null or configureCommand <> null or updateCommand <> null)\n" +
		"\t\t\t\t)\n" +
		"}.status";

	/**
	 * Validates the script_config_at_least_one_cmd constraint of '<em>Script Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScriptConfiguration_script_config_at_least_one_cmd(ScriptConfiguration scriptConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.SCRIPT_CONFIGURATION,
				 (EObject)scriptConfiguration,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "script_config_at_least_one_cmd",
				 SCRIPT_CONFIGURATION__SCRIPT_CONFIG_AT_LEAST_ONE_CMD__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the script_config_image_or_os constraint of '<em>Script Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SCRIPT_CONFIGURATION__SCRIPT_CONFIG_IMAGE_OR_OS__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Script Configuration: ' + self.name + ' cannot have both the os and imageId supplied',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(os = null or imageId = null)\n" +
		"}.status";

	/**
	 * Validates the script_config_image_or_os constraint of '<em>Script Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScriptConfiguration_script_config_image_or_os(ScriptConfiguration scriptConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.SCRIPT_CONFIGURATION,
				 (EObject)scriptConfiguration,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "script_config_image_or_os",
				 SCRIPT_CONFIGURATION__SCRIPT_CONFIG_IMAGE_OR_OS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateClusterConfiguration(ClusterConfiguration clusterConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)clusterConfiguration, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLocationCoupling(LocationCoupling locationCoupling, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)locationCoupling, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)locationCoupling, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)locationCoupling, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)locationCoupling, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)locationCoupling, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)locationCoupling, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)locationCoupling, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)locationCoupling, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)locationCoupling, diagnostics, context);
		if (result || diagnostics != null) result &= validateLocationCoupling_coupled_components_same_host(locationCoupling, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the coupled_components_same_host constraint of '<em>Location Coupling</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String LOCATION_COUPLING__COUPLED_COMPONENTS_SAME_HOST__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In this coupling:' + self.name + ' all components should be mapped to the same host',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(\n" +
		"\t\t\t\tlet hostings : Hosting[*] = self.oclContainer().oclAsType(DeploymentTypeModel).hostings->select(p | softwareComponents->exists(s | p.requiredHosts->includes(s.requiredHost)))\n" +
		"\t\t\t\tin \n" +
		"\t\t\t\t\tif (couplingType = LocationCouplingType::SAME_HOST and hostings->size() > 0) then softwareComponents->forAll(p1,p2 | \n" +
		"\t\t\t\t\t\tif (p1 <> p2) then (hostings->exists(h1,h2 | h1.requiredHosts->includes(p1.requiredHost) and h2.requiredHosts->includes(p2.requiredHost) \n" +
		"\t\t\t\t\t\t\tand h1.providedHost = h2.providedHost))\n" +
		"\t\t\t\t\t\telse true\n" +
		"\t\t\t\t\t\tendif\n" +
		"\t\t\t\t\t\t)\t\n" +
		"\t\t\t\t\telse true\n" +
		"\t\t\t\t\tendif)\n" +
		"}.status";

	/**
	 * Validates the coupled_components_same_host constraint of '<em>Location Coupling</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLocationCoupling_coupled_components_same_host(LocationCoupling locationCoupling, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.LOCATION_COUPLING,
				 (EObject)locationCoupling,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "coupled_components_same_host",
				 LOCATION_COUPLING__COUPLED_COMPONENTS_SAME_HOST__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateContainer(Container container, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)container, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateContainerInstance(ContainerInstance containerInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)containerInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)containerInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)containerInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)containerInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)containerInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)containerInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)containerInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)containerInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)containerInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateComponentInstance_component_port_instances_of_correct_type(containerInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateContainerInstance_container_port_instances_of_correct_type(containerInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the container_port_instances_of_correct_type constraint of '<em>Container Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CONTAINER_INSTANCE__CONTAINER_PORT_INSTANCES_OF_CORRECT_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'The type of container instance:' + self.name + ' should have as required communication/hosts the types of the container instance\\'s required communication/host instances',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(type.oclIsTypeOf(Container) and requiredCommunicationInstances->forAll(p | type.oclAsType(Container).requiredCommunications->includes(p.type)) \n" +
		"\t\t\t\t\tand (requiredHostInstance <> null implies requiredHostInstance.type = type.oclAsType(Container).requiredHost))\n" +
		"}.status";

	/**
	 * Validates the container_port_instances_of_correct_type constraint of '<em>Container Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateContainerInstance_container_port_instances_of_correct_type(ContainerInstance containerInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.CONTAINER_INSTANCE,
				 (EObject)containerInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "container_port_instances_of_correct_type",
				 CONTAINER_INSTANCE__CONTAINER_PORT_INSTANCES_OF_CORRECT_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentRelation(ComponentRelation componentRelation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)componentRelation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateComponentRelationInstance(ComponentRelationInstance componentRelationInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)componentRelationInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePaaS(PaaS paaS, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)paaS, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePaaSInstance(PaaSInstance paaSInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)paaSInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)paaSInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)paaSInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)paaSInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)paaSInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)paaSInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)paaSInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)paaSInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)paaSInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateComponentInstance_component_port_instances_of_correct_type(paaSInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validatePaaSInstance_correct_type_for_paas_instance(paaSInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the correct_type_for_paas_instance constraint of '<em>Paa SInstance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PAA_SINSTANCE__CORRECT_TYPE_FOR_PAAS_INSTANCE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'PaaS Instance: ' + self.name + ' should have as type a PaaS',\n" +
		"\tstatus : Boolean = asError(type.oclIsTypeOf(PaaS))\n" +
		"}.status";

	/**
	 * Validates the correct_type_for_paas_instance constraint of '<em>Paa SInstance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePaaSInstance_correct_type_for_paas_instance(PaaSInstance paaSInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.PAA_SINSTANCE,
				 (EObject)paaSInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_type_for_paas_instance",
				 PAA_SINSTANCE__CORRECT_TYPE_FOR_PAAS_INSTANCE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePaaSConfiguration(PaaSConfiguration paaSConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)paaSConfiguration, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateServerlessConfiguration(ServerlessConfiguration serverlessConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)serverlessConfiguration, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)serverlessConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)serverlessConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)serverlessConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)serverlessConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)serverlessConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)serverlessConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)serverlessConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)serverlessConfiguration, diagnostics, context);
		if (result || diagnostics != null) result &= validateServerlessConfiguration_correct_serverless_config(serverlessConfiguration, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the correct_serverless_config constraint of '<em>Serverless Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SERVERLESS_CONFIGURATION__CORRECT_SERVERLESS_CONFIG__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'ServerlessConfiguration: ' + self.name + ' should have either binary code URL given or a build configuration',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(\n" +
		"\t\t\t\t\tbinaryCodeURL <> null xor buildConfiguration <> null\n" +
		"\t\t\t\t)\n" +
		"}.status";

	/**
	 * Validates the correct_serverless_config constraint of '<em>Serverless Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateServerlessConfiguration_correct_serverless_config(ServerlessConfiguration serverlessConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(DeploymentPackage.Literals.SERVERLESS_CONFIGURATION,
				 (EObject)serverlessConfiguration,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_serverless_config",
				 SERVERLESS_CONFIGURATION__CORRECT_SERVERLESS_CONFIG__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateBuildConfiguration(BuildConfiguration buildConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)buildConfiguration, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEventConfiguration(EventConfiguration eventConfiguration, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)eventConfiguration, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLocationCouplingType(LocationCouplingType locationCouplingType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHTTPMethodType(HTTPMethodType httpMethodType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //DeploymentValidator
