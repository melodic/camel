/**
 */
package camel.deployment.util;

import camel.core.Feature;
import camel.core.Model;
import camel.core.NamedElement;

import camel.deployment.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see camel.deployment.DeploymentPackage
 * @generated
 */
public class DeploymentSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DeploymentPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentSwitch() {
		if (modelPackage == null) {
			modelPackage = DeploymentPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case DeploymentPackage.DEPLOYMENT_INSTANCE_MODEL: {
				DeploymentInstanceModel deploymentInstanceModel = (DeploymentInstanceModel)theEObject;
				T result = caseDeploymentInstanceModel(deploymentInstanceModel);
				if (result == null) result = caseDeploymentModel(deploymentInstanceModel);
				if (result == null) result = caseModel(deploymentInstanceModel);
				if (result == null) result = caseFeature(deploymentInstanceModel);
				if (result == null) result = caseNamedElement(deploymentInstanceModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.DEPLOYMENT_MODEL: {
				DeploymentModel deploymentModel = (DeploymentModel)theEObject;
				T result = caseDeploymentModel(deploymentModel);
				if (result == null) result = caseModel(deploymentModel);
				if (result == null) result = caseFeature(deploymentModel);
				if (result == null) result = caseNamedElement(deploymentModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.DEPLOYMENT_TYPE_MODEL: {
				DeploymentTypeModel deploymentTypeModel = (DeploymentTypeModel)theEObject;
				T result = caseDeploymentTypeModel(deploymentTypeModel);
				if (result == null) result = caseDeploymentModel(deploymentTypeModel);
				if (result == null) result = caseModel(deploymentTypeModel);
				if (result == null) result = caseFeature(deploymentTypeModel);
				if (result == null) result = caseNamedElement(deploymentTypeModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.COMPONENT: {
				Component component = (Component)theEObject;
				T result = caseComponent(component);
				if (result == null) result = caseFeature(component);
				if (result == null) result = caseNamedElement(component);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.SOFTWARE_COMPONENT: {
				SoftwareComponent softwareComponent = (SoftwareComponent)theEObject;
				T result = caseSoftwareComponent(softwareComponent);
				if (result == null) result = caseComponent(softwareComponent);
				if (result == null) result = caseFeature(softwareComponent);
				if (result == null) result = caseNamedElement(softwareComponent);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.VM: {
				VM vm = (VM)theEObject;
				T result = caseVM(vm);
				if (result == null) result = caseComponent(vm);
				if (result == null) result = caseFeature(vm);
				if (result == null) result = caseNamedElement(vm);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.REQUIREMENT_SET: {
				RequirementSet requirementSet = (RequirementSet)theEObject;
				T result = caseRequirementSet(requirementSet);
				if (result == null) result = caseFeature(requirementSet);
				if (result == null) result = caseNamedElement(requirementSet);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.CONFIGURATION: {
				Configuration configuration = (Configuration)theEObject;
				T result = caseConfiguration(configuration);
				if (result == null) result = caseFeature(configuration);
				if (result == null) result = caseNamedElement(configuration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.COMMUNICATION: {
				Communication communication = (Communication)theEObject;
				T result = caseCommunication(communication);
				if (result == null) result = caseComponentRelation(communication);
				if (result == null) result = caseFeature(communication);
				if (result == null) result = caseNamedElement(communication);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.COMMUNICATION_PORT: {
				CommunicationPort communicationPort = (CommunicationPort)theEObject;
				T result = caseCommunicationPort(communicationPort);
				if (result == null) result = caseFeature(communicationPort);
				if (result == null) result = caseNamedElement(communicationPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.PROVIDED_COMMUNICATION: {
				ProvidedCommunication providedCommunication = (ProvidedCommunication)theEObject;
				T result = caseProvidedCommunication(providedCommunication);
				if (result == null) result = caseCommunicationPort(providedCommunication);
				if (result == null) result = caseFeature(providedCommunication);
				if (result == null) result = caseNamedElement(providedCommunication);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.REQUIRED_COMMUNICATION: {
				RequiredCommunication requiredCommunication = (RequiredCommunication)theEObject;
				T result = caseRequiredCommunication(requiredCommunication);
				if (result == null) result = caseCommunicationPort(requiredCommunication);
				if (result == null) result = caseFeature(requiredCommunication);
				if (result == null) result = caseNamedElement(requiredCommunication);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.HOSTING: {
				Hosting hosting = (Hosting)theEObject;
				T result = caseHosting(hosting);
				if (result == null) result = caseComponentRelation(hosting);
				if (result == null) result = caseFeature(hosting);
				if (result == null) result = caseNamedElement(hosting);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.HOSTING_PORT: {
				HostingPort hostingPort = (HostingPort)theEObject;
				T result = caseHostingPort(hostingPort);
				if (result == null) result = caseFeature(hostingPort);
				if (result == null) result = caseNamedElement(hostingPort);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.PROVIDED_HOST: {
				ProvidedHost providedHost = (ProvidedHost)theEObject;
				T result = caseProvidedHost(providedHost);
				if (result == null) result = caseHostingPort(providedHost);
				if (result == null) result = caseFeature(providedHost);
				if (result == null) result = caseNamedElement(providedHost);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.REQUIRED_HOST: {
				RequiredHost requiredHost = (RequiredHost)theEObject;
				T result = caseRequiredHost(requiredHost);
				if (result == null) result = caseHostingPort(requiredHost);
				if (result == null) result = caseFeature(requiredHost);
				if (result == null) result = caseNamedElement(requiredHost);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.COMPONENT_INSTANCE: {
				ComponentInstance componentInstance = (ComponentInstance)theEObject;
				T result = caseComponentInstance(componentInstance);
				if (result == null) result = caseFeature(componentInstance);
				if (result == null) result = caseNamedElement(componentInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.SOFTWARE_COMPONENT_INSTANCE: {
				SoftwareComponentInstance softwareComponentInstance = (SoftwareComponentInstance)theEObject;
				T result = caseSoftwareComponentInstance(softwareComponentInstance);
				if (result == null) result = caseComponentInstance(softwareComponentInstance);
				if (result == null) result = caseFeature(softwareComponentInstance);
				if (result == null) result = caseNamedElement(softwareComponentInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.VM_INSTANCE: {
				VMInstance vmInstance = (VMInstance)theEObject;
				T result = caseVMInstance(vmInstance);
				if (result == null) result = caseComponentInstance(vmInstance);
				if (result == null) result = caseFeature(vmInstance);
				if (result == null) result = caseNamedElement(vmInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.COMMUNICATION_INSTANCE: {
				CommunicationInstance communicationInstance = (CommunicationInstance)theEObject;
				T result = caseCommunicationInstance(communicationInstance);
				if (result == null) result = caseComponentRelationInstance(communicationInstance);
				if (result == null) result = caseFeature(communicationInstance);
				if (result == null) result = caseNamedElement(communicationInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.COMMUNICATION_PORT_INSTANCE: {
				CommunicationPortInstance communicationPortInstance = (CommunicationPortInstance)theEObject;
				T result = caseCommunicationPortInstance(communicationPortInstance);
				if (result == null) result = caseFeature(communicationPortInstance);
				if (result == null) result = caseNamedElement(communicationPortInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.PROVIDED_COMMUNICATION_INSTANCE: {
				ProvidedCommunicationInstance providedCommunicationInstance = (ProvidedCommunicationInstance)theEObject;
				T result = caseProvidedCommunicationInstance(providedCommunicationInstance);
				if (result == null) result = caseCommunicationPortInstance(providedCommunicationInstance);
				if (result == null) result = caseFeature(providedCommunicationInstance);
				if (result == null) result = caseNamedElement(providedCommunicationInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.REQUIRED_COMMUNICATION_INSTANCE: {
				RequiredCommunicationInstance requiredCommunicationInstance = (RequiredCommunicationInstance)theEObject;
				T result = caseRequiredCommunicationInstance(requiredCommunicationInstance);
				if (result == null) result = caseCommunicationPortInstance(requiredCommunicationInstance);
				if (result == null) result = caseFeature(requiredCommunicationInstance);
				if (result == null) result = caseNamedElement(requiredCommunicationInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.HOSTING_INSTANCE: {
				HostingInstance hostingInstance = (HostingInstance)theEObject;
				T result = caseHostingInstance(hostingInstance);
				if (result == null) result = caseComponentRelationInstance(hostingInstance);
				if (result == null) result = caseFeature(hostingInstance);
				if (result == null) result = caseNamedElement(hostingInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.HOSTING_PORT_INSTANCE: {
				HostingPortInstance hostingPortInstance = (HostingPortInstance)theEObject;
				T result = caseHostingPortInstance(hostingPortInstance);
				if (result == null) result = caseFeature(hostingPortInstance);
				if (result == null) result = caseNamedElement(hostingPortInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.PROVIDED_HOST_INSTANCE: {
				ProvidedHostInstance providedHostInstance = (ProvidedHostInstance)theEObject;
				T result = caseProvidedHostInstance(providedHostInstance);
				if (result == null) result = caseHostingPortInstance(providedHostInstance);
				if (result == null) result = caseFeature(providedHostInstance);
				if (result == null) result = caseNamedElement(providedHostInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.REQUIRED_HOST_INSTANCE: {
				RequiredHostInstance requiredHostInstance = (RequiredHostInstance)theEObject;
				T result = caseRequiredHostInstance(requiredHostInstance);
				if (result == null) result = caseHostingPortInstance(requiredHostInstance);
				if (result == null) result = caseFeature(requiredHostInstance);
				if (result == null) result = caseNamedElement(requiredHostInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.SCRIPT_CONFIGURATION: {
				ScriptConfiguration scriptConfiguration = (ScriptConfiguration)theEObject;
				T result = caseScriptConfiguration(scriptConfiguration);
				if (result == null) result = caseConfiguration(scriptConfiguration);
				if (result == null) result = caseFeature(scriptConfiguration);
				if (result == null) result = caseNamedElement(scriptConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.CLUSTER_CONFIGURATION: {
				ClusterConfiguration clusterConfiguration = (ClusterConfiguration)theEObject;
				T result = caseClusterConfiguration(clusterConfiguration);
				if (result == null) result = caseConfiguration(clusterConfiguration);
				if (result == null) result = caseFeature(clusterConfiguration);
				if (result == null) result = caseNamedElement(clusterConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.LOCATION_COUPLING: {
				LocationCoupling locationCoupling = (LocationCoupling)theEObject;
				T result = caseLocationCoupling(locationCoupling);
				if (result == null) result = caseComponentRelation(locationCoupling);
				if (result == null) result = caseFeature(locationCoupling);
				if (result == null) result = caseNamedElement(locationCoupling);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.CONTAINER: {
				Container container = (Container)theEObject;
				T result = caseContainer(container);
				if (result == null) result = caseComponent(container);
				if (result == null) result = caseFeature(container);
				if (result == null) result = caseNamedElement(container);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.CONTAINER_INSTANCE: {
				ContainerInstance containerInstance = (ContainerInstance)theEObject;
				T result = caseContainerInstance(containerInstance);
				if (result == null) result = caseComponentInstance(containerInstance);
				if (result == null) result = caseFeature(containerInstance);
				if (result == null) result = caseNamedElement(containerInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.COMPONENT_RELATION: {
				ComponentRelation componentRelation = (ComponentRelation)theEObject;
				T result = caseComponentRelation(componentRelation);
				if (result == null) result = caseFeature(componentRelation);
				if (result == null) result = caseNamedElement(componentRelation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.COMPONENT_RELATION_INSTANCE: {
				ComponentRelationInstance componentRelationInstance = (ComponentRelationInstance)theEObject;
				T result = caseComponentRelationInstance(componentRelationInstance);
				if (result == null) result = caseFeature(componentRelationInstance);
				if (result == null) result = caseNamedElement(componentRelationInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.PAA_S: {
				PaaS paaS = (PaaS)theEObject;
				T result = casePaaS(paaS);
				if (result == null) result = caseComponent(paaS);
				if (result == null) result = caseFeature(paaS);
				if (result == null) result = caseNamedElement(paaS);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.PAA_SINSTANCE: {
				PaaSInstance paaSInstance = (PaaSInstance)theEObject;
				T result = casePaaSInstance(paaSInstance);
				if (result == null) result = caseComponentInstance(paaSInstance);
				if (result == null) result = caseFeature(paaSInstance);
				if (result == null) result = caseNamedElement(paaSInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.PAA_SCONFIGURATION: {
				PaaSConfiguration paaSConfiguration = (PaaSConfiguration)theEObject;
				T result = casePaaSConfiguration(paaSConfiguration);
				if (result == null) result = caseConfiguration(paaSConfiguration);
				if (result == null) result = caseFeature(paaSConfiguration);
				if (result == null) result = caseNamedElement(paaSConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.SERVERLESS_CONFIGURATION: {
				ServerlessConfiguration serverlessConfiguration = (ServerlessConfiguration)theEObject;
				T result = caseServerlessConfiguration(serverlessConfiguration);
				if (result == null) result = caseConfiguration(serverlessConfiguration);
				if (result == null) result = caseFeature(serverlessConfiguration);
				if (result == null) result = caseNamedElement(serverlessConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.BUILD_CONFIGURATION: {
				BuildConfiguration buildConfiguration = (BuildConfiguration)theEObject;
				T result = caseBuildConfiguration(buildConfiguration);
				if (result == null) result = caseFeature(buildConfiguration);
				if (result == null) result = caseNamedElement(buildConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case DeploymentPackage.EVENT_CONFIGURATION: {
				EventConfiguration eventConfiguration = (EventConfiguration)theEObject;
				T result = caseEventConfiguration(eventConfiguration);
				if (result == null) result = caseFeature(eventConfiguration);
				if (result == null) result = caseNamedElement(eventConfiguration);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instance Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instance Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeploymentInstanceModel(DeploymentInstanceModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeploymentModel(DeploymentModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDeploymentTypeModel(DeploymentTypeModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponent(Component object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Software Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Software Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoftwareComponent(SoftwareComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VM</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VM</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVM(VM object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Requirement Set</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Requirement Set</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequirementSet(RequirementSet object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseConfiguration(Configuration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunication(Communication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunicationPort(CommunicationPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Provided Communication</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Provided Communication</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProvidedCommunication(ProvidedCommunication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Required Communication</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Required Communication</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequiredCommunication(RequiredCommunication object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hosting</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hosting</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHosting(Hosting object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hosting Port</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hosting Port</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHostingPort(HostingPort object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Provided Host</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Provided Host</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProvidedHost(ProvidedHost object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Required Host</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Required Host</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequiredHost(RequiredHost object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentInstance(ComponentInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Software Component Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Software Component Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoftwareComponentInstance(SoftwareComponentInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VM Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VM Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVMInstance(VMInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunicationInstance(CommunicationInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Port Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Port Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunicationPortInstance(CommunicationPortInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Provided Communication Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Provided Communication Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProvidedCommunicationInstance(ProvidedCommunicationInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Required Communication Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Required Communication Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequiredCommunicationInstance(RequiredCommunicationInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hosting Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hosting Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHostingInstance(HostingInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Hosting Port Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Hosting Port Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHostingPortInstance(HostingPortInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Provided Host Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Provided Host Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseProvidedHostInstance(ProvidedHostInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Required Host Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Required Host Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRequiredHostInstance(RequiredHostInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Script Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Script Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseScriptConfiguration(ScriptConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cluster Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cluster Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseClusterConfiguration(ClusterConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Location Coupling</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Location Coupling</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseLocationCoupling(LocationCoupling object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Container</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Container</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainer(Container object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Container Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Container Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainerInstance(ContainerInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Relation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Relation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentRelation(ComponentRelation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component Relation Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component Relation Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponentRelationInstance(ComponentRelationInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Paa S</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Paa S</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePaaS(PaaS object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Paa SInstance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Paa SInstance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePaaSInstance(PaaSInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Paa SConfiguration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Paa SConfiguration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePaaSConfiguration(PaaSConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Serverless Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Serverless Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServerlessConfiguration(ServerlessConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Build Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Build Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseBuildConfiguration(BuildConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Event Configuration</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Event Configuration</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEventConfiguration(EventConfiguration object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeature(Feature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModel(Model object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //DeploymentSwitch
