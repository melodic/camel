/**
 */
package camel.deployment.util;

import camel.core.Feature;
import camel.core.Model;
import camel.core.NamedElement;

import camel.deployment.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see camel.deployment.DeploymentPackage
 * @generated
 */
public class DeploymentAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static DeploymentPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = DeploymentPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DeploymentSwitch<Adapter> modelSwitch =
		new DeploymentSwitch<Adapter>() {
			@Override
			public Adapter caseDeploymentInstanceModel(DeploymentInstanceModel object) {
				return createDeploymentInstanceModelAdapter();
			}
			@Override
			public Adapter caseDeploymentModel(DeploymentModel object) {
				return createDeploymentModelAdapter();
			}
			@Override
			public Adapter caseDeploymentTypeModel(DeploymentTypeModel object) {
				return createDeploymentTypeModelAdapter();
			}
			@Override
			public Adapter caseComponent(Component object) {
				return createComponentAdapter();
			}
			@Override
			public Adapter caseSoftwareComponent(SoftwareComponent object) {
				return createSoftwareComponentAdapter();
			}
			@Override
			public Adapter caseVM(VM object) {
				return createVMAdapter();
			}
			@Override
			public Adapter caseRequirementSet(RequirementSet object) {
				return createRequirementSetAdapter();
			}
			@Override
			public Adapter caseConfiguration(Configuration object) {
				return createConfigurationAdapter();
			}
			@Override
			public Adapter caseCommunication(Communication object) {
				return createCommunicationAdapter();
			}
			@Override
			public Adapter caseCommunicationPort(CommunicationPort object) {
				return createCommunicationPortAdapter();
			}
			@Override
			public Adapter caseProvidedCommunication(ProvidedCommunication object) {
				return createProvidedCommunicationAdapter();
			}
			@Override
			public Adapter caseRequiredCommunication(RequiredCommunication object) {
				return createRequiredCommunicationAdapter();
			}
			@Override
			public Adapter caseHosting(Hosting object) {
				return createHostingAdapter();
			}
			@Override
			public Adapter caseHostingPort(HostingPort object) {
				return createHostingPortAdapter();
			}
			@Override
			public Adapter caseProvidedHost(ProvidedHost object) {
				return createProvidedHostAdapter();
			}
			@Override
			public Adapter caseRequiredHost(RequiredHost object) {
				return createRequiredHostAdapter();
			}
			@Override
			public Adapter caseComponentInstance(ComponentInstance object) {
				return createComponentInstanceAdapter();
			}
			@Override
			public Adapter caseSoftwareComponentInstance(SoftwareComponentInstance object) {
				return createSoftwareComponentInstanceAdapter();
			}
			@Override
			public Adapter caseVMInstance(VMInstance object) {
				return createVMInstanceAdapter();
			}
			@Override
			public Adapter caseCommunicationInstance(CommunicationInstance object) {
				return createCommunicationInstanceAdapter();
			}
			@Override
			public Adapter caseCommunicationPortInstance(CommunicationPortInstance object) {
				return createCommunicationPortInstanceAdapter();
			}
			@Override
			public Adapter caseProvidedCommunicationInstance(ProvidedCommunicationInstance object) {
				return createProvidedCommunicationInstanceAdapter();
			}
			@Override
			public Adapter caseRequiredCommunicationInstance(RequiredCommunicationInstance object) {
				return createRequiredCommunicationInstanceAdapter();
			}
			@Override
			public Adapter caseHostingInstance(HostingInstance object) {
				return createHostingInstanceAdapter();
			}
			@Override
			public Adapter caseHostingPortInstance(HostingPortInstance object) {
				return createHostingPortInstanceAdapter();
			}
			@Override
			public Adapter caseProvidedHostInstance(ProvidedHostInstance object) {
				return createProvidedHostInstanceAdapter();
			}
			@Override
			public Adapter caseRequiredHostInstance(RequiredHostInstance object) {
				return createRequiredHostInstanceAdapter();
			}
			@Override
			public Adapter caseScriptConfiguration(ScriptConfiguration object) {
				return createScriptConfigurationAdapter();
			}
			@Override
			public Adapter caseClusterConfiguration(ClusterConfiguration object) {
				return createClusterConfigurationAdapter();
			}
			@Override
			public Adapter caseLocationCoupling(LocationCoupling object) {
				return createLocationCouplingAdapter();
			}
			@Override
			public Adapter caseContainer(Container object) {
				return createContainerAdapter();
			}
			@Override
			public Adapter caseContainerInstance(ContainerInstance object) {
				return createContainerInstanceAdapter();
			}
			@Override
			public Adapter caseComponentRelation(ComponentRelation object) {
				return createComponentRelationAdapter();
			}
			@Override
			public Adapter caseComponentRelationInstance(ComponentRelationInstance object) {
				return createComponentRelationInstanceAdapter();
			}
			@Override
			public Adapter casePaaS(PaaS object) {
				return createPaaSAdapter();
			}
			@Override
			public Adapter casePaaSInstance(PaaSInstance object) {
				return createPaaSInstanceAdapter();
			}
			@Override
			public Adapter casePaaSConfiguration(PaaSConfiguration object) {
				return createPaaSConfigurationAdapter();
			}
			@Override
			public Adapter caseServerlessConfiguration(ServerlessConfiguration object) {
				return createServerlessConfigurationAdapter();
			}
			@Override
			public Adapter caseBuildConfiguration(BuildConfiguration object) {
				return createBuildConfigurationAdapter();
			}
			@Override
			public Adapter caseEventConfiguration(EventConfiguration object) {
				return createEventConfigurationAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseFeature(Feature object) {
				return createFeatureAdapter();
			}
			@Override
			public Adapter caseModel(Model object) {
				return createModelAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.DeploymentInstanceModel <em>Instance Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.DeploymentInstanceModel
	 * @generated
	 */
	public Adapter createDeploymentInstanceModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.DeploymentModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.DeploymentModel
	 * @generated
	 */
	public Adapter createDeploymentModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.DeploymentTypeModel <em>Type Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.DeploymentTypeModel
	 * @generated
	 */
	public Adapter createDeploymentTypeModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.Component
	 * @generated
	 */
	public Adapter createComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.SoftwareComponent <em>Software Component</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.SoftwareComponent
	 * @generated
	 */
	public Adapter createSoftwareComponentAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.VM <em>VM</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.VM
	 * @generated
	 */
	public Adapter createVMAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.RequirementSet <em>Requirement Set</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.RequirementSet
	 * @generated
	 */
	public Adapter createRequirementSetAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.Configuration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.Configuration
	 * @generated
	 */
	public Adapter createConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.Communication <em>Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.Communication
	 * @generated
	 */
	public Adapter createCommunicationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.CommunicationPort <em>Communication Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.CommunicationPort
	 * @generated
	 */
	public Adapter createCommunicationPortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.ProvidedCommunication <em>Provided Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.ProvidedCommunication
	 * @generated
	 */
	public Adapter createProvidedCommunicationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.RequiredCommunication <em>Required Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.RequiredCommunication
	 * @generated
	 */
	public Adapter createRequiredCommunicationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.Hosting <em>Hosting</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.Hosting
	 * @generated
	 */
	public Adapter createHostingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.HostingPort <em>Hosting Port</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.HostingPort
	 * @generated
	 */
	public Adapter createHostingPortAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.ProvidedHost <em>Provided Host</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.ProvidedHost
	 * @generated
	 */
	public Adapter createProvidedHostAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.RequiredHost <em>Required Host</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.RequiredHost
	 * @generated
	 */
	public Adapter createRequiredHostAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.ComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.ComponentInstance
	 * @generated
	 */
	public Adapter createComponentInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.SoftwareComponentInstance <em>Software Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.SoftwareComponentInstance
	 * @generated
	 */
	public Adapter createSoftwareComponentInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.VMInstance <em>VM Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.VMInstance
	 * @generated
	 */
	public Adapter createVMInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.CommunicationInstance <em>Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.CommunicationInstance
	 * @generated
	 */
	public Adapter createCommunicationInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.CommunicationPortInstance <em>Communication Port Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.CommunicationPortInstance
	 * @generated
	 */
	public Adapter createCommunicationPortInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.ProvidedCommunicationInstance <em>Provided Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.ProvidedCommunicationInstance
	 * @generated
	 */
	public Adapter createProvidedCommunicationInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.RequiredCommunicationInstance <em>Required Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.RequiredCommunicationInstance
	 * @generated
	 */
	public Adapter createRequiredCommunicationInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.HostingInstance <em>Hosting Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.HostingInstance
	 * @generated
	 */
	public Adapter createHostingInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.HostingPortInstance <em>Hosting Port Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.HostingPortInstance
	 * @generated
	 */
	public Adapter createHostingPortInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.ProvidedHostInstance <em>Provided Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.ProvidedHostInstance
	 * @generated
	 */
	public Adapter createProvidedHostInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.RequiredHostInstance <em>Required Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.RequiredHostInstance
	 * @generated
	 */
	public Adapter createRequiredHostInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.ScriptConfiguration <em>Script Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.ScriptConfiguration
	 * @generated
	 */
	public Adapter createScriptConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.ClusterConfiguration <em>Cluster Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.ClusterConfiguration
	 * @generated
	 */
	public Adapter createClusterConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.LocationCoupling <em>Location Coupling</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.LocationCoupling
	 * @generated
	 */
	public Adapter createLocationCouplingAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.Container <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.Container
	 * @generated
	 */
	public Adapter createContainerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.ContainerInstance <em>Container Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.ContainerInstance
	 * @generated
	 */
	public Adapter createContainerInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.ComponentRelation <em>Component Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.ComponentRelation
	 * @generated
	 */
	public Adapter createComponentRelationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.ComponentRelationInstance <em>Component Relation Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.ComponentRelationInstance
	 * @generated
	 */
	public Adapter createComponentRelationInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.PaaS <em>Paa S</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.PaaS
	 * @generated
	 */
	public Adapter createPaaSAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.PaaSInstance <em>Paa SInstance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.PaaSInstance
	 * @generated
	 */
	public Adapter createPaaSInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.PaaSConfiguration <em>Paa SConfiguration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.PaaSConfiguration
	 * @generated
	 */
	public Adapter createPaaSConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.ServerlessConfiguration <em>Serverless Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.ServerlessConfiguration
	 * @generated
	 */
	public Adapter createServerlessConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.BuildConfiguration <em>Build Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.BuildConfiguration
	 * @generated
	 */
	public Adapter createBuildConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.deployment.EventConfiguration <em>Event Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.deployment.EventConfiguration
	 * @generated
	 */
	public Adapter createEventConfigurationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.core.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.core.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.core.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.core.Feature
	 * @generated
	 */
	public Adapter createFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.core.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.core.Model
	 * @generated
	 */
	public Adapter createModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //DeploymentAdapterFactory
