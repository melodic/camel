/**
 */
package camel.deployment;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Location Coupling Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see camel.deployment.DeploymentPackage#getLocationCouplingType()
 * @model
 * @generated
 */
public enum LocationCouplingType implements Enumerator {
	/**
	 * The '<em><b>SAME HOST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAME_HOST_VALUE
	 * @generated
	 * @ordered
	 */
	SAME_HOST(0, "SAME_HOST", "SAME_HOST"),

	/**
	 * The '<em><b>SAME ZONE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAME_ZONE_VALUE
	 * @generated
	 * @ordered
	 */
	SAME_ZONE(1, "SAME_ZONE", "SAME_ZONE"),

	/**
	 * The '<em><b>SAME REGION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAME_REGION_VALUE
	 * @generated
	 * @ordered
	 */
	SAME_REGION(2, "SAME_REGION", "SAME_REGION"),

	/**
	 * The '<em><b>SAME CLOUD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #SAME_CLOUD_VALUE
	 * @generated
	 * @ordered
	 */
	SAME_CLOUD(3, "SAME_CLOUD", "SAME_CLOUD");

	/**
	 * The '<em><b>SAME HOST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SAME HOST</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAME_HOST
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SAME_HOST_VALUE = 0;

	/**
	 * The '<em><b>SAME ZONE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SAME ZONE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAME_ZONE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SAME_ZONE_VALUE = 1;

	/**
	 * The '<em><b>SAME REGION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SAME REGION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAME_REGION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SAME_REGION_VALUE = 2;

	/**
	 * The '<em><b>SAME CLOUD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>SAME CLOUD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #SAME_CLOUD
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int SAME_CLOUD_VALUE = 3;

	/**
	 * An array of all the '<em><b>Location Coupling Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final LocationCouplingType[] VALUES_ARRAY =
		new LocationCouplingType[] {
			SAME_HOST,
			SAME_ZONE,
			SAME_REGION,
			SAME_CLOUD,
		};

	/**
	 * A public read-only list of all the '<em><b>Location Coupling Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<LocationCouplingType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Location Coupling Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static LocationCouplingType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LocationCouplingType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Location Coupling Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static LocationCouplingType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			LocationCouplingType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Location Coupling Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static LocationCouplingType get(int value) {
		switch (value) {
			case SAME_HOST_VALUE: return SAME_HOST;
			case SAME_ZONE_VALUE: return SAME_ZONE;
			case SAME_REGION_VALUE: return SAME_REGION;
			case SAME_CLOUD_VALUE: return SAME_CLOUD;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private LocationCouplingType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //LocationCouplingType
