/**
 */
package camel.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Required Communication Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.deployment.DeploymentPackage#getRequiredCommunicationInstance()
 * @model
 * @generated
 */
public interface RequiredCommunicationInstance extends CommunicationPortInstance {
} // RequiredCommunicationInstance
