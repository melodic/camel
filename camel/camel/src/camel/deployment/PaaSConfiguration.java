/**
 */
package camel.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Paa SConfiguration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.PaaSConfiguration#getApi <em>Api</em>}</li>
 *   <li>{@link camel.deployment.PaaSConfiguration#getVersion <em>Version</em>}</li>
 *   <li>{@link camel.deployment.PaaSConfiguration#getEndpoint <em>Endpoint</em>}</li>
 *   <li>{@link camel.deployment.PaaSConfiguration#getDownloadURL <em>Download URL</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getPaaSConfiguration()
 * @model
 * @generated
 */
public interface PaaSConfiguration extends Configuration {
	/**
	 * Returns the value of the '<em><b>Api</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Api</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Api</em>' attribute.
	 * @see #setApi(String)
	 * @see camel.deployment.DeploymentPackage#getPaaSConfiguration_Api()
	 * @model
	 * @generated
	 */
	String getApi();

	/**
	 * Sets the value of the '{@link camel.deployment.PaaSConfiguration#getApi <em>Api</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Api</em>' attribute.
	 * @see #getApi()
	 * @generated
	 */
	void setApi(String value);

	/**
	 * Returns the value of the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Version</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Version</em>' attribute.
	 * @see #setVersion(String)
	 * @see camel.deployment.DeploymentPackage#getPaaSConfiguration_Version()
	 * @model
	 * @generated
	 */
	String getVersion();

	/**
	 * Sets the value of the '{@link camel.deployment.PaaSConfiguration#getVersion <em>Version</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Version</em>' attribute.
	 * @see #getVersion()
	 * @generated
	 */
	void setVersion(String value);

	/**
	 * Returns the value of the '<em><b>Endpoint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Endpoint</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Endpoint</em>' attribute.
	 * @see #setEndpoint(String)
	 * @see camel.deployment.DeploymentPackage#getPaaSConfiguration_Endpoint()
	 * @model
	 * @generated
	 */
	String getEndpoint();

	/**
	 * Sets the value of the '{@link camel.deployment.PaaSConfiguration#getEndpoint <em>Endpoint</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Endpoint</em>' attribute.
	 * @see #getEndpoint()
	 * @generated
	 */
	void setEndpoint(String value);

	/**
	 * Returns the value of the '<em><b>Download URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Download URL</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Download URL</em>' attribute.
	 * @see #setDownloadURL(String)
	 * @see camel.deployment.DeploymentPackage#getPaaSConfiguration_DownloadURL()
	 * @model
	 * @generated
	 */
	String getDownloadURL();

	/**
	 * Sets the value of the '{@link camel.deployment.PaaSConfiguration#getDownloadURL <em>Download URL</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Download URL</em>' attribute.
	 * @see #getDownloadURL()
	 * @generated
	 */
	void setDownloadURL(String value);

} // PaaSConfiguration
