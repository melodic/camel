/**
 */
package camel.deployment;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Component Relation</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.deployment.DeploymentPackage#getComponentRelation()
 * @model abstract="true"
 * @generated
 */
public interface ComponentRelation extends Feature {
} // ComponentRelation
