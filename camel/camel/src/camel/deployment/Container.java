/**
 */
package camel.deployment;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.Container#getRequiredCommunications <em>Required Communications</em>}</li>
 *   <li>{@link camel.deployment.Container#getRequiredHost <em>Required Host</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getContainer()
 * @model
 * @generated
 */
public interface Container extends Component {
	/**
	 * Returns the value of the '<em><b>Required Communications</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.RequiredCommunication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Communications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Communications</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getContainer_RequiredCommunications()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequiredCommunication> getRequiredCommunications();

	/**
	 * Returns the value of the '<em><b>Required Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Host</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Host</em>' containment reference.
	 * @see #setRequiredHost(RequiredHost)
	 * @see camel.deployment.DeploymentPackage#getContainer_RequiredHost()
	 * @model containment="true"
	 * @generated
	 */
	RequiredHost getRequiredHost();

	/**
	 * Sets the value of the '{@link camel.deployment.Container#getRequiredHost <em>Required Host</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Host</em>' containment reference.
	 * @see #getRequiredHost()
	 * @generated
	 */
	void setRequiredHost(RequiredHost value);

} // Container
