/**
 */
package camel.deployment;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Port</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.CommunicationPort#getPortNumber <em>Port Number</em>}</li>
 *   <li>{@link camel.deployment.CommunicationPort#getLowPortNumber <em>Low Port Number</em>}</li>
 *   <li>{@link camel.deployment.CommunicationPort#getHighPortNumber <em>High Port Number</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getCommunicationPort()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='at_least_one_port_number'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot at_least_one_port_number='Tuple {\n\tmessage : String = \'CommunicationPort: \' + self.name + \' should have either a valid port number or a port range specified\',\n\tstatus : Boolean = \n\t\t\t\tasError(\n\t\t\t\t\t(portNumber &gt; 0 implies (lowPortNumber = 0 and highPortNumber = 0)) and ((lowPortNumber &gt; 0 and highPortNumber &gt; 0) implies portNumber = 0) and ((portNumber &gt; 0 and portNumber &lt;= 65535) or (lowPortNumber &gt; 0 and highPortNumber &gt; 0 and lowPortNumber &lt; highPortNumber and highPortNumber &lt;= 65535)) \n\t\t\t\t)\n}.status'"
 * @generated
 */
public interface CommunicationPort extends Feature {
	/**
	 * Returns the value of the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Port Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Port Number</em>' attribute.
	 * @see #setPortNumber(int)
	 * @see camel.deployment.DeploymentPackage#getCommunicationPort_PortNumber()
	 * @model required="true"
	 * @generated
	 */
	int getPortNumber();

	/**
	 * Sets the value of the '{@link camel.deployment.CommunicationPort#getPortNumber <em>Port Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Port Number</em>' attribute.
	 * @see #getPortNumber()
	 * @generated
	 */
	void setPortNumber(int value);

	/**
	 * Returns the value of the '<em><b>Low Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Low Port Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Low Port Number</em>' attribute.
	 * @see #setLowPortNumber(int)
	 * @see camel.deployment.DeploymentPackage#getCommunicationPort_LowPortNumber()
	 * @model required="true"
	 * @generated
	 */
	int getLowPortNumber();

	/**
	 * Sets the value of the '{@link camel.deployment.CommunicationPort#getLowPortNumber <em>Low Port Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Low Port Number</em>' attribute.
	 * @see #getLowPortNumber()
	 * @generated
	 */
	void setLowPortNumber(int value);

	/**
	 * Returns the value of the '<em><b>High Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>High Port Number</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>High Port Number</em>' attribute.
	 * @see #setHighPortNumber(int)
	 * @see camel.deployment.DeploymentPackage#getCommunicationPort_HighPortNumber()
	 * @model required="true"
	 * @generated
	 */
	int getHighPortNumber();

	/**
	 * Sets the value of the '{@link camel.deployment.CommunicationPort#getHighPortNumber <em>High Port Number</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>High Port Number</em>' attribute.
	 * @see #getHighPortNumber()
	 * @generated
	 */
	void setHighPortNumber(int value);

} // CommunicationPort
