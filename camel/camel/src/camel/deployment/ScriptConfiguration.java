/**
 */
package camel.deployment;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Script Configuration</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.ScriptConfiguration#getUploadCommand <em>Upload Command</em>}</li>
 *   <li>{@link camel.deployment.ScriptConfiguration#getInstallCommand <em>Install Command</em>}</li>
 *   <li>{@link camel.deployment.ScriptConfiguration#getStartCommand <em>Start Command</em>}</li>
 *   <li>{@link camel.deployment.ScriptConfiguration#getStopCommand <em>Stop Command</em>}</li>
 *   <li>{@link camel.deployment.ScriptConfiguration#getDownloadCommand <em>Download Command</em>}</li>
 *   <li>{@link camel.deployment.ScriptConfiguration#getConfigureCommand <em>Configure Command</em>}</li>
 *   <li>{@link camel.deployment.ScriptConfiguration#getUpdateCommand <em>Update Command</em>}</li>
 *   <li>{@link camel.deployment.ScriptConfiguration#getDevopsTool <em>Devops Tool</em>}</li>
 *   <li>{@link camel.deployment.ScriptConfiguration#getOs <em>Os</em>}</li>
 *   <li>{@link camel.deployment.ScriptConfiguration#getImageId <em>Image Id</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getScriptConfiguration()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='script_config_at_least_one_cmd script_config_image_or_os'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot script_config_at_least_one_cmd='Tuple {\n\tmessage : String = \'ScriptConfiguration: \' + self.name + \' should have at least one command specified\',\n\tstatus : Boolean = \n\t\t\t\tasError(imageId = null implies (uploadCommand &lt;&gt; null or installCommand &lt;&gt; null or startCommand &lt;&gt; null or stopCommand &lt;&gt; null\n\t\t\t\t\tor downloadCommand &lt;&gt; null or configureCommand &lt;&gt; null or updateCommand &lt;&gt; null)\n\t\t\t\t)\n}.status' script_config_image_or_os='Tuple {\n\tmessage : String = \'Script Configuration: \' + self.name + \' cannot have both the os and imageId supplied\',\n\tstatus : Boolean = \n\t\t\t\tasError(os = null or imageId = null)\n}.status'"
 * @generated
 */
public interface ScriptConfiguration extends Configuration {
	/**
	 * Returns the value of the '<em><b>Upload Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Upload Command</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Upload Command</em>' attribute.
	 * @see #setUploadCommand(String)
	 * @see camel.deployment.DeploymentPackage#getScriptConfiguration_UploadCommand()
	 * @model
	 * @generated
	 */
	String getUploadCommand();

	/**
	 * Sets the value of the '{@link camel.deployment.ScriptConfiguration#getUploadCommand <em>Upload Command</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Upload Command</em>' attribute.
	 * @see #getUploadCommand()
	 * @generated
	 */
	void setUploadCommand(String value);

	/**
	 * Returns the value of the '<em><b>Install Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Install Command</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Install Command</em>' attribute.
	 * @see #setInstallCommand(String)
	 * @see camel.deployment.DeploymentPackage#getScriptConfiguration_InstallCommand()
	 * @model
	 * @generated
	 */
	String getInstallCommand();

	/**
	 * Sets the value of the '{@link camel.deployment.ScriptConfiguration#getInstallCommand <em>Install Command</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Install Command</em>' attribute.
	 * @see #getInstallCommand()
	 * @generated
	 */
	void setInstallCommand(String value);

	/**
	 * Returns the value of the '<em><b>Start Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Command</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Command</em>' attribute.
	 * @see #setStartCommand(String)
	 * @see camel.deployment.DeploymentPackage#getScriptConfiguration_StartCommand()
	 * @model
	 * @generated
	 */
	String getStartCommand();

	/**
	 * Sets the value of the '{@link camel.deployment.ScriptConfiguration#getStartCommand <em>Start Command</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Command</em>' attribute.
	 * @see #getStartCommand()
	 * @generated
	 */
	void setStartCommand(String value);

	/**
	 * Returns the value of the '<em><b>Stop Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Stop Command</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Stop Command</em>' attribute.
	 * @see #setStopCommand(String)
	 * @see camel.deployment.DeploymentPackage#getScriptConfiguration_StopCommand()
	 * @model
	 * @generated
	 */
	String getStopCommand();

	/**
	 * Sets the value of the '{@link camel.deployment.ScriptConfiguration#getStopCommand <em>Stop Command</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Stop Command</em>' attribute.
	 * @see #getStopCommand()
	 * @generated
	 */
	void setStopCommand(String value);

	/**
	 * Returns the value of the '<em><b>Download Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Download Command</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Download Command</em>' attribute.
	 * @see #setDownloadCommand(String)
	 * @see camel.deployment.DeploymentPackage#getScriptConfiguration_DownloadCommand()
	 * @model
	 * @generated
	 */
	String getDownloadCommand();

	/**
	 * Sets the value of the '{@link camel.deployment.ScriptConfiguration#getDownloadCommand <em>Download Command</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Download Command</em>' attribute.
	 * @see #getDownloadCommand()
	 * @generated
	 */
	void setDownloadCommand(String value);

	/**
	 * Returns the value of the '<em><b>Configure Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configure Command</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configure Command</em>' attribute.
	 * @see #setConfigureCommand(String)
	 * @see camel.deployment.DeploymentPackage#getScriptConfiguration_ConfigureCommand()
	 * @model
	 * @generated
	 */
	String getConfigureCommand();

	/**
	 * Sets the value of the '{@link camel.deployment.ScriptConfiguration#getConfigureCommand <em>Configure Command</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configure Command</em>' attribute.
	 * @see #getConfigureCommand()
	 * @generated
	 */
	void setConfigureCommand(String value);

	/**
	 * Returns the value of the '<em><b>Update Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Update Command</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Update Command</em>' attribute.
	 * @see #setUpdateCommand(String)
	 * @see camel.deployment.DeploymentPackage#getScriptConfiguration_UpdateCommand()
	 * @model
	 * @generated
	 */
	String getUpdateCommand();

	/**
	 * Sets the value of the '{@link camel.deployment.ScriptConfiguration#getUpdateCommand <em>Update Command</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Update Command</em>' attribute.
	 * @see #getUpdateCommand()
	 * @generated
	 */
	void setUpdateCommand(String value);

	/**
	 * Returns the value of the '<em><b>Devops Tool</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Devops Tool</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Devops Tool</em>' attribute.
	 * @see #setDevopsTool(String)
	 * @see camel.deployment.DeploymentPackage#getScriptConfiguration_DevopsTool()
	 * @model
	 * @generated
	 */
	String getDevopsTool();

	/**
	 * Sets the value of the '{@link camel.deployment.ScriptConfiguration#getDevopsTool <em>Devops Tool</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Devops Tool</em>' attribute.
	 * @see #getDevopsTool()
	 * @generated
	 */
	void setDevopsTool(String value);

	/**
	 * Returns the value of the '<em><b>Os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Os</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Os</em>' attribute.
	 * @see #setOs(String)
	 * @see camel.deployment.DeploymentPackage#getScriptConfiguration_Os()
	 * @model
	 * @generated
	 */
	String getOs();

	/**
	 * Sets the value of the '{@link camel.deployment.ScriptConfiguration#getOs <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Os</em>' attribute.
	 * @see #getOs()
	 * @generated
	 */
	void setOs(String value);

	/**
	 * Returns the value of the '<em><b>Image Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Id</em>' attribute.
	 * @see #setImageId(String)
	 * @see camel.deployment.DeploymentPackage#getScriptConfiguration_ImageId()
	 * @model
	 * @generated
	 */
	String getImageId();

	/**
	 * Sets the value of the '{@link camel.deployment.ScriptConfiguration#getImageId <em>Image Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Id</em>' attribute.
	 * @see #getImageId()
	 * @generated
	 */
	void setImageId(String value);

} // ScriptConfiguration
