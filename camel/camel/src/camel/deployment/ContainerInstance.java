/**
 */
package camel.deployment;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.ContainerInstance#getRequiredCommunicationInstances <em>Required Communication Instances</em>}</li>
 *   <li>{@link camel.deployment.ContainerInstance#getRequiredHostInstance <em>Required Host Instance</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getContainerInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='container_port_instances_of_correct_type'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot container_port_instances_of_correct_type='Tuple {\n\tmessage : String = \'The type of container instance:\' + self.name + \' should have as required communication/hosts the types of the container instance\\\'s required communication/host instances\',\n\tstatus : Boolean = \n\t\t\t\tasError(type.oclIsTypeOf(Container) and requiredCommunicationInstances-&gt;forAll(p | type.oclAsType(Container).requiredCommunications-&gt;includes(p.type)) \n\t\t\t\t\tand (requiredHostInstance &lt;&gt; null implies requiredHostInstance.type = type.oclAsType(Container).requiredHost))\n}.status'"
 * @generated
 */
public interface ContainerInstance extends ComponentInstance {
	/**
	 * Returns the value of the '<em><b>Required Communication Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.RequiredCommunicationInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Communication Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Communication Instances</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getContainerInstance_RequiredCommunicationInstances()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequiredCommunicationInstance> getRequiredCommunicationInstances();

	/**
	 * Returns the value of the '<em><b>Required Host Instance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Required Host Instance</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Required Host Instance</em>' containment reference.
	 * @see #setRequiredHostInstance(RequiredHostInstance)
	 * @see camel.deployment.DeploymentPackage#getContainerInstance_RequiredHostInstance()
	 * @model containment="true"
	 * @generated
	 */
	RequiredHostInstance getRequiredHostInstance();

	/**
	 * Sets the value of the '{@link camel.deployment.ContainerInstance#getRequiredHostInstance <em>Required Host Instance</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Required Host Instance</em>' containment reference.
	 * @see #getRequiredHostInstance()
	 * @generated
	 */
	void setRequiredHostInstance(RequiredHostInstance value);

} // ContainerInstance
