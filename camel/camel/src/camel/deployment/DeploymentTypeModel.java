/**
 */
package camel.deployment;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.DeploymentTypeModel#getSoftwareComponents <em>Software Components</em>}</li>
 *   <li>{@link camel.deployment.DeploymentTypeModel#getVms <em>Vms</em>}</li>
 *   <li>{@link camel.deployment.DeploymentTypeModel#getPaases <em>Paases</em>}</li>
 *   <li>{@link camel.deployment.DeploymentTypeModel#getCommunications <em>Communications</em>}</li>
 *   <li>{@link camel.deployment.DeploymentTypeModel#getHostings <em>Hostings</em>}</li>
 *   <li>{@link camel.deployment.DeploymentTypeModel#getRequirementSets <em>Requirement Sets</em>}</li>
 *   <li>{@link camel.deployment.DeploymentTypeModel#getGlobalRequirementSet <em>Global Requirement Set</em>}</li>
 *   <li>{@link camel.deployment.DeploymentTypeModel#getLocationCouplings <em>Location Couplings</em>}</li>
 *   <li>{@link camel.deployment.DeploymentTypeModel#getContainers <em>Containers</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getDeploymentTypeModel()
 * @model
 * @generated
 */
public interface DeploymentTypeModel extends DeploymentModel {
	/**
	 * Returns the value of the '<em><b>Software Components</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.SoftwareComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software Components</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software Components</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentTypeModel_SoftwareComponents()
	 * @model containment="true"
	 * @generated
	 */
	EList<SoftwareComponent> getSoftwareComponents();

	/**
	 * Returns the value of the '<em><b>Vms</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.VM}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vms</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentTypeModel_Vms()
	 * @model containment="true"
	 * @generated
	 */
	EList<VM> getVms();

	/**
	 * Returns the value of the '<em><b>Paases</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.PaaS}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Paases</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Paases</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentTypeModel_Paases()
	 * @model containment="true"
	 * @generated
	 */
	EList<PaaS> getPaases();

	/**
	 * Returns the value of the '<em><b>Communications</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.Communication}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Communications</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Communications</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentTypeModel_Communications()
	 * @model containment="true"
	 * @generated
	 */
	EList<Communication> getCommunications();

	/**
	 * Returns the value of the '<em><b>Hostings</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.Hosting}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Hostings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Hostings</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentTypeModel_Hostings()
	 * @model containment="true"
	 * @generated
	 */
	EList<Hosting> getHostings();

	/**
	 * Returns the value of the '<em><b>Requirement Sets</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.RequirementSet}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirement Sets</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirement Sets</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentTypeModel_RequirementSets()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequirementSet> getRequirementSets();

	/**
	 * Returns the value of the '<em><b>Global Requirement Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Global Requirement Set</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Global Requirement Set</em>' reference.
	 * @see #setGlobalRequirementSet(RequirementSet)
	 * @see camel.deployment.DeploymentPackage#getDeploymentTypeModel_GlobalRequirementSet()
	 * @model
	 * @generated
	 */
	RequirementSet getGlobalRequirementSet();

	/**
	 * Sets the value of the '{@link camel.deployment.DeploymentTypeModel#getGlobalRequirementSet <em>Global Requirement Set</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Global Requirement Set</em>' reference.
	 * @see #getGlobalRequirementSet()
	 * @generated
	 */
	void setGlobalRequirementSet(RequirementSet value);

	/**
	 * Returns the value of the '<em><b>Location Couplings</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.LocationCoupling}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location Couplings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location Couplings</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentTypeModel_LocationCouplings()
	 * @model containment="true"
	 * @generated
	 */
	EList<LocationCoupling> getLocationCouplings();

	/**
	 * Returns the value of the '<em><b>Containers</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.Container}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Containers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Containers</em>' containment reference list.
	 * @see camel.deployment.DeploymentPackage#getDeploymentTypeModel_Containers()
	 * @model containment="true"
	 * @generated
	 */
	EList<Container> getContainers();

} // DeploymentTypeModel
