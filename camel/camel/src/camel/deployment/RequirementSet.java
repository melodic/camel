/**
 */
package camel.deployment;

import camel.core.Feature;

import camel.requirement.HorizontalScaleRequirement;
import camel.requirement.ImageRequirement;
import camel.requirement.LocationRequirement;
import camel.requirement.OSRequirement;
import camel.requirement.PaaSRequirement;
import camel.requirement.ProviderRequirement;
import camel.requirement.ResourceRequirement;
import camel.requirement.SecurityRequirement;
import camel.requirement.VerticalScaleRequirement;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Requirement Set</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.deployment.RequirementSet#getResourceRequirement <em>Resource Requirement</em>}</li>
 *   <li>{@link camel.deployment.RequirementSet#getPaasRequirement <em>Paas Requirement</em>}</li>
 *   <li>{@link camel.deployment.RequirementSet#getLocationRequirement <em>Location Requirement</em>}</li>
 *   <li>{@link camel.deployment.RequirementSet#getProviderRequirement <em>Provider Requirement</em>}</li>
 *   <li>{@link camel.deployment.RequirementSet#getVerticalScaleRequirement <em>Vertical Scale Requirement</em>}</li>
 *   <li>{@link camel.deployment.RequirementSet#getHorizontalScaleRequirement <em>Horizontal Scale Requirement</em>}</li>
 *   <li>{@link camel.deployment.RequirementSet#getSecurityRequirement <em>Security Requirement</em>}</li>
 *   <li>{@link camel.deployment.RequirementSet#getOsRequirement <em>Os Requirement</em>}</li>
 *   <li>{@link camel.deployment.RequirementSet#getImageRequirement <em>Image Requirement</em>}</li>
 * </ul>
 *
 * @see camel.deployment.DeploymentPackage#getRequirementSet()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='at_least_one_req_in_req_set osOrImageRequrement'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot at_least_one_req_in_req_set='Tuple {\n\tmessage : String = \'RequirementSet: \' + self.name + \' should have at least one requirement referenced\',\n\tstatus : Boolean = \n\t\t\t\tasError(resourceRequirement &lt;&gt; null or locationRequirement &lt;&gt; null or providerRequirement &lt;&gt; null or verticalScaleRequirement &lt;&gt; null \n\t\t\t\t\tor horizontalScaleRequirement &lt;&gt; null or securityRequirement &lt;&gt; null or osRequirement &lt;&gt; null or imageRequirement &lt;&gt; null or paasRequirement &lt;&gt; null\n\t\t\t\t)\n}.status' osOrImageRequrement='Tuple {\n\tmessage : String = \'RequirementSet: \' + self.name + \'cannot have both an os and image requirement referenced\',\n\tstatus : Boolean = \n\t\t\t\tasError(not(osRequirement &lt;&gt; null and imageRequirement &lt;&gt; null))\n}.status'"
 * @generated
 */
public interface RequirementSet extends Feature {
	/**
	 * Returns the value of the '<em><b>Resource Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Requirement</em>' reference.
	 * @see #setResourceRequirement(ResourceRequirement)
	 * @see camel.deployment.DeploymentPackage#getRequirementSet_ResourceRequirement()
	 * @model
	 * @generated
	 */
	ResourceRequirement getResourceRequirement();

	/**
	 * Sets the value of the '{@link camel.deployment.RequirementSet#getResourceRequirement <em>Resource Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Requirement</em>' reference.
	 * @see #getResourceRequirement()
	 * @generated
	 */
	void setResourceRequirement(ResourceRequirement value);

	/**
	 * Returns the value of the '<em><b>Paas Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Paas Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Paas Requirement</em>' reference.
	 * @see #setPaasRequirement(PaaSRequirement)
	 * @see camel.deployment.DeploymentPackage#getRequirementSet_PaasRequirement()
	 * @model
	 * @generated
	 */
	PaaSRequirement getPaasRequirement();

	/**
	 * Sets the value of the '{@link camel.deployment.RequirementSet#getPaasRequirement <em>Paas Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Paas Requirement</em>' reference.
	 * @see #getPaasRequirement()
	 * @generated
	 */
	void setPaasRequirement(PaaSRequirement value);

	/**
	 * Returns the value of the '<em><b>Location Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location Requirement</em>' reference.
	 * @see #setLocationRequirement(LocationRequirement)
	 * @see camel.deployment.DeploymentPackage#getRequirementSet_LocationRequirement()
	 * @model
	 * @generated
	 */
	LocationRequirement getLocationRequirement();

	/**
	 * Sets the value of the '{@link camel.deployment.RequirementSet#getLocationRequirement <em>Location Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Location Requirement</em>' reference.
	 * @see #getLocationRequirement()
	 * @generated
	 */
	void setLocationRequirement(LocationRequirement value);

	/**
	 * Returns the value of the '<em><b>Provider Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provider Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provider Requirement</em>' reference.
	 * @see #setProviderRequirement(ProviderRequirement)
	 * @see camel.deployment.DeploymentPackage#getRequirementSet_ProviderRequirement()
	 * @model
	 * @generated
	 */
	ProviderRequirement getProviderRequirement();

	/**
	 * Sets the value of the '{@link camel.deployment.RequirementSet#getProviderRequirement <em>Provider Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Provider Requirement</em>' reference.
	 * @see #getProviderRequirement()
	 * @generated
	 */
	void setProviderRequirement(ProviderRequirement value);

	/**
	 * Returns the value of the '<em><b>Vertical Scale Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vertical Scale Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vertical Scale Requirement</em>' reference.
	 * @see #setVerticalScaleRequirement(VerticalScaleRequirement)
	 * @see camel.deployment.DeploymentPackage#getRequirementSet_VerticalScaleRequirement()
	 * @model
	 * @generated
	 */
	VerticalScaleRequirement getVerticalScaleRequirement();

	/**
	 * Sets the value of the '{@link camel.deployment.RequirementSet#getVerticalScaleRequirement <em>Vertical Scale Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vertical Scale Requirement</em>' reference.
	 * @see #getVerticalScaleRequirement()
	 * @generated
	 */
	void setVerticalScaleRequirement(VerticalScaleRequirement value);

	/**
	 * Returns the value of the '<em><b>Horizontal Scale Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Horizontal Scale Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Horizontal Scale Requirement</em>' reference.
	 * @see #setHorizontalScaleRequirement(HorizontalScaleRequirement)
	 * @see camel.deployment.DeploymentPackage#getRequirementSet_HorizontalScaleRequirement()
	 * @model
	 * @generated
	 */
	HorizontalScaleRequirement getHorizontalScaleRequirement();

	/**
	 * Sets the value of the '{@link camel.deployment.RequirementSet#getHorizontalScaleRequirement <em>Horizontal Scale Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Horizontal Scale Requirement</em>' reference.
	 * @see #getHorizontalScaleRequirement()
	 * @generated
	 */
	void setHorizontalScaleRequirement(HorizontalScaleRequirement value);

	/**
	 * Returns the value of the '<em><b>Security Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Requirement</em>' reference.
	 * @see #setSecurityRequirement(SecurityRequirement)
	 * @see camel.deployment.DeploymentPackage#getRequirementSet_SecurityRequirement()
	 * @model
	 * @generated
	 */
	SecurityRequirement getSecurityRequirement();

	/**
	 * Sets the value of the '{@link camel.deployment.RequirementSet#getSecurityRequirement <em>Security Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Security Requirement</em>' reference.
	 * @see #getSecurityRequirement()
	 * @generated
	 */
	void setSecurityRequirement(SecurityRequirement value);

	/**
	 * Returns the value of the '<em><b>Os Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Os Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Os Requirement</em>' reference.
	 * @see #setOsRequirement(OSRequirement)
	 * @see camel.deployment.DeploymentPackage#getRequirementSet_OsRequirement()
	 * @model
	 * @generated
	 */
	OSRequirement getOsRequirement();

	/**
	 * Sets the value of the '{@link camel.deployment.RequirementSet#getOsRequirement <em>Os Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Os Requirement</em>' reference.
	 * @see #getOsRequirement()
	 * @generated
	 */
	void setOsRequirement(OSRequirement value);

	/**
	 * Returns the value of the '<em><b>Image Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Image Requirement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Image Requirement</em>' reference.
	 * @see #setImageRequirement(ImageRequirement)
	 * @see camel.deployment.DeploymentPackage#getRequirementSet_ImageRequirement()
	 * @model
	 * @generated
	 */
	ImageRequirement getImageRequirement();

	/**
	 * Sets the value of the '{@link camel.deployment.RequirementSet#getImageRequirement <em>Image Requirement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Image Requirement</em>' reference.
	 * @see #getImageRequirement()
	 * @generated
	 */
	void setImageRequirement(ImageRequirement value);

} // RequirementSet
