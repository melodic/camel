/**
 */
package camel.deployment;

import camel.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see camel.deployment.DeploymentFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface DeploymentPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "deployment";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.camel-dsl.org/camel/deployment";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "deployment";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	DeploymentPackage eINSTANCE = camel.deployment.impl.DeploymentPackageImpl.init();

	/**
	 * The meta object id for the '{@link camel.deployment.impl.DeploymentModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.DeploymentModelImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getDeploymentModel()
	 * @generated
	 */
	int DEPLOYMENT_MODEL = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__NAME = CorePackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__DESCRIPTION = CorePackage.MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__ANNOTATIONS = CorePackage.MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__ATTRIBUTES = CorePackage.MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__SUB_FEATURES = CorePackage.MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL__IMPORT_URI = CorePackage.MODEL__IMPORT_URI;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL_FEATURE_COUNT = CorePackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL___AS_ERROR__BOOLEAN = CorePackage.MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_MODEL_OPERATION_COUNT = CorePackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.DeploymentInstanceModelImpl <em>Instance Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.DeploymentInstanceModelImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getDeploymentInstanceModel()
	 * @generated
	 */
	int DEPLOYMENT_INSTANCE_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__NAME = DEPLOYMENT_MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__DESCRIPTION = DEPLOYMENT_MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__ANNOTATIONS = DEPLOYMENT_MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__ATTRIBUTES = DEPLOYMENT_MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__SUB_FEATURES = DEPLOYMENT_MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__IMPORT_URI = DEPLOYMENT_MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Software Component Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__SOFTWARE_COMPONENT_INSTANCES = DEPLOYMENT_MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vm Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__VM_INSTANCES = DEPLOYMENT_MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__COMMUNICATION_INSTANCES = DEPLOYMENT_MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Hosting Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__HOSTING_INSTANCES = DEPLOYMENT_MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__TYPE = DEPLOYMENT_MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Container Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__CONTAINER_INSTANCES = DEPLOYMENT_MODEL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Paas Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL__PAAS_INSTANCES = DEPLOYMENT_MODEL_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Instance Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL_FEATURE_COUNT = DEPLOYMENT_MODEL_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL___AS_ERROR__BOOLEAN = DEPLOYMENT_MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Instance Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_INSTANCE_MODEL_OPERATION_COUNT = DEPLOYMENT_MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.DeploymentTypeModelImpl <em>Type Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.DeploymentTypeModelImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getDeploymentTypeModel()
	 * @generated
	 */
	int DEPLOYMENT_TYPE_MODEL = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__NAME = DEPLOYMENT_MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__DESCRIPTION = DEPLOYMENT_MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__ANNOTATIONS = DEPLOYMENT_MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__ATTRIBUTES = DEPLOYMENT_MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__SUB_FEATURES = DEPLOYMENT_MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__IMPORT_URI = DEPLOYMENT_MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Software Components</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__SOFTWARE_COMPONENTS = DEPLOYMENT_MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__VMS = DEPLOYMENT_MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Paases</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__PAASES = DEPLOYMENT_MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__COMMUNICATIONS = DEPLOYMENT_MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Hostings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__HOSTINGS = DEPLOYMENT_MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Requirement Sets</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__REQUIREMENT_SETS = DEPLOYMENT_MODEL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Global Requirement Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__GLOBAL_REQUIREMENT_SET = DEPLOYMENT_MODEL_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Location Couplings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__LOCATION_COUPLINGS = DEPLOYMENT_MODEL_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Containers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL__CONTAINERS = DEPLOYMENT_MODEL_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Type Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL_FEATURE_COUNT = DEPLOYMENT_MODEL_FEATURE_COUNT + 9;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL___AS_ERROR__BOOLEAN = DEPLOYMENT_MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Type Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DEPLOYMENT_TYPE_MODEL_OPERATION_COUNT = DEPLOYMENT_MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ComponentImpl <em>Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ComponentImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getComponent()
	 * @generated
	 */
	int COMPONENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Provided Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__PROVIDED_COMMUNICATIONS = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided Hosts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__PROVIDED_HOSTS = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT__CONFIGURATIONS = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.SoftwareComponentImpl <em>Software Component</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.SoftwareComponentImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getSoftwareComponent()
	 * @generated
	 */
	int SOFTWARE_COMPONENT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__DESCRIPTION = COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__ANNOTATIONS = COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__ATTRIBUTES = COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__SUB_FEATURES = COMPONENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Provided Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__PROVIDED_COMMUNICATIONS = COMPONENT__PROVIDED_COMMUNICATIONS;

	/**
	 * The feature id for the '<em><b>Provided Hosts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__PROVIDED_HOSTS = COMPONENT__PROVIDED_HOSTS;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__CONFIGURATIONS = COMPONENT__CONFIGURATIONS;

	/**
	 * The feature id for the '<em><b>Required Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__REQUIRED_COMMUNICATIONS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__REQUIRED_HOST = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Requirement Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__REQUIREMENT_SET = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Generates Data</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__GENERATES_DATA = COMPONENT_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Consumes Data</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__CONSUMES_DATA = COMPONENT_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Manages Data Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__MANAGES_DATA_SOURCE = COMPONENT_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Long Lived</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__LONG_LIVED = COMPONENT_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Co Instance Hosting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT__CO_INSTANCE_HOSTING = COMPONENT_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Software Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT___AS_ERROR__BOOLEAN = COMPONENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Software Component</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.VMImpl <em>VM</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.VMImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getVM()
	 * @generated
	 */
	int VM = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__DESCRIPTION = COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__ANNOTATIONS = COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__ATTRIBUTES = COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__SUB_FEATURES = COMPONENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Provided Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__PROVIDED_COMMUNICATIONS = COMPONENT__PROVIDED_COMMUNICATIONS;

	/**
	 * The feature id for the '<em><b>Provided Hosts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__PROVIDED_HOSTS = COMPONENT__PROVIDED_HOSTS;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM__CONFIGURATIONS = COMPONENT__CONFIGURATIONS;

	/**
	 * The number of structural features of the '<em>VM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM___AS_ERROR__BOOLEAN = COMPONENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>VM</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.RequirementSetImpl <em>Requirement Set</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.RequirementSetImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getRequirementSet()
	 * @generated
	 */
	int REQUIREMENT_SET = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Resource Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__RESOURCE_REQUIREMENT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Paas Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__PAAS_REQUIREMENT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Location Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__LOCATION_REQUIREMENT = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Provider Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__PROVIDER_REQUIREMENT = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Vertical Scale Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__VERTICAL_SCALE_REQUIREMENT = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Horizontal Scale Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__HORIZONTAL_SCALE_REQUIREMENT = CorePackage.FEATURE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Security Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__SECURITY_REQUIREMENT = CorePackage.FEATURE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Os Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__OS_REQUIREMENT = CorePackage.FEATURE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Image Requirement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET__IMAGE_REQUIREMENT = CorePackage.FEATURE_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Requirement Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 9;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Requirement Set</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_SET_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ConfigurationImpl <em>Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ConfigurationImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getConfiguration()
	 * @generated
	 */
	int CONFIGURATION = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONFIGURATION_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ComponentRelationImpl <em>Component Relation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ComponentRelationImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getComponentRelation()
	 * @generated
	 */
	int COMPONENT_RELATION = 32;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Component Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Component Relation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.CommunicationImpl <em>Communication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.CommunicationImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getCommunication()
	 * @generated
	 */
	int COMMUNICATION = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__NAME = COMPONENT_RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__DESCRIPTION = COMPONENT_RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__ANNOTATIONS = COMPONENT_RELATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__ATTRIBUTES = COMPONENT_RELATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__SUB_FEATURES = COMPONENT_RELATION__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Provided Communication</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__PROVIDED_COMMUNICATION = COMPONENT_RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Communication</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__REQUIRED_COMMUNICATION = COMPONENT_RELATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Provided Port Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__PROVIDED_PORT_CONFIGURATION = COMPONENT_RELATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Required Port Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION__REQUIRED_PORT_CONFIGURATION = COMPONENT_RELATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_FEATURE_COUNT = COMPONENT_RELATION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION___AS_ERROR__BOOLEAN = COMPONENT_RELATION___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_OPERATION_COUNT = COMPONENT_RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.CommunicationPortImpl <em>Communication Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.CommunicationPortImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getCommunicationPort()
	 * @generated
	 */
	int COMMUNICATION_PORT = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__PORT_NUMBER = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Low Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__LOW_PORT_NUMBER = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>High Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT__HIGH_PORT_NUMBER = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Communication Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Communication Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ProvidedCommunicationImpl <em>Provided Communication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ProvidedCommunicationImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getProvidedCommunication()
	 * @generated
	 */
	int PROVIDED_COMMUNICATION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__NAME = COMMUNICATION_PORT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__DESCRIPTION = COMMUNICATION_PORT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__ANNOTATIONS = COMMUNICATION_PORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__ATTRIBUTES = COMMUNICATION_PORT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__SUB_FEATURES = COMMUNICATION_PORT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__PORT_NUMBER = COMMUNICATION_PORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Low Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__LOW_PORT_NUMBER = COMMUNICATION_PORT__LOW_PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>High Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION__HIGH_PORT_NUMBER = COMMUNICATION_PORT__HIGH_PORT_NUMBER;

	/**
	 * The number of structural features of the '<em>Provided Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_FEATURE_COUNT = COMMUNICATION_PORT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION___AS_ERROR__BOOLEAN = COMMUNICATION_PORT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Provided Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_OPERATION_COUNT = COMMUNICATION_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.RequiredCommunicationImpl <em>Required Communication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.RequiredCommunicationImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getRequiredCommunication()
	 * @generated
	 */
	int REQUIRED_COMMUNICATION = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__NAME = COMMUNICATION_PORT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__DESCRIPTION = COMMUNICATION_PORT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__ANNOTATIONS = COMMUNICATION_PORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__ATTRIBUTES = COMMUNICATION_PORT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__SUB_FEATURES = COMMUNICATION_PORT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__PORT_NUMBER = COMMUNICATION_PORT__PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Low Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__LOW_PORT_NUMBER = COMMUNICATION_PORT__LOW_PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>High Port Number</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__HIGH_PORT_NUMBER = COMMUNICATION_PORT__HIGH_PORT_NUMBER;

	/**
	 * The feature id for the '<em><b>Is Mandatory</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION__IS_MANDATORY = COMMUNICATION_PORT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Required Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_FEATURE_COUNT = COMMUNICATION_PORT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION___AS_ERROR__BOOLEAN = COMMUNICATION_PORT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Required Communication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_OPERATION_COUNT = COMMUNICATION_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.HostingImpl <em>Hosting</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.HostingImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getHosting()
	 * @generated
	 */
	int HOSTING = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__NAME = COMPONENT_RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__DESCRIPTION = COMPONENT_RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__ANNOTATIONS = COMPONENT_RELATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__ATTRIBUTES = COMPONENT_RELATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__SUB_FEATURES = COMPONENT_RELATION__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Provided Host</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__PROVIDED_HOST = COMPONENT_RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Hosts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__REQUIRED_HOSTS = COMPONENT_RELATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Provided Host Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__PROVIDED_HOST_CONFIGURATION = COMPONENT_RELATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Required Hosts Configuration</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING__REQUIRED_HOSTS_CONFIGURATION = COMPONENT_RELATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Hosting</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_FEATURE_COUNT = COMPONENT_RELATION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING___AS_ERROR__BOOLEAN = COMPONENT_RELATION___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Hosting</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_OPERATION_COUNT = COMPONENT_RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.HostingPortImpl <em>Hosting Port</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.HostingPortImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getHostingPort()
	 * @generated
	 */
	int HOSTING_PORT = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Hosting Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Hosting Port</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ProvidedHostImpl <em>Provided Host</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ProvidedHostImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getProvidedHost()
	 * @generated
	 */
	int PROVIDED_HOST = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST__NAME = HOSTING_PORT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST__DESCRIPTION = HOSTING_PORT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST__ANNOTATIONS = HOSTING_PORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST__ATTRIBUTES = HOSTING_PORT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST__SUB_FEATURES = HOSTING_PORT__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Provided Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_FEATURE_COUNT = HOSTING_PORT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST___AS_ERROR__BOOLEAN = HOSTING_PORT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Provided Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_OPERATION_COUNT = HOSTING_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.RequiredHostImpl <em>Required Host</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.RequiredHostImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getRequiredHost()
	 * @generated
	 */
	int REQUIRED_HOST = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST__NAME = HOSTING_PORT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST__DESCRIPTION = HOSTING_PORT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST__ANNOTATIONS = HOSTING_PORT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST__ATTRIBUTES = HOSTING_PORT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST__SUB_FEATURES = HOSTING_PORT__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Required Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_FEATURE_COUNT = HOSTING_PORT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST___AS_ERROR__BOOLEAN = HOSTING_PORT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Required Host</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_OPERATION_COUNT = HOSTING_PORT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ComponentInstanceImpl <em>Component Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ComponentInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getComponentInstance()
	 * @generated
	 */
	int COMPONENT_INSTANCE = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__TYPE = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Provided Host Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Instantiated On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__INSTANTIATED_ON = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Destroyed On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE__DESTROYED_ON = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Component Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Component Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_INSTANCE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.SoftwareComponentInstanceImpl <em>Software Component Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.SoftwareComponentInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getSoftwareComponentInstance()
	 * @generated
	 */
	int SOFTWARE_COMPONENT_INSTANCE = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__NAME = COMPONENT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__DESCRIPTION = COMPONENT_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__ANNOTATIONS = COMPONENT_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__ATTRIBUTES = COMPONENT_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__SUB_FEATURES = COMPONENT_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__TYPE = COMPONENT_INSTANCE__TYPE;

	/**
	 * The feature id for the '<em><b>Provided Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES = COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES;

	/**
	 * The feature id for the '<em><b>Provided Host Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES = COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES;

	/**
	 * The feature id for the '<em><b>Instantiated On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__INSTANTIATED_ON = COMPONENT_INSTANCE__INSTANTIATED_ON;

	/**
	 * The feature id for the '<em><b>Destroyed On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__DESTROYED_ON = COMPONENT_INSTANCE__DESTROYED_ON;

	/**
	 * The feature id for the '<em><b>Required Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__REQUIRED_COMMUNICATION_INSTANCES = COMPONENT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Host Instance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__REQUIRED_HOST_INSTANCE = COMPONENT_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Consumes Data Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__CONSUMES_DATA_INSTANCE = COMPONENT_INSTANCE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Generates Data Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__GENERATES_DATA_INSTANCE = COMPONENT_INSTANCE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Manages Data Source Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE__MANAGES_DATA_SOURCE_INSTANCE = COMPONENT_INSTANCE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Software Component Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE_FEATURE_COUNT = COMPONENT_INSTANCE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE___AS_ERROR__BOOLEAN = COMPONENT_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Software Component Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_INSTANCE_OPERATION_COUNT = COMPONENT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.VMInstanceImpl <em>VM Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.VMInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getVMInstance()
	 * @generated
	 */
	int VM_INSTANCE = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__NAME = COMPONENT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__DESCRIPTION = COMPONENT_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__ANNOTATIONS = COMPONENT_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__ATTRIBUTES = COMPONENT_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__SUB_FEATURES = COMPONENT_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__TYPE = COMPONENT_INSTANCE__TYPE;

	/**
	 * The feature id for the '<em><b>Provided Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES = COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES;

	/**
	 * The feature id for the '<em><b>Provided Host Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__PROVIDED_HOST_INSTANCES = COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES;

	/**
	 * The feature id for the '<em><b>Instantiated On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__INSTANTIATED_ON = COMPONENT_INSTANCE__INSTANTIATED_ON;

	/**
	 * The feature id for the '<em><b>Destroyed On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__DESTROYED_ON = COMPONENT_INSTANCE__DESTROYED_ON;

	/**
	 * The feature id for the '<em><b>Location</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__LOCATION = COMPONENT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Os</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE__OS = COMPONENT_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>VM Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE_FEATURE_COUNT = COMPONENT_INSTANCE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE___AS_ERROR__BOOLEAN = COMPONENT_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Check Dates</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE___CHECK_DATES__VMINSTANCE = COMPONENT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>VM Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_INSTANCE_OPERATION_COUNT = COMPONENT_INSTANCE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ComponentRelationInstanceImpl <em>Component Relation Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ComponentRelationInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getComponentRelationInstance()
	 * @generated
	 */
	int COMPONENT_RELATION_INSTANCE = 33;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION_INSTANCE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION_INSTANCE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION_INSTANCE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION_INSTANCE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION_INSTANCE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Component Relation Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION_INSTANCE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION_INSTANCE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Component Relation Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPONENT_RELATION_INSTANCE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.CommunicationInstanceImpl <em>Communication Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.CommunicationInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getCommunicationInstance()
	 * @generated
	 */
	int COMMUNICATION_INSTANCE = 19;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__NAME = COMPONENT_RELATION_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__DESCRIPTION = COMPONENT_RELATION_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__ANNOTATIONS = COMPONENT_RELATION_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__ATTRIBUTES = COMPONENT_RELATION_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__SUB_FEATURES = COMPONENT_RELATION_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__TYPE = COMPONENT_RELATION_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided Communication Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__PROVIDED_COMMUNICATION_INSTANCE = COMPONENT_RELATION_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Required Communication Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE__REQUIRED_COMMUNICATION_INSTANCE = COMPONENT_RELATION_INSTANCE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE_FEATURE_COUNT = COMPONENT_RELATION_INSTANCE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE___AS_ERROR__BOOLEAN = COMPONENT_RELATION_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_INSTANCE_OPERATION_COUNT = COMPONENT_RELATION_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.CommunicationPortInstanceImpl <em>Communication Port Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.CommunicationPortInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getCommunicationPortInstance()
	 * @generated
	 */
	int COMMUNICATION_PORT_INSTANCE = 20;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE__TYPE = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Communication Port Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Communication Port Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_PORT_INSTANCE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ProvidedCommunicationInstanceImpl <em>Provided Communication Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ProvidedCommunicationInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getProvidedCommunicationInstance()
	 * @generated
	 */
	int PROVIDED_COMMUNICATION_INSTANCE = 21;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE__NAME = COMMUNICATION_PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE__DESCRIPTION = COMMUNICATION_PORT_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE__ANNOTATIONS = COMMUNICATION_PORT_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE__ATTRIBUTES = COMMUNICATION_PORT_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE__SUB_FEATURES = COMMUNICATION_PORT_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE__TYPE = COMMUNICATION_PORT_INSTANCE__TYPE;

	/**
	 * The number of structural features of the '<em>Provided Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE_FEATURE_COUNT = COMMUNICATION_PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE___AS_ERROR__BOOLEAN = COMMUNICATION_PORT_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Provided Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_COMMUNICATION_INSTANCE_OPERATION_COUNT = COMMUNICATION_PORT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.RequiredCommunicationInstanceImpl <em>Required Communication Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.RequiredCommunicationInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getRequiredCommunicationInstance()
	 * @generated
	 */
	int REQUIRED_COMMUNICATION_INSTANCE = 22;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE__NAME = COMMUNICATION_PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE__DESCRIPTION = COMMUNICATION_PORT_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE__ANNOTATIONS = COMMUNICATION_PORT_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE__ATTRIBUTES = COMMUNICATION_PORT_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE__SUB_FEATURES = COMMUNICATION_PORT_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE__TYPE = COMMUNICATION_PORT_INSTANCE__TYPE;

	/**
	 * The number of structural features of the '<em>Required Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE_FEATURE_COUNT = COMMUNICATION_PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE___AS_ERROR__BOOLEAN = COMMUNICATION_PORT_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Required Communication Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_COMMUNICATION_INSTANCE_OPERATION_COUNT = COMMUNICATION_PORT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.HostingInstanceImpl <em>Hosting Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.HostingInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getHostingInstance()
	 * @generated
	 */
	int HOSTING_INSTANCE = 23;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__NAME = COMPONENT_RELATION_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__DESCRIPTION = COMPONENT_RELATION_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__ANNOTATIONS = COMPONENT_RELATION_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__ATTRIBUTES = COMPONENT_RELATION_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__SUB_FEATURES = COMPONENT_RELATION_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__TYPE = COMPONENT_RELATION_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Provided Host Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__PROVIDED_HOST_INSTANCE = COMPONENT_RELATION_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Required Host Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE__REQUIRED_HOST_INSTANCES = COMPONENT_RELATION_INSTANCE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Hosting Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE_FEATURE_COUNT = COMPONENT_RELATION_INSTANCE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE___AS_ERROR__BOOLEAN = COMPONENT_RELATION_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Hosting Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_INSTANCE_OPERATION_COUNT = COMPONENT_RELATION_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.HostingPortInstanceImpl <em>Hosting Port Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.HostingPortInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getHostingPortInstance()
	 * @generated
	 */
	int HOSTING_PORT_INSTANCE = 24;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE__TYPE = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Hosting Port Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Hosting Port Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HOSTING_PORT_INSTANCE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ProvidedHostInstanceImpl <em>Provided Host Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ProvidedHostInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getProvidedHostInstance()
	 * @generated
	 */
	int PROVIDED_HOST_INSTANCE = 25;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE__NAME = HOSTING_PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE__DESCRIPTION = HOSTING_PORT_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE__ANNOTATIONS = HOSTING_PORT_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE__ATTRIBUTES = HOSTING_PORT_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE__SUB_FEATURES = HOSTING_PORT_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE__TYPE = HOSTING_PORT_INSTANCE__TYPE;

	/**
	 * The number of structural features of the '<em>Provided Host Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE_FEATURE_COUNT = HOSTING_PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE___AS_ERROR__BOOLEAN = HOSTING_PORT_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Provided Host Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDED_HOST_INSTANCE_OPERATION_COUNT = HOSTING_PORT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.RequiredHostInstanceImpl <em>Required Host Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.RequiredHostInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getRequiredHostInstance()
	 * @generated
	 */
	int REQUIRED_HOST_INSTANCE = 26;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE__NAME = HOSTING_PORT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE__DESCRIPTION = HOSTING_PORT_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE__ANNOTATIONS = HOSTING_PORT_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE__ATTRIBUTES = HOSTING_PORT_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE__SUB_FEATURES = HOSTING_PORT_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE__TYPE = HOSTING_PORT_INSTANCE__TYPE;

	/**
	 * The number of structural features of the '<em>Required Host Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE_FEATURE_COUNT = HOSTING_PORT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE___AS_ERROR__BOOLEAN = HOSTING_PORT_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Required Host Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIRED_HOST_INSTANCE_OPERATION_COUNT = HOSTING_PORT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ScriptConfigurationImpl <em>Script Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ScriptConfigurationImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getScriptConfiguration()
	 * @generated
	 */
	int SCRIPT_CONFIGURATION = 27;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__NAME = CONFIGURATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__DESCRIPTION = CONFIGURATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__ANNOTATIONS = CONFIGURATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__ATTRIBUTES = CONFIGURATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__SUB_FEATURES = CONFIGURATION__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Upload Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__UPLOAD_COMMAND = CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Install Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__INSTALL_COMMAND = CONFIGURATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Start Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__START_COMMAND = CONFIGURATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Stop Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__STOP_COMMAND = CONFIGURATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Download Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__DOWNLOAD_COMMAND = CONFIGURATION_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Configure Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__CONFIGURE_COMMAND = CONFIGURATION_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Update Command</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__UPDATE_COMMAND = CONFIGURATION_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Devops Tool</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__DEVOPS_TOOL = CONFIGURATION_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__OS = CONFIGURATION_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Image Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION__IMAGE_ID = CONFIGURATION_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>Script Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION_FEATURE_COUNT = CONFIGURATION_FEATURE_COUNT + 10;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION___AS_ERROR__BOOLEAN = CONFIGURATION___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Script Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCRIPT_CONFIGURATION_OPERATION_COUNT = CONFIGURATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ClusterConfigurationImpl <em>Cluster Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ClusterConfigurationImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getClusterConfiguration()
	 * @generated
	 */
	int CLUSTER_CONFIGURATION = 28;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_CONFIGURATION__NAME = CONFIGURATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_CONFIGURATION__DESCRIPTION = CONFIGURATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_CONFIGURATION__ANNOTATIONS = CONFIGURATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_CONFIGURATION__ATTRIBUTES = CONFIGURATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_CONFIGURATION__SUB_FEATURES = CONFIGURATION__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Download URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_CONFIGURATION__DOWNLOAD_URL = CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Config Parameters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_CONFIGURATION__CONFIG_PARAMETERS = CONFIGURATION_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Cluster Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_CONFIGURATION_FEATURE_COUNT = CONFIGURATION_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_CONFIGURATION___AS_ERROR__BOOLEAN = CONFIGURATION___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Cluster Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLUSTER_CONFIGURATION_OPERATION_COUNT = CONFIGURATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.LocationCouplingImpl <em>Location Coupling</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.LocationCouplingImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getLocationCoupling()
	 * @generated
	 */
	int LOCATION_COUPLING = 29;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_COUPLING__NAME = COMPONENT_RELATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_COUPLING__DESCRIPTION = COMPONENT_RELATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_COUPLING__ANNOTATIONS = COMPONENT_RELATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_COUPLING__ATTRIBUTES = COMPONENT_RELATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_COUPLING__SUB_FEATURES = COMPONENT_RELATION__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Software Components</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_COUPLING__SOFTWARE_COMPONENTS = COMPONENT_RELATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Coupling Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_COUPLING__COUPLING_TYPE = COMPONENT_RELATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Relaxed</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_COUPLING__RELAXED = COMPONENT_RELATION_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Location Coupling</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_COUPLING_FEATURE_COUNT = COMPONENT_RELATION_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_COUPLING___AS_ERROR__BOOLEAN = COMPONENT_RELATION___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Location Coupling</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_COUPLING_OPERATION_COUNT = COMPONENT_RELATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ContainerImpl <em>Container</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ContainerImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getContainer()
	 * @generated
	 */
	int CONTAINER = 30;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__DESCRIPTION = COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__ANNOTATIONS = COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__ATTRIBUTES = COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__SUB_FEATURES = COMPONENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Provided Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__PROVIDED_COMMUNICATIONS = COMPONENT__PROVIDED_COMMUNICATIONS;

	/**
	 * The feature id for the '<em><b>Provided Hosts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__PROVIDED_HOSTS = COMPONENT__PROVIDED_HOSTS;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__CONFIGURATIONS = COMPONENT__CONFIGURATIONS;

	/**
	 * The feature id for the '<em><b>Required Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__REQUIRED_COMMUNICATIONS = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER__REQUIRED_HOST = COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER___AS_ERROR__BOOLEAN = COMPONENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Container</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ContainerInstanceImpl <em>Container Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ContainerInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getContainerInstance()
	 * @generated
	 */
	int CONTAINER_INSTANCE = 31;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__NAME = COMPONENT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__DESCRIPTION = COMPONENT_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__ANNOTATIONS = COMPONENT_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__ATTRIBUTES = COMPONENT_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__SUB_FEATURES = COMPONENT_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__TYPE = COMPONENT_INSTANCE__TYPE;

	/**
	 * The feature id for the '<em><b>Provided Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES = COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES;

	/**
	 * The feature id for the '<em><b>Provided Host Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__PROVIDED_HOST_INSTANCES = COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES;

	/**
	 * The feature id for the '<em><b>Instantiated On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__INSTANTIATED_ON = COMPONENT_INSTANCE__INSTANTIATED_ON;

	/**
	 * The feature id for the '<em><b>Destroyed On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__DESTROYED_ON = COMPONENT_INSTANCE__DESTROYED_ON;

	/**
	 * The feature id for the '<em><b>Required Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__REQUIRED_COMMUNICATION_INSTANCES = COMPONENT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Required Host Instance</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE__REQUIRED_HOST_INSTANCE = COMPONENT_INSTANCE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Container Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE_FEATURE_COUNT = COMPONENT_INSTANCE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE___AS_ERROR__BOOLEAN = COMPONENT_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Container Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_INSTANCE_OPERATION_COUNT = COMPONENT_INSTANCE_OPERATION_COUNT + 0;


	/**
	 * The meta object id for the '{@link camel.deployment.impl.PaaSImpl <em>Paa S</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.PaaSImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getPaaS()
	 * @generated
	 */
	int PAA_S = 34;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_S__NAME = COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_S__DESCRIPTION = COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_S__ANNOTATIONS = COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_S__ATTRIBUTES = COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_S__SUB_FEATURES = COMPONENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Provided Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_S__PROVIDED_COMMUNICATIONS = COMPONENT__PROVIDED_COMMUNICATIONS;

	/**
	 * The feature id for the '<em><b>Provided Hosts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_S__PROVIDED_HOSTS = COMPONENT__PROVIDED_HOSTS;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_S__CONFIGURATIONS = COMPONENT__CONFIGURATIONS;

	/**
	 * The number of structural features of the '<em>Paa S</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_S_FEATURE_COUNT = COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_S___AS_ERROR__BOOLEAN = COMPONENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Paa S</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_S_OPERATION_COUNT = COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.PaaSInstanceImpl <em>Paa SInstance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.PaaSInstanceImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getPaaSInstance()
	 * @generated
	 */
	int PAA_SINSTANCE = 35;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE__NAME = COMPONENT_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE__DESCRIPTION = COMPONENT_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE__ANNOTATIONS = COMPONENT_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE__ATTRIBUTES = COMPONENT_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE__SUB_FEATURES = COMPONENT_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE__TYPE = COMPONENT_INSTANCE__TYPE;

	/**
	 * The feature id for the '<em><b>Provided Communication Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE__PROVIDED_COMMUNICATION_INSTANCES = COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES;

	/**
	 * The feature id for the '<em><b>Provided Host Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE__PROVIDED_HOST_INSTANCES = COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES;

	/**
	 * The feature id for the '<em><b>Instantiated On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE__INSTANTIATED_ON = COMPONENT_INSTANCE__INSTANTIATED_ON;

	/**
	 * The feature id for the '<em><b>Destroyed On</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE__DESTROYED_ON = COMPONENT_INSTANCE__DESTROYED_ON;

	/**
	 * The number of structural features of the '<em>Paa SInstance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE_FEATURE_COUNT = COMPONENT_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE___AS_ERROR__BOOLEAN = COMPONENT_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Paa SInstance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SINSTANCE_OPERATION_COUNT = COMPONENT_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.PaaSConfigurationImpl <em>Paa SConfiguration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.PaaSConfigurationImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getPaaSConfiguration()
	 * @generated
	 */
	int PAA_SCONFIGURATION = 36;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION__NAME = CONFIGURATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION__DESCRIPTION = CONFIGURATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION__ANNOTATIONS = CONFIGURATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION__ATTRIBUTES = CONFIGURATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION__SUB_FEATURES = CONFIGURATION__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Api</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION__API = CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Version</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION__VERSION = CONFIGURATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Endpoint</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION__ENDPOINT = CONFIGURATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Download URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION__DOWNLOAD_URL = CONFIGURATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Paa SConfiguration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION_FEATURE_COUNT = CONFIGURATION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION___AS_ERROR__BOOLEAN = CONFIGURATION___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Paa SConfiguration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SCONFIGURATION_OPERATION_COUNT = CONFIGURATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.ServerlessConfigurationImpl <em>Serverless Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.ServerlessConfigurationImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getServerlessConfiguration()
	 * @generated
	 */
	int SERVERLESS_CONFIGURATION = 37;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION__NAME = CONFIGURATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION__DESCRIPTION = CONFIGURATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION__ANNOTATIONS = CONFIGURATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION__ATTRIBUTES = CONFIGURATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION__SUB_FEATURES = CONFIGURATION__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Binary Code URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION__BINARY_CODE_URL = CONFIGURATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Continuous Deployment</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION__CONTINUOUS_DEPLOYMENT = CONFIGURATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Environment Config Params</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION__ENVIRONMENT_CONFIG_PARAMS = CONFIGURATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Build Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION__BUILD_CONFIGURATION = CONFIGURATION_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Event Configuration</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION__EVENT_CONFIGURATION = CONFIGURATION_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Serverless Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION_FEATURE_COUNT = CONFIGURATION_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION___AS_ERROR__BOOLEAN = CONFIGURATION___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Serverless Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVERLESS_CONFIGURATION_OPERATION_COUNT = CONFIGURATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.BuildConfigurationImpl <em>Build Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.BuildConfigurationImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getBuildConfiguration()
	 * @generated
	 */
	int BUILD_CONFIGURATION = 38;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Artifact Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION__ARTIFACT_ID = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Build Framework</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION__BUILD_FRAMEWORK = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source Code URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION__SOURCE_CODE_URL = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Include</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION__INCLUDE = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Exclude</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION__EXCLUDE = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Build Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Build Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BUILD_CONFIGURATION_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.impl.EventConfigurationImpl <em>Event Configuration</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.impl.EventConfigurationImpl
	 * @see camel.deployment.impl.DeploymentPackageImpl#getEventConfiguration()
	 * @generated
	 */
	int EVENT_CONFIGURATION = 39;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Http Method Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION__HTTP_METHOD_NAME = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Http Method Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION__HTTP_METHOD_TYPE = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Execution Schedule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION__EXECUTION_SCHEDULE = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Scheduled Execution Config</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION__SCHEDULED_EXECUTION_CONFIG = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Event Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Event Configuration</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EVENT_CONFIGURATION_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.deployment.LocationCouplingType <em>Location Coupling Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.LocationCouplingType
	 * @see camel.deployment.impl.DeploymentPackageImpl#getLocationCouplingType()
	 * @generated
	 */
	int LOCATION_COUPLING_TYPE = 40;


	/**
	 * The meta object id for the '{@link camel.deployment.HTTPMethodType <em>HTTP Method Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.deployment.HTTPMethodType
	 * @see camel.deployment.impl.DeploymentPackageImpl#getHTTPMethodType()
	 * @generated
	 */
	int HTTP_METHOD_TYPE = 41;


	/**
	 * Returns the meta object for class '{@link camel.deployment.DeploymentInstanceModel <em>Instance Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instance Model</em>'.
	 * @see camel.deployment.DeploymentInstanceModel
	 * @generated
	 */
	EClass getDeploymentInstanceModel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentInstanceModel#getSoftwareComponentInstances <em>Software Component Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Software Component Instances</em>'.
	 * @see camel.deployment.DeploymentInstanceModel#getSoftwareComponentInstances()
	 * @see #getDeploymentInstanceModel()
	 * @generated
	 */
	EReference getDeploymentInstanceModel_SoftwareComponentInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentInstanceModel#getVmInstances <em>Vm Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Vm Instances</em>'.
	 * @see camel.deployment.DeploymentInstanceModel#getVmInstances()
	 * @see #getDeploymentInstanceModel()
	 * @generated
	 */
	EReference getDeploymentInstanceModel_VmInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentInstanceModel#getCommunicationInstances <em>Communication Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Communication Instances</em>'.
	 * @see camel.deployment.DeploymentInstanceModel#getCommunicationInstances()
	 * @see #getDeploymentInstanceModel()
	 * @generated
	 */
	EReference getDeploymentInstanceModel_CommunicationInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentInstanceModel#getHostingInstances <em>Hosting Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hosting Instances</em>'.
	 * @see camel.deployment.DeploymentInstanceModel#getHostingInstances()
	 * @see #getDeploymentInstanceModel()
	 * @generated
	 */
	EReference getDeploymentInstanceModel_HostingInstances();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.DeploymentInstanceModel#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see camel.deployment.DeploymentInstanceModel#getType()
	 * @see #getDeploymentInstanceModel()
	 * @generated
	 */
	EReference getDeploymentInstanceModel_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentInstanceModel#getContainerInstances <em>Container Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Container Instances</em>'.
	 * @see camel.deployment.DeploymentInstanceModel#getContainerInstances()
	 * @see #getDeploymentInstanceModel()
	 * @generated
	 */
	EReference getDeploymentInstanceModel_ContainerInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentInstanceModel#getPaasInstances <em>Paas Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Paas Instances</em>'.
	 * @see camel.deployment.DeploymentInstanceModel#getPaasInstances()
	 * @see #getDeploymentInstanceModel()
	 * @generated
	 */
	EReference getDeploymentInstanceModel_PaasInstances();

	/**
	 * Returns the meta object for class '{@link camel.deployment.DeploymentModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see camel.deployment.DeploymentModel
	 * @generated
	 */
	EClass getDeploymentModel();

	/**
	 * Returns the meta object for class '{@link camel.deployment.DeploymentTypeModel <em>Type Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Model</em>'.
	 * @see camel.deployment.DeploymentTypeModel
	 * @generated
	 */
	EClass getDeploymentTypeModel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentTypeModel#getSoftwareComponents <em>Software Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Software Components</em>'.
	 * @see camel.deployment.DeploymentTypeModel#getSoftwareComponents()
	 * @see #getDeploymentTypeModel()
	 * @generated
	 */
	EReference getDeploymentTypeModel_SoftwareComponents();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentTypeModel#getVms <em>Vms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Vms</em>'.
	 * @see camel.deployment.DeploymentTypeModel#getVms()
	 * @see #getDeploymentTypeModel()
	 * @generated
	 */
	EReference getDeploymentTypeModel_Vms();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentTypeModel#getPaases <em>Paases</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Paases</em>'.
	 * @see camel.deployment.DeploymentTypeModel#getPaases()
	 * @see #getDeploymentTypeModel()
	 * @generated
	 */
	EReference getDeploymentTypeModel_Paases();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentTypeModel#getCommunications <em>Communications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Communications</em>'.
	 * @see camel.deployment.DeploymentTypeModel#getCommunications()
	 * @see #getDeploymentTypeModel()
	 * @generated
	 */
	EReference getDeploymentTypeModel_Communications();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentTypeModel#getHostings <em>Hostings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Hostings</em>'.
	 * @see camel.deployment.DeploymentTypeModel#getHostings()
	 * @see #getDeploymentTypeModel()
	 * @generated
	 */
	EReference getDeploymentTypeModel_Hostings();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentTypeModel#getRequirementSets <em>Requirement Sets</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Requirement Sets</em>'.
	 * @see camel.deployment.DeploymentTypeModel#getRequirementSets()
	 * @see #getDeploymentTypeModel()
	 * @generated
	 */
	EReference getDeploymentTypeModel_RequirementSets();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.DeploymentTypeModel#getGlobalRequirementSet <em>Global Requirement Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Global Requirement Set</em>'.
	 * @see camel.deployment.DeploymentTypeModel#getGlobalRequirementSet()
	 * @see #getDeploymentTypeModel()
	 * @generated
	 */
	EReference getDeploymentTypeModel_GlobalRequirementSet();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentTypeModel#getLocationCouplings <em>Location Couplings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Location Couplings</em>'.
	 * @see camel.deployment.DeploymentTypeModel#getLocationCouplings()
	 * @see #getDeploymentTypeModel()
	 * @generated
	 */
	EReference getDeploymentTypeModel_LocationCouplings();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.DeploymentTypeModel#getContainers <em>Containers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Containers</em>'.
	 * @see camel.deployment.DeploymentTypeModel#getContainers()
	 * @see #getDeploymentTypeModel()
	 * @generated
	 */
	EReference getDeploymentTypeModel_Containers();

	/**
	 * Returns the meta object for class '{@link camel.deployment.Component <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component</em>'.
	 * @see camel.deployment.Component
	 * @generated
	 */
	EClass getComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.Component#getProvidedCommunications <em>Provided Communications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provided Communications</em>'.
	 * @see camel.deployment.Component#getProvidedCommunications()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_ProvidedCommunications();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.Component#getProvidedHosts <em>Provided Hosts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provided Hosts</em>'.
	 * @see camel.deployment.Component#getProvidedHosts()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_ProvidedHosts();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.Component#getConfigurations <em>Configurations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Configurations</em>'.
	 * @see camel.deployment.Component#getConfigurations()
	 * @see #getComponent()
	 * @generated
	 */
	EReference getComponent_Configurations();

	/**
	 * Returns the meta object for class '{@link camel.deployment.SoftwareComponent <em>Software Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Software Component</em>'.
	 * @see camel.deployment.SoftwareComponent
	 * @generated
	 */
	EClass getSoftwareComponent();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.SoftwareComponent#getRequiredCommunications <em>Required Communications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Communications</em>'.
	 * @see camel.deployment.SoftwareComponent#getRequiredCommunications()
	 * @see #getSoftwareComponent()
	 * @generated
	 */
	EReference getSoftwareComponent_RequiredCommunications();

	/**
	 * Returns the meta object for the containment reference '{@link camel.deployment.SoftwareComponent#getRequiredHost <em>Required Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Host</em>'.
	 * @see camel.deployment.SoftwareComponent#getRequiredHost()
	 * @see #getSoftwareComponent()
	 * @generated
	 */
	EReference getSoftwareComponent_RequiredHost();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.SoftwareComponent#getRequirementSet <em>Requirement Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Requirement Set</em>'.
	 * @see camel.deployment.SoftwareComponent#getRequirementSet()
	 * @see #getSoftwareComponent()
	 * @generated
	 */
	EReference getSoftwareComponent_RequirementSet();

	/**
	 * Returns the meta object for the reference list '{@link camel.deployment.SoftwareComponent#getGeneratesData <em>Generates Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Generates Data</em>'.
	 * @see camel.deployment.SoftwareComponent#getGeneratesData()
	 * @see #getSoftwareComponent()
	 * @generated
	 */
	EReference getSoftwareComponent_GeneratesData();

	/**
	 * Returns the meta object for the reference list '{@link camel.deployment.SoftwareComponent#getConsumesData <em>Consumes Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Consumes Data</em>'.
	 * @see camel.deployment.SoftwareComponent#getConsumesData()
	 * @see #getSoftwareComponent()
	 * @generated
	 */
	EReference getSoftwareComponent_ConsumesData();

	/**
	 * Returns the meta object for the reference list '{@link camel.deployment.SoftwareComponent#getManagesDataSource <em>Manages Data Source</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Manages Data Source</em>'.
	 * @see camel.deployment.SoftwareComponent#getManagesDataSource()
	 * @see #getSoftwareComponent()
	 * @generated
	 */
	EReference getSoftwareComponent_ManagesDataSource();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.SoftwareComponent#isLongLived <em>Long Lived</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Long Lived</em>'.
	 * @see camel.deployment.SoftwareComponent#isLongLived()
	 * @see #getSoftwareComponent()
	 * @generated
	 */
	EAttribute getSoftwareComponent_LongLived();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.SoftwareComponent#isCoInstanceHosting <em>Co Instance Hosting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Co Instance Hosting</em>'.
	 * @see camel.deployment.SoftwareComponent#isCoInstanceHosting()
	 * @see #getSoftwareComponent()
	 * @generated
	 */
	EAttribute getSoftwareComponent_CoInstanceHosting();

	/**
	 * Returns the meta object for class '{@link camel.deployment.VM <em>VM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>VM</em>'.
	 * @see camel.deployment.VM
	 * @generated
	 */
	EClass getVM();

	/**
	 * Returns the meta object for class '{@link camel.deployment.RequirementSet <em>Requirement Set</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requirement Set</em>'.
	 * @see camel.deployment.RequirementSet
	 * @generated
	 */
	EClass getRequirementSet();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.RequirementSet#getResourceRequirement <em>Resource Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource Requirement</em>'.
	 * @see camel.deployment.RequirementSet#getResourceRequirement()
	 * @see #getRequirementSet()
	 * @generated
	 */
	EReference getRequirementSet_ResourceRequirement();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.RequirementSet#getPaasRequirement <em>Paas Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Paas Requirement</em>'.
	 * @see camel.deployment.RequirementSet#getPaasRequirement()
	 * @see #getRequirementSet()
	 * @generated
	 */
	EReference getRequirementSet_PaasRequirement();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.RequirementSet#getLocationRequirement <em>Location Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location Requirement</em>'.
	 * @see camel.deployment.RequirementSet#getLocationRequirement()
	 * @see #getRequirementSet()
	 * @generated
	 */
	EReference getRequirementSet_LocationRequirement();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.RequirementSet#getProviderRequirement <em>Provider Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provider Requirement</em>'.
	 * @see camel.deployment.RequirementSet#getProviderRequirement()
	 * @see #getRequirementSet()
	 * @generated
	 */
	EReference getRequirementSet_ProviderRequirement();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.RequirementSet#getVerticalScaleRequirement <em>Vertical Scale Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Vertical Scale Requirement</em>'.
	 * @see camel.deployment.RequirementSet#getVerticalScaleRequirement()
	 * @see #getRequirementSet()
	 * @generated
	 */
	EReference getRequirementSet_VerticalScaleRequirement();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.RequirementSet#getHorizontalScaleRequirement <em>Horizontal Scale Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Horizontal Scale Requirement</em>'.
	 * @see camel.deployment.RequirementSet#getHorizontalScaleRequirement()
	 * @see #getRequirementSet()
	 * @generated
	 */
	EReference getRequirementSet_HorizontalScaleRequirement();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.RequirementSet#getSecurityRequirement <em>Security Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Security Requirement</em>'.
	 * @see camel.deployment.RequirementSet#getSecurityRequirement()
	 * @see #getRequirementSet()
	 * @generated
	 */
	EReference getRequirementSet_SecurityRequirement();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.RequirementSet#getOsRequirement <em>Os Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Os Requirement</em>'.
	 * @see camel.deployment.RequirementSet#getOsRequirement()
	 * @see #getRequirementSet()
	 * @generated
	 */
	EReference getRequirementSet_OsRequirement();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.RequirementSet#getImageRequirement <em>Image Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Image Requirement</em>'.
	 * @see camel.deployment.RequirementSet#getImageRequirement()
	 * @see #getRequirementSet()
	 * @generated
	 */
	EReference getRequirementSet_ImageRequirement();

	/**
	 * Returns the meta object for class '{@link camel.deployment.Configuration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Configuration</em>'.
	 * @see camel.deployment.Configuration
	 * @generated
	 */
	EClass getConfiguration();

	/**
	 * Returns the meta object for class '{@link camel.deployment.Communication <em>Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication</em>'.
	 * @see camel.deployment.Communication
	 * @generated
	 */
	EClass getCommunication();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.Communication#getProvidedCommunication <em>Provided Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provided Communication</em>'.
	 * @see camel.deployment.Communication#getProvidedCommunication()
	 * @see #getCommunication()
	 * @generated
	 */
	EReference getCommunication_ProvidedCommunication();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.Communication#getRequiredCommunication <em>Required Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Communication</em>'.
	 * @see camel.deployment.Communication#getRequiredCommunication()
	 * @see #getCommunication()
	 * @generated
	 */
	EReference getCommunication_RequiredCommunication();

	/**
	 * Returns the meta object for the containment reference '{@link camel.deployment.Communication#getProvidedPortConfiguration <em>Provided Port Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Provided Port Configuration</em>'.
	 * @see camel.deployment.Communication#getProvidedPortConfiguration()
	 * @see #getCommunication()
	 * @generated
	 */
	EReference getCommunication_ProvidedPortConfiguration();

	/**
	 * Returns the meta object for the containment reference '{@link camel.deployment.Communication#getRequiredPortConfiguration <em>Required Port Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Port Configuration</em>'.
	 * @see camel.deployment.Communication#getRequiredPortConfiguration()
	 * @see #getCommunication()
	 * @generated
	 */
	EReference getCommunication_RequiredPortConfiguration();

	/**
	 * Returns the meta object for class '{@link camel.deployment.CommunicationPort <em>Communication Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Port</em>'.
	 * @see camel.deployment.CommunicationPort
	 * @generated
	 */
	EClass getCommunicationPort();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.CommunicationPort#getPortNumber <em>Port Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Port Number</em>'.
	 * @see camel.deployment.CommunicationPort#getPortNumber()
	 * @see #getCommunicationPort()
	 * @generated
	 */
	EAttribute getCommunicationPort_PortNumber();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.CommunicationPort#getLowPortNumber <em>Low Port Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Low Port Number</em>'.
	 * @see camel.deployment.CommunicationPort#getLowPortNumber()
	 * @see #getCommunicationPort()
	 * @generated
	 */
	EAttribute getCommunicationPort_LowPortNumber();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.CommunicationPort#getHighPortNumber <em>High Port Number</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>High Port Number</em>'.
	 * @see camel.deployment.CommunicationPort#getHighPortNumber()
	 * @see #getCommunicationPort()
	 * @generated
	 */
	EAttribute getCommunicationPort_HighPortNumber();

	/**
	 * Returns the meta object for class '{@link camel.deployment.ProvidedCommunication <em>Provided Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provided Communication</em>'.
	 * @see camel.deployment.ProvidedCommunication
	 * @generated
	 */
	EClass getProvidedCommunication();

	/**
	 * Returns the meta object for class '{@link camel.deployment.RequiredCommunication <em>Required Communication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required Communication</em>'.
	 * @see camel.deployment.RequiredCommunication
	 * @generated
	 */
	EClass getRequiredCommunication();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.RequiredCommunication#isIsMandatory <em>Is Mandatory</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Mandatory</em>'.
	 * @see camel.deployment.RequiredCommunication#isIsMandatory()
	 * @see #getRequiredCommunication()
	 * @generated
	 */
	EAttribute getRequiredCommunication_IsMandatory();

	/**
	 * Returns the meta object for class '{@link camel.deployment.Hosting <em>Hosting</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hosting</em>'.
	 * @see camel.deployment.Hosting
	 * @generated
	 */
	EClass getHosting();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.Hosting#getProvidedHost <em>Provided Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provided Host</em>'.
	 * @see camel.deployment.Hosting#getProvidedHost()
	 * @see #getHosting()
	 * @generated
	 */
	EReference getHosting_ProvidedHost();

	/**
	 * Returns the meta object for the reference list '{@link camel.deployment.Hosting#getRequiredHosts <em>Required Hosts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Hosts</em>'.
	 * @see camel.deployment.Hosting#getRequiredHosts()
	 * @see #getHosting()
	 * @generated
	 */
	EReference getHosting_RequiredHosts();

	/**
	 * Returns the meta object for the containment reference '{@link camel.deployment.Hosting#getProvidedHostConfiguration <em>Provided Host Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Provided Host Configuration</em>'.
	 * @see camel.deployment.Hosting#getProvidedHostConfiguration()
	 * @see #getHosting()
	 * @generated
	 */
	EReference getHosting_ProvidedHostConfiguration();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.Hosting#getRequiredHostsConfiguration <em>Required Hosts Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Hosts Configuration</em>'.
	 * @see camel.deployment.Hosting#getRequiredHostsConfiguration()
	 * @see #getHosting()
	 * @generated
	 */
	EReference getHosting_RequiredHostsConfiguration();

	/**
	 * Returns the meta object for class '{@link camel.deployment.HostingPort <em>Hosting Port</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hosting Port</em>'.
	 * @see camel.deployment.HostingPort
	 * @generated
	 */
	EClass getHostingPort();

	/**
	 * Returns the meta object for class '{@link camel.deployment.ProvidedHost <em>Provided Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provided Host</em>'.
	 * @see camel.deployment.ProvidedHost
	 * @generated
	 */
	EClass getProvidedHost();

	/**
	 * Returns the meta object for class '{@link camel.deployment.RequiredHost <em>Required Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required Host</em>'.
	 * @see camel.deployment.RequiredHost
	 * @generated
	 */
	EClass getRequiredHost();

	/**
	 * Returns the meta object for class '{@link camel.deployment.ComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Instance</em>'.
	 * @see camel.deployment.ComponentInstance
	 * @generated
	 */
	EClass getComponentInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.ComponentInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see camel.deployment.ComponentInstance#getType()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_Type();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.ComponentInstance#getProvidedCommunicationInstances <em>Provided Communication Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provided Communication Instances</em>'.
	 * @see camel.deployment.ComponentInstance#getProvidedCommunicationInstances()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_ProvidedCommunicationInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.ComponentInstance#getProvidedHostInstances <em>Provided Host Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Provided Host Instances</em>'.
	 * @see camel.deployment.ComponentInstance#getProvidedHostInstances()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EReference getComponentInstance_ProvidedHostInstances();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ComponentInstance#getInstantiatedOn <em>Instantiated On</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Instantiated On</em>'.
	 * @see camel.deployment.ComponentInstance#getInstantiatedOn()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EAttribute getComponentInstance_InstantiatedOn();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ComponentInstance#getDestroyedOn <em>Destroyed On</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Destroyed On</em>'.
	 * @see camel.deployment.ComponentInstance#getDestroyedOn()
	 * @see #getComponentInstance()
	 * @generated
	 */
	EAttribute getComponentInstance_DestroyedOn();

	/**
	 * Returns the meta object for class '{@link camel.deployment.SoftwareComponentInstance <em>Software Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Software Component Instance</em>'.
	 * @see camel.deployment.SoftwareComponentInstance
	 * @generated
	 */
	EClass getSoftwareComponentInstance();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.SoftwareComponentInstance#getRequiredCommunicationInstances <em>Required Communication Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Communication Instances</em>'.
	 * @see camel.deployment.SoftwareComponentInstance#getRequiredCommunicationInstances()
	 * @see #getSoftwareComponentInstance()
	 * @generated
	 */
	EReference getSoftwareComponentInstance_RequiredCommunicationInstances();

	/**
	 * Returns the meta object for the containment reference '{@link camel.deployment.SoftwareComponentInstance#getRequiredHostInstance <em>Required Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Host Instance</em>'.
	 * @see camel.deployment.SoftwareComponentInstance#getRequiredHostInstance()
	 * @see #getSoftwareComponentInstance()
	 * @generated
	 */
	EReference getSoftwareComponentInstance_RequiredHostInstance();

	/**
	 * Returns the meta object for the reference list '{@link camel.deployment.SoftwareComponentInstance#getConsumesDataInstance <em>Consumes Data Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Consumes Data Instance</em>'.
	 * @see camel.deployment.SoftwareComponentInstance#getConsumesDataInstance()
	 * @see #getSoftwareComponentInstance()
	 * @generated
	 */
	EReference getSoftwareComponentInstance_ConsumesDataInstance();

	/**
	 * Returns the meta object for the reference list '{@link camel.deployment.SoftwareComponentInstance#getGeneratesDataInstance <em>Generates Data Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Generates Data Instance</em>'.
	 * @see camel.deployment.SoftwareComponentInstance#getGeneratesDataInstance()
	 * @see #getSoftwareComponentInstance()
	 * @generated
	 */
	EReference getSoftwareComponentInstance_GeneratesDataInstance();

	/**
	 * Returns the meta object for the reference list '{@link camel.deployment.SoftwareComponentInstance#getManagesDataSourceInstance <em>Manages Data Source Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Manages Data Source Instance</em>'.
	 * @see camel.deployment.SoftwareComponentInstance#getManagesDataSourceInstance()
	 * @see #getSoftwareComponentInstance()
	 * @generated
	 */
	EReference getSoftwareComponentInstance_ManagesDataSourceInstance();

	/**
	 * Returns the meta object for class '{@link camel.deployment.VMInstance <em>VM Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>VM Instance</em>'.
	 * @see camel.deployment.VMInstance
	 * @generated
	 */
	EClass getVMInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.VMInstance#getLocation <em>Location</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Location</em>'.
	 * @see camel.deployment.VMInstance#getLocation()
	 * @see #getVMInstance()
	 * @generated
	 */
	EReference getVMInstance_Location();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.VMInstance#getOs <em>Os</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Os</em>'.
	 * @see camel.deployment.VMInstance#getOs()
	 * @see #getVMInstance()
	 * @generated
	 */
	EReference getVMInstance_Os();

	/**
	 * Returns the meta object for the '{@link camel.deployment.VMInstance#checkDates(camel.deployment.VMInstance) <em>Check Dates</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Dates</em>' operation.
	 * @see camel.deployment.VMInstance#checkDates(camel.deployment.VMInstance)
	 * @generated
	 */
	EOperation getVMInstance__CheckDates__VMInstance();

	/**
	 * Returns the meta object for class '{@link camel.deployment.CommunicationInstance <em>Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Instance</em>'.
	 * @see camel.deployment.CommunicationInstance
	 * @generated
	 */
	EClass getCommunicationInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.CommunicationInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see camel.deployment.CommunicationInstance#getType()
	 * @see #getCommunicationInstance()
	 * @generated
	 */
	EReference getCommunicationInstance_Type();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.CommunicationInstance#getProvidedCommunicationInstance <em>Provided Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provided Communication Instance</em>'.
	 * @see camel.deployment.CommunicationInstance#getProvidedCommunicationInstance()
	 * @see #getCommunicationInstance()
	 * @generated
	 */
	EReference getCommunicationInstance_ProvidedCommunicationInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.CommunicationInstance#getRequiredCommunicationInstance <em>Required Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Required Communication Instance</em>'.
	 * @see camel.deployment.CommunicationInstance#getRequiredCommunicationInstance()
	 * @see #getCommunicationInstance()
	 * @generated
	 */
	EReference getCommunicationInstance_RequiredCommunicationInstance();

	/**
	 * Returns the meta object for class '{@link camel.deployment.CommunicationPortInstance <em>Communication Port Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Port Instance</em>'.
	 * @see camel.deployment.CommunicationPortInstance
	 * @generated
	 */
	EClass getCommunicationPortInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.CommunicationPortInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see camel.deployment.CommunicationPortInstance#getType()
	 * @see #getCommunicationPortInstance()
	 * @generated
	 */
	EReference getCommunicationPortInstance_Type();

	/**
	 * Returns the meta object for class '{@link camel.deployment.ProvidedCommunicationInstance <em>Provided Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provided Communication Instance</em>'.
	 * @see camel.deployment.ProvidedCommunicationInstance
	 * @generated
	 */
	EClass getProvidedCommunicationInstance();

	/**
	 * Returns the meta object for class '{@link camel.deployment.RequiredCommunicationInstance <em>Required Communication Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required Communication Instance</em>'.
	 * @see camel.deployment.RequiredCommunicationInstance
	 * @generated
	 */
	EClass getRequiredCommunicationInstance();

	/**
	 * Returns the meta object for class '{@link camel.deployment.HostingInstance <em>Hosting Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hosting Instance</em>'.
	 * @see camel.deployment.HostingInstance
	 * @generated
	 */
	EClass getHostingInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.HostingInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see camel.deployment.HostingInstance#getType()
	 * @see #getHostingInstance()
	 * @generated
	 */
	EReference getHostingInstance_Type();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.HostingInstance#getProvidedHostInstance <em>Provided Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Provided Host Instance</em>'.
	 * @see camel.deployment.HostingInstance#getProvidedHostInstance()
	 * @see #getHostingInstance()
	 * @generated
	 */
	EReference getHostingInstance_ProvidedHostInstance();

	/**
	 * Returns the meta object for the reference list '{@link camel.deployment.HostingInstance#getRequiredHostInstances <em>Required Host Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Required Host Instances</em>'.
	 * @see camel.deployment.HostingInstance#getRequiredHostInstances()
	 * @see #getHostingInstance()
	 * @generated
	 */
	EReference getHostingInstance_RequiredHostInstances();

	/**
	 * Returns the meta object for class '{@link camel.deployment.HostingPortInstance <em>Hosting Port Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hosting Port Instance</em>'.
	 * @see camel.deployment.HostingPortInstance
	 * @generated
	 */
	EClass getHostingPortInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.HostingPortInstance#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see camel.deployment.HostingPortInstance#getType()
	 * @see #getHostingPortInstance()
	 * @generated
	 */
	EReference getHostingPortInstance_Type();

	/**
	 * Returns the meta object for class '{@link camel.deployment.ProvidedHostInstance <em>Provided Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provided Host Instance</em>'.
	 * @see camel.deployment.ProvidedHostInstance
	 * @generated
	 */
	EClass getProvidedHostInstance();

	/**
	 * Returns the meta object for class '{@link camel.deployment.RequiredHostInstance <em>Required Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Required Host Instance</em>'.
	 * @see camel.deployment.RequiredHostInstance
	 * @generated
	 */
	EClass getRequiredHostInstance();

	/**
	 * Returns the meta object for class '{@link camel.deployment.ScriptConfiguration <em>Script Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Script Configuration</em>'.
	 * @see camel.deployment.ScriptConfiguration
	 * @generated
	 */
	EClass getScriptConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ScriptConfiguration#getUploadCommand <em>Upload Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Upload Command</em>'.
	 * @see camel.deployment.ScriptConfiguration#getUploadCommand()
	 * @see #getScriptConfiguration()
	 * @generated
	 */
	EAttribute getScriptConfiguration_UploadCommand();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ScriptConfiguration#getInstallCommand <em>Install Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Install Command</em>'.
	 * @see camel.deployment.ScriptConfiguration#getInstallCommand()
	 * @see #getScriptConfiguration()
	 * @generated
	 */
	EAttribute getScriptConfiguration_InstallCommand();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ScriptConfiguration#getStartCommand <em>Start Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Command</em>'.
	 * @see camel.deployment.ScriptConfiguration#getStartCommand()
	 * @see #getScriptConfiguration()
	 * @generated
	 */
	EAttribute getScriptConfiguration_StartCommand();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ScriptConfiguration#getStopCommand <em>Stop Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Stop Command</em>'.
	 * @see camel.deployment.ScriptConfiguration#getStopCommand()
	 * @see #getScriptConfiguration()
	 * @generated
	 */
	EAttribute getScriptConfiguration_StopCommand();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ScriptConfiguration#getDownloadCommand <em>Download Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Download Command</em>'.
	 * @see camel.deployment.ScriptConfiguration#getDownloadCommand()
	 * @see #getScriptConfiguration()
	 * @generated
	 */
	EAttribute getScriptConfiguration_DownloadCommand();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ScriptConfiguration#getConfigureCommand <em>Configure Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Configure Command</em>'.
	 * @see camel.deployment.ScriptConfiguration#getConfigureCommand()
	 * @see #getScriptConfiguration()
	 * @generated
	 */
	EAttribute getScriptConfiguration_ConfigureCommand();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ScriptConfiguration#getUpdateCommand <em>Update Command</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Update Command</em>'.
	 * @see camel.deployment.ScriptConfiguration#getUpdateCommand()
	 * @see #getScriptConfiguration()
	 * @generated
	 */
	EAttribute getScriptConfiguration_UpdateCommand();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ScriptConfiguration#getDevopsTool <em>Devops Tool</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Devops Tool</em>'.
	 * @see camel.deployment.ScriptConfiguration#getDevopsTool()
	 * @see #getScriptConfiguration()
	 * @generated
	 */
	EAttribute getScriptConfiguration_DevopsTool();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ScriptConfiguration#getOs <em>Os</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Os</em>'.
	 * @see camel.deployment.ScriptConfiguration#getOs()
	 * @see #getScriptConfiguration()
	 * @generated
	 */
	EAttribute getScriptConfiguration_Os();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ScriptConfiguration#getImageId <em>Image Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Image Id</em>'.
	 * @see camel.deployment.ScriptConfiguration#getImageId()
	 * @see #getScriptConfiguration()
	 * @generated
	 */
	EAttribute getScriptConfiguration_ImageId();

	/**
	 * Returns the meta object for class '{@link camel.deployment.ClusterConfiguration <em>Cluster Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cluster Configuration</em>'.
	 * @see camel.deployment.ClusterConfiguration
	 * @generated
	 */
	EClass getClusterConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ClusterConfiguration#getDownloadURL <em>Download URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Download URL</em>'.
	 * @see camel.deployment.ClusterConfiguration#getDownloadURL()
	 * @see #getClusterConfiguration()
	 * @generated
	 */
	EAttribute getClusterConfiguration_DownloadURL();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.ClusterConfiguration#getConfigParameters <em>Config Parameters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Config Parameters</em>'.
	 * @see camel.deployment.ClusterConfiguration#getConfigParameters()
	 * @see #getClusterConfiguration()
	 * @generated
	 */
	EReference getClusterConfiguration_ConfigParameters();

	/**
	 * Returns the meta object for class '{@link camel.deployment.LocationCoupling <em>Location Coupling</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Location Coupling</em>'.
	 * @see camel.deployment.LocationCoupling
	 * @generated
	 */
	EClass getLocationCoupling();

	/**
	 * Returns the meta object for the reference list '{@link camel.deployment.LocationCoupling#getSoftwareComponents <em>Software Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Software Components</em>'.
	 * @see camel.deployment.LocationCoupling#getSoftwareComponents()
	 * @see #getLocationCoupling()
	 * @generated
	 */
	EReference getLocationCoupling_SoftwareComponents();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.LocationCoupling#getCouplingType <em>Coupling Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Coupling Type</em>'.
	 * @see camel.deployment.LocationCoupling#getCouplingType()
	 * @see #getLocationCoupling()
	 * @generated
	 */
	EAttribute getLocationCoupling_CouplingType();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.LocationCoupling#isRelaxed <em>Relaxed</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Relaxed</em>'.
	 * @see camel.deployment.LocationCoupling#isRelaxed()
	 * @see #getLocationCoupling()
	 * @generated
	 */
	EAttribute getLocationCoupling_Relaxed();

	/**
	 * Returns the meta object for class '{@link camel.deployment.Container <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Container</em>'.
	 * @see camel.deployment.Container
	 * @generated
	 */
	EClass getContainer();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.Container#getRequiredCommunications <em>Required Communications</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Communications</em>'.
	 * @see camel.deployment.Container#getRequiredCommunications()
	 * @see #getContainer()
	 * @generated
	 */
	EReference getContainer_RequiredCommunications();

	/**
	 * Returns the meta object for the containment reference '{@link camel.deployment.Container#getRequiredHost <em>Required Host</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Host</em>'.
	 * @see camel.deployment.Container#getRequiredHost()
	 * @see #getContainer()
	 * @generated
	 */
	EReference getContainer_RequiredHost();

	/**
	 * Returns the meta object for class '{@link camel.deployment.ContainerInstance <em>Container Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Container Instance</em>'.
	 * @see camel.deployment.ContainerInstance
	 * @generated
	 */
	EClass getContainerInstance();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.ContainerInstance#getRequiredCommunicationInstances <em>Required Communication Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Required Communication Instances</em>'.
	 * @see camel.deployment.ContainerInstance#getRequiredCommunicationInstances()
	 * @see #getContainerInstance()
	 * @generated
	 */
	EReference getContainerInstance_RequiredCommunicationInstances();

	/**
	 * Returns the meta object for the containment reference '{@link camel.deployment.ContainerInstance#getRequiredHostInstance <em>Required Host Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Required Host Instance</em>'.
	 * @see camel.deployment.ContainerInstance#getRequiredHostInstance()
	 * @see #getContainerInstance()
	 * @generated
	 */
	EReference getContainerInstance_RequiredHostInstance();

	/**
	 * Returns the meta object for class '{@link camel.deployment.ComponentRelation <em>Component Relation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Relation</em>'.
	 * @see camel.deployment.ComponentRelation
	 * @generated
	 */
	EClass getComponentRelation();

	/**
	 * Returns the meta object for class '{@link camel.deployment.ComponentRelationInstance <em>Component Relation Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Component Relation Instance</em>'.
	 * @see camel.deployment.ComponentRelationInstance
	 * @generated
	 */
	EClass getComponentRelationInstance();

	/**
	 * Returns the meta object for class '{@link camel.deployment.PaaS <em>Paa S</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Paa S</em>'.
	 * @see camel.deployment.PaaS
	 * @generated
	 */
	EClass getPaaS();

	/**
	 * Returns the meta object for class '{@link camel.deployment.PaaSInstance <em>Paa SInstance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Paa SInstance</em>'.
	 * @see camel.deployment.PaaSInstance
	 * @generated
	 */
	EClass getPaaSInstance();

	/**
	 * Returns the meta object for class '{@link camel.deployment.PaaSConfiguration <em>Paa SConfiguration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Paa SConfiguration</em>'.
	 * @see camel.deployment.PaaSConfiguration
	 * @generated
	 */
	EClass getPaaSConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.PaaSConfiguration#getApi <em>Api</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Api</em>'.
	 * @see camel.deployment.PaaSConfiguration#getApi()
	 * @see #getPaaSConfiguration()
	 * @generated
	 */
	EAttribute getPaaSConfiguration_Api();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.PaaSConfiguration#getVersion <em>Version</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Version</em>'.
	 * @see camel.deployment.PaaSConfiguration#getVersion()
	 * @see #getPaaSConfiguration()
	 * @generated
	 */
	EAttribute getPaaSConfiguration_Version();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.PaaSConfiguration#getEndpoint <em>Endpoint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Endpoint</em>'.
	 * @see camel.deployment.PaaSConfiguration#getEndpoint()
	 * @see #getPaaSConfiguration()
	 * @generated
	 */
	EAttribute getPaaSConfiguration_Endpoint();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.PaaSConfiguration#getDownloadURL <em>Download URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Download URL</em>'.
	 * @see camel.deployment.PaaSConfiguration#getDownloadURL()
	 * @see #getPaaSConfiguration()
	 * @generated
	 */
	EAttribute getPaaSConfiguration_DownloadURL();

	/**
	 * Returns the meta object for class '{@link camel.deployment.ServerlessConfiguration <em>Serverless Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Serverless Configuration</em>'.
	 * @see camel.deployment.ServerlessConfiguration
	 * @generated
	 */
	EClass getServerlessConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ServerlessConfiguration#getBinaryCodeURL <em>Binary Code URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Binary Code URL</em>'.
	 * @see camel.deployment.ServerlessConfiguration#getBinaryCodeURL()
	 * @see #getServerlessConfiguration()
	 * @generated
	 */
	EAttribute getServerlessConfiguration_BinaryCodeURL();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.ServerlessConfiguration#isContinuousDeployment <em>Continuous Deployment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Continuous Deployment</em>'.
	 * @see camel.deployment.ServerlessConfiguration#isContinuousDeployment()
	 * @see #getServerlessConfiguration()
	 * @generated
	 */
	EAttribute getServerlessConfiguration_ContinuousDeployment();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.deployment.ServerlessConfiguration#getEnvironmentConfigParams <em>Environment Config Params</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Environment Config Params</em>'.
	 * @see camel.deployment.ServerlessConfiguration#getEnvironmentConfigParams()
	 * @see #getServerlessConfiguration()
	 * @generated
	 */
	EReference getServerlessConfiguration_EnvironmentConfigParams();

	/**
	 * Returns the meta object for the containment reference '{@link camel.deployment.ServerlessConfiguration#getBuildConfiguration <em>Build Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Build Configuration</em>'.
	 * @see camel.deployment.ServerlessConfiguration#getBuildConfiguration()
	 * @see #getServerlessConfiguration()
	 * @generated
	 */
	EReference getServerlessConfiguration_BuildConfiguration();

	/**
	 * Returns the meta object for the containment reference '{@link camel.deployment.ServerlessConfiguration#getEventConfiguration <em>Event Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Event Configuration</em>'.
	 * @see camel.deployment.ServerlessConfiguration#getEventConfiguration()
	 * @see #getServerlessConfiguration()
	 * @generated
	 */
	EReference getServerlessConfiguration_EventConfiguration();

	/**
	 * Returns the meta object for class '{@link camel.deployment.BuildConfiguration <em>Build Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Build Configuration</em>'.
	 * @see camel.deployment.BuildConfiguration
	 * @generated
	 */
	EClass getBuildConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.BuildConfiguration#getArtifactId <em>Artifact Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Artifact Id</em>'.
	 * @see camel.deployment.BuildConfiguration#getArtifactId()
	 * @see #getBuildConfiguration()
	 * @generated
	 */
	EAttribute getBuildConfiguration_ArtifactId();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.BuildConfiguration#getBuildFramework <em>Build Framework</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Build Framework</em>'.
	 * @see camel.deployment.BuildConfiguration#getBuildFramework()
	 * @see #getBuildConfiguration()
	 * @generated
	 */
	EAttribute getBuildConfiguration_BuildFramework();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.BuildConfiguration#getSourceCodeURL <em>Source Code URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Source Code URL</em>'.
	 * @see camel.deployment.BuildConfiguration#getSourceCodeURL()
	 * @see #getBuildConfiguration()
	 * @generated
	 */
	EAttribute getBuildConfiguration_SourceCodeURL();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.BuildConfiguration#getInclude <em>Include</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Include</em>'.
	 * @see camel.deployment.BuildConfiguration#getInclude()
	 * @see #getBuildConfiguration()
	 * @generated
	 */
	EAttribute getBuildConfiguration_Include();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.BuildConfiguration#getExclude <em>Exclude</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Exclude</em>'.
	 * @see camel.deployment.BuildConfiguration#getExclude()
	 * @see #getBuildConfiguration()
	 * @generated
	 */
	EAttribute getBuildConfiguration_Exclude();

	/**
	 * Returns the meta object for class '{@link camel.deployment.EventConfiguration <em>Event Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Event Configuration</em>'.
	 * @see camel.deployment.EventConfiguration
	 * @generated
	 */
	EClass getEventConfiguration();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.EventConfiguration#getHttpMethodName <em>Http Method Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Http Method Name</em>'.
	 * @see camel.deployment.EventConfiguration#getHttpMethodName()
	 * @see #getEventConfiguration()
	 * @generated
	 */
	EAttribute getEventConfiguration_HttpMethodName();

	/**
	 * Returns the meta object for the attribute '{@link camel.deployment.EventConfiguration#getHttpMethodType <em>Http Method Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Http Method Type</em>'.
	 * @see camel.deployment.EventConfiguration#getHttpMethodType()
	 * @see #getEventConfiguration()
	 * @generated
	 */
	EAttribute getEventConfiguration_HttpMethodType();

	/**
	 * Returns the meta object for the reference '{@link camel.deployment.EventConfiguration#getExecutionSchedule <em>Execution Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Execution Schedule</em>'.
	 * @see camel.deployment.EventConfiguration#getExecutionSchedule()
	 * @see #getEventConfiguration()
	 * @generated
	 */
	EReference getEventConfiguration_ExecutionSchedule();

	/**
	 * Returns the meta object for the containment reference '{@link camel.deployment.EventConfiguration#getScheduledExecutionConfig <em>Scheduled Execution Config</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Scheduled Execution Config</em>'.
	 * @see camel.deployment.EventConfiguration#getScheduledExecutionConfig()
	 * @see #getEventConfiguration()
	 * @generated
	 */
	EReference getEventConfiguration_ScheduledExecutionConfig();

	/**
	 * Returns the meta object for enum '{@link camel.deployment.LocationCouplingType <em>Location Coupling Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Location Coupling Type</em>'.
	 * @see camel.deployment.LocationCouplingType
	 * @generated
	 */
	EEnum getLocationCouplingType();

	/**
	 * Returns the meta object for enum '{@link camel.deployment.HTTPMethodType <em>HTTP Method Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>HTTP Method Type</em>'.
	 * @see camel.deployment.HTTPMethodType
	 * @generated
	 */
	EEnum getHTTPMethodType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	DeploymentFactory getDeploymentFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link camel.deployment.impl.DeploymentInstanceModelImpl <em>Instance Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.DeploymentInstanceModelImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getDeploymentInstanceModel()
		 * @generated
		 */
		EClass DEPLOYMENT_INSTANCE_MODEL = eINSTANCE.getDeploymentInstanceModel();

		/**
		 * The meta object literal for the '<em><b>Software Component Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_INSTANCE_MODEL__SOFTWARE_COMPONENT_INSTANCES = eINSTANCE.getDeploymentInstanceModel_SoftwareComponentInstances();

		/**
		 * The meta object literal for the '<em><b>Vm Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_INSTANCE_MODEL__VM_INSTANCES = eINSTANCE.getDeploymentInstanceModel_VmInstances();

		/**
		 * The meta object literal for the '<em><b>Communication Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_INSTANCE_MODEL__COMMUNICATION_INSTANCES = eINSTANCE.getDeploymentInstanceModel_CommunicationInstances();

		/**
		 * The meta object literal for the '<em><b>Hosting Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_INSTANCE_MODEL__HOSTING_INSTANCES = eINSTANCE.getDeploymentInstanceModel_HostingInstances();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_INSTANCE_MODEL__TYPE = eINSTANCE.getDeploymentInstanceModel_Type();

		/**
		 * The meta object literal for the '<em><b>Container Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_INSTANCE_MODEL__CONTAINER_INSTANCES = eINSTANCE.getDeploymentInstanceModel_ContainerInstances();

		/**
		 * The meta object literal for the '<em><b>Paas Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_INSTANCE_MODEL__PAAS_INSTANCES = eINSTANCE.getDeploymentInstanceModel_PaasInstances();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.DeploymentModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.DeploymentModelImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getDeploymentModel()
		 * @generated
		 */
		EClass DEPLOYMENT_MODEL = eINSTANCE.getDeploymentModel();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.DeploymentTypeModelImpl <em>Type Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.DeploymentTypeModelImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getDeploymentTypeModel()
		 * @generated
		 */
		EClass DEPLOYMENT_TYPE_MODEL = eINSTANCE.getDeploymentTypeModel();

		/**
		 * The meta object literal for the '<em><b>Software Components</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_TYPE_MODEL__SOFTWARE_COMPONENTS = eINSTANCE.getDeploymentTypeModel_SoftwareComponents();

		/**
		 * The meta object literal for the '<em><b>Vms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_TYPE_MODEL__VMS = eINSTANCE.getDeploymentTypeModel_Vms();

		/**
		 * The meta object literal for the '<em><b>Paases</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_TYPE_MODEL__PAASES = eINSTANCE.getDeploymentTypeModel_Paases();

		/**
		 * The meta object literal for the '<em><b>Communications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_TYPE_MODEL__COMMUNICATIONS = eINSTANCE.getDeploymentTypeModel_Communications();

		/**
		 * The meta object literal for the '<em><b>Hostings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_TYPE_MODEL__HOSTINGS = eINSTANCE.getDeploymentTypeModel_Hostings();

		/**
		 * The meta object literal for the '<em><b>Requirement Sets</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_TYPE_MODEL__REQUIREMENT_SETS = eINSTANCE.getDeploymentTypeModel_RequirementSets();

		/**
		 * The meta object literal for the '<em><b>Global Requirement Set</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_TYPE_MODEL__GLOBAL_REQUIREMENT_SET = eINSTANCE.getDeploymentTypeModel_GlobalRequirementSet();

		/**
		 * The meta object literal for the '<em><b>Location Couplings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_TYPE_MODEL__LOCATION_COUPLINGS = eINSTANCE.getDeploymentTypeModel_LocationCouplings();

		/**
		 * The meta object literal for the '<em><b>Containers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DEPLOYMENT_TYPE_MODEL__CONTAINERS = eINSTANCE.getDeploymentTypeModel_Containers();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ComponentImpl <em>Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ComponentImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getComponent()
		 * @generated
		 */
		EClass COMPONENT = eINSTANCE.getComponent();

		/**
		 * The meta object literal for the '<em><b>Provided Communications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__PROVIDED_COMMUNICATIONS = eINSTANCE.getComponent_ProvidedCommunications();

		/**
		 * The meta object literal for the '<em><b>Provided Hosts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__PROVIDED_HOSTS = eINSTANCE.getComponent_ProvidedHosts();

		/**
		 * The meta object literal for the '<em><b>Configurations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT__CONFIGURATIONS = eINSTANCE.getComponent_Configurations();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.SoftwareComponentImpl <em>Software Component</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.SoftwareComponentImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getSoftwareComponent()
		 * @generated
		 */
		EClass SOFTWARE_COMPONENT = eINSTANCE.getSoftwareComponent();

		/**
		 * The meta object literal for the '<em><b>Required Communications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT__REQUIRED_COMMUNICATIONS = eINSTANCE.getSoftwareComponent_RequiredCommunications();

		/**
		 * The meta object literal for the '<em><b>Required Host</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT__REQUIRED_HOST = eINSTANCE.getSoftwareComponent_RequiredHost();

		/**
		 * The meta object literal for the '<em><b>Requirement Set</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT__REQUIREMENT_SET = eINSTANCE.getSoftwareComponent_RequirementSet();

		/**
		 * The meta object literal for the '<em><b>Generates Data</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT__GENERATES_DATA = eINSTANCE.getSoftwareComponent_GeneratesData();

		/**
		 * The meta object literal for the '<em><b>Consumes Data</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT__CONSUMES_DATA = eINSTANCE.getSoftwareComponent_ConsumesData();

		/**
		 * The meta object literal for the '<em><b>Manages Data Source</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT__MANAGES_DATA_SOURCE = eINSTANCE.getSoftwareComponent_ManagesDataSource();

		/**
		 * The meta object literal for the '<em><b>Long Lived</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFTWARE_COMPONENT__LONG_LIVED = eINSTANCE.getSoftwareComponent_LongLived();

		/**
		 * The meta object literal for the '<em><b>Co Instance Hosting</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFTWARE_COMPONENT__CO_INSTANCE_HOSTING = eINSTANCE.getSoftwareComponent_CoInstanceHosting();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.VMImpl <em>VM</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.VMImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getVM()
		 * @generated
		 */
		EClass VM = eINSTANCE.getVM();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.RequirementSetImpl <em>Requirement Set</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.RequirementSetImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getRequirementSet()
		 * @generated
		 */
		EClass REQUIREMENT_SET = eINSTANCE.getRequirementSet();

		/**
		 * The meta object literal for the '<em><b>Resource Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_SET__RESOURCE_REQUIREMENT = eINSTANCE.getRequirementSet_ResourceRequirement();

		/**
		 * The meta object literal for the '<em><b>Paas Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_SET__PAAS_REQUIREMENT = eINSTANCE.getRequirementSet_PaasRequirement();

		/**
		 * The meta object literal for the '<em><b>Location Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_SET__LOCATION_REQUIREMENT = eINSTANCE.getRequirementSet_LocationRequirement();

		/**
		 * The meta object literal for the '<em><b>Provider Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_SET__PROVIDER_REQUIREMENT = eINSTANCE.getRequirementSet_ProviderRequirement();

		/**
		 * The meta object literal for the '<em><b>Vertical Scale Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_SET__VERTICAL_SCALE_REQUIREMENT = eINSTANCE.getRequirementSet_VerticalScaleRequirement();

		/**
		 * The meta object literal for the '<em><b>Horizontal Scale Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_SET__HORIZONTAL_SCALE_REQUIREMENT = eINSTANCE.getRequirementSet_HorizontalScaleRequirement();

		/**
		 * The meta object literal for the '<em><b>Security Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_SET__SECURITY_REQUIREMENT = eINSTANCE.getRequirementSet_SecurityRequirement();

		/**
		 * The meta object literal for the '<em><b>Os Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_SET__OS_REQUIREMENT = eINSTANCE.getRequirementSet_OsRequirement();

		/**
		 * The meta object literal for the '<em><b>Image Requirement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_SET__IMAGE_REQUIREMENT = eINSTANCE.getRequirementSet_ImageRequirement();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ConfigurationImpl <em>Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ConfigurationImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getConfiguration()
		 * @generated
		 */
		EClass CONFIGURATION = eINSTANCE.getConfiguration();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.CommunicationImpl <em>Communication</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.CommunicationImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getCommunication()
		 * @generated
		 */
		EClass COMMUNICATION = eINSTANCE.getCommunication();

		/**
		 * The meta object literal for the '<em><b>Provided Communication</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION__PROVIDED_COMMUNICATION = eINSTANCE.getCommunication_ProvidedCommunication();

		/**
		 * The meta object literal for the '<em><b>Required Communication</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION__REQUIRED_COMMUNICATION = eINSTANCE.getCommunication_RequiredCommunication();

		/**
		 * The meta object literal for the '<em><b>Provided Port Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION__PROVIDED_PORT_CONFIGURATION = eINSTANCE.getCommunication_ProvidedPortConfiguration();

		/**
		 * The meta object literal for the '<em><b>Required Port Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION__REQUIRED_PORT_CONFIGURATION = eINSTANCE.getCommunication_RequiredPortConfiguration();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.CommunicationPortImpl <em>Communication Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.CommunicationPortImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getCommunicationPort()
		 * @generated
		 */
		EClass COMMUNICATION_PORT = eINSTANCE.getCommunicationPort();

		/**
		 * The meta object literal for the '<em><b>Port Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_PORT__PORT_NUMBER = eINSTANCE.getCommunicationPort_PortNumber();

		/**
		 * The meta object literal for the '<em><b>Low Port Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_PORT__LOW_PORT_NUMBER = eINSTANCE.getCommunicationPort_LowPortNumber();

		/**
		 * The meta object literal for the '<em><b>High Port Number</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMMUNICATION_PORT__HIGH_PORT_NUMBER = eINSTANCE.getCommunicationPort_HighPortNumber();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ProvidedCommunicationImpl <em>Provided Communication</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ProvidedCommunicationImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getProvidedCommunication()
		 * @generated
		 */
		EClass PROVIDED_COMMUNICATION = eINSTANCE.getProvidedCommunication();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.RequiredCommunicationImpl <em>Required Communication</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.RequiredCommunicationImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getRequiredCommunication()
		 * @generated
		 */
		EClass REQUIRED_COMMUNICATION = eINSTANCE.getRequiredCommunication();

		/**
		 * The meta object literal for the '<em><b>Is Mandatory</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute REQUIRED_COMMUNICATION__IS_MANDATORY = eINSTANCE.getRequiredCommunication_IsMandatory();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.HostingImpl <em>Hosting</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.HostingImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getHosting()
		 * @generated
		 */
		EClass HOSTING = eINSTANCE.getHosting();

		/**
		 * The meta object literal for the '<em><b>Provided Host</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING__PROVIDED_HOST = eINSTANCE.getHosting_ProvidedHost();

		/**
		 * The meta object literal for the '<em><b>Required Hosts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING__REQUIRED_HOSTS = eINSTANCE.getHosting_RequiredHosts();

		/**
		 * The meta object literal for the '<em><b>Provided Host Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING__PROVIDED_HOST_CONFIGURATION = eINSTANCE.getHosting_ProvidedHostConfiguration();

		/**
		 * The meta object literal for the '<em><b>Required Hosts Configuration</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING__REQUIRED_HOSTS_CONFIGURATION = eINSTANCE.getHosting_RequiredHostsConfiguration();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.HostingPortImpl <em>Hosting Port</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.HostingPortImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getHostingPort()
		 * @generated
		 */
		EClass HOSTING_PORT = eINSTANCE.getHostingPort();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ProvidedHostImpl <em>Provided Host</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ProvidedHostImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getProvidedHost()
		 * @generated
		 */
		EClass PROVIDED_HOST = eINSTANCE.getProvidedHost();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.RequiredHostImpl <em>Required Host</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.RequiredHostImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getRequiredHost()
		 * @generated
		 */
		EClass REQUIRED_HOST = eINSTANCE.getRequiredHost();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ComponentInstanceImpl <em>Component Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ComponentInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getComponentInstance()
		 * @generated
		 */
		EClass COMPONENT_INSTANCE = eINSTANCE.getComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE__TYPE = eINSTANCE.getComponentInstance_Type();

		/**
		 * The meta object literal for the '<em><b>Provided Communication Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE__PROVIDED_COMMUNICATION_INSTANCES = eINSTANCE.getComponentInstance_ProvidedCommunicationInstances();

		/**
		 * The meta object literal for the '<em><b>Provided Host Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPONENT_INSTANCE__PROVIDED_HOST_INSTANCES = eINSTANCE.getComponentInstance_ProvidedHostInstances();

		/**
		 * The meta object literal for the '<em><b>Instantiated On</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_INSTANCE__INSTANTIATED_ON = eINSTANCE.getComponentInstance_InstantiatedOn();

		/**
		 * The meta object literal for the '<em><b>Destroyed On</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPONENT_INSTANCE__DESTROYED_ON = eINSTANCE.getComponentInstance_DestroyedOn();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.SoftwareComponentInstanceImpl <em>Software Component Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.SoftwareComponentInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getSoftwareComponentInstance()
		 * @generated
		 */
		EClass SOFTWARE_COMPONENT_INSTANCE = eINSTANCE.getSoftwareComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Required Communication Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT_INSTANCE__REQUIRED_COMMUNICATION_INSTANCES = eINSTANCE.getSoftwareComponentInstance_RequiredCommunicationInstances();

		/**
		 * The meta object literal for the '<em><b>Required Host Instance</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT_INSTANCE__REQUIRED_HOST_INSTANCE = eINSTANCE.getSoftwareComponentInstance_RequiredHostInstance();

		/**
		 * The meta object literal for the '<em><b>Consumes Data Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT_INSTANCE__CONSUMES_DATA_INSTANCE = eINSTANCE.getSoftwareComponentInstance_ConsumesDataInstance();

		/**
		 * The meta object literal for the '<em><b>Generates Data Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT_INSTANCE__GENERATES_DATA_INSTANCE = eINSTANCE.getSoftwareComponentInstance_GeneratesDataInstance();

		/**
		 * The meta object literal for the '<em><b>Manages Data Source Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT_INSTANCE__MANAGES_DATA_SOURCE_INSTANCE = eINSTANCE.getSoftwareComponentInstance_ManagesDataSourceInstance();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.VMInstanceImpl <em>VM Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.VMInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getVMInstance()
		 * @generated
		 */
		EClass VM_INSTANCE = eINSTANCE.getVMInstance();

		/**
		 * The meta object literal for the '<em><b>Location</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM_INSTANCE__LOCATION = eINSTANCE.getVMInstance_Location();

		/**
		 * The meta object literal for the '<em><b>Os</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM_INSTANCE__OS = eINSTANCE.getVMInstance_Os();

		/**
		 * The meta object literal for the '<em><b>Check Dates</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation VM_INSTANCE___CHECK_DATES__VMINSTANCE = eINSTANCE.getVMInstance__CheckDates__VMInstance();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.CommunicationInstanceImpl <em>Communication Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.CommunicationInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getCommunicationInstance()
		 * @generated
		 */
		EClass COMMUNICATION_INSTANCE = eINSTANCE.getCommunicationInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_INSTANCE__TYPE = eINSTANCE.getCommunicationInstance_Type();

		/**
		 * The meta object literal for the '<em><b>Provided Communication Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_INSTANCE__PROVIDED_COMMUNICATION_INSTANCE = eINSTANCE.getCommunicationInstance_ProvidedCommunicationInstance();

		/**
		 * The meta object literal for the '<em><b>Required Communication Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_INSTANCE__REQUIRED_COMMUNICATION_INSTANCE = eINSTANCE.getCommunicationInstance_RequiredCommunicationInstance();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.CommunicationPortInstanceImpl <em>Communication Port Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.CommunicationPortInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getCommunicationPortInstance()
		 * @generated
		 */
		EClass COMMUNICATION_PORT_INSTANCE = eINSTANCE.getCommunicationPortInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_PORT_INSTANCE__TYPE = eINSTANCE.getCommunicationPortInstance_Type();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ProvidedCommunicationInstanceImpl <em>Provided Communication Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ProvidedCommunicationInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getProvidedCommunicationInstance()
		 * @generated
		 */
		EClass PROVIDED_COMMUNICATION_INSTANCE = eINSTANCE.getProvidedCommunicationInstance();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.RequiredCommunicationInstanceImpl <em>Required Communication Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.RequiredCommunicationInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getRequiredCommunicationInstance()
		 * @generated
		 */
		EClass REQUIRED_COMMUNICATION_INSTANCE = eINSTANCE.getRequiredCommunicationInstance();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.HostingInstanceImpl <em>Hosting Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.HostingInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getHostingInstance()
		 * @generated
		 */
		EClass HOSTING_INSTANCE = eINSTANCE.getHostingInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING_INSTANCE__TYPE = eINSTANCE.getHostingInstance_Type();

		/**
		 * The meta object literal for the '<em><b>Provided Host Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING_INSTANCE__PROVIDED_HOST_INSTANCE = eINSTANCE.getHostingInstance_ProvidedHostInstance();

		/**
		 * The meta object literal for the '<em><b>Required Host Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING_INSTANCE__REQUIRED_HOST_INSTANCES = eINSTANCE.getHostingInstance_RequiredHostInstances();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.HostingPortInstanceImpl <em>Hosting Port Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.HostingPortInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getHostingPortInstance()
		 * @generated
		 */
		EClass HOSTING_PORT_INSTANCE = eINSTANCE.getHostingPortInstance();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HOSTING_PORT_INSTANCE__TYPE = eINSTANCE.getHostingPortInstance_Type();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ProvidedHostInstanceImpl <em>Provided Host Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ProvidedHostInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getProvidedHostInstance()
		 * @generated
		 */
		EClass PROVIDED_HOST_INSTANCE = eINSTANCE.getProvidedHostInstance();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.RequiredHostInstanceImpl <em>Required Host Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.RequiredHostInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getRequiredHostInstance()
		 * @generated
		 */
		EClass REQUIRED_HOST_INSTANCE = eINSTANCE.getRequiredHostInstance();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ScriptConfigurationImpl <em>Script Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ScriptConfigurationImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getScriptConfiguration()
		 * @generated
		 */
		EClass SCRIPT_CONFIGURATION = eINSTANCE.getScriptConfiguration();

		/**
		 * The meta object literal for the '<em><b>Upload Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT_CONFIGURATION__UPLOAD_COMMAND = eINSTANCE.getScriptConfiguration_UploadCommand();

		/**
		 * The meta object literal for the '<em><b>Install Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT_CONFIGURATION__INSTALL_COMMAND = eINSTANCE.getScriptConfiguration_InstallCommand();

		/**
		 * The meta object literal for the '<em><b>Start Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT_CONFIGURATION__START_COMMAND = eINSTANCE.getScriptConfiguration_StartCommand();

		/**
		 * The meta object literal for the '<em><b>Stop Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT_CONFIGURATION__STOP_COMMAND = eINSTANCE.getScriptConfiguration_StopCommand();

		/**
		 * The meta object literal for the '<em><b>Download Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT_CONFIGURATION__DOWNLOAD_COMMAND = eINSTANCE.getScriptConfiguration_DownloadCommand();

		/**
		 * The meta object literal for the '<em><b>Configure Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT_CONFIGURATION__CONFIGURE_COMMAND = eINSTANCE.getScriptConfiguration_ConfigureCommand();

		/**
		 * The meta object literal for the '<em><b>Update Command</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT_CONFIGURATION__UPDATE_COMMAND = eINSTANCE.getScriptConfiguration_UpdateCommand();

		/**
		 * The meta object literal for the '<em><b>Devops Tool</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT_CONFIGURATION__DEVOPS_TOOL = eINSTANCE.getScriptConfiguration_DevopsTool();

		/**
		 * The meta object literal for the '<em><b>Os</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT_CONFIGURATION__OS = eINSTANCE.getScriptConfiguration_Os();

		/**
		 * The meta object literal for the '<em><b>Image Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCRIPT_CONFIGURATION__IMAGE_ID = eINSTANCE.getScriptConfiguration_ImageId();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ClusterConfigurationImpl <em>Cluster Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ClusterConfigurationImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getClusterConfiguration()
		 * @generated
		 */
		EClass CLUSTER_CONFIGURATION = eINSTANCE.getClusterConfiguration();

		/**
		 * The meta object literal for the '<em><b>Download URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLUSTER_CONFIGURATION__DOWNLOAD_URL = eINSTANCE.getClusterConfiguration_DownloadURL();

		/**
		 * The meta object literal for the '<em><b>Config Parameters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CLUSTER_CONFIGURATION__CONFIG_PARAMETERS = eINSTANCE.getClusterConfiguration_ConfigParameters();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.LocationCouplingImpl <em>Location Coupling</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.LocationCouplingImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getLocationCoupling()
		 * @generated
		 */
		EClass LOCATION_COUPLING = eINSTANCE.getLocationCoupling();

		/**
		 * The meta object literal for the '<em><b>Software Components</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCATION_COUPLING__SOFTWARE_COMPONENTS = eINSTANCE.getLocationCoupling_SoftwareComponents();

		/**
		 * The meta object literal for the '<em><b>Coupling Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCATION_COUPLING__COUPLING_TYPE = eINSTANCE.getLocationCoupling_CouplingType();

		/**
		 * The meta object literal for the '<em><b>Relaxed</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCATION_COUPLING__RELAXED = eINSTANCE.getLocationCoupling_Relaxed();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ContainerImpl <em>Container</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ContainerImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getContainer()
		 * @generated
		 */
		EClass CONTAINER = eINSTANCE.getContainer();

		/**
		 * The meta object literal for the '<em><b>Required Communications</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER__REQUIRED_COMMUNICATIONS = eINSTANCE.getContainer_RequiredCommunications();

		/**
		 * The meta object literal for the '<em><b>Required Host</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER__REQUIRED_HOST = eINSTANCE.getContainer_RequiredHost();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ContainerInstanceImpl <em>Container Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ContainerInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getContainerInstance()
		 * @generated
		 */
		EClass CONTAINER_INSTANCE = eINSTANCE.getContainerInstance();

		/**
		 * The meta object literal for the '<em><b>Required Communication Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER_INSTANCE__REQUIRED_COMMUNICATION_INSTANCES = eINSTANCE.getContainerInstance_RequiredCommunicationInstances();

		/**
		 * The meta object literal for the '<em><b>Required Host Instance</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER_INSTANCE__REQUIRED_HOST_INSTANCE = eINSTANCE.getContainerInstance_RequiredHostInstance();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ComponentRelationImpl <em>Component Relation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ComponentRelationImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getComponentRelation()
		 * @generated
		 */
		EClass COMPONENT_RELATION = eINSTANCE.getComponentRelation();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ComponentRelationInstanceImpl <em>Component Relation Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ComponentRelationInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getComponentRelationInstance()
		 * @generated
		 */
		EClass COMPONENT_RELATION_INSTANCE = eINSTANCE.getComponentRelationInstance();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.PaaSImpl <em>Paa S</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.PaaSImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getPaaS()
		 * @generated
		 */
		EClass PAA_S = eINSTANCE.getPaaS();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.PaaSInstanceImpl <em>Paa SInstance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.PaaSInstanceImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getPaaSInstance()
		 * @generated
		 */
		EClass PAA_SINSTANCE = eINSTANCE.getPaaSInstance();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.PaaSConfigurationImpl <em>Paa SConfiguration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.PaaSConfigurationImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getPaaSConfiguration()
		 * @generated
		 */
		EClass PAA_SCONFIGURATION = eINSTANCE.getPaaSConfiguration();

		/**
		 * The meta object literal for the '<em><b>Api</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAA_SCONFIGURATION__API = eINSTANCE.getPaaSConfiguration_Api();

		/**
		 * The meta object literal for the '<em><b>Version</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAA_SCONFIGURATION__VERSION = eINSTANCE.getPaaSConfiguration_Version();

		/**
		 * The meta object literal for the '<em><b>Endpoint</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAA_SCONFIGURATION__ENDPOINT = eINSTANCE.getPaaSConfiguration_Endpoint();

		/**
		 * The meta object literal for the '<em><b>Download URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PAA_SCONFIGURATION__DOWNLOAD_URL = eINSTANCE.getPaaSConfiguration_DownloadURL();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.ServerlessConfigurationImpl <em>Serverless Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.ServerlessConfigurationImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getServerlessConfiguration()
		 * @generated
		 */
		EClass SERVERLESS_CONFIGURATION = eINSTANCE.getServerlessConfiguration();

		/**
		 * The meta object literal for the '<em><b>Binary Code URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVERLESS_CONFIGURATION__BINARY_CODE_URL = eINSTANCE.getServerlessConfiguration_BinaryCodeURL();

		/**
		 * The meta object literal for the '<em><b>Continuous Deployment</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVERLESS_CONFIGURATION__CONTINUOUS_DEPLOYMENT = eINSTANCE.getServerlessConfiguration_ContinuousDeployment();

		/**
		 * The meta object literal for the '<em><b>Environment Config Params</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVERLESS_CONFIGURATION__ENVIRONMENT_CONFIG_PARAMS = eINSTANCE.getServerlessConfiguration_EnvironmentConfigParams();

		/**
		 * The meta object literal for the '<em><b>Build Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVERLESS_CONFIGURATION__BUILD_CONFIGURATION = eINSTANCE.getServerlessConfiguration_BuildConfiguration();

		/**
		 * The meta object literal for the '<em><b>Event Configuration</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVERLESS_CONFIGURATION__EVENT_CONFIGURATION = eINSTANCE.getServerlessConfiguration_EventConfiguration();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.BuildConfigurationImpl <em>Build Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.BuildConfigurationImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getBuildConfiguration()
		 * @generated
		 */
		EClass BUILD_CONFIGURATION = eINSTANCE.getBuildConfiguration();

		/**
		 * The meta object literal for the '<em><b>Artifact Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUILD_CONFIGURATION__ARTIFACT_ID = eINSTANCE.getBuildConfiguration_ArtifactId();

		/**
		 * The meta object literal for the '<em><b>Build Framework</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUILD_CONFIGURATION__BUILD_FRAMEWORK = eINSTANCE.getBuildConfiguration_BuildFramework();

		/**
		 * The meta object literal for the '<em><b>Source Code URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUILD_CONFIGURATION__SOURCE_CODE_URL = eINSTANCE.getBuildConfiguration_SourceCodeURL();

		/**
		 * The meta object literal for the '<em><b>Include</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUILD_CONFIGURATION__INCLUDE = eINSTANCE.getBuildConfiguration_Include();

		/**
		 * The meta object literal for the '<em><b>Exclude</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute BUILD_CONFIGURATION__EXCLUDE = eINSTANCE.getBuildConfiguration_Exclude();

		/**
		 * The meta object literal for the '{@link camel.deployment.impl.EventConfigurationImpl <em>Event Configuration</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.impl.EventConfigurationImpl
		 * @see camel.deployment.impl.DeploymentPackageImpl#getEventConfiguration()
		 * @generated
		 */
		EClass EVENT_CONFIGURATION = eINSTANCE.getEventConfiguration();

		/**
		 * The meta object literal for the '<em><b>Http Method Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_CONFIGURATION__HTTP_METHOD_NAME = eINSTANCE.getEventConfiguration_HttpMethodName();

		/**
		 * The meta object literal for the '<em><b>Http Method Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EVENT_CONFIGURATION__HTTP_METHOD_TYPE = eINSTANCE.getEventConfiguration_HttpMethodType();

		/**
		 * The meta object literal for the '<em><b>Execution Schedule</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_CONFIGURATION__EXECUTION_SCHEDULE = eINSTANCE.getEventConfiguration_ExecutionSchedule();

		/**
		 * The meta object literal for the '<em><b>Scheduled Execution Config</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EVENT_CONFIGURATION__SCHEDULED_EXECUTION_CONFIG = eINSTANCE.getEventConfiguration_ScheduledExecutionConfig();

		/**
		 * The meta object literal for the '{@link camel.deployment.LocationCouplingType <em>Location Coupling Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.LocationCouplingType
		 * @see camel.deployment.impl.DeploymentPackageImpl#getLocationCouplingType()
		 * @generated
		 */
		EEnum LOCATION_COUPLING_TYPE = eINSTANCE.getLocationCouplingType();

		/**
		 * The meta object literal for the '{@link camel.deployment.HTTPMethodType <em>HTTP Method Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.deployment.HTTPMethodType
		 * @see camel.deployment.impl.DeploymentPackageImpl#getHTTPMethodType()
		 * @generated
		 */
		EEnum HTTP_METHOD_TYPE = eINSTANCE.getHTTPMethodType();

	}

} //DeploymentPackage
