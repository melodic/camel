/**
 */
package camel.mms;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Concept Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.mms.MmsConceptInstance#getPropertyInstance <em>Property Instance</em>}</li>
 * </ul>
 *
 * @see camel.mms.MmsPackage#getMmsConceptInstance()
 * @model
 * @generated
 */
public interface MmsConceptInstance extends MmsObject {
	/**
	 * Returns the value of the '<em><b>Property Instance</b></em>' containment reference list.
	 * The list contents are of type {@link camel.mms.MmsPropertyInstance}.
	 * It is bidirectional and its opposite is '{@link camel.mms.MmsPropertyInstance#getConceptInstance <em>Concept Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Instance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Instance</em>' containment reference list.
	 * @see camel.mms.MmsPackage#getMmsConceptInstance_PropertyInstance()
	 * @see camel.mms.MmsPropertyInstance#getConceptInstance
	 * @model opposite="conceptInstance" containment="true"
	 * @generated
	 */
	EList<MmsPropertyInstance> getPropertyInstance();

} // MmsConceptInstance
