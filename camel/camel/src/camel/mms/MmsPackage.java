/**
 */
package camel.mms;

import camel.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see camel.mms.MmsFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface MmsPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "mms";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.melodic.eu/metadata-schema";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "mms";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MmsPackage eINSTANCE = camel.mms.impl.MmsPackageImpl.init();

	/**
	 * The meta object id for the '{@link camel.mms.impl.MetaDataModelImpl <em>Meta Data Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.mms.impl.MetaDataModelImpl
	 * @see camel.mms.impl.MmsPackageImpl#getMetaDataModel()
	 * @generated
	 */
	int META_DATA_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_DATA_MODEL__NAME = CorePackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_DATA_MODEL__DESCRIPTION = CorePackage.MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_DATA_MODEL__ANNOTATIONS = CorePackage.MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_DATA_MODEL__ATTRIBUTES = CorePackage.MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_DATA_MODEL__SUB_FEATURES = CorePackage.MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_DATA_MODEL__IMPORT_URI = CorePackage.MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Metadata Elements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_DATA_MODEL__METADATA_ELEMENTS = CorePackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Meta Data Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_DATA_MODEL_FEATURE_COUNT = CorePackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_DATA_MODEL___AS_ERROR__BOOLEAN = CorePackage.MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Meta Data Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int META_DATA_MODEL_OPERATION_COUNT = CorePackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.mms.impl.MmsObjectImpl <em>Object</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.mms.impl.MmsObjectImpl
	 * @see camel.mms.impl.MmsPackageImpl#getMmsObject()
	 * @generated
	 */
	int MMS_OBJECT = 3;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_OBJECT__ID = 0;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_OBJECT__URI = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_OBJECT__NAME = 2;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_OBJECT__DESCRIPTION = 3;

	/**
	 * The number of structural features of the '<em>Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_OBJECT_FEATURE_COUNT = 4;

	/**
	 * The number of operations of the '<em>Object</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_OBJECT_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link camel.mms.impl.MmsConceptImpl <em>Concept</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.mms.impl.MmsConceptImpl
	 * @see camel.mms.impl.MmsPackageImpl#getMmsConcept()
	 * @generated
	 */
	int MMS_CONCEPT = 1;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT__ID = MMS_OBJECT__ID;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT__URI = MMS_OBJECT__URI;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT__NAME = MMS_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT__DESCRIPTION = MMS_OBJECT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Parent</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT__PARENT = MMS_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Concept</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT__CONCEPT = MMS_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Property</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT__PROPERTY = MMS_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Instance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT__INSTANCE = MMS_OBJECT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Concept</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT_FEATURE_COUNT = MMS_OBJECT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>Contains</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT___CONTAINS__MMSCONCEPT = MMS_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Concept</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT_OPERATION_COUNT = MMS_OBJECT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link camel.mms.impl.MmsPropertyImpl <em>Property</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.mms.impl.MmsPropertyImpl
	 * @see camel.mms.impl.MmsPackageImpl#getMmsProperty()
	 * @generated
	 */
	int MMS_PROPERTY = 2;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY__ID = MMS_OBJECT__ID;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY__URI = MMS_OBJECT__URI;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY__NAME = MMS_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY__DESCRIPTION = MMS_OBJECT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY__DOMAIN = MMS_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY__PROPERTY_TYPE = MMS_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Range</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY__RANGE = MMS_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Range Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY__RANGE_URI = MMS_OBJECT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY_FEATURE_COUNT = MMS_OBJECT_FEATURE_COUNT + 4;

	/**
	 * The number of operations of the '<em>Property</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY_OPERATION_COUNT = MMS_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.mms.impl.MmsConceptInstanceImpl <em>Concept Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.mms.impl.MmsConceptInstanceImpl
	 * @see camel.mms.impl.MmsPackageImpl#getMmsConceptInstance()
	 * @generated
	 */
	int MMS_CONCEPT_INSTANCE = 4;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT_INSTANCE__ID = MMS_OBJECT__ID;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT_INSTANCE__URI = MMS_OBJECT__URI;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT_INSTANCE__NAME = MMS_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT_INSTANCE__DESCRIPTION = MMS_OBJECT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Property Instance</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT_INSTANCE__PROPERTY_INSTANCE = MMS_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Concept Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT_INSTANCE_FEATURE_COUNT = MMS_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The number of operations of the '<em>Concept Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_CONCEPT_INSTANCE_OPERATION_COUNT = MMS_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.mms.impl.MmsPropertyInstanceImpl <em>Property Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.mms.impl.MmsPropertyInstanceImpl
	 * @see camel.mms.impl.MmsPackageImpl#getMmsPropertyInstance()
	 * @generated
	 */
	int MMS_PROPERTY_INSTANCE = 5;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY_INSTANCE__ID = MMS_OBJECT__ID;

	/**
	 * The feature id for the '<em><b>Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY_INSTANCE__URI = MMS_OBJECT__URI;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY_INSTANCE__NAME = MMS_OBJECT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY_INSTANCE__DESCRIPTION = MMS_OBJECT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Is A</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY_INSTANCE__IS_A = MMS_OBJECT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Property Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY_INSTANCE__PROPERTY_VALUE = MMS_OBJECT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Concept Instance</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY_INSTANCE__CONCEPT_INSTANCE = MMS_OBJECT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Property Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY_INSTANCE_FEATURE_COUNT = MMS_OBJECT_FEATURE_COUNT + 3;

	/**
	 * The number of operations of the '<em>Property Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MMS_PROPERTY_INSTANCE_OPERATION_COUNT = MMS_OBJECT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.mms.MmsPropertyType <em>Property Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.mms.MmsPropertyType
	 * @see camel.mms.impl.MmsPackageImpl#getMmsPropertyType()
	 * @generated
	 */
	int MMS_PROPERTY_TYPE = 6;


	/**
	 * Returns the meta object for class '{@link camel.mms.MetaDataModel <em>Meta Data Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Meta Data Model</em>'.
	 * @see camel.mms.MetaDataModel
	 * @generated
	 */
	EClass getMetaDataModel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.mms.MetaDataModel#getMetadataElements <em>Metadata Elements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Metadata Elements</em>'.
	 * @see camel.mms.MetaDataModel#getMetadataElements()
	 * @see #getMetaDataModel()
	 * @generated
	 */
	EReference getMetaDataModel_MetadataElements();

	/**
	 * Returns the meta object for class '{@link camel.mms.MmsConcept <em>Concept</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concept</em>'.
	 * @see camel.mms.MmsConcept
	 * @generated
	 */
	EClass getMmsConcept();

	/**
	 * Returns the meta object for the container reference '{@link camel.mms.MmsConcept#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Parent</em>'.
	 * @see camel.mms.MmsConcept#getParent()
	 * @see #getMmsConcept()
	 * @generated
	 */
	EReference getMmsConcept_Parent();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.mms.MmsConcept#getConcept <em>Concept</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Concept</em>'.
	 * @see camel.mms.MmsConcept#getConcept()
	 * @see #getMmsConcept()
	 * @generated
	 */
	EReference getMmsConcept_Concept();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.mms.MmsConcept#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property</em>'.
	 * @see camel.mms.MmsConcept#getProperty()
	 * @see #getMmsConcept()
	 * @generated
	 */
	EReference getMmsConcept_Property();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.mms.MmsConcept#getInstance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Instance</em>'.
	 * @see camel.mms.MmsConcept#getInstance()
	 * @see #getMmsConcept()
	 * @generated
	 */
	EReference getMmsConcept_Instance();

	/**
	 * Returns the meta object for the '{@link camel.mms.MmsConcept#contains(camel.mms.MmsConcept) <em>Contains</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Contains</em>' operation.
	 * @see camel.mms.MmsConcept#contains(camel.mms.MmsConcept)
	 * @generated
	 */
	EOperation getMmsConcept__Contains__MmsConcept();

	/**
	 * Returns the meta object for class '{@link camel.mms.MmsProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property</em>'.
	 * @see camel.mms.MmsProperty
	 * @generated
	 */
	EClass getMmsProperty();

	/**
	 * Returns the meta object for the container reference '{@link camel.mms.MmsProperty#getDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Domain</em>'.
	 * @see camel.mms.MmsProperty#getDomain()
	 * @see #getMmsProperty()
	 * @generated
	 */
	EReference getMmsProperty_Domain();

	/**
	 * Returns the meta object for the attribute '{@link camel.mms.MmsProperty#getPropertyType <em>Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Type</em>'.
	 * @see camel.mms.MmsProperty#getPropertyType()
	 * @see #getMmsProperty()
	 * @generated
	 */
	EAttribute getMmsProperty_PropertyType();

	/**
	 * Returns the meta object for the reference '{@link camel.mms.MmsProperty#getRange <em>Range</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Range</em>'.
	 * @see camel.mms.MmsProperty#getRange()
	 * @see #getMmsProperty()
	 * @generated
	 */
	EReference getMmsProperty_Range();

	/**
	 * Returns the meta object for the attribute '{@link camel.mms.MmsProperty#getRangeUri <em>Range Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Range Uri</em>'.
	 * @see camel.mms.MmsProperty#getRangeUri()
	 * @see #getMmsProperty()
	 * @generated
	 */
	EAttribute getMmsProperty_RangeUri();

	/**
	 * Returns the meta object for class '{@link camel.mms.MmsObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object</em>'.
	 * @see camel.mms.MmsObject
	 * @generated
	 */
	EClass getMmsObject();

	/**
	 * Returns the meta object for the attribute '{@link camel.mms.MmsObject#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see camel.mms.MmsObject#getId()
	 * @see #getMmsObject()
	 * @generated
	 */
	EAttribute getMmsObject_Id();

	/**
	 * Returns the meta object for the attribute '{@link camel.mms.MmsObject#getUri <em>Uri</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Uri</em>'.
	 * @see camel.mms.MmsObject#getUri()
	 * @see #getMmsObject()
	 * @generated
	 */
	EAttribute getMmsObject_Uri();

	/**
	 * Returns the meta object for the attribute '{@link camel.mms.MmsObject#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see camel.mms.MmsObject#getName()
	 * @see #getMmsObject()
	 * @generated
	 */
	EAttribute getMmsObject_Name();

	/**
	 * Returns the meta object for the attribute '{@link camel.mms.MmsObject#getDescription <em>Description</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Description</em>'.
	 * @see camel.mms.MmsObject#getDescription()
	 * @see #getMmsObject()
	 * @generated
	 */
	EAttribute getMmsObject_Description();

	/**
	 * Returns the meta object for class '{@link camel.mms.MmsConceptInstance <em>Concept Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Concept Instance</em>'.
	 * @see camel.mms.MmsConceptInstance
	 * @generated
	 */
	EClass getMmsConceptInstance();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.mms.MmsConceptInstance#getPropertyInstance <em>Property Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Property Instance</em>'.
	 * @see camel.mms.MmsConceptInstance#getPropertyInstance()
	 * @see #getMmsConceptInstance()
	 * @generated
	 */
	EReference getMmsConceptInstance_PropertyInstance();

	/**
	 * Returns the meta object for class '{@link camel.mms.MmsPropertyInstance <em>Property Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Property Instance</em>'.
	 * @see camel.mms.MmsPropertyInstance
	 * @generated
	 */
	EClass getMmsPropertyInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.mms.MmsPropertyInstance#getIsA <em>Is A</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Is A</em>'.
	 * @see camel.mms.MmsPropertyInstance#getIsA()
	 * @see #getMmsPropertyInstance()
	 * @generated
	 */
	EReference getMmsPropertyInstance_IsA();

	/**
	 * Returns the meta object for the attribute '{@link camel.mms.MmsPropertyInstance#getPropertyValue <em>Property Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Property Value</em>'.
	 * @see camel.mms.MmsPropertyInstance#getPropertyValue()
	 * @see #getMmsPropertyInstance()
	 * @generated
	 */
	EAttribute getMmsPropertyInstance_PropertyValue();

	/**
	 * Returns the meta object for the container reference '{@link camel.mms.MmsPropertyInstance#getConceptInstance <em>Concept Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Concept Instance</em>'.
	 * @see camel.mms.MmsPropertyInstance#getConceptInstance()
	 * @see #getMmsPropertyInstance()
	 * @generated
	 */
	EReference getMmsPropertyInstance_ConceptInstance();

	/**
	 * Returns the meta object for enum '{@link camel.mms.MmsPropertyType <em>Property Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Property Type</em>'.
	 * @see camel.mms.MmsPropertyType
	 * @generated
	 */
	EEnum getMmsPropertyType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MmsFactory getMmsFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link camel.mms.impl.MetaDataModelImpl <em>Meta Data Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.mms.impl.MetaDataModelImpl
		 * @see camel.mms.impl.MmsPackageImpl#getMetaDataModel()
		 * @generated
		 */
		EClass META_DATA_MODEL = eINSTANCE.getMetaDataModel();

		/**
		 * The meta object literal for the '<em><b>Metadata Elements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference META_DATA_MODEL__METADATA_ELEMENTS = eINSTANCE.getMetaDataModel_MetadataElements();

		/**
		 * The meta object literal for the '{@link camel.mms.impl.MmsConceptImpl <em>Concept</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.mms.impl.MmsConceptImpl
		 * @see camel.mms.impl.MmsPackageImpl#getMmsConcept()
		 * @generated
		 */
		EClass MMS_CONCEPT = eINSTANCE.getMmsConcept();

		/**
		 * The meta object literal for the '<em><b>Parent</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMS_CONCEPT__PARENT = eINSTANCE.getMmsConcept_Parent();

		/**
		 * The meta object literal for the '<em><b>Concept</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMS_CONCEPT__CONCEPT = eINSTANCE.getMmsConcept_Concept();

		/**
		 * The meta object literal for the '<em><b>Property</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMS_CONCEPT__PROPERTY = eINSTANCE.getMmsConcept_Property();

		/**
		 * The meta object literal for the '<em><b>Instance</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMS_CONCEPT__INSTANCE = eINSTANCE.getMmsConcept_Instance();

		/**
		 * The meta object literal for the '<em><b>Contains</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation MMS_CONCEPT___CONTAINS__MMSCONCEPT = eINSTANCE.getMmsConcept__Contains__MmsConcept();

		/**
		 * The meta object literal for the '{@link camel.mms.impl.MmsPropertyImpl <em>Property</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.mms.impl.MmsPropertyImpl
		 * @see camel.mms.impl.MmsPackageImpl#getMmsProperty()
		 * @generated
		 */
		EClass MMS_PROPERTY = eINSTANCE.getMmsProperty();

		/**
		 * The meta object literal for the '<em><b>Domain</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMS_PROPERTY__DOMAIN = eINSTANCE.getMmsProperty_Domain();

		/**
		 * The meta object literal for the '<em><b>Property Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MMS_PROPERTY__PROPERTY_TYPE = eINSTANCE.getMmsProperty_PropertyType();

		/**
		 * The meta object literal for the '<em><b>Range</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMS_PROPERTY__RANGE = eINSTANCE.getMmsProperty_Range();

		/**
		 * The meta object literal for the '<em><b>Range Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MMS_PROPERTY__RANGE_URI = eINSTANCE.getMmsProperty_RangeUri();

		/**
		 * The meta object literal for the '{@link camel.mms.impl.MmsObjectImpl <em>Object</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.mms.impl.MmsObjectImpl
		 * @see camel.mms.impl.MmsPackageImpl#getMmsObject()
		 * @generated
		 */
		EClass MMS_OBJECT = eINSTANCE.getMmsObject();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MMS_OBJECT__ID = eINSTANCE.getMmsObject_Id();

		/**
		 * The meta object literal for the '<em><b>Uri</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MMS_OBJECT__URI = eINSTANCE.getMmsObject_Uri();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MMS_OBJECT__NAME = eINSTANCE.getMmsObject_Name();

		/**
		 * The meta object literal for the '<em><b>Description</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MMS_OBJECT__DESCRIPTION = eINSTANCE.getMmsObject_Description();

		/**
		 * The meta object literal for the '{@link camel.mms.impl.MmsConceptInstanceImpl <em>Concept Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.mms.impl.MmsConceptInstanceImpl
		 * @see camel.mms.impl.MmsPackageImpl#getMmsConceptInstance()
		 * @generated
		 */
		EClass MMS_CONCEPT_INSTANCE = eINSTANCE.getMmsConceptInstance();

		/**
		 * The meta object literal for the '<em><b>Property Instance</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMS_CONCEPT_INSTANCE__PROPERTY_INSTANCE = eINSTANCE.getMmsConceptInstance_PropertyInstance();

		/**
		 * The meta object literal for the '{@link camel.mms.impl.MmsPropertyInstanceImpl <em>Property Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.mms.impl.MmsPropertyInstanceImpl
		 * @see camel.mms.impl.MmsPackageImpl#getMmsPropertyInstance()
		 * @generated
		 */
		EClass MMS_PROPERTY_INSTANCE = eINSTANCE.getMmsPropertyInstance();

		/**
		 * The meta object literal for the '<em><b>Is A</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMS_PROPERTY_INSTANCE__IS_A = eINSTANCE.getMmsPropertyInstance_IsA();

		/**
		 * The meta object literal for the '<em><b>Property Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MMS_PROPERTY_INSTANCE__PROPERTY_VALUE = eINSTANCE.getMmsPropertyInstance_PropertyValue();

		/**
		 * The meta object literal for the '<em><b>Concept Instance</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MMS_PROPERTY_INSTANCE__CONCEPT_INSTANCE = eINSTANCE.getMmsPropertyInstance_ConceptInstance();

		/**
		 * The meta object literal for the '{@link camel.mms.MmsPropertyType <em>Property Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.mms.MmsPropertyType
		 * @see camel.mms.impl.MmsPackageImpl#getMmsPropertyType()
		 * @generated
		 */
		EEnum MMS_PROPERTY_TYPE = eINSTANCE.getMmsPropertyType();

	}

} //MmsPackage
