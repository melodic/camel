/**
 */
package camel.mms;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.mms.MmsProperty#getDomain <em>Domain</em>}</li>
 *   <li>{@link camel.mms.MmsProperty#getPropertyType <em>Property Type</em>}</li>
 *   <li>{@link camel.mms.MmsProperty#getRange <em>Range</em>}</li>
 *   <li>{@link camel.mms.MmsProperty#getRangeUri <em>Range Uri</em>}</li>
 * </ul>
 *
 * @see camel.mms.MmsPackage#getMmsProperty()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='object_property_has_range'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot object_property_has_range='Tuple {\n\tmessage : String = \'Object MmsProperty: \' + self.name +\n\t\t\t\t\' should have an object/concept as range\',\n\tstatus : Boolean = propertyType = MmsPropertyType::DataProperty xor range &lt;&gt; null\n}.status'"
 * @generated
 */
public interface MmsProperty extends MmsObject {
	/**
	 * Returns the value of the '<em><b>Domain</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link camel.mms.MmsConcept#getProperty <em>Property</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain</em>' container reference.
	 * @see #setDomain(MmsConcept)
	 * @see camel.mms.MmsPackage#getMmsProperty_Domain()
	 * @see camel.mms.MmsConcept#getProperty
	 * @model opposite="property" required="true" transient="false"
	 * @generated
	 */
	MmsConcept getDomain();

	/**
	 * Sets the value of the '{@link camel.mms.MmsProperty#getDomain <em>Domain</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain</em>' container reference.
	 * @see #getDomain()
	 * @generated
	 */
	void setDomain(MmsConcept value);

	/**
	 * Returns the value of the '<em><b>Property Type</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.mms.MmsPropertyType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Type</em>' attribute.
	 * @see camel.mms.MmsPropertyType
	 * @see #setPropertyType(MmsPropertyType)
	 * @see camel.mms.MmsPackage#getMmsProperty_PropertyType()
	 * @model required="true"
	 * @generated
	 */
	MmsPropertyType getPropertyType();

	/**
	 * Sets the value of the '{@link camel.mms.MmsProperty#getPropertyType <em>Property Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Type</em>' attribute.
	 * @see camel.mms.MmsPropertyType
	 * @see #getPropertyType()
	 * @generated
	 */
	void setPropertyType(MmsPropertyType value);

	/**
	 * Returns the value of the '<em><b>Range</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range</em>' reference.
	 * @see #setRange(MmsConcept)
	 * @see camel.mms.MmsPackage#getMmsProperty_Range()
	 * @model
	 * @generated
	 */
	MmsConcept getRange();

	/**
	 * Sets the value of the '{@link camel.mms.MmsProperty#getRange <em>Range</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range</em>' reference.
	 * @see #getRange()
	 * @generated
	 */
	void setRange(MmsConcept value);

	/**
	 * Returns the value of the '<em><b>Range Uri</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Range Uri</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Range Uri</em>' attribute.
	 * @see #setRangeUri(String)
	 * @see camel.mms.MmsPackage#getMmsProperty_RangeUri()
	 * @model
	 * @generated
	 */
	String getRangeUri();

	/**
	 * Sets the value of the '{@link camel.mms.MmsProperty#getRangeUri <em>Range Uri</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Range Uri</em>' attribute.
	 * @see #getRangeUri()
	 * @generated
	 */
	void setRangeUri(String value);

} // MmsProperty
