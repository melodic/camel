/**
 */
package camel.mms.impl;

import camel.constraint.ConstraintPackage;

import camel.constraint.impl.ConstraintPackageImpl;

import camel.core.CorePackage;

import camel.core.impl.CorePackageImpl;

import camel.data.DataPackage;

import camel.data.impl.DataPackageImpl;

import camel.deployment.DeploymentPackage;

import camel.deployment.impl.DeploymentPackageImpl;

import camel.execution.ExecutionPackage;

import camel.execution.impl.ExecutionPackageImpl;

import camel.location.LocationPackage;

import camel.location.impl.LocationPackageImpl;

import camel.metric.MetricPackage;

import camel.metric.impl.MetricPackageImpl;

import camel.mms.MetaDataModel;
import camel.mms.MmsConcept;
import camel.mms.MmsConceptInstance;
import camel.mms.MmsFactory;
import camel.mms.MmsObject;
import camel.mms.MmsPackage;
import camel.mms.MmsProperty;
import camel.mms.MmsPropertyInstance;
import camel.mms.MmsPropertyType;

import camel.mms.util.MmsValidator;
import camel.organisation.OrganisationPackage;

import camel.organisation.impl.OrganisationPackageImpl;

import camel.requirement.RequirementPackage;

import camel.requirement.impl.RequirementPackageImpl;

import camel.scalability.ScalabilityPackage;

import camel.scalability.impl.ScalabilityPackageImpl;

import camel.security.SecurityPackage;

import camel.security.impl.SecurityPackageImpl;

import camel.type.TypePackage;

import camel.type.impl.TypePackageImpl;

import camel.unit.UnitPackage;

import camel.unit.impl.UnitPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MmsPackageImpl extends EPackageImpl implements MmsPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metaDataModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mmsConceptEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mmsPropertyEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mmsObjectEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mmsConceptInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass mmsPropertyInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum mmsPropertyTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see camel.mms.MmsPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MmsPackageImpl() {
		super(eNS_URI, MmsFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MmsPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MmsPackage init() {
		if (isInited) return (MmsPackage)EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI);

		// Obtain or create and register package
		MmsPackageImpl theMmsPackage = (MmsPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MmsPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MmsPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		DeploymentPackageImpl theDeploymentPackage = (DeploymentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI) instanceof DeploymentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI) : DeploymentPackage.eINSTANCE);
		ExecutionPackageImpl theExecutionPackage = (ExecutionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI) instanceof ExecutionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI) : ExecutionPackage.eINSTANCE);
		LocationPackageImpl theLocationPackage = (LocationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) instanceof LocationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) : LocationPackage.eINSTANCE);
		MetricPackageImpl theMetricPackage = (MetricPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI) instanceof MetricPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI) : MetricPackage.eINSTANCE);
		OrganisationPackageImpl theOrganisationPackage = (OrganisationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) instanceof OrganisationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) : OrganisationPackage.eINSTANCE);
		RequirementPackageImpl theRequirementPackage = (RequirementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) instanceof RequirementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) : RequirementPackage.eINSTANCE);
		ScalabilityPackageImpl theScalabilityPackage = (ScalabilityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI) instanceof ScalabilityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI) : ScalabilityPackage.eINSTANCE);
		SecurityPackageImpl theSecurityPackage = (SecurityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) instanceof SecurityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) : SecurityPackage.eINSTANCE);
		TypePackageImpl theTypePackage = (TypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI) instanceof TypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI) : TypePackage.eINSTANCE);
		UnitPackageImpl theUnitPackage = (UnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) instanceof UnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) : UnitPackage.eINSTANCE);
		ConstraintPackageImpl theConstraintPackage = (ConstraintPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) instanceof ConstraintPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) : ConstraintPackage.eINSTANCE);
		DataPackageImpl theDataPackage = (DataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) instanceof DataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) : DataPackage.eINSTANCE);

		// Create package meta-data objects
		theMmsPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theDeploymentPackage.createPackageContents();
		theExecutionPackage.createPackageContents();
		theLocationPackage.createPackageContents();
		theMetricPackage.createPackageContents();
		theOrganisationPackage.createPackageContents();
		theRequirementPackage.createPackageContents();
		theScalabilityPackage.createPackageContents();
		theSecurityPackage.createPackageContents();
		theTypePackage.createPackageContents();
		theUnitPackage.createPackageContents();
		theConstraintPackage.createPackageContents();
		theDataPackage.createPackageContents();

		// Initialize created meta-data
		theMmsPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theDeploymentPackage.initializePackageContents();
		theExecutionPackage.initializePackageContents();
		theLocationPackage.initializePackageContents();
		theMetricPackage.initializePackageContents();
		theOrganisationPackage.initializePackageContents();
		theRequirementPackage.initializePackageContents();
		theScalabilityPackage.initializePackageContents();
		theSecurityPackage.initializePackageContents();
		theTypePackage.initializePackageContents();
		theUnitPackage.initializePackageContents();
		theConstraintPackage.initializePackageContents();
		theDataPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theMmsPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return MmsValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theMmsPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MmsPackage.eNS_URI, theMmsPackage);
		return theMmsPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetaDataModel() {
		return metaDataModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetaDataModel_MetadataElements() {
		return (EReference)metaDataModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMmsConcept() {
		return mmsConceptEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMmsConcept_Parent() {
		return (EReference)mmsConceptEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMmsConcept_Concept() {
		return (EReference)mmsConceptEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMmsConcept_Property() {
		return (EReference)mmsConceptEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMmsConcept_Instance() {
		return (EReference)mmsConceptEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMmsConcept__Contains__MmsConcept() {
		return mmsConceptEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMmsProperty() {
		return mmsPropertyEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMmsProperty_Domain() {
		return (EReference)mmsPropertyEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMmsProperty_PropertyType() {
		return (EAttribute)mmsPropertyEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMmsProperty_Range() {
		return (EReference)mmsPropertyEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMmsProperty_RangeUri() {
		return (EAttribute)mmsPropertyEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMmsObject() {
		return mmsObjectEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMmsObject_Id() {
		return (EAttribute)mmsObjectEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMmsObject_Uri() {
		return (EAttribute)mmsObjectEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMmsObject_Name() {
		return (EAttribute)mmsObjectEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMmsObject_Description() {
		return (EAttribute)mmsObjectEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMmsConceptInstance() {
		return mmsConceptInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMmsConceptInstance_PropertyInstance() {
		return (EReference)mmsConceptInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMmsPropertyInstance() {
		return mmsPropertyInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMmsPropertyInstance_IsA() {
		return (EReference)mmsPropertyInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMmsPropertyInstance_PropertyValue() {
		return (EAttribute)mmsPropertyInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMmsPropertyInstance_ConceptInstance() {
		return (EReference)mmsPropertyInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getMmsPropertyType() {
		return mmsPropertyTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsFactory getMmsFactory() {
		return (MmsFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		metaDataModelEClass = createEClass(META_DATA_MODEL);
		createEReference(metaDataModelEClass, META_DATA_MODEL__METADATA_ELEMENTS);

		mmsConceptEClass = createEClass(MMS_CONCEPT);
		createEReference(mmsConceptEClass, MMS_CONCEPT__PARENT);
		createEReference(mmsConceptEClass, MMS_CONCEPT__CONCEPT);
		createEReference(mmsConceptEClass, MMS_CONCEPT__PROPERTY);
		createEReference(mmsConceptEClass, MMS_CONCEPT__INSTANCE);
		createEOperation(mmsConceptEClass, MMS_CONCEPT___CONTAINS__MMSCONCEPT);

		mmsPropertyEClass = createEClass(MMS_PROPERTY);
		createEReference(mmsPropertyEClass, MMS_PROPERTY__DOMAIN);
		createEAttribute(mmsPropertyEClass, MMS_PROPERTY__PROPERTY_TYPE);
		createEReference(mmsPropertyEClass, MMS_PROPERTY__RANGE);
		createEAttribute(mmsPropertyEClass, MMS_PROPERTY__RANGE_URI);

		mmsObjectEClass = createEClass(MMS_OBJECT);
		createEAttribute(mmsObjectEClass, MMS_OBJECT__ID);
		createEAttribute(mmsObjectEClass, MMS_OBJECT__URI);
		createEAttribute(mmsObjectEClass, MMS_OBJECT__NAME);
		createEAttribute(mmsObjectEClass, MMS_OBJECT__DESCRIPTION);

		mmsConceptInstanceEClass = createEClass(MMS_CONCEPT_INSTANCE);
		createEReference(mmsConceptInstanceEClass, MMS_CONCEPT_INSTANCE__PROPERTY_INSTANCE);

		mmsPropertyInstanceEClass = createEClass(MMS_PROPERTY_INSTANCE);
		createEReference(mmsPropertyInstanceEClass, MMS_PROPERTY_INSTANCE__IS_A);
		createEAttribute(mmsPropertyInstanceEClass, MMS_PROPERTY_INSTANCE__PROPERTY_VALUE);
		createEReference(mmsPropertyInstanceEClass, MMS_PROPERTY_INSTANCE__CONCEPT_INSTANCE);

		// Create enums
		mmsPropertyTypeEEnum = createEEnum(MMS_PROPERTY_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		metaDataModelEClass.getESuperTypes().add(theCorePackage.getModel());
		mmsConceptEClass.getESuperTypes().add(this.getMmsObject());
		mmsPropertyEClass.getESuperTypes().add(this.getMmsObject());
		mmsConceptInstanceEClass.getESuperTypes().add(this.getMmsObject());
		mmsPropertyInstanceEClass.getESuperTypes().add(this.getMmsObject());

		// Initialize classes, features, and operations; add parameters
		initEClass(metaDataModelEClass, MetaDataModel.class, "MetaDataModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMetaDataModel_MetadataElements(), this.getMmsObject(), null, "metadataElements", null, 0, -1, MetaDataModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mmsConceptEClass, MmsConcept.class, "MmsConcept", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMmsConcept_Parent(), this.getMmsConcept(), this.getMmsConcept_Concept(), "parent", null, 0, 1, MmsConcept.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMmsConcept_Concept(), this.getMmsConcept(), this.getMmsConcept_Parent(), "concept", null, 0, -1, MmsConcept.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMmsConcept_Property(), this.getMmsProperty(), this.getMmsProperty_Domain(), "property", null, 0, -1, MmsConcept.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMmsConcept_Instance(), this.getMmsConceptInstance(), null, "instance", null, 0, -1, MmsConcept.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getMmsConcept__Contains__MmsConcept(), ecorePackage.getEBoolean(), "contains", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMmsConcept(), "conc", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(mmsPropertyEClass, MmsProperty.class, "MmsProperty", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMmsProperty_Domain(), this.getMmsConcept(), this.getMmsConcept_Property(), "domain", null, 1, 1, MmsProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMmsProperty_PropertyType(), this.getMmsPropertyType(), "propertyType", null, 1, 1, MmsProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMmsProperty_Range(), this.getMmsConcept(), null, "range", null, 0, 1, MmsProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMmsProperty_RangeUri(), ecorePackage.getEString(), "rangeUri", null, 0, 1, MmsProperty.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mmsObjectEClass, MmsObject.class, "MmsObject", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMmsObject_Id(), ecorePackage.getEString(), "id", null, 1, 1, MmsObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMmsObject_Uri(), ecorePackage.getEString(), "uri", null, 1, 1, MmsObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMmsObject_Name(), ecorePackage.getEString(), "name", null, 1, 1, MmsObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMmsObject_Description(), ecorePackage.getEString(), "description", null, 0, 1, MmsObject.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mmsConceptInstanceEClass, MmsConceptInstance.class, "MmsConceptInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMmsConceptInstance_PropertyInstance(), this.getMmsPropertyInstance(), this.getMmsPropertyInstance_ConceptInstance(), "propertyInstance", null, 0, -1, MmsConceptInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(mmsPropertyInstanceEClass, MmsPropertyInstance.class, "MmsPropertyInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMmsPropertyInstance_IsA(), this.getMmsProperty(), null, "isA", null, 1, 1, MmsPropertyInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMmsPropertyInstance_PropertyValue(), ecorePackage.getEString(), "propertyValue", null, 1, 1, MmsPropertyInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMmsPropertyInstance_ConceptInstance(), this.getMmsConceptInstance(), this.getMmsConceptInstance_PropertyInstance(), "conceptInstance", null, 1, 1, MmsPropertyInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(mmsPropertyTypeEEnum, MmsPropertyType.class, "MmsPropertyType");
		addEEnumLiteral(mmsPropertyTypeEEnum, MmsPropertyType.DATA_PROPERTY);
		addEEnumLiteral(mmsPropertyTypeEEnum, MmsPropertyType.OBJECT_PROPERTY);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
		// teneo.jpa
		createTeneoAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
		   });	
		addAnnotation
		  (mmsConceptEClass, 
		   source, 
		   new String[] {
			 "constraints", "concept_recursively_containts_itself"
		   });	
		addAnnotation
		  (mmsPropertyEClass, 
		   source, 
		   new String[] {
			 "constraints", "object_property_has_range"
		   });	
		addAnnotation
		  (mmsPropertyInstanceEClass, 
		   source, 
		   new String[] {
			 "constraints", "correct_property_instance"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";	
		addAnnotation
		  (mmsConceptEClass, 
		   source, 
		   new String[] {
			 "concept_recursively_containts_itself", "Tuple {\n\tmessage : String = \'MmsConcept: \' + self.name +\n\t\t\t\t\' should not recursively contain itself\',\n\tstatus : Boolean = not self.contains(self)\n}.status"
		   });	
		addAnnotation
		  (getMmsConcept__Contains__MmsConcept(), 
		   source, 
		   new String[] {
			 "body", "self.concept\n\t\t\t\t\t->exists(p | p.name = conc.name or p.contains(conc))"
		   });	
		addAnnotation
		  (mmsPropertyEClass, 
		   source, 
		   new String[] {
			 "object_property_has_range", "Tuple {\n\tmessage : String = \'Object MmsProperty: \' + self.name +\n\t\t\t\t\' should have an object/concept as range\',\n\tstatus : Boolean = propertyType = MmsPropertyType::DataProperty xor range <> null\n}.status"
		   });	
		addAnnotation
		  (mmsPropertyInstanceEClass, 
		   source, 
		   new String[] {
			 "correct_property_instance", "Tuple {\n\tmessage : String = \'The type of the domain of MsmPropertyInstance: \' + self.name + \'\n\t\t\tshould correspond to the domain of it\\\'s type\',\n\tstatus : Boolean = self.conceptInstance.oclContainer().oclAsType(camel::mms::MmsConcept) = self.isA.domain\n}.status"
		   });
	}

	/**
	 * Initializes the annotations for <b>teneo.jpa</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createTeneoAnnotations() {
		String source = "teneo.jpa";	
		addAnnotation
		  (getMmsObject_Description(), 
		   source, 
		   new String[] {
			 "value", "@Column(length=4000)"
		   });
	}

} //MmsPackageImpl
