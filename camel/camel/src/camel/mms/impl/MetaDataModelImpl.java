/**
 */
package camel.mms.impl;

import camel.core.impl.ModelImpl;

import camel.mms.MetaDataModel;
import camel.mms.MmsObject;
import camel.mms.MmsPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Meta Data Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.mms.impl.MetaDataModelImpl#getMetadataElements <em>Metadata Elements</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetaDataModelImpl extends ModelImpl implements MetaDataModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetaDataModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MmsPackage.Literals.META_DATA_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MmsObject> getMetadataElements() {
		return (EList<MmsObject>)eGet(MmsPackage.Literals.META_DATA_MODEL__METADATA_ELEMENTS, true);
	}

} //MetaDataModelImpl
