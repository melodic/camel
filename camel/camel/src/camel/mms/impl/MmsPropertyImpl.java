/**
 */
package camel.mms.impl;

import camel.mms.MmsConcept;
import camel.mms.MmsPackage;
import camel.mms.MmsProperty;
import camel.mms.MmsPropertyType;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.mms.impl.MmsPropertyImpl#getDomain <em>Domain</em>}</li>
 *   <li>{@link camel.mms.impl.MmsPropertyImpl#getPropertyType <em>Property Type</em>}</li>
 *   <li>{@link camel.mms.impl.MmsPropertyImpl#getRange <em>Range</em>}</li>
 *   <li>{@link camel.mms.impl.MmsPropertyImpl#getRangeUri <em>Range Uri</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MmsPropertyImpl extends MmsObjectImpl implements MmsProperty {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MmsPropertyImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MmsPackage.Literals.MMS_PROPERTY;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsConcept getDomain() {
		return (MmsConcept)eGet(MmsPackage.Literals.MMS_PROPERTY__DOMAIN, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomain(MmsConcept newDomain) {
		eSet(MmsPackage.Literals.MMS_PROPERTY__DOMAIN, newDomain);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsPropertyType getPropertyType() {
		return (MmsPropertyType)eGet(MmsPackage.Literals.MMS_PROPERTY__PROPERTY_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyType(MmsPropertyType newPropertyType) {
		eSet(MmsPackage.Literals.MMS_PROPERTY__PROPERTY_TYPE, newPropertyType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsConcept getRange() {
		return (MmsConcept)eGet(MmsPackage.Literals.MMS_PROPERTY__RANGE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRange(MmsConcept newRange) {
		eSet(MmsPackage.Literals.MMS_PROPERTY__RANGE, newRange);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getRangeUri() {
		return (String)eGet(MmsPackage.Literals.MMS_PROPERTY__RANGE_URI, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRangeUri(String newRangeUri) {
		eSet(MmsPackage.Literals.MMS_PROPERTY__RANGE_URI, newRangeUri);
	}

} //MmsPropertyImpl
