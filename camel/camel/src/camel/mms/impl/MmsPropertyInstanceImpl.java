/**
 */
package camel.mms.impl;

import camel.mms.MmsConceptInstance;
import camel.mms.MmsPackage;
import camel.mms.MmsProperty;
import camel.mms.MmsPropertyInstance;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Property Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.mms.impl.MmsPropertyInstanceImpl#getIsA <em>Is A</em>}</li>
 *   <li>{@link camel.mms.impl.MmsPropertyInstanceImpl#getPropertyValue <em>Property Value</em>}</li>
 *   <li>{@link camel.mms.impl.MmsPropertyInstanceImpl#getConceptInstance <em>Concept Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MmsPropertyInstanceImpl extends MmsObjectImpl implements MmsPropertyInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MmsPropertyInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MmsPackage.Literals.MMS_PROPERTY_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsProperty getIsA() {
		return (MmsProperty)eGet(MmsPackage.Literals.MMS_PROPERTY_INSTANCE__IS_A, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsA(MmsProperty newIsA) {
		eSet(MmsPackage.Literals.MMS_PROPERTY_INSTANCE__IS_A, newIsA);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPropertyValue() {
		return (String)eGet(MmsPackage.Literals.MMS_PROPERTY_INSTANCE__PROPERTY_VALUE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPropertyValue(String newPropertyValue) {
		eSet(MmsPackage.Literals.MMS_PROPERTY_INSTANCE__PROPERTY_VALUE, newPropertyValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsConceptInstance getConceptInstance() {
		return (MmsConceptInstance)eGet(MmsPackage.Literals.MMS_PROPERTY_INSTANCE__CONCEPT_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConceptInstance(MmsConceptInstance newConceptInstance) {
		eSet(MmsPackage.Literals.MMS_PROPERTY_INSTANCE__CONCEPT_INSTANCE, newConceptInstance);
	}

} //MmsPropertyInstanceImpl
