/**
 */
package camel.mms.impl;

import camel.mms.MmsConcept;
import camel.mms.MmsConceptInstance;
import camel.mms.MmsPackage;
import camel.mms.MmsProperty;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concept</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.mms.impl.MmsConceptImpl#getParent <em>Parent</em>}</li>
 *   <li>{@link camel.mms.impl.MmsConceptImpl#getConcept <em>Concept</em>}</li>
 *   <li>{@link camel.mms.impl.MmsConceptImpl#getProperty <em>Property</em>}</li>
 *   <li>{@link camel.mms.impl.MmsConceptImpl#getInstance <em>Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MmsConceptImpl extends MmsObjectImpl implements MmsConcept {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MmsConceptImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MmsPackage.Literals.MMS_CONCEPT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsConcept getParent() {
		return (MmsConcept)eGet(MmsPackage.Literals.MMS_CONCEPT__PARENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setParent(MmsConcept newParent) {
		eSet(MmsPackage.Literals.MMS_CONCEPT__PARENT, newParent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MmsConcept> getConcept() {
		return (EList<MmsConcept>)eGet(MmsPackage.Literals.MMS_CONCEPT__CONCEPT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MmsProperty> getProperty() {
		return (EList<MmsProperty>)eGet(MmsPackage.Literals.MMS_CONCEPT__PROPERTY, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MmsConceptInstance> getInstance() {
		return (EList<MmsConceptInstance>)eGet(MmsPackage.Literals.MMS_CONCEPT__INSTANCE, true);
	}

	/**
	 * The cached invocation delegate for the '{@link #contains(camel.mms.MmsConcept) <em>Contains</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #contains(camel.mms.MmsConcept)
	 * @generated
	 * @ordered
	 */
	protected static final EOperation.Internal.InvocationDelegate CONTAINS_MMS_CONCEPT__EINVOCATION_DELEGATE = ((EOperation.Internal)MmsPackage.Literals.MMS_CONCEPT___CONTAINS__MMSCONCEPT).getInvocationDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean contains(MmsConcept conc) {
		try {
			return (Boolean)CONTAINS_MMS_CONCEPT__EINVOCATION_DELEGATE.dynamicInvoke(this, new BasicEList.UnmodifiableEList<Object>(1, new Object[]{conc}));
		}
		catch (InvocationTargetException ite) {
			throw new WrappedException(ite);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MmsPackage.MMS_CONCEPT___CONTAINS__MMSCONCEPT:
				return contains((MmsConcept)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //MmsConceptImpl
