/**
 */
package camel.mms.impl;

import camel.mms.MmsConceptInstance;
import camel.mms.MmsPackage;
import camel.mms.MmsPropertyInstance;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Concept Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.mms.impl.MmsConceptInstanceImpl#getPropertyInstance <em>Property Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MmsConceptInstanceImpl extends MmsObjectImpl implements MmsConceptInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MmsConceptInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MmsPackage.Literals.MMS_CONCEPT_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MmsPropertyInstance> getPropertyInstance() {
		return (EList<MmsPropertyInstance>)eGet(MmsPackage.Literals.MMS_CONCEPT_INSTANCE__PROPERTY_INSTANCE, true);
	}

} //MmsConceptInstanceImpl
