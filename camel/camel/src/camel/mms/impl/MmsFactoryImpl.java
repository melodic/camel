/**
 */
package camel.mms.impl;

import camel.mms.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MmsFactoryImpl extends EFactoryImpl implements MmsFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MmsFactory init() {
		try {
			MmsFactory theMmsFactory = (MmsFactory)EPackage.Registry.INSTANCE.getEFactory(MmsPackage.eNS_URI);
			if (theMmsFactory != null) {
				return theMmsFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MmsFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MmsPackage.META_DATA_MODEL: return (EObject)createMetaDataModel();
			case MmsPackage.MMS_CONCEPT: return (EObject)createMmsConcept();
			case MmsPackage.MMS_PROPERTY: return (EObject)createMmsProperty();
			case MmsPackage.MMS_CONCEPT_INSTANCE: return (EObject)createMmsConceptInstance();
			case MmsPackage.MMS_PROPERTY_INSTANCE: return (EObject)createMmsPropertyInstance();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MmsPackage.MMS_PROPERTY_TYPE:
				return createMmsPropertyTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MmsPackage.MMS_PROPERTY_TYPE:
				return convertMmsPropertyTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetaDataModel createMetaDataModel() {
		MetaDataModelImpl metaDataModel = new MetaDataModelImpl();
		return metaDataModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsConcept createMmsConcept() {
		MmsConceptImpl mmsConcept = new MmsConceptImpl();
		return mmsConcept;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsProperty createMmsProperty() {
		MmsPropertyImpl mmsProperty = new MmsPropertyImpl();
		return mmsProperty;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsConceptInstance createMmsConceptInstance() {
		MmsConceptInstanceImpl mmsConceptInstance = new MmsConceptInstanceImpl();
		return mmsConceptInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsPropertyInstance createMmsPropertyInstance() {
		MmsPropertyInstanceImpl mmsPropertyInstance = new MmsPropertyInstanceImpl();
		return mmsPropertyInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsPropertyType createMmsPropertyTypeFromString(EDataType eDataType, String initialValue) {
		MmsPropertyType result = MmsPropertyType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertMmsPropertyTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsPackage getMmsPackage() {
		return (MmsPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MmsPackage getPackage() {
		return MmsPackage.eINSTANCE;
	}

} //MmsFactoryImpl
