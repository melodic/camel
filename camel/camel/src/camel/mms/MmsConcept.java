/**
 */
package camel.mms;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Concept</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.mms.MmsConcept#getParent <em>Parent</em>}</li>
 *   <li>{@link camel.mms.MmsConcept#getConcept <em>Concept</em>}</li>
 *   <li>{@link camel.mms.MmsConcept#getProperty <em>Property</em>}</li>
 *   <li>{@link camel.mms.MmsConcept#getInstance <em>Instance</em>}</li>
 * </ul>
 *
 * @see camel.mms.MmsPackage#getMmsConcept()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='concept_recursively_containts_itself'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot concept_recursively_containts_itself='Tuple {\n\tmessage : String = \'MmsConcept: \' + self.name +\n\t\t\t\t\' should not recursively contain itself\',\n\tstatus : Boolean = not self.contains(self)\n}.status'"
 * @generated
 */
public interface MmsConcept extends MmsObject {
	/**
	 * Returns the value of the '<em><b>Parent</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link camel.mms.MmsConcept#getConcept <em>Concept</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Parent</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Parent</em>' container reference.
	 * @see #setParent(MmsConcept)
	 * @see camel.mms.MmsPackage#getMmsConcept_Parent()
	 * @see camel.mms.MmsConcept#getConcept
	 * @model opposite="concept" transient="false"
	 * @generated
	 */
	MmsConcept getParent();

	/**
	 * Sets the value of the '{@link camel.mms.MmsConcept#getParent <em>Parent</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Parent</em>' container reference.
	 * @see #getParent()
	 * @generated
	 */
	void setParent(MmsConcept value);

	/**
	 * Returns the value of the '<em><b>Concept</b></em>' containment reference list.
	 * The list contents are of type {@link camel.mms.MmsConcept}.
	 * It is bidirectional and its opposite is '{@link camel.mms.MmsConcept#getParent <em>Parent</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concept</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concept</em>' containment reference list.
	 * @see camel.mms.MmsPackage#getMmsConcept_Concept()
	 * @see camel.mms.MmsConcept#getParent
	 * @model opposite="parent" containment="true"
	 * @generated
	 */
	EList<MmsConcept> getConcept();

	/**
	 * Returns the value of the '<em><b>Property</b></em>' containment reference list.
	 * The list contents are of type {@link camel.mms.MmsProperty}.
	 * It is bidirectional and its opposite is '{@link camel.mms.MmsProperty#getDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property</em>' containment reference list.
	 * @see camel.mms.MmsPackage#getMmsConcept_Property()
	 * @see camel.mms.MmsProperty#getDomain
	 * @model opposite="domain" containment="true"
	 * @generated
	 */
	EList<MmsProperty> getProperty();

	/**
	 * Returns the value of the '<em><b>Instance</b></em>' containment reference list.
	 * The list contents are of type {@link camel.mms.MmsConceptInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Instance</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Instance</em>' containment reference list.
	 * @see camel.mms.MmsPackage#getMmsConcept_Instance()
	 * @model containment="true"
	 * @generated
	 */
	EList<MmsConceptInstance> getInstance();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" concRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='self.concept\n\t\t\t\t\t-&gt;exists(p | p.name = conc.name or p.contains(conc))'"
	 * @generated
	 */
	boolean contains(MmsConcept conc);

} // MmsConcept
