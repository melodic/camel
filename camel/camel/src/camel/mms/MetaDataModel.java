/**
 */
package camel.mms;

import camel.core.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Meta Data Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.mms.MetaDataModel#getMetadataElements <em>Metadata Elements</em>}</li>
 * </ul>
 *
 * @see camel.mms.MmsPackage#getMetaDataModel()
 * @model
 * @generated
 */
public interface MetaDataModel extends Model {
	/**
	 * Returns the value of the '<em><b>Metadata Elements</b></em>' containment reference list.
	 * The list contents are of type {@link camel.mms.MmsObject}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metadata Elements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metadata Elements</em>' containment reference list.
	 * @see camel.mms.MmsPackage#getMetaDataModel_MetadataElements()
	 * @model containment="true"
	 * @generated
	 */
	EList<MmsObject> getMetadataElements();

} // MetaDataModel
