/**
 */
package camel.mms.util;

import camel.mms.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.mms.MmsPackage
 * @generated
 */
public class MmsValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final MmsValidator INSTANCE = new MmsValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.mms";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return MmsPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case MmsPackage.META_DATA_MODEL:
				return validateMetaDataModel((MetaDataModel)value, diagnostics, context);
			case MmsPackage.MMS_CONCEPT:
				return validateMmsConcept((MmsConcept)value, diagnostics, context);
			case MmsPackage.MMS_PROPERTY:
				return validateMmsProperty((MmsProperty)value, diagnostics, context);
			case MmsPackage.MMS_OBJECT:
				return validateMmsObject((MmsObject)value, diagnostics, context);
			case MmsPackage.MMS_CONCEPT_INSTANCE:
				return validateMmsConceptInstance((MmsConceptInstance)value, diagnostics, context);
			case MmsPackage.MMS_PROPERTY_INSTANCE:
				return validateMmsPropertyInstance((MmsPropertyInstance)value, diagnostics, context);
			case MmsPackage.MMS_PROPERTY_TYPE:
				return validateMmsPropertyType((MmsPropertyType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetaDataModel(MetaDataModel metaDataModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)metaDataModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMmsConcept(MmsConcept mmsConcept, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)mmsConcept, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)mmsConcept, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)mmsConcept, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)mmsConcept, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)mmsConcept, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)mmsConcept, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)mmsConcept, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)mmsConcept, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)mmsConcept, diagnostics, context);
		if (result || diagnostics != null) result &= validateMmsConcept_concept_recursively_containts_itself(mmsConcept, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the concept_recursively_containts_itself constraint of '<em>Concept</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MMS_CONCEPT__CONCEPT_RECURSIVELY_CONTAINTS_ITSELF__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'MmsConcept: ' + self.name +\n" +
		"\t\t\t\t' should not recursively contain itself',\n" +
		"\tstatus : Boolean = not self.contains(self)\n" +
		"}.status";

	/**
	 * Validates the concept_recursively_containts_itself constraint of '<em>Concept</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMmsConcept_concept_recursively_containts_itself(MmsConcept mmsConcept, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MmsPackage.Literals.MMS_CONCEPT,
				 (EObject)mmsConcept,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "concept_recursively_containts_itself",
				 MMS_CONCEPT__CONCEPT_RECURSIVELY_CONTAINTS_ITSELF__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMmsProperty(MmsProperty mmsProperty, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)mmsProperty, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)mmsProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)mmsProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)mmsProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)mmsProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)mmsProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)mmsProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)mmsProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)mmsProperty, diagnostics, context);
		if (result || diagnostics != null) result &= validateMmsProperty_object_property_has_range(mmsProperty, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the object_property_has_range constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MMS_PROPERTY__OBJECT_PROPERTY_HAS_RANGE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Object MmsProperty: ' + self.name +\n" +
		"\t\t\t\t' should have an object/concept as range',\n" +
		"\tstatus : Boolean = propertyType = MmsPropertyType::DataProperty xor range <> null\n" +
		"}.status";

	/**
	 * Validates the object_property_has_range constraint of '<em>Property</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMmsProperty_object_property_has_range(MmsProperty mmsProperty, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MmsPackage.Literals.MMS_PROPERTY,
				 (EObject)mmsProperty,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "object_property_has_range",
				 MMS_PROPERTY__OBJECT_PROPERTY_HAS_RANGE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMmsObject(MmsObject mmsObject, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)mmsObject, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMmsConceptInstance(MmsConceptInstance mmsConceptInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)mmsConceptInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMmsPropertyInstance(MmsPropertyInstance mmsPropertyInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)mmsPropertyInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)mmsPropertyInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)mmsPropertyInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)mmsPropertyInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)mmsPropertyInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)mmsPropertyInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)mmsPropertyInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)mmsPropertyInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)mmsPropertyInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateMmsPropertyInstance_correct_property_instance(mmsPropertyInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the correct_property_instance constraint of '<em>Property Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MMS_PROPERTY_INSTANCE__CORRECT_PROPERTY_INSTANCE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'The type of the domain of MsmPropertyInstance: ' + self.name + '\n" +
		"\t\t\tshould correspond to the domain of it\\'s type',\n" +
		"\tstatus : Boolean = self.conceptInstance.oclContainer().oclAsType(camel::mms::MmsConcept) = self.isA.domain\n" +
		"}.status";

	/**
	 * Validates the correct_property_instance constraint of '<em>Property Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMmsPropertyInstance_correct_property_instance(MmsPropertyInstance mmsPropertyInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MmsPackage.Literals.MMS_PROPERTY_INSTANCE,
				 (EObject)mmsPropertyInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_property_instance",
				 MMS_PROPERTY_INSTANCE__CORRECT_PROPERTY_INSTANCE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMmsPropertyType(MmsPropertyType mmsPropertyType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //MmsValidator
