/**
 */
package camel.mms;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Property Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.mms.MmsPropertyInstance#getIsA <em>Is A</em>}</li>
 *   <li>{@link camel.mms.MmsPropertyInstance#getPropertyValue <em>Property Value</em>}</li>
 *   <li>{@link camel.mms.MmsPropertyInstance#getConceptInstance <em>Concept Instance</em>}</li>
 * </ul>
 *
 * @see camel.mms.MmsPackage#getMmsPropertyInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correct_property_instance'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot correct_property_instance='Tuple {\n\tmessage : String = \'The type of the domain of MsmPropertyInstance: \' + self.name + \'\n\t\t\tshould correspond to the domain of it\\\'s type\',\n\tstatus : Boolean = self.conceptInstance.oclContainer().oclAsType(camel::mms::MmsConcept) = self.isA.domain\n}.status'"
 * @generated
 */
public interface MmsPropertyInstance extends MmsObject {
	/**
	 * Returns the value of the '<em><b>Is A</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is A</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is A</em>' reference.
	 * @see #setIsA(MmsProperty)
	 * @see camel.mms.MmsPackage#getMmsPropertyInstance_IsA()
	 * @model required="true"
	 * @generated
	 */
	MmsProperty getIsA();

	/**
	 * Sets the value of the '{@link camel.mms.MmsPropertyInstance#getIsA <em>Is A</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is A</em>' reference.
	 * @see #getIsA()
	 * @generated
	 */
	void setIsA(MmsProperty value);

	/**
	 * Returns the value of the '<em><b>Property Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Property Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Property Value</em>' attribute.
	 * @see #setPropertyValue(String)
	 * @see camel.mms.MmsPackage#getMmsPropertyInstance_PropertyValue()
	 * @model required="true"
	 * @generated
	 */
	String getPropertyValue();

	/**
	 * Sets the value of the '{@link camel.mms.MmsPropertyInstance#getPropertyValue <em>Property Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Property Value</em>' attribute.
	 * @see #getPropertyValue()
	 * @generated
	 */
	void setPropertyValue(String value);

	/**
	 * Returns the value of the '<em><b>Concept Instance</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link camel.mms.MmsConceptInstance#getPropertyInstance <em>Property Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Concept Instance</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concept Instance</em>' container reference.
	 * @see #setConceptInstance(MmsConceptInstance)
	 * @see camel.mms.MmsPackage#getMmsPropertyInstance_ConceptInstance()
	 * @see camel.mms.MmsConceptInstance#getPropertyInstance
	 * @model opposite="propertyInstance" required="true" transient="false"
	 * @generated
	 */
	MmsConceptInstance getConceptInstance();

	/**
	 * Sets the value of the '{@link camel.mms.MmsPropertyInstance#getConceptInstance <em>Concept Instance</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Concept Instance</em>' container reference.
	 * @see #getConceptInstance()
	 * @generated
	 */
	void setConceptInstance(MmsConceptInstance value);

} // MmsPropertyInstance
