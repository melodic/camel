/**
 */
package camel.execution;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Application Measurement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.execution.ExecutionPackage#getApplicationMeasurement()
 * @model
 * @generated
 */
public interface ApplicationMeasurement extends Measurement {
} // ApplicationMeasurement
