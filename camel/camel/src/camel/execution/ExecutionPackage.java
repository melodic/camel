/**
 */
package camel.execution;

import camel.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see camel.execution.ExecutionFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface ExecutionPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "execution";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.camel-dsl.org/camel/execution";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "execution";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExecutionPackage eINSTANCE = camel.execution.impl.ExecutionPackageImpl.init();

	/**
	 * The meta object id for the '{@link camel.execution.impl.ExecutionModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.ExecutionModelImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getExecutionModel()
	 * @generated
	 */
	int EXECUTION_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__NAME = CorePackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__DESCRIPTION = CorePackage.MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__ANNOTATIONS = CorePackage.MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__ATTRIBUTES = CorePackage.MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__SUB_FEATURES = CorePackage.MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__IMPORT_URI = CorePackage.MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__START_TIME = CorePackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__END_TIME = CorePackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Total Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__TOTAL_COST = CorePackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Cost Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__COST_UNIT = CorePackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Deployment Type Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__DEPLOYMENT_TYPE_MODEL = CorePackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Requirement Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__REQUIREMENT_MODEL = CorePackage.MODEL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Data Type Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__DATA_TYPE_MODEL = CorePackage.MODEL_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Measurements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__MEASUREMENTS = CorePackage.MODEL_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Slo Violations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__SLO_VIOLATIONS = CorePackage.MODEL_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Rule Triggers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__RULE_TRIGGERS = CorePackage.MODEL_FEATURE_COUNT + 9;

	/**
	 * The feature id for the '<em><b>History Records</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL__HISTORY_RECORDS = CorePackage.MODEL_FEATURE_COUNT + 10;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL_FEATURE_COUNT = CorePackage.MODEL_FEATURE_COUNT + 11;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL___AS_ERROR__BOOLEAN = CorePackage.MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXECUTION_MODEL_OPERATION_COUNT = CorePackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.ActionInstanceImpl <em>Action Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.ActionInstanceImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getActionInstance()
	 * @generated
	 */
	int ACTION_INSTANCE = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE__ACTION = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE__START_TIME = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE__END_TIME = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Successful</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE__SUCCESSFUL = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Action Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Action Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ACTION_INSTANCE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.MeasurementImpl <em>Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.MeasurementImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getMeasurement()
	 * @generated
	 */
	int MEASUREMENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT__METRIC_INSTANCE = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT__VALUE = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Measurement Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT__MEASUREMENT_TIME = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Event Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT__EVENT_INSTANCE = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MEASUREMENT_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.ApplicationMeasurementImpl <em>Application Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.ApplicationMeasurementImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getApplicationMeasurement()
	 * @generated
	 */
	int APPLICATION_MEASUREMENT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT__NAME = MEASUREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT__DESCRIPTION = MEASUREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT__ANNOTATIONS = MEASUREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT__ATTRIBUTES = MEASUREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT__SUB_FEATURES = MEASUREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT__METRIC_INSTANCE = MEASUREMENT__METRIC_INSTANCE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT__VALUE = MEASUREMENT__VALUE;

	/**
	 * The feature id for the '<em><b>Measurement Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT__MEASUREMENT_TIME = MEASUREMENT__MEASUREMENT_TIME;

	/**
	 * The feature id for the '<em><b>Event Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT__EVENT_INSTANCE = MEASUREMENT__EVENT_INSTANCE;

	/**
	 * The number of structural features of the '<em>Application Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT___AS_ERROR__BOOLEAN = MEASUREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Application Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int APPLICATION_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.SoftwareComponentMeasurementImpl <em>Software Component Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.SoftwareComponentMeasurementImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getSoftwareComponentMeasurement()
	 * @generated
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT__NAME = MEASUREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT__DESCRIPTION = MEASUREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT__ANNOTATIONS = MEASUREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT__ATTRIBUTES = MEASUREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT__SUB_FEATURES = MEASUREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT__METRIC_INSTANCE = MEASUREMENT__METRIC_INSTANCE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT__VALUE = MEASUREMENT__VALUE;

	/**
	 * The feature id for the '<em><b>Measurement Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT__MEASUREMENT_TIME = MEASUREMENT__MEASUREMENT_TIME;

	/**
	 * The feature id for the '<em><b>Event Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT__EVENT_INSTANCE = MEASUREMENT__EVENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Software Component Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT__SOFTWARE_COMPONENT_INSTANCE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Software Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT__SOFTWARE_COMPONENT = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Software Component Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT___AS_ERROR__BOOLEAN = MEASUREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Software Component Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.CommunicationMeasurementImpl <em>Communication Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.CommunicationMeasurementImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getCommunicationMeasurement()
	 * @generated
	 */
	int COMMUNICATION_MEASUREMENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__NAME = MEASUREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__DESCRIPTION = MEASUREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__ANNOTATIONS = MEASUREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__ATTRIBUTES = MEASUREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__SUB_FEATURES = MEASUREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__METRIC_INSTANCE = MEASUREMENT__METRIC_INSTANCE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__VALUE = MEASUREMENT__VALUE;

	/**
	 * The feature id for the '<em><b>Measurement Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__MEASUREMENT_TIME = MEASUREMENT__MEASUREMENT_TIME;

	/**
	 * The feature id for the '<em><b>Event Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__EVENT_INSTANCE = MEASUREMENT__EVENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Source VM Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__SOURCE_VM_INSTANCE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Target VM Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__TARGET_VM_INSTANCE = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Source VM</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__SOURCE_VM = MEASUREMENT_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Target VM</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT__TARGET_VM = MEASUREMENT_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Communication Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT___AS_ERROR__BOOLEAN = MEASUREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Communication Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMMUNICATION_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.VMMeasurementImpl <em>VM Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.VMMeasurementImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getVMMeasurement()
	 * @generated
	 */
	int VM_MEASUREMENT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT__NAME = MEASUREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT__DESCRIPTION = MEASUREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT__ANNOTATIONS = MEASUREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT__ATTRIBUTES = MEASUREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT__SUB_FEATURES = MEASUREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT__METRIC_INSTANCE = MEASUREMENT__METRIC_INSTANCE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT__VALUE = MEASUREMENT__VALUE;

	/**
	 * The feature id for the '<em><b>Measurement Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT__MEASUREMENT_TIME = MEASUREMENT__MEASUREMENT_TIME;

	/**
	 * The feature id for the '<em><b>Event Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT__EVENT_INSTANCE = MEASUREMENT__EVENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Vm Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT__VM_INSTANCE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Vm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT__VM = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>VM Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT___AS_ERROR__BOOLEAN = MEASUREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>VM Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VM_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.PaaSMeasurementImpl <em>Paa SMeasurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.PaaSMeasurementImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getPaaSMeasurement()
	 * @generated
	 */
	int PAA_SMEASUREMENT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT__NAME = MEASUREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT__DESCRIPTION = MEASUREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT__ANNOTATIONS = MEASUREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT__ATTRIBUTES = MEASUREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT__SUB_FEATURES = MEASUREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT__METRIC_INSTANCE = MEASUREMENT__METRIC_INSTANCE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT__VALUE = MEASUREMENT__VALUE;

	/**
	 * The feature id for the '<em><b>Measurement Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT__MEASUREMENT_TIME = MEASUREMENT__MEASUREMENT_TIME;

	/**
	 * The feature id for the '<em><b>Event Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT__EVENT_INSTANCE = MEASUREMENT__EVENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Paas Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT__PAAS_INSTANCE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Paas</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT__PAAS = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Paa SMeasurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT___AS_ERROR__BOOLEAN = MEASUREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Paa SMeasurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SMEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.ContainerMeasurementImpl <em>Container Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.ContainerMeasurementImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getContainerMeasurement()
	 * @generated
	 */
	int CONTAINER_MEASUREMENT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT__NAME = MEASUREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT__DESCRIPTION = MEASUREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT__ANNOTATIONS = MEASUREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT__ATTRIBUTES = MEASUREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT__SUB_FEATURES = MEASUREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT__METRIC_INSTANCE = MEASUREMENT__METRIC_INSTANCE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT__VALUE = MEASUREMENT__VALUE;

	/**
	 * The feature id for the '<em><b>Measurement Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT__MEASUREMENT_TIME = MEASUREMENT__MEASUREMENT_TIME;

	/**
	 * The feature id for the '<em><b>Event Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT__EVENT_INSTANCE = MEASUREMENT__EVENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Container Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT__CONTAINER_INSTANCE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT__CONTAINER = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Container Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT___AS_ERROR__BOOLEAN = MEASUREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Container Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONTAINER_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.DataMeasurementImpl <em>Data Measurement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.DataMeasurementImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getDataMeasurement()
	 * @generated
	 */
	int DATA_MEASUREMENT = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT__NAME = MEASUREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT__DESCRIPTION = MEASUREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT__ANNOTATIONS = MEASUREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT__ATTRIBUTES = MEASUREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT__SUB_FEATURES = MEASUREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT__METRIC_INSTANCE = MEASUREMENT__METRIC_INSTANCE;

	/**
	 * The feature id for the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT__VALUE = MEASUREMENT__VALUE;

	/**
	 * The feature id for the '<em><b>Measurement Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT__MEASUREMENT_TIME = MEASUREMENT__MEASUREMENT_TIME;

	/**
	 * The feature id for the '<em><b>Event Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT__EVENT_INSTANCE = MEASUREMENT__EVENT_INSTANCE;

	/**
	 * The feature id for the '<em><b>Data Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT__DATA_INSTANCE = MEASUREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT__DATA = MEASUREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Data Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT_FEATURE_COUNT = MEASUREMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT___AS_ERROR__BOOLEAN = MEASUREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Data Measurement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_MEASUREMENT_OPERATION_COUNT = MEASUREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.CauseImpl <em>Cause</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.CauseImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getCause()
	 * @generated
	 */
	int CAUSE = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Event Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE__EVENT_INSTANCES = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Cause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Cause</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CAUSE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.SLOViolationImpl <em>SLO Violation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.SLOViolationImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getSLOViolation()
	 * @generated
	 */
	int SLO_VIOLATION = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION__NAME = CAUSE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION__DESCRIPTION = CAUSE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION__ANNOTATIONS = CAUSE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION__ATTRIBUTES = CAUSE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION__SUB_FEATURES = CAUSE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Event Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION__EVENT_INSTANCES = CAUSE__EVENT_INSTANCES;

	/**
	 * The feature id for the '<em><b>Slo</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION__SLO = CAUSE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Measurement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION__MEASUREMENT = CAUSE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Assessment Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION__ASSESSMENT_TIME = CAUSE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>SLO Violation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION_FEATURE_COUNT = CAUSE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION___AS_ERROR__BOOLEAN = CAUSE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>SLO Violation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SLO_VIOLATION_OPERATION_COUNT = CAUSE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.RuleTriggerImpl <em>Rule Trigger</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.RuleTriggerImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getRuleTrigger()
	 * @generated
	 */
	int RULE_TRIGGER = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TRIGGER__NAME = CAUSE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TRIGGER__DESCRIPTION = CAUSE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TRIGGER__ANNOTATIONS = CAUSE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TRIGGER__ATTRIBUTES = CAUSE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TRIGGER__SUB_FEATURES = CAUSE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Event Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TRIGGER__EVENT_INSTANCES = CAUSE__EVENT_INSTANCES;

	/**
	 * The feature id for the '<em><b>Scalability Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TRIGGER__SCALABILITY_RULE = CAUSE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Trigerring Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TRIGGER__TRIGERRING_TIME = CAUSE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Rule Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TRIGGER_FEATURE_COUNT = CAUSE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TRIGGER___AS_ERROR__BOOLEAN = CAUSE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Rule Trigger</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RULE_TRIGGER_OPERATION_COUNT = CAUSE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.HistoryRecordImpl <em>History Record</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.HistoryRecordImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getHistoryRecord()
	 * @generated
	 */
	int HISTORY_RECORD = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__TYPE = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__CAUSE = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Infos</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__INFOS = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__START_TIME = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__END_TIME = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>From Deployment Instance Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__FROM_DEPLOYMENT_INSTANCE_MODEL = CorePackage.FEATURE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>To Deployment Instance Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__TO_DEPLOYMENT_INSTANCE_MODEL = CorePackage.FEATURE_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>From Data Instance Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__FROM_DATA_INSTANCE_MODEL = CorePackage.FEATURE_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>To Data Instance Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD__TO_DATA_INSTANCE_MODEL = CorePackage.FEATURE_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>History Record</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 9;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>History Record</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_RECORD_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.execution.impl.HistoryInfoImpl <em>History Info</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.execution.impl.HistoryInfoImpl
	 * @see camel.execution.impl.ExecutionPackageImpl#getHistoryInfo()
	 * @generated
	 */
	int HISTORY_INFO = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Subject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO__SUBJECT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO__ACTION = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO__OBJECT = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO__OBJECT_REF = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO__START_TIME = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO__END_TIME = CorePackage.FEATURE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>History Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>History Info</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HISTORY_INFO_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link camel.execution.ExecutionModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see camel.execution.ExecutionModel
	 * @generated
	 */
	EClass getExecutionModel();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.ExecutionModel#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see camel.execution.ExecutionModel#getStartTime()
	 * @see #getExecutionModel()
	 * @generated
	 */
	EAttribute getExecutionModel_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.ExecutionModel#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see camel.execution.ExecutionModel#getEndTime()
	 * @see #getExecutionModel()
	 * @generated
	 */
	EAttribute getExecutionModel_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.ExecutionModel#getTotalCost <em>Total Cost</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Total Cost</em>'.
	 * @see camel.execution.ExecutionModel#getTotalCost()
	 * @see #getExecutionModel()
	 * @generated
	 */
	EAttribute getExecutionModel_TotalCost();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.ExecutionModel#getCostUnit <em>Cost Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Cost Unit</em>'.
	 * @see camel.execution.ExecutionModel#getCostUnit()
	 * @see #getExecutionModel()
	 * @generated
	 */
	EReference getExecutionModel_CostUnit();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.ExecutionModel#getDeploymentTypeModel <em>Deployment Type Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Deployment Type Model</em>'.
	 * @see camel.execution.ExecutionModel#getDeploymentTypeModel()
	 * @see #getExecutionModel()
	 * @generated
	 */
	EReference getExecutionModel_DeploymentTypeModel();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.ExecutionModel#getRequirementModel <em>Requirement Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Requirement Model</em>'.
	 * @see camel.execution.ExecutionModel#getRequirementModel()
	 * @see #getExecutionModel()
	 * @generated
	 */
	EReference getExecutionModel_RequirementModel();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.ExecutionModel#getDataTypeModel <em>Data Type Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Data Type Model</em>'.
	 * @see camel.execution.ExecutionModel#getDataTypeModel()
	 * @see #getExecutionModel()
	 * @generated
	 */
	EReference getExecutionModel_DataTypeModel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.execution.ExecutionModel#getMeasurements <em>Measurements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Measurements</em>'.
	 * @see camel.execution.ExecutionModel#getMeasurements()
	 * @see #getExecutionModel()
	 * @generated
	 */
	EReference getExecutionModel_Measurements();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.execution.ExecutionModel#getSloViolations <em>Slo Violations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Slo Violations</em>'.
	 * @see camel.execution.ExecutionModel#getSloViolations()
	 * @see #getExecutionModel()
	 * @generated
	 */
	EReference getExecutionModel_SloViolations();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.execution.ExecutionModel#getRuleTriggers <em>Rule Triggers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Rule Triggers</em>'.
	 * @see camel.execution.ExecutionModel#getRuleTriggers()
	 * @see #getExecutionModel()
	 * @generated
	 */
	EReference getExecutionModel_RuleTriggers();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.execution.ExecutionModel#getHistoryRecords <em>History Records</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>History Records</em>'.
	 * @see camel.execution.ExecutionModel#getHistoryRecords()
	 * @see #getExecutionModel()
	 * @generated
	 */
	EReference getExecutionModel_HistoryRecords();

	/**
	 * Returns the meta object for class '{@link camel.execution.ActionInstance <em>Action Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Action Instance</em>'.
	 * @see camel.execution.ActionInstance
	 * @generated
	 */
	EClass getActionInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.ActionInstance#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Action</em>'.
	 * @see camel.execution.ActionInstance#getAction()
	 * @see #getActionInstance()
	 * @generated
	 */
	EReference getActionInstance_Action();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.ActionInstance#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see camel.execution.ActionInstance#getStartTime()
	 * @see #getActionInstance()
	 * @generated
	 */
	EAttribute getActionInstance_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.ActionInstance#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see camel.execution.ActionInstance#getEndTime()
	 * @see #getActionInstance()
	 * @generated
	 */
	EAttribute getActionInstance_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.ActionInstance#isSuccessful <em>Successful</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Successful</em>'.
	 * @see camel.execution.ActionInstance#isSuccessful()
	 * @see #getActionInstance()
	 * @generated
	 */
	EAttribute getActionInstance_Successful();

	/**
	 * Returns the meta object for class '{@link camel.execution.Measurement <em>Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Measurement</em>'.
	 * @see camel.execution.Measurement
	 * @generated
	 */
	EClass getMeasurement();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.Measurement#getMetricInstance <em>Metric Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metric Instance</em>'.
	 * @see camel.execution.Measurement#getMetricInstance()
	 * @see #getMeasurement()
	 * @generated
	 */
	EReference getMeasurement_MetricInstance();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.Measurement#getValue <em>Value</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value</em>'.
	 * @see camel.execution.Measurement#getValue()
	 * @see #getMeasurement()
	 * @generated
	 */
	EAttribute getMeasurement_Value();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.Measurement#getMeasurementTime <em>Measurement Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Measurement Time</em>'.
	 * @see camel.execution.Measurement#getMeasurementTime()
	 * @see #getMeasurement()
	 * @generated
	 */
	EAttribute getMeasurement_MeasurementTime();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.Measurement#getEventInstance <em>Event Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Event Instance</em>'.
	 * @see camel.execution.Measurement#getEventInstance()
	 * @see #getMeasurement()
	 * @generated
	 */
	EReference getMeasurement_EventInstance();

	/**
	 * Returns the meta object for class '{@link camel.execution.ApplicationMeasurement <em>Application Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Application Measurement</em>'.
	 * @see camel.execution.ApplicationMeasurement
	 * @generated
	 */
	EClass getApplicationMeasurement();

	/**
	 * Returns the meta object for class '{@link camel.execution.SoftwareComponentMeasurement <em>Software Component Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Software Component Measurement</em>'.
	 * @see camel.execution.SoftwareComponentMeasurement
	 * @generated
	 */
	EClass getSoftwareComponentMeasurement();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.SoftwareComponentMeasurement#getSoftwareComponentInstance <em>Software Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Software Component Instance</em>'.
	 * @see camel.execution.SoftwareComponentMeasurement#getSoftwareComponentInstance()
	 * @see #getSoftwareComponentMeasurement()
	 * @generated
	 */
	EReference getSoftwareComponentMeasurement_SoftwareComponentInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.SoftwareComponentMeasurement#getSoftwareComponent <em>Software Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Software Component</em>'.
	 * @see camel.execution.SoftwareComponentMeasurement#getSoftwareComponent()
	 * @see #getSoftwareComponentMeasurement()
	 * @generated
	 */
	EReference getSoftwareComponentMeasurement_SoftwareComponent();

	/**
	 * Returns the meta object for class '{@link camel.execution.CommunicationMeasurement <em>Communication Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Communication Measurement</em>'.
	 * @see camel.execution.CommunicationMeasurement
	 * @generated
	 */
	EClass getCommunicationMeasurement();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.CommunicationMeasurement#getSourceVMInstance <em>Source VM Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source VM Instance</em>'.
	 * @see camel.execution.CommunicationMeasurement#getSourceVMInstance()
	 * @see #getCommunicationMeasurement()
	 * @generated
	 */
	EReference getCommunicationMeasurement_SourceVMInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.CommunicationMeasurement#getTargetVMInstance <em>Target VM Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target VM Instance</em>'.
	 * @see camel.execution.CommunicationMeasurement#getTargetVMInstance()
	 * @see #getCommunicationMeasurement()
	 * @generated
	 */
	EReference getCommunicationMeasurement_TargetVMInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.CommunicationMeasurement#getSourceVM <em>Source VM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Source VM</em>'.
	 * @see camel.execution.CommunicationMeasurement#getSourceVM()
	 * @see #getCommunicationMeasurement()
	 * @generated
	 */
	EReference getCommunicationMeasurement_SourceVM();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.CommunicationMeasurement#getTargetVM <em>Target VM</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Target VM</em>'.
	 * @see camel.execution.CommunicationMeasurement#getTargetVM()
	 * @see #getCommunicationMeasurement()
	 * @generated
	 */
	EReference getCommunicationMeasurement_TargetVM();

	/**
	 * Returns the meta object for class '{@link camel.execution.VMMeasurement <em>VM Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>VM Measurement</em>'.
	 * @see camel.execution.VMMeasurement
	 * @generated
	 */
	EClass getVMMeasurement();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.VMMeasurement#getVmInstance <em>Vm Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Vm Instance</em>'.
	 * @see camel.execution.VMMeasurement#getVmInstance()
	 * @see #getVMMeasurement()
	 * @generated
	 */
	EReference getVMMeasurement_VmInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.VMMeasurement#getVm <em>Vm</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Vm</em>'.
	 * @see camel.execution.VMMeasurement#getVm()
	 * @see #getVMMeasurement()
	 * @generated
	 */
	EReference getVMMeasurement_Vm();

	/**
	 * Returns the meta object for class '{@link camel.execution.PaaSMeasurement <em>Paa SMeasurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Paa SMeasurement</em>'.
	 * @see camel.execution.PaaSMeasurement
	 * @generated
	 */
	EClass getPaaSMeasurement();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.PaaSMeasurement#getPaasInstance <em>Paas Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Paas Instance</em>'.
	 * @see camel.execution.PaaSMeasurement#getPaasInstance()
	 * @see #getPaaSMeasurement()
	 * @generated
	 */
	EReference getPaaSMeasurement_PaasInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.PaaSMeasurement#getPaas <em>Paas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Paas</em>'.
	 * @see camel.execution.PaaSMeasurement#getPaas()
	 * @see #getPaaSMeasurement()
	 * @generated
	 */
	EReference getPaaSMeasurement_Paas();

	/**
	 * Returns the meta object for class '{@link camel.execution.ContainerMeasurement <em>Container Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Container Measurement</em>'.
	 * @see camel.execution.ContainerMeasurement
	 * @generated
	 */
	EClass getContainerMeasurement();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.ContainerMeasurement#getContainerInstance <em>Container Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Container Instance</em>'.
	 * @see camel.execution.ContainerMeasurement#getContainerInstance()
	 * @see #getContainerMeasurement()
	 * @generated
	 */
	EReference getContainerMeasurement_ContainerInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.ContainerMeasurement#getContainer <em>Container</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Container</em>'.
	 * @see camel.execution.ContainerMeasurement#getContainer()
	 * @see #getContainerMeasurement()
	 * @generated
	 */
	EReference getContainerMeasurement_Container();

	/**
	 * Returns the meta object for class '{@link camel.execution.DataMeasurement <em>Data Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Measurement</em>'.
	 * @see camel.execution.DataMeasurement
	 * @generated
	 */
	EClass getDataMeasurement();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.DataMeasurement#getDataInstance <em>Data Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Data Instance</em>'.
	 * @see camel.execution.DataMeasurement#getDataInstance()
	 * @see #getDataMeasurement()
	 * @generated
	 */
	EReference getDataMeasurement_DataInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.DataMeasurement#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Data</em>'.
	 * @see camel.execution.DataMeasurement#getData()
	 * @see #getDataMeasurement()
	 * @generated
	 */
	EReference getDataMeasurement_Data();

	/**
	 * Returns the meta object for class '{@link camel.execution.SLOViolation <em>SLO Violation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SLO Violation</em>'.
	 * @see camel.execution.SLOViolation
	 * @generated
	 */
	EClass getSLOViolation();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.SLOViolation#getSlo <em>Slo</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Slo</em>'.
	 * @see camel.execution.SLOViolation#getSlo()
	 * @see #getSLOViolation()
	 * @generated
	 */
	EReference getSLOViolation_Slo();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.SLOViolation#getMeasurement <em>Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Measurement</em>'.
	 * @see camel.execution.SLOViolation#getMeasurement()
	 * @see #getSLOViolation()
	 * @generated
	 */
	EReference getSLOViolation_Measurement();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.SLOViolation#getAssessmentTime <em>Assessment Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assessment Time</em>'.
	 * @see camel.execution.SLOViolation#getAssessmentTime()
	 * @see #getSLOViolation()
	 * @generated
	 */
	EAttribute getSLOViolation_AssessmentTime();

	/**
	 * Returns the meta object for class '{@link camel.execution.RuleTrigger <em>Rule Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Rule Trigger</em>'.
	 * @see camel.execution.RuleTrigger
	 * @generated
	 */
	EClass getRuleTrigger();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.RuleTrigger#getScalabilityRule <em>Scalability Rule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Scalability Rule</em>'.
	 * @see camel.execution.RuleTrigger#getScalabilityRule()
	 * @see #getRuleTrigger()
	 * @generated
	 */
	EReference getRuleTrigger_ScalabilityRule();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.RuleTrigger#getTrigerringTime <em>Trigerring Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Trigerring Time</em>'.
	 * @see camel.execution.RuleTrigger#getTrigerringTime()
	 * @see #getRuleTrigger()
	 * @generated
	 */
	EAttribute getRuleTrigger_TrigerringTime();

	/**
	 * Returns the meta object for class '{@link camel.execution.HistoryRecord <em>History Record</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>History Record</em>'.
	 * @see camel.execution.HistoryRecord
	 * @generated
	 */
	EClass getHistoryRecord();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.HistoryRecord#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see camel.execution.HistoryRecord#getType()
	 * @see #getHistoryRecord()
	 * @generated
	 */
	EReference getHistoryRecord_Type();

	/**
	 * Returns the meta object for the containment reference '{@link camel.execution.HistoryRecord#getCause <em>Cause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Cause</em>'.
	 * @see camel.execution.HistoryRecord#getCause()
	 * @see #getHistoryRecord()
	 * @generated
	 */
	EReference getHistoryRecord_Cause();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.execution.HistoryRecord#getInfos <em>Infos</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Infos</em>'.
	 * @see camel.execution.HistoryRecord#getInfos()
	 * @see #getHistoryRecord()
	 * @generated
	 */
	EReference getHistoryRecord_Infos();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.HistoryRecord#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see camel.execution.HistoryRecord#getStartTime()
	 * @see #getHistoryRecord()
	 * @generated
	 */
	EAttribute getHistoryRecord_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.HistoryRecord#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see camel.execution.HistoryRecord#getEndTime()
	 * @see #getHistoryRecord()
	 * @generated
	 */
	EAttribute getHistoryRecord_EndTime();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.HistoryRecord#getFromDeploymentInstanceModel <em>From Deployment Instance Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From Deployment Instance Model</em>'.
	 * @see camel.execution.HistoryRecord#getFromDeploymentInstanceModel()
	 * @see #getHistoryRecord()
	 * @generated
	 */
	EReference getHistoryRecord_FromDeploymentInstanceModel();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.HistoryRecord#getToDeploymentInstanceModel <em>To Deployment Instance Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To Deployment Instance Model</em>'.
	 * @see camel.execution.HistoryRecord#getToDeploymentInstanceModel()
	 * @see #getHistoryRecord()
	 * @generated
	 */
	EReference getHistoryRecord_ToDeploymentInstanceModel();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.HistoryRecord#getFromDataInstanceModel <em>From Data Instance Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>From Data Instance Model</em>'.
	 * @see camel.execution.HistoryRecord#getFromDataInstanceModel()
	 * @see #getHistoryRecord()
	 * @generated
	 */
	EReference getHistoryRecord_FromDataInstanceModel();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.HistoryRecord#getToDataInstanceModel <em>To Data Instance Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>To Data Instance Model</em>'.
	 * @see camel.execution.HistoryRecord#getToDataInstanceModel()
	 * @see #getHistoryRecord()
	 * @generated
	 */
	EReference getHistoryRecord_ToDataInstanceModel();

	/**
	 * Returns the meta object for class '{@link camel.execution.HistoryInfo <em>History Info</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>History Info</em>'.
	 * @see camel.execution.HistoryInfo
	 * @generated
	 */
	EClass getHistoryInfo();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.HistoryInfo#getSubject <em>Subject</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Subject</em>'.
	 * @see camel.execution.HistoryInfo#getSubject()
	 * @see #getHistoryInfo()
	 * @generated
	 */
	EReference getHistoryInfo_Subject();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.HistoryInfo#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Action</em>'.
	 * @see camel.execution.HistoryInfo#getAction()
	 * @see #getHistoryInfo()
	 * @generated
	 */
	EReference getHistoryInfo_Action();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.HistoryInfo#getObject <em>Object</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Object</em>'.
	 * @see camel.execution.HistoryInfo#getObject()
	 * @see #getHistoryInfo()
	 * @generated
	 */
	EAttribute getHistoryInfo_Object();

	/**
	 * Returns the meta object for the reference '{@link camel.execution.HistoryInfo#getObjectRef <em>Object Ref</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object Ref</em>'.
	 * @see camel.execution.HistoryInfo#getObjectRef()
	 * @see #getHistoryInfo()
	 * @generated
	 */
	EReference getHistoryInfo_ObjectRef();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.HistoryInfo#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see camel.execution.HistoryInfo#getStartTime()
	 * @see #getHistoryInfo()
	 * @generated
	 */
	EAttribute getHistoryInfo_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link camel.execution.HistoryInfo#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see camel.execution.HistoryInfo#getEndTime()
	 * @see #getHistoryInfo()
	 * @generated
	 */
	EAttribute getHistoryInfo_EndTime();

	/**
	 * Returns the meta object for class '{@link camel.execution.Cause <em>Cause</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cause</em>'.
	 * @see camel.execution.Cause
	 * @generated
	 */
	EClass getCause();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.execution.Cause#getEventInstances <em>Event Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Event Instances</em>'.
	 * @see camel.execution.Cause#getEventInstances()
	 * @see #getCause()
	 * @generated
	 */
	EReference getCause_EventInstances();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	ExecutionFactory getExecutionFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link camel.execution.impl.ExecutionModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.ExecutionModelImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getExecutionModel()
		 * @generated
		 */
		EClass EXECUTION_MODEL = eINSTANCE.getExecutionModel();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_MODEL__START_TIME = eINSTANCE.getExecutionModel_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_MODEL__END_TIME = eINSTANCE.getExecutionModel_EndTime();

		/**
		 * The meta object literal for the '<em><b>Total Cost</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXECUTION_MODEL__TOTAL_COST = eINSTANCE.getExecutionModel_TotalCost();

		/**
		 * The meta object literal for the '<em><b>Cost Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_MODEL__COST_UNIT = eINSTANCE.getExecutionModel_CostUnit();

		/**
		 * The meta object literal for the '<em><b>Deployment Type Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_MODEL__DEPLOYMENT_TYPE_MODEL = eINSTANCE.getExecutionModel_DeploymentTypeModel();

		/**
		 * The meta object literal for the '<em><b>Requirement Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_MODEL__REQUIREMENT_MODEL = eINSTANCE.getExecutionModel_RequirementModel();

		/**
		 * The meta object literal for the '<em><b>Data Type Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_MODEL__DATA_TYPE_MODEL = eINSTANCE.getExecutionModel_DataTypeModel();

		/**
		 * The meta object literal for the '<em><b>Measurements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_MODEL__MEASUREMENTS = eINSTANCE.getExecutionModel_Measurements();

		/**
		 * The meta object literal for the '<em><b>Slo Violations</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_MODEL__SLO_VIOLATIONS = eINSTANCE.getExecutionModel_SloViolations();

		/**
		 * The meta object literal for the '<em><b>Rule Triggers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_MODEL__RULE_TRIGGERS = eINSTANCE.getExecutionModel_RuleTriggers();

		/**
		 * The meta object literal for the '<em><b>History Records</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference EXECUTION_MODEL__HISTORY_RECORDS = eINSTANCE.getExecutionModel_HistoryRecords();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.ActionInstanceImpl <em>Action Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.ActionInstanceImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getActionInstance()
		 * @generated
		 */
		EClass ACTION_INSTANCE = eINSTANCE.getActionInstance();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ACTION_INSTANCE__ACTION = eINSTANCE.getActionInstance_Action();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION_INSTANCE__START_TIME = eINSTANCE.getActionInstance_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION_INSTANCE__END_TIME = eINSTANCE.getActionInstance_EndTime();

		/**
		 * The meta object literal for the '<em><b>Successful</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ACTION_INSTANCE__SUCCESSFUL = eINSTANCE.getActionInstance_Successful();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.MeasurementImpl <em>Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.MeasurementImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getMeasurement()
		 * @generated
		 */
		EClass MEASUREMENT = eINSTANCE.getMeasurement();

		/**
		 * The meta object literal for the '<em><b>Metric Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEASUREMENT__METRIC_INSTANCE = eINSTANCE.getMeasurement_MetricInstance();

		/**
		 * The meta object literal for the '<em><b>Value</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEASUREMENT__VALUE = eINSTANCE.getMeasurement_Value();

		/**
		 * The meta object literal for the '<em><b>Measurement Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MEASUREMENT__MEASUREMENT_TIME = eINSTANCE.getMeasurement_MeasurementTime();

		/**
		 * The meta object literal for the '<em><b>Event Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference MEASUREMENT__EVENT_INSTANCE = eINSTANCE.getMeasurement_EventInstance();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.ApplicationMeasurementImpl <em>Application Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.ApplicationMeasurementImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getApplicationMeasurement()
		 * @generated
		 */
		EClass APPLICATION_MEASUREMENT = eINSTANCE.getApplicationMeasurement();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.SoftwareComponentMeasurementImpl <em>Software Component Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.SoftwareComponentMeasurementImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getSoftwareComponentMeasurement()
		 * @generated
		 */
		EClass SOFTWARE_COMPONENT_MEASUREMENT = eINSTANCE.getSoftwareComponentMeasurement();

		/**
		 * The meta object literal for the '<em><b>Software Component Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT_MEASUREMENT__SOFTWARE_COMPONENT_INSTANCE = eINSTANCE.getSoftwareComponentMeasurement_SoftwareComponentInstance();

		/**
		 * The meta object literal for the '<em><b>Software Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT_MEASUREMENT__SOFTWARE_COMPONENT = eINSTANCE.getSoftwareComponentMeasurement_SoftwareComponent();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.CommunicationMeasurementImpl <em>Communication Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.CommunicationMeasurementImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getCommunicationMeasurement()
		 * @generated
		 */
		EClass COMMUNICATION_MEASUREMENT = eINSTANCE.getCommunicationMeasurement();

		/**
		 * The meta object literal for the '<em><b>Source VM Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_MEASUREMENT__SOURCE_VM_INSTANCE = eINSTANCE.getCommunicationMeasurement_SourceVMInstance();

		/**
		 * The meta object literal for the '<em><b>Target VM Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_MEASUREMENT__TARGET_VM_INSTANCE = eINSTANCE.getCommunicationMeasurement_TargetVMInstance();

		/**
		 * The meta object literal for the '<em><b>Source VM</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_MEASUREMENT__SOURCE_VM = eINSTANCE.getCommunicationMeasurement_SourceVM();

		/**
		 * The meta object literal for the '<em><b>Target VM</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMMUNICATION_MEASUREMENT__TARGET_VM = eINSTANCE.getCommunicationMeasurement_TargetVM();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.VMMeasurementImpl <em>VM Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.VMMeasurementImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getVMMeasurement()
		 * @generated
		 */
		EClass VM_MEASUREMENT = eINSTANCE.getVMMeasurement();

		/**
		 * The meta object literal for the '<em><b>Vm Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM_MEASUREMENT__VM_INSTANCE = eINSTANCE.getVMMeasurement_VmInstance();

		/**
		 * The meta object literal for the '<em><b>Vm</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference VM_MEASUREMENT__VM = eINSTANCE.getVMMeasurement_Vm();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.PaaSMeasurementImpl <em>Paa SMeasurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.PaaSMeasurementImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getPaaSMeasurement()
		 * @generated
		 */
		EClass PAA_SMEASUREMENT = eINSTANCE.getPaaSMeasurement();

		/**
		 * The meta object literal for the '<em><b>Paas Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PAA_SMEASUREMENT__PAAS_INSTANCE = eINSTANCE.getPaaSMeasurement_PaasInstance();

		/**
		 * The meta object literal for the '<em><b>Paas</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PAA_SMEASUREMENT__PAAS = eINSTANCE.getPaaSMeasurement_Paas();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.ContainerMeasurementImpl <em>Container Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.ContainerMeasurementImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getContainerMeasurement()
		 * @generated
		 */
		EClass CONTAINER_MEASUREMENT = eINSTANCE.getContainerMeasurement();

		/**
		 * The meta object literal for the '<em><b>Container Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER_MEASUREMENT__CONTAINER_INSTANCE = eINSTANCE.getContainerMeasurement_ContainerInstance();

		/**
		 * The meta object literal for the '<em><b>Container</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CONTAINER_MEASUREMENT__CONTAINER = eINSTANCE.getContainerMeasurement_Container();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.DataMeasurementImpl <em>Data Measurement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.DataMeasurementImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getDataMeasurement()
		 * @generated
		 */
		EClass DATA_MEASUREMENT = eINSTANCE.getDataMeasurement();

		/**
		 * The meta object literal for the '<em><b>Data Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_MEASUREMENT__DATA_INSTANCE = eINSTANCE.getDataMeasurement_DataInstance();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_MEASUREMENT__DATA = eINSTANCE.getDataMeasurement_Data();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.SLOViolationImpl <em>SLO Violation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.SLOViolationImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getSLOViolation()
		 * @generated
		 */
		EClass SLO_VIOLATION = eINSTANCE.getSLOViolation();

		/**
		 * The meta object literal for the '<em><b>Slo</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SLO_VIOLATION__SLO = eINSTANCE.getSLOViolation_Slo();

		/**
		 * The meta object literal for the '<em><b>Measurement</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SLO_VIOLATION__MEASUREMENT = eINSTANCE.getSLOViolation_Measurement();

		/**
		 * The meta object literal for the '<em><b>Assessment Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SLO_VIOLATION__ASSESSMENT_TIME = eINSTANCE.getSLOViolation_AssessmentTime();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.RuleTriggerImpl <em>Rule Trigger</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.RuleTriggerImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getRuleTrigger()
		 * @generated
		 */
		EClass RULE_TRIGGER = eINSTANCE.getRuleTrigger();

		/**
		 * The meta object literal for the '<em><b>Scalability Rule</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RULE_TRIGGER__SCALABILITY_RULE = eINSTANCE.getRuleTrigger_ScalabilityRule();

		/**
		 * The meta object literal for the '<em><b>Trigerring Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RULE_TRIGGER__TRIGERRING_TIME = eINSTANCE.getRuleTrigger_TrigerringTime();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.HistoryRecordImpl <em>History Record</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.HistoryRecordImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getHistoryRecord()
		 * @generated
		 */
		EClass HISTORY_RECORD = eINSTANCE.getHistoryRecord();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_RECORD__TYPE = eINSTANCE.getHistoryRecord_Type();

		/**
		 * The meta object literal for the '<em><b>Cause</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_RECORD__CAUSE = eINSTANCE.getHistoryRecord_Cause();

		/**
		 * The meta object literal for the '<em><b>Infos</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_RECORD__INFOS = eINSTANCE.getHistoryRecord_Infos();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HISTORY_RECORD__START_TIME = eINSTANCE.getHistoryRecord_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HISTORY_RECORD__END_TIME = eINSTANCE.getHistoryRecord_EndTime();

		/**
		 * The meta object literal for the '<em><b>From Deployment Instance Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_RECORD__FROM_DEPLOYMENT_INSTANCE_MODEL = eINSTANCE.getHistoryRecord_FromDeploymentInstanceModel();

		/**
		 * The meta object literal for the '<em><b>To Deployment Instance Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_RECORD__TO_DEPLOYMENT_INSTANCE_MODEL = eINSTANCE.getHistoryRecord_ToDeploymentInstanceModel();

		/**
		 * The meta object literal for the '<em><b>From Data Instance Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_RECORD__FROM_DATA_INSTANCE_MODEL = eINSTANCE.getHistoryRecord_FromDataInstanceModel();

		/**
		 * The meta object literal for the '<em><b>To Data Instance Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_RECORD__TO_DATA_INSTANCE_MODEL = eINSTANCE.getHistoryRecord_ToDataInstanceModel();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.HistoryInfoImpl <em>History Info</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.HistoryInfoImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getHistoryInfo()
		 * @generated
		 */
		EClass HISTORY_INFO = eINSTANCE.getHistoryInfo();

		/**
		 * The meta object literal for the '<em><b>Subject</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_INFO__SUBJECT = eINSTANCE.getHistoryInfo_Subject();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_INFO__ACTION = eINSTANCE.getHistoryInfo_Action();

		/**
		 * The meta object literal for the '<em><b>Object</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HISTORY_INFO__OBJECT = eINSTANCE.getHistoryInfo_Object();

		/**
		 * The meta object literal for the '<em><b>Object Ref</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference HISTORY_INFO__OBJECT_REF = eINSTANCE.getHistoryInfo_ObjectRef();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HISTORY_INFO__START_TIME = eINSTANCE.getHistoryInfo_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HISTORY_INFO__END_TIME = eINSTANCE.getHistoryInfo_EndTime();

		/**
		 * The meta object literal for the '{@link camel.execution.impl.CauseImpl <em>Cause</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.execution.impl.CauseImpl
		 * @see camel.execution.impl.ExecutionPackageImpl#getCause()
		 * @generated
		 */
		EClass CAUSE = eINSTANCE.getCause();

		/**
		 * The meta object literal for the '<em><b>Event Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference CAUSE__EVENT_INSTANCES = eINSTANCE.getCause_EventInstances();

	}

} //ExecutionPackage
