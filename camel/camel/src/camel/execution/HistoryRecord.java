/**
 */
package camel.execution;

import camel.core.Feature;
import camel.data.DataInstanceModel;
import camel.deployment.DeploymentInstanceModel;

import camel.mms.MmsObject;
import java.util.Date;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>History Record</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.HistoryRecord#getType <em>Type</em>}</li>
 *   <li>{@link camel.execution.HistoryRecord#getCause <em>Cause</em>}</li>
 *   <li>{@link camel.execution.HistoryRecord#getInfos <em>Infos</em>}</li>
 *   <li>{@link camel.execution.HistoryRecord#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link camel.execution.HistoryRecord#getEndTime <em>End Time</em>}</li>
 *   <li>{@link camel.execution.HistoryRecord#getFromDeploymentInstanceModel <em>From Deployment Instance Model</em>}</li>
 *   <li>{@link camel.execution.HistoryRecord#getToDeploymentInstanceModel <em>To Deployment Instance Model</em>}</li>
 *   <li>{@link camel.execution.HistoryRecord#getFromDataInstanceModel <em>From Data Instance Model</em>}</li>
 *   <li>{@link camel.execution.HistoryRecord#getToDataInstanceModel <em>To Data Instance Model</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getHistoryRecord()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='history_record_from_to_diff history_record_correct_instance_model'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot history_record_from_to_diff='Tuple {\n\tmessage : String = \'HistoryRecord: \' + self.name + \' has from and to instance models which are identical\'\n\t\t\t,\n\tstatus : Boolean = \n\t\t\t\tasError((fromDeploymentInstanceModel &lt;&gt; null and toDeploymentInstanceModel &lt;&gt; null implies fromDeploymentInstanceModel &lt;&gt; toDeploymentInstanceModel)\n\t\t\t\tand \n\t\t\t\t(fromDataInstanceModel &lt;&gt; null and toDataInstanceModel &lt;&gt; null implies fromDataInstanceModel &lt;&gt; toDataInstanceModel))\n}.status' history_record_correct_instance_model='Tuple {\n\tmessage : String = \'HistoryRecord: \' + self.name + \' should have all non-null instance models correct, i.e., instance models with a type that belongs to the encompassing execution model\',\n\tstatus : Boolean = \n\t\t\t\tasError(let em: ExecutionModel = self.oclContainer().oclAsType(ExecutionModel) in \n\t\t\t\t\t(fromDeploymentInstanceModel &lt;&gt; null implies fromDeploymentInstanceModel.type = em.deploymentTypeModel) and\n\t\t\t\t\t(toDeploymentInstanceModel &lt;&gt; null implies toDeploymentInstanceModel.type = em.deploymentTypeModel) and\n\t\t\t\t\t(fromDataInstanceModel &lt;&gt; null implies fromDataInstanceModel.type = em.dataTypeModel) and\n\t\t\t\t\t(toDataInstanceModel &lt;&gt; null implies toDataInstanceModel.type = em.dataTypeModel))\n}.status'"
 * @generated
 */
public interface HistoryRecord extends Feature {
	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(MmsObject)
	 * @see camel.execution.ExecutionPackage#getHistoryRecord_Type()
	 * @model required="true"
	 * @generated
	 */
	MmsObject getType();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryRecord#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(MmsObject value);

	/**
	 * Returns the value of the '<em><b>Cause</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cause</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cause</em>' containment reference.
	 * @see #setCause(Cause)
	 * @see camel.execution.ExecutionPackage#getHistoryRecord_Cause()
	 * @model containment="true"
	 * @generated
	 */
	Cause getCause();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryRecord#getCause <em>Cause</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cause</em>' containment reference.
	 * @see #getCause()
	 * @generated
	 */
	void setCause(Cause value);

	/**
	 * Returns the value of the '<em><b>Infos</b></em>' containment reference list.
	 * The list contents are of type {@link camel.execution.HistoryInfo}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Infos</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Infos</em>' containment reference list.
	 * @see camel.execution.ExecutionPackage#getHistoryRecord_Infos()
	 * @model containment="true"
	 * @generated
	 */
	EList<HistoryInfo> getInfos();

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(Date)
	 * @see camel.execution.ExecutionPackage#getHistoryRecord_StartTime()
	 * @model required="true"
	 * @generated
	 */
	Date getStartTime();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryRecord#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(Date value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(Date)
	 * @see camel.execution.ExecutionPackage#getHistoryRecord_EndTime()
	 * @model required="true"
	 * @generated
	 */
	Date getEndTime();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryRecord#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(Date value);

	/**
	 * Returns the value of the '<em><b>From Deployment Instance Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Deployment Instance Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Deployment Instance Model</em>' reference.
	 * @see #setFromDeploymentInstanceModel(DeploymentInstanceModel)
	 * @see camel.execution.ExecutionPackage#getHistoryRecord_FromDeploymentInstanceModel()
	 * @model
	 * @generated
	 */
	DeploymentInstanceModel getFromDeploymentInstanceModel();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryRecord#getFromDeploymentInstanceModel <em>From Deployment Instance Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Deployment Instance Model</em>' reference.
	 * @see #getFromDeploymentInstanceModel()
	 * @generated
	 */
	void setFromDeploymentInstanceModel(DeploymentInstanceModel value);

	/**
	 * Returns the value of the '<em><b>To Deployment Instance Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Deployment Instance Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Deployment Instance Model</em>' reference.
	 * @see #setToDeploymentInstanceModel(DeploymentInstanceModel)
	 * @see camel.execution.ExecutionPackage#getHistoryRecord_ToDeploymentInstanceModel()
	 * @model
	 * @generated
	 */
	DeploymentInstanceModel getToDeploymentInstanceModel();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryRecord#getToDeploymentInstanceModel <em>To Deployment Instance Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Deployment Instance Model</em>' reference.
	 * @see #getToDeploymentInstanceModel()
	 * @generated
	 */
	void setToDeploymentInstanceModel(DeploymentInstanceModel value);

	/**
	 * Returns the value of the '<em><b>From Data Instance Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>From Data Instance Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>From Data Instance Model</em>' reference.
	 * @see #setFromDataInstanceModel(DataInstanceModel)
	 * @see camel.execution.ExecutionPackage#getHistoryRecord_FromDataInstanceModel()
	 * @model
	 * @generated
	 */
	DataInstanceModel getFromDataInstanceModel();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryRecord#getFromDataInstanceModel <em>From Data Instance Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>From Data Instance Model</em>' reference.
	 * @see #getFromDataInstanceModel()
	 * @generated
	 */
	void setFromDataInstanceModel(DataInstanceModel value);

	/**
	 * Returns the value of the '<em><b>To Data Instance Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>To Data Instance Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>To Data Instance Model</em>' reference.
	 * @see #setToDataInstanceModel(DataInstanceModel)
	 * @see camel.execution.ExecutionPackage#getHistoryRecord_ToDataInstanceModel()
	 * @model
	 * @generated
	 */
	DataInstanceModel getToDataInstanceModel();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryRecord#getToDataInstanceModel <em>To Data Instance Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>To Data Instance Model</em>' reference.
	 * @see #getToDataInstanceModel()
	 * @generated
	 */
	void setToDataInstanceModel(DataInstanceModel value);

} // HistoryRecord
