/**
 */
package camel.execution;

import camel.core.Feature;

import camel.mms.MmsObject;
import java.util.Date;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>History Info</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.HistoryInfo#getSubject <em>Subject</em>}</li>
 *   <li>{@link camel.execution.HistoryInfo#getAction <em>Action</em>}</li>
 *   <li>{@link camel.execution.HistoryInfo#getObject <em>Object</em>}</li>
 *   <li>{@link camel.execution.HistoryInfo#getObjectRef <em>Object Ref</em>}</li>
 *   <li>{@link camel.execution.HistoryInfo#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link camel.execution.HistoryInfo#getEndTime <em>End Time</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getHistoryInfo()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='at_least_object_or_objectRef_in_history_info'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot at_least_object_or_objectRef_in_history_info='Tuple {\n\tmessage : String = \'HistoryInfo: \' + self.name + \' should either to a certain model or to a String-based object in case a certain message needs to be recorded (e.g., error)\',\n\tstatus : Boolean = \n\t\t\t\tasError((object &lt;&gt; null and objectRef = null) or (object = null and objectRef &lt;&gt; null))\n}.status'"
 * @generated
 */
public interface HistoryInfo extends Feature {
	/**
	 * Returns the value of the '<em><b>Subject</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Subject</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Subject</em>' reference.
	 * @see #setSubject(MmsObject)
	 * @see camel.execution.ExecutionPackage#getHistoryInfo_Subject()
	 * @model required="true"
	 * @generated
	 */
	MmsObject getSubject();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryInfo#getSubject <em>Subject</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Subject</em>' reference.
	 * @see #getSubject()
	 * @generated
	 */
	void setSubject(MmsObject value);

	/**
	 * Returns the value of the '<em><b>Action</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' reference.
	 * @see #setAction(MmsObject)
	 * @see camel.execution.ExecutionPackage#getHistoryInfo_Action()
	 * @model required="true"
	 * @generated
	 */
	MmsObject getAction();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryInfo#getAction <em>Action</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' reference.
	 * @see #getAction()
	 * @generated
	 */
	void setAction(MmsObject value);

	/**
	 * Returns the value of the '<em><b>Object</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object</em>' attribute.
	 * @see #setObject(String)
	 * @see camel.execution.ExecutionPackage#getHistoryInfo_Object()
	 * @model
	 * @generated
	 */
	String getObject();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryInfo#getObject <em>Object</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object</em>' attribute.
	 * @see #getObject()
	 * @generated
	 */
	void setObject(String value);

	/**
	 * Returns the value of the '<em><b>Object Ref</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Ref</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Ref</em>' reference.
	 * @see #setObjectRef(EObject)
	 * @see camel.execution.ExecutionPackage#getHistoryInfo_ObjectRef()
	 * @model
	 * @generated
	 */
	EObject getObjectRef();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryInfo#getObjectRef <em>Object Ref</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Ref</em>' reference.
	 * @see #getObjectRef()
	 * @generated
	 */
	void setObjectRef(EObject value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(Date)
	 * @see camel.execution.ExecutionPackage#getHistoryInfo_StartTime()
	 * @model required="true"
	 * @generated
	 */
	Date getStartTime();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryInfo#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(Date value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(Date)
	 * @see camel.execution.ExecutionPackage#getHistoryInfo_EndTime()
	 * @model required="true"
	 * @generated
	 */
	Date getEndTime();

	/**
	 * Sets the value of the '{@link camel.execution.HistoryInfo#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(Date value);

} // HistoryInfo
