/**
 */
package camel.execution.impl;

import camel.core.impl.FeatureImpl;

import camel.execution.ExecutionPackage;
import camel.execution.HistoryInfo;

import camel.mms.MmsObject;
import java.util.Date;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>History Info</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.HistoryInfoImpl#getSubject <em>Subject</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryInfoImpl#getAction <em>Action</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryInfoImpl#getObject <em>Object</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryInfoImpl#getObjectRef <em>Object Ref</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryInfoImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryInfoImpl#getEndTime <em>End Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HistoryInfoImpl extends FeatureImpl implements HistoryInfo {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HistoryInfoImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.HISTORY_INFO;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsObject getSubject() {
		return (MmsObject)eGet(ExecutionPackage.Literals.HISTORY_INFO__SUBJECT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSubject(MmsObject newSubject) {
		eSet(ExecutionPackage.Literals.HISTORY_INFO__SUBJECT, newSubject);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsObject getAction() {
		return (MmsObject)eGet(ExecutionPackage.Literals.HISTORY_INFO__ACTION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAction(MmsObject newAction) {
		eSet(ExecutionPackage.Literals.HISTORY_INFO__ACTION, newAction);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getObject() {
		return (String)eGet(ExecutionPackage.Literals.HISTORY_INFO__OBJECT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObject(String newObject) {
		eSet(ExecutionPackage.Literals.HISTORY_INFO__OBJECT, newObject);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EObject getObjectRef() {
		return (EObject)eGet(ExecutionPackage.Literals.HISTORY_INFO__OBJECT_REF, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectRef(EObject newObjectRef) {
		eSet(ExecutionPackage.Literals.HISTORY_INFO__OBJECT_REF, newObjectRef);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getStartTime() {
		return (Date)eGet(ExecutionPackage.Literals.HISTORY_INFO__START_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(Date newStartTime) {
		eSet(ExecutionPackage.Literals.HISTORY_INFO__START_TIME, newStartTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getEndTime() {
		return (Date)eGet(ExecutionPackage.Literals.HISTORY_INFO__END_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(Date newEndTime) {
		eSet(ExecutionPackage.Literals.HISTORY_INFO__END_TIME, newEndTime);
	}

} //HistoryInfoImpl
