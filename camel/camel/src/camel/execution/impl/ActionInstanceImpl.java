/**
 */
package camel.execution.impl;

import camel.core.Action;

import camel.core.impl.FeatureImpl;

import camel.execution.ActionInstance;
import camel.execution.ExecutionPackage;

import java.util.Date;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Action Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.ActionInstanceImpl#getAction <em>Action</em>}</li>
 *   <li>{@link camel.execution.impl.ActionInstanceImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link camel.execution.impl.ActionInstanceImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link camel.execution.impl.ActionInstanceImpl#isSuccessful <em>Successful</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ActionInstanceImpl extends FeatureImpl implements ActionInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ActionInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.ACTION_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Action getAction() {
		return (Action)eGet(ExecutionPackage.Literals.ACTION_INSTANCE__ACTION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAction(Action newAction) {
		eSet(ExecutionPackage.Literals.ACTION_INSTANCE__ACTION, newAction);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getStartTime() {
		return (Date)eGet(ExecutionPackage.Literals.ACTION_INSTANCE__START_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(Date newStartTime) {
		eSet(ExecutionPackage.Literals.ACTION_INSTANCE__START_TIME, newStartTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getEndTime() {
		return (Date)eGet(ExecutionPackage.Literals.ACTION_INSTANCE__END_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(Date newEndTime) {
		eSet(ExecutionPackage.Literals.ACTION_INSTANCE__END_TIME, newEndTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSuccessful() {
		return (Boolean)eGet(ExecutionPackage.Literals.ACTION_INSTANCE__SUCCESSFUL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSuccessful(boolean newSuccessful) {
		eSet(ExecutionPackage.Literals.ACTION_INSTANCE__SUCCESSFUL, newSuccessful);
	}

} //ActionInstanceImpl
