/**
 */
package camel.execution.impl;

import camel.core.impl.FeatureImpl;

import camel.execution.ExecutionPackage;
import camel.execution.Measurement;

import camel.metric.MetricInstance;

import camel.scalability.EventInstance;

import java.util.Date;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Measurement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.MeasurementImpl#getMetricInstance <em>Metric Instance</em>}</li>
 *   <li>{@link camel.execution.impl.MeasurementImpl#getValue <em>Value</em>}</li>
 *   <li>{@link camel.execution.impl.MeasurementImpl#getMeasurementTime <em>Measurement Time</em>}</li>
 *   <li>{@link camel.execution.impl.MeasurementImpl#getEventInstance <em>Event Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class MeasurementImpl extends FeatureImpl implements Measurement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MeasurementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.MEASUREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricInstance getMetricInstance() {
		return (MetricInstance)eGet(ExecutionPackage.Literals.MEASUREMENT__METRIC_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetricInstance(MetricInstance newMetricInstance) {
		eSet(ExecutionPackage.Literals.MEASUREMENT__METRIC_INSTANCE, newMetricInstance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getValue() {
		return (Double)eGet(ExecutionPackage.Literals.MEASUREMENT__VALUE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValue(double newValue) {
		eSet(ExecutionPackage.Literals.MEASUREMENT__VALUE, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getMeasurementTime() {
		return (Date)eGet(ExecutionPackage.Literals.MEASUREMENT__MEASUREMENT_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeasurementTime(Date newMeasurementTime) {
		eSet(ExecutionPackage.Literals.MEASUREMENT__MEASUREMENT_TIME, newMeasurementTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EventInstance getEventInstance() {
		return (EventInstance)eGet(ExecutionPackage.Literals.MEASUREMENT__EVENT_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEventInstance(EventInstance newEventInstance) {
		eSet(ExecutionPackage.Literals.MEASUREMENT__EVENT_INSTANCE, newEventInstance);
	}

} //MeasurementImpl
