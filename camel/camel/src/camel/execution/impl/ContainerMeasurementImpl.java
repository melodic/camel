/**
 */
package camel.execution.impl;

import camel.deployment.ContainerInstance;

import camel.execution.ContainerMeasurement;
import camel.execution.ExecutionPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Container Measurement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.ContainerMeasurementImpl#getContainerInstance <em>Container Instance</em>}</li>
 *   <li>{@link camel.execution.impl.ContainerMeasurementImpl#getContainer <em>Container</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ContainerMeasurementImpl extends MeasurementImpl implements ContainerMeasurement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ContainerMeasurementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.CONTAINER_MEASUREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContainerInstance getContainerInstance() {
		return (ContainerInstance)eGet(ExecutionPackage.Literals.CONTAINER_MEASUREMENT__CONTAINER_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainerInstance(ContainerInstance newContainerInstance) {
		eSet(ExecutionPackage.Literals.CONTAINER_MEASUREMENT__CONTAINER_INSTANCE, newContainerInstance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public camel.deployment.Container getContainer() {
		return (camel.deployment.Container)eGet(ExecutionPackage.Literals.CONTAINER_MEASUREMENT__CONTAINER, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setContainer(camel.deployment.Container newContainer) {
		eSet(ExecutionPackage.Literals.CONTAINER_MEASUREMENT__CONTAINER, newContainer);
	}

} //ContainerMeasurementImpl
