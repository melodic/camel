/**
 */
package camel.execution.impl;

import camel.constraint.ConstraintPackage;

import camel.constraint.impl.ConstraintPackageImpl;

import camel.core.CorePackage;

import camel.core.impl.CorePackageImpl;

import camel.data.DataPackage;

import camel.data.impl.DataPackageImpl;

import camel.deployment.DeploymentPackage;

import camel.deployment.impl.DeploymentPackageImpl;

import camel.execution.ActionInstance;
import camel.execution.ApplicationMeasurement;
import camel.execution.Cause;
import camel.execution.CommunicationMeasurement;
import camel.execution.ContainerMeasurement;
import camel.execution.DataMeasurement;
import camel.execution.ExecutionFactory;
import camel.execution.ExecutionModel;
import camel.execution.ExecutionPackage;
import camel.execution.HistoryInfo;
import camel.execution.HistoryRecord;
import camel.execution.Measurement;
import camel.execution.PaaSMeasurement;
import camel.execution.RuleTrigger;
import camel.execution.SLOViolation;
import camel.execution.SoftwareComponentMeasurement;
import camel.execution.VMMeasurement;
import camel.execution.util.ExecutionValidator;
import camel.location.LocationPackage;

import camel.location.impl.LocationPackageImpl;

import camel.metric.MetricPackage;

import camel.metric.impl.MetricPackageImpl;

import camel.mms.MmsPackage;

import camel.mms.impl.MmsPackageImpl;

import camel.organisation.OrganisationPackage;

import camel.organisation.impl.OrganisationPackageImpl;

import camel.requirement.RequirementPackage;

import camel.requirement.impl.RequirementPackageImpl;

import camel.scalability.ScalabilityPackage;

import camel.scalability.impl.ScalabilityPackageImpl;

import camel.security.SecurityPackage;

import camel.security.impl.SecurityPackageImpl;

import camel.type.TypePackage;

import camel.type.impl.TypePackageImpl;

import camel.unit.UnitPackage;

import camel.unit.impl.UnitPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;
import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExecutionPackageImpl extends EPackageImpl implements ExecutionPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass executionModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass actionInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass measurementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass applicationMeasurementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass softwareComponentMeasurementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass communicationMeasurementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass vmMeasurementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass paaSMeasurementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass containerMeasurementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass dataMeasurementEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sloViolationEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass ruleTriggerEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass historyRecordEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass historyInfoEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass causeEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see camel.execution.ExecutionPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private ExecutionPackageImpl() {
		super(eNS_URI, ExecutionFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link ExecutionPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static ExecutionPackage init() {
		if (isInited) return (ExecutionPackage)EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI);

		// Obtain or create and register package
		ExecutionPackageImpl theExecutionPackage = (ExecutionPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof ExecutionPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new ExecutionPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		MmsPackageImpl theMmsPackage = (MmsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI) instanceof MmsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI) : MmsPackage.eINSTANCE);
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		DeploymentPackageImpl theDeploymentPackage = (DeploymentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI) instanceof DeploymentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI) : DeploymentPackage.eINSTANCE);
		LocationPackageImpl theLocationPackage = (LocationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) instanceof LocationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) : LocationPackage.eINSTANCE);
		MetricPackageImpl theMetricPackage = (MetricPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI) instanceof MetricPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI) : MetricPackage.eINSTANCE);
		OrganisationPackageImpl theOrganisationPackage = (OrganisationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) instanceof OrganisationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) : OrganisationPackage.eINSTANCE);
		RequirementPackageImpl theRequirementPackage = (RequirementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) instanceof RequirementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) : RequirementPackage.eINSTANCE);
		ScalabilityPackageImpl theScalabilityPackage = (ScalabilityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI) instanceof ScalabilityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI) : ScalabilityPackage.eINSTANCE);
		SecurityPackageImpl theSecurityPackage = (SecurityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) instanceof SecurityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) : SecurityPackage.eINSTANCE);
		TypePackageImpl theTypePackage = (TypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI) instanceof TypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI) : TypePackage.eINSTANCE);
		UnitPackageImpl theUnitPackage = (UnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) instanceof UnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) : UnitPackage.eINSTANCE);
		ConstraintPackageImpl theConstraintPackage = (ConstraintPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) instanceof ConstraintPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) : ConstraintPackage.eINSTANCE);
		DataPackageImpl theDataPackage = (DataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) instanceof DataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) : DataPackage.eINSTANCE);

		// Create package meta-data objects
		theExecutionPackage.createPackageContents();
		theMmsPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theDeploymentPackage.createPackageContents();
		theLocationPackage.createPackageContents();
		theMetricPackage.createPackageContents();
		theOrganisationPackage.createPackageContents();
		theRequirementPackage.createPackageContents();
		theScalabilityPackage.createPackageContents();
		theSecurityPackage.createPackageContents();
		theTypePackage.createPackageContents();
		theUnitPackage.createPackageContents();
		theConstraintPackage.createPackageContents();
		theDataPackage.createPackageContents();

		// Initialize created meta-data
		theExecutionPackage.initializePackageContents();
		theMmsPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theDeploymentPackage.initializePackageContents();
		theLocationPackage.initializePackageContents();
		theMetricPackage.initializePackageContents();
		theOrganisationPackage.initializePackageContents();
		theRequirementPackage.initializePackageContents();
		theScalabilityPackage.initializePackageContents();
		theSecurityPackage.initializePackageContents();
		theTypePackage.initializePackageContents();
		theUnitPackage.initializePackageContents();
		theConstraintPackage.initializePackageContents();
		theDataPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theExecutionPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return ExecutionValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theExecutionPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(ExecutionPackage.eNS_URI, theExecutionPackage);
		return theExecutionPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getExecutionModel() {
		return executionModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionModel_StartTime() {
		return (EAttribute)executionModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionModel_EndTime() {
		return (EAttribute)executionModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getExecutionModel_TotalCost() {
		return (EAttribute)executionModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionModel_CostUnit() {
		return (EReference)executionModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionModel_DeploymentTypeModel() {
		return (EReference)executionModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionModel_RequirementModel() {
		return (EReference)executionModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionModel_DataTypeModel() {
		return (EReference)executionModelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionModel_Measurements() {
		return (EReference)executionModelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionModel_SloViolations() {
		return (EReference)executionModelEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionModel_RuleTriggers() {
		return (EReference)executionModelEClass.getEStructuralFeatures().get(9);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getExecutionModel_HistoryRecords() {
		return (EReference)executionModelEClass.getEStructuralFeatures().get(10);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getActionInstance() {
		return actionInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getActionInstance_Action() {
		return (EReference)actionInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActionInstance_StartTime() {
		return (EAttribute)actionInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActionInstance_EndTime() {
		return (EAttribute)actionInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getActionInstance_Successful() {
		return (EAttribute)actionInstanceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMeasurement() {
		return measurementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMeasurement_MetricInstance() {
		return (EReference)measurementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMeasurement_Value() {
		return (EAttribute)measurementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMeasurement_MeasurementTime() {
		return (EAttribute)measurementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMeasurement_EventInstance() {
		return (EReference)measurementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getApplicationMeasurement() {
		return applicationMeasurementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSoftwareComponentMeasurement() {
		return softwareComponentMeasurementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponentMeasurement_SoftwareComponentInstance() {
		return (EReference)softwareComponentMeasurementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSoftwareComponentMeasurement_SoftwareComponent() {
		return (EReference)softwareComponentMeasurementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCommunicationMeasurement() {
		return communicationMeasurementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationMeasurement_SourceVMInstance() {
		return (EReference)communicationMeasurementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationMeasurement_TargetVMInstance() {
		return (EReference)communicationMeasurementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationMeasurement_SourceVM() {
		return (EReference)communicationMeasurementEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCommunicationMeasurement_TargetVM() {
		return (EReference)communicationMeasurementEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getVMMeasurement() {
		return vmMeasurementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVMMeasurement_VmInstance() {
		return (EReference)vmMeasurementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getVMMeasurement_Vm() {
		return (EReference)vmMeasurementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getPaaSMeasurement() {
		return paaSMeasurementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPaaSMeasurement_PaasInstance() {
		return (EReference)paaSMeasurementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getPaaSMeasurement_Paas() {
		return (EReference)paaSMeasurementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getContainerMeasurement() {
		return containerMeasurementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContainerMeasurement_ContainerInstance() {
		return (EReference)containerMeasurementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getContainerMeasurement_Container() {
		return (EReference)containerMeasurementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getDataMeasurement() {
		return dataMeasurementEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataMeasurement_DataInstance() {
		return (EReference)dataMeasurementEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getDataMeasurement_Data() {
		return (EReference)dataMeasurementEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSLOViolation() {
		return sloViolationEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSLOViolation_Slo() {
		return (EReference)sloViolationEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSLOViolation_Measurement() {
		return (EReference)sloViolationEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSLOViolation_AssessmentTime() {
		return (EAttribute)sloViolationEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRuleTrigger() {
		return ruleTriggerEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRuleTrigger_ScalabilityRule() {
		return (EReference)ruleTriggerEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getRuleTrigger_TrigerringTime() {
		return (EAttribute)ruleTriggerEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHistoryRecord() {
		return historyRecordEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryRecord_Type() {
		return (EReference)historyRecordEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryRecord_Cause() {
		return (EReference)historyRecordEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryRecord_Infos() {
		return (EReference)historyRecordEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHistoryRecord_StartTime() {
		return (EAttribute)historyRecordEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHistoryRecord_EndTime() {
		return (EAttribute)historyRecordEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryRecord_FromDeploymentInstanceModel() {
		return (EReference)historyRecordEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryRecord_ToDeploymentInstanceModel() {
		return (EReference)historyRecordEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryRecord_FromDataInstanceModel() {
		return (EReference)historyRecordEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryRecord_ToDataInstanceModel() {
		return (EReference)historyRecordEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getHistoryInfo() {
		return historyInfoEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryInfo_Subject() {
		return (EReference)historyInfoEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryInfo_Action() {
		return (EReference)historyInfoEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHistoryInfo_Object() {
		return (EAttribute)historyInfoEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getHistoryInfo_ObjectRef() {
		return (EReference)historyInfoEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHistoryInfo_StartTime() {
		return (EAttribute)historyInfoEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getHistoryInfo_EndTime() {
		return (EAttribute)historyInfoEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCause() {
		return causeEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCause_EventInstances() {
		return (EReference)causeEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionFactory getExecutionFactory() {
		return (ExecutionFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		executionModelEClass = createEClass(EXECUTION_MODEL);
		createEAttribute(executionModelEClass, EXECUTION_MODEL__START_TIME);
		createEAttribute(executionModelEClass, EXECUTION_MODEL__END_TIME);
		createEAttribute(executionModelEClass, EXECUTION_MODEL__TOTAL_COST);
		createEReference(executionModelEClass, EXECUTION_MODEL__COST_UNIT);
		createEReference(executionModelEClass, EXECUTION_MODEL__DEPLOYMENT_TYPE_MODEL);
		createEReference(executionModelEClass, EXECUTION_MODEL__REQUIREMENT_MODEL);
		createEReference(executionModelEClass, EXECUTION_MODEL__DATA_TYPE_MODEL);
		createEReference(executionModelEClass, EXECUTION_MODEL__MEASUREMENTS);
		createEReference(executionModelEClass, EXECUTION_MODEL__SLO_VIOLATIONS);
		createEReference(executionModelEClass, EXECUTION_MODEL__RULE_TRIGGERS);
		createEReference(executionModelEClass, EXECUTION_MODEL__HISTORY_RECORDS);

		actionInstanceEClass = createEClass(ACTION_INSTANCE);
		createEReference(actionInstanceEClass, ACTION_INSTANCE__ACTION);
		createEAttribute(actionInstanceEClass, ACTION_INSTANCE__START_TIME);
		createEAttribute(actionInstanceEClass, ACTION_INSTANCE__END_TIME);
		createEAttribute(actionInstanceEClass, ACTION_INSTANCE__SUCCESSFUL);

		measurementEClass = createEClass(MEASUREMENT);
		createEReference(measurementEClass, MEASUREMENT__METRIC_INSTANCE);
		createEAttribute(measurementEClass, MEASUREMENT__VALUE);
		createEAttribute(measurementEClass, MEASUREMENT__MEASUREMENT_TIME);
		createEReference(measurementEClass, MEASUREMENT__EVENT_INSTANCE);

		applicationMeasurementEClass = createEClass(APPLICATION_MEASUREMENT);

		softwareComponentMeasurementEClass = createEClass(SOFTWARE_COMPONENT_MEASUREMENT);
		createEReference(softwareComponentMeasurementEClass, SOFTWARE_COMPONENT_MEASUREMENT__SOFTWARE_COMPONENT_INSTANCE);
		createEReference(softwareComponentMeasurementEClass, SOFTWARE_COMPONENT_MEASUREMENT__SOFTWARE_COMPONENT);

		communicationMeasurementEClass = createEClass(COMMUNICATION_MEASUREMENT);
		createEReference(communicationMeasurementEClass, COMMUNICATION_MEASUREMENT__SOURCE_VM_INSTANCE);
		createEReference(communicationMeasurementEClass, COMMUNICATION_MEASUREMENT__TARGET_VM_INSTANCE);
		createEReference(communicationMeasurementEClass, COMMUNICATION_MEASUREMENT__SOURCE_VM);
		createEReference(communicationMeasurementEClass, COMMUNICATION_MEASUREMENT__TARGET_VM);

		vmMeasurementEClass = createEClass(VM_MEASUREMENT);
		createEReference(vmMeasurementEClass, VM_MEASUREMENT__VM_INSTANCE);
		createEReference(vmMeasurementEClass, VM_MEASUREMENT__VM);

		paaSMeasurementEClass = createEClass(PAA_SMEASUREMENT);
		createEReference(paaSMeasurementEClass, PAA_SMEASUREMENT__PAAS_INSTANCE);
		createEReference(paaSMeasurementEClass, PAA_SMEASUREMENT__PAAS);

		containerMeasurementEClass = createEClass(CONTAINER_MEASUREMENT);
		createEReference(containerMeasurementEClass, CONTAINER_MEASUREMENT__CONTAINER_INSTANCE);
		createEReference(containerMeasurementEClass, CONTAINER_MEASUREMENT__CONTAINER);

		dataMeasurementEClass = createEClass(DATA_MEASUREMENT);
		createEReference(dataMeasurementEClass, DATA_MEASUREMENT__DATA_INSTANCE);
		createEReference(dataMeasurementEClass, DATA_MEASUREMENT__DATA);

		sloViolationEClass = createEClass(SLO_VIOLATION);
		createEReference(sloViolationEClass, SLO_VIOLATION__SLO);
		createEReference(sloViolationEClass, SLO_VIOLATION__MEASUREMENT);
		createEAttribute(sloViolationEClass, SLO_VIOLATION__ASSESSMENT_TIME);

		ruleTriggerEClass = createEClass(RULE_TRIGGER);
		createEReference(ruleTriggerEClass, RULE_TRIGGER__SCALABILITY_RULE);
		createEAttribute(ruleTriggerEClass, RULE_TRIGGER__TRIGERRING_TIME);

		historyRecordEClass = createEClass(HISTORY_RECORD);
		createEReference(historyRecordEClass, HISTORY_RECORD__TYPE);
		createEReference(historyRecordEClass, HISTORY_RECORD__CAUSE);
		createEReference(historyRecordEClass, HISTORY_RECORD__INFOS);
		createEAttribute(historyRecordEClass, HISTORY_RECORD__START_TIME);
		createEAttribute(historyRecordEClass, HISTORY_RECORD__END_TIME);
		createEReference(historyRecordEClass, HISTORY_RECORD__FROM_DEPLOYMENT_INSTANCE_MODEL);
		createEReference(historyRecordEClass, HISTORY_RECORD__TO_DEPLOYMENT_INSTANCE_MODEL);
		createEReference(historyRecordEClass, HISTORY_RECORD__FROM_DATA_INSTANCE_MODEL);
		createEReference(historyRecordEClass, HISTORY_RECORD__TO_DATA_INSTANCE_MODEL);

		historyInfoEClass = createEClass(HISTORY_INFO);
		createEReference(historyInfoEClass, HISTORY_INFO__SUBJECT);
		createEReference(historyInfoEClass, HISTORY_INFO__ACTION);
		createEAttribute(historyInfoEClass, HISTORY_INFO__OBJECT);
		createEReference(historyInfoEClass, HISTORY_INFO__OBJECT_REF);
		createEAttribute(historyInfoEClass, HISTORY_INFO__START_TIME);
		createEAttribute(historyInfoEClass, HISTORY_INFO__END_TIME);

		causeEClass = createEClass(CAUSE);
		createEReference(causeEClass, CAUSE__EVENT_INSTANCES);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		UnitPackage theUnitPackage = (UnitPackage)EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI);
		DeploymentPackage theDeploymentPackage = (DeploymentPackage)EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI);
		RequirementPackage theRequirementPackage = (RequirementPackage)EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI);
		DataPackage theDataPackage = (DataPackage)EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI);
		MetricPackage theMetricPackage = (MetricPackage)EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI);
		ScalabilityPackage theScalabilityPackage = (ScalabilityPackage)EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI);
		MmsPackage theMmsPackage = (MmsPackage)EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		executionModelEClass.getESuperTypes().add(theCorePackage.getModel());
		actionInstanceEClass.getESuperTypes().add(theCorePackage.getFeature());
		measurementEClass.getESuperTypes().add(theCorePackage.getFeature());
		applicationMeasurementEClass.getESuperTypes().add(this.getMeasurement());
		softwareComponentMeasurementEClass.getESuperTypes().add(this.getMeasurement());
		communicationMeasurementEClass.getESuperTypes().add(this.getMeasurement());
		vmMeasurementEClass.getESuperTypes().add(this.getMeasurement());
		paaSMeasurementEClass.getESuperTypes().add(this.getMeasurement());
		containerMeasurementEClass.getESuperTypes().add(this.getMeasurement());
		dataMeasurementEClass.getESuperTypes().add(this.getMeasurement());
		sloViolationEClass.getESuperTypes().add(this.getCause());
		ruleTriggerEClass.getESuperTypes().add(this.getCause());
		historyRecordEClass.getESuperTypes().add(theCorePackage.getFeature());
		historyInfoEClass.getESuperTypes().add(theCorePackage.getFeature());
		causeEClass.getESuperTypes().add(theCorePackage.getFeature());

		// Initialize classes, features, and operations; add parameters
		initEClass(executionModelEClass, ExecutionModel.class, "ExecutionModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getExecutionModel_StartTime(), ecorePackage.getEDate(), "startTime", null, 0, 1, ExecutionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionModel_EndTime(), ecorePackage.getEDate(), "endTime", null, 0, 1, ExecutionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getExecutionModel_TotalCost(), ecorePackage.getEDouble(), "totalCost", null, 1, 1, ExecutionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExecutionModel_CostUnit(), theUnitPackage.getUnit(), null, "costUnit", null, 0, 1, ExecutionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExecutionModel_DeploymentTypeModel(), theDeploymentPackage.getDeploymentTypeModel(), null, "deploymentTypeModel", null, 1, 1, ExecutionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExecutionModel_RequirementModel(), theRequirementPackage.getRequirementModel(), null, "requirementModel", null, 1, 1, ExecutionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExecutionModel_DataTypeModel(), theDataPackage.getDataTypeModel(), null, "dataTypeModel", null, 0, 1, ExecutionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getExecutionModel_Measurements(), this.getMeasurement(), null, "measurements", null, 0, -1, ExecutionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getExecutionModel_SloViolations(), this.getSLOViolation(), null, "sloViolations", null, 0, -1, ExecutionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getExecutionModel_RuleTriggers(), this.getRuleTrigger(), null, "ruleTriggers", null, 0, -1, ExecutionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getExecutionModel_HistoryRecords(), this.getHistoryRecord(), null, "historyRecords", null, 0, -1, ExecutionModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(actionInstanceEClass, ActionInstance.class, "ActionInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getActionInstance_Action(), theCorePackage.getAction(), null, "action", null, 1, 1, ActionInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActionInstance_StartTime(), ecorePackage.getEDate(), "startTime", null, 0, 1, ActionInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActionInstance_EndTime(), ecorePackage.getEDate(), "endTime", null, 0, 1, ActionInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getActionInstance_Successful(), ecorePackage.getEBoolean(), "successful", null, 1, 1, ActionInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(measurementEClass, Measurement.class, "Measurement", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMeasurement_MetricInstance(), theMetricPackage.getMetricInstance(), null, "metricInstance", null, 1, 1, Measurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMeasurement_Value(), ecorePackage.getEDouble(), "value", null, 1, 1, Measurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMeasurement_MeasurementTime(), ecorePackage.getEDate(), "measurementTime", null, 1, 1, Measurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMeasurement_EventInstance(), theScalabilityPackage.getEventInstance(), null, "eventInstance", null, 0, 1, Measurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(applicationMeasurementEClass, ApplicationMeasurement.class, "ApplicationMeasurement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(softwareComponentMeasurementEClass, SoftwareComponentMeasurement.class, "SoftwareComponentMeasurement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSoftwareComponentMeasurement_SoftwareComponentInstance(), theDeploymentPackage.getSoftwareComponentInstance(), null, "softwareComponentInstance", null, 0, 1, SoftwareComponentMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSoftwareComponentMeasurement_SoftwareComponent(), theDeploymentPackage.getSoftwareComponent(), null, "softwareComponent", null, 0, 1, SoftwareComponentMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(communicationMeasurementEClass, CommunicationMeasurement.class, "CommunicationMeasurement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCommunicationMeasurement_SourceVMInstance(), theDeploymentPackage.getVMInstance(), null, "sourceVMInstance", null, 0, 1, CommunicationMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationMeasurement_TargetVMInstance(), theDeploymentPackage.getVMInstance(), null, "targetVMInstance", null, 0, 1, CommunicationMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationMeasurement_SourceVM(), theDeploymentPackage.getVM(), null, "sourceVM", null, 0, 1, CommunicationMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCommunicationMeasurement_TargetVM(), theDeploymentPackage.getVM(), null, "targetVM", null, 0, 1, CommunicationMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(vmMeasurementEClass, VMMeasurement.class, "VMMeasurement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getVMMeasurement_VmInstance(), theDeploymentPackage.getVMInstance(), null, "vmInstance", null, 0, 1, VMMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getVMMeasurement_Vm(), theDeploymentPackage.getVM(), null, "vm", null, 0, 1, VMMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(paaSMeasurementEClass, PaaSMeasurement.class, "PaaSMeasurement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getPaaSMeasurement_PaasInstance(), theDeploymentPackage.getPaaSInstance(), null, "paasInstance", null, 0, 1, PaaSMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getPaaSMeasurement_Paas(), theDeploymentPackage.getPaaS(), null, "paas", null, 0, 1, PaaSMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(containerMeasurementEClass, ContainerMeasurement.class, "ContainerMeasurement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getContainerMeasurement_ContainerInstance(), theDeploymentPackage.getContainerInstance(), null, "containerInstance", null, 0, 1, ContainerMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getContainerMeasurement_Container(), theDeploymentPackage.getContainer(), null, "container", null, 0, 1, ContainerMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(dataMeasurementEClass, DataMeasurement.class, "DataMeasurement", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getDataMeasurement_DataInstance(), theDataPackage.getDataInstance(), null, "dataInstance", null, 0, 1, DataMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getDataMeasurement_Data(), theDataPackage.getData(), null, "data", null, 0, 1, DataMeasurement.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sloViolationEClass, SLOViolation.class, "SLOViolation", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getSLOViolation_Slo(), theRequirementPackage.getServiceLevelObjective(), null, "slo", null, 1, 1, SLOViolation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSLOViolation_Measurement(), this.getMeasurement(), null, "measurement", null, 1, 1, SLOViolation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSLOViolation_AssessmentTime(), ecorePackage.getEDate(), "assessmentTime", null, 1, 1, SLOViolation.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(ruleTriggerEClass, RuleTrigger.class, "RuleTrigger", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRuleTrigger_ScalabilityRule(), theScalabilityPackage.getScalabilityRule(), null, "scalabilityRule", null, 1, 1, RuleTrigger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getRuleTrigger_TrigerringTime(), ecorePackage.getEDate(), "trigerringTime", null, 1, 1, RuleTrigger.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(historyRecordEClass, HistoryRecord.class, "HistoryRecord", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHistoryRecord_Type(), theMmsPackage.getMmsObject(), null, "type", null, 1, 1, HistoryRecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHistoryRecord_Cause(), this.getCause(), null, "cause", null, 0, 1, HistoryRecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHistoryRecord_Infos(), this.getHistoryInfo(), null, "infos", null, 0, -1, HistoryRecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHistoryRecord_StartTime(), ecorePackage.getEDate(), "startTime", null, 1, 1, HistoryRecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHistoryRecord_EndTime(), ecorePackage.getEDate(), "endTime", null, 1, 1, HistoryRecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHistoryRecord_FromDeploymentInstanceModel(), theDeploymentPackage.getDeploymentInstanceModel(), null, "fromDeploymentInstanceModel", null, 0, 1, HistoryRecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHistoryRecord_ToDeploymentInstanceModel(), theDeploymentPackage.getDeploymentInstanceModel(), null, "toDeploymentInstanceModel", null, 0, 1, HistoryRecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHistoryRecord_FromDataInstanceModel(), theDataPackage.getDataInstanceModel(), null, "fromDataInstanceModel", null, 0, 1, HistoryRecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHistoryRecord_ToDataInstanceModel(), theDataPackage.getDataInstanceModel(), null, "toDataInstanceModel", null, 0, 1, HistoryRecord.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(historyInfoEClass, HistoryInfo.class, "HistoryInfo", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getHistoryInfo_Subject(), theMmsPackage.getMmsObject(), null, "subject", null, 1, 1, HistoryInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHistoryInfo_Action(), theMmsPackage.getMmsObject(), null, "action", null, 1, 1, HistoryInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHistoryInfo_Object(), ecorePackage.getEString(), "object", null, 0, 1, HistoryInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getHistoryInfo_ObjectRef(), ecorePackage.getEObject(), null, "objectRef", null, 0, 1, HistoryInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHistoryInfo_StartTime(), ecorePackage.getEDate(), "startTime", null, 1, 1, HistoryInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getHistoryInfo_EndTime(), ecorePackage.getEDate(), "endTime", null, 1, 1, HistoryInfo.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(causeEClass, Cause.class, "Cause", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getCause_EventInstances(), theScalabilityPackage.getEventInstance(), null, "eventInstances", null, 0, -1, Cause.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
		// invariant
		createInvariantAnnotations();
		// null
		createNullAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
		   });	
		addAnnotation
		  (measurementEClass, 
		   source, 
		   new String[] {
			 "constraints", "measurement_event_instance_same_metric correct_measurement_value measurement_metric_refers_to_correct_execution_model"
		   });	
		addAnnotation
		  (softwareComponentMeasurementEClass, 
		   source, 
		   new String[] {
			 "constraints", "correct_sw_component_measurement"
		   });	
		addAnnotation
		  (communicationMeasurementEClass, 
		   source, 
		   new String[] {
			 "constraints", "correct_communication_measurement communication_meas_diff_source_target"
		   });	
		addAnnotation
		  (vmMeasurementEClass, 
		   source, 
		   new String[] {
			 "constraints", "correct_vm_measurement"
		   });	
		addAnnotation
		  (paaSMeasurementEClass, 
		   source, 
		   new String[] {
			 "constraints", "correct_paas_measurement"
		   });	
		addAnnotation
		  (containerMeasurementEClass, 
		   source, 
		   new String[] {
			 "constraints", "correct_container_measurement"
		   });	
		addAnnotation
		  (dataMeasurementEClass, 
		   source, 
		   new String[] {
			 "constraints", "correct_data_measurement"
		   });	
		addAnnotation
		  (sloViolationEClass, 
		   source, 
		   new String[] {
			 "constraints", "slo_violation_same_metric slo_violation_slo_in_reqs_for_execution_model"
		   });	
		addAnnotation
		  (historyRecordEClass, 
		   source, 
		   new String[] {
			 "constraints", "history_record_from_to_diff history_record_correct_instance_model"
		   });	
		addAnnotation
		  (historyInfoEClass, 
		   source, 
		   new String[] {
			 "constraints", "at_least_object_or_objectRef_in_history_info"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";	
		addAnnotation
		  (measurementEClass, 
		   source, 
		   new String[] {
			 "measurement_event_instance_same_metric", "Tuple {\n\tmessage : String = \'Measurement: \' + self.name + \' maps to non-functional event instance : \' + eventInstance.toString() + \' with a type related to a metric different from the one of this measurement\',\n\tstatus : Boolean = \n\t\t\tasError(self.eventInstance <> null and\n\t\t\t\tself.eventInstance.type.oclIsTypeOf(camel::scalability::NonFunctionalEvent) implies self.metricInstance.metricContext.metric =\n\t\t\t\tself.eventInstance.type.oclAsType(camel::scalability::NonFunctionalEvent).metricConstraint.metricContext.metric)\n}.status",
			 "correct_measurement_value", "Tuple {\n\tmessage : String = \'Measurement: \' + self.name + \' has a value: \' + value.toString() + \' that is outside the range of values of metric instance: \' + metricInstance.name,\n\tstatus : Boolean = \n\t\t\tasError(\n\t\t\tlet type :\n\t\t\t\tcamel::type::ValueType = metricInstance.metricContext.metric.metricTemplate.valueType\n\t\t\tin if (type <> null)\n\t\t\t\tthen if (type.oclIsTypeOf(camel::type::Range))\n\t\t\t\t\tthen type.oclAsType(camel::type::Range).includesValue(self.value)\n\t\t\t\t\telse if (type.oclIsTypeOf(camel::type::RangeUnion))\n\t\t\t\t\t\tthen type.oclAsType(camel::type::RangeUnion).includesValue(self.value)\n\t\t\t\t\t\telse true\n\t\t\t\t\t\tendif\n\t\t\t\t\tendif\n\t\t\t\telse true\n\t\t\t\tendif)\n}.status",
			 "measurement_metric_refers_to_correct_execution_model", "Tuple {\n\tmessage : String = \'Measurement: \' + self.name + \' does not belong to the same execution model as the one of the binding of the respective metric instance:\' + metricInstance.name,\n\tstatus : Boolean = \n\t\t\t\tasError(self.oclContainer().oclAsType(ExecutionModel) = metricInstance.objectBinding.executionModel)\n}.status"
		   });	
		addAnnotation
		  (softwareComponentMeasurementEClass, 
		   source, 
		   new String[] {
			 "correct_sw_component_measurement", "Tuple {\n\tmessage : String = \'SoftwareComponentMeasurement: \' + self.name + \' should either map to an instance of a software component or the software component itself\',\n\tstatus : Boolean = \n\t\t\t\tasError((softwareComponentInstance <> null) xor (softwareComponent <> null))\n}.status"
		   });	
		addAnnotation
		  (communicationMeasurementEClass, 
		   source, 
		   new String[] {
			 "correct_communication_measurement", "Tuple {\n\tmessage : String = \'CommunicationMeasurement: \' + self.name + \' should either have a source and target VM instance or a source and target VM\',\n\tstatus : Boolean = \n\t\t\t \tasError((sourceVMInstance <> null and targetVMInstance <> null and sourceVM = null and targetVM = null) xor \n\t\t\t \t\t(sourceVM <> null and targetVM <> null and sourceVMInstance = null and targetVMInstance = null)\n\t\t\t \t)\n}.status",
			 "communication_meas_diff_source_target", "Tuple {\n\tmessage : String = \'In CommunicationMeasurement: \' + self.name + \' source VM should be different from target VM and the same should hold for the instance level\',\n\tstatus : Boolean = \n\t\t\t \tasError((sourceVM <> null and targetVM <> null implies sourceVM <> targetVM) and \n\t\t\t \t(sourceVMInstance <> null and targetVMInstance <> null implies sourceVMInstance <> targetVMInstance))\n}.status"
		   });	
		addAnnotation
		  (vmMeasurementEClass, 
		   source, 
		   new String[] {
			 "correct_vm_measurement", "Tuple {\n\tmessage : String = \'VMMeasurement: \' + self.name + \' should either map to an instance of a VM or the VM itself\',\n\tstatus : Boolean = \n\t\t\tasError((vmInstance <> null) xor (vm <> null))\n}.status"
		   });	
		addAnnotation
		  (paaSMeasurementEClass, 
		   source, 
		   new String[] {
			 "correct_paas_measurement", "Tuple {\n\tmessage : String = \'PaaSMeasurement: \' + self.name + \' should either map to an instance of a PaaS or the PaaS itself\',\n\tstatus : Boolean = \n\t\t\tasError((paasInstance <> null) xor (paas <> null))\n}.status"
		   });	
		addAnnotation
		  (containerMeasurementEClass, 
		   source, 
		   new String[] {
			 "correct_container_measurement", "Tuple {\n\tmessage : String = \'ContainerMeasurement: \' + self.name + \' should either map to an instance of a container or the container itself\',\n\tstatus : Boolean = \n\t\t\tasError((containerInstance <> null) xor (container <> null))\n}.status"
		   });	
		addAnnotation
		  (dataMeasurementEClass, 
		   source, 
		   new String[] {
			 "correct_data_measurement", "Tuple {\n\tmessage : String = \'DataMeasurement: \' + self.name + \' should either map to an instance of data or the data itself\',\n\tstatus : Boolean = \n\t\t\tasError((dataInstance <> null) xor (data <> null))\n}.status"
		   });	
		addAnnotation
		  (sloViolationEClass, 
		   source, 
		   new String[] {
			 "slo_violation_same_metric", "Tuple {\n\tmessage : String = \'The metric in slo: \' + slo.name + \' of SLOViolation: \' + self.name + \' is not the same as the one referenced by the respective measurement: \' + measurement.name,\n\tstatus : Boolean = \n\t\t\tasError(\n\t\t\t\tif (slo.constraint.oclIsTypeOf(camel::constraint::MetricConstraint)\n\t\t\t\t\t)\n\t\t\t\tthen \n\t\t\t\t\tmeasurement.metricInstance.metricContext.metric = slo.constraint.oclAsType(camel::constraint::MetricConstraint).metricContext.metric\n\t\t\t\telse false\n\t\t\t\tendif)\n}.status",
			 "slo_violation_slo_in_reqs_for_execution_model", "Tuple {\n\tmessage : String = \'SLO: \' + self.slo.name + \' is not included in the requirement model of the current execution model: \' + self.oclContainer().oclAsType(ExecutionModel).name + \' of SLO Violation: \' + self.name,\n\tstatus : Boolean = \n\t\t\t\tasError(self.oclContainer().oclAsType(ExecutionModel).requirementModel.requirements\n\t\t\t\t->includes(self.slo))\n}.status"
		   });	
		addAnnotation
		  (historyRecordEClass, 
		   source, 
		   new String[] {
			 "history_record_from_to_diff", "Tuple {\n\tmessage : String = \'HistoryRecord: \' + self.name + \' has from and to instance models which are identical\'\n\t\t\t,\n\tstatus : Boolean = \n\t\t\t\tasError((fromDeploymentInstanceModel <> null and toDeploymentInstanceModel <> null implies fromDeploymentInstanceModel <> toDeploymentInstanceModel)\n\t\t\t\tand \n\t\t\t\t(fromDataInstanceModel <> null and toDataInstanceModel <> null implies fromDataInstanceModel <> toDataInstanceModel))\n}.status",
			 "history_record_correct_instance_model", "Tuple {\n\tmessage : String = \'HistoryRecord: \' + self.name + \' should have all non-null instance models correct, i.e., instance models with a type that belongs to the encompassing execution model\',\n\tstatus : Boolean = \n\t\t\t\tasError(let em: ExecutionModel = self.oclContainer().oclAsType(ExecutionModel) in \n\t\t\t\t\t(fromDeploymentInstanceModel <> null implies fromDeploymentInstanceModel.type = em.deploymentTypeModel) and\n\t\t\t\t\t(toDeploymentInstanceModel <> null implies toDeploymentInstanceModel.type = em.deploymentTypeModel) and\n\t\t\t\t\t(fromDataInstanceModel <> null implies fromDataInstanceModel.type = em.dataTypeModel) and\n\t\t\t\t\t(toDataInstanceModel <> null implies toDataInstanceModel.type = em.dataTypeModel))\n}.status"
		   });	
		addAnnotation
		  (historyInfoEClass, 
		   source, 
		   new String[] {
			 "at_least_object_or_objectRef_in_history_info", "Tuple {\n\tmessage : String = \'HistoryInfo: \' + self.name + \' should either to a certain model or to a String-based object in case a certain message needs to be recorded (e.g., error)\',\n\tstatus : Boolean = \n\t\t\t\tasError((object <> null and objectRef = null) or (object = null and objectRef <> null))\n}.status"
		   });
	}

	/**
	 * Initializes the annotations for <b>invariant</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createInvariantAnnotations() {
		String source = "invariant";	
		addAnnotation
		  (communicationMeasurementEClass, 
		   source, 
		   new String[] {
			 "to do", "if containers not inside VMs always then communication should also refer to them"
		   });
	}

	/**
	 * Initializes the annotations for <b>null</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createNullAnnotations() {
		String source = null;	
		addAnnotation
		  (sloViolationEClass, 
		   source, 
		   new String[] {
			 "to do", "check how composite constraints in slos are handled"
		   });
	}

} //ExecutionPackageImpl
