/**
 */
package camel.execution.impl;

import camel.execution.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class ExecutionFactoryImpl extends EFactoryImpl implements ExecutionFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static ExecutionFactory init() {
		try {
			ExecutionFactory theExecutionFactory = (ExecutionFactory)EPackage.Registry.INSTANCE.getEFactory(ExecutionPackage.eNS_URI);
			if (theExecutionFactory != null) {
				return theExecutionFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new ExecutionFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case ExecutionPackage.EXECUTION_MODEL: return (EObject)createExecutionModel();
			case ExecutionPackage.ACTION_INSTANCE: return (EObject)createActionInstance();
			case ExecutionPackage.APPLICATION_MEASUREMENT: return (EObject)createApplicationMeasurement();
			case ExecutionPackage.SOFTWARE_COMPONENT_MEASUREMENT: return (EObject)createSoftwareComponentMeasurement();
			case ExecutionPackage.COMMUNICATION_MEASUREMENT: return (EObject)createCommunicationMeasurement();
			case ExecutionPackage.VM_MEASUREMENT: return (EObject)createVMMeasurement();
			case ExecutionPackage.PAA_SMEASUREMENT: return (EObject)createPaaSMeasurement();
			case ExecutionPackage.CONTAINER_MEASUREMENT: return (EObject)createContainerMeasurement();
			case ExecutionPackage.DATA_MEASUREMENT: return (EObject)createDataMeasurement();
			case ExecutionPackage.SLO_VIOLATION: return (EObject)createSLOViolation();
			case ExecutionPackage.RULE_TRIGGER: return (EObject)createRuleTrigger();
			case ExecutionPackage.HISTORY_RECORD: return (EObject)createHistoryRecord();
			case ExecutionPackage.HISTORY_INFO: return (EObject)createHistoryInfo();
			case ExecutionPackage.CAUSE: return (EObject)createCause();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionModel createExecutionModel() {
		ExecutionModelImpl executionModel = new ExecutionModelImpl();
		return executionModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ActionInstance createActionInstance() {
		ActionInstanceImpl actionInstance = new ActionInstanceImpl();
		return actionInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ApplicationMeasurement createApplicationMeasurement() {
		ApplicationMeasurementImpl applicationMeasurement = new ApplicationMeasurementImpl();
		return applicationMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareComponentMeasurement createSoftwareComponentMeasurement() {
		SoftwareComponentMeasurementImpl softwareComponentMeasurement = new SoftwareComponentMeasurementImpl();
		return softwareComponentMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CommunicationMeasurement createCommunicationMeasurement() {
		CommunicationMeasurementImpl communicationMeasurement = new CommunicationMeasurementImpl();
		return communicationMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VMMeasurement createVMMeasurement() {
		VMMeasurementImpl vmMeasurement = new VMMeasurementImpl();
		return vmMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaaSMeasurement createPaaSMeasurement() {
		PaaSMeasurementImpl paaSMeasurement = new PaaSMeasurementImpl();
		return paaSMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ContainerMeasurement createContainerMeasurement() {
		ContainerMeasurementImpl containerMeasurement = new ContainerMeasurementImpl();
		return containerMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataMeasurement createDataMeasurement() {
		DataMeasurementImpl dataMeasurement = new DataMeasurementImpl();
		return dataMeasurement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SLOViolation createSLOViolation() {
		SLOViolationImpl sloViolation = new SLOViolationImpl();
		return sloViolation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RuleTrigger createRuleTrigger() {
		RuleTriggerImpl ruleTrigger = new RuleTriggerImpl();
		return ruleTrigger;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryRecord createHistoryRecord() {
		HistoryRecordImpl historyRecord = new HistoryRecordImpl();
		return historyRecord;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HistoryInfo createHistoryInfo() {
		HistoryInfoImpl historyInfo = new HistoryInfoImpl();
		return historyInfo;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cause createCause() {
		CauseImpl cause = new CauseImpl();
		return cause;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionPackage getExecutionPackage() {
		return (ExecutionPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static ExecutionPackage getPackage() {
		return ExecutionPackage.eINSTANCE;
	}

} //ExecutionFactoryImpl
