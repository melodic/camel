/**
 */
package camel.execution.impl;

import camel.data.Data;
import camel.data.DataInstance;

import camel.execution.DataMeasurement;
import camel.execution.ExecutionPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Measurement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.DataMeasurementImpl#getDataInstance <em>Data Instance</em>}</li>
 *   <li>{@link camel.execution.impl.DataMeasurementImpl#getData <em>Data</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataMeasurementImpl extends MeasurementImpl implements DataMeasurement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataMeasurementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.DATA_MEASUREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInstance getDataInstance() {
		return (DataInstance)eGet(ExecutionPackage.Literals.DATA_MEASUREMENT__DATA_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataInstance(DataInstance newDataInstance) {
		eSet(ExecutionPackage.Literals.DATA_MEASUREMENT__DATA_INSTANCE, newDataInstance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Data getData() {
		return (Data)eGet(ExecutionPackage.Literals.DATA_MEASUREMENT__DATA, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setData(Data newData) {
		eSet(ExecutionPackage.Literals.DATA_MEASUREMENT__DATA, newData);
	}

} //DataMeasurementImpl
