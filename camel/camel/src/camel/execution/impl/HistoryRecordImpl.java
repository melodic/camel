/**
 */
package camel.execution.impl;

import camel.core.impl.FeatureImpl;
import camel.data.DataInstanceModel;
import camel.deployment.DeploymentInstanceModel;

import camel.execution.Cause;
import camel.execution.ExecutionPackage;
import camel.execution.HistoryInfo;
import camel.execution.HistoryRecord;

import camel.mms.MmsObject;
import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>History Record</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.HistoryRecordImpl#getType <em>Type</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryRecordImpl#getCause <em>Cause</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryRecordImpl#getInfos <em>Infos</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryRecordImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryRecordImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryRecordImpl#getFromDeploymentInstanceModel <em>From Deployment Instance Model</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryRecordImpl#getToDeploymentInstanceModel <em>To Deployment Instance Model</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryRecordImpl#getFromDataInstanceModel <em>From Data Instance Model</em>}</li>
 *   <li>{@link camel.execution.impl.HistoryRecordImpl#getToDataInstanceModel <em>To Data Instance Model</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HistoryRecordImpl extends FeatureImpl implements HistoryRecord {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HistoryRecordImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.HISTORY_RECORD;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MmsObject getType() {
		return (MmsObject)eGet(ExecutionPackage.Literals.HISTORY_RECORD__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(MmsObject newType) {
		eSet(ExecutionPackage.Literals.HISTORY_RECORD__TYPE, newType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Cause getCause() {
		return (Cause)eGet(ExecutionPackage.Literals.HISTORY_RECORD__CAUSE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCause(Cause newCause) {
		eSet(ExecutionPackage.Literals.HISTORY_RECORD__CAUSE, newCause);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<HistoryInfo> getInfos() {
		return (EList<HistoryInfo>)eGet(ExecutionPackage.Literals.HISTORY_RECORD__INFOS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getStartTime() {
		return (Date)eGet(ExecutionPackage.Literals.HISTORY_RECORD__START_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(Date newStartTime) {
		eSet(ExecutionPackage.Literals.HISTORY_RECORD__START_TIME, newStartTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getEndTime() {
		return (Date)eGet(ExecutionPackage.Literals.HISTORY_RECORD__END_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(Date newEndTime) {
		eSet(ExecutionPackage.Literals.HISTORY_RECORD__END_TIME, newEndTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentInstanceModel getFromDeploymentInstanceModel() {
		return (DeploymentInstanceModel)eGet(ExecutionPackage.Literals.HISTORY_RECORD__FROM_DEPLOYMENT_INSTANCE_MODEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromDeploymentInstanceModel(DeploymentInstanceModel newFromDeploymentInstanceModel) {
		eSet(ExecutionPackage.Literals.HISTORY_RECORD__FROM_DEPLOYMENT_INSTANCE_MODEL, newFromDeploymentInstanceModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentInstanceModel getToDeploymentInstanceModel() {
		return (DeploymentInstanceModel)eGet(ExecutionPackage.Literals.HISTORY_RECORD__TO_DEPLOYMENT_INSTANCE_MODEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToDeploymentInstanceModel(DeploymentInstanceModel newToDeploymentInstanceModel) {
		eSet(ExecutionPackage.Literals.HISTORY_RECORD__TO_DEPLOYMENT_INSTANCE_MODEL, newToDeploymentInstanceModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInstanceModel getFromDataInstanceModel() {
		return (DataInstanceModel)eGet(ExecutionPackage.Literals.HISTORY_RECORD__FROM_DATA_INSTANCE_MODEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFromDataInstanceModel(DataInstanceModel newFromDataInstanceModel) {
		eSet(ExecutionPackage.Literals.HISTORY_RECORD__FROM_DATA_INSTANCE_MODEL, newFromDataInstanceModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataInstanceModel getToDataInstanceModel() {
		return (DataInstanceModel)eGet(ExecutionPackage.Literals.HISTORY_RECORD__TO_DATA_INSTANCE_MODEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setToDataInstanceModel(DataInstanceModel newToDataInstanceModel) {
		eSet(ExecutionPackage.Literals.HISTORY_RECORD__TO_DATA_INSTANCE_MODEL, newToDataInstanceModel);
	}

} //HistoryRecordImpl
