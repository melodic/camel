/**
 */
package camel.execution.impl;

import camel.execution.ApplicationMeasurement;
import camel.execution.ExecutionPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Application Measurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ApplicationMeasurementImpl extends MeasurementImpl implements ApplicationMeasurement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ApplicationMeasurementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.APPLICATION_MEASUREMENT;
	}

} //ApplicationMeasurementImpl
