/**
 */
package camel.execution.impl;

import camel.execution.ExecutionPackage;
import camel.execution.Measurement;
import camel.execution.SLOViolation;

import camel.requirement.ServiceLevelObjective;

import java.util.Date;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SLO Violation</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.SLOViolationImpl#getSlo <em>Slo</em>}</li>
 *   <li>{@link camel.execution.impl.SLOViolationImpl#getMeasurement <em>Measurement</em>}</li>
 *   <li>{@link camel.execution.impl.SLOViolationImpl#getAssessmentTime <em>Assessment Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SLOViolationImpl extends CauseImpl implements SLOViolation {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SLOViolationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.SLO_VIOLATION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceLevelObjective getSlo() {
		return (ServiceLevelObjective)eGet(ExecutionPackage.Literals.SLO_VIOLATION__SLO, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSlo(ServiceLevelObjective newSlo) {
		eSet(ExecutionPackage.Literals.SLO_VIOLATION__SLO, newSlo);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Measurement getMeasurement() {
		return (Measurement)eGet(ExecutionPackage.Literals.SLO_VIOLATION__MEASUREMENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeasurement(Measurement newMeasurement) {
		eSet(ExecutionPackage.Literals.SLO_VIOLATION__MEASUREMENT, newMeasurement);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getAssessmentTime() {
		return (Date)eGet(ExecutionPackage.Literals.SLO_VIOLATION__ASSESSMENT_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAssessmentTime(Date newAssessmentTime) {
		eSet(ExecutionPackage.Literals.SLO_VIOLATION__ASSESSMENT_TIME, newAssessmentTime);
	}

} //SLOViolationImpl
