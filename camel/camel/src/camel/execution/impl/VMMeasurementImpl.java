/**
 */
package camel.execution.impl;

import camel.deployment.VM;
import camel.deployment.VMInstance;

import camel.execution.ExecutionPackage;
import camel.execution.VMMeasurement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>VM Measurement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.VMMeasurementImpl#getVmInstance <em>Vm Instance</em>}</li>
 *   <li>{@link camel.execution.impl.VMMeasurementImpl#getVm <em>Vm</em>}</li>
 * </ul>
 *
 * @generated
 */
public class VMMeasurementImpl extends MeasurementImpl implements VMMeasurement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VMMeasurementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.VM_MEASUREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VMInstance getVmInstance() {
		return (VMInstance)eGet(ExecutionPackage.Literals.VM_MEASUREMENT__VM_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVmInstance(VMInstance newVmInstance) {
		eSet(ExecutionPackage.Literals.VM_MEASUREMENT__VM_INSTANCE, newVmInstance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VM getVm() {
		return (VM)eGet(ExecutionPackage.Literals.VM_MEASUREMENT__VM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setVm(VM newVm) {
		eSet(ExecutionPackage.Literals.VM_MEASUREMENT__VM, newVm);
	}

} //VMMeasurementImpl
