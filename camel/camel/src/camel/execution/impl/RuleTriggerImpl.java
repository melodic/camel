/**
 */
package camel.execution.impl;

import camel.execution.ExecutionPackage;
import camel.execution.RuleTrigger;

import camel.scalability.ScalabilityRule;

import java.util.Date;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Rule Trigger</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.RuleTriggerImpl#getScalabilityRule <em>Scalability Rule</em>}</li>
 *   <li>{@link camel.execution.impl.RuleTriggerImpl#getTrigerringTime <em>Trigerring Time</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RuleTriggerImpl extends CauseImpl implements RuleTrigger {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RuleTriggerImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.RULE_TRIGGER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ScalabilityRule getScalabilityRule() {
		return (ScalabilityRule)eGet(ExecutionPackage.Literals.RULE_TRIGGER__SCALABILITY_RULE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setScalabilityRule(ScalabilityRule newScalabilityRule) {
		eSet(ExecutionPackage.Literals.RULE_TRIGGER__SCALABILITY_RULE, newScalabilityRule);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getTrigerringTime() {
		return (Date)eGet(ExecutionPackage.Literals.RULE_TRIGGER__TRIGERRING_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTrigerringTime(Date newTrigerringTime) {
		eSet(ExecutionPackage.Literals.RULE_TRIGGER__TRIGERRING_TIME, newTrigerringTime);
	}

} //RuleTriggerImpl
