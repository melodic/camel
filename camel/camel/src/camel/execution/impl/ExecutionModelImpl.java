/**
 */
package camel.execution.impl;

import camel.core.impl.ModelImpl;
import camel.data.DataTypeModel;
import camel.deployment.DeploymentTypeModel;

import camel.execution.ExecutionModel;
import camel.execution.ExecutionPackage;
import camel.execution.HistoryRecord;
import camel.execution.Measurement;
import camel.execution.RuleTrigger;
import camel.execution.SLOViolation;

import camel.requirement.RequirementModel;

import camel.unit.Unit;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.ExecutionModelImpl#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link camel.execution.impl.ExecutionModelImpl#getEndTime <em>End Time</em>}</li>
 *   <li>{@link camel.execution.impl.ExecutionModelImpl#getTotalCost <em>Total Cost</em>}</li>
 *   <li>{@link camel.execution.impl.ExecutionModelImpl#getCostUnit <em>Cost Unit</em>}</li>
 *   <li>{@link camel.execution.impl.ExecutionModelImpl#getDeploymentTypeModel <em>Deployment Type Model</em>}</li>
 *   <li>{@link camel.execution.impl.ExecutionModelImpl#getRequirementModel <em>Requirement Model</em>}</li>
 *   <li>{@link camel.execution.impl.ExecutionModelImpl#getDataTypeModel <em>Data Type Model</em>}</li>
 *   <li>{@link camel.execution.impl.ExecutionModelImpl#getMeasurements <em>Measurements</em>}</li>
 *   <li>{@link camel.execution.impl.ExecutionModelImpl#getSloViolations <em>Slo Violations</em>}</li>
 *   <li>{@link camel.execution.impl.ExecutionModelImpl#getRuleTriggers <em>Rule Triggers</em>}</li>
 *   <li>{@link camel.execution.impl.ExecutionModelImpl#getHistoryRecords <em>History Records</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ExecutionModelImpl extends ModelImpl implements ExecutionModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecutionModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.EXECUTION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getStartTime() {
		return (Date)eGet(ExecutionPackage.Literals.EXECUTION_MODEL__START_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStartTime(Date newStartTime) {
		eSet(ExecutionPackage.Literals.EXECUTION_MODEL__START_TIME, newStartTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getEndTime() {
		return (Date)eGet(ExecutionPackage.Literals.EXECUTION_MODEL__END_TIME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEndTime(Date newEndTime) {
		eSet(ExecutionPackage.Literals.EXECUTION_MODEL__END_TIME, newEndTime);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public double getTotalCost() {
		return (Double)eGet(ExecutionPackage.Literals.EXECUTION_MODEL__TOTAL_COST, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTotalCost(double newTotalCost) {
		eSet(ExecutionPackage.Literals.EXECUTION_MODEL__TOTAL_COST, newTotalCost);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Unit getCostUnit() {
		return (Unit)eGet(ExecutionPackage.Literals.EXECUTION_MODEL__COST_UNIT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCostUnit(Unit newCostUnit) {
		eSet(ExecutionPackage.Literals.EXECUTION_MODEL__COST_UNIT, newCostUnit);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DeploymentTypeModel getDeploymentTypeModel() {
		return (DeploymentTypeModel)eGet(ExecutionPackage.Literals.EXECUTION_MODEL__DEPLOYMENT_TYPE_MODEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDeploymentTypeModel(DeploymentTypeModel newDeploymentTypeModel) {
		eSet(ExecutionPackage.Literals.EXECUTION_MODEL__DEPLOYMENT_TYPE_MODEL, newDeploymentTypeModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementModel getRequirementModel() {
		return (RequirementModel)eGet(ExecutionPackage.Literals.EXECUTION_MODEL__REQUIREMENT_MODEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRequirementModel(RequirementModel newRequirementModel) {
		eSet(ExecutionPackage.Literals.EXECUTION_MODEL__REQUIREMENT_MODEL, newRequirementModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public DataTypeModel getDataTypeModel() {
		return (DataTypeModel)eGet(ExecutionPackage.Literals.EXECUTION_MODEL__DATA_TYPE_MODEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDataTypeModel(DataTypeModel newDataTypeModel) {
		eSet(ExecutionPackage.Literals.EXECUTION_MODEL__DATA_TYPE_MODEL, newDataTypeModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Measurement> getMeasurements() {
		return (EList<Measurement>)eGet(ExecutionPackage.Literals.EXECUTION_MODEL__MEASUREMENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SLOViolation> getSloViolations() {
		return (EList<SLOViolation>)eGet(ExecutionPackage.Literals.EXECUTION_MODEL__SLO_VIOLATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RuleTrigger> getRuleTriggers() {
		return (EList<RuleTrigger>)eGet(ExecutionPackage.Literals.EXECUTION_MODEL__RULE_TRIGGERS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<HistoryRecord> getHistoryRecords() {
		return (EList<HistoryRecord>)eGet(ExecutionPackage.Literals.EXECUTION_MODEL__HISTORY_RECORDS, true);
	}

} //ExecutionModelImpl
