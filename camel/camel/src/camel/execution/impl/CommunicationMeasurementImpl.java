/**
 */
package camel.execution.impl;

import camel.deployment.VM;
import camel.deployment.VMInstance;

import camel.execution.CommunicationMeasurement;
import camel.execution.ExecutionPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Communication Measurement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.CommunicationMeasurementImpl#getSourceVMInstance <em>Source VM Instance</em>}</li>
 *   <li>{@link camel.execution.impl.CommunicationMeasurementImpl#getTargetVMInstance <em>Target VM Instance</em>}</li>
 *   <li>{@link camel.execution.impl.CommunicationMeasurementImpl#getSourceVM <em>Source VM</em>}</li>
 *   <li>{@link camel.execution.impl.CommunicationMeasurementImpl#getTargetVM <em>Target VM</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CommunicationMeasurementImpl extends MeasurementImpl implements CommunicationMeasurement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CommunicationMeasurementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.COMMUNICATION_MEASUREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VMInstance getSourceVMInstance() {
		return (VMInstance)eGet(ExecutionPackage.Literals.COMMUNICATION_MEASUREMENT__SOURCE_VM_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceVMInstance(VMInstance newSourceVMInstance) {
		eSet(ExecutionPackage.Literals.COMMUNICATION_MEASUREMENT__SOURCE_VM_INSTANCE, newSourceVMInstance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VMInstance getTargetVMInstance() {
		return (VMInstance)eGet(ExecutionPackage.Literals.COMMUNICATION_MEASUREMENT__TARGET_VM_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetVMInstance(VMInstance newTargetVMInstance) {
		eSet(ExecutionPackage.Literals.COMMUNICATION_MEASUREMENT__TARGET_VM_INSTANCE, newTargetVMInstance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VM getSourceVM() {
		return (VM)eGet(ExecutionPackage.Literals.COMMUNICATION_MEASUREMENT__SOURCE_VM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSourceVM(VM newSourceVM) {
		eSet(ExecutionPackage.Literals.COMMUNICATION_MEASUREMENT__SOURCE_VM, newSourceVM);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VM getTargetVM() {
		return (VM)eGet(ExecutionPackage.Literals.COMMUNICATION_MEASUREMENT__TARGET_VM, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTargetVM(VM newTargetVM) {
		eSet(ExecutionPackage.Literals.COMMUNICATION_MEASUREMENT__TARGET_VM, newTargetVM);
	}

} //CommunicationMeasurementImpl
