/**
 */
package camel.execution.impl;

import camel.core.impl.FeatureImpl;

import camel.execution.Cause;
import camel.execution.ExecutionPackage;

import camel.scalability.EventInstance;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cause</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.CauseImpl#getEventInstances <em>Event Instances</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CauseImpl extends FeatureImpl implements Cause {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CauseImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.CAUSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<EventInstance> getEventInstances() {
		return (EList<EventInstance>)eGet(ExecutionPackage.Literals.CAUSE__EVENT_INSTANCES, true);
	}

} //CauseImpl
