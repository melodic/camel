/**
 */
package camel.execution.impl;

import camel.deployment.PaaS;
import camel.deployment.PaaSInstance;

import camel.execution.ExecutionPackage;
import camel.execution.PaaSMeasurement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Paa SMeasurement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.PaaSMeasurementImpl#getPaasInstance <em>Paas Instance</em>}</li>
 *   <li>{@link camel.execution.impl.PaaSMeasurementImpl#getPaas <em>Paas</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PaaSMeasurementImpl extends MeasurementImpl implements PaaSMeasurement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PaaSMeasurementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.PAA_SMEASUREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaaSInstance getPaasInstance() {
		return (PaaSInstance)eGet(ExecutionPackage.Literals.PAA_SMEASUREMENT__PAAS_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPaasInstance(PaaSInstance newPaasInstance) {
		eSet(ExecutionPackage.Literals.PAA_SMEASUREMENT__PAAS_INSTANCE, newPaasInstance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaaS getPaas() {
		return (PaaS)eGet(ExecutionPackage.Literals.PAA_SMEASUREMENT__PAAS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPaas(PaaS newPaas) {
		eSet(ExecutionPackage.Literals.PAA_SMEASUREMENT__PAAS, newPaas);
	}

} //PaaSMeasurementImpl
