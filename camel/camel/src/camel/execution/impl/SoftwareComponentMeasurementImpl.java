/**
 */
package camel.execution.impl;

import camel.deployment.SoftwareComponent;
import camel.deployment.SoftwareComponentInstance;

import camel.execution.ExecutionPackage;
import camel.execution.SoftwareComponentMeasurement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Software Component Measurement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.impl.SoftwareComponentMeasurementImpl#getSoftwareComponentInstance <em>Software Component Instance</em>}</li>
 *   <li>{@link camel.execution.impl.SoftwareComponentMeasurementImpl#getSoftwareComponent <em>Software Component</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SoftwareComponentMeasurementImpl extends MeasurementImpl implements SoftwareComponentMeasurement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SoftwareComponentMeasurementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return ExecutionPackage.Literals.SOFTWARE_COMPONENT_MEASUREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareComponentInstance getSoftwareComponentInstance() {
		return (SoftwareComponentInstance)eGet(ExecutionPackage.Literals.SOFTWARE_COMPONENT_MEASUREMENT__SOFTWARE_COMPONENT_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSoftwareComponentInstance(SoftwareComponentInstance newSoftwareComponentInstance) {
		eSet(ExecutionPackage.Literals.SOFTWARE_COMPONENT_MEASUREMENT__SOFTWARE_COMPONENT_INSTANCE, newSoftwareComponentInstance);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SoftwareComponent getSoftwareComponent() {
		return (SoftwareComponent)eGet(ExecutionPackage.Literals.SOFTWARE_COMPONENT_MEASUREMENT__SOFTWARE_COMPONENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSoftwareComponent(SoftwareComponent newSoftwareComponent) {
		eSet(ExecutionPackage.Literals.SOFTWARE_COMPONENT_MEASUREMENT__SOFTWARE_COMPONENT, newSoftwareComponent);
	}

} //SoftwareComponentMeasurementImpl
