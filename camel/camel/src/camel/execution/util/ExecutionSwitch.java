/**
 */
package camel.execution.util;

import camel.core.Feature;
import camel.core.Model;
import camel.core.NamedElement;

import camel.execution.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see camel.execution.ExecutionPackage
 * @generated
 */
public class ExecutionSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ExecutionPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionSwitch() {
		if (modelPackage == null) {
			modelPackage = ExecutionPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case ExecutionPackage.EXECUTION_MODEL: {
				ExecutionModel executionModel = (ExecutionModel)theEObject;
				T result = caseExecutionModel(executionModel);
				if (result == null) result = caseModel(executionModel);
				if (result == null) result = caseFeature(executionModel);
				if (result == null) result = caseNamedElement(executionModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.ACTION_INSTANCE: {
				ActionInstance actionInstance = (ActionInstance)theEObject;
				T result = caseActionInstance(actionInstance);
				if (result == null) result = caseFeature(actionInstance);
				if (result == null) result = caseNamedElement(actionInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.MEASUREMENT: {
				Measurement measurement = (Measurement)theEObject;
				T result = caseMeasurement(measurement);
				if (result == null) result = caseFeature(measurement);
				if (result == null) result = caseNamedElement(measurement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.APPLICATION_MEASUREMENT: {
				ApplicationMeasurement applicationMeasurement = (ApplicationMeasurement)theEObject;
				T result = caseApplicationMeasurement(applicationMeasurement);
				if (result == null) result = caseMeasurement(applicationMeasurement);
				if (result == null) result = caseFeature(applicationMeasurement);
				if (result == null) result = caseNamedElement(applicationMeasurement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.SOFTWARE_COMPONENT_MEASUREMENT: {
				SoftwareComponentMeasurement softwareComponentMeasurement = (SoftwareComponentMeasurement)theEObject;
				T result = caseSoftwareComponentMeasurement(softwareComponentMeasurement);
				if (result == null) result = caseMeasurement(softwareComponentMeasurement);
				if (result == null) result = caseFeature(softwareComponentMeasurement);
				if (result == null) result = caseNamedElement(softwareComponentMeasurement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.COMMUNICATION_MEASUREMENT: {
				CommunicationMeasurement communicationMeasurement = (CommunicationMeasurement)theEObject;
				T result = caseCommunicationMeasurement(communicationMeasurement);
				if (result == null) result = caseMeasurement(communicationMeasurement);
				if (result == null) result = caseFeature(communicationMeasurement);
				if (result == null) result = caseNamedElement(communicationMeasurement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.VM_MEASUREMENT: {
				VMMeasurement vmMeasurement = (VMMeasurement)theEObject;
				T result = caseVMMeasurement(vmMeasurement);
				if (result == null) result = caseMeasurement(vmMeasurement);
				if (result == null) result = caseFeature(vmMeasurement);
				if (result == null) result = caseNamedElement(vmMeasurement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.PAA_SMEASUREMENT: {
				PaaSMeasurement paaSMeasurement = (PaaSMeasurement)theEObject;
				T result = casePaaSMeasurement(paaSMeasurement);
				if (result == null) result = caseMeasurement(paaSMeasurement);
				if (result == null) result = caseFeature(paaSMeasurement);
				if (result == null) result = caseNamedElement(paaSMeasurement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.CONTAINER_MEASUREMENT: {
				ContainerMeasurement containerMeasurement = (ContainerMeasurement)theEObject;
				T result = caseContainerMeasurement(containerMeasurement);
				if (result == null) result = caseMeasurement(containerMeasurement);
				if (result == null) result = caseFeature(containerMeasurement);
				if (result == null) result = caseNamedElement(containerMeasurement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.DATA_MEASUREMENT: {
				DataMeasurement dataMeasurement = (DataMeasurement)theEObject;
				T result = caseDataMeasurement(dataMeasurement);
				if (result == null) result = caseMeasurement(dataMeasurement);
				if (result == null) result = caseFeature(dataMeasurement);
				if (result == null) result = caseNamedElement(dataMeasurement);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.SLO_VIOLATION: {
				SLOViolation sloViolation = (SLOViolation)theEObject;
				T result = caseSLOViolation(sloViolation);
				if (result == null) result = caseCause(sloViolation);
				if (result == null) result = caseFeature(sloViolation);
				if (result == null) result = caseNamedElement(sloViolation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.RULE_TRIGGER: {
				RuleTrigger ruleTrigger = (RuleTrigger)theEObject;
				T result = caseRuleTrigger(ruleTrigger);
				if (result == null) result = caseCause(ruleTrigger);
				if (result == null) result = caseFeature(ruleTrigger);
				if (result == null) result = caseNamedElement(ruleTrigger);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.HISTORY_RECORD: {
				HistoryRecord historyRecord = (HistoryRecord)theEObject;
				T result = caseHistoryRecord(historyRecord);
				if (result == null) result = caseFeature(historyRecord);
				if (result == null) result = caseNamedElement(historyRecord);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.HISTORY_INFO: {
				HistoryInfo historyInfo = (HistoryInfo)theEObject;
				T result = caseHistoryInfo(historyInfo);
				if (result == null) result = caseFeature(historyInfo);
				if (result == null) result = caseNamedElement(historyInfo);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case ExecutionPackage.CAUSE: {
				Cause cause = (Cause)theEObject;
				T result = caseCause(cause);
				if (result == null) result = caseFeature(cause);
				if (result == null) result = caseNamedElement(cause);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExecutionModel(ExecutionModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Action Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Action Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseActionInstance(ActionInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Measurement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMeasurement(Measurement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Application Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Application Measurement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseApplicationMeasurement(ApplicationMeasurement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Software Component Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Software Component Measurement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoftwareComponentMeasurement(SoftwareComponentMeasurement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Communication Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Communication Measurement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCommunicationMeasurement(CommunicationMeasurement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>VM Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>VM Measurement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseVMMeasurement(VMMeasurement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Paa SMeasurement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Paa SMeasurement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePaaSMeasurement(PaaSMeasurement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Container Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Container Measurement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseContainerMeasurement(ContainerMeasurement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Measurement</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataMeasurement(DataMeasurement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>SLO Violation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>SLO Violation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSLOViolation(SLOViolation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Rule Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Rule Trigger</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRuleTrigger(RuleTrigger object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>History Record</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>History Record</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHistoryRecord(HistoryRecord object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>History Info</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>History Info</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseHistoryInfo(HistoryInfo object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cause</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cause</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCause(Cause object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeature(Feature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModel(Model object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //ExecutionSwitch
