/**
 */
package camel.execution.util;

import camel.execution.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.execution.ExecutionPackage
 * @generated
 */
public class ExecutionValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final ExecutionValidator INSTANCE = new ExecutionValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.execution";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return ExecutionPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case ExecutionPackage.EXECUTION_MODEL:
				return validateExecutionModel((ExecutionModel)value, diagnostics, context);
			case ExecutionPackage.ACTION_INSTANCE:
				return validateActionInstance((ActionInstance)value, diagnostics, context);
			case ExecutionPackage.MEASUREMENT:
				return validateMeasurement((Measurement)value, diagnostics, context);
			case ExecutionPackage.APPLICATION_MEASUREMENT:
				return validateApplicationMeasurement((ApplicationMeasurement)value, diagnostics, context);
			case ExecutionPackage.SOFTWARE_COMPONENT_MEASUREMENT:
				return validateSoftwareComponentMeasurement((SoftwareComponentMeasurement)value, diagnostics, context);
			case ExecutionPackage.COMMUNICATION_MEASUREMENT:
				return validateCommunicationMeasurement((CommunicationMeasurement)value, diagnostics, context);
			case ExecutionPackage.VM_MEASUREMENT:
				return validateVMMeasurement((VMMeasurement)value, diagnostics, context);
			case ExecutionPackage.PAA_SMEASUREMENT:
				return validatePaaSMeasurement((PaaSMeasurement)value, diagnostics, context);
			case ExecutionPackage.CONTAINER_MEASUREMENT:
				return validateContainerMeasurement((ContainerMeasurement)value, diagnostics, context);
			case ExecutionPackage.DATA_MEASUREMENT:
				return validateDataMeasurement((DataMeasurement)value, diagnostics, context);
			case ExecutionPackage.SLO_VIOLATION:
				return validateSLOViolation((SLOViolation)value, diagnostics, context);
			case ExecutionPackage.RULE_TRIGGER:
				return validateRuleTrigger((RuleTrigger)value, diagnostics, context);
			case ExecutionPackage.HISTORY_RECORD:
				return validateHistoryRecord((HistoryRecord)value, diagnostics, context);
			case ExecutionPackage.HISTORY_INFO:
				return validateHistoryInfo((HistoryInfo)value, diagnostics, context);
			case ExecutionPackage.CAUSE:
				return validateCause((Cause)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExecutionModel(ExecutionModel executionModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)executionModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateActionInstance(ActionInstance actionInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)actionInstance, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMeasurement(Measurement measurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)measurement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)measurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)measurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)measurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)measurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)measurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)measurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)measurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)measurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_event_instance_same_metric(measurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_correct_measurement_value(measurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_metric_refers_to_correct_execution_model(measurement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the measurement_event_instance_same_metric constraint of '<em>Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MEASUREMENT__MEASUREMENT_EVENT_INSTANCE_SAME_METRIC__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Measurement: ' + self.name + ' maps to non-functional event instance : ' + eventInstance.toString() + ' with a type related to a metric different from the one of this measurement',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\tasError(self.eventInstance <> null and\n" +
		"\t\t\t\tself.eventInstance.type.oclIsTypeOf(camel::scalability::NonFunctionalEvent) implies self.metricInstance.metricContext.metric =\n" +
		"\t\t\t\tself.eventInstance.type.oclAsType(camel::scalability::NonFunctionalEvent).metricConstraint.metricContext.metric)\n" +
		"}.status";

	/**
	 * Validates the measurement_event_instance_same_metric constraint of '<em>Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMeasurement_measurement_event_instance_same_metric(Measurement measurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.MEASUREMENT,
				 (EObject)measurement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "measurement_event_instance_same_metric",
				 MEASUREMENT__MEASUREMENT_EVENT_INSTANCE_SAME_METRIC__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the correct_measurement_value constraint of '<em>Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MEASUREMENT__CORRECT_MEASUREMENT_VALUE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Measurement: ' + self.name + ' has a value: ' + value.toString() + ' that is outside the range of values of metric instance: ' + metricInstance.name,\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\tasError(\n" +
		"\t\t\tlet type :\n" +
		"\t\t\t\tcamel::type::ValueType = metricInstance.metricContext.metric.metricTemplate.valueType\n" +
		"\t\t\tin if (type <> null)\n" +
		"\t\t\t\tthen if (type.oclIsTypeOf(camel::type::Range))\n" +
		"\t\t\t\t\tthen type.oclAsType(camel::type::Range).includesValue(self.value)\n" +
		"\t\t\t\t\telse if (type.oclIsTypeOf(camel::type::RangeUnion))\n" +
		"\t\t\t\t\t\tthen type.oclAsType(camel::type::RangeUnion).includesValue(self.value)\n" +
		"\t\t\t\t\t\telse true\n" +
		"\t\t\t\t\t\tendif\n" +
		"\t\t\t\t\tendif\n" +
		"\t\t\t\telse true\n" +
		"\t\t\t\tendif)\n" +
		"}.status";

	/**
	 * Validates the correct_measurement_value constraint of '<em>Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMeasurement_correct_measurement_value(Measurement measurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.MEASUREMENT,
				 (EObject)measurement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_measurement_value",
				 MEASUREMENT__CORRECT_MEASUREMENT_VALUE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the measurement_metric_refers_to_correct_execution_model constraint of '<em>Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MEASUREMENT__MEASUREMENT_METRIC_REFERS_TO_CORRECT_EXECUTION_MODEL__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Measurement: ' + self.name + ' does not belong to the same execution model as the one of the binding of the respective metric instance:' + metricInstance.name,\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.oclContainer().oclAsType(ExecutionModel) = metricInstance.objectBinding.executionModel)\n" +
		"}.status";

	/**
	 * Validates the measurement_metric_refers_to_correct_execution_model constraint of '<em>Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMeasurement_measurement_metric_refers_to_correct_execution_model(Measurement measurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.MEASUREMENT,
				 (EObject)measurement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "measurement_metric_refers_to_correct_execution_model",
				 MEASUREMENT__MEASUREMENT_METRIC_REFERS_TO_CORRECT_EXECUTION_MODEL__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateApplicationMeasurement(ApplicationMeasurement applicationMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)applicationMeasurement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)applicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)applicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)applicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)applicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)applicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)applicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)applicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)applicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_event_instance_same_metric(applicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_correct_measurement_value(applicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_metric_refers_to_correct_execution_model(applicationMeasurement, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoftwareComponentMeasurement(SoftwareComponentMeasurement softwareComponentMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)softwareComponentMeasurement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)softwareComponentMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)softwareComponentMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)softwareComponentMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)softwareComponentMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)softwareComponentMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)softwareComponentMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)softwareComponentMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)softwareComponentMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_event_instance_same_metric(softwareComponentMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_correct_measurement_value(softwareComponentMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_metric_refers_to_correct_execution_model(softwareComponentMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateSoftwareComponentMeasurement_correct_sw_component_measurement(softwareComponentMeasurement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the correct_sw_component_measurement constraint of '<em>Software Component Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SOFTWARE_COMPONENT_MEASUREMENT__CORRECT_SW_COMPONENT_MEASUREMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'SoftwareComponentMeasurement: ' + self.name + ' should either map to an instance of a software component or the software component itself',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((softwareComponentInstance <> null) xor (softwareComponent <> null))\n" +
		"}.status";

	/**
	 * Validates the correct_sw_component_measurement constraint of '<em>Software Component Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoftwareComponentMeasurement_correct_sw_component_measurement(SoftwareComponentMeasurement softwareComponentMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.SOFTWARE_COMPONENT_MEASUREMENT,
				 (EObject)softwareComponentMeasurement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_sw_component_measurement",
				 SOFTWARE_COMPONENT_MEASUREMENT__CORRECT_SW_COMPONENT_MEASUREMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationMeasurement(CommunicationMeasurement communicationMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)communicationMeasurement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_event_instance_same_metric(communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_correct_measurement_value(communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_metric_refers_to_correct_execution_model(communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateCommunicationMeasurement_correct_communication_measurement(communicationMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateCommunicationMeasurement_communication_meas_diff_source_target(communicationMeasurement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the correct_communication_measurement constraint of '<em>Communication Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMMUNICATION_MEASUREMENT__CORRECT_COMMUNICATION_MEASUREMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'CommunicationMeasurement: ' + self.name + ' should either have a source and target VM instance or a source and target VM',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t \tasError((sourceVMInstance <> null and targetVMInstance <> null and sourceVM = null and targetVM = null) xor \n" +
		"\t\t\t \t\t(sourceVM <> null and targetVM <> null and sourceVMInstance = null and targetVMInstance = null)\n" +
		"\t\t\t \t)\n" +
		"}.status";

	/**
	 * Validates the correct_communication_measurement constraint of '<em>Communication Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationMeasurement_correct_communication_measurement(CommunicationMeasurement communicationMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.COMMUNICATION_MEASUREMENT,
				 (EObject)communicationMeasurement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_communication_measurement",
				 COMMUNICATION_MEASUREMENT__CORRECT_COMMUNICATION_MEASUREMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the communication_meas_diff_source_target constraint of '<em>Communication Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMMUNICATION_MEASUREMENT__COMMUNICATION_MEAS_DIFF_SOURCE_TARGET__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In CommunicationMeasurement: ' + self.name + ' source VM should be different from target VM and the same should hold for the instance level',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t \tasError((sourceVM <> null and targetVM <> null implies sourceVM <> targetVM) and \n" +
		"\t\t\t \t(sourceVMInstance <> null and targetVMInstance <> null implies sourceVMInstance <> targetVMInstance))\n" +
		"}.status";

	/**
	 * Validates the communication_meas_diff_source_target constraint of '<em>Communication Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCommunicationMeasurement_communication_meas_diff_source_target(CommunicationMeasurement communicationMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.COMMUNICATION_MEASUREMENT,
				 (EObject)communicationMeasurement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "communication_meas_diff_source_target",
				 COMMUNICATION_MEASUREMENT__COMMUNICATION_MEAS_DIFF_SOURCE_TARGET__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVMMeasurement(VMMeasurement vmMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)vmMeasurement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)vmMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)vmMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)vmMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)vmMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)vmMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)vmMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)vmMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)vmMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_event_instance_same_metric(vmMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_correct_measurement_value(vmMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_metric_refers_to_correct_execution_model(vmMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateVMMeasurement_correct_vm_measurement(vmMeasurement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the correct_vm_measurement constraint of '<em>VM Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String VM_MEASUREMENT__CORRECT_VM_MEASUREMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'VMMeasurement: ' + self.name + ' should either map to an instance of a VM or the VM itself',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\tasError((vmInstance <> null) xor (vm <> null))\n" +
		"}.status";

	/**
	 * Validates the correct_vm_measurement constraint of '<em>VM Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVMMeasurement_correct_vm_measurement(VMMeasurement vmMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.VM_MEASUREMENT,
				 (EObject)vmMeasurement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_vm_measurement",
				 VM_MEASUREMENT__CORRECT_VM_MEASUREMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePaaSMeasurement(PaaSMeasurement paaSMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)paaSMeasurement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)paaSMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)paaSMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)paaSMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)paaSMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)paaSMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)paaSMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)paaSMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)paaSMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_event_instance_same_metric(paaSMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_correct_measurement_value(paaSMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_metric_refers_to_correct_execution_model(paaSMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validatePaaSMeasurement_correct_paas_measurement(paaSMeasurement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the correct_paas_measurement constraint of '<em>Paa SMeasurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PAA_SMEASUREMENT__CORRECT_PAAS_MEASUREMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'PaaSMeasurement: ' + self.name + ' should either map to an instance of a PaaS or the PaaS itself',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\tasError((paasInstance <> null) xor (paas <> null))\n" +
		"}.status";

	/**
	 * Validates the correct_paas_measurement constraint of '<em>Paa SMeasurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePaaSMeasurement_correct_paas_measurement(PaaSMeasurement paaSMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.PAA_SMEASUREMENT,
				 (EObject)paaSMeasurement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_paas_measurement",
				 PAA_SMEASUREMENT__CORRECT_PAAS_MEASUREMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateContainerMeasurement(ContainerMeasurement containerMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)containerMeasurement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)containerMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)containerMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)containerMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)containerMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)containerMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)containerMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)containerMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)containerMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_event_instance_same_metric(containerMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_correct_measurement_value(containerMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_metric_refers_to_correct_execution_model(containerMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateContainerMeasurement_correct_container_measurement(containerMeasurement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the correct_container_measurement constraint of '<em>Container Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String CONTAINER_MEASUREMENT__CORRECT_CONTAINER_MEASUREMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'ContainerMeasurement: ' + self.name + ' should either map to an instance of a container or the container itself',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\tasError((containerInstance <> null) xor (container <> null))\n" +
		"}.status";

	/**
	 * Validates the correct_container_measurement constraint of '<em>Container Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateContainerMeasurement_correct_container_measurement(ContainerMeasurement containerMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.CONTAINER_MEASUREMENT,
				 (EObject)containerMeasurement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_container_measurement",
				 CONTAINER_MEASUREMENT__CORRECT_CONTAINER_MEASUREMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataMeasurement(DataMeasurement dataMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)dataMeasurement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)dataMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)dataMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)dataMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)dataMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)dataMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)dataMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)dataMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)dataMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_event_instance_same_metric(dataMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_correct_measurement_value(dataMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateMeasurement_measurement_metric_refers_to_correct_execution_model(dataMeasurement, diagnostics, context);
		if (result || diagnostics != null) result &= validateDataMeasurement_correct_data_measurement(dataMeasurement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the correct_data_measurement constraint of '<em>Data Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String DATA_MEASUREMENT__CORRECT_DATA_MEASUREMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'DataMeasurement: ' + self.name + ' should either map to an instance of data or the data itself',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\tasError((dataInstance <> null) xor (data <> null))\n" +
		"}.status";

	/**
	 * Validates the correct_data_measurement constraint of '<em>Data Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataMeasurement_correct_data_measurement(DataMeasurement dataMeasurement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.DATA_MEASUREMENT,
				 (EObject)dataMeasurement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "correct_data_measurement",
				 DATA_MEASUREMENT__CORRECT_DATA_MEASUREMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSLOViolation(SLOViolation sloViolation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)sloViolation, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)sloViolation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)sloViolation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)sloViolation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)sloViolation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)sloViolation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)sloViolation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)sloViolation, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)sloViolation, diagnostics, context);
		if (result || diagnostics != null) result &= validateSLOViolation_slo_violation_same_metric(sloViolation, diagnostics, context);
		if (result || diagnostics != null) result &= validateSLOViolation_slo_violation_slo_in_reqs_for_execution_model(sloViolation, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the slo_violation_same_metric constraint of '<em>SLO Violation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SLO_VIOLATION__SLO_VIOLATION_SAME_METRIC__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'The metric in slo: ' + slo.name + ' of SLOViolation: ' + self.name + ' is not the same as the one referenced by the respective measurement: ' + measurement.name,\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\tasError(\n" +
		"\t\t\t\tif (slo.constraint.oclIsTypeOf(camel::constraint::MetricConstraint)\n" +
		"\t\t\t\t\t)\n" +
		"\t\t\t\tthen \n" +
		"\t\t\t\t\tmeasurement.metricInstance.metricContext.metric = slo.constraint.oclAsType(camel::constraint::MetricConstraint).metricContext.metric\n" +
		"\t\t\t\telse false\n" +
		"\t\t\t\tendif)\n" +
		"}.status";

	/**
	 * Validates the slo_violation_same_metric constraint of '<em>SLO Violation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSLOViolation_slo_violation_same_metric(SLOViolation sloViolation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.SLO_VIOLATION,
				 (EObject)sloViolation,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "slo_violation_same_metric",
				 SLO_VIOLATION__SLO_VIOLATION_SAME_METRIC__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the slo_violation_slo_in_reqs_for_execution_model constraint of '<em>SLO Violation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SLO_VIOLATION__SLO_VIOLATION_SLO_IN_REQS_FOR_EXECUTION_MODEL__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'SLO: ' + self.slo.name + ' is not included in the requirement model of the current execution model: ' + self.oclContainer().oclAsType(ExecutionModel).name + ' of SLO Violation: ' + self.name,\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.oclContainer().oclAsType(ExecutionModel).requirementModel.requirements\n" +
		"\t\t\t\t->includes(self.slo))\n" +
		"}.status";

	/**
	 * Validates the slo_violation_slo_in_reqs_for_execution_model constraint of '<em>SLO Violation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSLOViolation_slo_violation_slo_in_reqs_for_execution_model(SLOViolation sloViolation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.SLO_VIOLATION,
				 (EObject)sloViolation,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "slo_violation_slo_in_reqs_for_execution_model",
				 SLO_VIOLATION__SLO_VIOLATION_SLO_IN_REQS_FOR_EXECUTION_MODEL__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRuleTrigger(RuleTrigger ruleTrigger, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)ruleTrigger, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHistoryRecord(HistoryRecord historyRecord, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)historyRecord, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)historyRecord, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)historyRecord, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)historyRecord, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)historyRecord, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)historyRecord, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)historyRecord, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)historyRecord, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)historyRecord, diagnostics, context);
		if (result || diagnostics != null) result &= validateHistoryRecord_history_record_from_to_diff(historyRecord, diagnostics, context);
		if (result || diagnostics != null) result &= validateHistoryRecord_history_record_correct_instance_model(historyRecord, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the history_record_from_to_diff constraint of '<em>History Record</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String HISTORY_RECORD__HISTORY_RECORD_FROM_TO_DIFF__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'HistoryRecord: ' + self.name + ' has from and to instance models which are identical'\n" +
		"\t\t\t,\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((fromDeploymentInstanceModel <> null and toDeploymentInstanceModel <> null implies fromDeploymentInstanceModel <> toDeploymentInstanceModel)\n" +
		"\t\t\t\tand \n" +
		"\t\t\t\t(fromDataInstanceModel <> null and toDataInstanceModel <> null implies fromDataInstanceModel <> toDataInstanceModel))\n" +
		"}.status";

	/**
	 * Validates the history_record_from_to_diff constraint of '<em>History Record</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHistoryRecord_history_record_from_to_diff(HistoryRecord historyRecord, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.HISTORY_RECORD,
				 (EObject)historyRecord,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "history_record_from_to_diff",
				 HISTORY_RECORD__HISTORY_RECORD_FROM_TO_DIFF__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the history_record_correct_instance_model constraint of '<em>History Record</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String HISTORY_RECORD__HISTORY_RECORD_CORRECT_INSTANCE_MODEL__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'HistoryRecord: ' + self.name + ' should have all non-null instance models correct, i.e., instance models with a type that belongs to the encompassing execution model',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(let em: ExecutionModel = self.oclContainer().oclAsType(ExecutionModel) in \n" +
		"\t\t\t\t\t(fromDeploymentInstanceModel <> null implies fromDeploymentInstanceModel.type = em.deploymentTypeModel) and\n" +
		"\t\t\t\t\t(toDeploymentInstanceModel <> null implies toDeploymentInstanceModel.type = em.deploymentTypeModel) and\n" +
		"\t\t\t\t\t(fromDataInstanceModel <> null implies fromDataInstanceModel.type = em.dataTypeModel) and\n" +
		"\t\t\t\t\t(toDataInstanceModel <> null implies toDataInstanceModel.type = em.dataTypeModel))\n" +
		"}.status";

	/**
	 * Validates the history_record_correct_instance_model constraint of '<em>History Record</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHistoryRecord_history_record_correct_instance_model(HistoryRecord historyRecord, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.HISTORY_RECORD,
				 (EObject)historyRecord,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "history_record_correct_instance_model",
				 HISTORY_RECORD__HISTORY_RECORD_CORRECT_INSTANCE_MODEL__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHistoryInfo(HistoryInfo historyInfo, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)historyInfo, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)historyInfo, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)historyInfo, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)historyInfo, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)historyInfo, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)historyInfo, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)historyInfo, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)historyInfo, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)historyInfo, diagnostics, context);
		if (result || diagnostics != null) result &= validateHistoryInfo_at_least_object_or_objectRef_in_history_info(historyInfo, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the at_least_object_or_objectRef_in_history_info constraint of '<em>History Info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String HISTORY_INFO__AT_LEAST_OBJECT_OR_OBJECT_REF_IN_HISTORY_INFO__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'HistoryInfo: ' + self.name + ' should either to a certain model or to a String-based object in case a certain message needs to be recorded (e.g., error)',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((object <> null and objectRef = null) or (object = null and objectRef <> null))\n" +
		"}.status";

	/**
	 * Validates the at_least_object_or_objectRef_in_history_info constraint of '<em>History Info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHistoryInfo_at_least_object_or_objectRef_in_history_info(HistoryInfo historyInfo, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(ExecutionPackage.Literals.HISTORY_INFO,
				 (EObject)historyInfo,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "at_least_object_or_objectRef_in_history_info",
				 HISTORY_INFO__AT_LEAST_OBJECT_OR_OBJECT_REF_IN_HISTORY_INFO__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCause(Cause cause, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)cause, diagnostics, context);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //ExecutionValidator
