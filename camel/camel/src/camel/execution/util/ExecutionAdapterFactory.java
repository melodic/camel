/**
 */
package camel.execution.util;

import camel.core.Feature;
import camel.core.Model;
import camel.core.NamedElement;

import camel.execution.*;

import org.eclipse.emf.common.notify.Adapter;
import org.eclipse.emf.common.notify.Notifier;

import org.eclipse.emf.common.notify.impl.AdapterFactoryImpl;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * The <b>Adapter Factory</b> for the model.
 * It provides an adapter <code>createXXX</code> method for each class of the model.
 * <!-- end-user-doc -->
 * @see camel.execution.ExecutionPackage
 * @generated
 */
public class ExecutionAdapterFactory extends AdapterFactoryImpl {
	/**
	 * The cached model package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static ExecutionPackage modelPackage;

	/**
	 * Creates an instance of the adapter factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionAdapterFactory() {
		if (modelPackage == null) {
			modelPackage = ExecutionPackage.eINSTANCE;
		}
	}

	/**
	 * Returns whether this factory is applicable for the type of the object.
	 * <!-- begin-user-doc -->
	 * This implementation returns <code>true</code> if the object is either the model's package or is an instance object of the model.
	 * <!-- end-user-doc -->
	 * @return whether this factory is applicable for the type of the object.
	 * @generated
	 */
	@Override
	public boolean isFactoryForType(Object object) {
		if (object == modelPackage) {
			return true;
		}
		if (object instanceof EObject) {
			return ((EObject)object).eClass().getEPackage() == modelPackage;
		}
		return false;
	}

	/**
	 * The switch that delegates to the <code>createXXX</code> methods.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ExecutionSwitch<Adapter> modelSwitch =
		new ExecutionSwitch<Adapter>() {
			@Override
			public Adapter caseExecutionModel(ExecutionModel object) {
				return createExecutionModelAdapter();
			}
			@Override
			public Adapter caseActionInstance(ActionInstance object) {
				return createActionInstanceAdapter();
			}
			@Override
			public Adapter caseMeasurement(Measurement object) {
				return createMeasurementAdapter();
			}
			@Override
			public Adapter caseApplicationMeasurement(ApplicationMeasurement object) {
				return createApplicationMeasurementAdapter();
			}
			@Override
			public Adapter caseSoftwareComponentMeasurement(SoftwareComponentMeasurement object) {
				return createSoftwareComponentMeasurementAdapter();
			}
			@Override
			public Adapter caseCommunicationMeasurement(CommunicationMeasurement object) {
				return createCommunicationMeasurementAdapter();
			}
			@Override
			public Adapter caseVMMeasurement(VMMeasurement object) {
				return createVMMeasurementAdapter();
			}
			@Override
			public Adapter casePaaSMeasurement(PaaSMeasurement object) {
				return createPaaSMeasurementAdapter();
			}
			@Override
			public Adapter caseContainerMeasurement(ContainerMeasurement object) {
				return createContainerMeasurementAdapter();
			}
			@Override
			public Adapter caseDataMeasurement(DataMeasurement object) {
				return createDataMeasurementAdapter();
			}
			@Override
			public Adapter caseSLOViolation(SLOViolation object) {
				return createSLOViolationAdapter();
			}
			@Override
			public Adapter caseRuleTrigger(RuleTrigger object) {
				return createRuleTriggerAdapter();
			}
			@Override
			public Adapter caseHistoryRecord(HistoryRecord object) {
				return createHistoryRecordAdapter();
			}
			@Override
			public Adapter caseHistoryInfo(HistoryInfo object) {
				return createHistoryInfoAdapter();
			}
			@Override
			public Adapter caseCause(Cause object) {
				return createCauseAdapter();
			}
			@Override
			public Adapter caseNamedElement(NamedElement object) {
				return createNamedElementAdapter();
			}
			@Override
			public Adapter caseFeature(Feature object) {
				return createFeatureAdapter();
			}
			@Override
			public Adapter caseModel(Model object) {
				return createModelAdapter();
			}
			@Override
			public Adapter defaultCase(EObject object) {
				return createEObjectAdapter();
			}
		};

	/**
	 * Creates an adapter for the <code>target</code>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param target the object to adapt.
	 * @return the adapter for the <code>target</code>.
	 * @generated
	 */
	@Override
	public Adapter createAdapter(Notifier target) {
		return modelSwitch.doSwitch((EObject)target);
	}


	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.ExecutionModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.ExecutionModel
	 * @generated
	 */
	public Adapter createExecutionModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.ActionInstance <em>Action Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.ActionInstance
	 * @generated
	 */
	public Adapter createActionInstanceAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.Measurement <em>Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.Measurement
	 * @generated
	 */
	public Adapter createMeasurementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.ApplicationMeasurement <em>Application Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.ApplicationMeasurement
	 * @generated
	 */
	public Adapter createApplicationMeasurementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.SoftwareComponentMeasurement <em>Software Component Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.SoftwareComponentMeasurement
	 * @generated
	 */
	public Adapter createSoftwareComponentMeasurementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.CommunicationMeasurement <em>Communication Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.CommunicationMeasurement
	 * @generated
	 */
	public Adapter createCommunicationMeasurementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.VMMeasurement <em>VM Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.VMMeasurement
	 * @generated
	 */
	public Adapter createVMMeasurementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.PaaSMeasurement <em>Paa SMeasurement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.PaaSMeasurement
	 * @generated
	 */
	public Adapter createPaaSMeasurementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.ContainerMeasurement <em>Container Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.ContainerMeasurement
	 * @generated
	 */
	public Adapter createContainerMeasurementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.DataMeasurement <em>Data Measurement</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.DataMeasurement
	 * @generated
	 */
	public Adapter createDataMeasurementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.SLOViolation <em>SLO Violation</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.SLOViolation
	 * @generated
	 */
	public Adapter createSLOViolationAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.RuleTrigger <em>Rule Trigger</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.RuleTrigger
	 * @generated
	 */
	public Adapter createRuleTriggerAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.HistoryRecord <em>History Record</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.HistoryRecord
	 * @generated
	 */
	public Adapter createHistoryRecordAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.HistoryInfo <em>History Info</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.HistoryInfo
	 * @generated
	 */
	public Adapter createHistoryInfoAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.execution.Cause <em>Cause</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.execution.Cause
	 * @generated
	 */
	public Adapter createCauseAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.core.NamedElement <em>Named Element</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.core.NamedElement
	 * @generated
	 */
	public Adapter createNamedElementAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.core.Feature <em>Feature</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.core.Feature
	 * @generated
	 */
	public Adapter createFeatureAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for an object of class '{@link camel.core.Model <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null so that we can easily ignore cases;
	 * it's useful to ignore a case when inheritance will catch all the cases anyway.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @see camel.core.Model
	 * @generated
	 */
	public Adapter createModelAdapter() {
		return null;
	}

	/**
	 * Creates a new adapter for the default case.
	 * <!-- begin-user-doc -->
	 * This default implementation returns null.
	 * <!-- end-user-doc -->
	 * @return the new adapter.
	 * @generated
	 */
	public Adapter createEObjectAdapter() {
		return null;
	}

} //ExecutionAdapterFactory
