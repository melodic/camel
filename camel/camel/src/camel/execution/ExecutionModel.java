/**
 */
package camel.execution;

import camel.core.Model;
import camel.data.DataTypeModel;
import camel.deployment.DeploymentTypeModel;

import camel.requirement.RequirementModel;

import camel.unit.Unit;

import java.util.Date;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.ExecutionModel#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link camel.execution.ExecutionModel#getEndTime <em>End Time</em>}</li>
 *   <li>{@link camel.execution.ExecutionModel#getTotalCost <em>Total Cost</em>}</li>
 *   <li>{@link camel.execution.ExecutionModel#getCostUnit <em>Cost Unit</em>}</li>
 *   <li>{@link camel.execution.ExecutionModel#getDeploymentTypeModel <em>Deployment Type Model</em>}</li>
 *   <li>{@link camel.execution.ExecutionModel#getRequirementModel <em>Requirement Model</em>}</li>
 *   <li>{@link camel.execution.ExecutionModel#getDataTypeModel <em>Data Type Model</em>}</li>
 *   <li>{@link camel.execution.ExecutionModel#getMeasurements <em>Measurements</em>}</li>
 *   <li>{@link camel.execution.ExecutionModel#getSloViolations <em>Slo Violations</em>}</li>
 *   <li>{@link camel.execution.ExecutionModel#getRuleTriggers <em>Rule Triggers</em>}</li>
 *   <li>{@link camel.execution.ExecutionModel#getHistoryRecords <em>History Records</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getExecutionModel()
 * @model
 * @generated
 */
public interface ExecutionModel extends Model {
	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(Date)
	 * @see camel.execution.ExecutionPackage#getExecutionModel_StartTime()
	 * @model
	 * @generated
	 */
	Date getStartTime();

	/**
	 * Sets the value of the '{@link camel.execution.ExecutionModel#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(Date value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(Date)
	 * @see camel.execution.ExecutionPackage#getExecutionModel_EndTime()
	 * @model
	 * @generated
	 */
	Date getEndTime();

	/**
	 * Sets the value of the '{@link camel.execution.ExecutionModel#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(Date value);

	/**
	 * Returns the value of the '<em><b>Total Cost</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Total Cost</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Total Cost</em>' attribute.
	 * @see #setTotalCost(double)
	 * @see camel.execution.ExecutionPackage#getExecutionModel_TotalCost()
	 * @model required="true"
	 * @generated
	 */
	double getTotalCost();

	/**
	 * Sets the value of the '{@link camel.execution.ExecutionModel#getTotalCost <em>Total Cost</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Total Cost</em>' attribute.
	 * @see #getTotalCost()
	 * @generated
	 */
	void setTotalCost(double value);

	/**
	 * Returns the value of the '<em><b>Cost Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cost Unit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cost Unit</em>' reference.
	 * @see #setCostUnit(Unit)
	 * @see camel.execution.ExecutionPackage#getExecutionModel_CostUnit()
	 * @model
	 * @generated
	 */
	Unit getCostUnit();

	/**
	 * Sets the value of the '{@link camel.execution.ExecutionModel#getCostUnit <em>Cost Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cost Unit</em>' reference.
	 * @see #getCostUnit()
	 * @generated
	 */
	void setCostUnit(Unit value);

	/**
	 * Returns the value of the '<em><b>Deployment Type Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deployment Type Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deployment Type Model</em>' reference.
	 * @see #setDeploymentTypeModel(DeploymentTypeModel)
	 * @see camel.execution.ExecutionPackage#getExecutionModel_DeploymentTypeModel()
	 * @model required="true"
	 * @generated
	 */
	DeploymentTypeModel getDeploymentTypeModel();

	/**
	 * Sets the value of the '{@link camel.execution.ExecutionModel#getDeploymentTypeModel <em>Deployment Type Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Deployment Type Model</em>' reference.
	 * @see #getDeploymentTypeModel()
	 * @generated
	 */
	void setDeploymentTypeModel(DeploymentTypeModel value);

	/**
	 * Returns the value of the '<em><b>Requirement Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirement Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirement Model</em>' reference.
	 * @see #setRequirementModel(RequirementModel)
	 * @see camel.execution.ExecutionPackage#getExecutionModel_RequirementModel()
	 * @model required="true"
	 * @generated
	 */
	RequirementModel getRequirementModel();

	/**
	 * Sets the value of the '{@link camel.execution.ExecutionModel#getRequirementModel <em>Requirement Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Requirement Model</em>' reference.
	 * @see #getRequirementModel()
	 * @generated
	 */
	void setRequirementModel(RequirementModel value);

	/**
	 * Returns the value of the '<em><b>Data Type Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Type Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Type Model</em>' reference.
	 * @see #setDataTypeModel(DataTypeModel)
	 * @see camel.execution.ExecutionPackage#getExecutionModel_DataTypeModel()
	 * @model
	 * @generated
	 */
	DataTypeModel getDataTypeModel();

	/**
	 * Sets the value of the '{@link camel.execution.ExecutionModel#getDataTypeModel <em>Data Type Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Type Model</em>' reference.
	 * @see #getDataTypeModel()
	 * @generated
	 */
	void setDataTypeModel(DataTypeModel value);

	/**
	 * Returns the value of the '<em><b>Measurements</b></em>' containment reference list.
	 * The list contents are of type {@link camel.execution.Measurement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measurements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measurements</em>' containment reference list.
	 * @see camel.execution.ExecutionPackage#getExecutionModel_Measurements()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Measurement> getMeasurements();

	/**
	 * Returns the value of the '<em><b>Slo Violations</b></em>' containment reference list.
	 * The list contents are of type {@link camel.execution.SLOViolation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slo Violations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slo Violations</em>' containment reference list.
	 * @see camel.execution.ExecutionPackage#getExecutionModel_SloViolations()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SLOViolation> getSloViolations();

	/**
	 * Returns the value of the '<em><b>Rule Triggers</b></em>' containment reference list.
	 * The list contents are of type {@link camel.execution.RuleTrigger}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Rule Triggers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Rule Triggers</em>' containment reference list.
	 * @see camel.execution.ExecutionPackage#getExecutionModel_RuleTriggers()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<RuleTrigger> getRuleTriggers();

	/**
	 * Returns the value of the '<em><b>History Records</b></em>' containment reference list.
	 * The list contents are of type {@link camel.execution.HistoryRecord}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>History Records</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>History Records</em>' containment reference list.
	 * @see camel.execution.ExecutionPackage#getExecutionModel_HistoryRecords()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<HistoryRecord> getHistoryRecords();

} // ExecutionModel
