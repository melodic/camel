/**
 */
package camel.execution;

import camel.deployment.Container;
import camel.deployment.ContainerInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Container Measurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.ContainerMeasurement#getContainerInstance <em>Container Instance</em>}</li>
 *   <li>{@link camel.execution.ContainerMeasurement#getContainer <em>Container</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getContainerMeasurement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correct_container_measurement'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot correct_container_measurement='Tuple {\n\tmessage : String = \'ContainerMeasurement: \' + self.name + \' should either map to an instance of a container or the container itself\',\n\tstatus : Boolean = \n\t\t\tasError((containerInstance &lt;&gt; null) xor (container &lt;&gt; null))\n}.status'"
 * @generated
 */
public interface ContainerMeasurement extends Measurement {
	/**
	 * Returns the value of the '<em><b>Container Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container Instance</em>' reference.
	 * @see #setContainerInstance(ContainerInstance)
	 * @see camel.execution.ExecutionPackage#getContainerMeasurement_ContainerInstance()
	 * @model
	 * @generated
	 */
	ContainerInstance getContainerInstance();

	/**
	 * Sets the value of the '{@link camel.execution.ContainerMeasurement#getContainerInstance <em>Container Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container Instance</em>' reference.
	 * @see #getContainerInstance()
	 * @generated
	 */
	void setContainerInstance(ContainerInstance value);

	/**
	 * Returns the value of the '<em><b>Container</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Container</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Container</em>' reference.
	 * @see #setContainer(Container)
	 * @see camel.execution.ExecutionPackage#getContainerMeasurement_Container()
	 * @model
	 * @generated
	 */
	Container getContainer();

	/**
	 * Sets the value of the '{@link camel.execution.ContainerMeasurement#getContainer <em>Container</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Container</em>' reference.
	 * @see #getContainer()
	 * @generated
	 */
	void setContainer(Container value);

} // ContainerMeasurement
