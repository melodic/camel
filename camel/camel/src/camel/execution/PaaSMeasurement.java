/**
 */
package camel.execution;

import camel.deployment.PaaS;
import camel.deployment.PaaSInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Paa SMeasurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.PaaSMeasurement#getPaasInstance <em>Paas Instance</em>}</li>
 *   <li>{@link camel.execution.PaaSMeasurement#getPaas <em>Paas</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getPaaSMeasurement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correct_paas_measurement'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot correct_paas_measurement='Tuple {\n\tmessage : String = \'PaaSMeasurement: \' + self.name + \' should either map to an instance of a PaaS or the PaaS itself\',\n\tstatus : Boolean = \n\t\t\tasError((paasInstance &lt;&gt; null) xor (paas &lt;&gt; null))\n}.status'"
 * @generated
 */
public interface PaaSMeasurement extends Measurement {
	/**
	 * Returns the value of the '<em><b>Paas Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Paas Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Paas Instance</em>' reference.
	 * @see #setPaasInstance(PaaSInstance)
	 * @see camel.execution.ExecutionPackage#getPaaSMeasurement_PaasInstance()
	 * @model
	 * @generated
	 */
	PaaSInstance getPaasInstance();

	/**
	 * Sets the value of the '{@link camel.execution.PaaSMeasurement#getPaasInstance <em>Paas Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Paas Instance</em>' reference.
	 * @see #getPaasInstance()
	 * @generated
	 */
	void setPaasInstance(PaaSInstance value);

	/**
	 * Returns the value of the '<em><b>Paas</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Paas</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Paas</em>' reference.
	 * @see #setPaas(PaaS)
	 * @see camel.execution.ExecutionPackage#getPaaSMeasurement_Paas()
	 * @model
	 * @generated
	 */
	PaaS getPaas();

	/**
	 * Sets the value of the '{@link camel.execution.PaaSMeasurement#getPaas <em>Paas</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Paas</em>' reference.
	 * @see #getPaas()
	 * @generated
	 */
	void setPaas(PaaS value);

} // PaaSMeasurement
