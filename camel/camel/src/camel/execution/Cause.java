/**
 */
package camel.execution;

import camel.core.Feature;

import camel.scalability.EventInstance;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cause</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.Cause#getEventInstances <em>Event Instances</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getCause()
 * @model
 * @generated
 */
public interface Cause extends Feature {
	/**
	 * Returns the value of the '<em><b>Event Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.scalability.EventInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Instances</em>' containment reference list.
	 * @see camel.execution.ExecutionPackage#getCause_EventInstances()
	 * @model containment="true"
	 * @generated
	 */
	EList<EventInstance> getEventInstances();

} // Cause
