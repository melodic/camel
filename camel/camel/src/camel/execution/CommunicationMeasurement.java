/**
 */
package camel.execution;

import camel.deployment.VM;
import camel.deployment.VMInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Communication Measurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.CommunicationMeasurement#getSourceVMInstance <em>Source VM Instance</em>}</li>
 *   <li>{@link camel.execution.CommunicationMeasurement#getTargetVMInstance <em>Target VM Instance</em>}</li>
 *   <li>{@link camel.execution.CommunicationMeasurement#getSourceVM <em>Source VM</em>}</li>
 *   <li>{@link camel.execution.CommunicationMeasurement#getTargetVM <em>Target VM</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getCommunicationMeasurement()
 * @model annotation="invariant to\040do='if containers not inside VMs always then communication should also refer to them'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correct_communication_measurement communication_meas_diff_source_target'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot correct_communication_measurement='Tuple {\n\tmessage : String = \'CommunicationMeasurement: \' + self.name + \' should either have a source and target VM instance or a source and target VM\',\n\tstatus : Boolean = \n\t\t\t \tasError((sourceVMInstance &lt;&gt; null and targetVMInstance &lt;&gt; null and sourceVM = null and targetVM = null) xor \n\t\t\t \t\t(sourceVM &lt;&gt; null and targetVM &lt;&gt; null and sourceVMInstance = null and targetVMInstance = null)\n\t\t\t \t)\n}.status' communication_meas_diff_source_target='Tuple {\n\tmessage : String = \'In CommunicationMeasurement: \' + self.name + \' source VM should be different from target VM and the same should hold for the instance level\',\n\tstatus : Boolean = \n\t\t\t \tasError((sourceVM &lt;&gt; null and targetVM &lt;&gt; null implies sourceVM &lt;&gt; targetVM) and \n\t\t\t \t(sourceVMInstance &lt;&gt; null and targetVMInstance &lt;&gt; null implies sourceVMInstance &lt;&gt; targetVMInstance))\n}.status'"
 * @generated
 */
public interface CommunicationMeasurement extends Measurement {
	/**
	 * Returns the value of the '<em><b>Source VM Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source VM Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source VM Instance</em>' reference.
	 * @see #setSourceVMInstance(VMInstance)
	 * @see camel.execution.ExecutionPackage#getCommunicationMeasurement_SourceVMInstance()
	 * @model
	 * @generated
	 */
	VMInstance getSourceVMInstance();

	/**
	 * Sets the value of the '{@link camel.execution.CommunicationMeasurement#getSourceVMInstance <em>Source VM Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source VM Instance</em>' reference.
	 * @see #getSourceVMInstance()
	 * @generated
	 */
	void setSourceVMInstance(VMInstance value);

	/**
	 * Returns the value of the '<em><b>Target VM Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target VM Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target VM Instance</em>' reference.
	 * @see #setTargetVMInstance(VMInstance)
	 * @see camel.execution.ExecutionPackage#getCommunicationMeasurement_TargetVMInstance()
	 * @model
	 * @generated
	 */
	VMInstance getTargetVMInstance();

	/**
	 * Sets the value of the '{@link camel.execution.CommunicationMeasurement#getTargetVMInstance <em>Target VM Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target VM Instance</em>' reference.
	 * @see #getTargetVMInstance()
	 * @generated
	 */
	void setTargetVMInstance(VMInstance value);

	/**
	 * Returns the value of the '<em><b>Source VM</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Source VM</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Source VM</em>' reference.
	 * @see #setSourceVM(VM)
	 * @see camel.execution.ExecutionPackage#getCommunicationMeasurement_SourceVM()
	 * @model
	 * @generated
	 */
	VM getSourceVM();

	/**
	 * Sets the value of the '{@link camel.execution.CommunicationMeasurement#getSourceVM <em>Source VM</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Source VM</em>' reference.
	 * @see #getSourceVM()
	 * @generated
	 */
	void setSourceVM(VM value);

	/**
	 * Returns the value of the '<em><b>Target VM</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Target VM</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Target VM</em>' reference.
	 * @see #setTargetVM(VM)
	 * @see camel.execution.ExecutionPackage#getCommunicationMeasurement_TargetVM()
	 * @model
	 * @generated
	 */
	VM getTargetVM();

	/**
	 * Sets the value of the '{@link camel.execution.CommunicationMeasurement#getTargetVM <em>Target VM</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Target VM</em>' reference.
	 * @see #getTargetVM()
	 * @generated
	 */
	void setTargetVM(VM value);

} // CommunicationMeasurement
