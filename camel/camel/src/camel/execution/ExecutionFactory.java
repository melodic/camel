/**
 */
package camel.execution;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see camel.execution.ExecutionPackage
 * @generated
 */
public interface ExecutionFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	ExecutionFactory eINSTANCE = camel.execution.impl.ExecutionFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model</em>'.
	 * @generated
	 */
	ExecutionModel createExecutionModel();

	/**
	 * Returns a new object of class '<em>Action Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Action Instance</em>'.
	 * @generated
	 */
	ActionInstance createActionInstance();

	/**
	 * Returns a new object of class '<em>Application Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Application Measurement</em>'.
	 * @generated
	 */
	ApplicationMeasurement createApplicationMeasurement();

	/**
	 * Returns a new object of class '<em>Software Component Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Software Component Measurement</em>'.
	 * @generated
	 */
	SoftwareComponentMeasurement createSoftwareComponentMeasurement();

	/**
	 * Returns a new object of class '<em>Communication Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Communication Measurement</em>'.
	 * @generated
	 */
	CommunicationMeasurement createCommunicationMeasurement();

	/**
	 * Returns a new object of class '<em>VM Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>VM Measurement</em>'.
	 * @generated
	 */
	VMMeasurement createVMMeasurement();

	/**
	 * Returns a new object of class '<em>Paa SMeasurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Paa SMeasurement</em>'.
	 * @generated
	 */
	PaaSMeasurement createPaaSMeasurement();

	/**
	 * Returns a new object of class '<em>Container Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Container Measurement</em>'.
	 * @generated
	 */
	ContainerMeasurement createContainerMeasurement();

	/**
	 * Returns a new object of class '<em>Data Measurement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Data Measurement</em>'.
	 * @generated
	 */
	DataMeasurement createDataMeasurement();

	/**
	 * Returns a new object of class '<em>SLO Violation</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>SLO Violation</em>'.
	 * @generated
	 */
	SLOViolation createSLOViolation();

	/**
	 * Returns a new object of class '<em>Rule Trigger</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Rule Trigger</em>'.
	 * @generated
	 */
	RuleTrigger createRuleTrigger();

	/**
	 * Returns a new object of class '<em>History Record</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>History Record</em>'.
	 * @generated
	 */
	HistoryRecord createHistoryRecord();

	/**
	 * Returns a new object of class '<em>History Info</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>History Info</em>'.
	 * @generated
	 */
	HistoryInfo createHistoryInfo();

	/**
	 * Returns a new object of class '<em>Cause</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Cause</em>'.
	 * @generated
	 */
	Cause createCause();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	ExecutionPackage getExecutionPackage();

} //ExecutionFactory
