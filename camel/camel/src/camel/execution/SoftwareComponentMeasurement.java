/**
 */
package camel.execution;

import camel.deployment.SoftwareComponent;
import camel.deployment.SoftwareComponentInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Software Component Measurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.SoftwareComponentMeasurement#getSoftwareComponentInstance <em>Software Component Instance</em>}</li>
 *   <li>{@link camel.execution.SoftwareComponentMeasurement#getSoftwareComponent <em>Software Component</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getSoftwareComponentMeasurement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correct_sw_component_measurement'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot correct_sw_component_measurement='Tuple {\n\tmessage : String = \'SoftwareComponentMeasurement: \' + self.name + \' should either map to an instance of a software component or the software component itself\',\n\tstatus : Boolean = \n\t\t\t\tasError((softwareComponentInstance &lt;&gt; null) xor (softwareComponent &lt;&gt; null))\n}.status'"
 * @generated
 */
public interface SoftwareComponentMeasurement extends Measurement {
	/**
	 * Returns the value of the '<em><b>Software Component Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software Component Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software Component Instance</em>' reference.
	 * @see #setSoftwareComponentInstance(SoftwareComponentInstance)
	 * @see camel.execution.ExecutionPackage#getSoftwareComponentMeasurement_SoftwareComponentInstance()
	 * @model
	 * @generated
	 */
	SoftwareComponentInstance getSoftwareComponentInstance();

	/**
	 * Sets the value of the '{@link camel.execution.SoftwareComponentMeasurement#getSoftwareComponentInstance <em>Software Component Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Software Component Instance</em>' reference.
	 * @see #getSoftwareComponentInstance()
	 * @generated
	 */
	void setSoftwareComponentInstance(SoftwareComponentInstance value);

	/**
	 * Returns the value of the '<em><b>Software Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software Component</em>' reference.
	 * @see #setSoftwareComponent(SoftwareComponent)
	 * @see camel.execution.ExecutionPackage#getSoftwareComponentMeasurement_SoftwareComponent()
	 * @model
	 * @generated
	 */
	SoftwareComponent getSoftwareComponent();

	/**
	 * Sets the value of the '{@link camel.execution.SoftwareComponentMeasurement#getSoftwareComponent <em>Software Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Software Component</em>' reference.
	 * @see #getSoftwareComponent()
	 * @generated
	 */
	void setSoftwareComponent(SoftwareComponent value);

} // SoftwareComponentMeasurement
