/**
 */
package camel.execution;

import camel.data.Data;
import camel.data.DataInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Measurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.DataMeasurement#getDataInstance <em>Data Instance</em>}</li>
 *   <li>{@link camel.execution.DataMeasurement#getData <em>Data</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getDataMeasurement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correct_data_measurement'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot correct_data_measurement='Tuple {\n\tmessage : String = \'DataMeasurement: \' + self.name + \' should either map to an instance of data or the data itself\',\n\tstatus : Boolean = \n\t\t\tasError((dataInstance &lt;&gt; null) xor (data &lt;&gt; null))\n}.status'"
 * @generated
 */
public interface DataMeasurement extends Measurement {
	/**
	 * Returns the value of the '<em><b>Data Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Instance</em>' reference.
	 * @see #setDataInstance(DataInstance)
	 * @see camel.execution.ExecutionPackage#getDataMeasurement_DataInstance()
	 * @model
	 * @generated
	 */
	DataInstance getDataInstance();

	/**
	 * Sets the value of the '{@link camel.execution.DataMeasurement#getDataInstance <em>Data Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data Instance</em>' reference.
	 * @see #getDataInstance()
	 * @generated
	 */
	void setDataInstance(DataInstance value);

	/**
	 * Returns the value of the '<em><b>Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data</em>' reference.
	 * @see #setData(Data)
	 * @see camel.execution.ExecutionPackage#getDataMeasurement_Data()
	 * @model
	 * @generated
	 */
	Data getData();

	/**
	 * Sets the value of the '{@link camel.execution.DataMeasurement#getData <em>Data</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data</em>' reference.
	 * @see #getData()
	 * @generated
	 */
	void setData(Data value);

} // DataMeasurement
