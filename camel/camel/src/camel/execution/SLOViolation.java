/**
 */
package camel.execution;

import camel.requirement.ServiceLevelObjective;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SLO Violation</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.SLOViolation#getSlo <em>Slo</em>}</li>
 *   <li>{@link camel.execution.SLOViolation#getMeasurement <em>Measurement</em>}</li>
 *   <li>{@link camel.execution.SLOViolation#getAssessmentTime <em>Assessment Time</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getSLOViolation()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='slo_violation_same_metric slo_violation_slo_in_reqs_for_execution_model'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot slo_violation_same_metric='Tuple {\n\tmessage : String = \'The metric in slo: \' + slo.name + \' of SLOViolation: \' + self.name + \' is not the same as the one referenced by the respective measurement: \' + measurement.name,\n\tstatus : Boolean = \n\t\t\tasError(\n\t\t\t\tif (slo.constraint.oclIsTypeOf(camel::constraint::MetricConstraint)\n\t\t\t\t\t)\n\t\t\t\tthen \n\t\t\t\t\tmeasurement.metricInstance.metricContext.metric = slo.constraint.oclAsType(camel::constraint::MetricConstraint).metricContext.metric\n\t\t\t\telse false\n\t\t\t\tendif)\n}.status' slo_violation_slo_in_reqs_for_execution_model='Tuple {\n\tmessage : String = \'SLO: \' + self.slo.name + \' is not included in the requirement model of the current execution model: \' + self.oclContainer().oclAsType(ExecutionModel).name + \' of SLO Violation: \' + self.name,\n\tstatus : Boolean = \n\t\t\t\tasError(self.oclContainer().oclAsType(ExecutionModel).requirementModel.requirements\n\t\t\t\t-&gt;includes(self.slo))\n}.status'"
 * @generated
 */
public interface SLOViolation extends Cause {
	/**
	 * Returns the value of the '<em><b>Slo</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Slo</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Slo</em>' reference.
	 * @see #setSlo(ServiceLevelObjective)
	 * @see camel.execution.ExecutionPackage#getSLOViolation_Slo()
	 * @model required="true"
	 * @generated
	 */
	ServiceLevelObjective getSlo();

	/**
	 * Sets the value of the '{@link camel.execution.SLOViolation#getSlo <em>Slo</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Slo</em>' reference.
	 * @see #getSlo()
	 * @generated
	 */
	void setSlo(ServiceLevelObjective value);

	/**
	 * Returns the value of the '<em><b>Measurement</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measurement</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measurement</em>' reference.
	 * @see #setMeasurement(Measurement)
	 * @see camel.execution.ExecutionPackage#getSLOViolation_Measurement()
	 * @model required="true"
	 * @generated
	 */
	Measurement getMeasurement();

	/**
	 * Sets the value of the '{@link camel.execution.SLOViolation#getMeasurement <em>Measurement</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Measurement</em>' reference.
	 * @see #getMeasurement()
	 * @generated
	 */
	void setMeasurement(Measurement value);

	/**
	 * Returns the value of the '<em><b>Assessment Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assessment Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assessment Time</em>' attribute.
	 * @see #setAssessmentTime(Date)
	 * @see camel.execution.ExecutionPackage#getSLOViolation_AssessmentTime()
	 * @model required="true"
	 * @generated
	 */
	Date getAssessmentTime();

	/**
	 * Sets the value of the '{@link camel.execution.SLOViolation#getAssessmentTime <em>Assessment Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assessment Time</em>' attribute.
	 * @see #getAssessmentTime()
	 * @generated
	 */
	void setAssessmentTime(Date value);

} // SLOViolation
