/**
 */
package camel.execution;

import camel.deployment.VM;
import camel.deployment.VMInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>VM Measurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.VMMeasurement#getVmInstance <em>Vm Instance</em>}</li>
 *   <li>{@link camel.execution.VMMeasurement#getVm <em>Vm</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getVMMeasurement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='correct_vm_measurement'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot correct_vm_measurement='Tuple {\n\tmessage : String = \'VMMeasurement: \' + self.name + \' should either map to an instance of a VM or the VM itself\',\n\tstatus : Boolean = \n\t\t\tasError((vmInstance &lt;&gt; null) xor (vm &lt;&gt; null))\n}.status'"
 * @generated
 */
public interface VMMeasurement extends Measurement {
	/**
	 * Returns the value of the '<em><b>Vm Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vm Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vm Instance</em>' reference.
	 * @see #setVmInstance(VMInstance)
	 * @see camel.execution.ExecutionPackage#getVMMeasurement_VmInstance()
	 * @model
	 * @generated
	 */
	VMInstance getVmInstance();

	/**
	 * Sets the value of the '{@link camel.execution.VMMeasurement#getVmInstance <em>Vm Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vm Instance</em>' reference.
	 * @see #getVmInstance()
	 * @generated
	 */
	void setVmInstance(VMInstance value);

	/**
	 * Returns the value of the '<em><b>Vm</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Vm</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Vm</em>' reference.
	 * @see #setVm(VM)
	 * @see camel.execution.ExecutionPackage#getVMMeasurement_Vm()
	 * @model
	 * @generated
	 */
	VM getVm();

	/**
	 * Sets the value of the '{@link camel.execution.VMMeasurement#getVm <em>Vm</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Vm</em>' reference.
	 * @see #getVm()
	 * @generated
	 */
	void setVm(VM value);

} // VMMeasurement
