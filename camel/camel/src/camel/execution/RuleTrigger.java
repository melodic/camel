/**
 */
package camel.execution;

import camel.scalability.ScalabilityRule;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Rule Trigger</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.RuleTrigger#getScalabilityRule <em>Scalability Rule</em>}</li>
 *   <li>{@link camel.execution.RuleTrigger#getTrigerringTime <em>Trigerring Time</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getRuleTrigger()
 * @model
 * @generated
 */
public interface RuleTrigger extends Cause {
	/**
	 * Returns the value of the '<em><b>Scalability Rule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scalability Rule</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scalability Rule</em>' reference.
	 * @see #setScalabilityRule(ScalabilityRule)
	 * @see camel.execution.ExecutionPackage#getRuleTrigger_ScalabilityRule()
	 * @model required="true"
	 * @generated
	 */
	ScalabilityRule getScalabilityRule();

	/**
	 * Sets the value of the '{@link camel.execution.RuleTrigger#getScalabilityRule <em>Scalability Rule</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Scalability Rule</em>' reference.
	 * @see #getScalabilityRule()
	 * @generated
	 */
	void setScalabilityRule(ScalabilityRule value);

	/**
	 * Returns the value of the '<em><b>Trigerring Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Trigerring Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Trigerring Time</em>' attribute.
	 * @see #setTrigerringTime(Date)
	 * @see camel.execution.ExecutionPackage#getRuleTrigger_TrigerringTime()
	 * @model required="true"
	 * @generated
	 */
	Date getTrigerringTime();

	/**
	 * Sets the value of the '{@link camel.execution.RuleTrigger#getTrigerringTime <em>Trigerring Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Trigerring Time</em>' attribute.
	 * @see #getTrigerringTime()
	 * @generated
	 */
	void setTrigerringTime(Date value);

} // RuleTrigger
