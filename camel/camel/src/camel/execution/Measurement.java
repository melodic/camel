/**
 */
package camel.execution;

import camel.core.Feature;

import camel.metric.MetricInstance;

import camel.scalability.EventInstance;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Measurement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.execution.Measurement#getMetricInstance <em>Metric Instance</em>}</li>
 *   <li>{@link camel.execution.Measurement#getValue <em>Value</em>}</li>
 *   <li>{@link camel.execution.Measurement#getMeasurementTime <em>Measurement Time</em>}</li>
 *   <li>{@link camel.execution.Measurement#getEventInstance <em>Event Instance</em>}</li>
 * </ul>
 *
 * @see camel.execution.ExecutionPackage#getMeasurement()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='measurement_event_instance_same_metric correct_measurement_value measurement_metric_refers_to_correct_execution_model'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot measurement_event_instance_same_metric='Tuple {\n\tmessage : String = \'Measurement: \' + self.name + \' maps to non-functional event instance : \' + eventInstance.toString() + \' with a type related to a metric different from the one of this measurement\',\n\tstatus : Boolean = \n\t\t\tasError(self.eventInstance &lt;&gt; null and\n\t\t\t\tself.eventInstance.type.oclIsTypeOf(camel::scalability::NonFunctionalEvent) implies self.metricInstance.metricContext.metric =\n\t\t\t\tself.eventInstance.type.oclAsType(camel::scalability::NonFunctionalEvent).metricConstraint.metricContext.metric)\n}.status' correct_measurement_value='Tuple {\n\tmessage : String = \'Measurement: \' + self.name + \' has a value: \' + value.toString() + \' that is outside the range of values of metric instance: \' + metricInstance.name,\n\tstatus : Boolean = \n\t\t\tasError(\n\t\t\tlet type :\n\t\t\t\tcamel::type::ValueType = metricInstance.metricContext.metric.metricTemplate.valueType\n\t\t\tin if (type &lt;&gt; null)\n\t\t\t\tthen if (type.oclIsTypeOf(camel::type::Range))\n\t\t\t\t\tthen type.oclAsType(camel::type::Range).includesValue(self.value)\n\t\t\t\t\telse if (type.oclIsTypeOf(camel::type::RangeUnion))\n\t\t\t\t\t\tthen type.oclAsType(camel::type::RangeUnion).includesValue(self.value)\n\t\t\t\t\t\telse true\n\t\t\t\t\t\tendif\n\t\t\t\t\tendif\n\t\t\t\telse true\n\t\t\t\tendif)\n}.status' measurement_metric_refers_to_correct_execution_model='Tuple {\n\tmessage : String = \'Measurement: \' + self.name + \' does not belong to the same execution model as the one of the binding of the respective metric instance:\' + metricInstance.name,\n\tstatus : Boolean = \n\t\t\t\tasError(self.oclContainer().oclAsType(ExecutionModel) = metricInstance.objectBinding.executionModel)\n}.status'"
 * @generated
 */
public interface Measurement extends Feature {
	/**
	 * Returns the value of the '<em><b>Metric Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Instance</em>' reference.
	 * @see #setMetricInstance(MetricInstance)
	 * @see camel.execution.ExecutionPackage#getMeasurement_MetricInstance()
	 * @model required="true"
	 * @generated
	 */
	MetricInstance getMetricInstance();

	/**
	 * Sets the value of the '{@link camel.execution.Measurement#getMetricInstance <em>Metric Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metric Instance</em>' reference.
	 * @see #getMetricInstance()
	 * @generated
	 */
	void setMetricInstance(MetricInstance value);

	/**
	 * Returns the value of the '<em><b>Value</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' attribute.
	 * @see #setValue(double)
	 * @see camel.execution.ExecutionPackage#getMeasurement_Value()
	 * @model required="true"
	 * @generated
	 */
	double getValue();

	/**
	 * Sets the value of the '{@link camel.execution.Measurement#getValue <em>Value</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' attribute.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(double value);

	/**
	 * Returns the value of the '<em><b>Measurement Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Measurement Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Measurement Time</em>' attribute.
	 * @see #setMeasurementTime(Date)
	 * @see camel.execution.ExecutionPackage#getMeasurement_MeasurementTime()
	 * @model required="true"
	 * @generated
	 */
	Date getMeasurementTime();

	/**
	 * Sets the value of the '{@link camel.execution.Measurement#getMeasurementTime <em>Measurement Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Measurement Time</em>' attribute.
	 * @see #getMeasurementTime()
	 * @generated
	 */
	void setMeasurementTime(Date value);

	/**
	 * Returns the value of the '<em><b>Event Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Event Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Event Instance</em>' reference.
	 * @see #setEventInstance(EventInstance)
	 * @see camel.execution.ExecutionPackage#getMeasurement_EventInstance()
	 * @model
	 * @generated
	 */
	EventInstance getEventInstance();

	/**
	 * Sets the value of the '{@link camel.execution.Measurement#getEventInstance <em>Event Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Event Instance</em>' reference.
	 * @see #getEventInstance()
	 * @generated
	 */
	void setEventInstance(EventInstance value);

} // Measurement
