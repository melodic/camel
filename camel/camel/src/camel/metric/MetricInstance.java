/**
 */
package camel.metric;

import camel.core.Feature;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.MetricInstance#getObjectBinding <em>Object Binding</em>}</li>
 *   <li>{@link camel.metric.MetricInstance#getMetricContext <em>Metric Context</em>}</li>
 *   <li>{@link camel.metric.MetricInstance#getComposingMetricInstances <em>Composing Metric Instances</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getMetricInstance()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='composite_metric_instance_to_components component_instances_metric_map_component_metrics'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot composite_metric_instance_to_components='Tuple {\n\tmessage : String = \'Instance: \' + self.name + \' of composite metric:\' + self.metricContext.metric.name + \'should have the same execution model as the one of all of its component metric instances\',\n\tstatus : Boolean = \n\t\t\t\tasError(metricContext.metric.oclIsTypeOf(CompositeMetric)\n\t\t\t\t\timplies self.composingMetricInstances-&gt;forAll(p | p.objectBinding.executionModel = self.objectBinding.executionModel))\n}.status' component_instances_metric_map_component_metrics='Tuple {\n\tmessage : String = \'CompositeMetricInstance: \' + self.name + \' should have component metric instances which map to the component metrics referenced by the composite instance\\\'s metric\',\n\tstatus : Boolean = \n\t\t\t\tasError(metricContext.metric.oclIsTypeOf(CompositeMetric)\n\t\t\t\t\timplies self.metricContext.metric.oclAsType(CompositeMetric).componentMetrics\n\t\t\t\t\t-&gt;forAll(p | self.composingMetricInstances\n\t\t\t\t\t\t-&gt;exists(q | q.metricContext.metric = p))\n\t\t\t\t\tand\n\t\t\t\t\tself.composingMetricInstances-&gt;forAll(inst | self.metricContext.metric.oclAsType(CompositeMetric).componentMetrics-&gt;exists(comp | comp = inst.metricContext.metric)))\n}.status'"
 * @generated
 */
public interface MetricInstance extends Feature {
	/**
	 * Returns the value of the '<em><b>Object Binding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Binding</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Binding</em>' reference.
	 * @see #setObjectBinding(MetricObjectBinding)
	 * @see camel.metric.MetricPackage#getMetricInstance_ObjectBinding()
	 * @model required="true"
	 * @generated
	 */
	MetricObjectBinding getObjectBinding();

	/**
	 * Sets the value of the '{@link camel.metric.MetricInstance#getObjectBinding <em>Object Binding</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Binding</em>' reference.
	 * @see #getObjectBinding()
	 * @generated
	 */
	void setObjectBinding(MetricObjectBinding value);

	/**
	 * Returns the value of the '<em><b>Metric Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Context</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Context</em>' reference.
	 * @see #setMetricContext(MetricContext)
	 * @see camel.metric.MetricPackage#getMetricInstance_MetricContext()
	 * @model
	 * @generated
	 */
	MetricContext getMetricContext();

	/**
	 * Sets the value of the '{@link camel.metric.MetricInstance#getMetricContext <em>Metric Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metric Context</em>' reference.
	 * @see #getMetricContext()
	 * @generated
	 */
	void setMetricContext(MetricContext value);

	/**
	 * Returns the value of the '<em><b>Composing Metric Instances</b></em>' reference list.
	 * The list contents are of type {@link camel.metric.MetricInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composing Metric Instances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composing Metric Instances</em>' reference list.
	 * @see camel.metric.MetricPackage#getMetricInstance_ComposingMetricInstances()
	 * @model ordered="false"
	 * @generated
	 */
	EList<MetricInstance> getComposingMetricInstances();

} // MetricInstance
