/**
 */
package camel.metric;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.eclipse.emf.common.util.Enumerator;

/**
 * <!-- begin-user-doc -->
 * A representation of the literals of the enumeration '<em><b>Grouping Type</b></em>',
 * and utility methods for working with them.
 * <!-- end-user-doc -->
 * @see camel.metric.MetricPackage#getGroupingType()
 * @model
 * @generated
 */
public enum GroupingType implements Enumerator {
	/**
	 * The '<em><b>PER INSTANCE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PER_INSTANCE_VALUE
	 * @generated
	 * @ordered
	 */
	PER_INSTANCE(0, "PER_INSTANCE", "PER_INSTANCE"),

	/**
	 * The '<em><b>PER HOST</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PER_HOST_VALUE
	 * @generated
	 * @ordered
	 */
	PER_HOST(1, "PER_HOST", "PER_HOST"),

	/**
	 * The '<em><b>PER ZONE</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PER_ZONE_VALUE
	 * @generated
	 * @ordered
	 */
	PER_ZONE(2, "PER_ZONE", "PER_ZONE"),

	/**
	 * The '<em><b>PER REGION</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PER_REGION_VALUE
	 * @generated
	 * @ordered
	 */
	PER_REGION(3, "PER_REGION", "PER_REGION"),

	/**
	 * The '<em><b>PER CLOUD</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #PER_CLOUD_VALUE
	 * @generated
	 * @ordered
	 */
	PER_CLOUD(4, "PER_CLOUD", "PER_CLOUD"),

	/**
	 * The '<em><b>GLOBAL</b></em>' literal object.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #GLOBAL_VALUE
	 * @generated
	 * @ordered
	 */
	GLOBAL(5, "GLOBAL", "GLOBAL");

	/**
	 * The '<em><b>PER INSTANCE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PER INSTANCE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PER_INSTANCE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PER_INSTANCE_VALUE = 0;

	/**
	 * The '<em><b>PER HOST</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PER HOST</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PER_HOST
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PER_HOST_VALUE = 1;

	/**
	 * The '<em><b>PER ZONE</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PER ZONE</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PER_ZONE
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PER_ZONE_VALUE = 2;

	/**
	 * The '<em><b>PER REGION</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PER REGION</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PER_REGION
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PER_REGION_VALUE = 3;

	/**
	 * The '<em><b>PER CLOUD</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>PER CLOUD</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #PER_CLOUD
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int PER_CLOUD_VALUE = 4;

	/**
	 * The '<em><b>GLOBAL</b></em>' literal value.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of '<em><b>GLOBAL</b></em>' literal object isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @see #GLOBAL
	 * @model
	 * @generated
	 * @ordered
	 */
	public static final int GLOBAL_VALUE = 5;

	/**
	 * An array of all the '<em><b>Grouping Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final GroupingType[] VALUES_ARRAY =
		new GroupingType[] {
			PER_INSTANCE,
			PER_HOST,
			PER_ZONE,
			PER_REGION,
			PER_CLOUD,
			GLOBAL,
		};

	/**
	 * A public read-only list of all the '<em><b>Grouping Type</b></em>' enumerators.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final List<GroupingType> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));

	/**
	 * Returns the '<em><b>Grouping Type</b></em>' literal with the specified literal value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param literal the literal.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GroupingType get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			GroupingType result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Grouping Type</b></em>' literal with the specified name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param name the name.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GroupingType getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			GroupingType result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}

	/**
	 * Returns the '<em><b>Grouping Type</b></em>' literal with the specified integer value.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the integer value.
	 * @return the matching enumerator or <code>null</code>.
	 * @generated
	 */
	public static GroupingType get(int value) {
		switch (value) {
			case PER_INSTANCE_VALUE: return PER_INSTANCE;
			case PER_HOST_VALUE: return PER_HOST;
			case PER_ZONE_VALUE: return PER_ZONE;
			case PER_REGION_VALUE: return PER_REGION;
			case PER_CLOUD_VALUE: return PER_CLOUD;
			case GLOBAL_VALUE: return GLOBAL;
		}
		return null;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final int value;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String name;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private final String literal;

	/**
	 * Only this class can construct instances.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private GroupingType(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getValue() {
	  return value;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
	  return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLiteral() {
	  return literal;
	}

	/**
	 * Returns the literal value of the enumerator, which is its string representation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		return literal;
	}
	
} //GroupingType
