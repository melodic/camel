/**
 */
package camel.metric;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Raw Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.metric.MetricPackage#getRawMetric()
 * @model
 * @generated
 */
public interface RawMetric extends Metric {
} // RawMetric
