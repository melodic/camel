/**
 */
package camel.metric;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.Metric#getMetricTemplate <em>Metric Template</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getMetric()
 * @model abstract="true"
 * @generated
 */
public interface Metric extends Feature {
	/**
	 * Returns the value of the '<em><b>Metric Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Template</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Template</em>' reference.
	 * @see #setMetricTemplate(MetricTemplate)
	 * @see camel.metric.MetricPackage#getMetric_MetricTemplate()
	 * @model required="true"
	 * @generated
	 */
	MetricTemplate getMetricTemplate();

	/**
	 * Sets the value of the '{@link camel.metric.Metric#getMetricTemplate <em>Metric Template</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metric Template</em>' reference.
	 * @see #getMetricTemplate()
	 * @generated
	 */
	void setMetricTemplate(MetricTemplate value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='System.out.println(\"Checking recursiveness for Metric: \" + mt1.getName());\n\t\t\t\tcamel.metric.CompositeMetric m1 = (camel.metric.CompositeMetric)mt1;\n\t\t\t\tfor (camel.metric.Metric mt: m1.getComponentMetrics()){\n\t\t\t\t\t\tif (mt.getName().equals(mt2.getName())) return Boolean.TRUE;\n\t\t\t\t\t\tif (mt instanceof camel.metric.CompositeMetric &amp;&amp; checkRecursiveness(mt,mt2)) return Boolean.TRUE;\n\t\t\t\t}\n\t\t\t\treturn Boolean.FALSE;'"
	 * @generated
	 */
	boolean checkRecursiveness(Metric mt1, Metric mt2);

} // Metric
