/**
 */
package camel.metric;

import camel.core.Feature;

import camel.data.Data;

import camel.deployment.Component;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.ObjectContext#getComponent <em>Component</em>}</li>
 *   <li>{@link camel.metric.ObjectContext#getData <em>Data</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getObjectContext()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='object_context_either_data_component'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot object_context_either_data_component='Tuple {\n\tmessage : String = \'In ObjectContext: \' + self.name + \' either the component or data should be supplied\',\n\tstatus : Boolean = \n\t\t\t\tasError(component &lt;&gt; null or data &lt;&gt; null)\n}.status'"
 * @generated
 */
public interface ObjectContext extends Feature {
	/**
	 * Returns the value of the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component</em>' reference.
	 * @see #setComponent(Component)
	 * @see camel.metric.MetricPackage#getObjectContext_Component()
	 * @model
	 * @generated
	 */
	Component getComponent();

	/**
	 * Sets the value of the '{@link camel.metric.ObjectContext#getComponent <em>Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component</em>' reference.
	 * @see #getComponent()
	 * @generated
	 */
	void setComponent(Component value);

	/**
	 * Returns the value of the '<em><b>Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data</em>' reference.
	 * @see #setData(Data)
	 * @see camel.metric.MetricPackage#getObjectContext_Data()
	 * @model
	 * @generated
	 */
	Data getData();

	/**
	 * Sets the value of the '{@link camel.metric.ObjectContext#getData <em>Data</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Data</em>' reference.
	 * @see #getData()
	 * @generated
	 */
	void setData(Data value);

} // ObjectContext
