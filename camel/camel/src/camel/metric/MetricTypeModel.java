/**
 */
package camel.metric;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Type Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.MetricTypeModel#getMetricContexts <em>Metric Contexts</em>}</li>
 *   <li>{@link camel.metric.MetricTypeModel#getAttributeContexts <em>Attribute Contexts</em>}</li>
 *   <li>{@link camel.metric.MetricTypeModel#getObjectContexts <em>Object Contexts</em>}</li>
 *   <li>{@link camel.metric.MetricTypeModel#getMetrics <em>Metrics</em>}</li>
 *   <li>{@link camel.metric.MetricTypeModel#getWindows <em>Windows</em>}</li>
 *   <li>{@link camel.metric.MetricTypeModel#getSchedules <em>Schedules</em>}</li>
 *   <li>{@link camel.metric.MetricTypeModel#getSensors <em>Sensors</em>}</li>
 *   <li>{@link camel.metric.MetricTypeModel#getFunctions <em>Functions</em>}</li>
 *   <li>{@link camel.metric.MetricTypeModel#getTemplates <em>Templates</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getMetricTypeModel()
 * @model
 * @generated
 */
public interface MetricTypeModel extends MetricModel {
	/**
	 * Returns the value of the '<em><b>Metric Contexts</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.MetricContext}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Contexts</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Contexts</em>' containment reference list.
	 * @see camel.metric.MetricPackage#getMetricTypeModel_MetricContexts()
	 * @model containment="true"
	 * @generated
	 */
	EList<MetricContext> getMetricContexts();

	/**
	 * Returns the value of the '<em><b>Attribute Contexts</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.AttributeContext}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute Contexts</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute Contexts</em>' containment reference list.
	 * @see camel.metric.MetricPackage#getMetricTypeModel_AttributeContexts()
	 * @model containment="true"
	 * @generated
	 */
	EList<AttributeContext> getAttributeContexts();

	/**
	 * Returns the value of the '<em><b>Object Contexts</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.ObjectContext}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Contexts</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Contexts</em>' containment reference list.
	 * @see camel.metric.MetricPackage#getMetricTypeModel_ObjectContexts()
	 * @model containment="true"
	 * @generated
	 */
	EList<ObjectContext> getObjectContexts();

	/**
	 * Returns the value of the '<em><b>Metrics</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.Metric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metrics</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metrics</em>' containment reference list.
	 * @see camel.metric.MetricPackage#getMetricTypeModel_Metrics()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Metric> getMetrics();

	/**
	 * Returns the value of the '<em><b>Windows</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.Window}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Windows</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Windows</em>' containment reference list.
	 * @see camel.metric.MetricPackage#getMetricTypeModel_Windows()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Window> getWindows();

	/**
	 * Returns the value of the '<em><b>Schedules</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.Schedule}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Schedules</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Schedules</em>' containment reference list.
	 * @see camel.metric.MetricPackage#getMetricTypeModel_Schedules()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Schedule> getSchedules();

	/**
	 * Returns the value of the '<em><b>Sensors</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.Sensor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sensors</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sensors</em>' containment reference list.
	 * @see camel.metric.MetricPackage#getMetricTypeModel_Sensors()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Sensor> getSensors();

	/**
	 * Returns the value of the '<em><b>Functions</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.Function}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Functions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Functions</em>' containment reference list.
	 * @see camel.metric.MetricPackage#getMetricTypeModel_Functions()
	 * @model containment="true"
	 * @generated
	 */
	EList<Function> getFunctions();

	/**
	 * Returns the value of the '<em><b>Templates</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.MetricTemplate}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Templates</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Templates</em>' containment reference list.
	 * @see camel.metric.MetricPackage#getMetricTypeModel_Templates()
	 * @model containment="true"
	 * @generated
	 */
	EList<MetricTemplate> getTemplates();

} // MetricTypeModel
