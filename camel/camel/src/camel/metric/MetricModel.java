/**
 */
package camel.metric;

import camel.core.Model;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.metric.MetricPackage#getMetricModel()
 * @model abstract="true"
 * @generated
 */
public interface MetricModel extends Model {

} // MetricModel
