/**
 */
package camel.metric;

import camel.core.Feature;

import camel.unit.Unit;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Schedule</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.Schedule#getStart <em>Start</em>}</li>
 *   <li>{@link camel.metric.Schedule#getEnd <em>End</em>}</li>
 *   <li>{@link camel.metric.Schedule#getTimeUnit <em>Time Unit</em>}</li>
 *   <li>{@link camel.metric.Schedule#getRepetitions <em>Repetitions</em>}</li>
 *   <li>{@link camel.metric.Schedule#getInterval <em>Interval</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getSchedule()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='schedule_correct_interval'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot schedule_correct_interval='Tuple {\n\tmessage : String = \'Schedule: \' + self.name + \' should have a correct interval\',\n\tstatus : Boolean = \n\t\t\t\tasError(interval &gt;0)\n}.status'"
 * @generated
 */
public interface Schedule extends Feature {
	/**
	 * Returns the value of the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start</em>' attribute.
	 * @see #setStart(Date)
	 * @see camel.metric.MetricPackage#getSchedule_Start()
	 * @model
	 * @generated
	 */
	Date getStart();

	/**
	 * Sets the value of the '{@link camel.metric.Schedule#getStart <em>Start</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start</em>' attribute.
	 * @see #getStart()
	 * @generated
	 */
	void setStart(Date value);

	/**
	 * Returns the value of the '<em><b>End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End</em>' attribute.
	 * @see #setEnd(Date)
	 * @see camel.metric.MetricPackage#getSchedule_End()
	 * @model
	 * @generated
	 */
	Date getEnd();

	/**
	 * Sets the value of the '{@link camel.metric.Schedule#getEnd <em>End</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End</em>' attribute.
	 * @see #getEnd()
	 * @generated
	 */
	void setEnd(Date value);

	/**
	 * Returns the value of the '<em><b>Time Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Time Unit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Time Unit</em>' reference.
	 * @see #setTimeUnit(Unit)
	 * @see camel.metric.MetricPackage#getSchedule_TimeUnit()
	 * @model required="true"
	 * @generated
	 */
	Unit getTimeUnit();

	/**
	 * Sets the value of the '{@link camel.metric.Schedule#getTimeUnit <em>Time Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Time Unit</em>' reference.
	 * @see #getTimeUnit()
	 * @generated
	 */
	void setTimeUnit(Unit value);

	/**
	 * Returns the value of the '<em><b>Repetitions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Repetitions</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Repetitions</em>' attribute.
	 * @see #setRepetitions(int)
	 * @see camel.metric.MetricPackage#getSchedule_Repetitions()
	 * @model required="true"
	 * @generated
	 */
	int getRepetitions();

	/**
	 * Sets the value of the '{@link camel.metric.Schedule#getRepetitions <em>Repetitions</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Repetitions</em>' attribute.
	 * @see #getRepetitions()
	 * @generated
	 */
	void setRepetitions(int value);

	/**
	 * Returns the value of the '<em><b>Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Interval</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Interval</em>' attribute.
	 * @see #setInterval(long)
	 * @see camel.metric.MetricPackage#getSchedule_Interval()
	 * @model required="true"
	 * @generated
	 */
	long getInterval();

	/**
	 * Sets the value of the '{@link camel.metric.Schedule#getInterval <em>Interval</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Interval</em>' attribute.
	 * @see #getInterval()
	 * @generated
	 */
	void setInterval(long value);

} // Schedule
