/**
 */
package camel.metric;

import camel.core.Attribute;
import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.AttributeContext#getAttribute <em>Attribute</em>}</li>
 *   <li>{@link camel.metric.AttributeContext#getObjectContext <em>Object Context</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getAttributeContext()
 * @model
 * @generated
 */
public interface AttributeContext extends Feature {
	/**
	 * Returns the value of the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute</em>' reference.
	 * @see #setAttribute(Attribute)
	 * @see camel.metric.MetricPackage#getAttributeContext_Attribute()
	 * @model required="true"
	 * @generated
	 */
	Attribute getAttribute();

	/**
	 * Sets the value of the '{@link camel.metric.AttributeContext#getAttribute <em>Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute</em>' reference.
	 * @see #getAttribute()
	 * @generated
	 */
	void setAttribute(Attribute value);

	/**
	 * Returns the value of the '<em><b>Object Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Object Context</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Context</em>' reference.
	 * @see #setObjectContext(ObjectContext)
	 * @see camel.metric.MetricPackage#getAttributeContext_ObjectContext()
	 * @model
	 * @generated
	 */
	ObjectContext getObjectContext();

	/**
	 * Sets the value of the '{@link camel.metric.AttributeContext#getObjectContext <em>Object Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Object Context</em>' reference.
	 * @see #getObjectContext()
	 * @generated
	 */
	void setObjectContext(ObjectContext value);

} // AttributeContext
