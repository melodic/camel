/**
 */
package camel.metric;

import camel.core.CorePackage;

import camel.deployment.DeploymentPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see camel.metric.MetricFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface MetricPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "metric";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.camel-dsl.org/camel/metric";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "metric";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	MetricPackage eINSTANCE = camel.metric.impl.MetricPackageImpl.init();

	/**
	 * The meta object id for the '{@link camel.metric.impl.MetricInstanceImpl <em>Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.MetricInstanceImpl
	 * @see camel.metric.impl.MetricPackageImpl#getMetricInstance()
	 * @generated
	 */
	int METRIC_INSTANCE = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Object Binding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE__OBJECT_BINDING = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Metric Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE__METRIC_CONTEXT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Composing Metric Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE__COMPOSING_METRIC_INSTANCES = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.MetricImpl <em>Metric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.MetricImpl
	 * @see camel.metric.impl.MetricPackageImpl#getMetric()
	 * @generated
	 */
	int METRIC = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC__METRIC_TEMPLATE = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Check Recursiveness</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC___CHECK_RECURSIVENESS__METRIC_METRIC = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link camel.metric.impl.CompositeMetricImpl <em>Composite Metric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.CompositeMetricImpl
	 * @see camel.metric.impl.MetricPackageImpl#getCompositeMetric()
	 * @generated
	 */
	int COMPOSITE_METRIC = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC__NAME = METRIC__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC__DESCRIPTION = METRIC__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC__ANNOTATIONS = METRIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC__ATTRIBUTES = METRIC__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC__SUB_FEATURES = METRIC__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC__METRIC_TEMPLATE = METRIC__METRIC_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Formula</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC__FORMULA = METRIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component Metrics</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC__COMPONENT_METRICS = METRIC_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Composite Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_FEATURE_COUNT = METRIC_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC___AS_ERROR__BOOLEAN = METRIC___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Check Recursiveness</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC___CHECK_RECURSIVENESS__METRIC_METRIC = METRIC___CHECK_RECURSIVENESS__METRIC_METRIC;

	/**
	 * The operation id for the '<em>Contains Metric</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC___CONTAINS_METRIC__METRIC = METRIC_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Composite Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_OPERATION_COUNT = METRIC_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link camel.metric.impl.RawMetricImpl <em>Raw Metric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.RawMetricImpl
	 * @see camel.metric.impl.MetricPackageImpl#getRawMetric()
	 * @generated
	 */
	int RAW_METRIC = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC__NAME = METRIC__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC__DESCRIPTION = METRIC__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC__ANNOTATIONS = METRIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC__ATTRIBUTES = METRIC__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC__SUB_FEATURES = METRIC__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC__METRIC_TEMPLATE = METRIC__METRIC_TEMPLATE;

	/**
	 * The number of structural features of the '<em>Raw Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_FEATURE_COUNT = METRIC_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC___AS_ERROR__BOOLEAN = METRIC___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Check Recursiveness</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC___CHECK_RECURSIVENESS__METRIC_METRIC = METRIC___CHECK_RECURSIVENESS__METRIC_METRIC;

	/**
	 * The number of operations of the '<em>Raw Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_OPERATION_COUNT = METRIC_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.MetricVariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.MetricVariableImpl
	 * @see camel.metric.impl.MetricPackageImpl#getMetricVariable()
	 * @generated
	 */
	int METRIC_VARIABLE = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE__NAME = METRIC__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE__DESCRIPTION = METRIC__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE__ANNOTATIONS = METRIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE__ATTRIBUTES = METRIC__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE__SUB_FEATURES = METRIC__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE__METRIC_TEMPLATE = METRIC__METRIC_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Current Configuration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE__CURRENT_CONFIGURATION = METRIC_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE__COMPONENT = METRIC_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>On Node Candidates</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE__ON_NODE_CANDIDATES = METRIC_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Formula</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE__FORMULA = METRIC_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Component Metrics</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE__COMPONENT_METRICS = METRIC_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_FEATURE_COUNT = METRIC_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE___AS_ERROR__BOOLEAN = METRIC___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Check Recursiveness</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE___CHECK_RECURSIVENESS__METRIC_METRIC = METRIC___CHECK_RECURSIVENESS__METRIC_METRIC;

	/**
	 * The operation id for the '<em>Contains Metric</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE___CONTAINS_METRIC__METRIC = METRIC_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_VARIABLE_OPERATION_COUNT = METRIC_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link camel.metric.impl.MetricObjectBindingImpl <em>Object Binding</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.MetricObjectBindingImpl
	 * @see camel.metric.impl.MetricPackageImpl#getMetricObjectBinding()
	 * @generated
	 */
	int METRIC_OBJECT_BINDING = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OBJECT_BINDING__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OBJECT_BINDING__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OBJECT_BINDING__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OBJECT_BINDING__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OBJECT_BINDING__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Execution Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OBJECT_BINDING__EXECUTION_MODEL = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Component Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OBJECT_BINDING__COMPONENT_INSTANCE = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Object Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OBJECT_BINDING_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OBJECT_BINDING___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Object Binding</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_OBJECT_BINDING_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.ScheduleImpl <em>Schedule</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.ScheduleImpl
	 * @see camel.metric.impl.MetricPackageImpl#getSchedule()
	 * @generated
	 */
	int SCHEDULE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Start</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__START = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>End</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__END = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Time Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__TIME_UNIT = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Repetitions</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__REPETITIONS = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Interval</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE__INTERVAL = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Schedule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Schedule</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCHEDULE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.SensorImpl <em>Sensor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.SensorImpl
	 * @see camel.metric.impl.MetricPackageImpl#getSensor()
	 * @generated
	 */
	int SENSOR = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__NAME = DeploymentPackage.SOFTWARE_COMPONENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__DESCRIPTION = DeploymentPackage.SOFTWARE_COMPONENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__ANNOTATIONS = DeploymentPackage.SOFTWARE_COMPONENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__ATTRIBUTES = DeploymentPackage.SOFTWARE_COMPONENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__SUB_FEATURES = DeploymentPackage.SOFTWARE_COMPONENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Provided Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__PROVIDED_COMMUNICATIONS = DeploymentPackage.SOFTWARE_COMPONENT__PROVIDED_COMMUNICATIONS;

	/**
	 * The feature id for the '<em><b>Provided Hosts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__PROVIDED_HOSTS = DeploymentPackage.SOFTWARE_COMPONENT__PROVIDED_HOSTS;

	/**
	 * The feature id for the '<em><b>Configurations</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__CONFIGURATIONS = DeploymentPackage.SOFTWARE_COMPONENT__CONFIGURATIONS;

	/**
	 * The feature id for the '<em><b>Required Communications</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__REQUIRED_COMMUNICATIONS = DeploymentPackage.SOFTWARE_COMPONENT__REQUIRED_COMMUNICATIONS;

	/**
	 * The feature id for the '<em><b>Required Host</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__REQUIRED_HOST = DeploymentPackage.SOFTWARE_COMPONENT__REQUIRED_HOST;

	/**
	 * The feature id for the '<em><b>Requirement Set</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__REQUIREMENT_SET = DeploymentPackage.SOFTWARE_COMPONENT__REQUIREMENT_SET;

	/**
	 * The feature id for the '<em><b>Generates Data</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__GENERATES_DATA = DeploymentPackage.SOFTWARE_COMPONENT__GENERATES_DATA;

	/**
	 * The feature id for the '<em><b>Consumes Data</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__CONSUMES_DATA = DeploymentPackage.SOFTWARE_COMPONENT__CONSUMES_DATA;

	/**
	 * The feature id for the '<em><b>Manages Data Source</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__MANAGES_DATA_SOURCE = DeploymentPackage.SOFTWARE_COMPONENT__MANAGES_DATA_SOURCE;

	/**
	 * The feature id for the '<em><b>Long Lived</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__LONG_LIVED = DeploymentPackage.SOFTWARE_COMPONENT__LONG_LIVED;

	/**
	 * The feature id for the '<em><b>Co Instance Hosting</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__CO_INSTANCE_HOSTING = DeploymentPackage.SOFTWARE_COMPONENT__CO_INSTANCE_HOSTING;

	/**
	 * The feature id for the '<em><b>Configuration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__CONFIGURATION = DeploymentPackage.SOFTWARE_COMPONENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is Push</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR__IS_PUSH = DeploymentPackage.SOFTWARE_COMPONENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Sensor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR_FEATURE_COUNT = DeploymentPackage.SOFTWARE_COMPONENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR___AS_ERROR__BOOLEAN = DeploymentPackage.SOFTWARE_COMPONENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Sensor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SENSOR_OPERATION_COUNT = DeploymentPackage.SOFTWARE_COMPONENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.WindowImpl <em>Window</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.WindowImpl
	 * @see camel.metric.impl.MetricPackageImpl#getWindow()
	 * @generated
	 */
	int WINDOW = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Time Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__TIME_UNIT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Window Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__WINDOW_TYPE = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Size Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__SIZE_TYPE = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Measurement Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__MEASUREMENT_SIZE = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Time Size</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW__TIME_SIZE = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Window</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int WINDOW_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.MetricModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.MetricModelImpl
	 * @see camel.metric.impl.MetricPackageImpl#getMetricModel()
	 * @generated
	 */
	int METRIC_MODEL = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_MODEL__NAME = CorePackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_MODEL__DESCRIPTION = CorePackage.MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_MODEL__ANNOTATIONS = CorePackage.MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_MODEL__ATTRIBUTES = CorePackage.MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_MODEL__SUB_FEATURES = CorePackage.MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_MODEL__IMPORT_URI = CorePackage.MODEL__IMPORT_URI;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_MODEL_FEATURE_COUNT = CorePackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_MODEL___AS_ERROR__BOOLEAN = CorePackage.MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_MODEL_OPERATION_COUNT = CorePackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.MetricContextImpl <em>Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.MetricContextImpl
	 * @see camel.metric.impl.MetricPackageImpl#getMetricContext()
	 * @generated
	 */
	int METRIC_CONTEXT = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT__METRIC = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Window</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT__WINDOW = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT__SCHEDULE = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Object Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT__OBJECT_CONTEXT = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_CONTEXT_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.CompositeMetricContextImpl <em>Composite Metric Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.CompositeMetricContextImpl
	 * @see camel.metric.impl.MetricPackageImpl#getCompositeMetricContext()
	 * @generated
	 */
	int COMPOSITE_METRIC_CONTEXT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT__NAME = METRIC_CONTEXT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT__DESCRIPTION = METRIC_CONTEXT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT__ANNOTATIONS = METRIC_CONTEXT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT__ATTRIBUTES = METRIC_CONTEXT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT__SUB_FEATURES = METRIC_CONTEXT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT__METRIC = METRIC_CONTEXT__METRIC;

	/**
	 * The feature id for the '<em><b>Window</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT__WINDOW = METRIC_CONTEXT__WINDOW;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT__SCHEDULE = METRIC_CONTEXT__SCHEDULE;

	/**
	 * The feature id for the '<em><b>Object Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT__OBJECT_CONTEXT = METRIC_CONTEXT__OBJECT_CONTEXT;

	/**
	 * The feature id for the '<em><b>Grouping Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT__GROUPING_TYPE = METRIC_CONTEXT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Composing Metric Contexts</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT__COMPOSING_METRIC_CONTEXTS = METRIC_CONTEXT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Composite Metric Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT_FEATURE_COUNT = METRIC_CONTEXT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT___AS_ERROR__BOOLEAN = METRIC_CONTEXT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Composite Metric Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_METRIC_CONTEXT_OPERATION_COUNT = METRIC_CONTEXT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.RawMetricContextImpl <em>Raw Metric Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.RawMetricContextImpl
	 * @see camel.metric.impl.MetricPackageImpl#getRawMetricContext()
	 * @generated
	 */
	int RAW_METRIC_CONTEXT = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT__NAME = METRIC_CONTEXT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT__DESCRIPTION = METRIC_CONTEXT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT__ANNOTATIONS = METRIC_CONTEXT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT__ATTRIBUTES = METRIC_CONTEXT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT__SUB_FEATURES = METRIC_CONTEXT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT__METRIC = METRIC_CONTEXT__METRIC;

	/**
	 * The feature id for the '<em><b>Window</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT__WINDOW = METRIC_CONTEXT__WINDOW;

	/**
	 * The feature id for the '<em><b>Schedule</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT__SCHEDULE = METRIC_CONTEXT__SCHEDULE;

	/**
	 * The feature id for the '<em><b>Object Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT__OBJECT_CONTEXT = METRIC_CONTEXT__OBJECT_CONTEXT;

	/**
	 * The feature id for the '<em><b>Sensor</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT__SENSOR = METRIC_CONTEXT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Raw Metric Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT_FEATURE_COUNT = METRIC_CONTEXT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT___AS_ERROR__BOOLEAN = METRIC_CONTEXT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Raw Metric Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_METRIC_CONTEXT_OPERATION_COUNT = METRIC_CONTEXT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.AttributeContextImpl <em>Attribute Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.AttributeContextImpl
	 * @see camel.metric.impl.MetricPackageImpl#getAttributeContext()
	 * @generated
	 */
	int ATTRIBUTE_CONTEXT = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTEXT__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTEXT__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTEXT__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTEXT__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTEXT__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTEXT__ATTRIBUTE = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Object Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTEXT__OBJECT_CONTEXT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Attribute Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTEXT_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTEXT___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Attribute Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATTRIBUTE_CONTEXT_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.FunctionImpl <em>Function</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.FunctionImpl
	 * @see camel.metric.impl.MetricPackageImpl#getFunction()
	 * @generated
	 */
	int FUNCTION = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Expression</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__EXPRESSION = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Arguments</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION__ARGUMENTS = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Function</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FUNCTION_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.MetricTemplateImpl <em>Template</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.MetricTemplateImpl
	 * @see camel.metric.impl.MetricPackageImpl#getMetricTemplate()
	 * @generated
	 */
	int METRIC_TEMPLATE = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Value Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE__VALUE_TYPE = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Value Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE__VALUE_DIRECTION = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE__UNIT = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE__ATTRIBUTE = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Template</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Template</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TEMPLATE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.ObjectContextImpl <em>Object Context</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.ObjectContextImpl
	 * @see camel.metric.impl.MetricPackageImpl#getObjectContext()
	 * @generated
	 */
	int OBJECT_CONTEXT = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONTEXT__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONTEXT__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONTEXT__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONTEXT__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONTEXT__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONTEXT__COMPONENT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONTEXT__DATA = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Object Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONTEXT_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONTEXT___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Object Context</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OBJECT_CONTEXT_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.MetricTypeModelImpl <em>Type Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.MetricTypeModelImpl
	 * @see camel.metric.impl.MetricPackageImpl#getMetricTypeModel()
	 * @generated
	 */
	int METRIC_TYPE_MODEL = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__NAME = METRIC_MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__DESCRIPTION = METRIC_MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__ANNOTATIONS = METRIC_MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__ATTRIBUTES = METRIC_MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__SUB_FEATURES = METRIC_MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__IMPORT_URI = METRIC_MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Metric Contexts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__METRIC_CONTEXTS = METRIC_MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Attribute Contexts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__ATTRIBUTE_CONTEXTS = METRIC_MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Object Contexts</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__OBJECT_CONTEXTS = METRIC_MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Metrics</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__METRICS = METRIC_MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Windows</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__WINDOWS = METRIC_MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Schedules</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__SCHEDULES = METRIC_MODEL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Sensors</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__SENSORS = METRIC_MODEL_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Functions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__FUNCTIONS = METRIC_MODEL_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Templates</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL__TEMPLATES = METRIC_MODEL_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Type Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL_FEATURE_COUNT = METRIC_MODEL_FEATURE_COUNT + 9;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL___AS_ERROR__BOOLEAN = METRIC_MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Type Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_TYPE_MODEL_OPERATION_COUNT = METRIC_MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.impl.MetricInstanceModelImpl <em>Instance Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.impl.MetricInstanceModelImpl
	 * @see camel.metric.impl.MetricPackageImpl#getMetricInstanceModel()
	 * @generated
	 */
	int METRIC_INSTANCE_MODEL = 18;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL__NAME = METRIC_MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL__DESCRIPTION = METRIC_MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL__ANNOTATIONS = METRIC_MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL__ATTRIBUTES = METRIC_MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL__SUB_FEATURES = METRIC_MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL__IMPORT_URI = METRIC_MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Metric Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL__METRIC_INSTANCES = METRIC_MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Bindings</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL__BINDINGS = METRIC_MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL__TYPE = METRIC_MODEL_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Instance Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL_FEATURE_COUNT = METRIC_MODEL_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL___AS_ERROR__BOOLEAN = METRIC_MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Instance Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int METRIC_INSTANCE_MODEL_OPERATION_COUNT = METRIC_MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.metric.WindowSizeType <em>Window Size Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.WindowSizeType
	 * @see camel.metric.impl.MetricPackageImpl#getWindowSizeType()
	 * @generated
	 */
	int WINDOW_SIZE_TYPE = 19;

	/**
	 * The meta object id for the '{@link camel.metric.WindowType <em>Window Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.WindowType
	 * @see camel.metric.impl.MetricPackageImpl#getWindowType()
	 * @generated
	 */
	int WINDOW_TYPE = 20;

	/**
	 * The meta object id for the '{@link camel.metric.GroupingType <em>Grouping Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.metric.GroupingType
	 * @see camel.metric.impl.MetricPackageImpl#getGroupingType()
	 * @generated
	 */
	int GROUPING_TYPE = 21;

	/**
	 * Returns the meta object for class '{@link camel.metric.MetricInstance <em>Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instance</em>'.
	 * @see camel.metric.MetricInstance
	 * @generated
	 */
	EClass getMetricInstance();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricInstance#getObjectBinding <em>Object Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object Binding</em>'.
	 * @see camel.metric.MetricInstance#getObjectBinding()
	 * @see #getMetricInstance()
	 * @generated
	 */
	EReference getMetricInstance_ObjectBinding();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricInstance#getMetricContext <em>Metric Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metric Context</em>'.
	 * @see camel.metric.MetricInstance#getMetricContext()
	 * @see #getMetricInstance()
	 * @generated
	 */
	EReference getMetricInstance_MetricContext();

	/**
	 * Returns the meta object for the reference list '{@link camel.metric.MetricInstance#getComposingMetricInstances <em>Composing Metric Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Composing Metric Instances</em>'.
	 * @see camel.metric.MetricInstance#getComposingMetricInstances()
	 * @see #getMetricInstance()
	 * @generated
	 */
	EReference getMetricInstance_ComposingMetricInstances();

	/**
	 * Returns the meta object for class '{@link camel.metric.Metric <em>Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Metric</em>'.
	 * @see camel.metric.Metric
	 * @generated
	 */
	EClass getMetric();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.Metric#getMetricTemplate <em>Metric Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metric Template</em>'.
	 * @see camel.metric.Metric#getMetricTemplate()
	 * @see #getMetric()
	 * @generated
	 */
	EReference getMetric_MetricTemplate();

	/**
	 * Returns the meta object for the '{@link camel.metric.Metric#checkRecursiveness(camel.metric.Metric, camel.metric.Metric) <em>Check Recursiveness</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Recursiveness</em>' operation.
	 * @see camel.metric.Metric#checkRecursiveness(camel.metric.Metric, camel.metric.Metric)
	 * @generated
	 */
	EOperation getMetric__CheckRecursiveness__Metric_Metric();

	/**
	 * Returns the meta object for class '{@link camel.metric.CompositeMetric <em>Composite Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Metric</em>'.
	 * @see camel.metric.CompositeMetric
	 * @generated
	 */
	EClass getCompositeMetric();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.CompositeMetric#getFormula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Formula</em>'.
	 * @see camel.metric.CompositeMetric#getFormula()
	 * @see #getCompositeMetric()
	 * @generated
	 */
	EAttribute getCompositeMetric_Formula();

	/**
	 * Returns the meta object for the reference list '{@link camel.metric.CompositeMetric#getComponentMetrics <em>Component Metrics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Component Metrics</em>'.
	 * @see camel.metric.CompositeMetric#getComponentMetrics()
	 * @see #getCompositeMetric()
	 * @generated
	 */
	EReference getCompositeMetric_ComponentMetrics();

	/**
	 * Returns the meta object for the '{@link camel.metric.CompositeMetric#containsMetric(camel.metric.Metric) <em>Contains Metric</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Contains Metric</em>' operation.
	 * @see camel.metric.CompositeMetric#containsMetric(camel.metric.Metric)
	 * @generated
	 */
	EOperation getCompositeMetric__ContainsMetric__Metric();

	/**
	 * Returns the meta object for class '{@link camel.metric.RawMetric <em>Raw Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Raw Metric</em>'.
	 * @see camel.metric.RawMetric
	 * @generated
	 */
	EClass getRawMetric();

	/**
	 * Returns the meta object for class '{@link camel.metric.MetricVariable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see camel.metric.MetricVariable
	 * @generated
	 */
	EClass getMetricVariable();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.MetricVariable#isCurrentConfiguration <em>Current Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Current Configuration</em>'.
	 * @see camel.metric.MetricVariable#isCurrentConfiguration()
	 * @see #getMetricVariable()
	 * @generated
	 */
	EAttribute getMetricVariable_CurrentConfiguration();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricVariable#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component</em>'.
	 * @see camel.metric.MetricVariable#getComponent()
	 * @see #getMetricVariable()
	 * @generated
	 */
	EReference getMetricVariable_Component();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.MetricVariable#isOnNodeCandidates <em>On Node Candidates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>On Node Candidates</em>'.
	 * @see camel.metric.MetricVariable#isOnNodeCandidates()
	 * @see #getMetricVariable()
	 * @generated
	 */
	EAttribute getMetricVariable_OnNodeCandidates();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.MetricVariable#getFormula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Formula</em>'.
	 * @see camel.metric.MetricVariable#getFormula()
	 * @see #getMetricVariable()
	 * @generated
	 */
	EAttribute getMetricVariable_Formula();

	/**
	 * Returns the meta object for the reference list '{@link camel.metric.MetricVariable#getComponentMetrics <em>Component Metrics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Component Metrics</em>'.
	 * @see camel.metric.MetricVariable#getComponentMetrics()
	 * @see #getMetricVariable()
	 * @generated
	 */
	EReference getMetricVariable_ComponentMetrics();

	/**
	 * Returns the meta object for the '{@link camel.metric.MetricVariable#containsMetric(camel.metric.Metric) <em>Contains Metric</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Contains Metric</em>' operation.
	 * @see camel.metric.MetricVariable#containsMetric(camel.metric.Metric)
	 * @generated
	 */
	EOperation getMetricVariable__ContainsMetric__Metric();

	/**
	 * Returns the meta object for class '{@link camel.metric.MetricObjectBinding <em>Object Binding</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Binding</em>'.
	 * @see camel.metric.MetricObjectBinding
	 * @generated
	 */
	EClass getMetricObjectBinding();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricObjectBinding#getExecutionModel <em>Execution Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Execution Model</em>'.
	 * @see camel.metric.MetricObjectBinding#getExecutionModel()
	 * @see #getMetricObjectBinding()
	 * @generated
	 */
	EReference getMetricObjectBinding_ExecutionModel();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricObjectBinding#getComponentInstance <em>Component Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component Instance</em>'.
	 * @see camel.metric.MetricObjectBinding#getComponentInstance()
	 * @see #getMetricObjectBinding()
	 * @generated
	 */
	EReference getMetricObjectBinding_ComponentInstance();

	/**
	 * Returns the meta object for class '{@link camel.metric.Schedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Schedule</em>'.
	 * @see camel.metric.Schedule
	 * @generated
	 */
	EClass getSchedule();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.Schedule#getStart <em>Start</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start</em>'.
	 * @see camel.metric.Schedule#getStart()
	 * @see #getSchedule()
	 * @generated
	 */
	EAttribute getSchedule_Start();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.Schedule#getEnd <em>End</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End</em>'.
	 * @see camel.metric.Schedule#getEnd()
	 * @see #getSchedule()
	 * @generated
	 */
	EAttribute getSchedule_End();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.Schedule#getTimeUnit <em>Time Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Time Unit</em>'.
	 * @see camel.metric.Schedule#getTimeUnit()
	 * @see #getSchedule()
	 * @generated
	 */
	EReference getSchedule_TimeUnit();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.Schedule#getRepetitions <em>Repetitions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Repetitions</em>'.
	 * @see camel.metric.Schedule#getRepetitions()
	 * @see #getSchedule()
	 * @generated
	 */
	EAttribute getSchedule_Repetitions();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.Schedule#getInterval <em>Interval</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Interval</em>'.
	 * @see camel.metric.Schedule#getInterval()
	 * @see #getSchedule()
	 * @generated
	 */
	EAttribute getSchedule_Interval();

	/**
	 * Returns the meta object for class '{@link camel.metric.Sensor <em>Sensor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Sensor</em>'.
	 * @see camel.metric.Sensor
	 * @generated
	 */
	EClass getSensor();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.Sensor#getConfiguration <em>Configuration</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Configuration</em>'.
	 * @see camel.metric.Sensor#getConfiguration()
	 * @see #getSensor()
	 * @generated
	 */
	EAttribute getSensor_Configuration();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.Sensor#isIsPush <em>Is Push</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is Push</em>'.
	 * @see camel.metric.Sensor#isIsPush()
	 * @see #getSensor()
	 * @generated
	 */
	EAttribute getSensor_IsPush();

	/**
	 * Returns the meta object for class '{@link camel.metric.Window <em>Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Window</em>'.
	 * @see camel.metric.Window
	 * @generated
	 */
	EClass getWindow();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.Window#getTimeUnit <em>Time Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Time Unit</em>'.
	 * @see camel.metric.Window#getTimeUnit()
	 * @see #getWindow()
	 * @generated
	 */
	EReference getWindow_TimeUnit();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.Window#getWindowType <em>Window Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Window Type</em>'.
	 * @see camel.metric.Window#getWindowType()
	 * @see #getWindow()
	 * @generated
	 */
	EAttribute getWindow_WindowType();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.Window#getSizeType <em>Size Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Size Type</em>'.
	 * @see camel.metric.Window#getSizeType()
	 * @see #getWindow()
	 * @generated
	 */
	EAttribute getWindow_SizeType();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.Window#getMeasurementSize <em>Measurement Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Measurement Size</em>'.
	 * @see camel.metric.Window#getMeasurementSize()
	 * @see #getWindow()
	 * @generated
	 */
	EAttribute getWindow_MeasurementSize();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.Window#getTimeSize <em>Time Size</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Time Size</em>'.
	 * @see camel.metric.Window#getTimeSize()
	 * @see #getWindow()
	 * @generated
	 */
	EAttribute getWindow_TimeSize();

	/**
	 * Returns the meta object for class '{@link camel.metric.MetricModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see camel.metric.MetricModel
	 * @generated
	 */
	EClass getMetricModel();

	/**
	 * Returns the meta object for class '{@link camel.metric.MetricContext <em>Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Context</em>'.
	 * @see camel.metric.MetricContext
	 * @generated
	 */
	EClass getMetricContext();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricContext#getMetric <em>Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metric</em>'.
	 * @see camel.metric.MetricContext#getMetric()
	 * @see #getMetricContext()
	 * @generated
	 */
	EReference getMetricContext_Metric();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricContext#getWindow <em>Window</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Window</em>'.
	 * @see camel.metric.MetricContext#getWindow()
	 * @see #getMetricContext()
	 * @generated
	 */
	EReference getMetricContext_Window();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricContext#getSchedule <em>Schedule</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Schedule</em>'.
	 * @see camel.metric.MetricContext#getSchedule()
	 * @see #getMetricContext()
	 * @generated
	 */
	EReference getMetricContext_Schedule();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricContext#getObjectContext <em>Object Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object Context</em>'.
	 * @see camel.metric.MetricContext#getObjectContext()
	 * @see #getMetricContext()
	 * @generated
	 */
	EReference getMetricContext_ObjectContext();

	/**
	 * Returns the meta object for class '{@link camel.metric.CompositeMetricContext <em>Composite Metric Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Metric Context</em>'.
	 * @see camel.metric.CompositeMetricContext
	 * @generated
	 */
	EClass getCompositeMetricContext();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.CompositeMetricContext#getGroupingType <em>Grouping Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Grouping Type</em>'.
	 * @see camel.metric.CompositeMetricContext#getGroupingType()
	 * @see #getCompositeMetricContext()
	 * @generated
	 */
	EAttribute getCompositeMetricContext_GroupingType();

	/**
	 * Returns the meta object for the reference list '{@link camel.metric.CompositeMetricContext#getComposingMetricContexts <em>Composing Metric Contexts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Composing Metric Contexts</em>'.
	 * @see camel.metric.CompositeMetricContext#getComposingMetricContexts()
	 * @see #getCompositeMetricContext()
	 * @generated
	 */
	EReference getCompositeMetricContext_ComposingMetricContexts();

	/**
	 * Returns the meta object for class '{@link camel.metric.RawMetricContext <em>Raw Metric Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Raw Metric Context</em>'.
	 * @see camel.metric.RawMetricContext
	 * @generated
	 */
	EClass getRawMetricContext();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.RawMetricContext#getSensor <em>Sensor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sensor</em>'.
	 * @see camel.metric.RawMetricContext#getSensor()
	 * @see #getRawMetricContext()
	 * @generated
	 */
	EReference getRawMetricContext_Sensor();

	/**
	 * Returns the meta object for class '{@link camel.metric.AttributeContext <em>Attribute Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute Context</em>'.
	 * @see camel.metric.AttributeContext
	 * @generated
	 */
	EClass getAttributeContext();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.AttributeContext#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see camel.metric.AttributeContext#getAttribute()
	 * @see #getAttributeContext()
	 * @generated
	 */
	EReference getAttributeContext_Attribute();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.AttributeContext#getObjectContext <em>Object Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Object Context</em>'.
	 * @see camel.metric.AttributeContext#getObjectContext()
	 * @see #getAttributeContext()
	 * @generated
	 */
	EReference getAttributeContext_ObjectContext();

	/**
	 * Returns the meta object for class '{@link camel.metric.Function <em>Function</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Function</em>'.
	 * @see camel.metric.Function
	 * @generated
	 */
	EClass getFunction();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.Function#getExpression <em>Expression</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Expression</em>'.
	 * @see camel.metric.Function#getExpression()
	 * @see #getFunction()
	 * @generated
	 */
	EAttribute getFunction_Expression();

	/**
	 * Returns the meta object for the attribute list '{@link camel.metric.Function#getArguments <em>Arguments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Arguments</em>'.
	 * @see camel.metric.Function#getArguments()
	 * @see #getFunction()
	 * @generated
	 */
	EAttribute getFunction_Arguments();

	/**
	 * Returns the meta object for class '{@link camel.metric.MetricTemplate <em>Template</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Template</em>'.
	 * @see camel.metric.MetricTemplate
	 * @generated
	 */
	EClass getMetricTemplate();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricTemplate#getValueType <em>Value Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Value Type</em>'.
	 * @see camel.metric.MetricTemplate#getValueType()
	 * @see #getMetricTemplate()
	 * @generated
	 */
	EReference getMetricTemplate_ValueType();

	/**
	 * Returns the meta object for the attribute '{@link camel.metric.MetricTemplate#getValueDirection <em>Value Direction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Value Direction</em>'.
	 * @see camel.metric.MetricTemplate#getValueDirection()
	 * @see #getMetricTemplate()
	 * @generated
	 */
	EAttribute getMetricTemplate_ValueDirection();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricTemplate#getUnit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Unit</em>'.
	 * @see camel.metric.MetricTemplate#getUnit()
	 * @see #getMetricTemplate()
	 * @generated
	 */
	EReference getMetricTemplate_Unit();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricTemplate#getAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Attribute</em>'.
	 * @see camel.metric.MetricTemplate#getAttribute()
	 * @see #getMetricTemplate()
	 * @generated
	 */
	EReference getMetricTemplate_Attribute();

	/**
	 * Returns the meta object for class '{@link camel.metric.ObjectContext <em>Object Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Object Context</em>'.
	 * @see camel.metric.ObjectContext
	 * @generated
	 */
	EClass getObjectContext();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.ObjectContext#getComponent <em>Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Component</em>'.
	 * @see camel.metric.ObjectContext#getComponent()
	 * @see #getObjectContext()
	 * @generated
	 */
	EReference getObjectContext_Component();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.ObjectContext#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Data</em>'.
	 * @see camel.metric.ObjectContext#getData()
	 * @see #getObjectContext()
	 * @generated
	 */
	EReference getObjectContext_Data();

	/**
	 * Returns the meta object for class '{@link camel.metric.MetricTypeModel <em>Type Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Type Model</em>'.
	 * @see camel.metric.MetricTypeModel
	 * @generated
	 */
	EClass getMetricTypeModel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.metric.MetricTypeModel#getMetricContexts <em>Metric Contexts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Metric Contexts</em>'.
	 * @see camel.metric.MetricTypeModel#getMetricContexts()
	 * @see #getMetricTypeModel()
	 * @generated
	 */
	EReference getMetricTypeModel_MetricContexts();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.metric.MetricTypeModel#getAttributeContexts <em>Attribute Contexts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Attribute Contexts</em>'.
	 * @see camel.metric.MetricTypeModel#getAttributeContexts()
	 * @see #getMetricTypeModel()
	 * @generated
	 */
	EReference getMetricTypeModel_AttributeContexts();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.metric.MetricTypeModel#getObjectContexts <em>Object Contexts</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Object Contexts</em>'.
	 * @see camel.metric.MetricTypeModel#getObjectContexts()
	 * @see #getMetricTypeModel()
	 * @generated
	 */
	EReference getMetricTypeModel_ObjectContexts();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.metric.MetricTypeModel#getMetrics <em>Metrics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Metrics</em>'.
	 * @see camel.metric.MetricTypeModel#getMetrics()
	 * @see #getMetricTypeModel()
	 * @generated
	 */
	EReference getMetricTypeModel_Metrics();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.metric.MetricTypeModel#getWindows <em>Windows</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Windows</em>'.
	 * @see camel.metric.MetricTypeModel#getWindows()
	 * @see #getMetricTypeModel()
	 * @generated
	 */
	EReference getMetricTypeModel_Windows();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.metric.MetricTypeModel#getSchedules <em>Schedules</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Schedules</em>'.
	 * @see camel.metric.MetricTypeModel#getSchedules()
	 * @see #getMetricTypeModel()
	 * @generated
	 */
	EReference getMetricTypeModel_Schedules();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.metric.MetricTypeModel#getSensors <em>Sensors</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Sensors</em>'.
	 * @see camel.metric.MetricTypeModel#getSensors()
	 * @see #getMetricTypeModel()
	 * @generated
	 */
	EReference getMetricTypeModel_Sensors();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.metric.MetricTypeModel#getFunctions <em>Functions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Functions</em>'.
	 * @see camel.metric.MetricTypeModel#getFunctions()
	 * @see #getMetricTypeModel()
	 * @generated
	 */
	EReference getMetricTypeModel_Functions();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.metric.MetricTypeModel#getTemplates <em>Templates</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Templates</em>'.
	 * @see camel.metric.MetricTypeModel#getTemplates()
	 * @see #getMetricTypeModel()
	 * @generated
	 */
	EReference getMetricTypeModel_Templates();

	/**
	 * Returns the meta object for class '{@link camel.metric.MetricInstanceModel <em>Instance Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Instance Model</em>'.
	 * @see camel.metric.MetricInstanceModel
	 * @generated
	 */
	EClass getMetricInstanceModel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.metric.MetricInstanceModel#getMetricInstances <em>Metric Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Metric Instances</em>'.
	 * @see camel.metric.MetricInstanceModel#getMetricInstances()
	 * @see #getMetricInstanceModel()
	 * @generated
	 */
	EReference getMetricInstanceModel_MetricInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.metric.MetricInstanceModel#getBindings <em>Bindings</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Bindings</em>'.
	 * @see camel.metric.MetricInstanceModel#getBindings()
	 * @see #getMetricInstanceModel()
	 * @generated
	 */
	EReference getMetricInstanceModel_Bindings();

	/**
	 * Returns the meta object for the reference '{@link camel.metric.MetricInstanceModel#getType <em>Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Type</em>'.
	 * @see camel.metric.MetricInstanceModel#getType()
	 * @see #getMetricInstanceModel()
	 * @generated
	 */
	EReference getMetricInstanceModel_Type();

	/**
	 * Returns the meta object for enum '{@link camel.metric.WindowSizeType <em>Window Size Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Window Size Type</em>'.
	 * @see camel.metric.WindowSizeType
	 * @generated
	 */
	EEnum getWindowSizeType();

	/**
	 * Returns the meta object for enum '{@link camel.metric.WindowType <em>Window Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Window Type</em>'.
	 * @see camel.metric.WindowType
	 * @generated
	 */
	EEnum getWindowType();

	/**
	 * Returns the meta object for enum '{@link camel.metric.GroupingType <em>Grouping Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Grouping Type</em>'.
	 * @see camel.metric.GroupingType
	 * @generated
	 */
	EEnum getGroupingType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	MetricFactory getMetricFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link camel.metric.impl.MetricInstanceImpl <em>Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.MetricInstanceImpl
		 * @see camel.metric.impl.MetricPackageImpl#getMetricInstance()
		 * @generated
		 */
		EClass METRIC_INSTANCE = eINSTANCE.getMetricInstance();

		/**
		 * The meta object literal for the '<em><b>Object Binding</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_INSTANCE__OBJECT_BINDING = eINSTANCE.getMetricInstance_ObjectBinding();

		/**
		 * The meta object literal for the '<em><b>Metric Context</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_INSTANCE__METRIC_CONTEXT = eINSTANCE.getMetricInstance_MetricContext();

		/**
		 * The meta object literal for the '<em><b>Composing Metric Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_INSTANCE__COMPOSING_METRIC_INSTANCES = eINSTANCE.getMetricInstance_ComposingMetricInstances();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.MetricImpl <em>Metric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.MetricImpl
		 * @see camel.metric.impl.MetricPackageImpl#getMetric()
		 * @generated
		 */
		EClass METRIC = eINSTANCE.getMetric();

		/**
		 * The meta object literal for the '<em><b>Metric Template</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC__METRIC_TEMPLATE = eINSTANCE.getMetric_MetricTemplate();

		/**
		 * The meta object literal for the '<em><b>Check Recursiveness</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation METRIC___CHECK_RECURSIVENESS__METRIC_METRIC = eINSTANCE.getMetric__CheckRecursiveness__Metric_Metric();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.CompositeMetricImpl <em>Composite Metric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.CompositeMetricImpl
		 * @see camel.metric.impl.MetricPackageImpl#getCompositeMetric()
		 * @generated
		 */
		EClass COMPOSITE_METRIC = eINSTANCE.getCompositeMetric();

		/**
		 * The meta object literal for the '<em><b>Formula</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_METRIC__FORMULA = eINSTANCE.getCompositeMetric_Formula();

		/**
		 * The meta object literal for the '<em><b>Component Metrics</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_METRIC__COMPONENT_METRICS = eINSTANCE.getCompositeMetric_ComponentMetrics();

		/**
		 * The meta object literal for the '<em><b>Contains Metric</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMPOSITE_METRIC___CONTAINS_METRIC__METRIC = eINSTANCE.getCompositeMetric__ContainsMetric__Metric();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.RawMetricImpl <em>Raw Metric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.RawMetricImpl
		 * @see camel.metric.impl.MetricPackageImpl#getRawMetric()
		 * @generated
		 */
		EClass RAW_METRIC = eINSTANCE.getRawMetric();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.MetricVariableImpl <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.MetricVariableImpl
		 * @see camel.metric.impl.MetricPackageImpl#getMetricVariable()
		 * @generated
		 */
		EClass METRIC_VARIABLE = eINSTANCE.getMetricVariable();

		/**
		 * The meta object literal for the '<em><b>Current Configuration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute METRIC_VARIABLE__CURRENT_CONFIGURATION = eINSTANCE.getMetricVariable_CurrentConfiguration();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_VARIABLE__COMPONENT = eINSTANCE.getMetricVariable_Component();

		/**
		 * The meta object literal for the '<em><b>On Node Candidates</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute METRIC_VARIABLE__ON_NODE_CANDIDATES = eINSTANCE.getMetricVariable_OnNodeCandidates();

		/**
		 * The meta object literal for the '<em><b>Formula</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute METRIC_VARIABLE__FORMULA = eINSTANCE.getMetricVariable_Formula();

		/**
		 * The meta object literal for the '<em><b>Component Metrics</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_VARIABLE__COMPONENT_METRICS = eINSTANCE.getMetricVariable_ComponentMetrics();

		/**
		 * The meta object literal for the '<em><b>Contains Metric</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation METRIC_VARIABLE___CONTAINS_METRIC__METRIC = eINSTANCE.getMetricVariable__ContainsMetric__Metric();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.MetricObjectBindingImpl <em>Object Binding</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.MetricObjectBindingImpl
		 * @see camel.metric.impl.MetricPackageImpl#getMetricObjectBinding()
		 * @generated
		 */
		EClass METRIC_OBJECT_BINDING = eINSTANCE.getMetricObjectBinding();

		/**
		 * The meta object literal for the '<em><b>Execution Model</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_OBJECT_BINDING__EXECUTION_MODEL = eINSTANCE.getMetricObjectBinding_ExecutionModel();

		/**
		 * The meta object literal for the '<em><b>Component Instance</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_OBJECT_BINDING__COMPONENT_INSTANCE = eINSTANCE.getMetricObjectBinding_ComponentInstance();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.ScheduleImpl <em>Schedule</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.ScheduleImpl
		 * @see camel.metric.impl.MetricPackageImpl#getSchedule()
		 * @generated
		 */
		EClass SCHEDULE = eINSTANCE.getSchedule();

		/**
		 * The meta object literal for the '<em><b>Start</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE__START = eINSTANCE.getSchedule_Start();

		/**
		 * The meta object literal for the '<em><b>End</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE__END = eINSTANCE.getSchedule_End();

		/**
		 * The meta object literal for the '<em><b>Time Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SCHEDULE__TIME_UNIT = eINSTANCE.getSchedule_TimeUnit();

		/**
		 * The meta object literal for the '<em><b>Repetitions</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE__REPETITIONS = eINSTANCE.getSchedule_Repetitions();

		/**
		 * The meta object literal for the '<em><b>Interval</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SCHEDULE__INTERVAL = eINSTANCE.getSchedule_Interval();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.SensorImpl <em>Sensor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.SensorImpl
		 * @see camel.metric.impl.MetricPackageImpl#getSensor()
		 * @generated
		 */
		EClass SENSOR = eINSTANCE.getSensor();

		/**
		 * The meta object literal for the '<em><b>Configuration</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SENSOR__CONFIGURATION = eINSTANCE.getSensor_Configuration();

		/**
		 * The meta object literal for the '<em><b>Is Push</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SENSOR__IS_PUSH = eINSTANCE.getSensor_IsPush();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.WindowImpl <em>Window</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.WindowImpl
		 * @see camel.metric.impl.MetricPackageImpl#getWindow()
		 * @generated
		 */
		EClass WINDOW = eINSTANCE.getWindow();

		/**
		 * The meta object literal for the '<em><b>Time Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference WINDOW__TIME_UNIT = eINSTANCE.getWindow_TimeUnit();

		/**
		 * The meta object literal for the '<em><b>Window Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WINDOW__WINDOW_TYPE = eINSTANCE.getWindow_WindowType();

		/**
		 * The meta object literal for the '<em><b>Size Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WINDOW__SIZE_TYPE = eINSTANCE.getWindow_SizeType();

		/**
		 * The meta object literal for the '<em><b>Measurement Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WINDOW__MEASUREMENT_SIZE = eINSTANCE.getWindow_MeasurementSize();

		/**
		 * The meta object literal for the '<em><b>Time Size</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute WINDOW__TIME_SIZE = eINSTANCE.getWindow_TimeSize();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.MetricModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.MetricModelImpl
		 * @see camel.metric.impl.MetricPackageImpl#getMetricModel()
		 * @generated
		 */
		EClass METRIC_MODEL = eINSTANCE.getMetricModel();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.MetricContextImpl <em>Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.MetricContextImpl
		 * @see camel.metric.impl.MetricPackageImpl#getMetricContext()
		 * @generated
		 */
		EClass METRIC_CONTEXT = eINSTANCE.getMetricContext();

		/**
		 * The meta object literal for the '<em><b>Metric</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_CONTEXT__METRIC = eINSTANCE.getMetricContext_Metric();

		/**
		 * The meta object literal for the '<em><b>Window</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_CONTEXT__WINDOW = eINSTANCE.getMetricContext_Window();

		/**
		 * The meta object literal for the '<em><b>Schedule</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_CONTEXT__SCHEDULE = eINSTANCE.getMetricContext_Schedule();

		/**
		 * The meta object literal for the '<em><b>Object Context</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_CONTEXT__OBJECT_CONTEXT = eINSTANCE.getMetricContext_ObjectContext();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.CompositeMetricContextImpl <em>Composite Metric Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.CompositeMetricContextImpl
		 * @see camel.metric.impl.MetricPackageImpl#getCompositeMetricContext()
		 * @generated
		 */
		EClass COMPOSITE_METRIC_CONTEXT = eINSTANCE.getCompositeMetricContext();

		/**
		 * The meta object literal for the '<em><b>Grouping Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_METRIC_CONTEXT__GROUPING_TYPE = eINSTANCE.getCompositeMetricContext_GroupingType();

		/**
		 * The meta object literal for the '<em><b>Composing Metric Contexts</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_METRIC_CONTEXT__COMPOSING_METRIC_CONTEXTS = eINSTANCE.getCompositeMetricContext_ComposingMetricContexts();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.RawMetricContextImpl <em>Raw Metric Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.RawMetricContextImpl
		 * @see camel.metric.impl.MetricPackageImpl#getRawMetricContext()
		 * @generated
		 */
		EClass RAW_METRIC_CONTEXT = eINSTANCE.getRawMetricContext();

		/**
		 * The meta object literal for the '<em><b>Sensor</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RAW_METRIC_CONTEXT__SENSOR = eINSTANCE.getRawMetricContext_Sensor();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.AttributeContextImpl <em>Attribute Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.AttributeContextImpl
		 * @see camel.metric.impl.MetricPackageImpl#getAttributeContext()
		 * @generated
		 */
		EClass ATTRIBUTE_CONTEXT = eINSTANCE.getAttributeContext();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_CONTEXT__ATTRIBUTE = eINSTANCE.getAttributeContext_Attribute();

		/**
		 * The meta object literal for the '<em><b>Object Context</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ATTRIBUTE_CONTEXT__OBJECT_CONTEXT = eINSTANCE.getAttributeContext_ObjectContext();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.FunctionImpl <em>Function</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.FunctionImpl
		 * @see camel.metric.impl.MetricPackageImpl#getFunction()
		 * @generated
		 */
		EClass FUNCTION = eINSTANCE.getFunction();

		/**
		 * The meta object literal for the '<em><b>Expression</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION__EXPRESSION = eINSTANCE.getFunction_Expression();

		/**
		 * The meta object literal for the '<em><b>Arguments</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute FUNCTION__ARGUMENTS = eINSTANCE.getFunction_Arguments();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.MetricTemplateImpl <em>Template</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.MetricTemplateImpl
		 * @see camel.metric.impl.MetricPackageImpl#getMetricTemplate()
		 * @generated
		 */
		EClass METRIC_TEMPLATE = eINSTANCE.getMetricTemplate();

		/**
		 * The meta object literal for the '<em><b>Value Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TEMPLATE__VALUE_TYPE = eINSTANCE.getMetricTemplate_ValueType();

		/**
		 * The meta object literal for the '<em><b>Value Direction</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute METRIC_TEMPLATE__VALUE_DIRECTION = eINSTANCE.getMetricTemplate_ValueDirection();

		/**
		 * The meta object literal for the '<em><b>Unit</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TEMPLATE__UNIT = eINSTANCE.getMetricTemplate_Unit();

		/**
		 * The meta object literal for the '<em><b>Attribute</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TEMPLATE__ATTRIBUTE = eINSTANCE.getMetricTemplate_Attribute();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.ObjectContextImpl <em>Object Context</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.ObjectContextImpl
		 * @see camel.metric.impl.MetricPackageImpl#getObjectContext()
		 * @generated
		 */
		EClass OBJECT_CONTEXT = eINSTANCE.getObjectContext();

		/**
		 * The meta object literal for the '<em><b>Component</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_CONTEXT__COMPONENT = eINSTANCE.getObjectContext_Component();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OBJECT_CONTEXT__DATA = eINSTANCE.getObjectContext_Data();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.MetricTypeModelImpl <em>Type Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.MetricTypeModelImpl
		 * @see camel.metric.impl.MetricPackageImpl#getMetricTypeModel()
		 * @generated
		 */
		EClass METRIC_TYPE_MODEL = eINSTANCE.getMetricTypeModel();

		/**
		 * The meta object literal for the '<em><b>Metric Contexts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TYPE_MODEL__METRIC_CONTEXTS = eINSTANCE.getMetricTypeModel_MetricContexts();

		/**
		 * The meta object literal for the '<em><b>Attribute Contexts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TYPE_MODEL__ATTRIBUTE_CONTEXTS = eINSTANCE.getMetricTypeModel_AttributeContexts();

		/**
		 * The meta object literal for the '<em><b>Object Contexts</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TYPE_MODEL__OBJECT_CONTEXTS = eINSTANCE.getMetricTypeModel_ObjectContexts();

		/**
		 * The meta object literal for the '<em><b>Metrics</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TYPE_MODEL__METRICS = eINSTANCE.getMetricTypeModel_Metrics();

		/**
		 * The meta object literal for the '<em><b>Windows</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TYPE_MODEL__WINDOWS = eINSTANCE.getMetricTypeModel_Windows();

		/**
		 * The meta object literal for the '<em><b>Schedules</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TYPE_MODEL__SCHEDULES = eINSTANCE.getMetricTypeModel_Schedules();

		/**
		 * The meta object literal for the '<em><b>Sensors</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TYPE_MODEL__SENSORS = eINSTANCE.getMetricTypeModel_Sensors();

		/**
		 * The meta object literal for the '<em><b>Functions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TYPE_MODEL__FUNCTIONS = eINSTANCE.getMetricTypeModel_Functions();

		/**
		 * The meta object literal for the '<em><b>Templates</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_TYPE_MODEL__TEMPLATES = eINSTANCE.getMetricTypeModel_Templates();

		/**
		 * The meta object literal for the '{@link camel.metric.impl.MetricInstanceModelImpl <em>Instance Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.impl.MetricInstanceModelImpl
		 * @see camel.metric.impl.MetricPackageImpl#getMetricInstanceModel()
		 * @generated
		 */
		EClass METRIC_INSTANCE_MODEL = eINSTANCE.getMetricInstanceModel();

		/**
		 * The meta object literal for the '<em><b>Metric Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_INSTANCE_MODEL__METRIC_INSTANCES = eINSTANCE.getMetricInstanceModel_MetricInstances();

		/**
		 * The meta object literal for the '<em><b>Bindings</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_INSTANCE_MODEL__BINDINGS = eINSTANCE.getMetricInstanceModel_Bindings();

		/**
		 * The meta object literal for the '<em><b>Type</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference METRIC_INSTANCE_MODEL__TYPE = eINSTANCE.getMetricInstanceModel_Type();

		/**
		 * The meta object literal for the '{@link camel.metric.WindowSizeType <em>Window Size Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.WindowSizeType
		 * @see camel.metric.impl.MetricPackageImpl#getWindowSizeType()
		 * @generated
		 */
		EEnum WINDOW_SIZE_TYPE = eINSTANCE.getWindowSizeType();

		/**
		 * The meta object literal for the '{@link camel.metric.WindowType <em>Window Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.WindowType
		 * @see camel.metric.impl.MetricPackageImpl#getWindowType()
		 * @generated
		 */
		EEnum WINDOW_TYPE = eINSTANCE.getWindowType();

		/**
		 * The meta object literal for the '{@link camel.metric.GroupingType <em>Grouping Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.metric.GroupingType
		 * @see camel.metric.impl.MetricPackageImpl#getGroupingType()
		 * @generated
		 */
		EEnum GROUPING_TYPE = eINSTANCE.getGroupingType();

	}

} //MetricPackage
