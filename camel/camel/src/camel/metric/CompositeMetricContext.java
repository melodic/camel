/**
 */
package camel.metric;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Metric Context</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.CompositeMetricContext#getGroupingType <em>Grouping Type</em>}</li>
 *   <li>{@link camel.metric.CompositeMetricContext#getComposingMetricContexts <em>Composing Metric Contexts</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getCompositeMetricContext()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='composite_metric_context_correct_metric_type metrics_in_composing_contexts_are_component_metrics'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot composite_metric_context_correct_metric_type='Tuple {\n\tmessage : String = \'In CompositeMetricContext: \' + self.name + \' the metric: \' + self.metric.name + \' should be composite but it isn\\\'t\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.metric.oclIsKindOf(CompositeMetric))\n}.status'"
 * @generated
 */
public interface CompositeMetricContext extends MetricContext {
	/**
	 * Returns the value of the '<em><b>Grouping Type</b></em>' attribute.
	 * The default value is <code>"PER_INSTANCE"</code>.
	 * The literals are from the enumeration {@link camel.metric.GroupingType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Grouping Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Grouping Type</em>' attribute.
	 * @see camel.metric.GroupingType
	 * @see #setGroupingType(GroupingType)
	 * @see camel.metric.MetricPackage#getCompositeMetricContext_GroupingType()
	 * @model default="PER_INSTANCE" required="true"
	 * @generated
	 */
	GroupingType getGroupingType();

	/**
	 * Sets the value of the '{@link camel.metric.CompositeMetricContext#getGroupingType <em>Grouping Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Grouping Type</em>' attribute.
	 * @see camel.metric.GroupingType
	 * @see #getGroupingType()
	 * @generated
	 */
	void setGroupingType(GroupingType value);

	/**
	 * Returns the value of the '<em><b>Composing Metric Contexts</b></em>' reference list.
	 * The list contents are of type {@link camel.metric.MetricContext}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composing Metric Contexts</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composing Metric Contexts</em>' reference list.
	 * @see camel.metric.MetricPackage#getCompositeMetricContext_ComposingMetricContexts()
	 * @model
	 * @generated
	 */
	EList<MetricContext> getComposingMetricContexts();

} // CompositeMetricContext
