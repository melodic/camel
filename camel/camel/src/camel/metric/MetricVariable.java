/**
 */
package camel.metric;

import camel.deployment.Component;
import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.MetricVariable#isCurrentConfiguration <em>Current Configuration</em>}</li>
 *   <li>{@link camel.metric.MetricVariable#getComponent <em>Component</em>}</li>
 *   <li>{@link camel.metric.MetricVariable#isOnNodeCandidates <em>On Node Candidates</em>}</li>
 *   <li>{@link camel.metric.MetricVariable#getFormula <em>Formula</em>}</li>
 *   <li>{@link camel.metric.MetricVariable#getComponentMetrics <em>Component Metrics</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getMetricVariable()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='metric_variable_current_config_candidates metric_variable_contains_recurs metric_variable_formula_components'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot metric_variable_current_config_candidates='Tuple {\n\tmessage : String = \'MetricVariable: \' + self.name + \' cannot span both the current configuration and the node candidates\',\n\tstatus : Boolean = \n \t\t\t \tasError(currentConfiguration = false or onNodeCandidates = false)\n}.status' metric_variable_contains_recurs='Tuple {\n\tmessage : String = \'MetricVariable: \' + self.name + \' should not contain recursively itself\'\n\t\t\t,\n\tstatus : Boolean = asError(not self.containsMetric(self))\n}.status'"
 * @generated
 */
public interface MetricVariable extends Metric {
	/**
	 * Returns the value of the '<em><b>Current Configuration</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Current Configuration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Current Configuration</em>' attribute.
	 * @see #setCurrentConfiguration(boolean)
	 * @see camel.metric.MetricPackage#getMetricVariable_CurrentConfiguration()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isCurrentConfiguration();

	/**
	 * Sets the value of the '{@link camel.metric.MetricVariable#isCurrentConfiguration <em>Current Configuration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Current Configuration</em>' attribute.
	 * @see #isCurrentConfiguration()
	 * @generated
	 */
	void setCurrentConfiguration(boolean value);

	/**
	 * Returns the value of the '<em><b>Component</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component</em>' reference.
	 * @see #setComponent(Component)
	 * @see camel.metric.MetricPackage#getMetricVariable_Component()
	 * @model
	 * @generated
	 */
	Component getComponent();

	/**
	 * Sets the value of the '{@link camel.metric.MetricVariable#getComponent <em>Component</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component</em>' reference.
	 * @see #getComponent()
	 * @generated
	 */
	void setComponent(Component value);

	/**
	 * Returns the value of the '<em><b>On Node Candidates</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>On Node Candidates</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>On Node Candidates</em>' attribute.
	 * @see #setOnNodeCandidates(boolean)
	 * @see camel.metric.MetricPackage#getMetricVariable_OnNodeCandidates()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isOnNodeCandidates();

	/**
	 * Sets the value of the '{@link camel.metric.MetricVariable#isOnNodeCandidates <em>On Node Candidates</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>On Node Candidates</em>' attribute.
	 * @see #isOnNodeCandidates()
	 * @generated
	 */
	void setOnNodeCandidates(boolean value);

	/**
	 * Returns the value of the '<em><b>Formula</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formula</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formula</em>' attribute.
	 * @see #setFormula(String)
	 * @see camel.metric.MetricPackage#getMetricVariable_Formula()
	 * @model
	 * @generated
	 */
	String getFormula();

	/**
	 * Sets the value of the '{@link camel.metric.MetricVariable#getFormula <em>Formula</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formula</em>' attribute.
	 * @see #getFormula()
	 * @generated
	 */
	void setFormula(String value);

	/**
	 * Returns the value of the '<em><b>Component Metrics</b></em>' reference list.
	 * The list contents are of type {@link camel.metric.Metric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Metrics</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Metrics</em>' reference list.
	 * @see camel.metric.MetricPackage#getMetricVariable_ComponentMetrics()
	 * @model
	 * @generated
	 */
	EList<Metric> getComponentMetrics();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" metricRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='\n\t\t\t\t\tcomponentMetrics-&gt;includes(metric) or componentMetrics-&gt;exists(m | \n\t\t\t\t\t\t(m.oclIsTypeOf(MetricVariable) and m.oclAsType(MetricVariable).containsMetric(metric)) or\n\t\t\t\t\t\t(m.oclIsTypeOf(CompositeMetric) and m.oclAsType(CompositeMetric).containsMetric(metric))\n\t\t\t\t\t)'"
	 * @generated
	 */
	boolean containsMetric(Metric metric);

} // MetricVariable
