/**
 */
package camel.metric.util;

import camel.core.Feature;
import camel.core.Model;
import camel.core.NamedElement;

import camel.deployment.Component;
import camel.deployment.SoftwareComponent;

import camel.metric.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see camel.metric.MetricPackage
 * @generated
 */
public class MetricSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static MetricPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricSwitch() {
		if (modelPackage == null) {
			modelPackage = MetricPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case MetricPackage.METRIC_INSTANCE: {
				MetricInstance metricInstance = (MetricInstance)theEObject;
				T result = caseMetricInstance(metricInstance);
				if (result == null) result = caseFeature(metricInstance);
				if (result == null) result = caseNamedElement(metricInstance);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.METRIC: {
				Metric metric = (Metric)theEObject;
				T result = caseMetric(metric);
				if (result == null) result = caseFeature(metric);
				if (result == null) result = caseNamedElement(metric);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.COMPOSITE_METRIC: {
				CompositeMetric compositeMetric = (CompositeMetric)theEObject;
				T result = caseCompositeMetric(compositeMetric);
				if (result == null) result = caseMetric(compositeMetric);
				if (result == null) result = caseFeature(compositeMetric);
				if (result == null) result = caseNamedElement(compositeMetric);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.RAW_METRIC: {
				RawMetric rawMetric = (RawMetric)theEObject;
				T result = caseRawMetric(rawMetric);
				if (result == null) result = caseMetric(rawMetric);
				if (result == null) result = caseFeature(rawMetric);
				if (result == null) result = caseNamedElement(rawMetric);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.METRIC_VARIABLE: {
				MetricVariable metricVariable = (MetricVariable)theEObject;
				T result = caseMetricVariable(metricVariable);
				if (result == null) result = caseMetric(metricVariable);
				if (result == null) result = caseFeature(metricVariable);
				if (result == null) result = caseNamedElement(metricVariable);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.METRIC_OBJECT_BINDING: {
				MetricObjectBinding metricObjectBinding = (MetricObjectBinding)theEObject;
				T result = caseMetricObjectBinding(metricObjectBinding);
				if (result == null) result = caseFeature(metricObjectBinding);
				if (result == null) result = caseNamedElement(metricObjectBinding);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.SCHEDULE: {
				Schedule schedule = (Schedule)theEObject;
				T result = caseSchedule(schedule);
				if (result == null) result = caseFeature(schedule);
				if (result == null) result = caseNamedElement(schedule);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.SENSOR: {
				Sensor sensor = (Sensor)theEObject;
				T result = caseSensor(sensor);
				if (result == null) result = caseSoftwareComponent(sensor);
				if (result == null) result = caseComponent(sensor);
				if (result == null) result = caseFeature(sensor);
				if (result == null) result = caseNamedElement(sensor);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.WINDOW: {
				Window window = (Window)theEObject;
				T result = caseWindow(window);
				if (result == null) result = caseFeature(window);
				if (result == null) result = caseNamedElement(window);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.METRIC_MODEL: {
				MetricModel metricModel = (MetricModel)theEObject;
				T result = caseMetricModel(metricModel);
				if (result == null) result = caseModel(metricModel);
				if (result == null) result = caseFeature(metricModel);
				if (result == null) result = caseNamedElement(metricModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.METRIC_CONTEXT: {
				MetricContext metricContext = (MetricContext)theEObject;
				T result = caseMetricContext(metricContext);
				if (result == null) result = caseFeature(metricContext);
				if (result == null) result = caseNamedElement(metricContext);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.COMPOSITE_METRIC_CONTEXT: {
				CompositeMetricContext compositeMetricContext = (CompositeMetricContext)theEObject;
				T result = caseCompositeMetricContext(compositeMetricContext);
				if (result == null) result = caseMetricContext(compositeMetricContext);
				if (result == null) result = caseFeature(compositeMetricContext);
				if (result == null) result = caseNamedElement(compositeMetricContext);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.RAW_METRIC_CONTEXT: {
				RawMetricContext rawMetricContext = (RawMetricContext)theEObject;
				T result = caseRawMetricContext(rawMetricContext);
				if (result == null) result = caseMetricContext(rawMetricContext);
				if (result == null) result = caseFeature(rawMetricContext);
				if (result == null) result = caseNamedElement(rawMetricContext);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.ATTRIBUTE_CONTEXT: {
				AttributeContext attributeContext = (AttributeContext)theEObject;
				T result = caseAttributeContext(attributeContext);
				if (result == null) result = caseFeature(attributeContext);
				if (result == null) result = caseNamedElement(attributeContext);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.FUNCTION: {
				Function function = (Function)theEObject;
				T result = caseFunction(function);
				if (result == null) result = caseFeature(function);
				if (result == null) result = caseNamedElement(function);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.METRIC_TEMPLATE: {
				MetricTemplate metricTemplate = (MetricTemplate)theEObject;
				T result = caseMetricTemplate(metricTemplate);
				if (result == null) result = caseFeature(metricTemplate);
				if (result == null) result = caseNamedElement(metricTemplate);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.OBJECT_CONTEXT: {
				ObjectContext objectContext = (ObjectContext)theEObject;
				T result = caseObjectContext(objectContext);
				if (result == null) result = caseFeature(objectContext);
				if (result == null) result = caseNamedElement(objectContext);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.METRIC_TYPE_MODEL: {
				MetricTypeModel metricTypeModel = (MetricTypeModel)theEObject;
				T result = caseMetricTypeModel(metricTypeModel);
				if (result == null) result = caseMetricModel(metricTypeModel);
				if (result == null) result = caseModel(metricTypeModel);
				if (result == null) result = caseFeature(metricTypeModel);
				if (result == null) result = caseNamedElement(metricTypeModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case MetricPackage.METRIC_INSTANCE_MODEL: {
				MetricInstanceModel metricInstanceModel = (MetricInstanceModel)theEObject;
				T result = caseMetricInstanceModel(metricInstanceModel);
				if (result == null) result = caseMetricModel(metricInstanceModel);
				if (result == null) result = caseModel(metricInstanceModel);
				if (result == null) result = caseFeature(metricInstanceModel);
				if (result == null) result = caseNamedElement(metricInstanceModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instance</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetricInstance(MetricInstance object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Metric</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Metric</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetric(Metric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Metric</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Metric</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompositeMetric(CompositeMetric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Raw Metric</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Raw Metric</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRawMetric(RawMetric object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Variable</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetricVariable(MetricVariable object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object Binding</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object Binding</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetricObjectBinding(MetricObjectBinding object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Schedule</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Schedule</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSchedule(Schedule object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Sensor</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Sensor</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSensor(Sensor object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Window</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Window</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseWindow(Window object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetricModel(MetricModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Context</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Context</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetricContext(MetricContext object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Composite Metric Context</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Composite Metric Context</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCompositeMetricContext(CompositeMetricContext object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Raw Metric Context</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Raw Metric Context</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRawMetricContext(RawMetricContext object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Attribute Context</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Attribute Context</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseAttributeContext(AttributeContext object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Function</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Function</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFunction(Function object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Template</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Template</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetricTemplate(MetricTemplate object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Object Context</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Object Context</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseObjectContext(ObjectContext object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Type Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Type Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetricTypeModel(MetricTypeModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Instance Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Instance Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseMetricInstanceModel(MetricInstanceModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeature(Feature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseComponent(Component object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Software Component</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Software Component</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoftwareComponent(SoftwareComponent object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModel(Model object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //MetricSwitch
