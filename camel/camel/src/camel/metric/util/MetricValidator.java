/**
 */
package camel.metric.util;

import camel.metric.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.metric.MetricPackage
 * @generated
 */
public class MetricValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final MetricValidator INSTANCE = new MetricValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.metric";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return MetricPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case MetricPackage.METRIC_INSTANCE:
				return validateMetricInstance((MetricInstance)value, diagnostics, context);
			case MetricPackage.METRIC:
				return validateMetric((Metric)value, diagnostics, context);
			case MetricPackage.COMPOSITE_METRIC:
				return validateCompositeMetric((CompositeMetric)value, diagnostics, context);
			case MetricPackage.RAW_METRIC:
				return validateRawMetric((RawMetric)value, diagnostics, context);
			case MetricPackage.METRIC_VARIABLE:
				return validateMetricVariable((MetricVariable)value, diagnostics, context);
			case MetricPackage.METRIC_OBJECT_BINDING:
				return validateMetricObjectBinding((MetricObjectBinding)value, diagnostics, context);
			case MetricPackage.SCHEDULE:
				return validateSchedule((Schedule)value, diagnostics, context);
			case MetricPackage.SENSOR:
				return validateSensor((Sensor)value, diagnostics, context);
			case MetricPackage.WINDOW:
				return validateWindow((Window)value, diagnostics, context);
			case MetricPackage.METRIC_MODEL:
				return validateMetricModel((MetricModel)value, diagnostics, context);
			case MetricPackage.METRIC_CONTEXT:
				return validateMetricContext((MetricContext)value, diagnostics, context);
			case MetricPackage.COMPOSITE_METRIC_CONTEXT:
				return validateCompositeMetricContext((CompositeMetricContext)value, diagnostics, context);
			case MetricPackage.RAW_METRIC_CONTEXT:
				return validateRawMetricContext((RawMetricContext)value, diagnostics, context);
			case MetricPackage.ATTRIBUTE_CONTEXT:
				return validateAttributeContext((AttributeContext)value, diagnostics, context);
			case MetricPackage.FUNCTION:
				return validateFunction((Function)value, diagnostics, context);
			case MetricPackage.METRIC_TEMPLATE:
				return validateMetricTemplate((MetricTemplate)value, diagnostics, context);
			case MetricPackage.OBJECT_CONTEXT:
				return validateObjectContext((ObjectContext)value, diagnostics, context);
			case MetricPackage.METRIC_TYPE_MODEL:
				return validateMetricTypeModel((MetricTypeModel)value, diagnostics, context);
			case MetricPackage.METRIC_INSTANCE_MODEL:
				return validateMetricInstanceModel((MetricInstanceModel)value, diagnostics, context);
			case MetricPackage.WINDOW_SIZE_TYPE:
				return validateWindowSizeType((WindowSizeType)value, diagnostics, context);
			case MetricPackage.WINDOW_TYPE:
				return validateWindowType((WindowType)value, diagnostics, context);
			case MetricPackage.GROUPING_TYPE:
				return validateGroupingType((GroupingType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricInstance(MetricInstance metricInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)metricInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)metricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)metricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)metricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)metricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)metricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)metricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)metricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)metricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateMetricInstance_composite_metric_instance_to_components(metricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validateMetricInstance_component_instances_metric_map_component_metrics(metricInstance, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the composite_metric_instance_to_components constraint of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String METRIC_INSTANCE__COMPOSITE_METRIC_INSTANCE_TO_COMPONENTS__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Instance: ' + self.name + ' of composite metric:' + self.metricContext.metric.name + 'should have the same execution model as the one of all of its component metric instances',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(metricContext.metric.oclIsTypeOf(CompositeMetric)\n" +
		"\t\t\t\t\timplies self.composingMetricInstances->forAll(p | p.objectBinding.executionModel = self.objectBinding.executionModel))\n" +
		"}.status";

	/**
	 * Validates the composite_metric_instance_to_components constraint of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricInstance_composite_metric_instance_to_components(MetricInstance metricInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.METRIC_INSTANCE,
				 (EObject)metricInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "composite_metric_instance_to_components",
				 METRIC_INSTANCE__COMPOSITE_METRIC_INSTANCE_TO_COMPONENTS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the component_instances_metric_map_component_metrics constraint of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String METRIC_INSTANCE__COMPONENT_INSTANCES_METRIC_MAP_COMPONENT_METRICS__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'CompositeMetricInstance: ' + self.name + ' should have component metric instances which map to the component metrics referenced by the composite instance\\'s metric',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(metricContext.metric.oclIsTypeOf(CompositeMetric)\n" +
		"\t\t\t\t\timplies self.metricContext.metric.oclAsType(CompositeMetric).componentMetrics\n" +
		"\t\t\t\t\t->forAll(p | self.composingMetricInstances\n" +
		"\t\t\t\t\t\t->exists(q | q.metricContext.metric = p))\n" +
		"\t\t\t\t\tand\n" +
		"\t\t\t\t\tself.composingMetricInstances->forAll(inst | self.metricContext.metric.oclAsType(CompositeMetric).componentMetrics->exists(comp | comp = inst.metricContext.metric)))\n" +
		"}.status";

	/**
	 * Validates the component_instances_metric_map_component_metrics constraint of '<em>Instance</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricInstance_component_instances_metric_map_component_metrics(MetricInstance metricInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.METRIC_INSTANCE,
				 (EObject)metricInstance,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "component_instances_metric_map_component_metrics",
				 METRIC_INSTANCE__COMPONENT_INSTANCES_METRIC_MAP_COMPONENT_METRICS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetric(Metric metric, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)metric, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeMetric(CompositeMetric compositeMetric, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)compositeMetric, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)compositeMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)compositeMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)compositeMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)compositeMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)compositeMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)compositeMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)compositeMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)compositeMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validateCompositeMetric_composite_metric_contains_recurs(compositeMetric, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the composite_metric_contains_recurs constraint of '<em>Composite Metric</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMPOSITE_METRIC__COMPOSITE_METRIC_CONTAINS_RECURS__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'CompositeMetric: ' + self.name + ' should not contain itself recursively'\n" +
		"\t\t\t,\n" +
		"\tstatus : Boolean = asError(not self.containsMetric(self))\n" +
		"}.status";

	/**
	 * Validates the composite_metric_contains_recurs constraint of '<em>Composite Metric</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeMetric_composite_metric_contains_recurs(CompositeMetric compositeMetric, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.COMPOSITE_METRIC,
				 (EObject)compositeMetric,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "composite_metric_contains_recurs",
				 COMPOSITE_METRIC__COMPOSITE_METRIC_CONTAINS_RECURS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRawMetric(RawMetric rawMetric, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)rawMetric, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricVariable(MetricVariable metricVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)metricVariable, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)metricVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)metricVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)metricVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)metricVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)metricVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)metricVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)metricVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)metricVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateMetricVariable_metric_variable_current_config_candidates(metricVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateMetricVariable_metric_variable_contains_recurs(metricVariable, diagnostics, context);
		if (result || diagnostics != null) result &= validateMetricVariable_metric_variable_formula_components(metricVariable, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the metric_variable_current_config_candidates constraint of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String METRIC_VARIABLE__METRIC_VARIABLE_CURRENT_CONFIG_CANDIDATES__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'MetricVariable: ' + self.name + ' cannot span both the current configuration and the node candidates',\n" +
		"\tstatus : Boolean = \n" +
		" \t\t\t \tasError(currentConfiguration = false or onNodeCandidates = false)\n" +
		"}.status";

	/**
	 * Validates the metric_variable_current_config_candidates constraint of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricVariable_metric_variable_current_config_candidates(MetricVariable metricVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.METRIC_VARIABLE,
				 (EObject)metricVariable,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "metric_variable_current_config_candidates",
				 METRIC_VARIABLE__METRIC_VARIABLE_CURRENT_CONFIG_CANDIDATES__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * Validates the metric_variable_formula_components constraint of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricVariable_metric_variable_formula_components(MetricVariable metricVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "metric_variable_formula_components", getObjectLabel(metricVariable, context) },
						 new Object[] { metricVariable },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * The cached validation expression for the metric_variable_contains_recurs constraint of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String METRIC_VARIABLE__METRIC_VARIABLE_CONTAINS_RECURS__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'MetricVariable: ' + self.name + ' should not contain recursively itself'\n" +
		"\t\t\t,\n" +
		"\tstatus : Boolean = asError(not self.containsMetric(self))\n" +
		"}.status";

	/**
	 * Validates the metric_variable_contains_recurs constraint of '<em>Variable</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricVariable_metric_variable_contains_recurs(MetricVariable metricVariable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.METRIC_VARIABLE,
				 (EObject)metricVariable,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "metric_variable_contains_recurs",
				 METRIC_VARIABLE__METRIC_VARIABLE_CONTAINS_RECURS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricObjectBinding(MetricObjectBinding metricObjectBinding, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)metricObjectBinding, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSchedule(Schedule schedule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)schedule, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)schedule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)schedule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)schedule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)schedule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)schedule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)schedule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)schedule, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)schedule, diagnostics, context);
		if (result || diagnostics != null) result &= validateSchedule_schedule_correct_interval(schedule, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the schedule_correct_interval constraint of '<em>Schedule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SCHEDULE__SCHEDULE_CORRECT_INTERVAL__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Schedule: ' + self.name + ' should have a correct interval',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(interval >0)\n" +
		"}.status";

	/**
	 * Validates the schedule_correct_interval constraint of '<em>Schedule</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSchedule_schedule_correct_interval(Schedule schedule, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.SCHEDULE,
				 (EObject)schedule,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "schedule_correct_interval",
				 SCHEDULE__SCHEDULE_CORRECT_INTERVAL__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSensor(Sensor sensor, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)sensor, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)sensor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)sensor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)sensor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)sensor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)sensor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)sensor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)sensor, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)sensor, diagnostics, context);
		if (result || diagnostics != null) result &= validateSensor_sensor_correct_config(sensor, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the sensor_correct_config constraint of '<em>Sensor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SENSOR__SENSOR_CORRECT_CONFIG__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Sensor: ' + self.name + ' should either have a configuration string, if it belongs to the platform or be mapped to a Configuration object',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t asError((configuration <> null and self.configurations->size() = 0) xor (configuration = null and self.configurations->size() > 0))\n" +
		"}.status";

	/**
	 * Validates the sensor_correct_config constraint of '<em>Sensor</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSensor_sensor_correct_config(Sensor sensor, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.SENSOR,
				 (EObject)sensor,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "sensor_correct_config",
				 SENSOR__SENSOR_CORRECT_CONFIG__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWindow(Window window, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)window, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)window, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)window, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)window, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)window, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)window, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)window, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)window, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)window, diagnostics, context);
		if (result || diagnostics != null) result &= validateWindow_window_positive_params(window, diagnostics, context);
		if (result || diagnostics != null) result &= validateWindow_window_right_params_exist(window, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the window_positive_params constraint of '<em>Window</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WINDOW__WINDOW_POSITIVE_PARAMS__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Window:' + self.name + ' has a negative value for the measurementSize and timeSize attributes',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((measurementSize >= 0) and (timeSize >= 0))\n" +
		"}.status";

	/**
	 * Validates the window_positive_params constraint of '<em>Window</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWindow_window_positive_params(Window window, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.WINDOW,
				 (EObject)window,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "window_positive_params",
				 WINDOW__WINDOW_POSITIVE_PARAMS__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the window_right_params_exist constraint of '<em>Window</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String WINDOW__WINDOW_RIGHT_PARAMS_EXIST__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Window: ' + self.name + ' has wrong value combinations for its attributes and properties. If sizeType is MEASUREMENTS_ONLY, then the measurementSize should be positive and all other values zero or null. If sizeType is TIME_ONLY, then both the unit should not be null and the timeSize should be positive, while the measurementSize should be zero. For the other values of sizeType, the values of all remaining attributes and units should be provided',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((self.sizeType\n" +
		"\t\t\t\t\t= WindowSizeType::MEASUREMENTS_ONLY implies (timeUnit = null and timeSize = 0 and measurementSize > 0)) and\n" +
		"\t\t\t\t\t(self.sizeType = WindowSizeType::TIME_ONLY implies (timeUnit <> null and timeSize > 0 and measurementSize = 0)) and\n" +
		"\t\t\t\t\t((self.sizeType = WindowSizeType::FIRST_MATCH or self.sizeType = WindowSizeType::BOTH_MATCH) implies (timeSize > 0\n" +
		"\t\t\t\t\tand timeUnit <> null and measurementSize > 0)))\n" +
		"}.status";

	/**
	 * Validates the window_right_params_exist constraint of '<em>Window</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWindow_window_right_params_exist(Window window, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.WINDOW,
				 (EObject)window,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "window_right_params_exist",
				 WINDOW__WINDOW_RIGHT_PARAMS_EXIST__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricModel(MetricModel metricModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)metricModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricContext(MetricContext metricContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)metricContext, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeMetricContext(CompositeMetricContext compositeMetricContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)compositeMetricContext, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)compositeMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)compositeMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)compositeMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)compositeMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)compositeMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)compositeMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)compositeMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)compositeMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validateCompositeMetricContext_composite_metric_context_correct_metric_type(compositeMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validateCompositeMetricContext_metrics_in_composing_contexts_are_component_metrics(compositeMetricContext, diagnostics, context);
		return result;
	}

	/**
	 * Validates the metrics_in_composing_contexts_are_component_metrics constraint of '<em>Composite Metric Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeMetricContext_metrics_in_composing_contexts_are_component_metrics(CompositeMetricContext compositeMetricContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		// TODO implement the constraint
		// -> specify the condition that violates the constraint
		// -> verify the diagnostic details, including severity, code, and message
		// Ensure that you remove @generated or mark it @generated NOT
		if (false) {
			if (diagnostics != null) {
				diagnostics.add
					(createDiagnostic
						(Diagnostic.ERROR,
						 DIAGNOSTIC_SOURCE,
						 0,
						 "_UI_GenericConstraint_diagnostic",
						 new Object[] { "metrics_in_composing_contexts_are_component_metrics", getObjectLabel(compositeMetricContext, context) },
						 new Object[] { compositeMetricContext },
						 context));
			}
			return false;
		}
		return true;
	}

	/**
	 * The cached validation expression for the composite_metric_context_correct_metric_type constraint of '<em>Composite Metric Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMPOSITE_METRIC_CONTEXT__COMPOSITE_METRIC_CONTEXT_CORRECT_METRIC_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In CompositeMetricContext: ' + self.name + ' the metric: ' + self.metric.name + ' should be composite but it isn\\'t',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.metric.oclIsKindOf(CompositeMetric))\n" +
		"}.status";

	/**
	 * Validates the composite_metric_context_correct_metric_type constraint of '<em>Composite Metric Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeMetricContext_composite_metric_context_correct_metric_type(CompositeMetricContext compositeMetricContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.COMPOSITE_METRIC_CONTEXT,
				 (EObject)compositeMetricContext,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "composite_metric_context_correct_metric_type",
				 COMPOSITE_METRIC_CONTEXT__COMPOSITE_METRIC_CONTEXT_CORRECT_METRIC_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRawMetricContext(RawMetricContext rawMetricContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)rawMetricContext, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)rawMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)rawMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)rawMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)rawMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)rawMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)rawMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)rawMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)rawMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validateRawMetricContext_raw_metric_context_correct_sensor(rawMetricContext, diagnostics, context);
		if (result || diagnostics != null) result &= validateRawMetricContext_raw_metric_context_correct_metric_type(rawMetricContext, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the raw_metric_context_correct_sensor constraint of '<em>Raw Metric Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String RAW_METRIC_CONTEXT__RAW_METRIC_CONTEXT_CORRECT_SENSOR__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'RawMetricContext: ' + self.name + ' has a sensor which is not included in the sensors of the attribute measured by the metric of this context',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(let sensors: Sensor[*] = self.metric.metricTemplate.attribute.sensors in\n" +
		"\t\t\t\t\tsensors->size() > 0 implies sensors->includes(self.sensor))\n" +
		"}.status";

	/**
	 * Validates the raw_metric_context_correct_sensor constraint of '<em>Raw Metric Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRawMetricContext_raw_metric_context_correct_sensor(RawMetricContext rawMetricContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.RAW_METRIC_CONTEXT,
				 (EObject)rawMetricContext,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "raw_metric_context_correct_sensor",
				 RAW_METRIC_CONTEXT__RAW_METRIC_CONTEXT_CORRECT_SENSOR__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the raw_metric_context_correct_metric_type constraint of '<em>Raw Metric Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String RAW_METRIC_CONTEXT__RAW_METRIC_CONTEXT_CORRECT_METRIC_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In RawMetricContext: ' + self.name + ' the metric: ' + self.metric.name + ' should be raw but it isn\\'t',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\t\tasError(self.metric.oclIsKindOf(RawMetric))\n" +
		"}.status";

	/**
	 * Validates the raw_metric_context_correct_metric_type constraint of '<em>Raw Metric Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRawMetricContext_raw_metric_context_correct_metric_type(RawMetricContext rawMetricContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.RAW_METRIC_CONTEXT,
				 (EObject)rawMetricContext,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "raw_metric_context_correct_metric_type",
				 RAW_METRIC_CONTEXT__RAW_METRIC_CONTEXT_CORRECT_METRIC_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAttributeContext(AttributeContext attributeContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)attributeContext, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFunction(Function function, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)function, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricTemplate(MetricTemplate metricTemplate, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)metricTemplate, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateObjectContext(ObjectContext objectContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)objectContext, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)objectContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)objectContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)objectContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)objectContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)objectContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)objectContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)objectContext, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)objectContext, diagnostics, context);
		if (result || diagnostics != null) result &= validateObjectContext_object_context_either_data_component(objectContext, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the object_context_either_data_component constraint of '<em>Object Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String OBJECT_CONTEXT__OBJECT_CONTEXT_EITHER_DATA_COMPONENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In ObjectContext: ' + self.name + ' either the component or data should be supplied',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(component <> null or data <> null)\n" +
		"}.status";

	/**
	 * Validates the object_context_either_data_component constraint of '<em>Object Context</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateObjectContext_object_context_either_data_component(ObjectContext objectContext, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(MetricPackage.Literals.OBJECT_CONTEXT,
				 (EObject)objectContext,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "object_context_either_data_component",
				 OBJECT_CONTEXT__OBJECT_CONTEXT_EITHER_DATA_COMPONENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricTypeModel(MetricTypeModel metricTypeModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)metricTypeModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMetricInstanceModel(MetricInstanceModel metricInstanceModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)metricInstanceModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWindowSizeType(WindowSizeType windowSizeType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateWindowType(WindowType windowType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateGroupingType(GroupingType groupingType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //MetricValidator
