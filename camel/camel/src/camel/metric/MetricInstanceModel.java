/**
 */
package camel.metric;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Instance Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.MetricInstanceModel#getMetricInstances <em>Metric Instances</em>}</li>
 *   <li>{@link camel.metric.MetricInstanceModel#getBindings <em>Bindings</em>}</li>
 *   <li>{@link camel.metric.MetricInstanceModel#getType <em>Type</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getMetricInstanceModel()
 * @model
 * @generated
 */
public interface MetricInstanceModel extends MetricModel {
	/**
	 * Returns the value of the '<em><b>Metric Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.MetricInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Instances</em>' containment reference list.
	 * @see camel.metric.MetricPackage#getMetricInstanceModel_MetricInstances()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<MetricInstance> getMetricInstances();

	/**
	 * Returns the value of the '<em><b>Bindings</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.MetricObjectBinding}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Bindings</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Bindings</em>' containment reference list.
	 * @see camel.metric.MetricPackage#getMetricInstanceModel_Bindings()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<MetricObjectBinding> getBindings();

	/**
	 * Returns the value of the '<em><b>Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type</em>' reference.
	 * @see #setType(MetricTypeModel)
	 * @see camel.metric.MetricPackage#getMetricInstanceModel_Type()
	 * @model required="true"
	 * @generated
	 */
	MetricTypeModel getType();

	/**
	 * Sets the value of the '{@link camel.metric.MetricInstanceModel#getType <em>Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Type</em>' reference.
	 * @see #getType()
	 * @generated
	 */
	void setType(MetricTypeModel value);

} // MetricInstanceModel
