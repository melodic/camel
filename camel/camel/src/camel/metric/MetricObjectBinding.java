/**
 */
package camel.metric;

import camel.core.Feature;

import camel.deployment.ComponentInstance;

import camel.execution.ExecutionModel;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Object Binding</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.MetricObjectBinding#getExecutionModel <em>Execution Model</em>}</li>
 *   <li>{@link camel.metric.MetricObjectBinding#getComponentInstance <em>Component Instance</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getMetricObjectBinding()
 * @model
 * @generated
 */
public interface MetricObjectBinding extends Feature {
	/**
	 * Returns the value of the '<em><b>Execution Model</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Model</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Model</em>' reference.
	 * @see #setExecutionModel(ExecutionModel)
	 * @see camel.metric.MetricPackage#getMetricObjectBinding_ExecutionModel()
	 * @model required="true"
	 * @generated
	 */
	ExecutionModel getExecutionModel();

	/**
	 * Sets the value of the '{@link camel.metric.MetricObjectBinding#getExecutionModel <em>Execution Model</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Execution Model</em>' reference.
	 * @see #getExecutionModel()
	 * @generated
	 */
	void setExecutionModel(ExecutionModel value);

	/**
	 * Returns the value of the '<em><b>Component Instance</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Instance</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Instance</em>' reference.
	 * @see #setComponentInstance(ComponentInstance)
	 * @see camel.metric.MetricPackage#getMetricObjectBinding_ComponentInstance()
	 * @model
	 * @generated
	 */
	ComponentInstance getComponentInstance();

	/**
	 * Sets the value of the '{@link camel.metric.MetricObjectBinding#getComponentInstance <em>Component Instance</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Component Instance</em>' reference.
	 * @see #getComponentInstance()
	 * @generated
	 */
	void setComponentInstance(ComponentInstance value);

} // MetricObjectBinding
