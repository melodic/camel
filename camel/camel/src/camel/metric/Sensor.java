/**
 */
package camel.metric;

import camel.deployment.SoftwareComponent;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Sensor</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.Sensor#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link camel.metric.Sensor#isIsPush <em>Is Push</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getSensor()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='sensor_correct_config'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot sensor_correct_config='Tuple {\n\tmessage : String = \'Sensor: \' + self.name + \' should either have a configuration string, if it belongs to the platform or be mapped to a Configuration object\',\n\tstatus : Boolean = \n\t\t\t asError((configuration &lt;&gt; null and self.configurations-&gt;size() = 0) xor (configuration = null and self.configurations-&gt;size() &gt; 0))\n}.status'"
 * @generated
 */
public interface Sensor extends SoftwareComponent {
	/**
	 * Returns the value of the '<em><b>Configuration</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Configuration</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Configuration</em>' attribute.
	 * @see #setConfiguration(String)
	 * @see camel.metric.MetricPackage#getSensor_Configuration()
	 * @model
	 * @generated
	 */
	String getConfiguration();

	/**
	 * Sets the value of the '{@link camel.metric.Sensor#getConfiguration <em>Configuration</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Configuration</em>' attribute.
	 * @see #getConfiguration()
	 * @generated
	 */
	void setConfiguration(String value);

	/**
	 * Returns the value of the '<em><b>Is Push</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is Push</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is Push</em>' attribute.
	 * @see #setIsPush(boolean)
	 * @see camel.metric.MetricPackage#getSensor_IsPush()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isIsPush();

	/**
	 * Sets the value of the '{@link camel.metric.Sensor#isIsPush <em>Is Push</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is Push</em>' attribute.
	 * @see #isIsPush()
	 * @generated
	 */
	void setIsPush(boolean value);

} // Sensor
