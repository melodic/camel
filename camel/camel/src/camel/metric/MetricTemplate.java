/**
 */
package camel.metric;

import camel.core.Feature;
import camel.core.MeasurableAttribute;

import camel.type.ValueType;

import camel.unit.Unit;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Template</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.MetricTemplate#getValueType <em>Value Type</em>}</li>
 *   <li>{@link camel.metric.MetricTemplate#getValueDirection <em>Value Direction</em>}</li>
 *   <li>{@link camel.metric.MetricTemplate#getUnit <em>Unit</em>}</li>
 *   <li>{@link camel.metric.MetricTemplate#getAttribute <em>Attribute</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getMetricTemplate()
 * @model
 * @generated
 */
public interface MetricTemplate extends Feature {
	/**
	 * Returns the value of the '<em><b>Value Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Type</em>' reference.
	 * @see #setValueType(ValueType)
	 * @see camel.metric.MetricPackage#getMetricTemplate_ValueType()
	 * @model
	 * @generated
	 */
	ValueType getValueType();

	/**
	 * Sets the value of the '{@link camel.metric.MetricTemplate#getValueType <em>Value Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Type</em>' reference.
	 * @see #getValueType()
	 * @generated
	 */
	void setValueType(ValueType value);

	/**
	 * Returns the value of the '<em><b>Value Direction</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Direction</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Direction</em>' attribute.
	 * @see #setValueDirection(short)
	 * @see camel.metric.MetricPackage#getMetricTemplate_ValueDirection()
	 * @model required="true"
	 * @generated
	 */
	short getValueDirection();

	/**
	 * Sets the value of the '{@link camel.metric.MetricTemplate#getValueDirection <em>Value Direction</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Direction</em>' attribute.
	 * @see #getValueDirection()
	 * @generated
	 */
	void setValueDirection(short value);

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' reference.
	 * @see #setUnit(Unit)
	 * @see camel.metric.MetricPackage#getMetricTemplate_Unit()
	 * @model required="true"
	 * @generated
	 */
	Unit getUnit();

	/**
	 * Sets the value of the '{@link camel.metric.MetricTemplate#getUnit <em>Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' reference.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(Unit value);

	/**
	 * Returns the value of the '<em><b>Attribute</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Attribute</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Attribute</em>' reference.
	 * @see #setAttribute(MeasurableAttribute)
	 * @see camel.metric.MetricPackage#getMetricTemplate_Attribute()
	 * @model required="true"
	 * @generated
	 */
	MeasurableAttribute getAttribute();

	/**
	 * Sets the value of the '{@link camel.metric.MetricTemplate#getAttribute <em>Attribute</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Attribute</em>' reference.
	 * @see #getAttribute()
	 * @generated
	 */
	void setAttribute(MeasurableAttribute value);

} // MetricTemplate
