/**
 */
package camel.metric.impl;

import camel.core.Attribute;

import camel.core.impl.FeatureImpl;

import camel.metric.AttributeContext;
import camel.metric.MetricPackage;
import camel.metric.ObjectContext;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.AttributeContextImpl#getAttribute <em>Attribute</em>}</li>
 *   <li>{@link camel.metric.impl.AttributeContextImpl#getObjectContext <em>Object Context</em>}</li>
 * </ul>
 *
 * @generated
 */
public class AttributeContextImpl extends FeatureImpl implements AttributeContext {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected AttributeContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.ATTRIBUTE_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Attribute getAttribute() {
		return (Attribute)eGet(MetricPackage.Literals.ATTRIBUTE_CONTEXT__ATTRIBUTE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttribute(Attribute newAttribute) {
		eSet(MetricPackage.Literals.ATTRIBUTE_CONTEXT__ATTRIBUTE, newAttribute);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectContext getObjectContext() {
		return (ObjectContext)eGet(MetricPackage.Literals.ATTRIBUTE_CONTEXT__OBJECT_CONTEXT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectContext(ObjectContext newObjectContext) {
		eSet(MetricPackage.Literals.ATTRIBUTE_CONTEXT__OBJECT_CONTEXT, newObjectContext);
	}

} //AttributeContextImpl
