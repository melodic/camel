/**
 */
package camel.metric.impl;

import camel.deployment.Component;

import camel.metric.Metric;
import camel.metric.MetricPackage;
import camel.metric.MetricVariable;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Variable</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.MetricVariableImpl#isCurrentConfiguration <em>Current Configuration</em>}</li>
 *   <li>{@link camel.metric.impl.MetricVariableImpl#getComponent <em>Component</em>}</li>
 *   <li>{@link camel.metric.impl.MetricVariableImpl#isOnNodeCandidates <em>On Node Candidates</em>}</li>
 *   <li>{@link camel.metric.impl.MetricVariableImpl#getFormula <em>Formula</em>}</li>
 *   <li>{@link camel.metric.impl.MetricVariableImpl#getComponentMetrics <em>Component Metrics</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetricVariableImpl extends MetricImpl implements MetricVariable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricVariableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.METRIC_VARIABLE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isCurrentConfiguration() {
		return (Boolean)eGet(MetricPackage.Literals.METRIC_VARIABLE__CURRENT_CONFIGURATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCurrentConfiguration(boolean newCurrentConfiguration) {
		eSet(MetricPackage.Literals.METRIC_VARIABLE__CURRENT_CONFIGURATION, newCurrentConfiguration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component getComponent() {
		return (Component)eGet(MetricPackage.Literals.METRIC_VARIABLE__COMPONENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponent(Component newComponent) {
		eSet(MetricPackage.Literals.METRIC_VARIABLE__COMPONENT, newComponent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isOnNodeCandidates() {
		return (Boolean)eGet(MetricPackage.Literals.METRIC_VARIABLE__ON_NODE_CANDIDATES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOnNodeCandidates(boolean newOnNodeCandidates) {
		eSet(MetricPackage.Literals.METRIC_VARIABLE__ON_NODE_CANDIDATES, newOnNodeCandidates);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormula() {
		return (String)eGet(MetricPackage.Literals.METRIC_VARIABLE__FORMULA, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormula(String newFormula) {
		eSet(MetricPackage.Literals.METRIC_VARIABLE__FORMULA, newFormula);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Metric> getComponentMetrics() {
		return (EList<Metric>)eGet(MetricPackage.Literals.METRIC_VARIABLE__COMPONENT_METRICS, true);
	}

	/**
	 * The cached invocation delegate for the '{@link #containsMetric(camel.metric.Metric) <em>Contains Metric</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #containsMetric(camel.metric.Metric)
	 * @generated
	 * @ordered
	 */
	protected static final EOperation.Internal.InvocationDelegate CONTAINS_METRIC_METRIC__EINVOCATION_DELEGATE = ((EOperation.Internal)MetricPackage.Literals.METRIC_VARIABLE___CONTAINS_METRIC__METRIC).getInvocationDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean containsMetric(Metric metric) {
		try {
			return (Boolean)CONTAINS_METRIC_METRIC__EINVOCATION_DELEGATE.dynamicInvoke(this, new BasicEList.UnmodifiableEList<Object>(1, new Object[]{metric}));
		}
		catch (InvocationTargetException ite) {
			throw new WrappedException(ite);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MetricPackage.METRIC_VARIABLE___CONTAINS_METRIC__METRIC:
				return containsMetric((Metric)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //MetricVariableImpl
