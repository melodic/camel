/**
 */
package camel.metric.impl;

import camel.core.impl.FeatureImpl;

import camel.metric.MetricPackage;
import camel.metric.Schedule;

import camel.unit.Unit;

import java.util.Date;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Schedule</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.ScheduleImpl#getStart <em>Start</em>}</li>
 *   <li>{@link camel.metric.impl.ScheduleImpl#getEnd <em>End</em>}</li>
 *   <li>{@link camel.metric.impl.ScheduleImpl#getTimeUnit <em>Time Unit</em>}</li>
 *   <li>{@link camel.metric.impl.ScheduleImpl#getRepetitions <em>Repetitions</em>}</li>
 *   <li>{@link camel.metric.impl.ScheduleImpl#getInterval <em>Interval</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ScheduleImpl extends FeatureImpl implements Schedule {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ScheduleImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.SCHEDULE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getStart() {
		return (Date)eGet(MetricPackage.Literals.SCHEDULE__START, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStart(Date newStart) {
		eSet(MetricPackage.Literals.SCHEDULE__START, newStart);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Date getEnd() {
		return (Date)eGet(MetricPackage.Literals.SCHEDULE__END, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEnd(Date newEnd) {
		eSet(MetricPackage.Literals.SCHEDULE__END, newEnd);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Unit getTimeUnit() {
		return (Unit)eGet(MetricPackage.Literals.SCHEDULE__TIME_UNIT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeUnit(Unit newTimeUnit) {
		eSet(MetricPackage.Literals.SCHEDULE__TIME_UNIT, newTimeUnit);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getRepetitions() {
		return (Integer)eGet(MetricPackage.Literals.SCHEDULE__REPETITIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRepetitions(int newRepetitions) {
		eSet(MetricPackage.Literals.SCHEDULE__REPETITIONS, newRepetitions);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getInterval() {
		return (Long)eGet(MetricPackage.Literals.SCHEDULE__INTERVAL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInterval(long newInterval) {
		eSet(MetricPackage.Literals.SCHEDULE__INTERVAL, newInterval);
	}

} //ScheduleImpl
