/**
 */
package camel.metric.impl;

import camel.constraint.ConstraintPackage;

import camel.constraint.impl.ConstraintPackageImpl;

import camel.core.CorePackage;

import camel.core.impl.CorePackageImpl;

import camel.data.DataPackage;

import camel.data.impl.DataPackageImpl;

import camel.deployment.DeploymentPackage;

import camel.deployment.impl.DeploymentPackageImpl;

import camel.execution.ExecutionPackage;

import camel.execution.impl.ExecutionPackageImpl;

import camel.location.LocationPackage;

import camel.location.impl.LocationPackageImpl;

import camel.metric.AttributeContext;
import camel.metric.CompositeMetric;
import camel.metric.CompositeMetricContext;
import camel.metric.Function;
import camel.metric.GroupingType;
import camel.metric.Metric;
import camel.metric.MetricContext;
import camel.metric.MetricFactory;
import camel.metric.MetricInstance;
import camel.metric.MetricInstanceModel;
import camel.metric.MetricModel;
import camel.metric.MetricObjectBinding;
import camel.metric.MetricPackage;
import camel.metric.MetricTemplate;
import camel.metric.MetricTypeModel;
import camel.metric.MetricVariable;
import camel.metric.ObjectContext;
import camel.metric.RawMetric;
import camel.metric.RawMetricContext;
import camel.metric.Schedule;
import camel.metric.Sensor;
import camel.metric.Window;
import camel.metric.WindowSizeType;
import camel.metric.WindowType;

import camel.metric.util.MetricValidator;

import camel.mms.MmsPackage;

import camel.mms.impl.MmsPackageImpl;

import camel.organisation.OrganisationPackage;

import camel.organisation.impl.OrganisationPackageImpl;

import camel.requirement.RequirementPackage;

import camel.requirement.impl.RequirementPackageImpl;

import camel.scalability.ScalabilityPackage;

import camel.scalability.impl.ScalabilityPackageImpl;

import camel.security.SecurityPackage;

import camel.security.impl.SecurityPackageImpl;

import camel.type.TypePackage;

import camel.type.impl.TypePackageImpl;

import camel.unit.UnitPackage;

import camel.unit.impl.UnitPackageImpl;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EValidator;

import org.eclipse.emf.ecore.impl.EPackageImpl;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MetricPackageImpl extends EPackageImpl implements MetricPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metricInstanceEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metricEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeMetricEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rawMetricEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metricVariableEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metricObjectBindingEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass scheduleEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass sensorEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass windowEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metricModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metricContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass compositeMetricContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass rawMetricContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass attributeContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass functionEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metricTemplateEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass objectContextEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metricTypeModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass metricInstanceModelEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum windowSizeTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum windowTypeEEnum = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EEnum groupingTypeEEnum = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see camel.metric.MetricPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private MetricPackageImpl() {
		super(eNS_URI, MetricFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 * 
	 * <p>This method is used to initialize {@link MetricPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static MetricPackage init() {
		if (isInited) return (MetricPackage)EPackage.Registry.INSTANCE.getEPackage(MetricPackage.eNS_URI);

		// Obtain or create and register package
		MetricPackageImpl theMetricPackage = (MetricPackageImpl)(EPackage.Registry.INSTANCE.get(eNS_URI) instanceof MetricPackageImpl ? EPackage.Registry.INSTANCE.get(eNS_URI) : new MetricPackageImpl());

		isInited = true;

		// Obtain or create and register interdependencies
		MmsPackageImpl theMmsPackage = (MmsPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI) instanceof MmsPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(MmsPackage.eNS_URI) : MmsPackage.eINSTANCE);
		CorePackageImpl theCorePackage = (CorePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) instanceof CorePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI) : CorePackage.eINSTANCE);
		DeploymentPackageImpl theDeploymentPackage = (DeploymentPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI) instanceof DeploymentPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI) : DeploymentPackage.eINSTANCE);
		ExecutionPackageImpl theExecutionPackage = (ExecutionPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI) instanceof ExecutionPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI) : ExecutionPackage.eINSTANCE);
		LocationPackageImpl theLocationPackage = (LocationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) instanceof LocationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(LocationPackage.eNS_URI) : LocationPackage.eINSTANCE);
		OrganisationPackageImpl theOrganisationPackage = (OrganisationPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) instanceof OrganisationPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(OrganisationPackage.eNS_URI) : OrganisationPackage.eINSTANCE);
		RequirementPackageImpl theRequirementPackage = (RequirementPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) instanceof RequirementPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(RequirementPackage.eNS_URI) : RequirementPackage.eINSTANCE);
		ScalabilityPackageImpl theScalabilityPackage = (ScalabilityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI) instanceof ScalabilityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ScalabilityPackage.eNS_URI) : ScalabilityPackage.eINSTANCE);
		SecurityPackageImpl theSecurityPackage = (SecurityPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) instanceof SecurityPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(SecurityPackage.eNS_URI) : SecurityPackage.eINSTANCE);
		TypePackageImpl theTypePackage = (TypePackageImpl)(EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI) instanceof TypePackageImpl ? EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI) : TypePackage.eINSTANCE);
		UnitPackageImpl theUnitPackage = (UnitPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) instanceof UnitPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI) : UnitPackage.eINSTANCE);
		ConstraintPackageImpl theConstraintPackage = (ConstraintPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) instanceof ConstraintPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(ConstraintPackage.eNS_URI) : ConstraintPackage.eINSTANCE);
		DataPackageImpl theDataPackage = (DataPackageImpl)(EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) instanceof DataPackageImpl ? EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI) : DataPackage.eINSTANCE);

		// Create package meta-data objects
		theMetricPackage.createPackageContents();
		theMmsPackage.createPackageContents();
		theCorePackage.createPackageContents();
		theDeploymentPackage.createPackageContents();
		theExecutionPackage.createPackageContents();
		theLocationPackage.createPackageContents();
		theOrganisationPackage.createPackageContents();
		theRequirementPackage.createPackageContents();
		theScalabilityPackage.createPackageContents();
		theSecurityPackage.createPackageContents();
		theTypePackage.createPackageContents();
		theUnitPackage.createPackageContents();
		theConstraintPackage.createPackageContents();
		theDataPackage.createPackageContents();

		// Initialize created meta-data
		theMetricPackage.initializePackageContents();
		theMmsPackage.initializePackageContents();
		theCorePackage.initializePackageContents();
		theDeploymentPackage.initializePackageContents();
		theExecutionPackage.initializePackageContents();
		theLocationPackage.initializePackageContents();
		theOrganisationPackage.initializePackageContents();
		theRequirementPackage.initializePackageContents();
		theScalabilityPackage.initializePackageContents();
		theSecurityPackage.initializePackageContents();
		theTypePackage.initializePackageContents();
		theUnitPackage.initializePackageContents();
		theConstraintPackage.initializePackageContents();
		theDataPackage.initializePackageContents();

		// Register package validator
		EValidator.Registry.INSTANCE.put
			(theMetricPackage, 
			 new EValidator.Descriptor() {
				 public EValidator getEValidator() {
					 return MetricValidator.INSTANCE;
				 }
			 });

		// Mark meta-data to indicate it can't be changed
		theMetricPackage.freeze();

  
		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(MetricPackage.eNS_URI, theMetricPackage);
		return theMetricPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetricInstance() {
		return metricInstanceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricInstance_ObjectBinding() {
		return (EReference)metricInstanceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricInstance_MetricContext() {
		return (EReference)metricInstanceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricInstance_ComposingMetricInstances() {
		return (EReference)metricInstanceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetric() {
		return metricEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetric_MetricTemplate() {
		return (EReference)metricEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMetric__CheckRecursiveness__Metric_Metric() {
		return metricEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeMetric() {
		return compositeMetricEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCompositeMetric_Formula() {
		return (EAttribute)compositeMetricEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeMetric_ComponentMetrics() {
		return (EReference)compositeMetricEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getCompositeMetric__ContainsMetric__Metric() {
		return compositeMetricEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRawMetric() {
		return rawMetricEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetricVariable() {
		return metricVariableEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMetricVariable_CurrentConfiguration() {
		return (EAttribute)metricVariableEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricVariable_Component() {
		return (EReference)metricVariableEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMetricVariable_OnNodeCandidates() {
		return (EAttribute)metricVariableEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMetricVariable_Formula() {
		return (EAttribute)metricVariableEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricVariable_ComponentMetrics() {
		return (EReference)metricVariableEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EOperation getMetricVariable__ContainsMetric__Metric() {
		return metricVariableEClass.getEOperations().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetricObjectBinding() {
		return metricObjectBindingEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricObjectBinding_ExecutionModel() {
		return (EReference)metricObjectBindingEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricObjectBinding_ComponentInstance() {
		return (EReference)metricObjectBindingEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSchedule() {
		return scheduleEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSchedule_Start() {
		return (EAttribute)scheduleEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSchedule_End() {
		return (EAttribute)scheduleEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getSchedule_TimeUnit() {
		return (EReference)scheduleEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSchedule_Repetitions() {
		return (EAttribute)scheduleEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSchedule_Interval() {
		return (EAttribute)scheduleEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getSensor() {
		return sensorEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSensor_Configuration() {
		return (EAttribute)sensorEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getSensor_IsPush() {
		return (EAttribute)sensorEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getWindow() {
		return windowEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getWindow_TimeUnit() {
		return (EReference)windowEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWindow_WindowType() {
		return (EAttribute)windowEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWindow_SizeType() {
		return (EAttribute)windowEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWindow_MeasurementSize() {
		return (EAttribute)windowEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getWindow_TimeSize() {
		return (EAttribute)windowEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetricModel() {
		return metricModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetricContext() {
		return metricContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricContext_Metric() {
		return (EReference)metricContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricContext_Window() {
		return (EReference)metricContextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricContext_Schedule() {
		return (EReference)metricContextEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricContext_ObjectContext() {
		return (EReference)metricContextEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getCompositeMetricContext() {
		return compositeMetricContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getCompositeMetricContext_GroupingType() {
		return (EAttribute)compositeMetricContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getCompositeMetricContext_ComposingMetricContexts() {
		return (EReference)compositeMetricContextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getRawMetricContext() {
		return rawMetricContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getRawMetricContext_Sensor() {
		return (EReference)rawMetricContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAttributeContext() {
		return attributeContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeContext_Attribute() {
		return (EReference)attributeContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAttributeContext_ObjectContext() {
		return (EReference)attributeContextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getFunction() {
		return functionEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunction_Expression() {
		return (EAttribute)functionEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getFunction_Arguments() {
		return (EAttribute)functionEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetricTemplate() {
		return metricTemplateEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTemplate_ValueType() {
		return (EReference)metricTemplateEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EAttribute getMetricTemplate_ValueDirection() {
		return (EAttribute)metricTemplateEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTemplate_Unit() {
		return (EReference)metricTemplateEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTemplate_Attribute() {
		return (EReference)metricTemplateEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getObjectContext() {
		return objectContextEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectContext_Component() {
		return (EReference)objectContextEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getObjectContext_Data() {
		return (EReference)objectContextEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetricTypeModel() {
		return metricTypeModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTypeModel_MetricContexts() {
		return (EReference)metricTypeModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTypeModel_AttributeContexts() {
		return (EReference)metricTypeModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTypeModel_ObjectContexts() {
		return (EReference)metricTypeModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTypeModel_Metrics() {
		return (EReference)metricTypeModelEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTypeModel_Windows() {
		return (EReference)metricTypeModelEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTypeModel_Schedules() {
		return (EReference)metricTypeModelEClass.getEStructuralFeatures().get(5);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTypeModel_Sensors() {
		return (EReference)metricTypeModelEClass.getEStructuralFeatures().get(6);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTypeModel_Functions() {
		return (EReference)metricTypeModelEClass.getEStructuralFeatures().get(7);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricTypeModel_Templates() {
		return (EReference)metricTypeModelEClass.getEStructuralFeatures().get(8);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getMetricInstanceModel() {
		return metricInstanceModelEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricInstanceModel_MetricInstances() {
		return (EReference)metricInstanceModelEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricInstanceModel_Bindings() {
		return (EReference)metricInstanceModelEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getMetricInstanceModel_Type() {
		return (EReference)metricInstanceModelEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getWindowSizeType() {
		return windowSizeTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getWindowType() {
		return windowTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EEnum getGroupingType() {
		return groupingTypeEEnum;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricFactory getMetricFactory() {
		return (MetricFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		metricInstanceEClass = createEClass(METRIC_INSTANCE);
		createEReference(metricInstanceEClass, METRIC_INSTANCE__OBJECT_BINDING);
		createEReference(metricInstanceEClass, METRIC_INSTANCE__METRIC_CONTEXT);
		createEReference(metricInstanceEClass, METRIC_INSTANCE__COMPOSING_METRIC_INSTANCES);

		metricEClass = createEClass(METRIC);
		createEReference(metricEClass, METRIC__METRIC_TEMPLATE);
		createEOperation(metricEClass, METRIC___CHECK_RECURSIVENESS__METRIC_METRIC);

		compositeMetricEClass = createEClass(COMPOSITE_METRIC);
		createEAttribute(compositeMetricEClass, COMPOSITE_METRIC__FORMULA);
		createEReference(compositeMetricEClass, COMPOSITE_METRIC__COMPONENT_METRICS);
		createEOperation(compositeMetricEClass, COMPOSITE_METRIC___CONTAINS_METRIC__METRIC);

		rawMetricEClass = createEClass(RAW_METRIC);

		metricVariableEClass = createEClass(METRIC_VARIABLE);
		createEAttribute(metricVariableEClass, METRIC_VARIABLE__CURRENT_CONFIGURATION);
		createEReference(metricVariableEClass, METRIC_VARIABLE__COMPONENT);
		createEAttribute(metricVariableEClass, METRIC_VARIABLE__ON_NODE_CANDIDATES);
		createEAttribute(metricVariableEClass, METRIC_VARIABLE__FORMULA);
		createEReference(metricVariableEClass, METRIC_VARIABLE__COMPONENT_METRICS);
		createEOperation(metricVariableEClass, METRIC_VARIABLE___CONTAINS_METRIC__METRIC);

		metricObjectBindingEClass = createEClass(METRIC_OBJECT_BINDING);
		createEReference(metricObjectBindingEClass, METRIC_OBJECT_BINDING__EXECUTION_MODEL);
		createEReference(metricObjectBindingEClass, METRIC_OBJECT_BINDING__COMPONENT_INSTANCE);

		scheduleEClass = createEClass(SCHEDULE);
		createEAttribute(scheduleEClass, SCHEDULE__START);
		createEAttribute(scheduleEClass, SCHEDULE__END);
		createEReference(scheduleEClass, SCHEDULE__TIME_UNIT);
		createEAttribute(scheduleEClass, SCHEDULE__REPETITIONS);
		createEAttribute(scheduleEClass, SCHEDULE__INTERVAL);

		sensorEClass = createEClass(SENSOR);
		createEAttribute(sensorEClass, SENSOR__CONFIGURATION);
		createEAttribute(sensorEClass, SENSOR__IS_PUSH);

		windowEClass = createEClass(WINDOW);
		createEReference(windowEClass, WINDOW__TIME_UNIT);
		createEAttribute(windowEClass, WINDOW__WINDOW_TYPE);
		createEAttribute(windowEClass, WINDOW__SIZE_TYPE);
		createEAttribute(windowEClass, WINDOW__MEASUREMENT_SIZE);
		createEAttribute(windowEClass, WINDOW__TIME_SIZE);

		metricModelEClass = createEClass(METRIC_MODEL);

		metricContextEClass = createEClass(METRIC_CONTEXT);
		createEReference(metricContextEClass, METRIC_CONTEXT__METRIC);
		createEReference(metricContextEClass, METRIC_CONTEXT__WINDOW);
		createEReference(metricContextEClass, METRIC_CONTEXT__SCHEDULE);
		createEReference(metricContextEClass, METRIC_CONTEXT__OBJECT_CONTEXT);

		compositeMetricContextEClass = createEClass(COMPOSITE_METRIC_CONTEXT);
		createEAttribute(compositeMetricContextEClass, COMPOSITE_METRIC_CONTEXT__GROUPING_TYPE);
		createEReference(compositeMetricContextEClass, COMPOSITE_METRIC_CONTEXT__COMPOSING_METRIC_CONTEXTS);

		rawMetricContextEClass = createEClass(RAW_METRIC_CONTEXT);
		createEReference(rawMetricContextEClass, RAW_METRIC_CONTEXT__SENSOR);

		attributeContextEClass = createEClass(ATTRIBUTE_CONTEXT);
		createEReference(attributeContextEClass, ATTRIBUTE_CONTEXT__ATTRIBUTE);
		createEReference(attributeContextEClass, ATTRIBUTE_CONTEXT__OBJECT_CONTEXT);

		functionEClass = createEClass(FUNCTION);
		createEAttribute(functionEClass, FUNCTION__EXPRESSION);
		createEAttribute(functionEClass, FUNCTION__ARGUMENTS);

		metricTemplateEClass = createEClass(METRIC_TEMPLATE);
		createEReference(metricTemplateEClass, METRIC_TEMPLATE__VALUE_TYPE);
		createEAttribute(metricTemplateEClass, METRIC_TEMPLATE__VALUE_DIRECTION);
		createEReference(metricTemplateEClass, METRIC_TEMPLATE__UNIT);
		createEReference(metricTemplateEClass, METRIC_TEMPLATE__ATTRIBUTE);

		objectContextEClass = createEClass(OBJECT_CONTEXT);
		createEReference(objectContextEClass, OBJECT_CONTEXT__COMPONENT);
		createEReference(objectContextEClass, OBJECT_CONTEXT__DATA);

		metricTypeModelEClass = createEClass(METRIC_TYPE_MODEL);
		createEReference(metricTypeModelEClass, METRIC_TYPE_MODEL__METRIC_CONTEXTS);
		createEReference(metricTypeModelEClass, METRIC_TYPE_MODEL__ATTRIBUTE_CONTEXTS);
		createEReference(metricTypeModelEClass, METRIC_TYPE_MODEL__OBJECT_CONTEXTS);
		createEReference(metricTypeModelEClass, METRIC_TYPE_MODEL__METRICS);
		createEReference(metricTypeModelEClass, METRIC_TYPE_MODEL__WINDOWS);
		createEReference(metricTypeModelEClass, METRIC_TYPE_MODEL__SCHEDULES);
		createEReference(metricTypeModelEClass, METRIC_TYPE_MODEL__SENSORS);
		createEReference(metricTypeModelEClass, METRIC_TYPE_MODEL__FUNCTIONS);
		createEReference(metricTypeModelEClass, METRIC_TYPE_MODEL__TEMPLATES);

		metricInstanceModelEClass = createEClass(METRIC_INSTANCE_MODEL);
		createEReference(metricInstanceModelEClass, METRIC_INSTANCE_MODEL__METRIC_INSTANCES);
		createEReference(metricInstanceModelEClass, METRIC_INSTANCE_MODEL__BINDINGS);
		createEReference(metricInstanceModelEClass, METRIC_INSTANCE_MODEL__TYPE);

		// Create enums
		windowSizeTypeEEnum = createEEnum(WINDOW_SIZE_TYPE);
		windowTypeEEnum = createEEnum(WINDOW_TYPE);
		groupingTypeEEnum = createEEnum(GROUPING_TYPE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		CorePackage theCorePackage = (CorePackage)EPackage.Registry.INSTANCE.getEPackage(CorePackage.eNS_URI);
		DeploymentPackage theDeploymentPackage = (DeploymentPackage)EPackage.Registry.INSTANCE.getEPackage(DeploymentPackage.eNS_URI);
		ExecutionPackage theExecutionPackage = (ExecutionPackage)EPackage.Registry.INSTANCE.getEPackage(ExecutionPackage.eNS_URI);
		UnitPackage theUnitPackage = (UnitPackage)EPackage.Registry.INSTANCE.getEPackage(UnitPackage.eNS_URI);
		TypePackage theTypePackage = (TypePackage)EPackage.Registry.INSTANCE.getEPackage(TypePackage.eNS_URI);
		DataPackage theDataPackage = (DataPackage)EPackage.Registry.INSTANCE.getEPackage(DataPackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes
		metricInstanceEClass.getESuperTypes().add(theCorePackage.getFeature());
		metricEClass.getESuperTypes().add(theCorePackage.getFeature());
		compositeMetricEClass.getESuperTypes().add(this.getMetric());
		rawMetricEClass.getESuperTypes().add(this.getMetric());
		metricVariableEClass.getESuperTypes().add(this.getMetric());
		metricObjectBindingEClass.getESuperTypes().add(theCorePackage.getFeature());
		scheduleEClass.getESuperTypes().add(theCorePackage.getFeature());
		sensorEClass.getESuperTypes().add(theDeploymentPackage.getSoftwareComponent());
		windowEClass.getESuperTypes().add(theCorePackage.getFeature());
		metricModelEClass.getESuperTypes().add(theCorePackage.getModel());
		metricContextEClass.getESuperTypes().add(theCorePackage.getFeature());
		compositeMetricContextEClass.getESuperTypes().add(this.getMetricContext());
		rawMetricContextEClass.getESuperTypes().add(this.getMetricContext());
		attributeContextEClass.getESuperTypes().add(theCorePackage.getFeature());
		functionEClass.getESuperTypes().add(theCorePackage.getFeature());
		metricTemplateEClass.getESuperTypes().add(theCorePackage.getFeature());
		objectContextEClass.getESuperTypes().add(theCorePackage.getFeature());
		metricTypeModelEClass.getESuperTypes().add(this.getMetricModel());
		metricInstanceModelEClass.getESuperTypes().add(this.getMetricModel());

		// Initialize classes, features, and operations; add parameters
		initEClass(metricInstanceEClass, MetricInstance.class, "MetricInstance", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMetricInstance_ObjectBinding(), this.getMetricObjectBinding(), null, "objectBinding", null, 1, 1, MetricInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricInstance_MetricContext(), this.getMetricContext(), null, "metricContext", null, 0, 1, MetricInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricInstance_ComposingMetricInstances(), this.getMetricInstance(), null, "composingMetricInstances", null, 0, -1, MetricInstance.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);

		initEClass(metricEClass, Metric.class, "Metric", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMetric_MetricTemplate(), this.getMetricTemplate(), null, "metricTemplate", null, 1, 1, Metric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		EOperation op = initEOperation(getMetric__CheckRecursiveness__Metric_Metric(), ecorePackage.getEBoolean(), "checkRecursiveness", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMetric(), "mt1", 0, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMetric(), "mt2", 0, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(compositeMetricEClass, CompositeMetric.class, "CompositeMetric", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCompositeMetric_Formula(), ecorePackage.getEString(), "formula", null, 1, 1, CompositeMetric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCompositeMetric_ComponentMetrics(), this.getMetric(), null, "componentMetrics", null, 0, -1, CompositeMetric.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getCompositeMetric__ContainsMetric__Metric(), ecorePackage.getEBoolean(), "containsMetric", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMetric(), "metric", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(rawMetricEClass, RawMetric.class, "RawMetric", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(metricVariableEClass, MetricVariable.class, "MetricVariable", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getMetricVariable_CurrentConfiguration(), ecorePackage.getEBoolean(), "currentConfiguration", "false", 1, 1, MetricVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricVariable_Component(), theDeploymentPackage.getComponent(), null, "component", null, 0, 1, MetricVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMetricVariable_OnNodeCandidates(), ecorePackage.getEBoolean(), "onNodeCandidates", "false", 1, 1, MetricVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMetricVariable_Formula(), ecorePackage.getEString(), "formula", null, 0, 1, MetricVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricVariable_ComponentMetrics(), this.getMetric(), null, "componentMetrics", null, 0, -1, MetricVariable.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		op = initEOperation(getMetricVariable__ContainsMetric__Metric(), ecorePackage.getEBoolean(), "containsMetric", 1, 1, IS_UNIQUE, IS_ORDERED);
		addEParameter(op, this.getMetric(), "metric", 1, 1, IS_UNIQUE, IS_ORDERED);

		initEClass(metricObjectBindingEClass, MetricObjectBinding.class, "MetricObjectBinding", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMetricObjectBinding_ExecutionModel(), theExecutionPackage.getExecutionModel(), null, "executionModel", null, 1, 1, MetricObjectBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricObjectBinding_ComponentInstance(), theDeploymentPackage.getComponentInstance(), null, "componentInstance", null, 0, 1, MetricObjectBinding.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(scheduleEClass, Schedule.class, "Schedule", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSchedule_Start(), ecorePackage.getEDate(), "start", null, 0, 1, Schedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSchedule_End(), ecorePackage.getEDate(), "end", null, 0, 1, Schedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getSchedule_TimeUnit(), theUnitPackage.getUnit(), null, "timeUnit", null, 1, 1, Schedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSchedule_Repetitions(), ecorePackage.getEInt(), "repetitions", null, 1, 1, Schedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSchedule_Interval(), ecorePackage.getELong(), "interval", null, 1, 1, Schedule.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(sensorEClass, Sensor.class, "Sensor", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getSensor_Configuration(), ecorePackage.getEString(), "configuration", null, 0, 1, Sensor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getSensor_IsPush(), ecorePackage.getEBoolean(), "isPush", "false", 1, 1, Sensor.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(windowEClass, Window.class, "Window", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getWindow_TimeUnit(), theUnitPackage.getUnit(), null, "timeUnit", null, 0, 1, Window.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWindow_WindowType(), this.getWindowType(), "windowType", null, 1, 1, Window.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWindow_SizeType(), this.getWindowSizeType(), "sizeType", null, 1, 1, Window.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWindow_MeasurementSize(), ecorePackage.getELong(), "measurementSize", null, 1, 1, Window.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getWindow_TimeSize(), ecorePackage.getELong(), "timeSize", null, 1, 1, Window.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(metricModelEClass, MetricModel.class, "MetricModel", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);

		initEClass(metricContextEClass, MetricContext.class, "MetricContext", IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMetricContext_Metric(), this.getMetric(), null, "metric", null, 1, 1, MetricContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricContext_Window(), this.getWindow(), null, "window", null, 0, 1, MetricContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricContext_Schedule(), this.getSchedule(), null, "schedule", null, 0, 1, MetricContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricContext_ObjectContext(), this.getObjectContext(), null, "objectContext", null, 0, 1, MetricContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(compositeMetricContextEClass, CompositeMetricContext.class, "CompositeMetricContext", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getCompositeMetricContext_GroupingType(), this.getGroupingType(), "groupingType", "PER_INSTANCE", 1, 1, CompositeMetricContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getCompositeMetricContext_ComposingMetricContexts(), this.getMetricContext(), null, "composingMetricContexts", null, 0, -1, CompositeMetricContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(rawMetricContextEClass, RawMetricContext.class, "RawMetricContext", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getRawMetricContext_Sensor(), this.getSensor(), null, "sensor", null, 1, 1, RawMetricContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(attributeContextEClass, AttributeContext.class, "AttributeContext", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAttributeContext_Attribute(), theCorePackage.getAttribute(), null, "attribute", null, 1, 1, AttributeContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAttributeContext_ObjectContext(), this.getObjectContext(), null, "objectContext", null, 0, 1, AttributeContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(functionEClass, Function.class, "Function", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEAttribute(getFunction_Expression(), ecorePackage.getEString(), "expression", null, 1, 1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getFunction_Arguments(), ecorePackage.getEString(), "arguments", null, 1, -1, Function.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(metricTemplateEClass, MetricTemplate.class, "MetricTemplate", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMetricTemplate_ValueType(), theTypePackage.getValueType(), null, "valueType", null, 0, 1, MetricTemplate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEAttribute(getMetricTemplate_ValueDirection(), ecorePackage.getEShort(), "valueDirection", null, 1, 1, MetricTemplate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_UNSETTABLE, !IS_ID, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricTemplate_Unit(), theUnitPackage.getUnit(), null, "unit", null, 1, 1, MetricTemplate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricTemplate_Attribute(), theCorePackage.getMeasurableAttribute(), null, "attribute", null, 1, 1, MetricTemplate.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(objectContextEClass, ObjectContext.class, "ObjectContext", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getObjectContext_Component(), theDeploymentPackage.getComponent(), null, "component", null, 0, 1, ObjectContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getObjectContext_Data(), theDataPackage.getData(), null, "data", null, 0, 1, ObjectContext.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(metricTypeModelEClass, MetricTypeModel.class, "MetricTypeModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMetricTypeModel_MetricContexts(), this.getMetricContext(), null, "metricContexts", null, 0, -1, MetricTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricTypeModel_AttributeContexts(), this.getAttributeContext(), null, "attributeContexts", null, 0, -1, MetricTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricTypeModel_ObjectContexts(), this.getObjectContext(), null, "objectContexts", null, 0, -1, MetricTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricTypeModel_Metrics(), this.getMetric(), null, "metrics", null, 0, -1, MetricTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMetricTypeModel_Windows(), this.getWindow(), null, "windows", null, 0, -1, MetricTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMetricTypeModel_Schedules(), this.getSchedule(), null, "schedules", null, 0, -1, MetricTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMetricTypeModel_Sensors(), this.getSensor(), null, "sensors", null, 0, -1, MetricTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMetricTypeModel_Functions(), this.getFunction(), null, "functions", null, 0, -1, MetricTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getMetricTypeModel_Templates(), this.getMetricTemplate(), null, "templates", null, 0, -1, MetricTypeModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(metricInstanceModelEClass, MetricInstanceModel.class, "MetricInstanceModel", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getMetricInstanceModel_MetricInstances(), this.getMetricInstance(), null, "metricInstances", null, 0, -1, MetricInstanceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMetricInstanceModel_Bindings(), this.getMetricObjectBinding(), null, "bindings", null, 0, -1, MetricInstanceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, !IS_ORDERED);
		initEReference(getMetricInstanceModel_Type(), this.getMetricTypeModel(), null, "type", null, 1, 1, MetricInstanceModel.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Initialize enums and add enum literals
		initEEnum(windowSizeTypeEEnum, WindowSizeType.class, "WindowSizeType");
		addEEnumLiteral(windowSizeTypeEEnum, WindowSizeType.MEASUREMENTS_ONLY);
		addEEnumLiteral(windowSizeTypeEEnum, WindowSizeType.TIME_ONLY);
		addEEnumLiteral(windowSizeTypeEEnum, WindowSizeType.FIRST_MATCH);
		addEEnumLiteral(windowSizeTypeEEnum, WindowSizeType.BOTH_MATCH);

		initEEnum(windowTypeEEnum, WindowType.class, "WindowType");
		addEEnumLiteral(windowTypeEEnum, WindowType.FIXED);
		addEEnumLiteral(windowTypeEEnum, WindowType.SLIDING);

		initEEnum(groupingTypeEEnum, GroupingType.class, "GroupingType");
		addEEnumLiteral(groupingTypeEEnum, GroupingType.PER_INSTANCE);
		addEEnumLiteral(groupingTypeEEnum, GroupingType.PER_HOST);
		addEEnumLiteral(groupingTypeEEnum, GroupingType.PER_ZONE);
		addEEnumLiteral(groupingTypeEEnum, GroupingType.PER_REGION);
		addEEnumLiteral(groupingTypeEEnum, GroupingType.PER_CLOUD);
		addEEnumLiteral(groupingTypeEEnum, GroupingType.GLOBAL);

		// Create resource
		createResource(eNS_URI);

		// Create annotations
		// http://www.eclipse.org/emf/2002/Ecore
		createEcoreAnnotations();
		// http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot
		createPivotAnnotations();
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createEcoreAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore";	
		addAnnotation
		  (this, 
		   source, 
		   new String[] {
			 "invocationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "settingDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
			 "validationDelegates", "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"
		   });	
		addAnnotation
		  (metricInstanceEClass, 
		   source, 
		   new String[] {
			 "constraints", "composite_metric_instance_to_components component_instances_metric_map_component_metrics"
		   });	
		addAnnotation
		  (compositeMetricEClass, 
		   source, 
		   new String[] {
			 "constraints", "composite_metric_contains_recurs"
		   });	
		addAnnotation
		  (metricVariableEClass, 
		   source, 
		   new String[] {
			 "constraints", "metric_variable_current_config_candidates metric_variable_contains_recurs metric_variable_formula_components"
		   });	
		addAnnotation
		  (scheduleEClass, 
		   source, 
		   new String[] {
			 "constraints", "schedule_correct_interval"
		   });	
		addAnnotation
		  (sensorEClass, 
		   source, 
		   new String[] {
			 "constraints", "sensor_correct_config"
		   });	
		addAnnotation
		  (windowEClass, 
		   source, 
		   new String[] {
			 "constraints", "window_positive_params window_right_params_exist"
		   });	
		addAnnotation
		  (compositeMetricContextEClass, 
		   source, 
		   new String[] {
			 "constraints", "composite_metric_context_correct_metric_type metrics_in_composing_contexts_are_component_metrics"
		   });	
		addAnnotation
		  (rawMetricContextEClass, 
		   source, 
		   new String[] {
			 "constraints", "raw_metric_context_correct_sensor raw_metric_context_correct_metric_type"
		   });	
		addAnnotation
		  (objectContextEClass, 
		   source, 
		   new String[] {
			 "constraints", "object_context_either_data_component"
		   });
	}

	/**
	 * Initializes the annotations for <b>http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot</b>.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void createPivotAnnotations() {
		String source = "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot";	
		addAnnotation
		  (metricInstanceEClass, 
		   source, 
		   new String[] {
			 "composite_metric_instance_to_components", "Tuple {\n\tmessage : String = \'Instance: \' + self.name + \' of composite metric:\' + self.metricContext.metric.name + \'should have the same execution model as the one of all of its component metric instances\',\n\tstatus : Boolean = \n\t\t\t\tasError(metricContext.metric.oclIsTypeOf(CompositeMetric)\n\t\t\t\t\timplies self.composingMetricInstances->forAll(p | p.objectBinding.executionModel = self.objectBinding.executionModel))\n}.status",
			 "component_instances_metric_map_component_metrics", "Tuple {\n\tmessage : String = \'CompositeMetricInstance: \' + self.name + \' should have component metric instances which map to the component metrics referenced by the composite instance\\\'s metric\',\n\tstatus : Boolean = \n\t\t\t\tasError(metricContext.metric.oclIsTypeOf(CompositeMetric)\n\t\t\t\t\timplies self.metricContext.metric.oclAsType(CompositeMetric).componentMetrics\n\t\t\t\t\t->forAll(p | self.composingMetricInstances\n\t\t\t\t\t\t->exists(q | q.metricContext.metric = p))\n\t\t\t\t\tand\n\t\t\t\t\tself.composingMetricInstances->forAll(inst | self.metricContext.metric.oclAsType(CompositeMetric).componentMetrics->exists(comp | comp = inst.metricContext.metric)))\n}.status"
		   });	
		addAnnotation
		  (compositeMetricEClass, 
		   source, 
		   new String[] {
			 "composite_metric_contains_recurs", "Tuple {\n\tmessage : String = \'CompositeMetric: \' + self.name + \' should not contain itself recursively\'\n\t\t\t,\n\tstatus : Boolean = asError(not self.containsMetric(self))\n}.status"
		   });	
		addAnnotation
		  (getCompositeMetric__ContainsMetric__Metric(), 
		   source, 
		   new String[] {
			 "body", "\n\t\t\t\t\tcomponentMetrics->includes(metric) or componentMetrics->exists(m | \n\t\t\t\t\t\t(m.oclIsTypeOf(CompositeMetric) and m.oclAsType(CompositeMetric).containsMetric(metric)) or\n\t\t\t\t\t\t(m.oclIsTypeOf(MetricVariable) and m.oclAsType(MetricVariable).containsMetric(metric))\n\t\t\t\t\t)"
		   });	
		addAnnotation
		  (metricVariableEClass, 
		   source, 
		   new String[] {
			 "metric_variable_current_config_candidates", "Tuple {\n\tmessage : String = \'MetricVariable: \' + self.name + \' cannot span both the current configuration and the node candidates\',\n\tstatus : Boolean = \n \t\t\t \tasError(currentConfiguration = false or onNodeCandidates = false)\n}.status",
			 "metric_variable_contains_recurs", "Tuple {\n\tmessage : String = \'MetricVariable: \' + self.name + \' should not contain recursively itself\'\n\t\t\t,\n\tstatus : Boolean = asError(not self.containsMetric(self))\n}.status"
		   });	
		addAnnotation
		  (getMetricVariable__ContainsMetric__Metric(), 
		   source, 
		   new String[] {
			 "body", "\n\t\t\t\t\tcomponentMetrics->includes(metric) or componentMetrics->exists(m | \n\t\t\t\t\t\t(m.oclIsTypeOf(MetricVariable) and m.oclAsType(MetricVariable).containsMetric(metric)) or\n\t\t\t\t\t\t(m.oclIsTypeOf(CompositeMetric) and m.oclAsType(CompositeMetric).containsMetric(metric))\n\t\t\t\t\t)"
		   });	
		addAnnotation
		  (scheduleEClass, 
		   source, 
		   new String[] {
			 "schedule_correct_interval", "Tuple {\n\tmessage : String = \'Schedule: \' + self.name + \' should have a correct interval\',\n\tstatus : Boolean = \n\t\t\t\tasError(interval >0)\n}.status"
		   });	
		addAnnotation
		  (sensorEClass, 
		   source, 
		   new String[] {
			 "sensor_correct_config", "Tuple {\n\tmessage : String = \'Sensor: \' + self.name + \' should either have a configuration string, if it belongs to the platform or be mapped to a Configuration object\',\n\tstatus : Boolean = \n\t\t\t asError((configuration <> null and self.configurations->size() = 0) xor (configuration = null and self.configurations->size() > 0))\n}.status"
		   });	
		addAnnotation
		  (windowEClass, 
		   source, 
		   new String[] {
			 "window_positive_params", "Tuple {\n\tmessage : String = \'Window:\' + self.name + \' has a negative value for the measurementSize and timeSize attributes\',\n\tstatus : Boolean = \n\t\t\t\tasError((measurementSize >= 0) and (timeSize >= 0))\n}.status",
			 "window_right_params_exist", "Tuple {\n\tmessage : String = \'Window: \' + self.name + \' has wrong value combinations for its attributes and properties. If sizeType is MEASUREMENTS_ONLY, then the measurementSize should be positive and all other values zero or null. If sizeType is TIME_ONLY, then both the unit should not be null and the timeSize should be positive, while the measurementSize should be zero. For the other values of sizeType, the values of all remaining attributes and units should be provided\',\n\tstatus : Boolean = \n\t\t\t\tasError((self.sizeType\n\t\t\t\t\t= WindowSizeType::MEASUREMENTS_ONLY implies (timeUnit = null and timeSize = 0 and measurementSize > 0)) and\n\t\t\t\t\t(self.sizeType = WindowSizeType::TIME_ONLY implies (timeUnit <> null and timeSize > 0 and measurementSize = 0)) and\n\t\t\t\t\t((self.sizeType = WindowSizeType::FIRST_MATCH or self.sizeType = WindowSizeType::BOTH_MATCH) implies (timeSize > 0\n\t\t\t\t\tand timeUnit <> null and measurementSize > 0)))\n}.status"
		   });	
		addAnnotation
		  (compositeMetricContextEClass, 
		   source, 
		   new String[] {
			 "composite_metric_context_correct_metric_type", "Tuple {\n\tmessage : String = \'In CompositeMetricContext: \' + self.name + \' the metric: \' + self.metric.name + \' should be composite but it isn\\\'t\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.metric.oclIsKindOf(CompositeMetric))\n}.status"
		   });	
		addAnnotation
		  (rawMetricContextEClass, 
		   source, 
		   new String[] {
			 "raw_metric_context_correct_sensor", "Tuple {\n\tmessage : String = \'RawMetricContext: \' + self.name + \' has a sensor which is not included in the sensors of the attribute measured by the metric of this context\',\n\tstatus : Boolean = \n\t\t\t\tasError(let sensors: Sensor[*] = self.metric.metricTemplate.attribute.sensors in\n\t\t\t\t\tsensors->size() > 0 implies sensors->includes(self.sensor))\n}.status",
			 "raw_metric_context_correct_metric_type", "Tuple {\n\tmessage : String = \'In RawMetricContext: \' + self.name + \' the metric: \' + self.metric.name + \' should be raw but it isn\\\'t\',\n\tstatus : Boolean = \n\t\t\t\t\tasError(self.metric.oclIsKindOf(RawMetric))\n}.status"
		   });	
		addAnnotation
		  (objectContextEClass, 
		   source, 
		   new String[] {
			 "object_context_either_data_component", "Tuple {\n\tmessage : String = \'In ObjectContext: \' + self.name + \' either the component or data should be supplied\',\n\tstatus : Boolean = \n\t\t\t\tasError(component <> null or data <> null)\n}.status"
		   });
	}

} //MetricPackageImpl
