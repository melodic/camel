/**
 */
package camel.metric.impl;

import camel.core.MeasurableAttribute;

import camel.core.impl.FeatureImpl;

import camel.metric.MetricPackage;
import camel.metric.MetricTemplate;

import camel.type.ValueType;

import camel.unit.Unit;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Template</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.MetricTemplateImpl#getValueType <em>Value Type</em>}</li>
 *   <li>{@link camel.metric.impl.MetricTemplateImpl#getValueDirection <em>Value Direction</em>}</li>
 *   <li>{@link camel.metric.impl.MetricTemplateImpl#getUnit <em>Unit</em>}</li>
 *   <li>{@link camel.metric.impl.MetricTemplateImpl#getAttribute <em>Attribute</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetricTemplateImpl extends FeatureImpl implements MetricTemplate {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricTemplateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.METRIC_TEMPLATE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ValueType getValueType() {
		return (ValueType)eGet(MetricPackage.Literals.METRIC_TEMPLATE__VALUE_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueType(ValueType newValueType) {
		eSet(MetricPackage.Literals.METRIC_TEMPLATE__VALUE_TYPE, newValueType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public short getValueDirection() {
		return (Short)eGet(MetricPackage.Literals.METRIC_TEMPLATE__VALUE_DIRECTION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setValueDirection(short newValueDirection) {
		eSet(MetricPackage.Literals.METRIC_TEMPLATE__VALUE_DIRECTION, newValueDirection);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Unit getUnit() {
		return (Unit)eGet(MetricPackage.Literals.METRIC_TEMPLATE__UNIT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setUnit(Unit newUnit) {
		eSet(MetricPackage.Literals.METRIC_TEMPLATE__UNIT, newUnit);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MeasurableAttribute getAttribute() {
		return (MeasurableAttribute)eGet(MetricPackage.Literals.METRIC_TEMPLATE__ATTRIBUTE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAttribute(MeasurableAttribute newAttribute) {
		eSet(MetricPackage.Literals.METRIC_TEMPLATE__ATTRIBUTE, newAttribute);
	}

} //MetricTemplateImpl
