/**
 */
package camel.metric.impl;

import camel.metric.AttributeContext;
import camel.metric.Function;
import camel.metric.Metric;
import camel.metric.MetricContext;
import camel.metric.MetricPackage;
import camel.metric.MetricTemplate;
import camel.metric.MetricTypeModel;
import camel.metric.ObjectContext;
import camel.metric.Schedule;
import camel.metric.Sensor;
import camel.metric.Window;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Type Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.MetricTypeModelImpl#getMetricContexts <em>Metric Contexts</em>}</li>
 *   <li>{@link camel.metric.impl.MetricTypeModelImpl#getAttributeContexts <em>Attribute Contexts</em>}</li>
 *   <li>{@link camel.metric.impl.MetricTypeModelImpl#getObjectContexts <em>Object Contexts</em>}</li>
 *   <li>{@link camel.metric.impl.MetricTypeModelImpl#getMetrics <em>Metrics</em>}</li>
 *   <li>{@link camel.metric.impl.MetricTypeModelImpl#getWindows <em>Windows</em>}</li>
 *   <li>{@link camel.metric.impl.MetricTypeModelImpl#getSchedules <em>Schedules</em>}</li>
 *   <li>{@link camel.metric.impl.MetricTypeModelImpl#getSensors <em>Sensors</em>}</li>
 *   <li>{@link camel.metric.impl.MetricTypeModelImpl#getFunctions <em>Functions</em>}</li>
 *   <li>{@link camel.metric.impl.MetricTypeModelImpl#getTemplates <em>Templates</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetricTypeModelImpl extends MetricModelImpl implements MetricTypeModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricTypeModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.METRIC_TYPE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MetricContext> getMetricContexts() {
		return (EList<MetricContext>)eGet(MetricPackage.Literals.METRIC_TYPE_MODEL__METRIC_CONTEXTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<AttributeContext> getAttributeContexts() {
		return (EList<AttributeContext>)eGet(MetricPackage.Literals.METRIC_TYPE_MODEL__ATTRIBUTE_CONTEXTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ObjectContext> getObjectContexts() {
		return (EList<ObjectContext>)eGet(MetricPackage.Literals.METRIC_TYPE_MODEL__OBJECT_CONTEXTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Metric> getMetrics() {
		return (EList<Metric>)eGet(MetricPackage.Literals.METRIC_TYPE_MODEL__METRICS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Window> getWindows() {
		return (EList<Window>)eGet(MetricPackage.Literals.METRIC_TYPE_MODEL__WINDOWS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Schedule> getSchedules() {
		return (EList<Schedule>)eGet(MetricPackage.Literals.METRIC_TYPE_MODEL__SCHEDULES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Sensor> getSensors() {
		return (EList<Sensor>)eGet(MetricPackage.Literals.METRIC_TYPE_MODEL__SENSORS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Function> getFunctions() {
		return (EList<Function>)eGet(MetricPackage.Literals.METRIC_TYPE_MODEL__FUNCTIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MetricTemplate> getTemplates() {
		return (EList<MetricTemplate>)eGet(MetricPackage.Literals.METRIC_TYPE_MODEL__TEMPLATES, true);
	}

} //MetricTypeModelImpl
