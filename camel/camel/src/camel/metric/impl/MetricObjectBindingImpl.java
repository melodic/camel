/**
 */
package camel.metric.impl;

import camel.core.impl.FeatureImpl;

import camel.deployment.ComponentInstance;

import camel.execution.ExecutionModel;

import camel.metric.MetricObjectBinding;
import camel.metric.MetricPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object Binding</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.MetricObjectBindingImpl#getExecutionModel <em>Execution Model</em>}</li>
 *   <li>{@link camel.metric.impl.MetricObjectBindingImpl#getComponentInstance <em>Component Instance</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetricObjectBindingImpl extends FeatureImpl implements MetricObjectBinding {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricObjectBindingImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.METRIC_OBJECT_BINDING;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ExecutionModel getExecutionModel() {
		return (ExecutionModel)eGet(MetricPackage.Literals.METRIC_OBJECT_BINDING__EXECUTION_MODEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setExecutionModel(ExecutionModel newExecutionModel) {
		eSet(MetricPackage.Literals.METRIC_OBJECT_BINDING__EXECUTION_MODEL, newExecutionModel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ComponentInstance getComponentInstance() {
		return (ComponentInstance)eGet(MetricPackage.Literals.METRIC_OBJECT_BINDING__COMPONENT_INSTANCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponentInstance(ComponentInstance newComponentInstance) {
		eSet(MetricPackage.Literals.METRIC_OBJECT_BINDING__COMPONENT_INSTANCE, newComponentInstance);
	}

} //MetricObjectBindingImpl
