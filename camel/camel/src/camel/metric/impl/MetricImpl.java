/**
 */
package camel.metric.impl;

import camel.core.impl.FeatureImpl;

import camel.metric.Metric;
import camel.metric.MetricPackage;
import camel.metric.MetricTemplate;

import java.lang.reflect.InvocationTargetException;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Metric</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.MetricImpl#getMetricTemplate <em>Metric Template</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class MetricImpl extends FeatureImpl implements Metric {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.METRIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricTemplate getMetricTemplate() {
		return (MetricTemplate)eGet(MetricPackage.Literals.METRIC__METRIC_TEMPLATE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetricTemplate(MetricTemplate newMetricTemplate) {
		eSet(MetricPackage.Literals.METRIC__METRIC_TEMPLATE, newMetricTemplate);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean checkRecursiveness(final Metric mt1, final Metric mt2) {
		System.out.println("Checking recursiveness for Metric: " + mt1.getName());
						camel.metric.CompositeMetric m1 = (camel.metric.CompositeMetric)mt1;
						for (camel.metric.Metric mt: m1.getComponentMetrics()){
								if (mt.getName().equals(mt2.getName())) return Boolean.TRUE;
								if (mt instanceof camel.metric.CompositeMetric && checkRecursiveness(mt,mt2)) return Boolean.TRUE;
						}
						return Boolean.FALSE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MetricPackage.METRIC___CHECK_RECURSIVENESS__METRIC_METRIC:
				return checkRecursiveness((Metric)arguments.get(0), (Metric)arguments.get(1));
		}
		return super.eInvoke(operationID, arguments);
	}

} //MetricImpl
