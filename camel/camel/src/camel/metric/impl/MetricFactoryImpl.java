/**
 */
package camel.metric.impl;

import camel.metric.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class MetricFactoryImpl extends EFactoryImpl implements MetricFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static MetricFactory init() {
		try {
			MetricFactory theMetricFactory = (MetricFactory)EPackage.Registry.INSTANCE.getEFactory(MetricPackage.eNS_URI);
			if (theMetricFactory != null) {
				return theMetricFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new MetricFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case MetricPackage.METRIC_INSTANCE: return (EObject)createMetricInstance();
			case MetricPackage.COMPOSITE_METRIC: return (EObject)createCompositeMetric();
			case MetricPackage.RAW_METRIC: return (EObject)createRawMetric();
			case MetricPackage.METRIC_VARIABLE: return (EObject)createMetricVariable();
			case MetricPackage.METRIC_OBJECT_BINDING: return (EObject)createMetricObjectBinding();
			case MetricPackage.SCHEDULE: return (EObject)createSchedule();
			case MetricPackage.SENSOR: return (EObject)createSensor();
			case MetricPackage.WINDOW: return (EObject)createWindow();
			case MetricPackage.COMPOSITE_METRIC_CONTEXT: return (EObject)createCompositeMetricContext();
			case MetricPackage.RAW_METRIC_CONTEXT: return (EObject)createRawMetricContext();
			case MetricPackage.ATTRIBUTE_CONTEXT: return (EObject)createAttributeContext();
			case MetricPackage.FUNCTION: return (EObject)createFunction();
			case MetricPackage.METRIC_TEMPLATE: return (EObject)createMetricTemplate();
			case MetricPackage.OBJECT_CONTEXT: return (EObject)createObjectContext();
			case MetricPackage.METRIC_TYPE_MODEL: return (EObject)createMetricTypeModel();
			case MetricPackage.METRIC_INSTANCE_MODEL: return (EObject)createMetricInstanceModel();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case MetricPackage.WINDOW_SIZE_TYPE:
				return createWindowSizeTypeFromString(eDataType, initialValue);
			case MetricPackage.WINDOW_TYPE:
				return createWindowTypeFromString(eDataType, initialValue);
			case MetricPackage.GROUPING_TYPE:
				return createGroupingTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case MetricPackage.WINDOW_SIZE_TYPE:
				return convertWindowSizeTypeToString(eDataType, instanceValue);
			case MetricPackage.WINDOW_TYPE:
				return convertWindowTypeToString(eDataType, instanceValue);
			case MetricPackage.GROUPING_TYPE:
				return convertGroupingTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricInstance createMetricInstance() {
		MetricInstanceImpl metricInstance = new MetricInstanceImpl();
		return metricInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeMetric createCompositeMetric() {
		CompositeMetricImpl compositeMetric = new CompositeMetricImpl();
		return compositeMetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RawMetric createRawMetric() {
		RawMetricImpl rawMetric = new RawMetricImpl();
		return rawMetric;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricVariable createMetricVariable() {
		MetricVariableImpl metricVariable = new MetricVariableImpl();
		return metricVariable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricObjectBinding createMetricObjectBinding() {
		MetricObjectBindingImpl metricObjectBinding = new MetricObjectBindingImpl();
		return metricObjectBinding;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Schedule createSchedule() {
		ScheduleImpl schedule = new ScheduleImpl();
		return schedule;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Sensor createSensor() {
		SensorImpl sensor = new SensorImpl();
		return sensor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Window createWindow() {
		WindowImpl window = new WindowImpl();
		return window;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeMetricContext createCompositeMetricContext() {
		CompositeMetricContextImpl compositeMetricContext = new CompositeMetricContextImpl();
		return compositeMetricContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RawMetricContext createRawMetricContext() {
		RawMetricContextImpl rawMetricContext = new RawMetricContextImpl();
		return rawMetricContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AttributeContext createAttributeContext() {
		AttributeContextImpl attributeContext = new AttributeContextImpl();
		return attributeContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Function createFunction() {
		FunctionImpl function = new FunctionImpl();
		return function;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricTemplate createMetricTemplate() {
		MetricTemplateImpl metricTemplate = new MetricTemplateImpl();
		return metricTemplate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ObjectContext createObjectContext() {
		ObjectContextImpl objectContext = new ObjectContextImpl();
		return objectContext;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricTypeModel createMetricTypeModel() {
		MetricTypeModelImpl metricTypeModel = new MetricTypeModelImpl();
		return metricTypeModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricInstanceModel createMetricInstanceModel() {
		MetricInstanceModelImpl metricInstanceModel = new MetricInstanceModelImpl();
		return metricInstanceModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WindowSizeType createWindowSizeTypeFromString(EDataType eDataType, String initialValue) {
		WindowSizeType result = WindowSizeType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertWindowSizeTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WindowType createWindowTypeFromString(EDataType eDataType, String initialValue) {
		WindowType result = WindowType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertWindowTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupingType createGroupingTypeFromString(EDataType eDataType, String initialValue) {
		GroupingType result = GroupingType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertGroupingTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricPackage getMetricPackage() {
		return (MetricPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static MetricPackage getPackage() {
		return MetricPackage.eINSTANCE;
	}

} //MetricFactoryImpl
