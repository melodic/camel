/**
 */
package camel.metric.impl;

import camel.metric.CompositeMetric;
import camel.metric.Metric;
import camel.metric.MetricPackage;
import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Metric</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.CompositeMetricImpl#getFormula <em>Formula</em>}</li>
 *   <li>{@link camel.metric.impl.CompositeMetricImpl#getComponentMetrics <em>Component Metrics</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompositeMetricImpl extends MetricImpl implements CompositeMetric {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeMetricImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.COMPOSITE_METRIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormula() {
		return (String)eGet(MetricPackage.Literals.COMPOSITE_METRIC__FORMULA, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormula(String newFormula) {
		eSet(MetricPackage.Literals.COMPOSITE_METRIC__FORMULA, newFormula);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Metric> getComponentMetrics() {
		return (EList<Metric>)eGet(MetricPackage.Literals.COMPOSITE_METRIC__COMPONENT_METRICS, true);
	}

	/**
	 * The cached invocation delegate for the '{@link #containsMetric(camel.metric.Metric) <em>Contains Metric</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #containsMetric(camel.metric.Metric)
	 * @generated
	 * @ordered
	 */
	protected static final EOperation.Internal.InvocationDelegate CONTAINS_METRIC_METRIC__EINVOCATION_DELEGATE = ((EOperation.Internal)MetricPackage.Literals.COMPOSITE_METRIC___CONTAINS_METRIC__METRIC).getInvocationDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean containsMetric(Metric metric) {
		try {
			return (Boolean)CONTAINS_METRIC_METRIC__EINVOCATION_DELEGATE.dynamicInvoke(this, new BasicEList.UnmodifiableEList<Object>(1, new Object[]{metric}));
		}
		catch (InvocationTargetException ite) {
			throw new WrappedException(ite);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case MetricPackage.COMPOSITE_METRIC___CONTAINS_METRIC__METRIC:
				return containsMetric((Metric)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //CompositeMetricImpl
