/**
 */
package camel.metric.impl;

import camel.metric.CompositeMetricContext;
import camel.metric.GroupingType;
import camel.metric.MetricContext;
import camel.metric.MetricPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Metric Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.CompositeMetricContextImpl#getGroupingType <em>Grouping Type</em>}</li>
 *   <li>{@link camel.metric.impl.CompositeMetricContextImpl#getComposingMetricContexts <em>Composing Metric Contexts</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompositeMetricContextImpl extends MetricContextImpl implements CompositeMetricContext {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeMetricContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.COMPOSITE_METRIC_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public GroupingType getGroupingType() {
		return (GroupingType)eGet(MetricPackage.Literals.COMPOSITE_METRIC_CONTEXT__GROUPING_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setGroupingType(GroupingType newGroupingType) {
		eSet(MetricPackage.Literals.COMPOSITE_METRIC_CONTEXT__GROUPING_TYPE, newGroupingType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MetricContext> getComposingMetricContexts() {
		return (EList<MetricContext>)eGet(MetricPackage.Literals.COMPOSITE_METRIC_CONTEXT__COMPOSING_METRIC_CONTEXTS, true);
	}

} //CompositeMetricContextImpl
