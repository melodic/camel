/**
 */
package camel.metric.impl;

import camel.core.impl.FeatureImpl;

import camel.data.Data;

import camel.deployment.Component;

import camel.metric.MetricPackage;
import camel.metric.ObjectContext;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Object Context</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.ObjectContextImpl#getComponent <em>Component</em>}</li>
 *   <li>{@link camel.metric.impl.ObjectContextImpl#getData <em>Data</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ObjectContextImpl extends FeatureImpl implements ObjectContext {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ObjectContextImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.OBJECT_CONTEXT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Component getComponent() {
		return (Component)eGet(MetricPackage.Literals.OBJECT_CONTEXT__COMPONENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setComponent(Component newComponent) {
		eSet(MetricPackage.Literals.OBJECT_CONTEXT__COMPONENT, newComponent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Data getData() {
		return (Data)eGet(MetricPackage.Literals.OBJECT_CONTEXT__DATA, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setData(Data newData) {
		eSet(MetricPackage.Literals.OBJECT_CONTEXT__DATA, newData);
	}

} //ObjectContextImpl
