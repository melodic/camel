/**
 */
package camel.metric.impl;

import camel.core.impl.FeatureImpl;

import camel.metric.MetricPackage;
import camel.metric.Window;
import camel.metric.WindowSizeType;
import camel.metric.WindowType;

import camel.unit.Unit;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Window</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.WindowImpl#getTimeUnit <em>Time Unit</em>}</li>
 *   <li>{@link camel.metric.impl.WindowImpl#getWindowType <em>Window Type</em>}</li>
 *   <li>{@link camel.metric.impl.WindowImpl#getSizeType <em>Size Type</em>}</li>
 *   <li>{@link camel.metric.impl.WindowImpl#getMeasurementSize <em>Measurement Size</em>}</li>
 *   <li>{@link camel.metric.impl.WindowImpl#getTimeSize <em>Time Size</em>}</li>
 * </ul>
 *
 * @generated
 */
public class WindowImpl extends FeatureImpl implements Window {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected WindowImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.WINDOW;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Unit getTimeUnit() {
		return (Unit)eGet(MetricPackage.Literals.WINDOW__TIME_UNIT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeUnit(Unit newTimeUnit) {
		eSet(MetricPackage.Literals.WINDOW__TIME_UNIT, newTimeUnit);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WindowType getWindowType() {
		return (WindowType)eGet(MetricPackage.Literals.WINDOW__WINDOW_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWindowType(WindowType newWindowType) {
		eSet(MetricPackage.Literals.WINDOW__WINDOW_TYPE, newWindowType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public WindowSizeType getSizeType() {
		return (WindowSizeType)eGet(MetricPackage.Literals.WINDOW__SIZE_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSizeType(WindowSizeType newSizeType) {
		eSet(MetricPackage.Literals.WINDOW__SIZE_TYPE, newSizeType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getMeasurementSize() {
		return (Long)eGet(MetricPackage.Literals.WINDOW__MEASUREMENT_SIZE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMeasurementSize(long newMeasurementSize) {
		eSet(MetricPackage.Literals.WINDOW__MEASUREMENT_SIZE, newMeasurementSize);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public long getTimeSize() {
		return (Long)eGet(MetricPackage.Literals.WINDOW__TIME_SIZE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setTimeSize(long newTimeSize) {
		eSet(MetricPackage.Literals.WINDOW__TIME_SIZE, newTimeSize);
	}

} //WindowImpl
