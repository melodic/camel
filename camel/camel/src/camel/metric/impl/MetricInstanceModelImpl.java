/**
 */
package camel.metric.impl;

import camel.metric.MetricInstance;
import camel.metric.MetricInstanceModel;
import camel.metric.MetricObjectBinding;
import camel.metric.MetricPackage;
import camel.metric.MetricTypeModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instance Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.MetricInstanceModelImpl#getMetricInstances <em>Metric Instances</em>}</li>
 *   <li>{@link camel.metric.impl.MetricInstanceModelImpl#getBindings <em>Bindings</em>}</li>
 *   <li>{@link camel.metric.impl.MetricInstanceModelImpl#getType <em>Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetricInstanceModelImpl extends MetricModelImpl implements MetricInstanceModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricInstanceModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.METRIC_INSTANCE_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MetricInstance> getMetricInstances() {
		return (EList<MetricInstance>)eGet(MetricPackage.Literals.METRIC_INSTANCE_MODEL__METRIC_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MetricObjectBinding> getBindings() {
		return (EList<MetricObjectBinding>)eGet(MetricPackage.Literals.METRIC_INSTANCE_MODEL__BINDINGS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricTypeModel getType() {
		return (MetricTypeModel)eGet(MetricPackage.Literals.METRIC_INSTANCE_MODEL__TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setType(MetricTypeModel newType) {
		eSet(MetricPackage.Literals.METRIC_INSTANCE_MODEL__TYPE, newType);
	}

} //MetricInstanceModelImpl
