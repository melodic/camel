/**
 */
package camel.metric.impl;

import camel.metric.MetricPackage;
import camel.metric.RawMetric;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Raw Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class RawMetricImpl extends MetricImpl implements RawMetric {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RawMetricImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.RAW_METRIC;
	}

} //RawMetricImpl
