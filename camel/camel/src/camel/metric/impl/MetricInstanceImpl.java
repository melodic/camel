/**
 */
package camel.metric.impl;

import camel.core.impl.FeatureImpl;

import camel.metric.MetricContext;
import camel.metric.MetricInstance;
import camel.metric.MetricObjectBinding;
import camel.metric.MetricPackage;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Instance</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.MetricInstanceImpl#getObjectBinding <em>Object Binding</em>}</li>
 *   <li>{@link camel.metric.impl.MetricInstanceImpl#getMetricContext <em>Metric Context</em>}</li>
 *   <li>{@link camel.metric.impl.MetricInstanceImpl#getComposingMetricInstances <em>Composing Metric Instances</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MetricInstanceImpl extends FeatureImpl implements MetricInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.METRIC_INSTANCE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricObjectBinding getObjectBinding() {
		return (MetricObjectBinding)eGet(MetricPackage.Literals.METRIC_INSTANCE__OBJECT_BINDING, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setObjectBinding(MetricObjectBinding newObjectBinding) {
		eSet(MetricPackage.Literals.METRIC_INSTANCE__OBJECT_BINDING, newObjectBinding);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricContext getMetricContext() {
		return (MetricContext)eGet(MetricPackage.Literals.METRIC_INSTANCE__METRIC_CONTEXT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetricContext(MetricContext newMetricContext) {
		eSet(MetricPackage.Literals.METRIC_INSTANCE__METRIC_CONTEXT, newMetricContext);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MetricInstance> getComposingMetricInstances() {
		return (EList<MetricInstance>)eGet(MetricPackage.Literals.METRIC_INSTANCE__COMPOSING_METRIC_INSTANCES, true);
	}

} //MetricInstanceImpl
