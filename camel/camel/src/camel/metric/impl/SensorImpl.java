/**
 */
package camel.metric.impl;

import camel.deployment.impl.SoftwareComponentImpl;

import camel.metric.MetricPackage;
import camel.metric.Sensor;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Sensor</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.impl.SensorImpl#getConfiguration <em>Configuration</em>}</li>
 *   <li>{@link camel.metric.impl.SensorImpl#isIsPush <em>Is Push</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SensorImpl extends SoftwareComponentImpl implements Sensor {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SensorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return MetricPackage.Literals.SENSOR;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getConfiguration() {
		return (String)eGet(MetricPackage.Literals.SENSOR__CONFIGURATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConfiguration(String newConfiguration) {
		eSet(MetricPackage.Literals.SENSOR__CONFIGURATION, newConfiguration);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIsPush() {
		return (Boolean)eGet(MetricPackage.Literals.SENSOR__IS_PUSH, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIsPush(boolean newIsPush) {
		eSet(MetricPackage.Literals.SENSOR__IS_PUSH, newIsPush);
	}

} //SensorImpl
