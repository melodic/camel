/**
 */
package camel.metric;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Metric</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.metric.CompositeMetric#getFormula <em>Formula</em>}</li>
 *   <li>{@link camel.metric.CompositeMetric#getComponentMetrics <em>Component Metrics</em>}</li>
 * </ul>
 *
 * @see camel.metric.MetricPackage#getCompositeMetric()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='composite_metric_contains_recurs'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot composite_metric_contains_recurs='Tuple {\n\tmessage : String = \'CompositeMetric: \' + self.name + \' should not contain itself recursively\'\n\t\t\t,\n\tstatus : Boolean = asError(not self.containsMetric(self))\n}.status'"
 * @generated
 */
public interface CompositeMetric extends Metric {
	/**
	 * Returns the value of the '<em><b>Formula</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formula</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formula</em>' attribute.
	 * @see #setFormula(String)
	 * @see camel.metric.MetricPackage#getCompositeMetric_Formula()
	 * @model required="true"
	 * @generated
	 */
	String getFormula();

	/**
	 * Sets the value of the '{@link camel.metric.CompositeMetric#getFormula <em>Formula</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formula</em>' attribute.
	 * @see #getFormula()
	 * @generated
	 */
	void setFormula(String value);

	/**
	 * Returns the value of the '<em><b>Component Metrics</b></em>' reference list.
	 * The list contents are of type {@link camel.metric.Metric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Metrics</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Metrics</em>' reference list.
	 * @see camel.metric.MetricPackage#getCompositeMetric_ComponentMetrics()
	 * @model
	 * @generated
	 */
	EList<Metric> getComponentMetrics();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" metricRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='\n\t\t\t\t\tcomponentMetrics-&gt;includes(metric) or componentMetrics-&gt;exists(m | \n\t\t\t\t\t\t(m.oclIsTypeOf(CompositeMetric) and m.oclAsType(CompositeMetric).containsMetric(metric)) or\n\t\t\t\t\t\t(m.oclIsTypeOf(MetricVariable) and m.oclAsType(MetricVariable).containsMetric(metric))\n\t\t\t\t\t)'"
	 * @generated
	 */
	boolean containsMetric(Metric metric);

} // CompositeMetric
