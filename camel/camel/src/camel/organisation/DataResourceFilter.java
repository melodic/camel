/**
 */
package camel.organisation;

import camel.data.Data;
import camel.data.DataInstance;

import camel.location.Location;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Data Resource Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.DataResourceFilter#getData <em>Data</em>}</li>
 *   <li>{@link camel.organisation.DataResourceFilter#getDataInstances <em>Data Instances</em>}</li>
 *   <li>{@link camel.organisation.DataResourceFilter#getLocations <em>Locations</em>}</li>
 *   <li>{@link camel.organisation.DataResourceFilter#isEveryData <em>Every Data</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getDataResourceFilter()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='data_resource_filter_validity'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot data_resource_filter_validity='Tuple {\n\tmessage : String = \'It is meaningless for DataResourceFilter: \' + self.name + \' to apply for any data resource and to specify that it holds for specific data resources\',\n\tstatus : Boolean = \n\t\t\t\tasError(everyData = true implies (data-&gt;size() = 0 and dataInstances-&gt;size() = 0))\n}.status'"
 * @generated
 */
public interface DataResourceFilter extends ResourceFilter {
	/**
	 * Returns the value of the '<em><b>Data</b></em>' reference list.
	 * The list contents are of type {@link camel.data.Data}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data</em>' reference list.
	 * @see camel.organisation.OrganisationPackage#getDataResourceFilter_Data()
	 * @model
	 * @generated
	 */
	EList<Data> getData();

	/**
	 * Returns the value of the '<em><b>Data Instances</b></em>' reference list.
	 * The list contents are of type {@link camel.data.DataInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Instances</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Instances</em>' reference list.
	 * @see camel.organisation.OrganisationPackage#getDataResourceFilter_DataInstances()
	 * @model
	 * @generated
	 */
	EList<DataInstance> getDataInstances();

	/**
	 * Returns the value of the '<em><b>Locations</b></em>' reference list.
	 * The list contents are of type {@link camel.location.Location}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locations</em>' reference list.
	 * @see camel.organisation.OrganisationPackage#getDataResourceFilter_Locations()
	 * @model
	 * @generated
	 */
	EList<Location> getLocations();

	/**
	 * Returns the value of the '<em><b>Every Data</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Every Data</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Every Data</em>' attribute.
	 * @see #setEveryData(boolean)
	 * @see camel.organisation.OrganisationPackage#getDataResourceFilter_EveryData()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isEveryData();

	/**
	 * Sets the value of the '{@link camel.organisation.DataResourceFilter#isEveryData <em>Every Data</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Every Data</em>' attribute.
	 * @see #isEveryData()
	 * @generated
	 */
	void setEveryData(boolean value);

} // DataResourceFilter
