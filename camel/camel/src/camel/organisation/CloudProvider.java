/**
 */
package camel.organisation;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Cloud Provider</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.CloudProvider#isPublic <em>Public</em>}</li>
 *   <li>{@link camel.organisation.CloudProvider#isSaaS <em>Saa S</em>}</li>
 *   <li>{@link camel.organisation.CloudProvider#isPaaS <em>Paa S</em>}</li>
 *   <li>{@link camel.organisation.CloudProvider#isIaaS <em>Iaa S</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getCloudProvider()
 * @model
 * @generated
 */
public interface CloudProvider extends Organisation {
	/**
	 * Returns the value of the '<em><b>Public</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Public</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Public</em>' attribute.
	 * @see #setPublic(boolean)
	 * @see camel.organisation.OrganisationPackage#getCloudProvider_Public()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isPublic();

	/**
	 * Sets the value of the '{@link camel.organisation.CloudProvider#isPublic <em>Public</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Public</em>' attribute.
	 * @see #isPublic()
	 * @generated
	 */
	void setPublic(boolean value);

	/**
	 * Returns the value of the '<em><b>Saa S</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Saa S</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Saa S</em>' attribute.
	 * @see #setSaaS(boolean)
	 * @see camel.organisation.OrganisationPackage#getCloudProvider_SaaS()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isSaaS();

	/**
	 * Sets the value of the '{@link camel.organisation.CloudProvider#isSaaS <em>Saa S</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Saa S</em>' attribute.
	 * @see #isSaaS()
	 * @generated
	 */
	void setSaaS(boolean value);

	/**
	 * Returns the value of the '<em><b>Paa S</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Paa S</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Paa S</em>' attribute.
	 * @see #setPaaS(boolean)
	 * @see camel.organisation.OrganisationPackage#getCloudProvider_PaaS()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isPaaS();

	/**
	 * Sets the value of the '{@link camel.organisation.CloudProvider#isPaaS <em>Paa S</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Paa S</em>' attribute.
	 * @see #isPaaS()
	 * @generated
	 */
	void setPaaS(boolean value);

	/**
	 * Returns the value of the '<em><b>Iaa S</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Iaa S</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Iaa S</em>' attribute.
	 * @see #setIaaS(boolean)
	 * @see camel.organisation.OrganisationPackage#getCloudProvider_IaaS()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isIaaS();

	/**
	 * Sets the value of the '{@link camel.organisation.CloudProvider#isIaaS <em>Iaa S</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Iaa S</em>' attribute.
	 * @see #isIaaS()
	 * @generated
	 */
	void setIaaS(boolean value);

} // CloudProvider
