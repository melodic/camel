/**
 */
package camel.organisation;

import camel.core.Feature;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role Assignment</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.RoleAssignment#getUser <em>User</em>}</li>
 *   <li>{@link camel.organisation.RoleAssignment#getRole <em>Role</em>}</li>
 *   <li>{@link camel.organisation.RoleAssignment#getUserGroup <em>User Group</em>}</li>
 *   <li>{@link camel.organisation.RoleAssignment#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link camel.organisation.RoleAssignment#getEndTime <em>End Time</em>}</li>
 *   <li>{@link camel.organisation.RoleAssignment#getAssignmentTime <em>Assignment Time</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getRoleAssignment()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='role_assignment_at_least_user_or_group'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot role_assignment_at_least_user_or_group='Tuple {\n\tmessage : String = \'There is no user or user group associated to RoleAssigment:\' + self.name,\n\tstatus : Boolean = \n\t\t\t\tasError(self.user &lt;&gt; null or self.userGroup &lt;&gt; null)\n}.status'"
 * @generated
 */
public interface RoleAssignment extends Feature {
	/**
	 * Returns the value of the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User</em>' reference.
	 * @see #setUser(User)
	 * @see camel.organisation.OrganisationPackage#getRoleAssignment_User()
	 * @model
	 * @generated
	 */
	User getUser();

	/**
	 * Sets the value of the '{@link camel.organisation.RoleAssignment#getUser <em>User</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User</em>' reference.
	 * @see #getUser()
	 * @generated
	 */
	void setUser(User value);

	/**
	 * Returns the value of the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' reference.
	 * @see #setRole(Role)
	 * @see camel.organisation.OrganisationPackage#getRoleAssignment_Role()
	 * @model required="true"
	 * @generated
	 */
	Role getRole();

	/**
	 * Sets the value of the '{@link camel.organisation.RoleAssignment#getRole <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' reference.
	 * @see #getRole()
	 * @generated
	 */
	void setRole(Role value);

	/**
	 * Returns the value of the '<em><b>User Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Group</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Group</em>' reference.
	 * @see #setUserGroup(UserGroup)
	 * @see camel.organisation.OrganisationPackage#getRoleAssignment_UserGroup()
	 * @model
	 * @generated
	 */
	UserGroup getUserGroup();

	/**
	 * Sets the value of the '{@link camel.organisation.RoleAssignment#getUserGroup <em>User Group</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>User Group</em>' reference.
	 * @see #getUserGroup()
	 * @generated
	 */
	void setUserGroup(UserGroup value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(Date)
	 * @see camel.organisation.OrganisationPackage#getRoleAssignment_StartTime()
	 * @model
	 * @generated
	 */
	Date getStartTime();

	/**
	 * Sets the value of the '{@link camel.organisation.RoleAssignment#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(Date value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(Date)
	 * @see camel.organisation.OrganisationPackage#getRoleAssignment_EndTime()
	 * @model
	 * @generated
	 */
	Date getEndTime();

	/**
	 * Sets the value of the '{@link camel.organisation.RoleAssignment#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(Date value);

	/**
	 * Returns the value of the '<em><b>Assignment Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Assignment Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Assignment Time</em>' attribute.
	 * @see #setAssignmentTime(Date)
	 * @see camel.organisation.OrganisationPackage#getRoleAssignment_AssignmentTime()
	 * @model required="true"
	 * @generated
	 */
	Date getAssignmentTime();

	/**
	 * Sets the value of the '{@link camel.organisation.RoleAssignment#getAssignmentTime <em>Assignment Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Assignment Time</em>' attribute.
	 * @see #getAssignmentTime()
	 * @generated
	 */
	void setAssignmentTime(Date value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" thisRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='System.out.println(\"CHECKING Assignment_Assigned_Before_Start: \" + this + \" \" + this.getStartTime() + \" \" + this.getEndTime() + \" \" + this.getAssignmentTime()); java.util.Date date1 = this.getStartTime(); java.util.Date date2 = this.getEndTime(); java.util.Date date = this.getAssignmentTime(); if (date == null) return Boolean.TRUE; else if (date1 != null){ if (date.equals(date1) || date.before(date1)) return Boolean.TRUE; else return Boolean.FALSE;} else if (date2 != null &amp;&amp; date.before(date2)) return Boolean.TRUE; return Boolean.FALSE;'"
	 * @generated
	 */
	boolean checkAssignedOnDates(RoleAssignment this_);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" thisRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='System.out.println(\"CHECKING Assignment_Start_Before_End: \" + this + \" \" + this.getStartTime() + \" \" + this.getEndTime() + \" \" + this.getAssignmentTime()); java.util.Date date1 = this.getStartTime(); java.util.Date date2 = this.getEndTime(); if (date1 == null || date2 == null || (date1 != null &amp;&amp; date2 != null &amp;&amp; date1.before(date2))) return Boolean.TRUE; return Boolean.FALSE;'"
	 * @generated
	 */
	boolean checkStartEndDates(RoleAssignment this_);

} // RoleAssignment
