/**
 */
package camel.organisation;

import camel.core.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.OrganisationModel#getOrganisation <em>Organisation</em>}</li>
 *   <li>{@link camel.organisation.OrganisationModel#getExternalIdentifiers <em>External Identifiers</em>}</li>
 *   <li>{@link camel.organisation.OrganisationModel#getUsers <em>Users</em>}</li>
 *   <li>{@link camel.organisation.OrganisationModel#getUserGroups <em>User Groups</em>}</li>
 *   <li>{@link camel.organisation.OrganisationModel#getRoles <em>Roles</em>}</li>
 *   <li>{@link camel.organisation.OrganisationModel#getRoleAssigments <em>Role Assigments</em>}</li>
 *   <li>{@link camel.organisation.OrganisationModel#getPermissions <em>Permissions</em>}</li>
 *   <li>{@link camel.organisation.OrganisationModel#getSecurityLevel <em>Security Level</em>}</li>
 *   <li>{@link camel.organisation.OrganisationModel#getResourceFilters <em>Resource Filters</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getOrganisationModel()
 * @model
 * @generated
 */
public interface OrganisationModel extends Model {
	/**
	 * Returns the value of the '<em><b>Organisation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Organisation</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Organisation</em>' containment reference.
	 * @see #setOrganisation(Organisation)
	 * @see camel.organisation.OrganisationPackage#getOrganisationModel_Organisation()
	 * @model containment="true"
	 * @generated
	 */
	Organisation getOrganisation();

	/**
	 * Sets the value of the '{@link camel.organisation.OrganisationModel#getOrganisation <em>Organisation</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Organisation</em>' containment reference.
	 * @see #getOrganisation()
	 * @generated
	 */
	void setOrganisation(Organisation value);

	/**
	 * Returns the value of the '<em><b>External Identifiers</b></em>' containment reference list.
	 * The list contents are of type {@link camel.organisation.ExternalIdentifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Identifiers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Identifiers</em>' containment reference list.
	 * @see camel.organisation.OrganisationPackage#getOrganisationModel_ExternalIdentifiers()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ExternalIdentifier> getExternalIdentifiers();

	/**
	 * Returns the value of the '<em><b>Users</b></em>' containment reference list.
	 * The list contents are of type {@link camel.organisation.User}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Users</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Users</em>' containment reference list.
	 * @see camel.organisation.OrganisationPackage#getOrganisationModel_Users()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<User> getUsers();

	/**
	 * Returns the value of the '<em><b>User Groups</b></em>' containment reference list.
	 * The list contents are of type {@link camel.organisation.UserGroup}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>User Groups</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>User Groups</em>' containment reference list.
	 * @see camel.organisation.OrganisationPackage#getOrganisationModel_UserGroups()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<UserGroup> getUserGroups();

	/**
	 * Returns the value of the '<em><b>Roles</b></em>' containment reference list.
	 * The list contents are of type {@link camel.organisation.Role}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Roles</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Roles</em>' containment reference list.
	 * @see camel.organisation.OrganisationPackage#getOrganisationModel_Roles()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Role> getRoles();

	/**
	 * Returns the value of the '<em><b>Role Assigments</b></em>' containment reference list.
	 * The list contents are of type {@link camel.organisation.RoleAssignment}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role Assigments</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role Assigments</em>' containment reference list.
	 * @see camel.organisation.OrganisationPackage#getOrganisationModel_RoleAssigments()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<RoleAssignment> getRoleAssigments();

	/**
	 * Returns the value of the '<em><b>Permissions</b></em>' containment reference list.
	 * The list contents are of type {@link camel.organisation.Permission}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Permissions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Permissions</em>' containment reference list.
	 * @see camel.organisation.OrganisationPackage#getOrganisationModel_Permissions()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Permission> getPermissions();

	/**
	 * Returns the value of the '<em><b>Security Level</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.organisation.SecurityLevel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Level</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Level</em>' attribute.
	 * @see camel.organisation.SecurityLevel
	 * @see #setSecurityLevel(SecurityLevel)
	 * @see camel.organisation.OrganisationPackage#getOrganisationModel_SecurityLevel()
	 * @model
	 * @generated
	 */
	SecurityLevel getSecurityLevel();

	/**
	 * Sets the value of the '{@link camel.organisation.OrganisationModel#getSecurityLevel <em>Security Level</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Security Level</em>' attribute.
	 * @see camel.organisation.SecurityLevel
	 * @see #getSecurityLevel()
	 * @generated
	 */
	void setSecurityLevel(SecurityLevel value);

	/**
	 * Returns the value of the '<em><b>Resource Filters</b></em>' containment reference list.
	 * The list contents are of type {@link camel.organisation.ResourceFilter}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Filters</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Filters</em>' containment reference list.
	 * @see camel.organisation.OrganisationPackage#getOrganisationModel_ResourceFilters()
	 * @model containment="true"
	 * @generated
	 */
	EList<ResourceFilter> getResourceFilters();

} // OrganisationModel
