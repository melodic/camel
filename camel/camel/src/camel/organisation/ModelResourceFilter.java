/**
 */
package camel.organisation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model Resource Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.ModelResourceFilter#getInformationResourcePath <em>Information Resource Path</em>}</li>
 *   <li>{@link camel.organisation.ModelResourceFilter#isEveryInformationResource <em>Every Information Resource</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getModelResourceFilter()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='information_resource_filter_validity'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot information_resource_filter_validity='Tuple {\n\tmessage : String = \'It is meaningless for InformationResourceFilter: \' + self.name + \' to apply for any model resource and to specify a path for these resources\',\n\tstatus : Boolean = \n\t\t\t\tasError(everyInformationResource = true implies\n\t\t\t\t\tinformationResourcePath = null)\n}.status'"
 * @generated
 */
public interface ModelResourceFilter extends ResourceFilter {
	/**
	 * Returns the value of the '<em><b>Information Resource Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Information Resource Path</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Information Resource Path</em>' attribute.
	 * @see #setInformationResourcePath(String)
	 * @see camel.organisation.OrganisationPackage#getModelResourceFilter_InformationResourcePath()
	 * @model
	 * @generated
	 */
	String getInformationResourcePath();

	/**
	 * Sets the value of the '{@link camel.organisation.ModelResourceFilter#getInformationResourcePath <em>Information Resource Path</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Information Resource Path</em>' attribute.
	 * @see #getInformationResourcePath()
	 * @generated
	 */
	void setInformationResourcePath(String value);

	/**
	 * Returns the value of the '<em><b>Every Information Resource</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Every Information Resource</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Every Information Resource</em>' attribute.
	 * @see #setEveryInformationResource(boolean)
	 * @see camel.organisation.OrganisationPackage#getModelResourceFilter_EveryInformationResource()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isEveryInformationResource();

	/**
	 * Sets the value of the '{@link camel.organisation.ModelResourceFilter#isEveryInformationResource <em>Every Information Resource</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Every Information Resource</em>' attribute.
	 * @see #isEveryInformationResource()
	 * @generated
	 */
	void setEveryInformationResource(boolean value);

} // ModelResourceFilter
