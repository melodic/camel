/**
 */
package camel.organisation;

import camel.deployment.DeploymentModel;

import camel.metric.MetricModel;

import camel.requirement.RequirementModel;

import camel.scalability.ScalabilityModel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.User#getEmail <em>Email</em>}</li>
 *   <li>{@link camel.organisation.User#getFirstName <em>First Name</em>}</li>
 *   <li>{@link camel.organisation.User#getLastName <em>Last Name</em>}</li>
 *   <li>{@link camel.organisation.User#getWww <em>Www</em>}</li>
 *   <li>{@link camel.organisation.User#getExternalIdentifiers <em>External Identifiers</em>}</li>
 *   <li>{@link camel.organisation.User#getPlatformCredentials <em>Platform Credentials</em>}</li>
 *   <li>{@link camel.organisation.User#getRequirementModels <em>Requirement Models</em>}</li>
 *   <li>{@link camel.organisation.User#getDeploymentModels <em>Deployment Models</em>}</li>
 *   <li>{@link camel.organisation.User#getMetricModels <em>Metric Models</em>}</li>
 *   <li>{@link camel.organisation.User#getScalabilityModels <em>Scalability Models</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getUser()
 * @model
 * @generated
 */
public interface User extends Entity {
	/**
	 * Returns the value of the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Email</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Email</em>' attribute.
	 * @see #setEmail(String)
	 * @see camel.organisation.OrganisationPackage#getUser_Email()
	 * @model required="true"
	 * @generated
	 */
	String getEmail();

	/**
	 * Sets the value of the '{@link camel.organisation.User#getEmail <em>Email</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Email</em>' attribute.
	 * @see #getEmail()
	 * @generated
	 */
	void setEmail(String value);

	/**
	 * Returns the value of the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Name</em>' attribute.
	 * @see #setFirstName(String)
	 * @see camel.organisation.OrganisationPackage#getUser_FirstName()
	 * @model required="true"
	 * @generated
	 */
	String getFirstName();

	/**
	 * Sets the value of the '{@link camel.organisation.User#getFirstName <em>First Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Name</em>' attribute.
	 * @see #getFirstName()
	 * @generated
	 */
	void setFirstName(String value);

	/**
	 * Returns the value of the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Last Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Last Name</em>' attribute.
	 * @see #setLastName(String)
	 * @see camel.organisation.OrganisationPackage#getUser_LastName()
	 * @model required="true"
	 * @generated
	 */
	String getLastName();

	/**
	 * Sets the value of the '{@link camel.organisation.User#getLastName <em>Last Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Last Name</em>' attribute.
	 * @see #getLastName()
	 * @generated
	 */
	void setLastName(String value);

	/**
	 * Returns the value of the '<em><b>Www</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Www</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Www</em>' attribute.
	 * @see #setWww(String)
	 * @see camel.organisation.OrganisationPackage#getUser_Www()
	 * @model
	 * @generated
	 */
	String getWww();

	/**
	 * Sets the value of the '{@link camel.organisation.User#getWww <em>Www</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Www</em>' attribute.
	 * @see #getWww()
	 * @generated
	 */
	void setWww(String value);

	/**
	 * Returns the value of the '<em><b>External Identifiers</b></em>' containment reference list.
	 * The list contents are of type {@link camel.organisation.ExternalIdentifier}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>External Identifiers</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>External Identifiers</em>' containment reference list.
	 * @see camel.organisation.OrganisationPackage#getUser_ExternalIdentifiers()
	 * @model containment="true"
	 * @generated
	 */
	EList<ExternalIdentifier> getExternalIdentifiers();

	/**
	 * Returns the value of the '<em><b>Requirement Models</b></em>' reference list.
	 * The list contents are of type {@link camel.requirement.RequirementModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirement Models</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirement Models</em>' reference list.
	 * @see camel.organisation.OrganisationPackage#getUser_RequirementModels()
	 * @model
	 * @generated
	 */
	EList<RequirementModel> getRequirementModels();

	/**
	 * Returns the value of the '<em><b>Deployment Models</b></em>' reference list.
	 * The list contents are of type {@link camel.deployment.DeploymentModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deployment Models</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deployment Models</em>' reference list.
	 * @see camel.organisation.OrganisationPackage#getUser_DeploymentModels()
	 * @model
	 * @generated
	 */
	EList<DeploymentModel> getDeploymentModels();

	/**
	 * Returns the value of the '<em><b>Platform Credentials</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Platform Credentials</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Platform Credentials</em>' containment reference.
	 * @see #setPlatformCredentials(PlatformCredentials)
	 * @see camel.organisation.OrganisationPackage#getUser_PlatformCredentials()
	 * @model containment="true" required="true"
	 * @generated
	 */
	PlatformCredentials getPlatformCredentials();

	/**
	 * Sets the value of the '{@link camel.organisation.User#getPlatformCredentials <em>Platform Credentials</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Platform Credentials</em>' containment reference.
	 * @see #getPlatformCredentials()
	 * @generated
	 */
	void setPlatformCredentials(PlatformCredentials value);

	/**
	 * Returns the value of the '<em><b>Metric Models</b></em>' reference list.
	 * The list contents are of type {@link camel.metric.MetricModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Models</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Models</em>' reference list.
	 * @see camel.organisation.OrganisationPackage#getUser_MetricModels()
	 * @model
	 * @generated
	 */
	EList<MetricModel> getMetricModels();

	/**
	 * Returns the value of the '<em><b>Scalability Models</b></em>' reference list.
	 * The list contents are of type {@link camel.scalability.ScalabilityModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scalability Models</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scalability Models</em>' reference list.
	 * @see camel.organisation.OrganisationPackage#getUser_ScalabilityModels()
	 * @model
	 * @generated
	 */
	EList<ScalabilityModel> getScalabilityModels();

} // User
