/**
 */
package camel.organisation;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.ResourceFilter#getResourcePattern <em>Resource Pattern</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getResourceFilter()
 * @model abstract="true"
 * @generated
 */
public interface ResourceFilter extends Feature {
	/**
	 * Returns the value of the '<em><b>Resource Pattern</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.organisation.ResourcePattern}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Pattern</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Pattern</em>' attribute.
	 * @see camel.organisation.ResourcePattern
	 * @see #setResourcePattern(ResourcePattern)
	 * @see camel.organisation.OrganisationPackage#getResourceFilter_ResourcePattern()
	 * @model
	 * @generated
	 */
	ResourcePattern getResourcePattern();

	/**
	 * Sets the value of the '{@link camel.organisation.ResourceFilter#getResourcePattern <em>Resource Pattern</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Pattern</em>' attribute.
	 * @see camel.organisation.ResourcePattern
	 * @see #getResourcePattern()
	 * @generated
	 */
	void setResourcePattern(ResourcePattern value);

} // ResourceFilter
