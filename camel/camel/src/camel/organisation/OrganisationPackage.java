/**
 */
package camel.organisation;

import camel.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see camel.organisation.OrganisationFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface OrganisationPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "organisation";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.camel-dsl.org/camel/organisation";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "organisation";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	OrganisationPackage eINSTANCE = camel.organisation.impl.OrganisationPackageImpl.init();

	/**
	 * The meta object id for the '{@link camel.organisation.impl.OrganisationModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.OrganisationModelImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getOrganisationModel()
	 * @generated
	 */
	int ORGANISATION_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__NAME = CorePackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__DESCRIPTION = CorePackage.MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__ANNOTATIONS = CorePackage.MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__ATTRIBUTES = CorePackage.MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__SUB_FEATURES = CorePackage.MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__IMPORT_URI = CorePackage.MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Organisation</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__ORGANISATION = CorePackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>External Identifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__EXTERNAL_IDENTIFIERS = CorePackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Users</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__USERS = CorePackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>User Groups</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__USER_GROUPS = CorePackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Roles</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__ROLES = CorePackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Role Assigments</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__ROLE_ASSIGMENTS = CorePackage.MODEL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Permissions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__PERMISSIONS = CorePackage.MODEL_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Security Level</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__SECURITY_LEVEL = CorePackage.MODEL_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Resource Filters</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL__RESOURCE_FILTERS = CorePackage.MODEL_FEATURE_COUNT + 8;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL_FEATURE_COUNT = CorePackage.MODEL_FEATURE_COUNT + 9;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL___AS_ERROR__BOOLEAN = CorePackage.MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_MODEL_OPERATION_COUNT = CorePackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.CredentialsImpl <em>Credentials</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.CredentialsImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getCredentials()
	 * @generated
	 */
	int CREDENTIALS = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDENTIALS__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDENTIALS__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDENTIALS__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDENTIALS__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDENTIALS__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Credentials</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDENTIALS_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDENTIALS___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Credentials</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CREDENTIALS_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.CloudCredentialsImpl <em>Cloud Credentials</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.CloudCredentialsImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getCloudCredentials()
	 * @generated
	 */
	int CLOUD_CREDENTIALS = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS__NAME = CREDENTIALS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS__DESCRIPTION = CREDENTIALS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS__ANNOTATIONS = CREDENTIALS__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS__ATTRIBUTES = CREDENTIALS__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS__SUB_FEATURES = CREDENTIALS__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Cloud Provider Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS__CLOUD_PROVIDER_NAME = CREDENTIALS_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Security Group</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS__SECURITY_GROUP = CREDENTIALS_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Public SSH Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS__PUBLIC_SSH_KEY = CREDENTIALS_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Private SSH Key</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS__PRIVATE_SSH_KEY = CREDENTIALS_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Username</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS__USERNAME = CREDENTIALS_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS__PASSWORD = CREDENTIALS_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Cloud Credentials</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS_FEATURE_COUNT = CREDENTIALS_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS___AS_ERROR__BOOLEAN = CREDENTIALS___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Cloud Credentials</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_CREDENTIALS_OPERATION_COUNT = CREDENTIALS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.EntityImpl <em>Entity</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.EntityImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getEntity()
	 * @generated
	 */
	int ENTITY = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Entity</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ENTITY_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.OrganisationImpl <em>Organisation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.OrganisationImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getOrganisation()
	 * @generated
	 */
	int ORGANISATION = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__NAME = ENTITY__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__DESCRIPTION = ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__ANNOTATIONS = ENTITY__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__ATTRIBUTES = ENTITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__SUB_FEATURES = ENTITY__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Www</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__WWW = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Postal Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__POSTAL_ADDRESS = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__EMAIL = ENTITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Cloud Credentials</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION__CLOUD_CREDENTIALS = ENTITY_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Organisation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION___AS_ERROR__BOOLEAN = ENTITY___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Organisation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ORGANISATION_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.CloudProviderImpl <em>Cloud Provider</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.CloudProviderImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getCloudProvider()
	 * @generated
	 */
	int CLOUD_PROVIDER = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__NAME = ORGANISATION__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__DESCRIPTION = ORGANISATION__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__ANNOTATIONS = ORGANISATION__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__ATTRIBUTES = ORGANISATION__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__SUB_FEATURES = ORGANISATION__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Www</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__WWW = ORGANISATION__WWW;

	/**
	 * The feature id for the '<em><b>Postal Address</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__POSTAL_ADDRESS = ORGANISATION__POSTAL_ADDRESS;

	/**
	 * The feature id for the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__EMAIL = ORGANISATION__EMAIL;

	/**
	 * The feature id for the '<em><b>Cloud Credentials</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__CLOUD_CREDENTIALS = ORGANISATION__CLOUD_CREDENTIALS;

	/**
	 * The feature id for the '<em><b>Public</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__PUBLIC = ORGANISATION_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Saa S</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__SAA_S = ORGANISATION_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Paa S</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__PAA_S = ORGANISATION_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Iaa S</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER__IAA_S = ORGANISATION_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Cloud Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER_FEATURE_COUNT = ORGANISATION_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER___AS_ERROR__BOOLEAN = ORGANISATION___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Cloud Provider</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CLOUD_PROVIDER_OPERATION_COUNT = ORGANISATION_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.UserImpl <em>User</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.UserImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getUser()
	 * @generated
	 */
	int USER = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__NAME = ENTITY__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__DESCRIPTION = ENTITY__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__ANNOTATIONS = ENTITY__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__ATTRIBUTES = ENTITY__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__SUB_FEATURES = ENTITY__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__EMAIL = ENTITY_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>First Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__FIRST_NAME = ENTITY_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Last Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__LAST_NAME = ENTITY_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Www</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__WWW = ENTITY_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>External Identifiers</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__EXTERNAL_IDENTIFIERS = ENTITY_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Platform Credentials</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__PLATFORM_CREDENTIALS = ENTITY_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Requirement Models</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__REQUIREMENT_MODELS = ENTITY_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Deployment Models</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__DEPLOYMENT_MODELS = ENTITY_FEATURE_COUNT + 7;

	/**
	 * The feature id for the '<em><b>Metric Models</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__METRIC_MODELS = ENTITY_FEATURE_COUNT + 8;

	/**
	 * The feature id for the '<em><b>Scalability Models</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER__SCALABILITY_MODELS = ENTITY_FEATURE_COUNT + 9;

	/**
	 * The number of structural features of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_FEATURE_COUNT = ENTITY_FEATURE_COUNT + 10;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER___AS_ERROR__BOOLEAN = ENTITY___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>User</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_OPERATION_COUNT = ENTITY_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.ExternalIdentifierImpl <em>External Identifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.ExternalIdentifierImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getExternalIdentifier()
	 * @generated
	 */
	int EXTERNAL_IDENTIFIER = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_IDENTIFIER__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_IDENTIFIER__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_IDENTIFIER__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_IDENTIFIER__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_IDENTIFIER__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_IDENTIFIER__IDENTIFIER = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>External Identifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_IDENTIFIER_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_IDENTIFIER___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>External Identifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXTERNAL_IDENTIFIER_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.PermissionImpl <em>Permission</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.PermissionImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getPermission()
	 * @generated
	 */
	int PERMISSION = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION__ROLE = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION__START_TIME = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION__END_TIME = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Resource Filter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION__RESOURCE_FILTER = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Action</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION__ACTION = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The number of structural features of the '<em>Permission</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 5;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Check Start End Dates</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION___CHECK_START_END_DATES__PERMISSION = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Permission</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PERMISSION_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.ResourceFilterImpl <em>Resource Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.ResourceFilterImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getResourceFilter()
	 * @generated
	 */
	int RESOURCE_FILTER = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FILTER__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FILTER__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FILTER__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FILTER__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FILTER__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Resource Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FILTER__RESOURCE_PATTERN = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Resource Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FILTER_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FILTER___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Resource Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_FILTER_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.ModelResourceFilterImpl <em>Model Resource Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.ModelResourceFilterImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getModelResourceFilter()
	 * @generated
	 */
	int MODEL_RESOURCE_FILTER = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_RESOURCE_FILTER__NAME = RESOURCE_FILTER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_RESOURCE_FILTER__DESCRIPTION = RESOURCE_FILTER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_RESOURCE_FILTER__ANNOTATIONS = RESOURCE_FILTER__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_RESOURCE_FILTER__ATTRIBUTES = RESOURCE_FILTER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_RESOURCE_FILTER__SUB_FEATURES = RESOURCE_FILTER__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Resource Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_RESOURCE_FILTER__RESOURCE_PATTERN = RESOURCE_FILTER__RESOURCE_PATTERN;

	/**
	 * The feature id for the '<em><b>Information Resource Path</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_RESOURCE_FILTER__INFORMATION_RESOURCE_PATH = RESOURCE_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Every Information Resource</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_RESOURCE_FILTER__EVERY_INFORMATION_RESOURCE = RESOURCE_FILTER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Model Resource Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_RESOURCE_FILTER_FEATURE_COUNT = RESOURCE_FILTER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_RESOURCE_FILTER___AS_ERROR__BOOLEAN = RESOURCE_FILTER___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Model Resource Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int MODEL_RESOURCE_FILTER_OPERATION_COUNT = RESOURCE_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.ServiceResourceFilterImpl <em>Service Resource Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.ServiceResourceFilterImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getServiceResourceFilter()
	 * @generated
	 */
	int SERVICE_RESOURCE_FILTER = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_RESOURCE_FILTER__NAME = RESOURCE_FILTER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_RESOURCE_FILTER__DESCRIPTION = RESOURCE_FILTER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_RESOURCE_FILTER__ANNOTATIONS = RESOURCE_FILTER__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_RESOURCE_FILTER__ATTRIBUTES = RESOURCE_FILTER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_RESOURCE_FILTER__SUB_FEATURES = RESOURCE_FILTER__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Resource Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_RESOURCE_FILTER__RESOURCE_PATTERN = RESOURCE_FILTER__RESOURCE_PATTERN;

	/**
	 * The feature id for the '<em><b>Service URL</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_RESOURCE_FILTER__SERVICE_URL = RESOURCE_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Every Service</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_RESOURCE_FILTER__EVERY_SERVICE = RESOURCE_FILTER_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Service Resource Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_RESOURCE_FILTER_FEATURE_COUNT = RESOURCE_FILTER_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_RESOURCE_FILTER___AS_ERROR__BOOLEAN = RESOURCE_FILTER___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Service Resource Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_RESOURCE_FILTER_OPERATION_COUNT = RESOURCE_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.RoleImpl <em>Role</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.RoleImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getRole()
	 * @generated
	 */
	int ROLE = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Role</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.RoleAssignmentImpl <em>Role Assignment</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.RoleAssignmentImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getRoleAssignment()
	 * @generated
	 */
	int ROLE_ASSIGNMENT = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>User</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__USER = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__ROLE = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>User Group</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__USER_GROUP = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__START_TIME = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__END_TIME = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Assignment Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT__ASSIGNMENT_TIME = CorePackage.FEATURE_FEATURE_COUNT + 5;

	/**
	 * The number of structural features of the '<em>Role Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 6;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Check Assigned On Dates</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT___CHECK_ASSIGNED_ON_DATES__ROLEASSIGNMENT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The operation id for the '<em>Check Start End Dates</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT___CHECK_START_END_DATES__ROLEASSIGNMENT = CorePackage.FEATURE_OPERATION_COUNT + 1;

	/**
	 * The number of operations of the '<em>Role Assignment</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ROLE_ASSIGNMENT_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 2;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.UserGroupImpl <em>User Group</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.UserGroupImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getUserGroup()
	 * @generated
	 */
	int USER_GROUP = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_GROUP__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_GROUP__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_GROUP__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_GROUP__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_GROUP__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Users</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_GROUP__USERS = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>User Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_GROUP_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_GROUP___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>User Group</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int USER_GROUP_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.PlatformCredentialsImpl <em>Platform Credentials</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.PlatformCredentialsImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getPlatformCredentials()
	 * @generated
	 */
	int PLATFORM_CREDENTIALS = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLATFORM_CREDENTIALS__NAME = CREDENTIALS__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLATFORM_CREDENTIALS__DESCRIPTION = CREDENTIALS__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLATFORM_CREDENTIALS__ANNOTATIONS = CREDENTIALS__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLATFORM_CREDENTIALS__ATTRIBUTES = CREDENTIALS__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLATFORM_CREDENTIALS__SUB_FEATURES = CREDENTIALS__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLATFORM_CREDENTIALS__PASSWORD = CREDENTIALS_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Platform Credentials</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLATFORM_CREDENTIALS_FEATURE_COUNT = CREDENTIALS_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLATFORM_CREDENTIALS___AS_ERROR__BOOLEAN = CREDENTIALS___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Platform Credentials</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PLATFORM_CREDENTIALS_OPERATION_COUNT = CREDENTIALS_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.DataResourceFilterImpl <em>Data Resource Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.DataResourceFilterImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getDataResourceFilter()
	 * @generated
	 */
	int DATA_RESOURCE_FILTER = 16;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER__NAME = RESOURCE_FILTER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER__DESCRIPTION = RESOURCE_FILTER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER__ANNOTATIONS = RESOURCE_FILTER__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER__ATTRIBUTES = RESOURCE_FILTER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER__SUB_FEATURES = RESOURCE_FILTER__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Resource Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER__RESOURCE_PATTERN = RESOURCE_FILTER__RESOURCE_PATTERN;

	/**
	 * The feature id for the '<em><b>Data</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER__DATA = RESOURCE_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Data Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER__DATA_INSTANCES = RESOURCE_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Locations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER__LOCATIONS = RESOURCE_FILTER_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Every Data</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER__EVERY_DATA = RESOURCE_FILTER_FEATURE_COUNT + 3;

	/**
	 * The number of structural features of the '<em>Data Resource Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER_FEATURE_COUNT = RESOURCE_FILTER_FEATURE_COUNT + 4;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER___AS_ERROR__BOOLEAN = RESOURCE_FILTER___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Data Resource Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DATA_RESOURCE_FILTER_OPERATION_COUNT = RESOURCE_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.impl.SoftwareComponentResourceFilterImpl <em>Software Component Resource Filter</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.impl.SoftwareComponentResourceFilterImpl
	 * @see camel.organisation.impl.OrganisationPackageImpl#getSoftwareComponentResourceFilter()
	 * @generated
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER = 17;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER__NAME = RESOURCE_FILTER__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER__DESCRIPTION = RESOURCE_FILTER__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER__ANNOTATIONS = RESOURCE_FILTER__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER__ATTRIBUTES = RESOURCE_FILTER__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER__SUB_FEATURES = RESOURCE_FILTER__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Resource Pattern</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER__RESOURCE_PATTERN = RESOURCE_FILTER__RESOURCE_PATTERN;

	/**
	 * The feature id for the '<em><b>Every Component</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER__EVERY_COMPONENT = RESOURCE_FILTER_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Application</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER__APPLICATION = RESOURCE_FILTER_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Software Components</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER__SOFTWARE_COMPONENTS = RESOURCE_FILTER_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Software Component Resource Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER_FEATURE_COUNT = RESOURCE_FILTER_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER___AS_ERROR__BOOLEAN = RESOURCE_FILTER___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Software Component Resource Filter</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFTWARE_COMPONENT_RESOURCE_FILTER_OPERATION_COUNT = RESOURCE_FILTER_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.organisation.PermissionActionType <em>Permission Action Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.PermissionActionType
	 * @see camel.organisation.impl.OrganisationPackageImpl#getPermissionActionType()
	 * @generated
	 */
	int PERMISSION_ACTION_TYPE = 18;

	/**
	 * The meta object id for the '{@link camel.organisation.SecurityLevel <em>Security Level</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.SecurityLevel
	 * @see camel.organisation.impl.OrganisationPackageImpl#getSecurityLevel()
	 * @generated
	 */
	int SECURITY_LEVEL = 19;

	/**
	 * The meta object id for the '{@link camel.organisation.ResourcePattern <em>Resource Pattern</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.organisation.ResourcePattern
	 * @see camel.organisation.impl.OrganisationPackageImpl#getResourcePattern()
	 * @generated
	 */
	int RESOURCE_PATTERN = 20;


	/**
	 * Returns the meta object for class '{@link camel.organisation.OrganisationModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see camel.organisation.OrganisationModel
	 * @generated
	 */
	EClass getOrganisationModel();

	/**
	 * Returns the meta object for the containment reference '{@link camel.organisation.OrganisationModel#getOrganisation <em>Organisation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Organisation</em>'.
	 * @see camel.organisation.OrganisationModel#getOrganisation()
	 * @see #getOrganisationModel()
	 * @generated
	 */
	EReference getOrganisationModel_Organisation();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.organisation.OrganisationModel#getExternalIdentifiers <em>External Identifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>External Identifiers</em>'.
	 * @see camel.organisation.OrganisationModel#getExternalIdentifiers()
	 * @see #getOrganisationModel()
	 * @generated
	 */
	EReference getOrganisationModel_ExternalIdentifiers();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.organisation.OrganisationModel#getUsers <em>Users</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Users</em>'.
	 * @see camel.organisation.OrganisationModel#getUsers()
	 * @see #getOrganisationModel()
	 * @generated
	 */
	EReference getOrganisationModel_Users();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.organisation.OrganisationModel#getUserGroups <em>User Groups</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>User Groups</em>'.
	 * @see camel.organisation.OrganisationModel#getUserGroups()
	 * @see #getOrganisationModel()
	 * @generated
	 */
	EReference getOrganisationModel_UserGroups();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.organisation.OrganisationModel#getRoles <em>Roles</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Roles</em>'.
	 * @see camel.organisation.OrganisationModel#getRoles()
	 * @see #getOrganisationModel()
	 * @generated
	 */
	EReference getOrganisationModel_Roles();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.organisation.OrganisationModel#getRoleAssigments <em>Role Assigments</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Role Assigments</em>'.
	 * @see camel.organisation.OrganisationModel#getRoleAssigments()
	 * @see #getOrganisationModel()
	 * @generated
	 */
	EReference getOrganisationModel_RoleAssigments();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.organisation.OrganisationModel#getPermissions <em>Permissions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Permissions</em>'.
	 * @see camel.organisation.OrganisationModel#getPermissions()
	 * @see #getOrganisationModel()
	 * @generated
	 */
	EReference getOrganisationModel_Permissions();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.OrganisationModel#getSecurityLevel <em>Security Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Security Level</em>'.
	 * @see camel.organisation.OrganisationModel#getSecurityLevel()
	 * @see #getOrganisationModel()
	 * @generated
	 */
	EAttribute getOrganisationModel_SecurityLevel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.organisation.OrganisationModel#getResourceFilters <em>Resource Filters</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Resource Filters</em>'.
	 * @see camel.organisation.OrganisationModel#getResourceFilters()
	 * @see #getOrganisationModel()
	 * @generated
	 */
	EReference getOrganisationModel_ResourceFilters();

	/**
	 * Returns the meta object for class '{@link camel.organisation.Credentials <em>Credentials</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Credentials</em>'.
	 * @see camel.organisation.Credentials
	 * @generated
	 */
	EClass getCredentials();

	/**
	 * Returns the meta object for class '{@link camel.organisation.CloudCredentials <em>Cloud Credentials</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cloud Credentials</em>'.
	 * @see camel.organisation.CloudCredentials
	 * @generated
	 */
	EClass getCloudCredentials();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.CloudCredentials#getCloudProviderName <em>Cloud Provider Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cloud Provider Name</em>'.
	 * @see camel.organisation.CloudCredentials#getCloudProviderName()
	 * @see #getCloudCredentials()
	 * @generated
	 */
	EAttribute getCloudCredentials_CloudProviderName();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.CloudCredentials#getSecurityGroup <em>Security Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Security Group</em>'.
	 * @see camel.organisation.CloudCredentials#getSecurityGroup()
	 * @see #getCloudCredentials()
	 * @generated
	 */
	EAttribute getCloudCredentials_SecurityGroup();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.CloudCredentials#getPublicSSHKey <em>Public SSH Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Public SSH Key</em>'.
	 * @see camel.organisation.CloudCredentials#getPublicSSHKey()
	 * @see #getCloudCredentials()
	 * @generated
	 */
	EAttribute getCloudCredentials_PublicSSHKey();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.CloudCredentials#getPrivateSSHKey <em>Private SSH Key</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Private SSH Key</em>'.
	 * @see camel.organisation.CloudCredentials#getPrivateSSHKey()
	 * @see #getCloudCredentials()
	 * @generated
	 */
	EAttribute getCloudCredentials_PrivateSSHKey();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.CloudCredentials#getUsername <em>Username</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Username</em>'.
	 * @see camel.organisation.CloudCredentials#getUsername()
	 * @see #getCloudCredentials()
	 * @generated
	 */
	EAttribute getCloudCredentials_Username();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.CloudCredentials#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Password</em>'.
	 * @see camel.organisation.CloudCredentials#getPassword()
	 * @see #getCloudCredentials()
	 * @generated
	 */
	EAttribute getCloudCredentials_Password();

	/**
	 * Returns the meta object for class '{@link camel.organisation.Entity <em>Entity</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Entity</em>'.
	 * @see camel.organisation.Entity
	 * @generated
	 */
	EClass getEntity();

	/**
	 * Returns the meta object for class '{@link camel.organisation.Organisation <em>Organisation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Organisation</em>'.
	 * @see camel.organisation.Organisation
	 * @generated
	 */
	EClass getOrganisation();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.Organisation#getWww <em>Www</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Www</em>'.
	 * @see camel.organisation.Organisation#getWww()
	 * @see #getOrganisation()
	 * @generated
	 */
	EAttribute getOrganisation_Www();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.Organisation#getPostalAddress <em>Postal Address</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Postal Address</em>'.
	 * @see camel.organisation.Organisation#getPostalAddress()
	 * @see #getOrganisation()
	 * @generated
	 */
	EAttribute getOrganisation_PostalAddress();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.Organisation#getEmail <em>Email</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Email</em>'.
	 * @see camel.organisation.Organisation#getEmail()
	 * @see #getOrganisation()
	 * @generated
	 */
	EAttribute getOrganisation_Email();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.organisation.Organisation#getCloudCredentials <em>Cloud Credentials</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Cloud Credentials</em>'.
	 * @see camel.organisation.Organisation#getCloudCredentials()
	 * @see #getOrganisation()
	 * @generated
	 */
	EReference getOrganisation_CloudCredentials();

	/**
	 * Returns the meta object for class '{@link camel.organisation.CloudProvider <em>Cloud Provider</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Cloud Provider</em>'.
	 * @see camel.organisation.CloudProvider
	 * @generated
	 */
	EClass getCloudProvider();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.CloudProvider#isPublic <em>Public</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Public</em>'.
	 * @see camel.organisation.CloudProvider#isPublic()
	 * @see #getCloudProvider()
	 * @generated
	 */
	EAttribute getCloudProvider_Public();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.CloudProvider#isSaaS <em>Saa S</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Saa S</em>'.
	 * @see camel.organisation.CloudProvider#isSaaS()
	 * @see #getCloudProvider()
	 * @generated
	 */
	EAttribute getCloudProvider_SaaS();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.CloudProvider#isPaaS <em>Paa S</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Paa S</em>'.
	 * @see camel.organisation.CloudProvider#isPaaS()
	 * @see #getCloudProvider()
	 * @generated
	 */
	EAttribute getCloudProvider_PaaS();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.CloudProvider#isIaaS <em>Iaa S</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Iaa S</em>'.
	 * @see camel.organisation.CloudProvider#isIaaS()
	 * @see #getCloudProvider()
	 * @generated
	 */
	EAttribute getCloudProvider_IaaS();

	/**
	 * Returns the meta object for class '{@link camel.organisation.User <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User</em>'.
	 * @see camel.organisation.User
	 * @generated
	 */
	EClass getUser();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.User#getEmail <em>Email</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Email</em>'.
	 * @see camel.organisation.User#getEmail()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Email();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.User#getFirstName <em>First Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>First Name</em>'.
	 * @see camel.organisation.User#getFirstName()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_FirstName();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.User#getLastName <em>Last Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Last Name</em>'.
	 * @see camel.organisation.User#getLastName()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_LastName();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.User#getWww <em>Www</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Www</em>'.
	 * @see camel.organisation.User#getWww()
	 * @see #getUser()
	 * @generated
	 */
	EAttribute getUser_Www();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.organisation.User#getExternalIdentifiers <em>External Identifiers</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>External Identifiers</em>'.
	 * @see camel.organisation.User#getExternalIdentifiers()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_ExternalIdentifiers();

	/**
	 * Returns the meta object for the reference list '{@link camel.organisation.User#getRequirementModels <em>Requirement Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Requirement Models</em>'.
	 * @see camel.organisation.User#getRequirementModels()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_RequirementModels();

	/**
	 * Returns the meta object for the reference list '{@link camel.organisation.User#getDeploymentModels <em>Deployment Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Deployment Models</em>'.
	 * @see camel.organisation.User#getDeploymentModels()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_DeploymentModels();

	/**
	 * Returns the meta object for the containment reference '{@link camel.organisation.User#getPlatformCredentials <em>Platform Credentials</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Platform Credentials</em>'.
	 * @see camel.organisation.User#getPlatformCredentials()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_PlatformCredentials();

	/**
	 * Returns the meta object for the reference list '{@link camel.organisation.User#getMetricModels <em>Metric Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Metric Models</em>'.
	 * @see camel.organisation.User#getMetricModels()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_MetricModels();

	/**
	 * Returns the meta object for the reference list '{@link camel.organisation.User#getScalabilityModels <em>Scalability Models</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Scalability Models</em>'.
	 * @see camel.organisation.User#getScalabilityModels()
	 * @see #getUser()
	 * @generated
	 */
	EReference getUser_ScalabilityModels();

	/**
	 * Returns the meta object for class '{@link camel.organisation.ExternalIdentifier <em>External Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>External Identifier</em>'.
	 * @see camel.organisation.ExternalIdentifier
	 * @generated
	 */
	EClass getExternalIdentifier();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.ExternalIdentifier#getIdentifier <em>Identifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Identifier</em>'.
	 * @see camel.organisation.ExternalIdentifier#getIdentifier()
	 * @see #getExternalIdentifier()
	 * @generated
	 */
	EAttribute getExternalIdentifier_Identifier();

	/**
	 * Returns the meta object for class '{@link camel.organisation.Permission <em>Permission</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Permission</em>'.
	 * @see camel.organisation.Permission
	 * @generated
	 */
	EClass getPermission();

	/**
	 * Returns the meta object for the reference '{@link camel.organisation.Permission#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see camel.organisation.Permission#getRole()
	 * @see #getPermission()
	 * @generated
	 */
	EReference getPermission_Role();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.Permission#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see camel.organisation.Permission#getStartTime()
	 * @see #getPermission()
	 * @generated
	 */
	EAttribute getPermission_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.Permission#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see camel.organisation.Permission#getEndTime()
	 * @see #getPermission()
	 * @generated
	 */
	EAttribute getPermission_EndTime();

	/**
	 * Returns the meta object for the reference '{@link camel.organisation.Permission#getResourceFilter <em>Resource Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Resource Filter</em>'.
	 * @see camel.organisation.Permission#getResourceFilter()
	 * @see #getPermission()
	 * @generated
	 */
	EReference getPermission_ResourceFilter();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.Permission#getAction <em>Action</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Action</em>'.
	 * @see camel.organisation.Permission#getAction()
	 * @see #getPermission()
	 * @generated
	 */
	EAttribute getPermission_Action();

	/**
	 * Returns the meta object for the '{@link camel.organisation.Permission#checkStartEndDates(camel.organisation.Permission) <em>Check Start End Dates</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Start End Dates</em>' operation.
	 * @see camel.organisation.Permission#checkStartEndDates(camel.organisation.Permission)
	 * @generated
	 */
	EOperation getPermission__CheckStartEndDates__Permission();

	/**
	 * Returns the meta object for class '{@link camel.organisation.ResourceFilter <em>Resource Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Filter</em>'.
	 * @see camel.organisation.ResourceFilter
	 * @generated
	 */
	EClass getResourceFilter();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.ResourceFilter#getResourcePattern <em>Resource Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Resource Pattern</em>'.
	 * @see camel.organisation.ResourceFilter#getResourcePattern()
	 * @see #getResourceFilter()
	 * @generated
	 */
	EAttribute getResourceFilter_ResourcePattern();

	/**
	 * Returns the meta object for class '{@link camel.organisation.ModelResourceFilter <em>Model Resource Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model Resource Filter</em>'.
	 * @see camel.organisation.ModelResourceFilter
	 * @generated
	 */
	EClass getModelResourceFilter();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.ModelResourceFilter#getInformationResourcePath <em>Information Resource Path</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Information Resource Path</em>'.
	 * @see camel.organisation.ModelResourceFilter#getInformationResourcePath()
	 * @see #getModelResourceFilter()
	 * @generated
	 */
	EAttribute getModelResourceFilter_InformationResourcePath();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.ModelResourceFilter#isEveryInformationResource <em>Every Information Resource</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Every Information Resource</em>'.
	 * @see camel.organisation.ModelResourceFilter#isEveryInformationResource()
	 * @see #getModelResourceFilter()
	 * @generated
	 */
	EAttribute getModelResourceFilter_EveryInformationResource();

	/**
	 * Returns the meta object for class '{@link camel.organisation.ServiceResourceFilter <em>Service Resource Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Resource Filter</em>'.
	 * @see camel.organisation.ServiceResourceFilter
	 * @generated
	 */
	EClass getServiceResourceFilter();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.ServiceResourceFilter#getServiceURL <em>Service URL</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Service URL</em>'.
	 * @see camel.organisation.ServiceResourceFilter#getServiceURL()
	 * @see #getServiceResourceFilter()
	 * @generated
	 */
	EAttribute getServiceResourceFilter_ServiceURL();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.ServiceResourceFilter#isEveryService <em>Every Service</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Every Service</em>'.
	 * @see camel.organisation.ServiceResourceFilter#isEveryService()
	 * @see #getServiceResourceFilter()
	 * @generated
	 */
	EAttribute getServiceResourceFilter_EveryService();

	/**
	 * Returns the meta object for class '{@link camel.organisation.Role <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role</em>'.
	 * @see camel.organisation.Role
	 * @generated
	 */
	EClass getRole();

	/**
	 * Returns the meta object for class '{@link camel.organisation.RoleAssignment <em>Role Assignment</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Role Assignment</em>'.
	 * @see camel.organisation.RoleAssignment
	 * @generated
	 */
	EClass getRoleAssignment();

	/**
	 * Returns the meta object for the reference '{@link camel.organisation.RoleAssignment#getUser <em>User</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>User</em>'.
	 * @see camel.organisation.RoleAssignment#getUser()
	 * @see #getRoleAssignment()
	 * @generated
	 */
	EReference getRoleAssignment_User();

	/**
	 * Returns the meta object for the reference '{@link camel.organisation.RoleAssignment#getRole <em>Role</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Role</em>'.
	 * @see camel.organisation.RoleAssignment#getRole()
	 * @see #getRoleAssignment()
	 * @generated
	 */
	EReference getRoleAssignment_Role();

	/**
	 * Returns the meta object for the reference '{@link camel.organisation.RoleAssignment#getUserGroup <em>User Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>User Group</em>'.
	 * @see camel.organisation.RoleAssignment#getUserGroup()
	 * @see #getRoleAssignment()
	 * @generated
	 */
	EReference getRoleAssignment_UserGroup();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.RoleAssignment#getStartTime <em>Start Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Start Time</em>'.
	 * @see camel.organisation.RoleAssignment#getStartTime()
	 * @see #getRoleAssignment()
	 * @generated
	 */
	EAttribute getRoleAssignment_StartTime();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.RoleAssignment#getEndTime <em>End Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>End Time</em>'.
	 * @see camel.organisation.RoleAssignment#getEndTime()
	 * @see #getRoleAssignment()
	 * @generated
	 */
	EAttribute getRoleAssignment_EndTime();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.RoleAssignment#getAssignmentTime <em>Assignment Time</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Assignment Time</em>'.
	 * @see camel.organisation.RoleAssignment#getAssignmentTime()
	 * @see #getRoleAssignment()
	 * @generated
	 */
	EAttribute getRoleAssignment_AssignmentTime();

	/**
	 * Returns the meta object for the '{@link camel.organisation.RoleAssignment#checkAssignedOnDates(camel.organisation.RoleAssignment) <em>Check Assigned On Dates</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Assigned On Dates</em>' operation.
	 * @see camel.organisation.RoleAssignment#checkAssignedOnDates(camel.organisation.RoleAssignment)
	 * @generated
	 */
	EOperation getRoleAssignment__CheckAssignedOnDates__RoleAssignment();

	/**
	 * Returns the meta object for the '{@link camel.organisation.RoleAssignment#checkStartEndDates(camel.organisation.RoleAssignment) <em>Check Start End Dates</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Check Start End Dates</em>' operation.
	 * @see camel.organisation.RoleAssignment#checkStartEndDates(camel.organisation.RoleAssignment)
	 * @generated
	 */
	EOperation getRoleAssignment__CheckStartEndDates__RoleAssignment();

	/**
	 * Returns the meta object for class '{@link camel.organisation.UserGroup <em>User Group</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>User Group</em>'.
	 * @see camel.organisation.UserGroup
	 * @generated
	 */
	EClass getUserGroup();

	/**
	 * Returns the meta object for the reference list '{@link camel.organisation.UserGroup#getUsers <em>Users</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Users</em>'.
	 * @see camel.organisation.UserGroup#getUsers()
	 * @see #getUserGroup()
	 * @generated
	 */
	EReference getUserGroup_Users();

	/**
	 * Returns the meta object for class '{@link camel.organisation.PlatformCredentials <em>Platform Credentials</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Platform Credentials</em>'.
	 * @see camel.organisation.PlatformCredentials
	 * @generated
	 */
	EClass getPlatformCredentials();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.PlatformCredentials#getPassword <em>Password</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Password</em>'.
	 * @see camel.organisation.PlatformCredentials#getPassword()
	 * @see #getPlatformCredentials()
	 * @generated
	 */
	EAttribute getPlatformCredentials_Password();

	/**
	 * Returns the meta object for class '{@link camel.organisation.DataResourceFilter <em>Data Resource Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Data Resource Filter</em>'.
	 * @see camel.organisation.DataResourceFilter
	 * @generated
	 */
	EClass getDataResourceFilter();

	/**
	 * Returns the meta object for the reference list '{@link camel.organisation.DataResourceFilter#getData <em>Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Data</em>'.
	 * @see camel.organisation.DataResourceFilter#getData()
	 * @see #getDataResourceFilter()
	 * @generated
	 */
	EReference getDataResourceFilter_Data();

	/**
	 * Returns the meta object for the reference list '{@link camel.organisation.DataResourceFilter#getDataInstances <em>Data Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Data Instances</em>'.
	 * @see camel.organisation.DataResourceFilter#getDataInstances()
	 * @see #getDataResourceFilter()
	 * @generated
	 */
	EReference getDataResourceFilter_DataInstances();

	/**
	 * Returns the meta object for the reference list '{@link camel.organisation.DataResourceFilter#getLocations <em>Locations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Locations</em>'.
	 * @see camel.organisation.DataResourceFilter#getLocations()
	 * @see #getDataResourceFilter()
	 * @generated
	 */
	EReference getDataResourceFilter_Locations();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.DataResourceFilter#isEveryData <em>Every Data</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Every Data</em>'.
	 * @see camel.organisation.DataResourceFilter#isEveryData()
	 * @see #getDataResourceFilter()
	 * @generated
	 */
	EAttribute getDataResourceFilter_EveryData();

	/**
	 * Returns the meta object for class '{@link camel.organisation.SoftwareComponentResourceFilter <em>Software Component Resource Filter</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Software Component Resource Filter</em>'.
	 * @see camel.organisation.SoftwareComponentResourceFilter
	 * @generated
	 */
	EClass getSoftwareComponentResourceFilter();

	/**
	 * Returns the meta object for the attribute '{@link camel.organisation.SoftwareComponentResourceFilter#isEveryComponent <em>Every Component</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Every Component</em>'.
	 * @see camel.organisation.SoftwareComponentResourceFilter#isEveryComponent()
	 * @see #getSoftwareComponentResourceFilter()
	 * @generated
	 */
	EAttribute getSoftwareComponentResourceFilter_EveryComponent();

	/**
	 * Returns the meta object for the reference '{@link camel.organisation.SoftwareComponentResourceFilter#getApplication <em>Application</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Application</em>'.
	 * @see camel.organisation.SoftwareComponentResourceFilter#getApplication()
	 * @see #getSoftwareComponentResourceFilter()
	 * @generated
	 */
	EReference getSoftwareComponentResourceFilter_Application();

	/**
	 * Returns the meta object for the reference list '{@link camel.organisation.SoftwareComponentResourceFilter#getSoftwareComponents <em>Software Components</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Software Components</em>'.
	 * @see camel.organisation.SoftwareComponentResourceFilter#getSoftwareComponents()
	 * @see #getSoftwareComponentResourceFilter()
	 * @generated
	 */
	EReference getSoftwareComponentResourceFilter_SoftwareComponents();

	/**
	 * Returns the meta object for enum '{@link camel.organisation.PermissionActionType <em>Permission Action Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Permission Action Type</em>'.
	 * @see camel.organisation.PermissionActionType
	 * @generated
	 */
	EEnum getPermissionActionType();

	/**
	 * Returns the meta object for enum '{@link camel.organisation.SecurityLevel <em>Security Level</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Security Level</em>'.
	 * @see camel.organisation.SecurityLevel
	 * @generated
	 */
	EEnum getSecurityLevel();

	/**
	 * Returns the meta object for enum '{@link camel.organisation.ResourcePattern <em>Resource Pattern</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Resource Pattern</em>'.
	 * @see camel.organisation.ResourcePattern
	 * @generated
	 */
	EEnum getResourcePattern();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	OrganisationFactory getOrganisationFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link camel.organisation.impl.OrganisationModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.OrganisationModelImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getOrganisationModel()
		 * @generated
		 */
		EClass ORGANISATION_MODEL = eINSTANCE.getOrganisationModel();

		/**
		 * The meta object literal for the '<em><b>Organisation</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION_MODEL__ORGANISATION = eINSTANCE.getOrganisationModel_Organisation();

		/**
		 * The meta object literal for the '<em><b>External Identifiers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION_MODEL__EXTERNAL_IDENTIFIERS = eINSTANCE.getOrganisationModel_ExternalIdentifiers();

		/**
		 * The meta object literal for the '<em><b>Users</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION_MODEL__USERS = eINSTANCE.getOrganisationModel_Users();

		/**
		 * The meta object literal for the '<em><b>User Groups</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION_MODEL__USER_GROUPS = eINSTANCE.getOrganisationModel_UserGroups();

		/**
		 * The meta object literal for the '<em><b>Roles</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION_MODEL__ROLES = eINSTANCE.getOrganisationModel_Roles();

		/**
		 * The meta object literal for the '<em><b>Role Assigments</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION_MODEL__ROLE_ASSIGMENTS = eINSTANCE.getOrganisationModel_RoleAssigments();

		/**
		 * The meta object literal for the '<em><b>Permissions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION_MODEL__PERMISSIONS = eINSTANCE.getOrganisationModel_Permissions();

		/**
		 * The meta object literal for the '<em><b>Security Level</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORGANISATION_MODEL__SECURITY_LEVEL = eINSTANCE.getOrganisationModel_SecurityLevel();

		/**
		 * The meta object literal for the '<em><b>Resource Filters</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION_MODEL__RESOURCE_FILTERS = eINSTANCE.getOrganisationModel_ResourceFilters();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.CredentialsImpl <em>Credentials</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.CredentialsImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getCredentials()
		 * @generated
		 */
		EClass CREDENTIALS = eINSTANCE.getCredentials();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.CloudCredentialsImpl <em>Cloud Credentials</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.CloudCredentialsImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getCloudCredentials()
		 * @generated
		 */
		EClass CLOUD_CREDENTIALS = eINSTANCE.getCloudCredentials();

		/**
		 * The meta object literal for the '<em><b>Cloud Provider Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD_CREDENTIALS__CLOUD_PROVIDER_NAME = eINSTANCE.getCloudCredentials_CloudProviderName();

		/**
		 * The meta object literal for the '<em><b>Security Group</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD_CREDENTIALS__SECURITY_GROUP = eINSTANCE.getCloudCredentials_SecurityGroup();

		/**
		 * The meta object literal for the '<em><b>Public SSH Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD_CREDENTIALS__PUBLIC_SSH_KEY = eINSTANCE.getCloudCredentials_PublicSSHKey();

		/**
		 * The meta object literal for the '<em><b>Private SSH Key</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD_CREDENTIALS__PRIVATE_SSH_KEY = eINSTANCE.getCloudCredentials_PrivateSSHKey();

		/**
		 * The meta object literal for the '<em><b>Username</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD_CREDENTIALS__USERNAME = eINSTANCE.getCloudCredentials_Username();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD_CREDENTIALS__PASSWORD = eINSTANCE.getCloudCredentials_Password();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.EntityImpl <em>Entity</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.EntityImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getEntity()
		 * @generated
		 */
		EClass ENTITY = eINSTANCE.getEntity();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.OrganisationImpl <em>Organisation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.OrganisationImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getOrganisation()
		 * @generated
		 */
		EClass ORGANISATION = eINSTANCE.getOrganisation();

		/**
		 * The meta object literal for the '<em><b>Www</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORGANISATION__WWW = eINSTANCE.getOrganisation_Www();

		/**
		 * The meta object literal for the '<em><b>Postal Address</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORGANISATION__POSTAL_ADDRESS = eINSTANCE.getOrganisation_PostalAddress();

		/**
		 * The meta object literal for the '<em><b>Email</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ORGANISATION__EMAIL = eINSTANCE.getOrganisation_Email();

		/**
		 * The meta object literal for the '<em><b>Cloud Credentials</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ORGANISATION__CLOUD_CREDENTIALS = eINSTANCE.getOrganisation_CloudCredentials();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.CloudProviderImpl <em>Cloud Provider</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.CloudProviderImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getCloudProvider()
		 * @generated
		 */
		EClass CLOUD_PROVIDER = eINSTANCE.getCloudProvider();

		/**
		 * The meta object literal for the '<em><b>Public</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD_PROVIDER__PUBLIC = eINSTANCE.getCloudProvider_Public();

		/**
		 * The meta object literal for the '<em><b>Saa S</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD_PROVIDER__SAA_S = eINSTANCE.getCloudProvider_SaaS();

		/**
		 * The meta object literal for the '<em><b>Paa S</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD_PROVIDER__PAA_S = eINSTANCE.getCloudProvider_PaaS();

		/**
		 * The meta object literal for the '<em><b>Iaa S</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute CLOUD_PROVIDER__IAA_S = eINSTANCE.getCloudProvider_IaaS();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.UserImpl <em>User</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.UserImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getUser()
		 * @generated
		 */
		EClass USER = eINSTANCE.getUser();

		/**
		 * The meta object literal for the '<em><b>Email</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__EMAIL = eINSTANCE.getUser_Email();

		/**
		 * The meta object literal for the '<em><b>First Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__FIRST_NAME = eINSTANCE.getUser_FirstName();

		/**
		 * The meta object literal for the '<em><b>Last Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__LAST_NAME = eINSTANCE.getUser_LastName();

		/**
		 * The meta object literal for the '<em><b>Www</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute USER__WWW = eINSTANCE.getUser_Www();

		/**
		 * The meta object literal for the '<em><b>External Identifiers</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__EXTERNAL_IDENTIFIERS = eINSTANCE.getUser_ExternalIdentifiers();

		/**
		 * The meta object literal for the '<em><b>Requirement Models</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__REQUIREMENT_MODELS = eINSTANCE.getUser_RequirementModels();

		/**
		 * The meta object literal for the '<em><b>Deployment Models</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__DEPLOYMENT_MODELS = eINSTANCE.getUser_DeploymentModels();

		/**
		 * The meta object literal for the '<em><b>Platform Credentials</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__PLATFORM_CREDENTIALS = eINSTANCE.getUser_PlatformCredentials();

		/**
		 * The meta object literal for the '<em><b>Metric Models</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__METRIC_MODELS = eINSTANCE.getUser_MetricModels();

		/**
		 * The meta object literal for the '<em><b>Scalability Models</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER__SCALABILITY_MODELS = eINSTANCE.getUser_ScalabilityModels();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.ExternalIdentifierImpl <em>External Identifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.ExternalIdentifierImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getExternalIdentifier()
		 * @generated
		 */
		EClass EXTERNAL_IDENTIFIER = eINSTANCE.getExternalIdentifier();

		/**
		 * The meta object literal for the '<em><b>Identifier</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute EXTERNAL_IDENTIFIER__IDENTIFIER = eINSTANCE.getExternalIdentifier_Identifier();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.PermissionImpl <em>Permission</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.PermissionImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getPermission()
		 * @generated
		 */
		EClass PERMISSION = eINSTANCE.getPermission();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERMISSION__ROLE = eINSTANCE.getPermission_Role();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERMISSION__START_TIME = eINSTANCE.getPermission_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERMISSION__END_TIME = eINSTANCE.getPermission_EndTime();

		/**
		 * The meta object literal for the '<em><b>Resource Filter</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference PERMISSION__RESOURCE_FILTER = eINSTANCE.getPermission_ResourceFilter();

		/**
		 * The meta object literal for the '<em><b>Action</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PERMISSION__ACTION = eINSTANCE.getPermission_Action();

		/**
		 * The meta object literal for the '<em><b>Check Start End Dates</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation PERMISSION___CHECK_START_END_DATES__PERMISSION = eINSTANCE.getPermission__CheckStartEndDates__Permission();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.ResourceFilterImpl <em>Resource Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.ResourceFilterImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getResourceFilter()
		 * @generated
		 */
		EClass RESOURCE_FILTER = eINSTANCE.getResourceFilter();

		/**
		 * The meta object literal for the '<em><b>Resource Pattern</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute RESOURCE_FILTER__RESOURCE_PATTERN = eINSTANCE.getResourceFilter_ResourcePattern();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.ModelResourceFilterImpl <em>Model Resource Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.ModelResourceFilterImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getModelResourceFilter()
		 * @generated
		 */
		EClass MODEL_RESOURCE_FILTER = eINSTANCE.getModelResourceFilter();

		/**
		 * The meta object literal for the '<em><b>Information Resource Path</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_RESOURCE_FILTER__INFORMATION_RESOURCE_PATH = eINSTANCE.getModelResourceFilter_InformationResourcePath();

		/**
		 * The meta object literal for the '<em><b>Every Information Resource</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute MODEL_RESOURCE_FILTER__EVERY_INFORMATION_RESOURCE = eINSTANCE.getModelResourceFilter_EveryInformationResource();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.ServiceResourceFilterImpl <em>Service Resource Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.ServiceResourceFilterImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getServiceResourceFilter()
		 * @generated
		 */
		EClass SERVICE_RESOURCE_FILTER = eINSTANCE.getServiceResourceFilter();

		/**
		 * The meta object literal for the '<em><b>Service URL</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE_RESOURCE_FILTER__SERVICE_URL = eINSTANCE.getServiceResourceFilter_ServiceURL();

		/**
		 * The meta object literal for the '<em><b>Every Service</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SERVICE_RESOURCE_FILTER__EVERY_SERVICE = eINSTANCE.getServiceResourceFilter_EveryService();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.RoleImpl <em>Role</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.RoleImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getRole()
		 * @generated
		 */
		EClass ROLE = eINSTANCE.getRole();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.RoleAssignmentImpl <em>Role Assignment</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.RoleAssignmentImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getRoleAssignment()
		 * @generated
		 */
		EClass ROLE_ASSIGNMENT = eINSTANCE.getRoleAssignment();

		/**
		 * The meta object literal for the '<em><b>User</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_ASSIGNMENT__USER = eINSTANCE.getRoleAssignment_User();

		/**
		 * The meta object literal for the '<em><b>Role</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_ASSIGNMENT__ROLE = eINSTANCE.getRoleAssignment_Role();

		/**
		 * The meta object literal for the '<em><b>User Group</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference ROLE_ASSIGNMENT__USER_GROUP = eINSTANCE.getRoleAssignment_UserGroup();

		/**
		 * The meta object literal for the '<em><b>Start Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE_ASSIGNMENT__START_TIME = eINSTANCE.getRoleAssignment_StartTime();

		/**
		 * The meta object literal for the '<em><b>End Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE_ASSIGNMENT__END_TIME = eINSTANCE.getRoleAssignment_EndTime();

		/**
		 * The meta object literal for the '<em><b>Assignment Time</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ROLE_ASSIGNMENT__ASSIGNMENT_TIME = eINSTANCE.getRoleAssignment_AssignmentTime();

		/**
		 * The meta object literal for the '<em><b>Check Assigned On Dates</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROLE_ASSIGNMENT___CHECK_ASSIGNED_ON_DATES__ROLEASSIGNMENT = eINSTANCE.getRoleAssignment__CheckAssignedOnDates__RoleAssignment();

		/**
		 * The meta object literal for the '<em><b>Check Start End Dates</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation ROLE_ASSIGNMENT___CHECK_START_END_DATES__ROLEASSIGNMENT = eINSTANCE.getRoleAssignment__CheckStartEndDates__RoleAssignment();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.UserGroupImpl <em>User Group</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.UserGroupImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getUserGroup()
		 * @generated
		 */
		EClass USER_GROUP = eINSTANCE.getUserGroup();

		/**
		 * The meta object literal for the '<em><b>Users</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference USER_GROUP__USERS = eINSTANCE.getUserGroup_Users();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.PlatformCredentialsImpl <em>Platform Credentials</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.PlatformCredentialsImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getPlatformCredentials()
		 * @generated
		 */
		EClass PLATFORM_CREDENTIALS = eINSTANCE.getPlatformCredentials();

		/**
		 * The meta object literal for the '<em><b>Password</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PLATFORM_CREDENTIALS__PASSWORD = eINSTANCE.getPlatformCredentials_Password();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.DataResourceFilterImpl <em>Data Resource Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.DataResourceFilterImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getDataResourceFilter()
		 * @generated
		 */
		EClass DATA_RESOURCE_FILTER = eINSTANCE.getDataResourceFilter();

		/**
		 * The meta object literal for the '<em><b>Data</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_RESOURCE_FILTER__DATA = eINSTANCE.getDataResourceFilter_Data();

		/**
		 * The meta object literal for the '<em><b>Data Instances</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_RESOURCE_FILTER__DATA_INSTANCES = eINSTANCE.getDataResourceFilter_DataInstances();

		/**
		 * The meta object literal for the '<em><b>Locations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DATA_RESOURCE_FILTER__LOCATIONS = eINSTANCE.getDataResourceFilter_Locations();

		/**
		 * The meta object literal for the '<em><b>Every Data</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute DATA_RESOURCE_FILTER__EVERY_DATA = eINSTANCE.getDataResourceFilter_EveryData();

		/**
		 * The meta object literal for the '{@link camel.organisation.impl.SoftwareComponentResourceFilterImpl <em>Software Component Resource Filter</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.impl.SoftwareComponentResourceFilterImpl
		 * @see camel.organisation.impl.OrganisationPackageImpl#getSoftwareComponentResourceFilter()
		 * @generated
		 */
		EClass SOFTWARE_COMPONENT_RESOURCE_FILTER = eINSTANCE.getSoftwareComponentResourceFilter();

		/**
		 * The meta object literal for the '<em><b>Every Component</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFTWARE_COMPONENT_RESOURCE_FILTER__EVERY_COMPONENT = eINSTANCE.getSoftwareComponentResourceFilter_EveryComponent();

		/**
		 * The meta object literal for the '<em><b>Application</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT_RESOURCE_FILTER__APPLICATION = eINSTANCE.getSoftwareComponentResourceFilter_Application();

		/**
		 * The meta object literal for the '<em><b>Software Components</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SOFTWARE_COMPONENT_RESOURCE_FILTER__SOFTWARE_COMPONENTS = eINSTANCE.getSoftwareComponentResourceFilter_SoftwareComponents();

		/**
		 * The meta object literal for the '{@link camel.organisation.PermissionActionType <em>Permission Action Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.PermissionActionType
		 * @see camel.organisation.impl.OrganisationPackageImpl#getPermissionActionType()
		 * @generated
		 */
		EEnum PERMISSION_ACTION_TYPE = eINSTANCE.getPermissionActionType();

		/**
		 * The meta object literal for the '{@link camel.organisation.SecurityLevel <em>Security Level</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.SecurityLevel
		 * @see camel.organisation.impl.OrganisationPackageImpl#getSecurityLevel()
		 * @generated
		 */
		EEnum SECURITY_LEVEL = eINSTANCE.getSecurityLevel();

		/**
		 * The meta object literal for the '{@link camel.organisation.ResourcePattern <em>Resource Pattern</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.organisation.ResourcePattern
		 * @see camel.organisation.impl.OrganisationPackageImpl#getResourcePattern()
		 * @generated
		 */
		EEnum RESOURCE_PATTERN = eINSTANCE.getResourcePattern();

	}

} //OrganisationPackage
