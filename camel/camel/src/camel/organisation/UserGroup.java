/**
 */
package camel.organisation;

import camel.core.Feature;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>User Group</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.UserGroup#getUsers <em>Users</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getUserGroup()
 * @model
 * @generated
 */
public interface UserGroup extends Feature {
	/**
	 * Returns the value of the '<em><b>Users</b></em>' reference list.
	 * The list contents are of type {@link camel.organisation.User}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Users</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Users</em>' reference list.
	 * @see camel.organisation.OrganisationPackage#getUserGroup_Users()
	 * @model required="true"
	 * @generated
	 */
	EList<User> getUsers();

} // UserGroup
