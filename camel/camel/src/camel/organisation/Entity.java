/**
 */
package camel.organisation;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Entity</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.organisation.OrganisationPackage#getEntity()
 * @model
 * @generated
 */
public interface Entity extends Feature {
} // Entity
