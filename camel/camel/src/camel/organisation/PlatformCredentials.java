/**
 */
package camel.organisation;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Platform Credentials</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.PlatformCredentials#getPassword <em>Password</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getPlatformCredentials()
 * @model
 * @generated
 */
public interface PlatformCredentials extends Credentials {
	/**
	 * Returns the value of the '<em><b>Password</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Password</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Password</em>' attribute.
	 * @see #setPassword(String)
	 * @see camel.organisation.OrganisationPackage#getPlatformCredentials_Password()
	 * @model required="true"
	 * @generated
	 */
	String getPassword();

	/**
	 * Sets the value of the '{@link camel.organisation.PlatformCredentials#getPassword <em>Password</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Password</em>' attribute.
	 * @see #getPassword()
	 * @generated
	 */
	void setPassword(String value);

} // PlatformCredentials
