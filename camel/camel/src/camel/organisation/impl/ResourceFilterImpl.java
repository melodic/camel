/**
 */
package camel.organisation.impl;

import camel.core.impl.FeatureImpl;

import camel.organisation.OrganisationPackage;
import camel.organisation.ResourceFilter;
import camel.organisation.ResourcePattern;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.impl.ResourceFilterImpl#getResourcePattern <em>Resource Pattern</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ResourceFilterImpl extends FeatureImpl implements ResourceFilter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganisationPackage.Literals.RESOURCE_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourcePattern getResourcePattern() {
		return (ResourcePattern)eGet(OrganisationPackage.Literals.RESOURCE_FILTER__RESOURCE_PATTERN, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setResourcePattern(ResourcePattern newResourcePattern) {
		eSet(OrganisationPackage.Literals.RESOURCE_FILTER__RESOURCE_PATTERN, newResourcePattern);
	}

} //ResourceFilterImpl
