/**
 */
package camel.organisation.impl;

import camel.organisation.OrganisationPackage;
import camel.organisation.PlatformCredentials;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Platform Credentials</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.impl.PlatformCredentialsImpl#getPassword <em>Password</em>}</li>
 * </ul>
 *
 * @generated
 */
public class PlatformCredentialsImpl extends CredentialsImpl implements PlatformCredentials {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PlatformCredentialsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganisationPackage.Literals.PLATFORM_CREDENTIALS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPassword() {
		return (String)eGet(OrganisationPackage.Literals.PLATFORM_CREDENTIALS__PASSWORD, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPassword(String newPassword) {
		eSet(OrganisationPackage.Literals.PLATFORM_CREDENTIALS__PASSWORD, newPassword);
	}

} //PlatformCredentialsImpl
