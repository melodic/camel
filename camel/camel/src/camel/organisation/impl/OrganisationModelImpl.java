/**
 */
package camel.organisation.impl;

import camel.core.impl.ModelImpl;
import camel.organisation.ExternalIdentifier;
import camel.organisation.Organisation;
import camel.organisation.OrganisationModel;
import camel.organisation.OrganisationPackage;
import camel.organisation.Permission;
import camel.organisation.ResourceFilter;
import camel.organisation.Role;
import camel.organisation.RoleAssignment;
import camel.organisation.SecurityLevel;
import camel.organisation.User;
import camel.organisation.UserGroup;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.impl.OrganisationModelImpl#getOrganisation <em>Organisation</em>}</li>
 *   <li>{@link camel.organisation.impl.OrganisationModelImpl#getExternalIdentifiers <em>External Identifiers</em>}</li>
 *   <li>{@link camel.organisation.impl.OrganisationModelImpl#getUsers <em>Users</em>}</li>
 *   <li>{@link camel.organisation.impl.OrganisationModelImpl#getUserGroups <em>User Groups</em>}</li>
 *   <li>{@link camel.organisation.impl.OrganisationModelImpl#getRoles <em>Roles</em>}</li>
 *   <li>{@link camel.organisation.impl.OrganisationModelImpl#getRoleAssigments <em>Role Assigments</em>}</li>
 *   <li>{@link camel.organisation.impl.OrganisationModelImpl#getPermissions <em>Permissions</em>}</li>
 *   <li>{@link camel.organisation.impl.OrganisationModelImpl#getSecurityLevel <em>Security Level</em>}</li>
 *   <li>{@link camel.organisation.impl.OrganisationModelImpl#getResourceFilters <em>Resource Filters</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OrganisationModelImpl extends ModelImpl implements OrganisationModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OrganisationModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganisationPackage.Literals.ORGANISATION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Organisation getOrganisation() {
		return (Organisation)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__ORGANISATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setOrganisation(Organisation newOrganisation) {
		eSet(OrganisationPackage.Literals.ORGANISATION_MODEL__ORGANISATION, newOrganisation);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ExternalIdentifier> getExternalIdentifiers() {
		return (EList<ExternalIdentifier>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__EXTERNAL_IDENTIFIERS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<User> getUsers() {
		return (EList<User>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__USERS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<UserGroup> getUserGroups() {
		return (EList<UserGroup>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__USER_GROUPS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Role> getRoles() {
		return (EList<Role>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__ROLES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RoleAssignment> getRoleAssigments() {
		return (EList<RoleAssignment>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__ROLE_ASSIGMENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Permission> getPermissions() {
		return (EList<Permission>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__PERMISSIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityLevel getSecurityLevel() {
		return (SecurityLevel)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__SECURITY_LEVEL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecurityLevel(SecurityLevel newSecurityLevel) {
		eSet(OrganisationPackage.Literals.ORGANISATION_MODEL__SECURITY_LEVEL, newSecurityLevel);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ResourceFilter> getResourceFilters() {
		return (EList<ResourceFilter>)eGet(OrganisationPackage.Literals.ORGANISATION_MODEL__RESOURCE_FILTERS, true);
	}

} //OrganisationModelImpl
