/**
 */
package camel.organisation.impl;

import camel.core.impl.FeatureImpl;

import camel.organisation.Credentials;
import camel.organisation.OrganisationPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Credentials</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public abstract class CredentialsImpl extends FeatureImpl implements Credentials {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CredentialsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganisationPackage.Literals.CREDENTIALS;
	}

} //CredentialsImpl
