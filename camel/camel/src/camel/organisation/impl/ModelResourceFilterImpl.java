/**
 */
package camel.organisation.impl;

import camel.organisation.ModelResourceFilter;
import camel.organisation.OrganisationPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model Resource Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.impl.ModelResourceFilterImpl#getInformationResourcePath <em>Information Resource Path</em>}</li>
 *   <li>{@link camel.organisation.impl.ModelResourceFilterImpl#isEveryInformationResource <em>Every Information Resource</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ModelResourceFilterImpl extends ResourceFilterImpl implements ModelResourceFilter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelResourceFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganisationPackage.Literals.MODEL_RESOURCE_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getInformationResourcePath() {
		return (String)eGet(OrganisationPackage.Literals.MODEL_RESOURCE_FILTER__INFORMATION_RESOURCE_PATH, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setInformationResourcePath(String newInformationResourcePath) {
		eSet(OrganisationPackage.Literals.MODEL_RESOURCE_FILTER__INFORMATION_RESOURCE_PATH, newInformationResourcePath);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEveryInformationResource() {
		return (Boolean)eGet(OrganisationPackage.Literals.MODEL_RESOURCE_FILTER__EVERY_INFORMATION_RESOURCE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEveryInformationResource(boolean newEveryInformationResource) {
		eSet(OrganisationPackage.Literals.MODEL_RESOURCE_FILTER__EVERY_INFORMATION_RESOURCE, newEveryInformationResource);
	}

} //ModelResourceFilterImpl
