/**
 */
package camel.organisation.impl;

import camel.organisation.CloudProvider;
import camel.organisation.OrganisationPackage;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Cloud Provider</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.impl.CloudProviderImpl#isPublic <em>Public</em>}</li>
 *   <li>{@link camel.organisation.impl.CloudProviderImpl#isSaaS <em>Saa S</em>}</li>
 *   <li>{@link camel.organisation.impl.CloudProviderImpl#isPaaS <em>Paa S</em>}</li>
 *   <li>{@link camel.organisation.impl.CloudProviderImpl#isIaaS <em>Iaa S</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CloudProviderImpl extends OrganisationImpl implements CloudProvider {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CloudProviderImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganisationPackage.Literals.CLOUD_PROVIDER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPublic() {
		return (Boolean)eGet(OrganisationPackage.Literals.CLOUD_PROVIDER__PUBLIC, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPublic(boolean newPublic) {
		eSet(OrganisationPackage.Literals.CLOUD_PROVIDER__PUBLIC, newPublic);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isSaaS() {
		return (Boolean)eGet(OrganisationPackage.Literals.CLOUD_PROVIDER__SAA_S, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setSaaS(boolean newSaaS) {
		eSet(OrganisationPackage.Literals.CLOUD_PROVIDER__SAA_S, newSaaS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isPaaS() {
		return (Boolean)eGet(OrganisationPackage.Literals.CLOUD_PROVIDER__PAA_S, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPaaS(boolean newPaaS) {
		eSet(OrganisationPackage.Literals.CLOUD_PROVIDER__PAA_S, newPaaS);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isIaaS() {
		return (Boolean)eGet(OrganisationPackage.Literals.CLOUD_PROVIDER__IAA_S, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setIaaS(boolean newIaaS) {
		eSet(OrganisationPackage.Literals.CLOUD_PROVIDER__IAA_S, newIaaS);
	}

} //CloudProviderImpl
