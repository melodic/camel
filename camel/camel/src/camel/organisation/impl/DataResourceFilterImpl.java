/**
 */
package camel.organisation.impl;

import camel.data.Data;
import camel.data.DataInstance;

import camel.location.Location;

import camel.organisation.DataResourceFilter;
import camel.organisation.OrganisationPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Data Resource Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.impl.DataResourceFilterImpl#getData <em>Data</em>}</li>
 *   <li>{@link camel.organisation.impl.DataResourceFilterImpl#getDataInstances <em>Data Instances</em>}</li>
 *   <li>{@link camel.organisation.impl.DataResourceFilterImpl#getLocations <em>Locations</em>}</li>
 *   <li>{@link camel.organisation.impl.DataResourceFilterImpl#isEveryData <em>Every Data</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DataResourceFilterImpl extends ResourceFilterImpl implements DataResourceFilter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DataResourceFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganisationPackage.Literals.DATA_RESOURCE_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Data> getData() {
		return (EList<Data>)eGet(OrganisationPackage.Literals.DATA_RESOURCE_FILTER__DATA, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DataInstance> getDataInstances() {
		return (EList<DataInstance>)eGet(OrganisationPackage.Literals.DATA_RESOURCE_FILTER__DATA_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Location> getLocations() {
		return (EList<Location>)eGet(OrganisationPackage.Literals.DATA_RESOURCE_FILTER__LOCATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEveryData() {
		return (Boolean)eGet(OrganisationPackage.Literals.DATA_RESOURCE_FILTER__EVERY_DATA, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEveryData(boolean newEveryData) {
		eSet(OrganisationPackage.Literals.DATA_RESOURCE_FILTER__EVERY_DATA, newEveryData);
	}

} //DataResourceFilterImpl
