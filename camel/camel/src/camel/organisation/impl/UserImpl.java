/**
 */
package camel.organisation.impl;

import camel.deployment.DeploymentModel;

import camel.metric.MetricModel;
import camel.organisation.ExternalIdentifier;
import camel.organisation.OrganisationPackage;
import camel.organisation.PlatformCredentials;
import camel.organisation.User;

import camel.requirement.RequirementModel;

import camel.scalability.ScalabilityModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>User</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.impl.UserImpl#getEmail <em>Email</em>}</li>
 *   <li>{@link camel.organisation.impl.UserImpl#getFirstName <em>First Name</em>}</li>
 *   <li>{@link camel.organisation.impl.UserImpl#getLastName <em>Last Name</em>}</li>
 *   <li>{@link camel.organisation.impl.UserImpl#getWww <em>Www</em>}</li>
 *   <li>{@link camel.organisation.impl.UserImpl#getExternalIdentifiers <em>External Identifiers</em>}</li>
 *   <li>{@link camel.organisation.impl.UserImpl#getPlatformCredentials <em>Platform Credentials</em>}</li>
 *   <li>{@link camel.organisation.impl.UserImpl#getRequirementModels <em>Requirement Models</em>}</li>
 *   <li>{@link camel.organisation.impl.UserImpl#getDeploymentModels <em>Deployment Models</em>}</li>
 *   <li>{@link camel.organisation.impl.UserImpl#getMetricModels <em>Metric Models</em>}</li>
 *   <li>{@link camel.organisation.impl.UserImpl#getScalabilityModels <em>Scalability Models</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UserImpl extends EntityImpl implements User {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UserImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganisationPackage.Literals.USER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEmail() {
		return (String)eGet(OrganisationPackage.Literals.USER__EMAIL, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEmail(String newEmail) {
		eSet(OrganisationPackage.Literals.USER__EMAIL, newEmail);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFirstName() {
		return (String)eGet(OrganisationPackage.Literals.USER__FIRST_NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstName(String newFirstName) {
		eSet(OrganisationPackage.Literals.USER__FIRST_NAME, newFirstName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getLastName() {
		return (String)eGet(OrganisationPackage.Literals.USER__LAST_NAME, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLastName(String newLastName) {
		eSet(OrganisationPackage.Literals.USER__LAST_NAME, newLastName);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getWww() {
		return (String)eGet(OrganisationPackage.Literals.USER__WWW, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setWww(String newWww) {
		eSet(OrganisationPackage.Literals.USER__WWW, newWww);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ExternalIdentifier> getExternalIdentifiers() {
		return (EList<ExternalIdentifier>)eGet(OrganisationPackage.Literals.USER__EXTERNAL_IDENTIFIERS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RequirementModel> getRequirementModels() {
		return (EList<RequirementModel>)eGet(OrganisationPackage.Literals.USER__REQUIREMENT_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DeploymentModel> getDeploymentModels() {
		return (EList<DeploymentModel>)eGet(OrganisationPackage.Literals.USER__DEPLOYMENT_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PlatformCredentials getPlatformCredentials() {
		return (PlatformCredentials)eGet(OrganisationPackage.Literals.USER__PLATFORM_CREDENTIALS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPlatformCredentials(PlatformCredentials newPlatformCredentials) {
		eSet(OrganisationPackage.Literals.USER__PLATFORM_CREDENTIALS, newPlatformCredentials);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MetricModel> getMetricModels() {
		return (EList<MetricModel>)eGet(OrganisationPackage.Literals.USER__METRIC_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ScalabilityModel> getScalabilityModels() {
		return (EList<ScalabilityModel>)eGet(OrganisationPackage.Literals.USER__SCALABILITY_MODELS, true);
	}

} //UserImpl
