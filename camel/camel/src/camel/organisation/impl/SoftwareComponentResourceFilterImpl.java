/**
 */
package camel.organisation.impl;

import camel.core.Application;

import camel.deployment.SoftwareComponent;

import camel.organisation.OrganisationPackage;
import camel.organisation.SoftwareComponentResourceFilter;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Software Component Resource Filter</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.impl.SoftwareComponentResourceFilterImpl#isEveryComponent <em>Every Component</em>}</li>
 *   <li>{@link camel.organisation.impl.SoftwareComponentResourceFilterImpl#getApplication <em>Application</em>}</li>
 *   <li>{@link camel.organisation.impl.SoftwareComponentResourceFilterImpl#getSoftwareComponents <em>Software Components</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SoftwareComponentResourceFilterImpl extends ResourceFilterImpl implements SoftwareComponentResourceFilter {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SoftwareComponentResourceFilterImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return OrganisationPackage.Literals.SOFTWARE_COMPONENT_RESOURCE_FILTER;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isEveryComponent() {
		return (Boolean)eGet(OrganisationPackage.Literals.SOFTWARE_COMPONENT_RESOURCE_FILTER__EVERY_COMPONENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEveryComponent(boolean newEveryComponent) {
		eSet(OrganisationPackage.Literals.SOFTWARE_COMPONENT_RESOURCE_FILTER__EVERY_COMPONENT, newEveryComponent);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Application getApplication() {
		return (Application)eGet(OrganisationPackage.Literals.SOFTWARE_COMPONENT_RESOURCE_FILTER__APPLICATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplication(Application newApplication) {
		eSet(OrganisationPackage.Literals.SOFTWARE_COMPONENT_RESOURCE_FILTER__APPLICATION, newApplication);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SoftwareComponent> getSoftwareComponents() {
		return (EList<SoftwareComponent>)eGet(OrganisationPackage.Literals.SOFTWARE_COMPONENT_RESOURCE_FILTER__SOFTWARE_COMPONENTS, true);
	}

} //SoftwareComponentResourceFilterImpl
