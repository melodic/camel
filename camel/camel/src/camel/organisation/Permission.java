/**
 */
package camel.organisation;

import camel.core.Feature;

import java.util.Date;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Permission</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.Permission#getRole <em>Role</em>}</li>
 *   <li>{@link camel.organisation.Permission#getStartTime <em>Start Time</em>}</li>
 *   <li>{@link camel.organisation.Permission#getEndTime <em>End Time</em>}</li>
 *   <li>{@link camel.organisation.Permission#getResourceFilter <em>Resource Filter</em>}</li>
 *   <li>{@link camel.organisation.Permission#getAction <em>Action</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getPermission()
 * @model
 * @generated
 */
public interface Permission extends Feature {
	/**
	 * Returns the value of the '<em><b>Role</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Role</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Role</em>' reference.
	 * @see #setRole(Role)
	 * @see camel.organisation.OrganisationPackage#getPermission_Role()
	 * @model required="true"
	 * @generated
	 */
	Role getRole();

	/**
	 * Sets the value of the '{@link camel.organisation.Permission#getRole <em>Role</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Role</em>' reference.
	 * @see #getRole()
	 * @generated
	 */
	void setRole(Role value);

	/**
	 * Returns the value of the '<em><b>Start Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Start Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Start Time</em>' attribute.
	 * @see #setStartTime(Date)
	 * @see camel.organisation.OrganisationPackage#getPermission_StartTime()
	 * @model required="true"
	 * @generated
	 */
	Date getStartTime();

	/**
	 * Sets the value of the '{@link camel.organisation.Permission#getStartTime <em>Start Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Start Time</em>' attribute.
	 * @see #getStartTime()
	 * @generated
	 */
	void setStartTime(Date value);

	/**
	 * Returns the value of the '<em><b>End Time</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>End Time</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>End Time</em>' attribute.
	 * @see #setEndTime(Date)
	 * @see camel.organisation.OrganisationPackage#getPermission_EndTime()
	 * @model
	 * @generated
	 */
	Date getEndTime();

	/**
	 * Sets the value of the '{@link camel.organisation.Permission#getEndTime <em>End Time</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>End Time</em>' attribute.
	 * @see #getEndTime()
	 * @generated
	 */
	void setEndTime(Date value);

	/**
	 * Returns the value of the '<em><b>Resource Filter</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Resource Filter</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Resource Filter</em>' reference.
	 * @see #setResourceFilter(ResourceFilter)
	 * @see camel.organisation.OrganisationPackage#getPermission_ResourceFilter()
	 * @model required="true"
	 * @generated
	 */
	ResourceFilter getResourceFilter();

	/**
	 * Sets the value of the '{@link camel.organisation.Permission#getResourceFilter <em>Resource Filter</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Resource Filter</em>' reference.
	 * @see #getResourceFilter()
	 * @generated
	 */
	void setResourceFilter(ResourceFilter value);

	/**
	 * Returns the value of the '<em><b>Action</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.organisation.PermissionActionType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Action</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Action</em>' attribute.
	 * @see camel.organisation.PermissionActionType
	 * @see #setAction(PermissionActionType)
	 * @see camel.organisation.OrganisationPackage#getPermission_Action()
	 * @model required="true"
	 * @generated
	 */
	PermissionActionType getAction();

	/**
	 * Sets the value of the '{@link camel.organisation.Permission#getAction <em>Action</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Action</em>' attribute.
	 * @see camel.organisation.PermissionActionType
	 * @see #getAction()
	 * @generated
	 */
	void setAction(PermissionActionType value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" thisRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/GenModel body='System.out.println(\"CHECKING Permission_Start_Before_End: \" + this + \" \" + this.getStartTime() + \" \" + this.getEndTime()); java.util.Date date1 = this.getStartTime(); java.util.Date date2 = this.getEndTime(); if (date1 == null || date2 == null || (date1 != null &amp;&amp; date2 != null &amp;&amp; date1.before(date2))) return Boolean.TRUE; return Boolean.FALSE;'"
	 * @generated
	 */
	boolean checkStartEndDates(Permission this_);

} // Permission
