/**
 */
package camel.organisation;

import camel.core.Application;

import camel.deployment.SoftwareComponent;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Software Component Resource Filter</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.SoftwareComponentResourceFilter#isEveryComponent <em>Every Component</em>}</li>
 *   <li>{@link camel.organisation.SoftwareComponentResourceFilter#getApplication <em>Application</em>}</li>
 *   <li>{@link camel.organisation.SoftwareComponentResourceFilter#getSoftwareComponents <em>Software Components</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getSoftwareComponentResourceFilter()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='component_resource_filter_validity'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot component_resource_filter_validity='Tuple {\n\tmessage : String = \'It is meaningless for SWComponentResourceFilter: \' + self.name + \' to apply for any sw component resource and to specify exactly what are these resources\',\n\tstatus : Boolean = \n\t\t\t\tasError(everyComponent = true implies softwareComponents-&gt;size() = 0)\n}.status'"
 * @generated
 */
public interface SoftwareComponentResourceFilter extends ResourceFilter {
	/**
	 * Returns the value of the '<em><b>Every Component</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Every Component</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Every Component</em>' attribute.
	 * @see #setEveryComponent(boolean)
	 * @see camel.organisation.OrganisationPackage#getSoftwareComponentResourceFilter_EveryComponent()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isEveryComponent();

	/**
	 * Sets the value of the '{@link camel.organisation.SoftwareComponentResourceFilter#isEveryComponent <em>Every Component</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Every Component</em>' attribute.
	 * @see #isEveryComponent()
	 * @generated
	 */
	void setEveryComponent(boolean value);

	/**
	 * Returns the value of the '<em><b>Application</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application</em>' reference.
	 * @see #setApplication(Application)
	 * @see camel.organisation.OrganisationPackage#getSoftwareComponentResourceFilter_Application()
	 * @model
	 * @generated
	 */
	Application getApplication();

	/**
	 * Sets the value of the '{@link camel.organisation.SoftwareComponentResourceFilter#getApplication <em>Application</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Application</em>' reference.
	 * @see #getApplication()
	 * @generated
	 */
	void setApplication(Application value);

	/**
	 * Returns the value of the '<em><b>Software Components</b></em>' reference list.
	 * The list contents are of type {@link camel.deployment.SoftwareComponent}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Software Components</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Software Components</em>' reference list.
	 * @see camel.organisation.OrganisationPackage#getSoftwareComponentResourceFilter_SoftwareComponents()
	 * @model
	 * @generated
	 */
	EList<SoftwareComponent> getSoftwareComponents();

} // SoftwareComponentResourceFilter
