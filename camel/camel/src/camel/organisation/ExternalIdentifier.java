/**
 */
package camel.organisation;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>External Identifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.organisation.ExternalIdentifier#getIdentifier <em>Identifier</em>}</li>
 * </ul>
 *
 * @see camel.organisation.OrganisationPackage#getExternalIdentifier()
 * @model
 * @generated
 */
public interface ExternalIdentifier extends Feature {
	/**
	 * Returns the value of the '<em><b>Identifier</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Identifier</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Identifier</em>' attribute.
	 * @see #setIdentifier(String)
	 * @see camel.organisation.OrganisationPackage#getExternalIdentifier_Identifier()
	 * @model required="true"
	 * @generated
	 */
	String getIdentifier();

	/**
	 * Sets the value of the '{@link camel.organisation.ExternalIdentifier#getIdentifier <em>Identifier</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Identifier</em>' attribute.
	 * @see #getIdentifier()
	 * @generated
	 */
	void setIdentifier(String value);

} // ExternalIdentifier
