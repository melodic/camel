/**
 */
package camel.organisation;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Role</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.organisation.OrganisationPackage#getRole()
 * @model
 * @generated
 */
public interface Role extends Feature {
} // Role
