/**
 */
package camel.organisation.util;

import camel.core.Feature;
import camel.core.Model;
import camel.core.NamedElement;

import camel.organisation.*;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.Switch;

/**
 * <!-- begin-user-doc -->
 * The <b>Switch</b> for the model's inheritance hierarchy.
 * It supports the call {@link #doSwitch(EObject) doSwitch(object)}
 * to invoke the <code>caseXXX</code> method for each class of the model,
 * starting with the actual class of the object
 * and proceeding up the inheritance hierarchy
 * until a non-null result is returned,
 * which is the result of the switch.
 * <!-- end-user-doc -->
 * @see camel.organisation.OrganisationPackage
 * @generated
 */
public class OrganisationSwitch<T> extends Switch<T> {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static OrganisationPackage modelPackage;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganisationSwitch() {
		if (modelPackage == null) {
			modelPackage = OrganisationPackage.eINSTANCE;
		}
	}

	/**
	 * Checks whether this is a switch for the given package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param ePackage the package in question.
	 * @return whether this is a switch for the given package.
	 * @generated
	 */
	@Override
	protected boolean isSwitchFor(EPackage ePackage) {
		return ePackage == modelPackage;
	}

	/**
	 * Calls <code>caseXXX</code> for each class of the model until one returns a non null result; it yields that result.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the first non-null result returned by a <code>caseXXX</code> call.
	 * @generated
	 */
	@Override
	protected T doSwitch(int classifierID, EObject theEObject) {
		switch (classifierID) {
			case OrganisationPackage.ORGANISATION_MODEL: {
				OrganisationModel organisationModel = (OrganisationModel)theEObject;
				T result = caseOrganisationModel(organisationModel);
				if (result == null) result = caseModel(organisationModel);
				if (result == null) result = caseFeature(organisationModel);
				if (result == null) result = caseNamedElement(organisationModel);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.CREDENTIALS: {
				Credentials credentials = (Credentials)theEObject;
				T result = caseCredentials(credentials);
				if (result == null) result = caseFeature(credentials);
				if (result == null) result = caseNamedElement(credentials);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.CLOUD_CREDENTIALS: {
				CloudCredentials cloudCredentials = (CloudCredentials)theEObject;
				T result = caseCloudCredentials(cloudCredentials);
				if (result == null) result = caseCredentials(cloudCredentials);
				if (result == null) result = caseFeature(cloudCredentials);
				if (result == null) result = caseNamedElement(cloudCredentials);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.ENTITY: {
				Entity entity = (Entity)theEObject;
				T result = caseEntity(entity);
				if (result == null) result = caseFeature(entity);
				if (result == null) result = caseNamedElement(entity);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.ORGANISATION: {
				Organisation organisation = (Organisation)theEObject;
				T result = caseOrganisation(organisation);
				if (result == null) result = caseEntity(organisation);
				if (result == null) result = caseFeature(organisation);
				if (result == null) result = caseNamedElement(organisation);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.CLOUD_PROVIDER: {
				CloudProvider cloudProvider = (CloudProvider)theEObject;
				T result = caseCloudProvider(cloudProvider);
				if (result == null) result = caseOrganisation(cloudProvider);
				if (result == null) result = caseEntity(cloudProvider);
				if (result == null) result = caseFeature(cloudProvider);
				if (result == null) result = caseNamedElement(cloudProvider);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.USER: {
				User user = (User)theEObject;
				T result = caseUser(user);
				if (result == null) result = caseEntity(user);
				if (result == null) result = caseFeature(user);
				if (result == null) result = caseNamedElement(user);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.EXTERNAL_IDENTIFIER: {
				ExternalIdentifier externalIdentifier = (ExternalIdentifier)theEObject;
				T result = caseExternalIdentifier(externalIdentifier);
				if (result == null) result = caseFeature(externalIdentifier);
				if (result == null) result = caseNamedElement(externalIdentifier);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.PERMISSION: {
				Permission permission = (Permission)theEObject;
				T result = casePermission(permission);
				if (result == null) result = caseFeature(permission);
				if (result == null) result = caseNamedElement(permission);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.RESOURCE_FILTER: {
				ResourceFilter resourceFilter = (ResourceFilter)theEObject;
				T result = caseResourceFilter(resourceFilter);
				if (result == null) result = caseFeature(resourceFilter);
				if (result == null) result = caseNamedElement(resourceFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.MODEL_RESOURCE_FILTER: {
				ModelResourceFilter modelResourceFilter = (ModelResourceFilter)theEObject;
				T result = caseModelResourceFilter(modelResourceFilter);
				if (result == null) result = caseResourceFilter(modelResourceFilter);
				if (result == null) result = caseFeature(modelResourceFilter);
				if (result == null) result = caseNamedElement(modelResourceFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.SERVICE_RESOURCE_FILTER: {
				ServiceResourceFilter serviceResourceFilter = (ServiceResourceFilter)theEObject;
				T result = caseServiceResourceFilter(serviceResourceFilter);
				if (result == null) result = caseResourceFilter(serviceResourceFilter);
				if (result == null) result = caseFeature(serviceResourceFilter);
				if (result == null) result = caseNamedElement(serviceResourceFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.ROLE: {
				Role role = (Role)theEObject;
				T result = caseRole(role);
				if (result == null) result = caseFeature(role);
				if (result == null) result = caseNamedElement(role);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.ROLE_ASSIGNMENT: {
				RoleAssignment roleAssignment = (RoleAssignment)theEObject;
				T result = caseRoleAssignment(roleAssignment);
				if (result == null) result = caseFeature(roleAssignment);
				if (result == null) result = caseNamedElement(roleAssignment);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.USER_GROUP: {
				UserGroup userGroup = (UserGroup)theEObject;
				T result = caseUserGroup(userGroup);
				if (result == null) result = caseFeature(userGroup);
				if (result == null) result = caseNamedElement(userGroup);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.PLATFORM_CREDENTIALS: {
				PlatformCredentials platformCredentials = (PlatformCredentials)theEObject;
				T result = casePlatformCredentials(platformCredentials);
				if (result == null) result = caseCredentials(platformCredentials);
				if (result == null) result = caseFeature(platformCredentials);
				if (result == null) result = caseNamedElement(platformCredentials);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.DATA_RESOURCE_FILTER: {
				DataResourceFilter dataResourceFilter = (DataResourceFilter)theEObject;
				T result = caseDataResourceFilter(dataResourceFilter);
				if (result == null) result = caseResourceFilter(dataResourceFilter);
				if (result == null) result = caseFeature(dataResourceFilter);
				if (result == null) result = caseNamedElement(dataResourceFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			case OrganisationPackage.SOFTWARE_COMPONENT_RESOURCE_FILTER: {
				SoftwareComponentResourceFilter softwareComponentResourceFilter = (SoftwareComponentResourceFilter)theEObject;
				T result = caseSoftwareComponentResourceFilter(softwareComponentResourceFilter);
				if (result == null) result = caseResourceFilter(softwareComponentResourceFilter);
				if (result == null) result = caseFeature(softwareComponentResourceFilter);
				if (result == null) result = caseNamedElement(softwareComponentResourceFilter);
				if (result == null) result = defaultCase(theEObject);
				return result;
			}
			default: return defaultCase(theEObject);
		}
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrganisationModel(OrganisationModel object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Credentials</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Credentials</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCredentials(Credentials object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cloud Credentials</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cloud Credentials</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCloudCredentials(CloudCredentials object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Entity</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseEntity(Entity object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Organisation</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Organisation</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseOrganisation(Organisation object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Cloud Provider</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Cloud Provider</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseCloudProvider(CloudProvider object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUser(User object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>External Identifier</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>External Identifier</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseExternalIdentifier(ExternalIdentifier object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Permission</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Permission</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePermission(Permission object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Resource Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseResourceFilter(ResourceFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model Resource Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModelResourceFilter(ModelResourceFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Service Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Service Resource Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseServiceResourceFilter(ServiceResourceFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRole(Role object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Role Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Role Assignment</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseRoleAssignment(RoleAssignment object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>User Group</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>User Group</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseUserGroup(UserGroup object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Platform Credentials</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Platform Credentials</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T casePlatformCredentials(PlatformCredentials object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Data Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Data Resource Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseDataResourceFilter(DataResourceFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Software Component Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Software Component Resource Filter</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseSoftwareComponentResourceFilter(SoftwareComponentResourceFilter object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Named Element</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseNamedElement(NamedElement object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Feature</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Feature</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseFeature(Feature object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>Model</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject) doSwitch(EObject)
	 * @generated
	 */
	public T caseModel(Model object) {
		return null;
	}

	/**
	 * Returns the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * <!-- begin-user-doc -->
	 * This implementation returns null;
	 * returning a non-null result will terminate the switch, but this is the last case anyway.
	 * <!-- end-user-doc -->
	 * @param object the target of the switch.
	 * @return the result of interpreting the object as an instance of '<em>EObject</em>'.
	 * @see #doSwitch(org.eclipse.emf.ecore.EObject)
	 * @generated
	 */
	@Override
	public T defaultCase(EObject object) {
		return null;
	}

} //OrganisationSwitch
