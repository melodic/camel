/**
 */
package camel.organisation.util;

import camel.organisation.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.organisation.OrganisationPackage
 * @generated
 */
public class OrganisationValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final OrganisationValidator INSTANCE = new OrganisationValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.organisation";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OrganisationValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return OrganisationPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case OrganisationPackage.ORGANISATION_MODEL:
				return validateOrganisationModel((OrganisationModel)value, diagnostics, context);
			case OrganisationPackage.CREDENTIALS:
				return validateCredentials((Credentials)value, diagnostics, context);
			case OrganisationPackage.CLOUD_CREDENTIALS:
				return validateCloudCredentials((CloudCredentials)value, diagnostics, context);
			case OrganisationPackage.ENTITY:
				return validateEntity((Entity)value, diagnostics, context);
			case OrganisationPackage.ORGANISATION:
				return validateOrganisation((Organisation)value, diagnostics, context);
			case OrganisationPackage.CLOUD_PROVIDER:
				return validateCloudProvider((CloudProvider)value, diagnostics, context);
			case OrganisationPackage.USER:
				return validateUser((User)value, diagnostics, context);
			case OrganisationPackage.EXTERNAL_IDENTIFIER:
				return validateExternalIdentifier((ExternalIdentifier)value, diagnostics, context);
			case OrganisationPackage.PERMISSION:
				return validatePermission((Permission)value, diagnostics, context);
			case OrganisationPackage.RESOURCE_FILTER:
				return validateResourceFilter((ResourceFilter)value, diagnostics, context);
			case OrganisationPackage.MODEL_RESOURCE_FILTER:
				return validateModelResourceFilter((ModelResourceFilter)value, diagnostics, context);
			case OrganisationPackage.SERVICE_RESOURCE_FILTER:
				return validateServiceResourceFilter((ServiceResourceFilter)value, diagnostics, context);
			case OrganisationPackage.ROLE:
				return validateRole((Role)value, diagnostics, context);
			case OrganisationPackage.ROLE_ASSIGNMENT:
				return validateRoleAssignment((RoleAssignment)value, diagnostics, context);
			case OrganisationPackage.USER_GROUP:
				return validateUserGroup((UserGroup)value, diagnostics, context);
			case OrganisationPackage.PLATFORM_CREDENTIALS:
				return validatePlatformCredentials((PlatformCredentials)value, diagnostics, context);
			case OrganisationPackage.DATA_RESOURCE_FILTER:
				return validateDataResourceFilter((DataResourceFilter)value, diagnostics, context);
			case OrganisationPackage.SOFTWARE_COMPONENT_RESOURCE_FILTER:
				return validateSoftwareComponentResourceFilter((SoftwareComponentResourceFilter)value, diagnostics, context);
			case OrganisationPackage.PERMISSION_ACTION_TYPE:
				return validatePermissionActionType((PermissionActionType)value, diagnostics, context);
			case OrganisationPackage.SECURITY_LEVEL:
				return validateSecurityLevel((SecurityLevel)value, diagnostics, context);
			case OrganisationPackage.RESOURCE_PATTERN:
				return validateResourcePattern((ResourcePattern)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOrganisationModel(OrganisationModel organisationModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)organisationModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCredentials(Credentials credentials, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)credentials, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCloudCredentials(CloudCredentials cloudCredentials, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)cloudCredentials, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateEntity(Entity entity, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)entity, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOrganisation(Organisation organisation, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)organisation, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCloudProvider(CloudProvider cloudProvider, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)cloudProvider, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUser(User user, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)user, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateExternalIdentifier(ExternalIdentifier externalIdentifier, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)externalIdentifier, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePermission(Permission permission, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)permission, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceFilter(ResourceFilter resourceFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)resourceFilter, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateModelResourceFilter(ModelResourceFilter modelResourceFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)modelResourceFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)modelResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)modelResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)modelResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)modelResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)modelResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)modelResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)modelResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)modelResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateModelResourceFilter_information_resource_filter_validity(modelResourceFilter, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the information_resource_filter_validity constraint of '<em>Model Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String MODEL_RESOURCE_FILTER__INFORMATION_RESOURCE_FILTER_VALIDITY__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'It is meaningless for InformationResourceFilter: ' + self.name + ' to apply for any model resource and to specify a path for these resources',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(everyInformationResource = true implies\n" +
		"\t\t\t\t\tinformationResourcePath = null)\n" +
		"}.status";

	/**
	 * Validates the information_resource_filter_validity constraint of '<em>Model Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateModelResourceFilter_information_resource_filter_validity(ModelResourceFilter modelResourceFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(OrganisationPackage.Literals.MODEL_RESOURCE_FILTER,
				 (EObject)modelResourceFilter,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "information_resource_filter_validity",
				 MODEL_RESOURCE_FILTER__INFORMATION_RESOURCE_FILTER_VALIDITY__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateServiceResourceFilter(ServiceResourceFilter serviceResourceFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)serviceResourceFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)serviceResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)serviceResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)serviceResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)serviceResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)serviceResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)serviceResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)serviceResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)serviceResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateServiceResourceFilter_service_resource_filter_validity(serviceResourceFilter, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the service_resource_filter_validity constraint of '<em>Service Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SERVICE_RESOURCE_FILTER__SERVICE_RESOURCE_FILTER_VALIDITY__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'It is meaningless for ServiceResourceFilter: ' + self.name + ' to apply for any service resource and to specify a URL for these resources',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(everyService = true implies serviceURL = null)\n" +
		"}.status";

	/**
	 * Validates the service_resource_filter_validity constraint of '<em>Service Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateServiceResourceFilter_service_resource_filter_validity(ServiceResourceFilter serviceResourceFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(OrganisationPackage.Literals.SERVICE_RESOURCE_FILTER,
				 (EObject)serviceResourceFilter,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "service_resource_filter_validity",
				 SERVICE_RESOURCE_FILTER__SERVICE_RESOURCE_FILTER_VALIDITY__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRole(Role role, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)role, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRoleAssignment(RoleAssignment roleAssignment, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)roleAssignment, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)roleAssignment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)roleAssignment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)roleAssignment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)roleAssignment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)roleAssignment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)roleAssignment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)roleAssignment, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)roleAssignment, diagnostics, context);
		if (result || diagnostics != null) result &= validateRoleAssignment_role_assignment_at_least_user_or_group(roleAssignment, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the role_assignment_at_least_user_or_group constraint of '<em>Role Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ROLE_ASSIGNMENT__ROLE_ASSIGNMENT_AT_LEAST_USER_OR_GROUP__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'There is no user or user group associated to RoleAssigment:' + self.name,\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.user <> null or self.userGroup <> null)\n" +
		"}.status";

	/**
	 * Validates the role_assignment_at_least_user_or_group constraint of '<em>Role Assignment</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRoleAssignment_role_assignment_at_least_user_or_group(RoleAssignment roleAssignment, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(OrganisationPackage.Literals.ROLE_ASSIGNMENT,
				 (EObject)roleAssignment,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "role_assignment_at_least_user_or_group",
				 ROLE_ASSIGNMENT__ROLE_ASSIGNMENT_AT_LEAST_USER_OR_GROUP__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUserGroup(UserGroup userGroup, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)userGroup, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePlatformCredentials(PlatformCredentials platformCredentials, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)platformCredentials, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataResourceFilter(DataResourceFilter dataResourceFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)dataResourceFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)dataResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)dataResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)dataResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)dataResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)dataResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)dataResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)dataResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)dataResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateDataResourceFilter_data_resource_filter_validity(dataResourceFilter, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the data_resource_filter_validity constraint of '<em>Data Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String DATA_RESOURCE_FILTER__DATA_RESOURCE_FILTER_VALIDITY__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'It is meaningless for DataResourceFilter: ' + self.name + ' to apply for any data resource and to specify that it holds for specific data resources',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(everyData = true implies (data->size() = 0 and dataInstances->size() = 0))\n" +
		"}.status";

	/**
	 * Validates the data_resource_filter_validity constraint of '<em>Data Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDataResourceFilter_data_resource_filter_validity(DataResourceFilter dataResourceFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(OrganisationPackage.Literals.DATA_RESOURCE_FILTER,
				 (EObject)dataResourceFilter,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "data_resource_filter_validity",
				 DATA_RESOURCE_FILTER__DATA_RESOURCE_FILTER_VALIDITY__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoftwareComponentResourceFilter(SoftwareComponentResourceFilter softwareComponentResourceFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)softwareComponentResourceFilter, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)softwareComponentResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)softwareComponentResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)softwareComponentResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)softwareComponentResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)softwareComponentResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)softwareComponentResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)softwareComponentResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)softwareComponentResourceFilter, diagnostics, context);
		if (result || diagnostics != null) result &= validateSoftwareComponentResourceFilter_component_resource_filter_validity(softwareComponentResourceFilter, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the component_resource_filter_validity constraint of '<em>Software Component Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SOFTWARE_COMPONENT_RESOURCE_FILTER__COMPONENT_RESOURCE_FILTER_VALIDITY__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'It is meaningless for SWComponentResourceFilter: ' + self.name + ' to apply for any sw component resource and to specify exactly what are these resources',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(everyComponent = true implies softwareComponents->size() = 0)\n" +
		"}.status";

	/**
	 * Validates the component_resource_filter_validity constraint of '<em>Software Component Resource Filter</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoftwareComponentResourceFilter_component_resource_filter_validity(SoftwareComponentResourceFilter softwareComponentResourceFilter, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(OrganisationPackage.Literals.SOFTWARE_COMPONENT_RESOURCE_FILTER,
				 (EObject)softwareComponentResourceFilter,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "component_resource_filter_validity",
				 SOFTWARE_COMPONENT_RESOURCE_FILTER__COMPONENT_RESOURCE_FILTER_VALIDITY__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePermissionActionType(PermissionActionType permissionActionType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecurityLevel(SecurityLevel securityLevel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourcePattern(ResourcePattern resourcePattern, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //OrganisationValidator
