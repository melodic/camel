/**
 */
package camel.organisation;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Credentials</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.organisation.OrganisationPackage#getCredentials()
 * @model abstract="true"
 * @generated
 */
public interface Credentials extends Feature {
} // Credentials
