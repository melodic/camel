/**
 */
package camel.security;

import camel.core.Feature;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Control</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.security.SecurityControl#getId <em>Id</em>}</li>
 *   <li>{@link camel.security.SecurityControl#getDomain <em>Domain</em>}</li>
 *   <li>{@link camel.security.SecurityControl#getSubDomain <em>Sub Domain</em>}</li>
 *   <li>{@link camel.security.SecurityControl#getSpecification <em>Specification</em>}</li>
 *   <li>{@link camel.security.SecurityControl#getSecurityProperties <em>Security Properties</em>}</li>
 *   <li>{@link camel.security.SecurityControl#getRawSecurityMetrics <em>Raw Security Metrics</em>}</li>
 *   <li>{@link camel.security.SecurityControl#getCompositeSecurityMetrics <em>Composite Security Metrics</em>}</li>
 * </ul>
 *
 * @see camel.security.SecurityPackage#getSecurityControl()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='security_control_domain_subdomain_diff security_control_all_elems_align_domain'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot security_control_domain_subdomain_diff='Tuple {\n\tmessage : String = \'In SecurityControl: \' + self.name +\' the domain cannot be the same as the sub-domain\',\n\tstatus : Boolean = \n\t\t\t\tasError(domain &lt;&gt; subDomain)\n}.status' security_control_all_elems_align_domain='Tuple {\n\tmessage : String = \'SecurityControl: \' + self.name + \' should have all properties or metrics referenced belonging to the same domain or sub-domain\',\n\tstatus : Boolean = \n\t\t\t\tasError(securityProperties-&gt;forAll(p| p.domain = self.domain or p.domain = self.subDomain) and\n\t\t\t\trawSecurityMetrics-&gt;forAll(p| p.domain = self.domain or p.domain = self.subDomain) and\n\t\t\t\tcompositeSecurityMetrics-&gt;forAll(p| p.domain = self.domain or p.domain = self.subDomain))\n}.status'"
 * @generated
 */
public interface SecurityControl extends Feature {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see camel.security.SecurityPackage#getSecurityControl_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link camel.security.SecurityControl#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Domain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain</em>' reference.
	 * @see #setDomain(SecurityDomain)
	 * @see camel.security.SecurityPackage#getSecurityControl_Domain()
	 * @model required="true"
	 * @generated
	 */
	SecurityDomain getDomain();

	/**
	 * Sets the value of the '{@link camel.security.SecurityControl#getDomain <em>Domain</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain</em>' reference.
	 * @see #getDomain()
	 * @generated
	 */
	void setDomain(SecurityDomain value);

	/**
	 * Returns the value of the '<em><b>Sub Domain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Domain</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Domain</em>' reference.
	 * @see #setSubDomain(SecurityDomain)
	 * @see camel.security.SecurityPackage#getSecurityControl_SubDomain()
	 * @model required="true"
	 * @generated
	 */
	SecurityDomain getSubDomain();

	/**
	 * Sets the value of the '{@link camel.security.SecurityControl#getSubDomain <em>Sub Domain</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub Domain</em>' reference.
	 * @see #getSubDomain()
	 * @generated
	 */
	void setSubDomain(SecurityDomain value);

	/**
	 * Returns the value of the '<em><b>Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Specification</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Specification</em>' attribute.
	 * @see #setSpecification(String)
	 * @see camel.security.SecurityPackage#getSecurityControl_Specification()
	 * @model required="true"
	 *        annotation="teneo.jpa value='@Column(length=4000)'"
	 * @generated
	 */
	String getSpecification();

	/**
	 * Sets the value of the '{@link camel.security.SecurityControl#getSpecification <em>Specification</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Specification</em>' attribute.
	 * @see #getSpecification()
	 * @generated
	 */
	void setSpecification(String value);

	/**
	 * Returns the value of the '<em><b>Security Properties</b></em>' reference list.
	 * The list contents are of type {@link camel.security.SecurityAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Properties</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Properties</em>' reference list.
	 * @see camel.security.SecurityPackage#getSecurityControl_SecurityProperties()
	 * @model
	 * @generated
	 */
	EList<SecurityAttribute> getSecurityProperties();

	/**
	 * Returns the value of the '<em><b>Raw Security Metrics</b></em>' reference list.
	 * The list contents are of type {@link camel.security.RawSecurityMetric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raw Security Metrics</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raw Security Metrics</em>' reference list.
	 * @see camel.security.SecurityPackage#getSecurityControl_RawSecurityMetrics()
	 * @model
	 * @generated
	 */
	EList<RawSecurityMetric> getRawSecurityMetrics();

	/**
	 * Returns the value of the '<em><b>Composite Security Metrics</b></em>' reference list.
	 * The list contents are of type {@link camel.security.CompositeSecurityMetric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite Security Metrics</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Security Metrics</em>' reference list.
	 * @see camel.security.SecurityPackage#getSecurityControl_CompositeSecurityMetrics()
	 * @model
	 * @generated
	 */
	EList<CompositeSecurityMetric> getCompositeSecurityMetrics();

} // SecurityControl
