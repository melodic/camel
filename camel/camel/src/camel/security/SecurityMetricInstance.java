/**
 */
package camel.security;

import camel.metric.MetricInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Metric Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.security.SecurityPackage#getSecurityMetricInstance()
 * @model
 * @generated
 */
public interface SecurityMetricInstance extends MetricInstance {
} // SecurityMetricInstance
