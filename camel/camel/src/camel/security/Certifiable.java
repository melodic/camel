/**
 */
package camel.security;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Certifiable</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.security.SecurityPackage#getCertifiable()
 * @model
 * @generated
 */
public interface Certifiable extends SecurityAttribute {
} // Certifiable
