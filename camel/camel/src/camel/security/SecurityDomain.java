/**
 */
package camel.security;

import camel.core.Feature;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Domain</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.security.SecurityDomain#getId <em>Id</em>}</li>
 *   <li>{@link camel.security.SecurityDomain#getSubDomains <em>Sub Domains</em>}</li>
 * </ul>
 *
 * @see camel.security.SecurityPackage#getSecurityDomain()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='security_domain_recursiv_containment'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot security_domain_recursiv_containment='Tuple {\n\tmessage : String = \'SecurityDomain: \' + self.name + \' cannot be contained recursively in its sub-domains\',\n\tstatus : Boolean = \n\t\t\t\tasError(not self.contains(self))\n}.status'"
 * @generated
 */
public interface SecurityDomain extends Feature {
	/**
	 * Returns the value of the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Id</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Id</em>' attribute.
	 * @see #setId(String)
	 * @see camel.security.SecurityPackage#getSecurityDomain_Id()
	 * @model required="true"
	 * @generated
	 */
	String getId();

	/**
	 * Sets the value of the '{@link camel.security.SecurityDomain#getId <em>Id</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Id</em>' attribute.
	 * @see #getId()
	 * @generated
	 */
	void setId(String value);

	/**
	 * Returns the value of the '<em><b>Sub Domains</b></em>' reference list.
	 * The list contents are of type {@link camel.security.SecurityDomain}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Domains</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Domains</em>' reference list.
	 * @see camel.security.SecurityPackage#getSecurityDomain_SubDomains()
	 * @model ordered="false"
	 * @generated
	 */
	EList<SecurityDomain> getSubDomains();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='\n\t\t\t\t\tif (self.subDomains-&gt;includes(dom) or self.subDomains-&gt;exists(d | d.contains(dom))) then true\n\t\t\t\t\telse false\n\t\t\t\t\tendif'"
	 * @generated
	 */
	boolean contains(SecurityDomain dom);

} // SecurityDomain
