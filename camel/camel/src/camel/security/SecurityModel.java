/**
 */
package camel.security;

import camel.core.Model;

import camel.requirement.SecurityRequirement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.security.SecurityModel#getSecurityControls <em>Security Controls</em>}</li>
 *   <li>{@link camel.security.SecurityModel#getSecurityRequirements <em>Security Requirements</em>}</li>
 *   <li>{@link camel.security.SecurityModel#getSecurityAttributes <em>Security Attributes</em>}</li>
 *   <li>{@link camel.security.SecurityModel#getRawSecurityMetrics <em>Raw Security Metrics</em>}</li>
 *   <li>{@link camel.security.SecurityModel#getCompositeSecurityMetrics <em>Composite Security Metrics</em>}</li>
 *   <li>{@link camel.security.SecurityModel#getSecurityMetricInstances <em>Security Metric Instances</em>}</li>
 *   <li>{@link camel.security.SecurityModel#getSecurityDomains <em>Security Domains</em>}</li>
 *   <li>{@link camel.security.SecurityModel#getSecuritySLOs <em>Security SL Os</em>}</li>
 * </ul>
 *
 * @see camel.security.SecurityPackage#getSecurityModel()
 * @model
 * @generated
 */
public interface SecurityModel extends Model {
	/**
	 * Returns the value of the '<em><b>Security Controls</b></em>' containment reference list.
	 * The list contents are of type {@link camel.security.SecurityControl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Controls</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Controls</em>' containment reference list.
	 * @see camel.security.SecurityPackage#getSecurityModel_SecurityControls()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityControl> getSecurityControls();

	/**
	 * Returns the value of the '<em><b>Security Requirements</b></em>' containment reference list.
	 * The list contents are of type {@link camel.requirement.SecurityRequirement}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Requirements</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Requirements</em>' containment reference list.
	 * @see camel.security.SecurityPackage#getSecurityModel_SecurityRequirements()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityRequirement> getSecurityRequirements();

	/**
	 * Returns the value of the '<em><b>Security Attributes</b></em>' containment reference list.
	 * The list contents are of type {@link camel.security.SecurityAttribute}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Attributes</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Attributes</em>' containment reference list.
	 * @see camel.security.SecurityPackage#getSecurityModel_SecurityAttributes()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityAttribute> getSecurityAttributes();

	/**
	 * Returns the value of the '<em><b>Raw Security Metrics</b></em>' containment reference list.
	 * The list contents are of type {@link camel.security.RawSecurityMetric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Raw Security Metrics</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Raw Security Metrics</em>' containment reference list.
	 * @see camel.security.SecurityPackage#getSecurityModel_RawSecurityMetrics()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<RawSecurityMetric> getRawSecurityMetrics();

	/**
	 * Returns the value of the '<em><b>Composite Security Metrics</b></em>' containment reference list.
	 * The list contents are of type {@link camel.security.CompositeSecurityMetric}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Composite Security Metrics</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Composite Security Metrics</em>' containment reference list.
	 * @see camel.security.SecurityPackage#getSecurityModel_CompositeSecurityMetrics()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<CompositeSecurityMetric> getCompositeSecurityMetrics();

	/**
	 * Returns the value of the '<em><b>Security Metric Instances</b></em>' containment reference list.
	 * The list contents are of type {@link camel.security.SecurityMetricInstance}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Metric Instances</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Metric Instances</em>' containment reference list.
	 * @see camel.security.SecurityPackage#getSecurityModel_SecurityMetricInstances()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityMetricInstance> getSecurityMetricInstances();

	/**
	 * Returns the value of the '<em><b>Security Domains</b></em>' containment reference list.
	 * The list contents are of type {@link camel.security.SecurityDomain}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Domains</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Domains</em>' containment reference list.
	 * @see camel.security.SecurityPackage#getSecurityModel_SecurityDomains()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityDomain> getSecurityDomains();

	/**
	 * Returns the value of the '<em><b>Security SL Os</b></em>' containment reference list.
	 * The list contents are of type {@link camel.security.SecuritySLO}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security SL Os</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security SL Os</em>' containment reference list.
	 * @see camel.security.SecurityPackage#getSecurityModel_SecuritySLOs()
	 * @model containment="true"
	 * @generated
	 */
	EList<SecuritySLO> getSecuritySLOs();

} // SecurityModel
