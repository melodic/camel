/**
 */
package camel.security;

import camel.core.QualityAttribute;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.security.SecurityAttribute#getDomain <em>Domain</em>}</li>
 * </ul>
 *
 * @see camel.security.SecurityPackage#getSecurityAttribute()
 * @model
 * @generated
 */
public interface SecurityAttribute extends QualityAttribute {
	/**
	 * Returns the value of the '<em><b>Domain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Domain</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Domain</em>' reference.
	 * @see #setDomain(SecurityDomain)
	 * @see camel.security.SecurityPackage#getSecurityAttribute_Domain()
	 * @model required="true"
	 * @generated
	 */
	SecurityDomain getDomain();

	/**
	 * Sets the value of the '{@link camel.security.SecurityAttribute#getDomain <em>Domain</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Domain</em>' reference.
	 * @see #getDomain()
	 * @generated
	 */
	void setDomain(SecurityDomain value);

} // SecurityAttribute
