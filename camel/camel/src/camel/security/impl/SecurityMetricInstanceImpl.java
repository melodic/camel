/**
 */
package camel.security.impl;

import camel.metric.impl.MetricInstanceImpl;

import camel.security.SecurityMetricInstance;
import camel.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Metric Instance</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SecurityMetricInstanceImpl extends MetricInstanceImpl implements SecurityMetricInstance {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityMetricInstanceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.SECURITY_METRIC_INSTANCE;
	}

} //SecurityMetricInstanceImpl
