/**
 */
package camel.security.impl;

import camel.core.impl.FeatureImpl;

import camel.security.SecurityDomain;
import camel.security.SecurityPackage;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Domain</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.security.impl.SecurityDomainImpl#getId <em>Id</em>}</li>
 *   <li>{@link camel.security.impl.SecurityDomainImpl#getSubDomains <em>Sub Domains</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SecurityDomainImpl extends FeatureImpl implements SecurityDomain {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityDomainImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.SECURITY_DOMAIN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getId() {
		return (String)eGet(SecurityPackage.Literals.SECURITY_DOMAIN__ID, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setId(String newId) {
		eSet(SecurityPackage.Literals.SECURITY_DOMAIN__ID, newId);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityDomain> getSubDomains() {
		return (EList<SecurityDomain>)eGet(SecurityPackage.Literals.SECURITY_DOMAIN__SUB_DOMAINS, true);
	}

	/**
	 * The cached invocation delegate for the '{@link #contains(camel.security.SecurityDomain) <em>Contains</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #contains(camel.security.SecurityDomain)
	 * @generated
	 * @ordered
	 */
	protected static final EOperation.Internal.InvocationDelegate CONTAINS_SECURITY_DOMAIN__EINVOCATION_DELEGATE = ((EOperation.Internal)SecurityPackage.Literals.SECURITY_DOMAIN___CONTAINS__SECURITYDOMAIN).getInvocationDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean contains(SecurityDomain dom) {
		try {
			return (Boolean)CONTAINS_SECURITY_DOMAIN__EINVOCATION_DELEGATE.dynamicInvoke(this, new BasicEList.UnmodifiableEList<Object>(1, new Object[]{dom}));
		}
		catch (InvocationTargetException ite) {
			throw new WrappedException(ite);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case SecurityPackage.SECURITY_DOMAIN___CONTAINS__SECURITYDOMAIN:
				return contains((SecurityDomain)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //SecurityDomainImpl
