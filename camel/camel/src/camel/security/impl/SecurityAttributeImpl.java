/**
 */
package camel.security.impl;

import camel.core.impl.QualityAttributeImpl;

import camel.security.SecurityAttribute;
import camel.security.SecurityDomain;
import camel.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.security.impl.SecurityAttributeImpl#getDomain <em>Domain</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SecurityAttributeImpl extends QualityAttributeImpl implements SecurityAttribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.SECURITY_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityDomain getDomain() {
		return (SecurityDomain)eGet(SecurityPackage.Literals.SECURITY_ATTRIBUTE__DOMAIN, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomain(SecurityDomain newDomain) {
		eSet(SecurityPackage.Literals.SECURITY_ATTRIBUTE__DOMAIN, newDomain);
	}

} //SecurityAttributeImpl
