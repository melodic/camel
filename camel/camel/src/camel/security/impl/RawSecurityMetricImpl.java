/**
 */
package camel.security.impl;

import camel.metric.impl.RawMetricImpl;

import camel.security.RawSecurityMetric;
import camel.security.SecurityDomain;
import camel.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Raw Security Metric</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.security.impl.RawSecurityMetricImpl#getDomain <em>Domain</em>}</li>
 * </ul>
 *
 * @generated
 */
public class RawSecurityMetricImpl extends RawMetricImpl implements RawSecurityMetric {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RawSecurityMetricImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.RAW_SECURITY_METRIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityDomain getDomain() {
		return (SecurityDomain)eGet(SecurityPackage.Literals.RAW_SECURITY_METRIC__DOMAIN, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomain(SecurityDomain newDomain) {
		eSet(SecurityPackage.Literals.RAW_SECURITY_METRIC__DOMAIN, newDomain);
	}

} //RawSecurityMetricImpl
