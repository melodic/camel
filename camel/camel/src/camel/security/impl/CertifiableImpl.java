/**
 */
package camel.security.impl;

import camel.security.Certifiable;
import camel.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Certifiable</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class CertifiableImpl extends SecurityAttributeImpl implements Certifiable {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CertifiableImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.CERTIFIABLE;
	}

} //CertifiableImpl
