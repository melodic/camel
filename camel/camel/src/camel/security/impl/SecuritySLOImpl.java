/**
 */
package camel.security.impl;

import camel.requirement.impl.ServiceLevelObjectiveImpl;

import camel.security.SecurityPackage;
import camel.security.SecuritySLO;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>SLO</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SecuritySLOImpl extends ServiceLevelObjectiveImpl implements SecuritySLO {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecuritySLOImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.SECURITY_SLO;
	}

} //SecuritySLOImpl
