/**
 */
package camel.security.impl;

import camel.core.impl.ModelImpl;

import camel.requirement.SecurityRequirement;

import camel.security.CompositeSecurityMetric;
import camel.security.RawSecurityMetric;
import camel.security.SecurityAttribute;
import camel.security.SecurityControl;
import camel.security.SecurityDomain;
import camel.security.SecurityMetricInstance;
import camel.security.SecurityModel;
import camel.security.SecurityPackage;
import camel.security.SecuritySLO;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.security.impl.SecurityModelImpl#getSecurityControls <em>Security Controls</em>}</li>
 *   <li>{@link camel.security.impl.SecurityModelImpl#getSecurityRequirements <em>Security Requirements</em>}</li>
 *   <li>{@link camel.security.impl.SecurityModelImpl#getSecurityAttributes <em>Security Attributes</em>}</li>
 *   <li>{@link camel.security.impl.SecurityModelImpl#getRawSecurityMetrics <em>Raw Security Metrics</em>}</li>
 *   <li>{@link camel.security.impl.SecurityModelImpl#getCompositeSecurityMetrics <em>Composite Security Metrics</em>}</li>
 *   <li>{@link camel.security.impl.SecurityModelImpl#getSecurityMetricInstances <em>Security Metric Instances</em>}</li>
 *   <li>{@link camel.security.impl.SecurityModelImpl#getSecurityDomains <em>Security Domains</em>}</li>
 *   <li>{@link camel.security.impl.SecurityModelImpl#getSecuritySLOs <em>Security SL Os</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SecurityModelImpl extends ModelImpl implements SecurityModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.SECURITY_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityControl> getSecurityControls() {
		return (EList<SecurityControl>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_CONTROLS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityRequirement> getSecurityRequirements() {
		return (EList<SecurityRequirement>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_REQUIREMENTS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityAttribute> getSecurityAttributes() {
		return (EList<SecurityAttribute>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_ATTRIBUTES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RawSecurityMetric> getRawSecurityMetrics() {
		return (EList<RawSecurityMetric>)eGet(SecurityPackage.Literals.SECURITY_MODEL__RAW_SECURITY_METRICS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<CompositeSecurityMetric> getCompositeSecurityMetrics() {
		return (EList<CompositeSecurityMetric>)eGet(SecurityPackage.Literals.SECURITY_MODEL__COMPOSITE_SECURITY_METRICS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityMetricInstance> getSecurityMetricInstances() {
		return (EList<SecurityMetricInstance>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_METRIC_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityDomain> getSecurityDomains() {
		return (EList<SecurityDomain>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_DOMAINS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecuritySLO> getSecuritySLOs() {
		return (EList<SecuritySLO>)eGet(SecurityPackage.Literals.SECURITY_MODEL__SECURITY_SL_OS, true);
	}

} //SecurityModelImpl
