/**
 */
package camel.security.impl;

import camel.metric.impl.CompositeMetricImpl;

import camel.security.CompositeSecurityMetric;
import camel.security.SecurityDomain;
import camel.security.SecurityPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Security Metric</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.security.impl.CompositeSecurityMetricImpl#getDomain <em>Domain</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompositeSecurityMetricImpl extends CompositeMetricImpl implements CompositeSecurityMetric {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeSecurityMetricImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return SecurityPackage.Literals.COMPOSITE_SECURITY_METRIC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityDomain getDomain() {
		return (SecurityDomain)eGet(SecurityPackage.Literals.COMPOSITE_SECURITY_METRIC__DOMAIN, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDomain(SecurityDomain newDomain) {
		eSet(SecurityPackage.Literals.COMPOSITE_SECURITY_METRIC__DOMAIN, newDomain);
	}

} //CompositeSecurityMetricImpl
