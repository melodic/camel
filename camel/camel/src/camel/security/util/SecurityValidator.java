/**
 */
package camel.security.util;

import camel.core.util.CoreValidator;

import camel.metric.util.MetricValidator;

import camel.requirement.util.RequirementValidator;
import camel.security.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.security.SecurityPackage
 * @generated
 */
public class SecurityValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final SecurityValidator INSTANCE = new SecurityValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.security";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MetricValidator metricValidator;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CoreValidator coreValidator;

	/**
	 * The cached base package validator.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected RequirementValidator requirementValidator;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityValidator() {
		super();
		metricValidator = MetricValidator.INSTANCE;
		coreValidator = CoreValidator.INSTANCE;
		requirementValidator = RequirementValidator.INSTANCE;
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return SecurityPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case SecurityPackage.SECURITY_MODEL:
				return validateSecurityModel((SecurityModel)value, diagnostics, context);
			case SecurityPackage.SECURITY_DOMAIN:
				return validateSecurityDomain((SecurityDomain)value, diagnostics, context);
			case SecurityPackage.SECURITY_CONTROL:
				return validateSecurityControl((SecurityControl)value, diagnostics, context);
			case SecurityPackage.SECURITY_METRIC_INSTANCE:
				return validateSecurityMetricInstance((SecurityMetricInstance)value, diagnostics, context);
			case SecurityPackage.RAW_SECURITY_METRIC:
				return validateRawSecurityMetric((RawSecurityMetric)value, diagnostics, context);
			case SecurityPackage.SECURITY_ATTRIBUTE:
				return validateSecurityAttribute((SecurityAttribute)value, diagnostics, context);
			case SecurityPackage.CERTIFIABLE:
				return validateCertifiable((Certifiable)value, diagnostics, context);
			case SecurityPackage.SECURITY_SLO:
				return validateSecuritySLO((SecuritySLO)value, diagnostics, context);
			case SecurityPackage.COMPOSITE_SECURITY_METRIC:
				return validateCompositeSecurityMetric((CompositeSecurityMetric)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecurityModel(SecurityModel securityModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)securityModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecurityDomain(SecurityDomain securityDomain, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)securityDomain, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)securityDomain, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)securityDomain, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)securityDomain, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)securityDomain, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)securityDomain, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)securityDomain, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)securityDomain, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)securityDomain, diagnostics, context);
		if (result || diagnostics != null) result &= validateSecurityDomain_security_domain_recursiv_containment(securityDomain, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the security_domain_recursiv_containment constraint of '<em>Domain</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SECURITY_DOMAIN__SECURITY_DOMAIN_RECURSIV_CONTAINMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'SecurityDomain: ' + self.name + ' cannot be contained recursively in its sub-domains',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(not self.contains(self))\n" +
		"}.status";

	/**
	 * Validates the security_domain_recursiv_containment constraint of '<em>Domain</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecurityDomain_security_domain_recursiv_containment(SecurityDomain securityDomain, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(SecurityPackage.Literals.SECURITY_DOMAIN,
				 (EObject)securityDomain,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "security_domain_recursiv_containment",
				 SECURITY_DOMAIN__SECURITY_DOMAIN_RECURSIV_CONTAINMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecurityControl(SecurityControl securityControl, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)securityControl, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)securityControl, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)securityControl, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)securityControl, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)securityControl, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)securityControl, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)securityControl, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)securityControl, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)securityControl, diagnostics, context);
		if (result || diagnostics != null) result &= validateSecurityControl_security_control_domain_subdomain_diff(securityControl, diagnostics, context);
		if (result || diagnostics != null) result &= validateSecurityControl_security_control_all_elems_align_domain(securityControl, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the security_control_domain_subdomain_diff constraint of '<em>Control</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SECURITY_CONTROL__SECURITY_CONTROL_DOMAIN_SUBDOMAIN_DIFF__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In SecurityControl: ' + self.name +' the domain cannot be the same as the sub-domain',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(domain <> subDomain)\n" +
		"}.status";

	/**
	 * Validates the security_control_domain_subdomain_diff constraint of '<em>Control</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecurityControl_security_control_domain_subdomain_diff(SecurityControl securityControl, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(SecurityPackage.Literals.SECURITY_CONTROL,
				 (EObject)securityControl,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "security_control_domain_subdomain_diff",
				 SECURITY_CONTROL__SECURITY_CONTROL_DOMAIN_SUBDOMAIN_DIFF__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the security_control_all_elems_align_domain constraint of '<em>Control</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SECURITY_CONTROL__SECURITY_CONTROL_ALL_ELEMS_ALIGN_DOMAIN__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'SecurityControl: ' + self.name + ' should have all properties or metrics referenced belonging to the same domain or sub-domain',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(securityProperties->forAll(p| p.domain = self.domain or p.domain = self.subDomain) and\n" +
		"\t\t\t\trawSecurityMetrics->forAll(p| p.domain = self.domain or p.domain = self.subDomain) and\n" +
		"\t\t\t\tcompositeSecurityMetrics->forAll(p| p.domain = self.domain or p.domain = self.subDomain))\n" +
		"}.status";

	/**
	 * Validates the security_control_all_elems_align_domain constraint of '<em>Control</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecurityControl_security_control_all_elems_align_domain(SecurityControl securityControl, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(SecurityPackage.Literals.SECURITY_CONTROL,
				 (EObject)securityControl,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "security_control_all_elems_align_domain",
				 SECURITY_CONTROL__SECURITY_CONTROL_ALL_ELEMS_ALIGN_DOMAIN__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecurityMetricInstance(SecurityMetricInstance securityMetricInstance, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)securityMetricInstance, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)securityMetricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)securityMetricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)securityMetricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)securityMetricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)securityMetricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)securityMetricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)securityMetricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)securityMetricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= metricValidator.validateMetricInstance_composite_metric_instance_to_components(securityMetricInstance, diagnostics, context);
		if (result || diagnostics != null) result &= metricValidator.validateMetricInstance_component_instances_metric_map_component_metrics(securityMetricInstance, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRawSecurityMetric(RawSecurityMetric rawSecurityMetric, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)rawSecurityMetric, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecurityAttribute(SecurityAttribute securityAttribute, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)securityAttribute, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)securityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)securityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)securityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)securityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)securityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)securityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)securityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)securityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= coreValidator.validateAttribute_attribute_must_have_at_least_value_or_value_type(securityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= coreValidator.validateAttribute_attribute_value_in_value_type(securityAttribute, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCertifiable(Certifiable certifiable, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)certifiable, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)certifiable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)certifiable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)certifiable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)certifiable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)certifiable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)certifiable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)certifiable, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)certifiable, diagnostics, context);
		if (result || diagnostics != null) result &= coreValidator.validateAttribute_attribute_must_have_at_least_value_or_value_type(certifiable, diagnostics, context);
		if (result || diagnostics != null) result &= coreValidator.validateAttribute_attribute_value_in_value_type(certifiable, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecuritySLO(SecuritySLO securitySLO, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)securitySLO, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)securitySLO, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)securitySLO, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)securitySLO, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)securitySLO, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)securitySLO, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)securitySLO, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)securitySLO, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)securitySLO, diagnostics, context);
		if (result || diagnostics != null) result &= requirementValidator.validateServiceLevelObjective_slo_invalid_input(securitySLO, diagnostics, context);
		if (result || diagnostics != null) result &= validateSecuritySLO_only_security_metrics_in_security_slo(securitySLO, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the only_security_metrics_in_security_slo constraint of '<em>SLO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SECURITY_SLO__ONLY_SECURITY_METRICS_IN_SECURITY_SLO__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'SecuritySLO: ' + self.name + ' should have only a security metric involved in the respective metric constraint',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.constraint.oclIsTypeOf(camel::constraint::MetricConstraint) implies \n" +
		"\t\t\t\t\tself.constraint.oclAsType(camel::constraint::MetricConstraint).metricContext.metric.oclIsTypeOf(RawSecurityMetric) or \n" +
		"\t\t\t\t\tself.constraint.oclAsType(camel::constraint::MetricConstraint).metricContext.metric.oclIsTypeOf(CompositeSecurityMetric))\n" +
		"}.status";

	/**
	 * Validates the only_security_metrics_in_security_slo constraint of '<em>SLO</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecuritySLO_only_security_metrics_in_security_slo(SecuritySLO securitySLO, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(SecurityPackage.Literals.SECURITY_SLO,
				 (EObject)securitySLO,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "only_security_metrics_in_security_slo",
				 SECURITY_SLO__ONLY_SECURITY_METRICS_IN_SECURITY_SLO__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeSecurityMetric(CompositeSecurityMetric compositeSecurityMetric, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)compositeSecurityMetric, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)compositeSecurityMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)compositeSecurityMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)compositeSecurityMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)compositeSecurityMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)compositeSecurityMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)compositeSecurityMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)compositeSecurityMetric, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)compositeSecurityMetric, diagnostics, context);
		if (result || diagnostics != null) result &= metricValidator.validateCompositeMetric_composite_metric_contains_recurs(compositeSecurityMetric, diagnostics, context);
		return result;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //SecurityValidator
