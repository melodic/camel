/**
 */
package camel.security;

import camel.requirement.ServiceLevelObjective;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>SLO</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.security.SecurityPackage#getSecuritySLO()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='only_security_metrics_in_security_slo'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot only_security_metrics_in_security_slo='Tuple {\n\tmessage : String = \'SecuritySLO: \' + self.name + \' should have only a security metric involved in the respective metric constraint\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.constraint.oclIsTypeOf(camel::constraint::MetricConstraint) implies \n\t\t\t\t\tself.constraint.oclAsType(camel::constraint::MetricConstraint).metricContext.metric.oclIsTypeOf(RawSecurityMetric) or \n\t\t\t\t\tself.constraint.oclAsType(camel::constraint::MetricConstraint).metricContext.metric.oclIsTypeOf(CompositeSecurityMetric))\n}.status'"
 * @generated
 */
public interface SecuritySLO extends ServiceLevelObjective {
} // SecuritySLO
