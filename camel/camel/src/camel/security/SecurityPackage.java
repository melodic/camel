/**
 */
package camel.security;

import camel.core.CorePackage;

import camel.metric.MetricPackage;

import camel.requirement.RequirementPackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see camel.security.SecurityFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface SecurityPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "security";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.camel-dsl.org/camel/security";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "security";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	SecurityPackage eINSTANCE = camel.security.impl.SecurityPackageImpl.init();

	/**
	 * The meta object id for the '{@link camel.security.impl.SecurityModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.security.impl.SecurityModelImpl
	 * @see camel.security.impl.SecurityPackageImpl#getSecurityModel()
	 * @generated
	 */
	int SECURITY_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__NAME = CorePackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__DESCRIPTION = CorePackage.MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__ANNOTATIONS = CorePackage.MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__ATTRIBUTES = CorePackage.MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__SUB_FEATURES = CorePackage.MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__IMPORT_URI = CorePackage.MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Security Controls</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__SECURITY_CONTROLS = CorePackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Security Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__SECURITY_REQUIREMENTS = CorePackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Security Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__SECURITY_ATTRIBUTES = CorePackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Raw Security Metrics</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__RAW_SECURITY_METRICS = CorePackage.MODEL_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Composite Security Metrics</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__COMPOSITE_SECURITY_METRICS = CorePackage.MODEL_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Security Metric Instances</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__SECURITY_METRIC_INSTANCES = CorePackage.MODEL_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Security Domains</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__SECURITY_DOMAINS = CorePackage.MODEL_FEATURE_COUNT + 6;

	/**
	 * The feature id for the '<em><b>Security SL Os</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL__SECURITY_SL_OS = CorePackage.MODEL_FEATURE_COUNT + 7;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL_FEATURE_COUNT = CorePackage.MODEL_FEATURE_COUNT + 8;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL___AS_ERROR__BOOLEAN = CorePackage.MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_MODEL_OPERATION_COUNT = CorePackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.security.impl.SecurityDomainImpl <em>Domain</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.security.impl.SecurityDomainImpl
	 * @see camel.security.impl.SecurityPackageImpl#getSecurityDomain()
	 * @generated
	 */
	int SECURITY_DOMAIN = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_DOMAIN__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_DOMAIN__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_DOMAIN__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_DOMAIN__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_DOMAIN__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_DOMAIN__ID = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Sub Domains</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_DOMAIN__SUB_DOMAINS = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Domain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_DOMAIN_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_DOMAIN___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Contains</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_DOMAIN___CONTAINS__SECURITYDOMAIN = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Domain</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_DOMAIN_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link camel.security.impl.SecurityControlImpl <em>Control</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.security.impl.SecurityControlImpl
	 * @see camel.security.impl.SecurityPackageImpl#getSecurityControl()
	 * @generated
	 */
	int SECURITY_CONTROL = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Id</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__ID = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__DOMAIN = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Sub Domain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__SUB_DOMAIN = CorePackage.FEATURE_FEATURE_COUNT + 2;

	/**
	 * The feature id for the '<em><b>Specification</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__SPECIFICATION = CorePackage.FEATURE_FEATURE_COUNT + 3;

	/**
	 * The feature id for the '<em><b>Security Properties</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__SECURITY_PROPERTIES = CorePackage.FEATURE_FEATURE_COUNT + 4;

	/**
	 * The feature id for the '<em><b>Raw Security Metrics</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__RAW_SECURITY_METRICS = CorePackage.FEATURE_FEATURE_COUNT + 5;

	/**
	 * The feature id for the '<em><b>Composite Security Metrics</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL__COMPOSITE_SECURITY_METRICS = CorePackage.FEATURE_FEATURE_COUNT + 6;

	/**
	 * The number of structural features of the '<em>Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 7;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Control</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_CONTROL_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.security.impl.SecurityMetricInstanceImpl <em>Metric Instance</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.security.impl.SecurityMetricInstanceImpl
	 * @see camel.security.impl.SecurityPackageImpl#getSecurityMetricInstance()
	 * @generated
	 */
	int SECURITY_METRIC_INSTANCE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_METRIC_INSTANCE__NAME = MetricPackage.METRIC_INSTANCE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_METRIC_INSTANCE__DESCRIPTION = MetricPackage.METRIC_INSTANCE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_METRIC_INSTANCE__ANNOTATIONS = MetricPackage.METRIC_INSTANCE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_METRIC_INSTANCE__ATTRIBUTES = MetricPackage.METRIC_INSTANCE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_METRIC_INSTANCE__SUB_FEATURES = MetricPackage.METRIC_INSTANCE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Object Binding</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_METRIC_INSTANCE__OBJECT_BINDING = MetricPackage.METRIC_INSTANCE__OBJECT_BINDING;

	/**
	 * The feature id for the '<em><b>Metric Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_METRIC_INSTANCE__METRIC_CONTEXT = MetricPackage.METRIC_INSTANCE__METRIC_CONTEXT;

	/**
	 * The feature id for the '<em><b>Composing Metric Instances</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_METRIC_INSTANCE__COMPOSING_METRIC_INSTANCES = MetricPackage.METRIC_INSTANCE__COMPOSING_METRIC_INSTANCES;

	/**
	 * The number of structural features of the '<em>Metric Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_METRIC_INSTANCE_FEATURE_COUNT = MetricPackage.METRIC_INSTANCE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_METRIC_INSTANCE___AS_ERROR__BOOLEAN = MetricPackage.METRIC_INSTANCE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Metric Instance</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_METRIC_INSTANCE_OPERATION_COUNT = MetricPackage.METRIC_INSTANCE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.security.impl.RawSecurityMetricImpl <em>Raw Security Metric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.security.impl.RawSecurityMetricImpl
	 * @see camel.security.impl.SecurityPackageImpl#getRawSecurityMetric()
	 * @generated
	 */
	int RAW_SECURITY_METRIC = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_SECURITY_METRIC__NAME = MetricPackage.RAW_METRIC__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_SECURITY_METRIC__DESCRIPTION = MetricPackage.RAW_METRIC__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_SECURITY_METRIC__ANNOTATIONS = MetricPackage.RAW_METRIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_SECURITY_METRIC__ATTRIBUTES = MetricPackage.RAW_METRIC__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_SECURITY_METRIC__SUB_FEATURES = MetricPackage.RAW_METRIC__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_SECURITY_METRIC__METRIC_TEMPLATE = MetricPackage.RAW_METRIC__METRIC_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_SECURITY_METRIC__DOMAIN = MetricPackage.RAW_METRIC_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Raw Security Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_SECURITY_METRIC_FEATURE_COUNT = MetricPackage.RAW_METRIC_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_SECURITY_METRIC___AS_ERROR__BOOLEAN = MetricPackage.RAW_METRIC___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Check Recursiveness</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_SECURITY_METRIC___CHECK_RECURSIVENESS__METRIC_METRIC = MetricPackage.RAW_METRIC___CHECK_RECURSIVENESS__METRIC_METRIC;

	/**
	 * The number of operations of the '<em>Raw Security Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RAW_SECURITY_METRIC_OPERATION_COUNT = MetricPackage.RAW_METRIC_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.security.impl.SecurityAttributeImpl <em>Attribute</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.security.impl.SecurityAttributeImpl
	 * @see camel.security.impl.SecurityPackageImpl#getSecurityAttribute()
	 * @generated
	 */
	int SECURITY_ATTRIBUTE = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_ATTRIBUTE__NAME = CorePackage.QUALITY_ATTRIBUTE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_ATTRIBUTE__DESCRIPTION = CorePackage.QUALITY_ATTRIBUTE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_ATTRIBUTE__ANNOTATIONS = CorePackage.QUALITY_ATTRIBUTE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_ATTRIBUTE__VALUE = CorePackage.QUALITY_ATTRIBUTE__VALUE;

	/**
	 * The feature id for the '<em><b>Value Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_ATTRIBUTE__VALUE_TYPE = CorePackage.QUALITY_ATTRIBUTE__VALUE_TYPE;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_ATTRIBUTE__UNIT = CorePackage.QUALITY_ATTRIBUTE__UNIT;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_ATTRIBUTE__DOMAIN = CorePackage.QUALITY_ATTRIBUTE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_ATTRIBUTE_FEATURE_COUNT = CorePackage.QUALITY_ATTRIBUTE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_ATTRIBUTE___AS_ERROR__BOOLEAN = CorePackage.QUALITY_ATTRIBUTE___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Check Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_ATTRIBUTE___CHECK_VALUE__VALUE_BOOLEAN = CorePackage.QUALITY_ATTRIBUTE___CHECK_VALUE__VALUE_BOOLEAN;

	/**
	 * The number of operations of the '<em>Attribute</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_ATTRIBUTE_OPERATION_COUNT = CorePackage.QUALITY_ATTRIBUTE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.security.impl.CertifiableImpl <em>Certifiable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.security.impl.CertifiableImpl
	 * @see camel.security.impl.SecurityPackageImpl#getCertifiable()
	 * @generated
	 */
	int CERTIFIABLE = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFIABLE__NAME = SECURITY_ATTRIBUTE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFIABLE__DESCRIPTION = SECURITY_ATTRIBUTE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFIABLE__ANNOTATIONS = SECURITY_ATTRIBUTE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFIABLE__VALUE = SECURITY_ATTRIBUTE__VALUE;

	/**
	 * The feature id for the '<em><b>Value Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFIABLE__VALUE_TYPE = SECURITY_ATTRIBUTE__VALUE_TYPE;

	/**
	 * The feature id for the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFIABLE__UNIT = SECURITY_ATTRIBUTE__UNIT;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFIABLE__DOMAIN = SECURITY_ATTRIBUTE__DOMAIN;

	/**
	 * The number of structural features of the '<em>Certifiable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFIABLE_FEATURE_COUNT = SECURITY_ATTRIBUTE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFIABLE___AS_ERROR__BOOLEAN = SECURITY_ATTRIBUTE___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Check Value</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFIABLE___CHECK_VALUE__VALUE_BOOLEAN = SECURITY_ATTRIBUTE___CHECK_VALUE__VALUE_BOOLEAN;

	/**
	 * The number of operations of the '<em>Certifiable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CERTIFIABLE_OPERATION_COUNT = SECURITY_ATTRIBUTE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.security.impl.SecuritySLOImpl <em>SLO</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.security.impl.SecuritySLOImpl
	 * @see camel.security.impl.SecurityPackageImpl#getSecuritySLO()
	 * @generated
	 */
	int SECURITY_SLO = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_SLO__NAME = RequirementPackage.SERVICE_LEVEL_OBJECTIVE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_SLO__DESCRIPTION = RequirementPackage.SERVICE_LEVEL_OBJECTIVE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_SLO__ANNOTATIONS = RequirementPackage.SERVICE_LEVEL_OBJECTIVE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_SLO__ATTRIBUTES = RequirementPackage.SERVICE_LEVEL_OBJECTIVE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_SLO__SUB_FEATURES = RequirementPackage.SERVICE_LEVEL_OBJECTIVE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_SLO__CONSTRAINT = RequirementPackage.SERVICE_LEVEL_OBJECTIVE__CONSTRAINT;

	/**
	 * The feature id for the '<em><b>Violation Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_SLO__VIOLATION_EVENT = RequirementPackage.SERVICE_LEVEL_OBJECTIVE__VIOLATION_EVENT;

	/**
	 * The number of structural features of the '<em>SLO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_SLO_FEATURE_COUNT = RequirementPackage.SERVICE_LEVEL_OBJECTIVE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_SLO___AS_ERROR__BOOLEAN = RequirementPackage.SERVICE_LEVEL_OBJECTIVE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>SLO</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_SLO_OPERATION_COUNT = RequirementPackage.SERVICE_LEVEL_OBJECTIVE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.security.impl.CompositeSecurityMetricImpl <em>Composite Security Metric</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.security.impl.CompositeSecurityMetricImpl
	 * @see camel.security.impl.SecurityPackageImpl#getCompositeSecurityMetric()
	 * @generated
	 */
	int COMPOSITE_SECURITY_METRIC = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC__NAME = MetricPackage.COMPOSITE_METRIC__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC__DESCRIPTION = MetricPackage.COMPOSITE_METRIC__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC__ANNOTATIONS = MetricPackage.COMPOSITE_METRIC__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC__ATTRIBUTES = MetricPackage.COMPOSITE_METRIC__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC__SUB_FEATURES = MetricPackage.COMPOSITE_METRIC__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Metric Template</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC__METRIC_TEMPLATE = MetricPackage.COMPOSITE_METRIC__METRIC_TEMPLATE;

	/**
	 * The feature id for the '<em><b>Formula</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC__FORMULA = MetricPackage.COMPOSITE_METRIC__FORMULA;

	/**
	 * The feature id for the '<em><b>Component Metrics</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC__COMPONENT_METRICS = MetricPackage.COMPOSITE_METRIC__COMPONENT_METRICS;

	/**
	 * The feature id for the '<em><b>Domain</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC__DOMAIN = MetricPackage.COMPOSITE_METRIC_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Composite Security Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC_FEATURE_COUNT = MetricPackage.COMPOSITE_METRIC_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC___AS_ERROR__BOOLEAN = MetricPackage.COMPOSITE_METRIC___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Check Recursiveness</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC___CHECK_RECURSIVENESS__METRIC_METRIC = MetricPackage.COMPOSITE_METRIC___CHECK_RECURSIVENESS__METRIC_METRIC;

	/**
	 * The operation id for the '<em>Contains Metric</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC___CONTAINS_METRIC__METRIC = MetricPackage.COMPOSITE_METRIC___CONTAINS_METRIC__METRIC;

	/**
	 * The number of operations of the '<em>Composite Security Metric</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_SECURITY_METRIC_OPERATION_COUNT = MetricPackage.COMPOSITE_METRIC_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link camel.security.SecurityModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see camel.security.SecurityModel
	 * @generated
	 */
	EClass getSecurityModel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.security.SecurityModel#getSecurityControls <em>Security Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Security Controls</em>'.
	 * @see camel.security.SecurityModel#getSecurityControls()
	 * @see #getSecurityModel()
	 * @generated
	 */
	EReference getSecurityModel_SecurityControls();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.security.SecurityModel#getSecurityRequirements <em>Security Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Security Requirements</em>'.
	 * @see camel.security.SecurityModel#getSecurityRequirements()
	 * @see #getSecurityModel()
	 * @generated
	 */
	EReference getSecurityModel_SecurityRequirements();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.security.SecurityModel#getSecurityAttributes <em>Security Attributes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Security Attributes</em>'.
	 * @see camel.security.SecurityModel#getSecurityAttributes()
	 * @see #getSecurityModel()
	 * @generated
	 */
	EReference getSecurityModel_SecurityAttributes();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.security.SecurityModel#getRawSecurityMetrics <em>Raw Security Metrics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Raw Security Metrics</em>'.
	 * @see camel.security.SecurityModel#getRawSecurityMetrics()
	 * @see #getSecurityModel()
	 * @generated
	 */
	EReference getSecurityModel_RawSecurityMetrics();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.security.SecurityModel#getCompositeSecurityMetrics <em>Composite Security Metrics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Composite Security Metrics</em>'.
	 * @see camel.security.SecurityModel#getCompositeSecurityMetrics()
	 * @see #getSecurityModel()
	 * @generated
	 */
	EReference getSecurityModel_CompositeSecurityMetrics();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.security.SecurityModel#getSecurityMetricInstances <em>Security Metric Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Security Metric Instances</em>'.
	 * @see camel.security.SecurityModel#getSecurityMetricInstances()
	 * @see #getSecurityModel()
	 * @generated
	 */
	EReference getSecurityModel_SecurityMetricInstances();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.security.SecurityModel#getSecurityDomains <em>Security Domains</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Security Domains</em>'.
	 * @see camel.security.SecurityModel#getSecurityDomains()
	 * @see #getSecurityModel()
	 * @generated
	 */
	EReference getSecurityModel_SecurityDomains();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.security.SecurityModel#getSecuritySLOs <em>Security SL Os</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Security SL Os</em>'.
	 * @see camel.security.SecurityModel#getSecuritySLOs()
	 * @see #getSecurityModel()
	 * @generated
	 */
	EReference getSecurityModel_SecuritySLOs();

	/**
	 * Returns the meta object for class '{@link camel.security.SecurityDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Domain</em>'.
	 * @see camel.security.SecurityDomain
	 * @generated
	 */
	EClass getSecurityDomain();

	/**
	 * Returns the meta object for the attribute '{@link camel.security.SecurityDomain#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see camel.security.SecurityDomain#getId()
	 * @see #getSecurityDomain()
	 * @generated
	 */
	EAttribute getSecurityDomain_Id();

	/**
	 * Returns the meta object for the reference list '{@link camel.security.SecurityDomain#getSubDomains <em>Sub Domains</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Sub Domains</em>'.
	 * @see camel.security.SecurityDomain#getSubDomains()
	 * @see #getSecurityDomain()
	 * @generated
	 */
	EReference getSecurityDomain_SubDomains();

	/**
	 * Returns the meta object for the '{@link camel.security.SecurityDomain#contains(camel.security.SecurityDomain) <em>Contains</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Contains</em>' operation.
	 * @see camel.security.SecurityDomain#contains(camel.security.SecurityDomain)
	 * @generated
	 */
	EOperation getSecurityDomain__Contains__SecurityDomain();

	/**
	 * Returns the meta object for class '{@link camel.security.SecurityControl <em>Control</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Control</em>'.
	 * @see camel.security.SecurityControl
	 * @generated
	 */
	EClass getSecurityControl();

	/**
	 * Returns the meta object for the attribute '{@link camel.security.SecurityControl#getId <em>Id</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Id</em>'.
	 * @see camel.security.SecurityControl#getId()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EAttribute getSecurityControl_Id();

	/**
	 * Returns the meta object for the reference '{@link camel.security.SecurityControl#getDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Domain</em>'.
	 * @see camel.security.SecurityControl#getDomain()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EReference getSecurityControl_Domain();

	/**
	 * Returns the meta object for the reference '{@link camel.security.SecurityControl#getSubDomain <em>Sub Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Sub Domain</em>'.
	 * @see camel.security.SecurityControl#getSubDomain()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EReference getSecurityControl_SubDomain();

	/**
	 * Returns the meta object for the attribute '{@link camel.security.SecurityControl#getSpecification <em>Specification</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Specification</em>'.
	 * @see camel.security.SecurityControl#getSpecification()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EAttribute getSecurityControl_Specification();

	/**
	 * Returns the meta object for the reference list '{@link camel.security.SecurityControl#getSecurityProperties <em>Security Properties</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Security Properties</em>'.
	 * @see camel.security.SecurityControl#getSecurityProperties()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EReference getSecurityControl_SecurityProperties();

	/**
	 * Returns the meta object for the reference list '{@link camel.security.SecurityControl#getRawSecurityMetrics <em>Raw Security Metrics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Raw Security Metrics</em>'.
	 * @see camel.security.SecurityControl#getRawSecurityMetrics()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EReference getSecurityControl_RawSecurityMetrics();

	/**
	 * Returns the meta object for the reference list '{@link camel.security.SecurityControl#getCompositeSecurityMetrics <em>Composite Security Metrics</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Composite Security Metrics</em>'.
	 * @see camel.security.SecurityControl#getCompositeSecurityMetrics()
	 * @see #getSecurityControl()
	 * @generated
	 */
	EReference getSecurityControl_CompositeSecurityMetrics();

	/**
	 * Returns the meta object for class '{@link camel.security.SecurityMetricInstance <em>Metric Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Metric Instance</em>'.
	 * @see camel.security.SecurityMetricInstance
	 * @generated
	 */
	EClass getSecurityMetricInstance();

	/**
	 * Returns the meta object for class '{@link camel.security.RawSecurityMetric <em>Raw Security Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Raw Security Metric</em>'.
	 * @see camel.security.RawSecurityMetric
	 * @generated
	 */
	EClass getRawSecurityMetric();

	/**
	 * Returns the meta object for the reference '{@link camel.security.RawSecurityMetric#getDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Domain</em>'.
	 * @see camel.security.RawSecurityMetric#getDomain()
	 * @see #getRawSecurityMetric()
	 * @generated
	 */
	EReference getRawSecurityMetric_Domain();

	/**
	 * Returns the meta object for class '{@link camel.security.SecurityAttribute <em>Attribute</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Attribute</em>'.
	 * @see camel.security.SecurityAttribute
	 * @generated
	 */
	EClass getSecurityAttribute();

	/**
	 * Returns the meta object for the reference '{@link camel.security.SecurityAttribute#getDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Domain</em>'.
	 * @see camel.security.SecurityAttribute#getDomain()
	 * @see #getSecurityAttribute()
	 * @generated
	 */
	EReference getSecurityAttribute_Domain();

	/**
	 * Returns the meta object for class '{@link camel.security.Certifiable <em>Certifiable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Certifiable</em>'.
	 * @see camel.security.Certifiable
	 * @generated
	 */
	EClass getCertifiable();

	/**
	 * Returns the meta object for class '{@link camel.security.SecuritySLO <em>SLO</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>SLO</em>'.
	 * @see camel.security.SecuritySLO
	 * @generated
	 */
	EClass getSecuritySLO();

	/**
	 * Returns the meta object for class '{@link camel.security.CompositeSecurityMetric <em>Composite Security Metric</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Security Metric</em>'.
	 * @see camel.security.CompositeSecurityMetric
	 * @generated
	 */
	EClass getCompositeSecurityMetric();

	/**
	 * Returns the meta object for the reference '{@link camel.security.CompositeSecurityMetric#getDomain <em>Domain</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Domain</em>'.
	 * @see camel.security.CompositeSecurityMetric#getDomain()
	 * @see #getCompositeSecurityMetric()
	 * @generated
	 */
	EReference getCompositeSecurityMetric_Domain();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	SecurityFactory getSecurityFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link camel.security.impl.SecurityModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.security.impl.SecurityModelImpl
		 * @see camel.security.impl.SecurityPackageImpl#getSecurityModel()
		 * @generated
		 */
		EClass SECURITY_MODEL = eINSTANCE.getSecurityModel();

		/**
		 * The meta object literal for the '<em><b>Security Controls</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_MODEL__SECURITY_CONTROLS = eINSTANCE.getSecurityModel_SecurityControls();

		/**
		 * The meta object literal for the '<em><b>Security Requirements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_MODEL__SECURITY_REQUIREMENTS = eINSTANCE.getSecurityModel_SecurityRequirements();

		/**
		 * The meta object literal for the '<em><b>Security Attributes</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_MODEL__SECURITY_ATTRIBUTES = eINSTANCE.getSecurityModel_SecurityAttributes();

		/**
		 * The meta object literal for the '<em><b>Raw Security Metrics</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_MODEL__RAW_SECURITY_METRICS = eINSTANCE.getSecurityModel_RawSecurityMetrics();

		/**
		 * The meta object literal for the '<em><b>Composite Security Metrics</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_MODEL__COMPOSITE_SECURITY_METRICS = eINSTANCE.getSecurityModel_CompositeSecurityMetrics();

		/**
		 * The meta object literal for the '<em><b>Security Metric Instances</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_MODEL__SECURITY_METRIC_INSTANCES = eINSTANCE.getSecurityModel_SecurityMetricInstances();

		/**
		 * The meta object literal for the '<em><b>Security Domains</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_MODEL__SECURITY_DOMAINS = eINSTANCE.getSecurityModel_SecurityDomains();

		/**
		 * The meta object literal for the '<em><b>Security SL Os</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_MODEL__SECURITY_SL_OS = eINSTANCE.getSecurityModel_SecuritySLOs();

		/**
		 * The meta object literal for the '{@link camel.security.impl.SecurityDomainImpl <em>Domain</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.security.impl.SecurityDomainImpl
		 * @see camel.security.impl.SecurityPackageImpl#getSecurityDomain()
		 * @generated
		 */
		EClass SECURITY_DOMAIN = eINSTANCE.getSecurityDomain();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SECURITY_DOMAIN__ID = eINSTANCE.getSecurityDomain_Id();

		/**
		 * The meta object literal for the '<em><b>Sub Domains</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_DOMAIN__SUB_DOMAINS = eINSTANCE.getSecurityDomain_SubDomains();

		/**
		 * The meta object literal for the '<em><b>Contains</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation SECURITY_DOMAIN___CONTAINS__SECURITYDOMAIN = eINSTANCE.getSecurityDomain__Contains__SecurityDomain();

		/**
		 * The meta object literal for the '{@link camel.security.impl.SecurityControlImpl <em>Control</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.security.impl.SecurityControlImpl
		 * @see camel.security.impl.SecurityPackageImpl#getSecurityControl()
		 * @generated
		 */
		EClass SECURITY_CONTROL = eINSTANCE.getSecurityControl();

		/**
		 * The meta object literal for the '<em><b>Id</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SECURITY_CONTROL__ID = eINSTANCE.getSecurityControl_Id();

		/**
		 * The meta object literal for the '<em><b>Domain</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_CONTROL__DOMAIN = eINSTANCE.getSecurityControl_Domain();

		/**
		 * The meta object literal for the '<em><b>Sub Domain</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_CONTROL__SUB_DOMAIN = eINSTANCE.getSecurityControl_SubDomain();

		/**
		 * The meta object literal for the '<em><b>Specification</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SECURITY_CONTROL__SPECIFICATION = eINSTANCE.getSecurityControl_Specification();

		/**
		 * The meta object literal for the '<em><b>Security Properties</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_CONTROL__SECURITY_PROPERTIES = eINSTANCE.getSecurityControl_SecurityProperties();

		/**
		 * The meta object literal for the '<em><b>Raw Security Metrics</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_CONTROL__RAW_SECURITY_METRICS = eINSTANCE.getSecurityControl_RawSecurityMetrics();

		/**
		 * The meta object literal for the '<em><b>Composite Security Metrics</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_CONTROL__COMPOSITE_SECURITY_METRICS = eINSTANCE.getSecurityControl_CompositeSecurityMetrics();

		/**
		 * The meta object literal for the '{@link camel.security.impl.SecurityMetricInstanceImpl <em>Metric Instance</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.security.impl.SecurityMetricInstanceImpl
		 * @see camel.security.impl.SecurityPackageImpl#getSecurityMetricInstance()
		 * @generated
		 */
		EClass SECURITY_METRIC_INSTANCE = eINSTANCE.getSecurityMetricInstance();

		/**
		 * The meta object literal for the '{@link camel.security.impl.RawSecurityMetricImpl <em>Raw Security Metric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.security.impl.RawSecurityMetricImpl
		 * @see camel.security.impl.SecurityPackageImpl#getRawSecurityMetric()
		 * @generated
		 */
		EClass RAW_SECURITY_METRIC = eINSTANCE.getRawSecurityMetric();

		/**
		 * The meta object literal for the '<em><b>Domain</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference RAW_SECURITY_METRIC__DOMAIN = eINSTANCE.getRawSecurityMetric_Domain();

		/**
		 * The meta object literal for the '{@link camel.security.impl.SecurityAttributeImpl <em>Attribute</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.security.impl.SecurityAttributeImpl
		 * @see camel.security.impl.SecurityPackageImpl#getSecurityAttribute()
		 * @generated
		 */
		EClass SECURITY_ATTRIBUTE = eINSTANCE.getSecurityAttribute();

		/**
		 * The meta object literal for the '<em><b>Domain</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_ATTRIBUTE__DOMAIN = eINSTANCE.getSecurityAttribute_Domain();

		/**
		 * The meta object literal for the '{@link camel.security.impl.CertifiableImpl <em>Certifiable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.security.impl.CertifiableImpl
		 * @see camel.security.impl.SecurityPackageImpl#getCertifiable()
		 * @generated
		 */
		EClass CERTIFIABLE = eINSTANCE.getCertifiable();

		/**
		 * The meta object literal for the '{@link camel.security.impl.SecuritySLOImpl <em>SLO</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.security.impl.SecuritySLOImpl
		 * @see camel.security.impl.SecurityPackageImpl#getSecuritySLO()
		 * @generated
		 */
		EClass SECURITY_SLO = eINSTANCE.getSecuritySLO();

		/**
		 * The meta object literal for the '{@link camel.security.impl.CompositeSecurityMetricImpl <em>Composite Security Metric</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.security.impl.CompositeSecurityMetricImpl
		 * @see camel.security.impl.SecurityPackageImpl#getCompositeSecurityMetric()
		 * @generated
		 */
		EClass COMPOSITE_SECURITY_METRIC = eINSTANCE.getCompositeSecurityMetric();

		/**
		 * The meta object literal for the '<em><b>Domain</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_SECURITY_METRIC__DOMAIN = eINSTANCE.getCompositeSecurityMetric_Domain();

	}

} //SecurityPackage
