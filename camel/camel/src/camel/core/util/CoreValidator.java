/**
 */
package camel.core.util;

import camel.core.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.core.CorePackage
 * @generated
 */
public class CoreValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final CoreValidator INSTANCE = new CoreValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.core";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CoreValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return CorePackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case CorePackage.MODEL:
				return validateModel((Model)value, diagnostics, context);
			case CorePackage.CAMEL_MODEL:
				return validateCamelModel((CamelModel)value, diagnostics, context);
			case CorePackage.ACTION:
				return validateAction((Action)value, diagnostics, context);
			case CorePackage.APPLICATION:
				return validateApplication((Application)value, diagnostics, context);
			case CorePackage.NAMED_ELEMENT:
				return validateNamedElement((NamedElement)value, diagnostics, context);
			case CorePackage.FEATURE:
				return validateFeature((Feature)value, diagnostics, context);
			case CorePackage.ATTRIBUTE:
				return validateAttribute((Attribute)value, diagnostics, context);
			case CorePackage.QUALITY_ATTRIBUTE:
				return validateQualityAttribute((QualityAttribute)value, diagnostics, context);
			case CorePackage.MEASURABLE_ATTRIBUTE:
				return validateMeasurableAttribute((MeasurableAttribute)value, diagnostics, context);
			case CorePackage.LAYER_TYPE:
				return validateLayerType((LayerType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateModel(Model model, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)model, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCamelModel(CamelModel camelModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)camelModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAction(Action action, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)action, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateApplication(Application application, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)application, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateNamedElement(NamedElement namedElement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)namedElement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateFeature(Feature feature, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)feature, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAttribute(Attribute attribute, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)attribute, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)attribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)attribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)attribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)attribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)attribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)attribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)attribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)attribute, diagnostics, context);
		if (result || diagnostics != null) result &= validateAttribute_attribute_must_have_at_least_value_or_value_type(attribute, diagnostics, context);
		if (result || diagnostics != null) result &= validateAttribute_attribute_value_in_value_type(attribute, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the attribute_must_have_at_least_value_or_value_type constraint of '<em>Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ATTRIBUTE__ATTRIBUTE_MUST_HAVE_AT_LEAST_VALUE_OR_VALUE_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Attribute: ' + self.name + ' does not have a value or value type',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\tasError((not(self.oclIsTypeOf(MeasurableAttribute)) implies (self.value <> null or valueType <> null)) and (self.oclIsTypeOf(MeasurableAttribute) implies self.value = null and self.valueType = null))\n" +
		"}.status";

	/**
	 * Validates the attribute_must_have_at_least_value_or_value_type constraint of '<em>Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAttribute_attribute_must_have_at_least_value_or_value_type(Attribute attribute, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CorePackage.Literals.ATTRIBUTE,
				 (EObject)attribute,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "attribute_must_have_at_least_value_or_value_type",
				 ATTRIBUTE__ATTRIBUTE_MUST_HAVE_AT_LEAST_VALUE_OR_VALUE_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * The cached validation expression for the attribute_value_in_value_type constraint of '<em>Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String ATTRIBUTE__ATTRIBUTE_VALUE_IN_VALUE_TYPE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Value  is not contained in ValueType in Attribute: ' + self.name,\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\tasError((value <> null and valueType <> null) implies self.checkValue(value, false))\n" +
		"}.status";

	/**
	 * Validates the attribute_value_in_value_type constraint of '<em>Attribute</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateAttribute_attribute_value_in_value_type(Attribute attribute, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(CorePackage.Literals.ATTRIBUTE,
				 (EObject)attribute,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "attribute_value_in_value_type",
				 ATTRIBUTE__ATTRIBUTE_VALUE_IN_VALUE_TYPE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateQualityAttribute(QualityAttribute qualityAttribute, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)qualityAttribute, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)qualityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)qualityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)qualityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)qualityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)qualityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)qualityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)qualityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)qualityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validateAttribute_attribute_must_have_at_least_value_or_value_type(qualityAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validateAttribute_attribute_value_in_value_type(qualityAttribute, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateMeasurableAttribute(MeasurableAttribute measurableAttribute, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)measurableAttribute, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)measurableAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)measurableAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)measurableAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)measurableAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)measurableAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)measurableAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)measurableAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)measurableAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validateAttribute_attribute_must_have_at_least_value_or_value_type(measurableAttribute, diagnostics, context);
		if (result || diagnostics != null) result &= validateAttribute_attribute_value_in_value_type(measurableAttribute, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLayerType(LayerType layerType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //CoreValidator
