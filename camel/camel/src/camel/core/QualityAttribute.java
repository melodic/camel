/**
 */
package camel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quality Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.core.CorePackage#getQualityAttribute()
 * @model
 * @generated
 */
public interface QualityAttribute extends Attribute {
} // QualityAttribute
