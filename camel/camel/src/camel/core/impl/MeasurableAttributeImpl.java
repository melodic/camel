/**
 */
package camel.core.impl;

import camel.core.CorePackage;
import camel.core.MeasurableAttribute;

import camel.metric.Sensor;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Measurable Attribute</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.core.impl.MeasurableAttributeImpl#getSensors <em>Sensors</em>}</li>
 * </ul>
 *
 * @generated
 */
public class MeasurableAttributeImpl extends QualityAttributeImpl implements MeasurableAttribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected MeasurableAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.MEASURABLE_ATTRIBUTE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Sensor> getSensors() {
		return (EList<Sensor>)eGet(CorePackage.Literals.MEASURABLE_ATTRIBUTE__SENSORS, true);
	}

} //MeasurableAttributeImpl
