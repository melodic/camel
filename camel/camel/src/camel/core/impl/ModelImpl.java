/**
 */
package camel.core.impl;

import camel.core.CorePackage;
import camel.core.Model;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.core.impl.ModelImpl#getImportURI <em>Import URI</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class ModelImpl extends FeatureImpl implements Model {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<String> getImportURI() {
		return (EList<String>)eGet(CorePackage.Literals.MODEL__IMPORT_URI, true);
	}

} //ModelImpl
