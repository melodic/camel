/**
 */
package camel.core.impl;

import camel.core.CorePackage;
import camel.core.QualityAttribute;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Quality Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class QualityAttributeImpl extends AttributeImpl implements QualityAttribute {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected QualityAttributeImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.QUALITY_ATTRIBUTE;
	}

} //QualityAttributeImpl
