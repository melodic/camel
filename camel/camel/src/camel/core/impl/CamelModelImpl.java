/**
 */
package camel.core.impl;

import camel.constraint.ConstraintModel;

import camel.core.Action;
import camel.core.Application;
import camel.core.CamelModel;
import camel.core.CorePackage;

import camel.data.DataModel;

import camel.deployment.DeploymentModel;

import camel.execution.ExecutionModel;

import camel.location.LocationModel;

import camel.metric.MetricModel;

import camel.mms.MetaDataModel;

import camel.organisation.OrganisationModel;

import camel.requirement.RequirementModel;

import camel.scalability.ScalabilityModel;

import camel.security.SecurityModel;

import camel.type.TypeModel;

import camel.unit.UnitModel;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Camel Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.core.impl.CamelModelImpl#getActions <em>Actions</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getApplication <em>Application</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getDeploymentModels <em>Deployment Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getExecutionModels <em>Execution Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getLocationModels <em>Location Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getMetricModels <em>Metric Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getOrganisationModels <em>Organisation Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getRequirementModels <em>Requirement Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getScalabilityModels <em>Scalability Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getSecurityModels <em>Security Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getTypeModels <em>Type Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getUnitModels <em>Unit Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getConstraintModels <em>Constraint Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getDataModels <em>Data Models</em>}</li>
 *   <li>{@link camel.core.impl.CamelModelImpl#getMetadataModels <em>Metadata Models</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CamelModelImpl extends ModelImpl implements CamelModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CamelModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CorePackage.Literals.CAMEL_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Action> getActions() {
		return (EList<Action>)eGet(CorePackage.Literals.CAMEL_MODEL__ACTIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Application getApplication() {
		return (Application)eGet(CorePackage.Literals.CAMEL_MODEL__APPLICATION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setApplication(Application newApplication) {
		eSet(CorePackage.Literals.CAMEL_MODEL__APPLICATION, newApplication);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DeploymentModel> getDeploymentModels() {
		return (EList<DeploymentModel>)eGet(CorePackage.Literals.CAMEL_MODEL__DEPLOYMENT_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ExecutionModel> getExecutionModels() {
		return (EList<ExecutionModel>)eGet(CorePackage.Literals.CAMEL_MODEL__EXECUTION_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<LocationModel> getLocationModels() {
		return (EList<LocationModel>)eGet(CorePackage.Literals.CAMEL_MODEL__LOCATION_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MetricModel> getMetricModels() {
		return (EList<MetricModel>)eGet(CorePackage.Literals.CAMEL_MODEL__METRIC_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<OrganisationModel> getOrganisationModels() {
		return (EList<OrganisationModel>)eGet(CorePackage.Literals.CAMEL_MODEL__ORGANISATION_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<RequirementModel> getRequirementModels() {
		return (EList<RequirementModel>)eGet(CorePackage.Literals.CAMEL_MODEL__REQUIREMENT_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ScalabilityModel> getScalabilityModels() {
		return (EList<ScalabilityModel>)eGet(CorePackage.Literals.CAMEL_MODEL__SCALABILITY_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityModel> getSecurityModels() {
		return (EList<SecurityModel>)eGet(CorePackage.Literals.CAMEL_MODEL__SECURITY_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<TypeModel> getTypeModels() {
		return (EList<TypeModel>)eGet(CorePackage.Literals.CAMEL_MODEL__TYPE_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<UnitModel> getUnitModels() {
		return (EList<UnitModel>)eGet(CorePackage.Literals.CAMEL_MODEL__UNIT_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<ConstraintModel> getConstraintModels() {
		return (EList<ConstraintModel>)eGet(CorePackage.Literals.CAMEL_MODEL__CONSTRAINT_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<DataModel> getDataModels() {
		return (EList<DataModel>)eGet(CorePackage.Literals.CAMEL_MODEL__DATA_MODELS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<MetaDataModel> getMetadataModels() {
		return (EList<MetaDataModel>)eGet(CorePackage.Literals.CAMEL_MODEL__METADATA_MODELS, true);
	}

} //CamelModelImpl
