/**
 */
package camel.core;

import camel.metric.Sensor;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Measurable Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.core.MeasurableAttribute#getSensors <em>Sensors</em>}</li>
 * </ul>
 *
 * @see camel.core.CorePackage#getMeasurableAttribute()
 * @model
 * @generated
 */
public interface MeasurableAttribute extends QualityAttribute {
	/**
	 * Returns the value of the '<em><b>Sensors</b></em>' reference list.
	 * The list contents are of type {@link camel.metric.Sensor}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sensors</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sensors</em>' reference list.
	 * @see camel.core.CorePackage#getMeasurableAttribute_Sensors()
	 * @model
	 * @generated
	 */
	EList<Sensor> getSensors();

} // MeasurableAttribute
