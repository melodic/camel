/**
 */
package camel.core;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Action</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.core.CorePackage#getAction()
 * @model abstract="true"
 * @generated
 */
public interface Action extends Feature {

} // Action
