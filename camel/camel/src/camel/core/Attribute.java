/**
 */
package camel.core;

import camel.type.Value;
import camel.type.ValueType;

import camel.unit.Unit;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Attribute</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.core.Attribute#getValue <em>Value</em>}</li>
 *   <li>{@link camel.core.Attribute#getValueType <em>Value Type</em>}</li>
 *   <li>{@link camel.core.Attribute#getUnit <em>Unit</em>}</li>
 * </ul>
 *
 * @see camel.core.CorePackage#getAttribute()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='attribute_must_have_at_least_value_or_value_type attribute_value_in_value_type'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot attribute_must_have_at_least_value_or_value_type='Tuple {\n\tmessage : String = \'Attribute: \' + self.name + \' does not have a value or value type\',\n\tstatus : Boolean = \n\t\t\tasError((not(self.oclIsTypeOf(MeasurableAttribute)) implies (self.value &lt;&gt; null or valueType &lt;&gt; null)) and (self.oclIsTypeOf(MeasurableAttribute) implies self.value = null and self.valueType = null))\n}.status' attribute_value_in_value_type='Tuple {\n\tmessage : String = \'Value  is not contained in ValueType in Attribute: \' + self.name,\n\tstatus : Boolean = \n\t\t\tasError((value &lt;&gt; null and valueType &lt;&gt; null) implies self.checkValue(value, false))\n}.status'"
 * @generated
 */
public interface Attribute extends NamedElement {
	/**
	 * Returns the value of the '<em><b>Value</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value</em>' containment reference.
	 * @see #setValue(Value)
	 * @see camel.core.CorePackage#getAttribute_Value()
	 * @model containment="true"
	 * @generated
	 */
	Value getValue();

	/**
	 * Sets the value of the '{@link camel.core.Attribute#getValue <em>Value</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value</em>' containment reference.
	 * @see #getValue()
	 * @generated
	 */
	void setValue(Value value);

	/**
	 * Returns the value of the '<em><b>Value Type</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Value Type</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Value Type</em>' reference.
	 * @see #setValueType(ValueType)
	 * @see camel.core.CorePackage#getAttribute_ValueType()
	 * @model
	 * @generated
	 */
	ValueType getValueType();

	/**
	 * Sets the value of the '{@link camel.core.Attribute#getValueType <em>Value Type</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Value Type</em>' reference.
	 * @see #getValueType()
	 * @generated
	 */
	void setValueType(ValueType value);

	/**
	 * Returns the value of the '<em><b>Unit</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit</em>' reference.
	 * @see #setUnit(Unit)
	 * @see camel.core.CorePackage#getAttribute_Unit()
	 * @model
	 * @generated
	 */
	Unit getUnit();

	/**
	 * Sets the value of the '{@link camel.core.Attribute#getUnit <em>Unit</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Unit</em>' reference.
	 * @see #getUnit()
	 * @generated
	 */
	void setUnit(Unit value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" vRequired="true" diffRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='if (self.valueType &lt;&gt; null)\n\t\t\t\t\t\tthen if (self.valueType.oclIsTypeOf(camel::type::Range))\n\t\t\t\t\t\t\tthen if (v.oclIsTypeOf(camel::type::BooleanValue) or v.oclIsTypeOf(camel::type::StringValue))\n\t\t\t\t\t\t\t\tthen false\n\t\t\t\t\t\t\t\telse if (v.oclIsTypeOf(camel::type::IntValue))\n\t\t\t\t\t\t\t\t\tthen\n\t\t\t\t\t\t\t\t\t\tself.valueType.oclAsType(camel::type::Range).includesValue(v.oclAsType(camel::type::IntValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\telse if (v.oclIsTypeOf(camel::type::FloatValue))\n\t\t\t\t\t\t\t\t\t\tthen\n\t\t\t\t\t\t\t\t\t\t\tself.valueType.oclAsType(camel::type::Range).includesValue(v.oclAsType(camel::type::FloatValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\t\telse\n\t\t\t\t\t\t\t\t\t\t\tself.valueType.oclAsType(camel::type::Range).includesValue(v.oclAsType(camel::type::DoubleValue).value)\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\telse if (self.valueType.oclIsTypeOf(camel::type::RangeUnion))\n\t\t\t\t\t\t\t\tthen if (v.oclIsTypeOf(camel::type::BooleanValue) or v.oclIsTypeOf(camel::type::StringValue))\n\t\t\t\t\t\t\t\t\tthen false\n\t\t\t\t\t\t\t\t\telse if (v.oclIsTypeOf(camel::type::IntValue))\n\t\t\t\t\t\t\t\t\t\tthen\n\t\t\t\t\t\t\t\t\t\t\tself.valueType.oclAsType(camel::type::RangeUnion).includesValue(v.oclAsType(camel::type::IntValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\t\telse if (v.oclIsTypeOf(camel::type::FloatValue))\n\t\t\t\t\t\t\t\t\t\t\tthen\n\t\t\t\t\t\t\t\t\t\t\t\tself.valueType.oclAsType(camel::type::RangeUnion).includesValue(v.oclAsType(camel::type::FloatValue).value.oclAsType(ecore::EDouble))\n\t\t\t\t\t\t\t\t\t\t\telse\n\t\t\t\t\t\t\t\t\t\t\t\tself.valueType.oclAsType(camel::type::RangeUnion).includesValue(v.oclAsType(camel::type::DoubleValue).value)\n\t\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\telse if (self.valueType.oclIsTypeOf(camel::type::List))\n\t\t\t\t\t\t\t\t\tthen (self.valueType.oclAsType(camel::type::List).checkValueType(v) and\n\t\t\t\t\t\t\t\t\t\tself.valueType.oclAsType(camel::type::List).includesValue(v))\n\t\t\t\t\t\t\t\t\telse if (self.valueType.oclIsTypeOf(camel::type::StringValueType))\n\t\t\t\t\t\t\t\t\t\t then v.oclIsTypeOf(camel::type::StringValue)\n\t\t\t\t\t\t\t\t\t\t else if (self.valueType.oclIsTypeOf(camel::type::BooleanValueType))\n\t\t\t\t\t\t\t\t\t\t\t\tthen v.oclIsTypeOf(camel::type::BooleanValue)\n\t\t\t\t\t\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\t\t\tendif\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\telse if (diff and self.value &lt;&gt; null)\n\t\t\t\t\t\t\tthen self.value.valueEquals(v)\n\t\t\t\t\t\t\telse false\n\t\t\t\t\t\t\tendif\n\t\t\t\t\t\tendif'"
	 * @generated
	 */
	boolean checkValue(Value v, boolean diff);

} // Attribute
