/**
 */
package camel.core;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.core.Model#getImportURI <em>Import URI</em>}</li>
 * </ul>
 *
 * @see camel.core.CorePackage#getModel()
 * @model abstract="true"
 * @generated
 */
public interface Model extends Feature {
	/**
	 * Returns the value of the '<em><b>Import URI</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Import URI</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Import URI</em>' attribute list.
	 * @see camel.core.CorePackage#getModel_ImportURI()
	 * @model
	 * @generated
	 */
	EList<String> getImportURI();

} // Model
