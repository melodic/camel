/**
 */
package camel.core;

import camel.constraint.ConstraintModel;

import camel.data.DataModel;

import camel.deployment.DeploymentModel;

import camel.execution.ExecutionModel;

import camel.location.LocationModel;

import camel.metric.MetricModel;

import camel.mms.MetaDataModel;

import camel.organisation.OrganisationModel;

import camel.requirement.RequirementModel;

import camel.scalability.ScalabilityModel;

import camel.security.SecurityModel;

import camel.type.TypeModel;

import camel.unit.UnitModel;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Camel Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.core.CamelModel#getActions <em>Actions</em>}</li>
 *   <li>{@link camel.core.CamelModel#getApplication <em>Application</em>}</li>
 *   <li>{@link camel.core.CamelModel#getDeploymentModels <em>Deployment Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getExecutionModels <em>Execution Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getLocationModels <em>Location Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getMetricModels <em>Metric Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getOrganisationModels <em>Organisation Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getRequirementModels <em>Requirement Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getScalabilityModels <em>Scalability Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getSecurityModels <em>Security Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getTypeModels <em>Type Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getUnitModels <em>Unit Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getConstraintModels <em>Constraint Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getDataModels <em>Data Models</em>}</li>
 *   <li>{@link camel.core.CamelModel#getMetadataModels <em>Metadata Models</em>}</li>
 * </ul>
 *
 * @see camel.core.CorePackage#getCamelModel()
 * @model
 * @generated
 */
public interface CamelModel extends Model {
	/**
	 * Returns the value of the '<em><b>Actions</b></em>' containment reference list.
	 * The list contents are of type {@link camel.core.Action}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Actions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Actions</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_Actions()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<Action> getActions();

	/**
	 * Returns the value of the '<em><b>Application</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Application</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Application</em>' containment reference.
	 * @see #setApplication(Application)
	 * @see camel.core.CorePackage#getCamelModel_Application()
	 * @model containment="true"
	 * @generated
	 */
	Application getApplication();

	/**
	 * Sets the value of the '{@link camel.core.CamelModel#getApplication <em>Application</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Application</em>' containment reference.
	 * @see #getApplication()
	 * @generated
	 */
	void setApplication(Application value);

	/**
	 * Returns the value of the '<em><b>Deployment Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.deployment.DeploymentModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Deployment Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Deployment Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_DeploymentModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<DeploymentModel> getDeploymentModels();

	/**
	 * Returns the value of the '<em><b>Execution Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.execution.ExecutionModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Execution Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Execution Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_ExecutionModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ExecutionModel> getExecutionModels();

	/**
	 * Returns the value of the '<em><b>Location Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.location.LocationModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Location Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Location Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_LocationModels()
	 * @model containment="true"
	 * @generated
	 */
	EList<LocationModel> getLocationModels();

	/**
	 * Returns the value of the '<em><b>Metric Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.metric.MetricModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_MetricModels()
	 * @model containment="true"
	 * @generated
	 */
	EList<MetricModel> getMetricModels();

	/**
	 * Returns the value of the '<em><b>Organisation Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.organisation.OrganisationModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Organisation Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Organisation Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_OrganisationModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<OrganisationModel> getOrganisationModels();

	/**
	 * Returns the value of the '<em><b>Requirement Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.requirement.RequirementModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Requirement Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Requirement Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_RequirementModels()
	 * @model containment="true"
	 * @generated
	 */
	EList<RequirementModel> getRequirementModels();

	/**
	 * Returns the value of the '<em><b>Scalability Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.scalability.ScalabilityModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Scalability Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Scalability Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_ScalabilityModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<ScalabilityModel> getScalabilityModels();

	/**
	 * Returns the value of the '<em><b>Security Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.security.SecurityModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_SecurityModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<SecurityModel> getSecurityModels();

	/**
	 * Returns the value of the '<em><b>Type Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.type.TypeModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Type Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Type Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_TypeModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<TypeModel> getTypeModels();

	/**
	 * Returns the value of the '<em><b>Unit Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.unit.UnitModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Unit Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Unit Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_UnitModels()
	 * @model containment="true" ordered="false"
	 * @generated
	 */
	EList<UnitModel> getUnitModels();

	/**
	 * Returns the value of the '<em><b>Constraint Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.constraint.ConstraintModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_ConstraintModels()
	 * @model containment="true"
	 * @generated
	 */
	EList<ConstraintModel> getConstraintModels();

	/**
	 * Returns the value of the '<em><b>Data Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.data.DataModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Data Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Data Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_DataModels()
	 * @model containment="true"
	 * @generated
	 */
	EList<DataModel> getDataModels();

	/**
	 * Returns the value of the '<em><b>Metadata Models</b></em>' containment reference list.
	 * The list contents are of type {@link camel.mms.MetaDataModel}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metadata Models</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metadata Models</em>' containment reference list.
	 * @see camel.core.CorePackage#getCamelModel_MetadataModels()
	 * @model containment="true"
	 * @generated
	 */
	EList<MetaDataModel> getMetadataModels();

} // CamelModel
