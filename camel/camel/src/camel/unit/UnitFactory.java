/**
 */
package camel.unit;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see camel.unit.UnitPackage
 * @generated
 */
public interface UnitFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UnitFactory eINSTANCE = camel.unit.impl.UnitFactoryImpl.init();

	/**
	 * Returns a new object of class '<em>Dimensionless</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dimensionless</em>'.
	 * @generated
	 */
	Dimensionless createDimensionless();

	/**
	 * Returns a new object of class '<em>Model</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Model</em>'.
	 * @generated
	 */
	UnitModel createUnitModel();

	/**
	 * Returns a new object of class '<em>Single Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Single Unit</em>'.
	 * @generated
	 */
	SingleUnit createSingleUnit();

	/**
	 * Returns a new object of class '<em>Composite Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Composite Unit</em>'.
	 * @generated
	 */
	CompositeUnit createCompositeUnit();

	/**
	 * Returns a new object of class '<em>Dimension</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Dimension</em>'.
	 * @generated
	 */
	UnitDimension createUnitDimension();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	UnitPackage getUnitPackage();

} //UnitFactory
