/**
 */
package camel.unit;

import camel.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see camel.unit.UnitFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface UnitPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "unit";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.camel-dsl.org/camel/unit";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "unit";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	UnitPackage eINSTANCE = camel.unit.impl.UnitPackageImpl.init();

	/**
	 * The meta object id for the '{@link camel.unit.impl.UnitImpl <em>Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.unit.impl.UnitImpl
	 * @see camel.unit.impl.UnitPackageImpl#getUnit()
	 * @generated
	 */
	int UNIT = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Multiple Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT__MULTIPLE_OF = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.unit.impl.DimensionlessImpl <em>Dimensionless</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.unit.impl.DimensionlessImpl
	 * @see camel.unit.impl.UnitPackageImpl#getDimensionless()
	 * @generated
	 */
	int DIMENSIONLESS = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONLESS__NAME = UNIT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONLESS__DESCRIPTION = UNIT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONLESS__ANNOTATIONS = UNIT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONLESS__ATTRIBUTES = UNIT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONLESS__SUB_FEATURES = UNIT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Multiple Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONLESS__MULTIPLE_OF = UNIT__MULTIPLE_OF;

	/**
	 * The number of structural features of the '<em>Dimensionless</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONLESS_FEATURE_COUNT = UNIT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONLESS___AS_ERROR__BOOLEAN = UNIT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Dimensionless</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONLESS_OPERATION_COUNT = UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.unit.impl.UnitModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.unit.impl.UnitModelImpl
	 * @see camel.unit.impl.UnitPackageImpl#getUnitModel()
	 * @generated
	 */
	int UNIT_MODEL = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_MODEL__NAME = CorePackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_MODEL__DESCRIPTION = CorePackage.MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_MODEL__ANNOTATIONS = CorePackage.MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_MODEL__ATTRIBUTES = CorePackage.MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_MODEL__SUB_FEATURES = CorePackage.MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_MODEL__IMPORT_URI = CorePackage.MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Units</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_MODEL__UNITS = CorePackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Dimensions</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_MODEL__DIMENSIONS = CorePackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_MODEL_FEATURE_COUNT = CorePackage.MODEL_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_MODEL___AS_ERROR__BOOLEAN = CorePackage.MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_MODEL_OPERATION_COUNT = CorePackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.unit.impl.DimensionedUnitImpl <em>Dimensioned Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.unit.impl.DimensionedUnitImpl
	 * @see camel.unit.impl.UnitPackageImpl#getDimensionedUnit()
	 * @generated
	 */
	int DIMENSIONED_UNIT = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONED_UNIT__NAME = UNIT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONED_UNIT__DESCRIPTION = UNIT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONED_UNIT__ANNOTATIONS = UNIT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONED_UNIT__ATTRIBUTES = UNIT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONED_UNIT__SUB_FEATURES = UNIT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Multiple Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONED_UNIT__MULTIPLE_OF = UNIT__MULTIPLE_OF;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONED_UNIT__DIMENSION = UNIT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Dimensioned Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONED_UNIT_FEATURE_COUNT = UNIT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONED_UNIT___AS_ERROR__BOOLEAN = UNIT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Dimensioned Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DIMENSIONED_UNIT_OPERATION_COUNT = UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.unit.impl.SingleUnitImpl <em>Single Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.unit.impl.SingleUnitImpl
	 * @see camel.unit.impl.UnitPackageImpl#getSingleUnit()
	 * @generated
	 */
	int SINGLE_UNIT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_UNIT__NAME = DIMENSIONED_UNIT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_UNIT__DESCRIPTION = DIMENSIONED_UNIT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_UNIT__ANNOTATIONS = DIMENSIONED_UNIT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_UNIT__ATTRIBUTES = DIMENSIONED_UNIT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_UNIT__SUB_FEATURES = DIMENSIONED_UNIT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Multiple Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_UNIT__MULTIPLE_OF = DIMENSIONED_UNIT__MULTIPLE_OF;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_UNIT__DIMENSION = DIMENSIONED_UNIT__DIMENSION;

	/**
	 * The number of structural features of the '<em>Single Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_UNIT_FEATURE_COUNT = DIMENSIONED_UNIT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_UNIT___AS_ERROR__BOOLEAN = DIMENSIONED_UNIT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Single Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SINGLE_UNIT_OPERATION_COUNT = DIMENSIONED_UNIT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.unit.impl.CompositeUnitImpl <em>Composite Unit</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.unit.impl.CompositeUnitImpl
	 * @see camel.unit.impl.UnitPackageImpl#getCompositeUnit()
	 * @generated
	 */
	int COMPOSITE_UNIT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT__NAME = DIMENSIONED_UNIT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT__DESCRIPTION = DIMENSIONED_UNIT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT__ANNOTATIONS = DIMENSIONED_UNIT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT__ATTRIBUTES = DIMENSIONED_UNIT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT__SUB_FEATURES = DIMENSIONED_UNIT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Multiple Of</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT__MULTIPLE_OF = DIMENSIONED_UNIT__MULTIPLE_OF;

	/**
	 * The feature id for the '<em><b>Dimension</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT__DIMENSION = DIMENSIONED_UNIT__DIMENSION;

	/**
	 * The feature id for the '<em><b>Component Units</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT__COMPONENT_UNITS = DIMENSIONED_UNIT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Formula</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT__FORMULA = DIMENSIONED_UNIT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Composite Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT_FEATURE_COUNT = DIMENSIONED_UNIT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT___AS_ERROR__BOOLEAN = DIMENSIONED_UNIT___AS_ERROR__BOOLEAN;

	/**
	 * The operation id for the '<em>Includes Unit</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT___INCLUDES_UNIT__UNIT = DIMENSIONED_UNIT_OPERATION_COUNT + 0;

	/**
	 * The number of operations of the '<em>Composite Unit</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int COMPOSITE_UNIT_OPERATION_COUNT = DIMENSIONED_UNIT_OPERATION_COUNT + 1;

	/**
	 * The meta object id for the '{@link camel.unit.impl.UnitDimensionImpl <em>Dimension</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.unit.impl.UnitDimensionImpl
	 * @see camel.unit.impl.UnitPackageImpl#getUnitDimension()
	 * @generated
	 */
	int UNIT_DIMENSION = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_DIMENSION__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_DIMENSION__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_DIMENSION__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_DIMENSION__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_DIMENSION__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Dimension</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_DIMENSION_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_DIMENSION___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Dimension</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIT_DIMENSION_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link camel.unit.Unit <em>Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unit</em>'.
	 * @see camel.unit.Unit
	 * @generated
	 */
	EClass getUnit();

	/**
	 * Returns the meta object for the reference list '{@link camel.unit.Unit#getMultipleOf <em>Multiple Of</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Multiple Of</em>'.
	 * @see camel.unit.Unit#getMultipleOf()
	 * @see #getUnit()
	 * @generated
	 */
	EReference getUnit_MultipleOf();

	/**
	 * Returns the meta object for class '{@link camel.unit.Dimensionless <em>Dimensionless</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dimensionless</em>'.
	 * @see camel.unit.Dimensionless
	 * @generated
	 */
	EClass getDimensionless();

	/**
	 * Returns the meta object for class '{@link camel.unit.UnitModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see camel.unit.UnitModel
	 * @generated
	 */
	EClass getUnitModel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.unit.UnitModel#getUnits <em>Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Units</em>'.
	 * @see camel.unit.UnitModel#getUnits()
	 * @see #getUnitModel()
	 * @generated
	 */
	EReference getUnitModel_Units();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.unit.UnitModel#getDimensions <em>Dimensions</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Dimensions</em>'.
	 * @see camel.unit.UnitModel#getDimensions()
	 * @see #getUnitModel()
	 * @generated
	 */
	EReference getUnitModel_Dimensions();

	/**
	 * Returns the meta object for class '{@link camel.unit.DimensionedUnit <em>Dimensioned Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dimensioned Unit</em>'.
	 * @see camel.unit.DimensionedUnit
	 * @generated
	 */
	EClass getDimensionedUnit();

	/**
	 * Returns the meta object for the reference '{@link camel.unit.DimensionedUnit#getDimension <em>Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Dimension</em>'.
	 * @see camel.unit.DimensionedUnit#getDimension()
	 * @see #getDimensionedUnit()
	 * @generated
	 */
	EReference getDimensionedUnit_Dimension();

	/**
	 * Returns the meta object for class '{@link camel.unit.SingleUnit <em>Single Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Single Unit</em>'.
	 * @see camel.unit.SingleUnit
	 * @generated
	 */
	EClass getSingleUnit();

	/**
	 * Returns the meta object for class '{@link camel.unit.CompositeUnit <em>Composite Unit</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Composite Unit</em>'.
	 * @see camel.unit.CompositeUnit
	 * @generated
	 */
	EClass getCompositeUnit();

	/**
	 * Returns the meta object for the reference list '{@link camel.unit.CompositeUnit#getComponentUnits <em>Component Units</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Component Units</em>'.
	 * @see camel.unit.CompositeUnit#getComponentUnits()
	 * @see #getCompositeUnit()
	 * @generated
	 */
	EReference getCompositeUnit_ComponentUnits();

	/**
	 * Returns the meta object for the attribute '{@link camel.unit.CompositeUnit#getFormula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Formula</em>'.
	 * @see camel.unit.CompositeUnit#getFormula()
	 * @see #getCompositeUnit()
	 * @generated
	 */
	EAttribute getCompositeUnit_Formula();

	/**
	 * Returns the meta object for the '{@link camel.unit.CompositeUnit#includesUnit(camel.unit.Unit) <em>Includes Unit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the '<em>Includes Unit</em>' operation.
	 * @see camel.unit.CompositeUnit#includesUnit(camel.unit.Unit)
	 * @generated
	 */
	EOperation getCompositeUnit__IncludesUnit__Unit();

	/**
	 * Returns the meta object for class '{@link camel.unit.UnitDimension <em>Dimension</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Dimension</em>'.
	 * @see camel.unit.UnitDimension
	 * @generated
	 */
	EClass getUnitDimension();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	UnitFactory getUnitFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link camel.unit.impl.UnitImpl <em>Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.unit.impl.UnitImpl
		 * @see camel.unit.impl.UnitPackageImpl#getUnit()
		 * @generated
		 */
		EClass UNIT = eINSTANCE.getUnit();

		/**
		 * The meta object literal for the '<em><b>Multiple Of</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT__MULTIPLE_OF = eINSTANCE.getUnit_MultipleOf();

		/**
		 * The meta object literal for the '{@link camel.unit.impl.DimensionlessImpl <em>Dimensionless</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.unit.impl.DimensionlessImpl
		 * @see camel.unit.impl.UnitPackageImpl#getDimensionless()
		 * @generated
		 */
		EClass DIMENSIONLESS = eINSTANCE.getDimensionless();

		/**
		 * The meta object literal for the '{@link camel.unit.impl.UnitModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.unit.impl.UnitModelImpl
		 * @see camel.unit.impl.UnitPackageImpl#getUnitModel()
		 * @generated
		 */
		EClass UNIT_MODEL = eINSTANCE.getUnitModel();

		/**
		 * The meta object literal for the '<em><b>Units</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT_MODEL__UNITS = eINSTANCE.getUnitModel_Units();

		/**
		 * The meta object literal for the '<em><b>Dimensions</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNIT_MODEL__DIMENSIONS = eINSTANCE.getUnitModel_Dimensions();

		/**
		 * The meta object literal for the '{@link camel.unit.impl.DimensionedUnitImpl <em>Dimensioned Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.unit.impl.DimensionedUnitImpl
		 * @see camel.unit.impl.UnitPackageImpl#getDimensionedUnit()
		 * @generated
		 */
		EClass DIMENSIONED_UNIT = eINSTANCE.getDimensionedUnit();

		/**
		 * The meta object literal for the '<em><b>Dimension</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference DIMENSIONED_UNIT__DIMENSION = eINSTANCE.getDimensionedUnit_Dimension();

		/**
		 * The meta object literal for the '{@link camel.unit.impl.SingleUnitImpl <em>Single Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.unit.impl.SingleUnitImpl
		 * @see camel.unit.impl.UnitPackageImpl#getSingleUnit()
		 * @generated
		 */
		EClass SINGLE_UNIT = eINSTANCE.getSingleUnit();

		/**
		 * The meta object literal for the '{@link camel.unit.impl.CompositeUnitImpl <em>Composite Unit</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.unit.impl.CompositeUnitImpl
		 * @see camel.unit.impl.UnitPackageImpl#getCompositeUnit()
		 * @generated
		 */
		EClass COMPOSITE_UNIT = eINSTANCE.getCompositeUnit();

		/**
		 * The meta object literal for the '<em><b>Component Units</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference COMPOSITE_UNIT__COMPONENT_UNITS = eINSTANCE.getCompositeUnit_ComponentUnits();

		/**
		 * The meta object literal for the '<em><b>Formula</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute COMPOSITE_UNIT__FORMULA = eINSTANCE.getCompositeUnit_Formula();

		/**
		 * The meta object literal for the '<em><b>Includes Unit</b></em>' operation.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EOperation COMPOSITE_UNIT___INCLUDES_UNIT__UNIT = eINSTANCE.getCompositeUnit__IncludesUnit__Unit();

		/**
		 * The meta object literal for the '{@link camel.unit.impl.UnitDimensionImpl <em>Dimension</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.unit.impl.UnitDimensionImpl
		 * @see camel.unit.impl.UnitPackageImpl#getUnitDimension()
		 * @generated
		 */
		EClass UNIT_DIMENSION = eINSTANCE.getUnitDimension();

	}

} //UnitPackage
