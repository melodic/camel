/**
 */
package camel.unit;

import camel.core.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.unit.UnitModel#getUnits <em>Units</em>}</li>
 *   <li>{@link camel.unit.UnitModel#getDimensions <em>Dimensions</em>}</li>
 * </ul>
 *
 * @see camel.unit.UnitPackage#getUnitModel()
 * @model
 * @generated
 */
public interface UnitModel extends Model {
	/**
	 * Returns the value of the '<em><b>Units</b></em>' containment reference list.
	 * The list contents are of type {@link camel.unit.Unit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Units</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Units</em>' containment reference list.
	 * @see camel.unit.UnitPackage#getUnitModel_Units()
	 * @model containment="true"
	 * @generated
	 */
	EList<Unit> getUnits();

	/**
	 * Returns the value of the '<em><b>Dimensions</b></em>' containment reference list.
	 * The list contents are of type {@link camel.unit.UnitDimension}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimensions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dimensions</em>' containment reference list.
	 * @see camel.unit.UnitPackage#getUnitModel_Dimensions()
	 * @model containment="true"
	 * @generated
	 */
	EList<UnitDimension> getDimensions();

} // UnitModel
