/**
 */
package camel.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dimensioned Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.unit.DimensionedUnit#getDimension <em>Dimension</em>}</li>
 * </ul>
 *
 * @see camel.unit.UnitPackage#getDimensionedUnit()
 * @model abstract="true"
 * @generated
 */
public interface DimensionedUnit extends Unit {
	/**
	 * Returns the value of the '<em><b>Dimension</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Dimension</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Dimension</em>' reference.
	 * @see #setDimension(UnitDimension)
	 * @see camel.unit.UnitPackage#getDimensionedUnit_Dimension()
	 * @model
	 * @generated
	 */
	UnitDimension getDimension();

	/**
	 * Sets the value of the '{@link camel.unit.DimensionedUnit#getDimension <em>Dimension</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Dimension</em>' reference.
	 * @see #getDimension()
	 * @generated
	 */
	void setDimension(UnitDimension value);

} // DimensionedUnit
