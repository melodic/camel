/**
 */
package camel.unit;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Single Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.unit.UnitPackage#getSingleUnit()
 * @model
 * @generated
 */
public interface SingleUnit extends DimensionedUnit {
} // SingleUnit
