/**
 */
package camel.unit;

import camel.core.Feature;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Dimension</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.unit.UnitPackage#getUnitDimension()
 * @model
 * @generated
 */
public interface UnitDimension extends Feature {
} // UnitDimension
