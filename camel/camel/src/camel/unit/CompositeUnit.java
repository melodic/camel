/**
 */
package camel.unit;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Composite Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.unit.CompositeUnit#getComponentUnits <em>Component Units</em>}</li>
 *   <li>{@link camel.unit.CompositeUnit#getFormula <em>Formula</em>}</li>
 * </ul>
 *
 * @see camel.unit.UnitPackage#getCompositeUnit()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='composite_unit_recursiv_itself'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot composite_unit_recursiv_itself='Tuple {\n\tmessage : String = \'Composite Unit: \' + self.name + \' should not contain recursively itself\'\n\t\t\t,\n\tstatus : Boolean = asError(not self.includesUnit(self))\n}.status'"
 * @generated
 */
public interface CompositeUnit extends DimensionedUnit {
	/**
	 * Returns the value of the '<em><b>Component Units</b></em>' reference list.
	 * The list contents are of type {@link camel.unit.Unit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Component Units</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Component Units</em>' reference list.
	 * @see camel.unit.UnitPackage#getCompositeUnit_ComponentUnits()
	 * @model
	 * @generated
	 */
	EList<Unit> getComponentUnits();

	/**
	 * Returns the value of the '<em><b>Formula</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formula</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formula</em>' attribute.
	 * @see #setFormula(String)
	 * @see camel.unit.UnitPackage#getCompositeUnit_Formula()
	 * @model required="true"
	 * @generated
	 */
	String getFormula();

	/**
	 * Sets the value of the '{@link camel.unit.CompositeUnit#getFormula <em>Formula</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Formula</em>' attribute.
	 * @see #getFormula()
	 * @generated
	 */
	void setFormula(String value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model required="true" uRequired="true"
	 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot body='\n\t\t\t\t\tif (self.componentUnits-&gt;exists(unit| u = unit or (unit.oclIsTypeOf(CompositeUnit) and unit.oclAsType(CompositeUnit).includesUnit(u)))) then true\n\t\t\t\t\telse false\n\t\t\t\t\tendif'"
	 * @generated
	 */
	boolean includesUnit(Unit u);

} // CompositeUnit
