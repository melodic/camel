/**
 */
package camel.unit;

import camel.core.Feature;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.unit.Unit#getMultipleOf <em>Multiple Of</em>}</li>
 * </ul>
 *
 * @see camel.unit.UnitPackage#getUnit()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='unit_multipleof_itself'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot unit_multipleof_itself='Tuple {\n\tmessage : String = \'Unit: \' + self.name + \' should not be a multiple of itself\'\n\t\t\t,\n\tstatus : Boolean = asError(multipleOf-&gt;forAll(p | p &lt;&gt; self))\n}.status'"
 * @generated
 */
public interface Unit extends Feature {
	/**
	 * Returns the value of the '<em><b>Multiple Of</b></em>' reference list.
	 * The list contents are of type {@link camel.unit.Unit}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Multiple Of</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Multiple Of</em>' reference list.
	 * @see camel.unit.UnitPackage#getUnit_MultipleOf()
	 * @model
	 * @generated
	 */
	EList<Unit> getMultipleOf();

} // Unit
