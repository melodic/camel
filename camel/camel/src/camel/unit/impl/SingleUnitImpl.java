/**
 */
package camel.unit.impl;

import camel.unit.SingleUnit;
import camel.unit.UnitPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Single Unit</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class SingleUnitImpl extends DimensionedUnitImpl implements SingleUnit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SingleUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.SINGLE_UNIT;
	}

} //SingleUnitImpl
