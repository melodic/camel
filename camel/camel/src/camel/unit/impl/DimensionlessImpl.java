/**
 */
package camel.unit.impl;

import camel.unit.Dimensionless;
import camel.unit.UnitPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dimensionless</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class DimensionlessImpl extends UnitImpl implements Dimensionless {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DimensionlessImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.DIMENSIONLESS;
	}

} //DimensionlessImpl
