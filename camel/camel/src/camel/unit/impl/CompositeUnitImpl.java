/**
 */
package camel.unit.impl;

import camel.unit.CompositeUnit;
import camel.unit.Unit;
import camel.unit.UnitPackage;

import java.lang.reflect.InvocationTargetException;
import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.common.util.WrappedException;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EOperation;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Composite Unit</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.unit.impl.CompositeUnitImpl#getComponentUnits <em>Component Units</em>}</li>
 *   <li>{@link camel.unit.impl.CompositeUnitImpl#getFormula <em>Formula</em>}</li>
 * </ul>
 *
 * @generated
 */
public class CompositeUnitImpl extends DimensionedUnitImpl implements CompositeUnit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected CompositeUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.COMPOSITE_UNIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Unit> getComponentUnits() {
		return (EList<Unit>)eGet(UnitPackage.Literals.COMPOSITE_UNIT__COMPONENT_UNITS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getFormula() {
		return (String)eGet(UnitPackage.Literals.COMPOSITE_UNIT__FORMULA, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setFormula(String newFormula) {
		eSet(UnitPackage.Literals.COMPOSITE_UNIT__FORMULA, newFormula);
	}

	/**
	 * The cached invocation delegate for the '{@link #includesUnit(camel.unit.Unit) <em>Includes Unit</em>}' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #includesUnit(camel.unit.Unit)
	 * @generated
	 * @ordered
	 */
	protected static final EOperation.Internal.InvocationDelegate INCLUDES_UNIT_UNIT__EINVOCATION_DELEGATE = ((EOperation.Internal)UnitPackage.Literals.COMPOSITE_UNIT___INCLUDES_UNIT__UNIT).getInvocationDelegate();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean includesUnit(Unit u) {
		try {
			return (Boolean)INCLUDES_UNIT_UNIT__EINVOCATION_DELEGATE.dynamicInvoke(this, new BasicEList.UnmodifiableEList<Object>(1, new Object[]{u}));
		}
		catch (InvocationTargetException ite) {
			throw new WrappedException(ite);
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eInvoke(int operationID, EList<?> arguments) throws InvocationTargetException {
		switch (operationID) {
			case UnitPackage.COMPOSITE_UNIT___INCLUDES_UNIT__UNIT:
				return includesUnit((Unit)arguments.get(0));
		}
		return super.eInvoke(operationID, arguments);
	}

} //CompositeUnitImpl
