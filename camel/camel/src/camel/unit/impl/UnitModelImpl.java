/**
 */
package camel.unit.impl;

import camel.core.impl.ModelImpl;

import camel.unit.Unit;
import camel.unit.UnitDimension;
import camel.unit.UnitModel;
import camel.unit.UnitPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.unit.impl.UnitModelImpl#getUnits <em>Units</em>}</li>
 *   <li>{@link camel.unit.impl.UnitModelImpl#getDimensions <em>Dimensions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class UnitModelImpl extends ModelImpl implements UnitModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnitModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.UNIT_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Unit> getUnits() {
		return (EList<Unit>)eGet(UnitPackage.Literals.UNIT_MODEL__UNITS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<UnitDimension> getDimensions() {
		return (EList<UnitDimension>)eGet(UnitPackage.Literals.UNIT_MODEL__DIMENSIONS, true);
	}

} //UnitModelImpl
