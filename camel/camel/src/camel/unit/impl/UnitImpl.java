/**
 */
package camel.unit.impl;

import camel.core.impl.FeatureImpl;

import camel.unit.Unit;
import camel.unit.UnitPackage;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Unit</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.unit.impl.UnitImpl#getMultipleOf <em>Multiple Of</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class UnitImpl extends FeatureImpl implements Unit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.UNIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Unit> getMultipleOf() {
		return (EList<Unit>)eGet(UnitPackage.Literals.UNIT__MULTIPLE_OF, true);
	}

} //UnitImpl
