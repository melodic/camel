/**
 */
package camel.unit.impl;

import camel.unit.DimensionedUnit;
import camel.unit.UnitDimension;
import camel.unit.UnitPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dimensioned Unit</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.unit.impl.DimensionedUnitImpl#getDimension <em>Dimension</em>}</li>
 * </ul>
 *
 * @generated
 */
public abstract class DimensionedUnitImpl extends UnitImpl implements DimensionedUnit {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DimensionedUnitImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.DIMENSIONED_UNIT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitDimension getDimension() {
		return (UnitDimension)eGet(UnitPackage.Literals.DIMENSIONED_UNIT__DIMENSION, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDimension(UnitDimension newDimension) {
		eSet(UnitPackage.Literals.DIMENSIONED_UNIT__DIMENSION, newDimension);
	}

} //DimensionedUnitImpl
