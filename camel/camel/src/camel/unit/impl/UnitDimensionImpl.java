/**
 */
package camel.unit.impl;

import camel.core.impl.FeatureImpl;

import camel.unit.UnitDimension;
import camel.unit.UnitPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Dimension</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class UnitDimensionImpl extends FeatureImpl implements UnitDimension {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected UnitDimensionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return UnitPackage.Literals.UNIT_DIMENSION;
	}

} //UnitDimensionImpl
