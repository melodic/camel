/**
 */
package camel.unit.impl;

import camel.unit.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class UnitFactoryImpl extends EFactoryImpl implements UnitFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static UnitFactory init() {
		try {
			UnitFactory theUnitFactory = (UnitFactory)EPackage.Registry.INSTANCE.getEFactory(UnitPackage.eNS_URI);
			if (theUnitFactory != null) {
				return theUnitFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new UnitFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case UnitPackage.DIMENSIONLESS: return (EObject)createDimensionless();
			case UnitPackage.UNIT_MODEL: return (EObject)createUnitModel();
			case UnitPackage.SINGLE_UNIT: return (EObject)createSingleUnit();
			case UnitPackage.COMPOSITE_UNIT: return (EObject)createCompositeUnit();
			case UnitPackage.UNIT_DIMENSION: return (EObject)createUnitDimension();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Dimensionless createDimensionless() {
		DimensionlessImpl dimensionless = new DimensionlessImpl();
		return dimensionless;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitModel createUnitModel() {
		UnitModelImpl unitModel = new UnitModelImpl();
		return unitModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SingleUnit createSingleUnit() {
		SingleUnitImpl singleUnit = new SingleUnitImpl();
		return singleUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CompositeUnit createCompositeUnit() {
		CompositeUnitImpl compositeUnit = new CompositeUnitImpl();
		return compositeUnit;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitDimension createUnitDimension() {
		UnitDimensionImpl unitDimension = new UnitDimensionImpl();
		return unitDimension;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitPackage getUnitPackage() {
		return (UnitPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static UnitPackage getPackage() {
		return UnitPackage.eINSTANCE;
	}

} //UnitFactoryImpl
