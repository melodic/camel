/**
 */
package camel.unit.util;

import camel.unit.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.unit.UnitPackage
 * @generated
 */
public class UnitValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final UnitValidator INSTANCE = new UnitValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.unit";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnitValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return UnitPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case UnitPackage.UNIT:
				return validateUnit((Unit)value, diagnostics, context);
			case UnitPackage.DIMENSIONLESS:
				return validateDimensionless((Dimensionless)value, diagnostics, context);
			case UnitPackage.UNIT_MODEL:
				return validateUnitModel((UnitModel)value, diagnostics, context);
			case UnitPackage.DIMENSIONED_UNIT:
				return validateDimensionedUnit((DimensionedUnit)value, diagnostics, context);
			case UnitPackage.SINGLE_UNIT:
				return validateSingleUnit((SingleUnit)value, diagnostics, context);
			case UnitPackage.COMPOSITE_UNIT:
				return validateCompositeUnit((CompositeUnit)value, diagnostics, context);
			case UnitPackage.UNIT_DIMENSION:
				return validateUnitDimension((UnitDimension)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnit(Unit unit, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)unit, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)unit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)unit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)unit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)unit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)unit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)unit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)unit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)unit, diagnostics, context);
		if (result || diagnostics != null) result &= validateUnit_unit_multipleof_itself(unit, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the unit_multipleof_itself constraint of '<em>Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String UNIT__UNIT_MULTIPLEOF_ITSELF__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Unit: ' + self.name + ' should not be a multiple of itself'\n" +
		"\t\t\t,\n" +
		"\tstatus : Boolean = asError(multipleOf->forAll(p | p <> self))\n" +
		"}.status";

	/**
	 * Validates the unit_multipleof_itself constraint of '<em>Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnit_unit_multipleof_itself(Unit unit, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(UnitPackage.Literals.UNIT,
				 (EObject)unit,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "unit_multipleof_itself",
				 UNIT__UNIT_MULTIPLEOF_ITSELF__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDimensionless(Dimensionless dimensionless, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)dimensionless, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)dimensionless, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)dimensionless, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)dimensionless, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)dimensionless, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)dimensionless, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)dimensionless, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)dimensionless, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)dimensionless, diagnostics, context);
		if (result || diagnostics != null) result &= validateUnit_unit_multipleof_itself(dimensionless, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnitModel(UnitModel unitModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)unitModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateDimensionedUnit(DimensionedUnit dimensionedUnit, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)dimensionedUnit, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)dimensionedUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)dimensionedUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)dimensionedUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)dimensionedUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)dimensionedUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)dimensionedUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)dimensionedUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)dimensionedUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validateUnit_unit_multipleof_itself(dimensionedUnit, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSingleUnit(SingleUnit singleUnit, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)singleUnit, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)singleUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)singleUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)singleUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)singleUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)singleUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)singleUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)singleUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)singleUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validateUnit_unit_multipleof_itself(singleUnit, diagnostics, context);
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeUnit(CompositeUnit compositeUnit, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)compositeUnit, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)compositeUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)compositeUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)compositeUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)compositeUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)compositeUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)compositeUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)compositeUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)compositeUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validateUnit_unit_multipleof_itself(compositeUnit, diagnostics, context);
		if (result || diagnostics != null) result &= validateCompositeUnit_composite_unit_recursiv_itself(compositeUnit, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the composite_unit_recursiv_itself constraint of '<em>Composite Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String COMPOSITE_UNIT__COMPOSITE_UNIT_RECURSIV_ITSELF__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Composite Unit: ' + self.name + ' should not contain recursively itself'\n" +
		"\t\t\t,\n" +
		"\tstatus : Boolean = asError(not self.includesUnit(self))\n" +
		"}.status";

	/**
	 * Validates the composite_unit_recursiv_itself constraint of '<em>Composite Unit</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCompositeUnit_composite_unit_recursiv_itself(CompositeUnit compositeUnit, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(UnitPackage.Literals.COMPOSITE_UNIT,
				 (EObject)compositeUnit,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "composite_unit_recursiv_itself",
				 COMPOSITE_UNIT__COMPOSITE_UNIT_RECURSIV_ITSELF__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateUnitDimension(UnitDimension unitDimension, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)unitDimension, diagnostics, context);
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //UnitValidator
