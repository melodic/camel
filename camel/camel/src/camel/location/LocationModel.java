/**
 */
package camel.location;

import camel.core.Model;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.location.LocationModel#getCloudLocations <em>Cloud Locations</em>}</li>
 *   <li>{@link camel.location.LocationModel#getRegions <em>Regions</em>}</li>
 * </ul>
 *
 * @see camel.location.LocationPackage#getLocationModel()
 * @model
 * @generated
 */
public interface LocationModel extends Model {
	/**
	 * Returns the value of the '<em><b>Cloud Locations</b></em>' containment reference list.
	 * The list contents are of type {@link camel.location.CloudLocation}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cloud Locations</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cloud Locations</em>' containment reference list.
	 * @see camel.location.LocationPackage#getLocationModel_CloudLocations()
	 * @model containment="true"
	 * @generated
	 */
	EList<CloudLocation> getCloudLocations();

	/**
	 * Returns the value of the '<em><b>Regions</b></em>' containment reference list.
	 * The list contents are of type {@link camel.location.GeographicalRegion}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Regions</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Regions</em>' containment reference list.
	 * @see camel.location.LocationPackage#getLocationModel_Regions()
	 * @model containment="true"
	 * @generated
	 */
	EList<GeographicalRegion> getRegions();

} // LocationModel
