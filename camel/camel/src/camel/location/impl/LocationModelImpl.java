/**
 */
package camel.location.impl;

import camel.core.impl.ModelImpl;

import camel.location.CloudLocation;
import camel.location.GeographicalRegion;
import camel.location.LocationModel;
import camel.location.LocationPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Model</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.location.impl.LocationModelImpl#getCloudLocations <em>Cloud Locations</em>}</li>
 *   <li>{@link camel.location.impl.LocationModelImpl#getRegions <em>Regions</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocationModelImpl extends ModelImpl implements LocationModel {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocationModelImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return LocationPackage.Literals.LOCATION_MODEL;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<CloudLocation> getCloudLocations() {
		return (EList<CloudLocation>)eGet(LocationPackage.Literals.LOCATION_MODEL__CLOUD_LOCATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<GeographicalRegion> getRegions() {
		return (EList<GeographicalRegion>)eGet(LocationPackage.Literals.LOCATION_MODEL__REGIONS, true);
	}

} //LocationModelImpl
