/**
 */
package camel.requirement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Horizontal Scale Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.HorizontalScaleRequirement#getMinInstances <em>Min Instances</em>}</li>
 *   <li>{@link camel.requirement.HorizontalScaleRequirement#getMaxInstances <em>Max Instances</em>}</li>
 * </ul>
 *
 * @see camel.requirement.RequirementPackage#getHorizontalScaleRequirement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='horiz_scale_requirement_min_max_enforcement'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot horiz_scale_requirement_min_max_enforcement='Tuple {\n\tmessage : String = \'HorizontalScaleRequirement: \' + self.name + \' has wrong values for the minInstances and/or maxInstance properties. The minInstances value should be non-negative, the maxInstances value positive or equal to -1, and when maxInstances value is positive, then minInstances value should not be greater than it\',\n\tstatus : Boolean = \n\t\t\t\t asError(self.minInstances &gt;= 0 and (self.maxInstances &gt; 0 or self.maxInstances = - 1) and \n\t\t\t\t(self.maxInstances &lt;&gt; - 1 implies self.minInstances&lt;= self.maxInstances))\n}.status'"
 * @generated
 */
public interface HorizontalScaleRequirement extends ScaleRequirement {
	/**
	 * Returns the value of the '<em><b>Min Instances</b></em>' attribute.
	 * The default value is <code>"0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Min Instances</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Min Instances</em>' attribute.
	 * @see #setMinInstances(int)
	 * @see camel.requirement.RequirementPackage#getHorizontalScaleRequirement_MinInstances()
	 * @model default="0" required="true"
	 * @generated
	 */
	int getMinInstances();

	/**
	 * Sets the value of the '{@link camel.requirement.HorizontalScaleRequirement#getMinInstances <em>Min Instances</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Min Instances</em>' attribute.
	 * @see #getMinInstances()
	 * @generated
	 */
	void setMinInstances(int value);

	/**
	 * Returns the value of the '<em><b>Max Instances</b></em>' attribute.
	 * The default value is <code>"-1"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Max Instances</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Max Instances</em>' attribute.
	 * @see #setMaxInstances(int)
	 * @see camel.requirement.RequirementPackage#getHorizontalScaleRequirement_MaxInstances()
	 * @model default="-1" required="true"
	 * @generated
	 */
	int getMaxInstances();

	/**
	 * Sets the value of the '{@link camel.requirement.HorizontalScaleRequirement#getMaxInstances <em>Max Instances</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Max Instances</em>' attribute.
	 * @see #getMaxInstances()
	 * @generated
	 */
	void setMaxInstances(int value);

} // HorizontalScaleRequirement
