/**
 */
package camel.requirement;

import camel.location.Location;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Location Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.LocationRequirement#getLocations <em>Locations</em>}</li>
 *   <li>{@link camel.requirement.LocationRequirement#isAllRequired <em>All Required</em>}</li>
 * </ul>
 *
 * @see camel.requirement.RequirementPackage#getLocationRequirement()
 * @model
 * @generated
 */
public interface LocationRequirement extends HardRequirement {
	/**
	 * Returns the value of the '<em><b>Locations</b></em>' reference list.
	 * The list contents are of type {@link camel.location.Location}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Locations</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Locations</em>' reference list.
	 * @see camel.requirement.RequirementPackage#getLocationRequirement_Locations()
	 * @model required="true"
	 * @generated
	 */
	EList<Location> getLocations();

	/**
	 * Returns the value of the '<em><b>All Required</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>All Required</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>All Required</em>' attribute.
	 * @see #setAllRequired(boolean)
	 * @see camel.requirement.RequirementPackage#getLocationRequirement_AllRequired()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isAllRequired();

	/**
	 * Sets the value of the '{@link camel.requirement.LocationRequirement#isAllRequired <em>All Required</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>All Required</em>' attribute.
	 * @see #isAllRequired()
	 * @generated
	 */
	void setAllRequired(boolean value);

} // LocationRequirement
