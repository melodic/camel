/**
 */
package camel.requirement;

import camel.metric.MetricContext;
import camel.metric.MetricVariable;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Optimisation Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.OptimisationRequirement#getMetricContext <em>Metric Context</em>}</li>
 *   <li>{@link camel.requirement.OptimisationRequirement#getMetricVariable <em>Metric Variable</em>}</li>
 *   <li>{@link camel.requirement.OptimisationRequirement#isMinimise <em>Minimise</em>}</li>
 * </ul>
 *
 * @see camel.requirement.RequirementPackage#getOptimisationRequirement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='optimization_requirement_metric_or_variable'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot optimization_requirement_metric_or_variable='Tuple {\n\tmessage : String = \'In OptimizationRequirement: \' + self.name + \' either a metric context or a metric variable must given\',\n\tstatus : Boolean = \n\t\t\t\tasError((metricContext &lt;&gt; null and metricVariable = null) xor (metricContext = null and metricVariable &lt;&gt; null))\n}.status'"
 * @generated
 */
public interface OptimisationRequirement extends SoftRequirement {
	/**
	 * Returns the value of the '<em><b>Metric Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Context</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Context</em>' reference.
	 * @see #setMetricContext(MetricContext)
	 * @see camel.requirement.RequirementPackage#getOptimisationRequirement_MetricContext()
	 * @model
	 * @generated
	 */
	MetricContext getMetricContext();

	/**
	 * Sets the value of the '{@link camel.requirement.OptimisationRequirement#getMetricContext <em>Metric Context</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metric Context</em>' reference.
	 * @see #getMetricContext()
	 * @generated
	 */
	void setMetricContext(MetricContext value);

	/**
	 * Returns the value of the '<em><b>Metric Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Metric Variable</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Metric Variable</em>' reference.
	 * @see #setMetricVariable(MetricVariable)
	 * @see camel.requirement.RequirementPackage#getOptimisationRequirement_MetricVariable()
	 * @model
	 * @generated
	 */
	MetricVariable getMetricVariable();

	/**
	 * Sets the value of the '{@link camel.requirement.OptimisationRequirement#getMetricVariable <em>Metric Variable</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Metric Variable</em>' reference.
	 * @see #getMetricVariable()
	 * @generated
	 */
	void setMetricVariable(MetricVariable value);

	/**
	 * Returns the value of the '<em><b>Minimise</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Minimise</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Minimise</em>' attribute.
	 * @see #setMinimise(boolean)
	 * @see camel.requirement.RequirementPackage#getOptimisationRequirement_Minimise()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isMinimise();

	/**
	 * Sets the value of the '{@link camel.requirement.OptimisationRequirement#isMinimise <em>Minimise</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Minimise</em>' attribute.
	 * @see #isMinimise()
	 * @generated
	 */
	void setMinimise(boolean value);

} // OptimisationRequirement
