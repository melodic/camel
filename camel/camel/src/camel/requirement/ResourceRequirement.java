/**
 */
package camel.requirement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Resource Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.requirement.RequirementPackage#getResourceRequirement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='resource_requirement'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot resource_requirement='Tuple {\n\tmessage : String = \'ResourceRequirement: \' + self.name + \' should have at least one feature or attribute being specified\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.subFeatures-&gt;size() &gt; 0 or self.attributes-&gt;size() &gt; 0)\n}.status'"
 * @generated
 */
public interface ResourceRequirement extends HardRequirement {
} // ResourceRequirement
