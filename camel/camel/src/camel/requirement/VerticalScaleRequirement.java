/**
 */
package camel.requirement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Vertical Scale Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see camel.requirement.RequirementPackage#getVerticalScaleRequirement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='vertical_scale_requirement'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot vertical_scale_requirement='Tuple {\n\tmessage : String = \'VerticalScaleRequirement: \' + self.name + \' should have at least one feature or attribute being specified\',\n\tstatus : Boolean = \n\t\t\t\tasError(self.subFeatures-&gt;size() &lt;&gt; 0 xor self.attributes-&gt;size() &lt;&gt; 0)\n}.status'"
 * @generated
 */
public interface VerticalScaleRequirement extends ScaleRequirement {

} // VerticalScaleRequirement
