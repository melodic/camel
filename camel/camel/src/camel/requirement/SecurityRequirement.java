/**
 */
package camel.requirement;

import camel.security.SecurityControl;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Security Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.SecurityRequirement#getSecurityControls <em>Security Controls</em>}</li>
 * </ul>
 *
 * @see camel.requirement.RequirementPackage#getSecurityRequirement()
 * @model
 * @generated
 */
public interface SecurityRequirement extends HardRequirement {
	/**
	 * Returns the value of the '<em><b>Security Controls</b></em>' reference list.
	 * The list contents are of type {@link camel.security.SecurityControl}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Security Controls</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Security Controls</em>' reference list.
	 * @see camel.requirement.RequirementPackage#getSecurityRequirement_SecurityControls()
	 * @model required="true"
	 * @generated
	 */
	EList<SecurityControl> getSecurityControls();

} // SecurityRequirement
