/**
 */
package camel.requirement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>OS Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.OSRequirement#getOs <em>Os</em>}</li>
 *   <li>{@link camel.requirement.OSRequirement#isIs64os <em>Is64os</em>}</li>
 * </ul>
 *
 * @see camel.requirement.RequirementPackage#getOSRequirement()
 * @model
 * @generated
 */
public interface OSRequirement extends HardRequirement {
	/**
	 * Returns the value of the '<em><b>Os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Os</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Os</em>' attribute.
	 * @see #setOs(String)
	 * @see camel.requirement.RequirementPackage#getOSRequirement_Os()
	 * @model required="true"
	 * @generated
	 */
	String getOs();

	/**
	 * Sets the value of the '{@link camel.requirement.OSRequirement#getOs <em>Os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Os</em>' attribute.
	 * @see #getOs()
	 * @generated
	 */
	void setOs(String value);

	/**
	 * Returns the value of the '<em><b>Is64os</b></em>' attribute.
	 * The default value is <code>"false"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Is64os</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Is64os</em>' attribute.
	 * @see #setIs64os(boolean)
	 * @see camel.requirement.RequirementPackage#getOSRequirement_Is64os()
	 * @model default="false" required="true"
	 * @generated
	 */
	boolean isIs64os();

	/**
	 * Sets the value of the '{@link camel.requirement.OSRequirement#isIs64os <em>Is64os</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Is64os</em>' attribute.
	 * @see #isIs64os()
	 * @generated
	 */
	void setIs64os(boolean value);

} // OSRequirement
