/**
 */
package camel.requirement.util;

import camel.requirement.*;

import java.util.Map;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.DiagnosticChain;
import org.eclipse.emf.common.util.ResourceLocator;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.util.EObjectValidator;
import org.eclipse.ocl.xtext.oclinecore.validation.OCLinEcoreEObjectValidator;

/**
 * <!-- begin-user-doc -->
 * The <b>Validator</b> for the model.
 * <!-- end-user-doc -->
 * @see camel.requirement.RequirementPackage
 * @generated
 */
public class RequirementValidator extends OCLinEcoreEObjectValidator {
	/**
	 * The cached model package
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static final RequirementValidator INSTANCE = new RequirementValidator();

	/**
	 * A constant for the {@link org.eclipse.emf.common.util.Diagnostic#getSource() source} of diagnostic {@link org.eclipse.emf.common.util.Diagnostic#getCode() codes} from this package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.common.util.Diagnostic#getSource()
	 * @see org.eclipse.emf.common.util.Diagnostic#getCode()
	 * @generated
	 */
	public static final String DIAGNOSTIC_SOURCE = "camel.requirement";

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static final int GENERATED_DIAGNOSTIC_CODE_COUNT = 0;

	/**
	 * A constant with a fixed name that can be used as the base value for additional hand written constants in a derived class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final int DIAGNOSTIC_CODE_COUNT = GENERATED_DIAGNOSTIC_CODE_COUNT;

	/**
	 * Creates an instance of the switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementValidator() {
		super();
	}

	/**
	 * Returns the package of this validator switch.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EPackage getEPackage() {
	  return RequirementPackage.eINSTANCE;
	}

	/**
	 * Calls <code>validateXXX</code> for the corresponding classifier of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected boolean validate(int classifierID, Object value, DiagnosticChain diagnostics, Map<Object, Object> context) {
		switch (classifierID) {
			case RequirementPackage.REQUIREMENT_MODEL:
				return validateRequirementModel((RequirementModel)value, diagnostics, context);
			case RequirementPackage.REQUIREMENT:
				return validateRequirement((Requirement)value, diagnostics, context);
			case RequirementPackage.HARD_REQUIREMENT:
				return validateHardRequirement((HardRequirement)value, diagnostics, context);
			case RequirementPackage.SERVICE_LEVEL_OBJECTIVE:
				return validateServiceLevelObjective((ServiceLevelObjective)value, diagnostics, context);
			case RequirementPackage.PROVIDER_REQUIREMENT:
				return validateProviderRequirement((ProviderRequirement)value, diagnostics, context);
			case RequirementPackage.OS_REQUIREMENT:
				return validateOSRequirement((OSRequirement)value, diagnostics, context);
			case RequirementPackage.SECURITY_REQUIREMENT:
				return validateSecurityRequirement((SecurityRequirement)value, diagnostics, context);
			case RequirementPackage.LOCATION_REQUIREMENT:
				return validateLocationRequirement((LocationRequirement)value, diagnostics, context);
			case RequirementPackage.IMAGE_REQUIREMENT:
				return validateImageRequirement((ImageRequirement)value, diagnostics, context);
			case RequirementPackage.SCALE_REQUIREMENT:
				return validateScaleRequirement((ScaleRequirement)value, diagnostics, context);
			case RequirementPackage.HORIZONTAL_SCALE_REQUIREMENT:
				return validateHorizontalScaleRequirement((HorizontalScaleRequirement)value, diagnostics, context);
			case RequirementPackage.VERTICAL_SCALE_REQUIREMENT:
				return validateVerticalScaleRequirement((VerticalScaleRequirement)value, diagnostics, context);
			case RequirementPackage.OPTIMISATION_REQUIREMENT:
				return validateOptimisationRequirement((OptimisationRequirement)value, diagnostics, context);
			case RequirementPackage.SOFT_REQUIREMENT:
				return validateSoftRequirement((SoftRequirement)value, diagnostics, context);
			case RequirementPackage.RESOURCE_REQUIREMENT:
				return validateResourceRequirement((ResourceRequirement)value, diagnostics, context);
			case RequirementPackage.PAA_SREQUIREMENT:
				return validatePaaSRequirement((PaaSRequirement)value, diagnostics, context);
			case RequirementPackage.CLOUD_TYPE:
				return validateCloudType((CloudType)value, diagnostics, context);
			default:
				return true;
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirementModel(RequirementModel requirementModel, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)requirementModel, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateRequirement(Requirement requirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)requirement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHardRequirement(HardRequirement hardRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)hardRequirement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateServiceLevelObjective(ServiceLevelObjective serviceLevelObjective, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)serviceLevelObjective, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)serviceLevelObjective, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)serviceLevelObjective, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)serviceLevelObjective, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)serviceLevelObjective, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)serviceLevelObjective, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)serviceLevelObjective, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)serviceLevelObjective, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)serviceLevelObjective, diagnostics, context);
		if (result || diagnostics != null) result &= validateServiceLevelObjective_slo_invalid_input(serviceLevelObjective, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the slo_invalid_input constraint of '<em>Service Level Objective</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SERVICE_LEVEL_OBJECTIVE__SLO_INVALID_INPUT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'SLO: ' + self.name + ' should have either a constraint or a violation event referenced',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((constraint <> null xor violationEvent <> null))\n" +
		"}.status";

	/**
	 * Validates the slo_invalid_input constraint of '<em>Service Level Objective</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateServiceLevelObjective_slo_invalid_input(ServiceLevelObjective serviceLevelObjective, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(RequirementPackage.Literals.SERVICE_LEVEL_OBJECTIVE,
				 (EObject)serviceLevelObjective,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "slo_invalid_input",
				 SERVICE_LEVEL_OBJECTIVE__SLO_INVALID_INPUT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProviderRequirement(ProviderRequirement providerRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)providerRequirement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)providerRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)providerRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)providerRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)providerRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)providerRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)providerRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)providerRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)providerRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validateProviderRequirement_provider_requirement_invalid_input(providerRequirement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the provider_requirement_invalid_input constraint of '<em>Provider Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PROVIDER_REQUIREMENT__PROVIDER_REQUIREMENT_INVALID_INPUT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Provider Requirement: ' + self.name + ' should specify at least either specific providers or provider names when its cloud type is null',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((cloudType = null or cloudType = CloudType::ANY) implies (providerNames->size() > 0))\n" +
		"}.status";

	/**
	 * Validates the provider_requirement_invalid_input constraint of '<em>Provider Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateProviderRequirement_provider_requirement_invalid_input(ProviderRequirement providerRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(RequirementPackage.Literals.PROVIDER_REQUIREMENT,
				 (EObject)providerRequirement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "provider_requirement_invalid_input",
				 PROVIDER_REQUIREMENT__PROVIDER_REQUIREMENT_INVALID_INPUT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOSRequirement(OSRequirement osRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)osRequirement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSecurityRequirement(SecurityRequirement securityRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)securityRequirement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateLocationRequirement(LocationRequirement locationRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)locationRequirement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateImageRequirement(ImageRequirement imageRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)imageRequirement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateScaleRequirement(ScaleRequirement scaleRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return validate_EveryDefaultConstraint((EObject)scaleRequirement, diagnostics, context);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHorizontalScaleRequirement(HorizontalScaleRequirement horizontalScaleRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)horizontalScaleRequirement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)horizontalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)horizontalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)horizontalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)horizontalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)horizontalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)horizontalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)horizontalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)horizontalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validateHorizontalScaleRequirement_horiz_scale_requirement_min_max_enforcement(horizontalScaleRequirement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the horiz_scale_requirement_min_max_enforcement constraint of '<em>Horizontal Scale Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String HORIZONTAL_SCALE_REQUIREMENT__HORIZ_SCALE_REQUIREMENT_MIN_MAX_ENFORCEMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'HorizontalScaleRequirement: ' + self.name + ' has wrong values for the minInstances and/or maxInstance properties. The minInstances value should be non-negative, the maxInstances value positive or equal to -1, and when maxInstances value is positive, then minInstances value should not be greater than it',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\t asError(self.minInstances >= 0 and (self.maxInstances > 0 or self.maxInstances = - 1) and \n" +
		"\t\t\t\t(self.maxInstances <> - 1 implies self.minInstances<= self.maxInstances))\n" +
		"}.status";

	/**
	 * Validates the horiz_scale_requirement_min_max_enforcement constraint of '<em>Horizontal Scale Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateHorizontalScaleRequirement_horiz_scale_requirement_min_max_enforcement(HorizontalScaleRequirement horizontalScaleRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(RequirementPackage.Literals.HORIZONTAL_SCALE_REQUIREMENT,
				 (EObject)horizontalScaleRequirement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "horiz_scale_requirement_min_max_enforcement",
				 HORIZONTAL_SCALE_REQUIREMENT__HORIZ_SCALE_REQUIREMENT_MIN_MAX_ENFORCEMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVerticalScaleRequirement(VerticalScaleRequirement verticalScaleRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)verticalScaleRequirement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)verticalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)verticalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)verticalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)verticalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)verticalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)verticalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)verticalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)verticalScaleRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validateVerticalScaleRequirement_vertical_scale_requirement(verticalScaleRequirement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the vertical_scale_requirement constraint of '<em>Vertical Scale Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String VERTICAL_SCALE_REQUIREMENT__VERTICAL_SCALE_REQUIREMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'VerticalScaleRequirement: ' + self.name + ' should have at least one feature or attribute being specified',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.subFeatures->size() <> 0 xor self.attributes->size() <> 0)\n" +
		"}.status";

	/**
	 * Validates the vertical_scale_requirement constraint of '<em>Vertical Scale Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateVerticalScaleRequirement_vertical_scale_requirement(VerticalScaleRequirement verticalScaleRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(RequirementPackage.Literals.VERTICAL_SCALE_REQUIREMENT,
				 (EObject)verticalScaleRequirement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "vertical_scale_requirement",
				 VERTICAL_SCALE_REQUIREMENT__VERTICAL_SCALE_REQUIREMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOptimisationRequirement(OptimisationRequirement optimisationRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)optimisationRequirement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)optimisationRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)optimisationRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)optimisationRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)optimisationRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)optimisationRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)optimisationRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)optimisationRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)optimisationRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validateSoftRequirement_non_negative_priorities_for_soft_requirement(optimisationRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validateOptimisationRequirement_optimization_requirement_metric_or_variable(optimisationRequirement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the optimization_requirement_metric_or_variable constraint of '<em>Optimisation Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String OPTIMISATION_REQUIREMENT__OPTIMIZATION_REQUIREMENT_METRIC_OR_VARIABLE__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'In OptimizationRequirement: ' + self.name + ' either a metric context or a metric variable must given',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError((metricContext <> null and metricVariable = null) xor (metricContext = null and metricVariable <> null))\n" +
		"}.status";

	/**
	 * Validates the optimization_requirement_metric_or_variable constraint of '<em>Optimisation Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateOptimisationRequirement_optimization_requirement_metric_or_variable(OptimisationRequirement optimisationRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(RequirementPackage.Literals.OPTIMISATION_REQUIREMENT,
				 (EObject)optimisationRequirement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "optimization_requirement_metric_or_variable",
				 OPTIMISATION_REQUIREMENT__OPTIMIZATION_REQUIREMENT_METRIC_OR_VARIABLE__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoftRequirement(SoftRequirement softRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)softRequirement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)softRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)softRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)softRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)softRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)softRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)softRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)softRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)softRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validateSoftRequirement_non_negative_priorities_for_soft_requirement(softRequirement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the non_negative_priorities_for_soft_requirement constraint of '<em>Soft Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String SOFT_REQUIREMENT__NON_NEGATIVE_PRIORITIES_FOR_SOFT_REQUIREMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'Requirement: ' + self.name + ' has a negative priority: ' + self.priority.toString(),\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.priority >= 0.0)\n" +
		"}.status";

	/**
	 * Validates the non_negative_priorities_for_soft_requirement constraint of '<em>Soft Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateSoftRequirement_non_negative_priorities_for_soft_requirement(SoftRequirement softRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(RequirementPackage.Literals.SOFT_REQUIREMENT,
				 (EObject)softRequirement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "non_negative_priorities_for_soft_requirement",
				 SOFT_REQUIREMENT__NON_NEGATIVE_PRIORITIES_FOR_SOFT_REQUIREMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceRequirement(ResourceRequirement resourceRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)resourceRequirement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)resourceRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)resourceRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)resourceRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)resourceRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)resourceRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)resourceRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)resourceRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)resourceRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validateResourceRequirement_resource_requirement(resourceRequirement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the resource_requirement constraint of '<em>Resource Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String RESOURCE_REQUIREMENT__RESOURCE_REQUIREMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'ResourceRequirement: ' + self.name + ' should have at least one feature or attribute being specified',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.subFeatures->size() > 0 or self.attributes->size() > 0)\n" +
		"}.status";

	/**
	 * Validates the resource_requirement constraint of '<em>Resource Requirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateResourceRequirement_resource_requirement(ResourceRequirement resourceRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(RequirementPackage.Literals.RESOURCE_REQUIREMENT,
				 (EObject)resourceRequirement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "resource_requirement",
				 RESOURCE_REQUIREMENT__RESOURCE_REQUIREMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePaaSRequirement(PaaSRequirement paaSRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		if (!validate_NoCircularContainment((EObject)paaSRequirement, diagnostics, context)) return false;
		boolean result = validate_EveryMultiplicityConforms((EObject)paaSRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryDataValueConforms((EObject)paaSRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryReferenceIsContained((EObject)paaSRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryBidirectionalReferenceIsPaired((EObject)paaSRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryProxyResolves((EObject)paaSRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_UniqueID((EObject)paaSRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryKeyUnique((EObject)paaSRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validate_EveryMapEntryUnique((EObject)paaSRequirement, diagnostics, context);
		if (result || diagnostics != null) result &= validatePaaSRequirement_paas_requirement(paaSRequirement, diagnostics, context);
		return result;
	}

	/**
	 * The cached validation expression for the paas_requirement constraint of '<em>Paa SRequirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected static final String PAA_SREQUIREMENT__PAAS_REQUIREMENT__EEXPRESSION = "Tuple {\n" +
		"\tmessage : String = 'PaaSRequirement: ' + self.name + ' should have at least one feature or attribute being specified',\n" +
		"\tstatus : Boolean = \n" +
		"\t\t\t\tasError(self.subFeatures->size() > 0 or self.attributes->size() > 0)\n" +
		"}.status";

	/**
	 * Validates the paas_requirement constraint of '<em>Paa SRequirement</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validatePaaSRequirement_paas_requirement(PaaSRequirement paaSRequirement, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return
			validate
				(RequirementPackage.Literals.PAA_SREQUIREMENT,
				 (EObject)paaSRequirement,
				 diagnostics,
				 context,
				 "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot",
				 "paas_requirement",
				 PAA_SREQUIREMENT__PAAS_REQUIREMENT__EEXPRESSION,
				 Diagnostic.ERROR,
				 DIAGNOSTIC_SOURCE,
				 0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean validateCloudType(CloudType cloudType, DiagnosticChain diagnostics, Map<Object, Object> context) {
		return true;
	}

	/**
	 * Returns the resource locator that will be used to fetch messages for this validator's diagnostics.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ResourceLocator getResourceLocator() {
		// TODO
		// Specialize this to return a resource locator for messages specific to this validator.
		// Ensure that you remove @generated or mark it @generated NOT
		return super.getResourceLocator();
	}

} //RequirementValidator
