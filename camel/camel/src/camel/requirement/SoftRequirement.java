/**
 */
package camel.requirement;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Soft Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.SoftRequirement#getPriority <em>Priority</em>}</li>
 * </ul>
 *
 * @see camel.requirement.RequirementPackage#getSoftRequirement()
 * @model abstract="true"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore constraints='non_negative_priorities_for_soft_requirement'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot non_negative_priorities_for_soft_requirement='Tuple {\n\tmessage : String = \'Requirement: \' + self.name + \' has a negative priority: \' + self.priority.toString(),\n\tstatus : Boolean = \n\t\t\t\tasError(self.priority &gt;= 0.0)\n}.status'"
 * @generated
 */
public interface SoftRequirement extends Requirement {
	/**
	 * Returns the value of the '<em><b>Priority</b></em>' attribute.
	 * The default value is <code>"0.0"</code>.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Priority</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Priority</em>' attribute.
	 * @see #setPriority(double)
	 * @see camel.requirement.RequirementPackage#getSoftRequirement_Priority()
	 * @model default="0.0" required="true"
	 * @generated
	 */
	double getPriority();

	/**
	 * Sets the value of the '{@link camel.requirement.SoftRequirement#getPriority <em>Priority</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Priority</em>' attribute.
	 * @see #getPriority()
	 * @generated
	 */
	void setPriority(double value);

} // SoftRequirement
