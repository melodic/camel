/**
 */
package camel.requirement;

import camel.core.CorePackage;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EEnum;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see camel.requirement.RequirementFactory
 * @model kind="package"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore invocationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' settingDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot' validationDelegates='http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot'"
 * @generated
 */
public interface RequirementPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "requirement";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://www.camel-dsl.org/camel/requirement";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "requirement";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	RequirementPackage eINSTANCE = camel.requirement.impl.RequirementPackageImpl.init();

	/**
	 * The meta object id for the '{@link camel.requirement.impl.RequirementModelImpl <em>Model</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.RequirementModelImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getRequirementModel()
	 * @generated
	 */
	int REQUIREMENT_MODEL = 0;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_MODEL__NAME = CorePackage.MODEL__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_MODEL__DESCRIPTION = CorePackage.MODEL__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_MODEL__ANNOTATIONS = CorePackage.MODEL__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_MODEL__ATTRIBUTES = CorePackage.MODEL__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_MODEL__SUB_FEATURES = CorePackage.MODEL__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Import URI</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_MODEL__IMPORT_URI = CorePackage.MODEL__IMPORT_URI;

	/**
	 * The feature id for the '<em><b>Requirements</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_MODEL__REQUIREMENTS = CorePackage.MODEL_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_MODEL_FEATURE_COUNT = CorePackage.MODEL_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_MODEL___AS_ERROR__BOOLEAN = CorePackage.MODEL___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Model</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_MODEL_OPERATION_COUNT = CorePackage.MODEL_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.RequirementImpl <em>Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.RequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getRequirement()
	 * @generated
	 */
	int REQUIREMENT = 1;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__NAME = CorePackage.FEATURE__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__DESCRIPTION = CorePackage.FEATURE__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__ANNOTATIONS = CorePackage.FEATURE__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__ATTRIBUTES = CorePackage.FEATURE__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT__SUB_FEATURES = CorePackage.FEATURE__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_FEATURE_COUNT = CorePackage.FEATURE_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT___AS_ERROR__BOOLEAN = CorePackage.FEATURE___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int REQUIREMENT_OPERATION_COUNT = CorePackage.FEATURE_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.HardRequirementImpl <em>Hard Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.HardRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getHardRequirement()
	 * @generated
	 */
	int HARD_REQUIREMENT = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_REQUIREMENT__NAME = REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_REQUIREMENT__DESCRIPTION = REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_REQUIREMENT__ANNOTATIONS = REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_REQUIREMENT__ATTRIBUTES = REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_REQUIREMENT__SUB_FEATURES = REQUIREMENT__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Hard Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_REQUIREMENT_FEATURE_COUNT = REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_REQUIREMENT___AS_ERROR__BOOLEAN = REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Hard Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HARD_REQUIREMENT_OPERATION_COUNT = REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.ServiceLevelObjectiveImpl <em>Service Level Objective</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.ServiceLevelObjectiveImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getServiceLevelObjective()
	 * @generated
	 */
	int SERVICE_LEVEL_OBJECTIVE = 3;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LEVEL_OBJECTIVE__NAME = HARD_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LEVEL_OBJECTIVE__DESCRIPTION = HARD_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LEVEL_OBJECTIVE__ANNOTATIONS = HARD_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LEVEL_OBJECTIVE__ATTRIBUTES = HARD_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LEVEL_OBJECTIVE__SUB_FEATURES = HARD_REQUIREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LEVEL_OBJECTIVE__CONSTRAINT = HARD_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Violation Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LEVEL_OBJECTIVE__VIOLATION_EVENT = HARD_REQUIREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Service Level Objective</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LEVEL_OBJECTIVE_FEATURE_COUNT = HARD_REQUIREMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LEVEL_OBJECTIVE___AS_ERROR__BOOLEAN = HARD_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Service Level Objective</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SERVICE_LEVEL_OBJECTIVE_OPERATION_COUNT = HARD_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.ProviderRequirementImpl <em>Provider Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.ProviderRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getProviderRequirement()
	 * @generated
	 */
	int PROVIDER_REQUIREMENT = 4;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_REQUIREMENT__NAME = HARD_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_REQUIREMENT__DESCRIPTION = HARD_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_REQUIREMENT__ANNOTATIONS = HARD_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_REQUIREMENT__ATTRIBUTES = HARD_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_REQUIREMENT__SUB_FEATURES = HARD_REQUIREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Provider Names</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_REQUIREMENT__PROVIDER_NAMES = HARD_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Cloud Type</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_REQUIREMENT__CLOUD_TYPE = HARD_REQUIREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Provider Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_REQUIREMENT_FEATURE_COUNT = HARD_REQUIREMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_REQUIREMENT___AS_ERROR__BOOLEAN = HARD_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Provider Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PROVIDER_REQUIREMENT_OPERATION_COUNT = HARD_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.OSRequirementImpl <em>OS Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.OSRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getOSRequirement()
	 * @generated
	 */
	int OS_REQUIREMENT = 5;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_REQUIREMENT__NAME = HARD_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_REQUIREMENT__DESCRIPTION = HARD_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_REQUIREMENT__ANNOTATIONS = HARD_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_REQUIREMENT__ATTRIBUTES = HARD_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_REQUIREMENT__SUB_FEATURES = HARD_REQUIREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_REQUIREMENT__OS = HARD_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Is64os</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_REQUIREMENT__IS64OS = HARD_REQUIREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>OS Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_REQUIREMENT_FEATURE_COUNT = HARD_REQUIREMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_REQUIREMENT___AS_ERROR__BOOLEAN = HARD_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>OS Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OS_REQUIREMENT_OPERATION_COUNT = HARD_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.SecurityRequirementImpl <em>Security Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.SecurityRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getSecurityRequirement()
	 * @generated
	 */
	int SECURITY_REQUIREMENT = 6;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_REQUIREMENT__NAME = HARD_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_REQUIREMENT__DESCRIPTION = HARD_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_REQUIREMENT__ANNOTATIONS = HARD_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_REQUIREMENT__ATTRIBUTES = HARD_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_REQUIREMENT__SUB_FEATURES = HARD_REQUIREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Security Controls</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_REQUIREMENT__SECURITY_CONTROLS = HARD_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Security Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_REQUIREMENT_FEATURE_COUNT = HARD_REQUIREMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_REQUIREMENT___AS_ERROR__BOOLEAN = HARD_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Security Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SECURITY_REQUIREMENT_OPERATION_COUNT = HARD_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.LocationRequirementImpl <em>Location Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.LocationRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getLocationRequirement()
	 * @generated
	 */
	int LOCATION_REQUIREMENT = 7;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_REQUIREMENT__NAME = HARD_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_REQUIREMENT__DESCRIPTION = HARD_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_REQUIREMENT__ANNOTATIONS = HARD_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_REQUIREMENT__ATTRIBUTES = HARD_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_REQUIREMENT__SUB_FEATURES = HARD_REQUIREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Locations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_REQUIREMENT__LOCATIONS = HARD_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>All Required</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_REQUIREMENT__ALL_REQUIRED = HARD_REQUIREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Location Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_REQUIREMENT_FEATURE_COUNT = HARD_REQUIREMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_REQUIREMENT___AS_ERROR__BOOLEAN = HARD_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Location Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int LOCATION_REQUIREMENT_OPERATION_COUNT = HARD_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.ImageRequirementImpl <em>Image Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.ImageRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getImageRequirement()
	 * @generated
	 */
	int IMAGE_REQUIREMENT = 8;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_REQUIREMENT__NAME = HARD_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_REQUIREMENT__DESCRIPTION = HARD_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_REQUIREMENT__ANNOTATIONS = HARD_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_REQUIREMENT__ATTRIBUTES = HARD_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_REQUIREMENT__SUB_FEATURES = HARD_REQUIREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Images</b></em>' attribute list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_REQUIREMENT__IMAGES = HARD_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Image Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_REQUIREMENT_FEATURE_COUNT = HARD_REQUIREMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_REQUIREMENT___AS_ERROR__BOOLEAN = HARD_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Image Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMAGE_REQUIREMENT_OPERATION_COUNT = HARD_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.ScaleRequirementImpl <em>Scale Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.ScaleRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getScaleRequirement()
	 * @generated
	 */
	int SCALE_REQUIREMENT = 9;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_REQUIREMENT__NAME = HARD_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_REQUIREMENT__DESCRIPTION = HARD_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_REQUIREMENT__ANNOTATIONS = HARD_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_REQUIREMENT__ATTRIBUTES = HARD_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_REQUIREMENT__SUB_FEATURES = HARD_REQUIREMENT__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Scale Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_REQUIREMENT_FEATURE_COUNT = HARD_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_REQUIREMENT___AS_ERROR__BOOLEAN = HARD_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Scale Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SCALE_REQUIREMENT_OPERATION_COUNT = HARD_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.HorizontalScaleRequirementImpl <em>Horizontal Scale Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.HorizontalScaleRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getHorizontalScaleRequirement()
	 * @generated
	 */
	int HORIZONTAL_SCALE_REQUIREMENT = 10;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALE_REQUIREMENT__NAME = SCALE_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALE_REQUIREMENT__DESCRIPTION = SCALE_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALE_REQUIREMENT__ANNOTATIONS = SCALE_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALE_REQUIREMENT__ATTRIBUTES = SCALE_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALE_REQUIREMENT__SUB_FEATURES = SCALE_REQUIREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Min Instances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALE_REQUIREMENT__MIN_INSTANCES = SCALE_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Max Instances</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALE_REQUIREMENT__MAX_INSTANCES = SCALE_REQUIREMENT_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Horizontal Scale Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALE_REQUIREMENT_FEATURE_COUNT = SCALE_REQUIREMENT_FEATURE_COUNT + 2;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALE_REQUIREMENT___AS_ERROR__BOOLEAN = SCALE_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Horizontal Scale Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int HORIZONTAL_SCALE_REQUIREMENT_OPERATION_COUNT = SCALE_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.VerticalScaleRequirementImpl <em>Vertical Scale Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.VerticalScaleRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getVerticalScaleRequirement()
	 * @generated
	 */
	int VERTICAL_SCALE_REQUIREMENT = 11;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALE_REQUIREMENT__NAME = SCALE_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALE_REQUIREMENT__DESCRIPTION = SCALE_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALE_REQUIREMENT__ANNOTATIONS = SCALE_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALE_REQUIREMENT__ATTRIBUTES = SCALE_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALE_REQUIREMENT__SUB_FEATURES = SCALE_REQUIREMENT__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Vertical Scale Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALE_REQUIREMENT_FEATURE_COUNT = SCALE_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALE_REQUIREMENT___AS_ERROR__BOOLEAN = SCALE_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Vertical Scale Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VERTICAL_SCALE_REQUIREMENT_OPERATION_COUNT = SCALE_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.SoftRequirementImpl <em>Soft Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.SoftRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getSoftRequirement()
	 * @generated
	 */
	int SOFT_REQUIREMENT = 13;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_REQUIREMENT__NAME = REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_REQUIREMENT__DESCRIPTION = REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_REQUIREMENT__ANNOTATIONS = REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_REQUIREMENT__ATTRIBUTES = REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_REQUIREMENT__SUB_FEATURES = REQUIREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_REQUIREMENT__PRIORITY = REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Soft Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_REQUIREMENT_FEATURE_COUNT = REQUIREMENT_FEATURE_COUNT + 1;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_REQUIREMENT___AS_ERROR__BOOLEAN = REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Soft Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int SOFT_REQUIREMENT_OPERATION_COUNT = REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.OptimisationRequirementImpl <em>Optimisation Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.OptimisationRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getOptimisationRequirement()
	 * @generated
	 */
	int OPTIMISATION_REQUIREMENT = 12;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT__NAME = SOFT_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT__DESCRIPTION = SOFT_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT__ANNOTATIONS = SOFT_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT__ATTRIBUTES = SOFT_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT__SUB_FEATURES = SOFT_REQUIREMENT__SUB_FEATURES;

	/**
	 * The feature id for the '<em><b>Priority</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT__PRIORITY = SOFT_REQUIREMENT__PRIORITY;

	/**
	 * The feature id for the '<em><b>Metric Context</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT__METRIC_CONTEXT = SOFT_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Metric Variable</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT__METRIC_VARIABLE = SOFT_REQUIREMENT_FEATURE_COUNT + 1;

	/**
	 * The feature id for the '<em><b>Minimise</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT__MINIMISE = SOFT_REQUIREMENT_FEATURE_COUNT + 2;

	/**
	 * The number of structural features of the '<em>Optimisation Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT_FEATURE_COUNT = SOFT_REQUIREMENT_FEATURE_COUNT + 3;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT___AS_ERROR__BOOLEAN = SOFT_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Optimisation Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int OPTIMISATION_REQUIREMENT_OPERATION_COUNT = SOFT_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.ResourceRequirementImpl <em>Resource Requirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.ResourceRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getResourceRequirement()
	 * @generated
	 */
	int RESOURCE_REQUIREMENT = 14;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_REQUIREMENT__NAME = HARD_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_REQUIREMENT__DESCRIPTION = HARD_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_REQUIREMENT__ANNOTATIONS = HARD_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_REQUIREMENT__ATTRIBUTES = HARD_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_REQUIREMENT__SUB_FEATURES = HARD_REQUIREMENT__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Resource Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_REQUIREMENT_FEATURE_COUNT = HARD_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_REQUIREMENT___AS_ERROR__BOOLEAN = HARD_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Resource Requirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int RESOURCE_REQUIREMENT_OPERATION_COUNT = HARD_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.impl.PaaSRequirementImpl <em>Paa SRequirement</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.impl.PaaSRequirementImpl
	 * @see camel.requirement.impl.RequirementPackageImpl#getPaaSRequirement()
	 * @generated
	 */
	int PAA_SREQUIREMENT = 15;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SREQUIREMENT__NAME = HARD_REQUIREMENT__NAME;

	/**
	 * The feature id for the '<em><b>Description</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SREQUIREMENT__DESCRIPTION = HARD_REQUIREMENT__DESCRIPTION;

	/**
	 * The feature id for the '<em><b>Annotations</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SREQUIREMENT__ANNOTATIONS = HARD_REQUIREMENT__ANNOTATIONS;

	/**
	 * The feature id for the '<em><b>Attributes</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SREQUIREMENT__ATTRIBUTES = HARD_REQUIREMENT__ATTRIBUTES;

	/**
	 * The feature id for the '<em><b>Sub Features</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SREQUIREMENT__SUB_FEATURES = HARD_REQUIREMENT__SUB_FEATURES;

	/**
	 * The number of structural features of the '<em>Paa SRequirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SREQUIREMENT_FEATURE_COUNT = HARD_REQUIREMENT_FEATURE_COUNT + 0;

	/**
	 * The operation id for the '<em>As Error</em>' operation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SREQUIREMENT___AS_ERROR__BOOLEAN = HARD_REQUIREMENT___AS_ERROR__BOOLEAN;

	/**
	 * The number of operations of the '<em>Paa SRequirement</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PAA_SREQUIREMENT_OPERATION_COUNT = HARD_REQUIREMENT_OPERATION_COUNT + 0;

	/**
	 * The meta object id for the '{@link camel.requirement.CloudType <em>Cloud Type</em>}' enum.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see camel.requirement.CloudType
	 * @see camel.requirement.impl.RequirementPackageImpl#getCloudType()
	 * @generated
	 */
	int CLOUD_TYPE = 16;


	/**
	 * Returns the meta object for class '{@link camel.requirement.RequirementModel <em>Model</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Model</em>'.
	 * @see camel.requirement.RequirementModel
	 * @generated
	 */
	EClass getRequirementModel();

	/**
	 * Returns the meta object for the containment reference list '{@link camel.requirement.RequirementModel#getRequirements <em>Requirements</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Requirements</em>'.
	 * @see camel.requirement.RequirementModel#getRequirements()
	 * @see #getRequirementModel()
	 * @generated
	 */
	EReference getRequirementModel_Requirements();

	/**
	 * Returns the meta object for class '{@link camel.requirement.Requirement <em>Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Requirement</em>'.
	 * @see camel.requirement.Requirement
	 * @generated
	 */
	EClass getRequirement();

	/**
	 * Returns the meta object for class '{@link camel.requirement.HardRequirement <em>Hard Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Hard Requirement</em>'.
	 * @see camel.requirement.HardRequirement
	 * @generated
	 */
	EClass getHardRequirement();

	/**
	 * Returns the meta object for class '{@link camel.requirement.ServiceLevelObjective <em>Service Level Objective</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Service Level Objective</em>'.
	 * @see camel.requirement.ServiceLevelObjective
	 * @generated
	 */
	EClass getServiceLevelObjective();

	/**
	 * Returns the meta object for the reference '{@link camel.requirement.ServiceLevelObjective#getConstraint <em>Constraint</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Constraint</em>'.
	 * @see camel.requirement.ServiceLevelObjective#getConstraint()
	 * @see #getServiceLevelObjective()
	 * @generated
	 */
	EReference getServiceLevelObjective_Constraint();

	/**
	 * Returns the meta object for the reference '{@link camel.requirement.ServiceLevelObjective#getViolationEvent <em>Violation Event</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Violation Event</em>'.
	 * @see camel.requirement.ServiceLevelObjective#getViolationEvent()
	 * @see #getServiceLevelObjective()
	 * @generated
	 */
	EReference getServiceLevelObjective_ViolationEvent();

	/**
	 * Returns the meta object for class '{@link camel.requirement.ProviderRequirement <em>Provider Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Provider Requirement</em>'.
	 * @see camel.requirement.ProviderRequirement
	 * @generated
	 */
	EClass getProviderRequirement();

	/**
	 * Returns the meta object for the attribute list '{@link camel.requirement.ProviderRequirement#getProviderNames <em>Provider Names</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Provider Names</em>'.
	 * @see camel.requirement.ProviderRequirement#getProviderNames()
	 * @see #getProviderRequirement()
	 * @generated
	 */
	EAttribute getProviderRequirement_ProviderNames();

	/**
	 * Returns the meta object for the attribute '{@link camel.requirement.ProviderRequirement#getCloudType <em>Cloud Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Cloud Type</em>'.
	 * @see camel.requirement.ProviderRequirement#getCloudType()
	 * @see #getProviderRequirement()
	 * @generated
	 */
	EAttribute getProviderRequirement_CloudType();

	/**
	 * Returns the meta object for class '{@link camel.requirement.OSRequirement <em>OS Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>OS Requirement</em>'.
	 * @see camel.requirement.OSRequirement
	 * @generated
	 */
	EClass getOSRequirement();

	/**
	 * Returns the meta object for the attribute '{@link camel.requirement.OSRequirement#getOs <em>Os</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Os</em>'.
	 * @see camel.requirement.OSRequirement#getOs()
	 * @see #getOSRequirement()
	 * @generated
	 */
	EAttribute getOSRequirement_Os();

	/**
	 * Returns the meta object for the attribute '{@link camel.requirement.OSRequirement#isIs64os <em>Is64os</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Is64os</em>'.
	 * @see camel.requirement.OSRequirement#isIs64os()
	 * @see #getOSRequirement()
	 * @generated
	 */
	EAttribute getOSRequirement_Is64os();

	/**
	 * Returns the meta object for class '{@link camel.requirement.SecurityRequirement <em>Security Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Security Requirement</em>'.
	 * @see camel.requirement.SecurityRequirement
	 * @generated
	 */
	EClass getSecurityRequirement();

	/**
	 * Returns the meta object for the reference list '{@link camel.requirement.SecurityRequirement#getSecurityControls <em>Security Controls</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Security Controls</em>'.
	 * @see camel.requirement.SecurityRequirement#getSecurityControls()
	 * @see #getSecurityRequirement()
	 * @generated
	 */
	EReference getSecurityRequirement_SecurityControls();

	/**
	 * Returns the meta object for class '{@link camel.requirement.LocationRequirement <em>Location Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Location Requirement</em>'.
	 * @see camel.requirement.LocationRequirement
	 * @generated
	 */
	EClass getLocationRequirement();

	/**
	 * Returns the meta object for the reference list '{@link camel.requirement.LocationRequirement#getLocations <em>Locations</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Locations</em>'.
	 * @see camel.requirement.LocationRequirement#getLocations()
	 * @see #getLocationRequirement()
	 * @generated
	 */
	EReference getLocationRequirement_Locations();

	/**
	 * Returns the meta object for the attribute '{@link camel.requirement.LocationRequirement#isAllRequired <em>All Required</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>All Required</em>'.
	 * @see camel.requirement.LocationRequirement#isAllRequired()
	 * @see #getLocationRequirement()
	 * @generated
	 */
	EAttribute getLocationRequirement_AllRequired();

	/**
	 * Returns the meta object for class '{@link camel.requirement.ImageRequirement <em>Image Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Image Requirement</em>'.
	 * @see camel.requirement.ImageRequirement
	 * @generated
	 */
	EClass getImageRequirement();

	/**
	 * Returns the meta object for the attribute list '{@link camel.requirement.ImageRequirement#getImages <em>Images</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute list '<em>Images</em>'.
	 * @see camel.requirement.ImageRequirement#getImages()
	 * @see #getImageRequirement()
	 * @generated
	 */
	EAttribute getImageRequirement_Images();

	/**
	 * Returns the meta object for class '{@link camel.requirement.ScaleRequirement <em>Scale Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Scale Requirement</em>'.
	 * @see camel.requirement.ScaleRequirement
	 * @generated
	 */
	EClass getScaleRequirement();

	/**
	 * Returns the meta object for class '{@link camel.requirement.HorizontalScaleRequirement <em>Horizontal Scale Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Horizontal Scale Requirement</em>'.
	 * @see camel.requirement.HorizontalScaleRequirement
	 * @generated
	 */
	EClass getHorizontalScaleRequirement();

	/**
	 * Returns the meta object for the attribute '{@link camel.requirement.HorizontalScaleRequirement#getMinInstances <em>Min Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Min Instances</em>'.
	 * @see camel.requirement.HorizontalScaleRequirement#getMinInstances()
	 * @see #getHorizontalScaleRequirement()
	 * @generated
	 */
	EAttribute getHorizontalScaleRequirement_MinInstances();

	/**
	 * Returns the meta object for the attribute '{@link camel.requirement.HorizontalScaleRequirement#getMaxInstances <em>Max Instances</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Max Instances</em>'.
	 * @see camel.requirement.HorizontalScaleRequirement#getMaxInstances()
	 * @see #getHorizontalScaleRequirement()
	 * @generated
	 */
	EAttribute getHorizontalScaleRequirement_MaxInstances();

	/**
	 * Returns the meta object for class '{@link camel.requirement.VerticalScaleRequirement <em>Vertical Scale Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Vertical Scale Requirement</em>'.
	 * @see camel.requirement.VerticalScaleRequirement
	 * @generated
	 */
	EClass getVerticalScaleRequirement();

	/**
	 * Returns the meta object for class '{@link camel.requirement.OptimisationRequirement <em>Optimisation Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Optimisation Requirement</em>'.
	 * @see camel.requirement.OptimisationRequirement
	 * @generated
	 */
	EClass getOptimisationRequirement();

	/**
	 * Returns the meta object for the reference '{@link camel.requirement.OptimisationRequirement#getMetricContext <em>Metric Context</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metric Context</em>'.
	 * @see camel.requirement.OptimisationRequirement#getMetricContext()
	 * @see #getOptimisationRequirement()
	 * @generated
	 */
	EReference getOptimisationRequirement_MetricContext();

	/**
	 * Returns the meta object for the reference '{@link camel.requirement.OptimisationRequirement#getMetricVariable <em>Metric Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Metric Variable</em>'.
	 * @see camel.requirement.OptimisationRequirement#getMetricVariable()
	 * @see #getOptimisationRequirement()
	 * @generated
	 */
	EReference getOptimisationRequirement_MetricVariable();

	/**
	 * Returns the meta object for the attribute '{@link camel.requirement.OptimisationRequirement#isMinimise <em>Minimise</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Minimise</em>'.
	 * @see camel.requirement.OptimisationRequirement#isMinimise()
	 * @see #getOptimisationRequirement()
	 * @generated
	 */
	EAttribute getOptimisationRequirement_Minimise();

	/**
	 * Returns the meta object for class '{@link camel.requirement.SoftRequirement <em>Soft Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Soft Requirement</em>'.
	 * @see camel.requirement.SoftRequirement
	 * @generated
	 */
	EClass getSoftRequirement();

	/**
	 * Returns the meta object for the attribute '{@link camel.requirement.SoftRequirement#getPriority <em>Priority</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Priority</em>'.
	 * @see camel.requirement.SoftRequirement#getPriority()
	 * @see #getSoftRequirement()
	 * @generated
	 */
	EAttribute getSoftRequirement_Priority();

	/**
	 * Returns the meta object for class '{@link camel.requirement.ResourceRequirement <em>Resource Requirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Resource Requirement</em>'.
	 * @see camel.requirement.ResourceRequirement
	 * @generated
	 */
	EClass getResourceRequirement();

	/**
	 * Returns the meta object for class '{@link camel.requirement.PaaSRequirement <em>Paa SRequirement</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Paa SRequirement</em>'.
	 * @see camel.requirement.PaaSRequirement
	 * @generated
	 */
	EClass getPaaSRequirement();

	/**
	 * Returns the meta object for enum '{@link camel.requirement.CloudType <em>Cloud Type</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for enum '<em>Cloud Type</em>'.
	 * @see camel.requirement.CloudType
	 * @generated
	 */
	EEnum getCloudType();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	RequirementFactory getRequirementFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link camel.requirement.impl.RequirementModelImpl <em>Model</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.RequirementModelImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getRequirementModel()
		 * @generated
		 */
		EClass REQUIREMENT_MODEL = eINSTANCE.getRequirementModel();

		/**
		 * The meta object literal for the '<em><b>Requirements</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference REQUIREMENT_MODEL__REQUIREMENTS = eINSTANCE.getRequirementModel_Requirements();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.RequirementImpl <em>Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.RequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getRequirement()
		 * @generated
		 */
		EClass REQUIREMENT = eINSTANCE.getRequirement();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.HardRequirementImpl <em>Hard Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.HardRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getHardRequirement()
		 * @generated
		 */
		EClass HARD_REQUIREMENT = eINSTANCE.getHardRequirement();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.ServiceLevelObjectiveImpl <em>Service Level Objective</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.ServiceLevelObjectiveImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getServiceLevelObjective()
		 * @generated
		 */
		EClass SERVICE_LEVEL_OBJECTIVE = eINSTANCE.getServiceLevelObjective();

		/**
		 * The meta object literal for the '<em><b>Constraint</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_LEVEL_OBJECTIVE__CONSTRAINT = eINSTANCE.getServiceLevelObjective_Constraint();

		/**
		 * The meta object literal for the '<em><b>Violation Event</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SERVICE_LEVEL_OBJECTIVE__VIOLATION_EVENT = eINSTANCE.getServiceLevelObjective_ViolationEvent();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.ProviderRequirementImpl <em>Provider Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.ProviderRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getProviderRequirement()
		 * @generated
		 */
		EClass PROVIDER_REQUIREMENT = eINSTANCE.getProviderRequirement();

		/**
		 * The meta object literal for the '<em><b>Provider Names</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROVIDER_REQUIREMENT__PROVIDER_NAMES = eINSTANCE.getProviderRequirement_ProviderNames();

		/**
		 * The meta object literal for the '<em><b>Cloud Type</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute PROVIDER_REQUIREMENT__CLOUD_TYPE = eINSTANCE.getProviderRequirement_CloudType();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.OSRequirementImpl <em>OS Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.OSRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getOSRequirement()
		 * @generated
		 */
		EClass OS_REQUIREMENT = eINSTANCE.getOSRequirement();

		/**
		 * The meta object literal for the '<em><b>Os</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_REQUIREMENT__OS = eINSTANCE.getOSRequirement_Os();

		/**
		 * The meta object literal for the '<em><b>Is64os</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OS_REQUIREMENT__IS64OS = eINSTANCE.getOSRequirement_Is64os();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.SecurityRequirementImpl <em>Security Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.SecurityRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getSecurityRequirement()
		 * @generated
		 */
		EClass SECURITY_REQUIREMENT = eINSTANCE.getSecurityRequirement();

		/**
		 * The meta object literal for the '<em><b>Security Controls</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference SECURITY_REQUIREMENT__SECURITY_CONTROLS = eINSTANCE.getSecurityRequirement_SecurityControls();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.LocationRequirementImpl <em>Location Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.LocationRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getLocationRequirement()
		 * @generated
		 */
		EClass LOCATION_REQUIREMENT = eINSTANCE.getLocationRequirement();

		/**
		 * The meta object literal for the '<em><b>Locations</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference LOCATION_REQUIREMENT__LOCATIONS = eINSTANCE.getLocationRequirement_Locations();

		/**
		 * The meta object literal for the '<em><b>All Required</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute LOCATION_REQUIREMENT__ALL_REQUIRED = eINSTANCE.getLocationRequirement_AllRequired();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.ImageRequirementImpl <em>Image Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.ImageRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getImageRequirement()
		 * @generated
		 */
		EClass IMAGE_REQUIREMENT = eINSTANCE.getImageRequirement();

		/**
		 * The meta object literal for the '<em><b>Images</b></em>' attribute list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute IMAGE_REQUIREMENT__IMAGES = eINSTANCE.getImageRequirement_Images();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.ScaleRequirementImpl <em>Scale Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.ScaleRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getScaleRequirement()
		 * @generated
		 */
		EClass SCALE_REQUIREMENT = eINSTANCE.getScaleRequirement();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.HorizontalScaleRequirementImpl <em>Horizontal Scale Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.HorizontalScaleRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getHorizontalScaleRequirement()
		 * @generated
		 */
		EClass HORIZONTAL_SCALE_REQUIREMENT = eINSTANCE.getHorizontalScaleRequirement();

		/**
		 * The meta object literal for the '<em><b>Min Instances</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HORIZONTAL_SCALE_REQUIREMENT__MIN_INSTANCES = eINSTANCE.getHorizontalScaleRequirement_MinInstances();

		/**
		 * The meta object literal for the '<em><b>Max Instances</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute HORIZONTAL_SCALE_REQUIREMENT__MAX_INSTANCES = eINSTANCE.getHorizontalScaleRequirement_MaxInstances();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.VerticalScaleRequirementImpl <em>Vertical Scale Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.VerticalScaleRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getVerticalScaleRequirement()
		 * @generated
		 */
		EClass VERTICAL_SCALE_REQUIREMENT = eINSTANCE.getVerticalScaleRequirement();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.OptimisationRequirementImpl <em>Optimisation Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.OptimisationRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getOptimisationRequirement()
		 * @generated
		 */
		EClass OPTIMISATION_REQUIREMENT = eINSTANCE.getOptimisationRequirement();

		/**
		 * The meta object literal for the '<em><b>Metric Context</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPTIMISATION_REQUIREMENT__METRIC_CONTEXT = eINSTANCE.getOptimisationRequirement_MetricContext();

		/**
		 * The meta object literal for the '<em><b>Metric Variable</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference OPTIMISATION_REQUIREMENT__METRIC_VARIABLE = eINSTANCE.getOptimisationRequirement_MetricVariable();

		/**
		 * The meta object literal for the '<em><b>Minimise</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute OPTIMISATION_REQUIREMENT__MINIMISE = eINSTANCE.getOptimisationRequirement_Minimise();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.SoftRequirementImpl <em>Soft Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.SoftRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getSoftRequirement()
		 * @generated
		 */
		EClass SOFT_REQUIREMENT = eINSTANCE.getSoftRequirement();

		/**
		 * The meta object literal for the '<em><b>Priority</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute SOFT_REQUIREMENT__PRIORITY = eINSTANCE.getSoftRequirement_Priority();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.ResourceRequirementImpl <em>Resource Requirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.ResourceRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getResourceRequirement()
		 * @generated
		 */
		EClass RESOURCE_REQUIREMENT = eINSTANCE.getResourceRequirement();

		/**
		 * The meta object literal for the '{@link camel.requirement.impl.PaaSRequirementImpl <em>Paa SRequirement</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.impl.PaaSRequirementImpl
		 * @see camel.requirement.impl.RequirementPackageImpl#getPaaSRequirement()
		 * @generated
		 */
		EClass PAA_SREQUIREMENT = eINSTANCE.getPaaSRequirement();

		/**
		 * The meta object literal for the '{@link camel.requirement.CloudType <em>Cloud Type</em>}' enum.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see camel.requirement.CloudType
		 * @see camel.requirement.impl.RequirementPackageImpl#getCloudType()
		 * @generated
		 */
		EEnum CLOUD_TYPE = eINSTANCE.getCloudType();

	}

} //RequirementPackage
