/**
 */
package camel.requirement;

import camel.constraint.Constraint;
import camel.scalability.Event;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Service Level Objective</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.ServiceLevelObjective#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link camel.requirement.ServiceLevelObjective#getViolationEvent <em>Violation Event</em>}</li>
 * </ul>
 *
 * @see camel.requirement.RequirementPackage#getServiceLevelObjective()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='slo_invalid_input'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot slo_invalid_input='Tuple {\n\tmessage : String = \'SLO: \' + self.name + \' should have either a constraint or a violation event referenced\',\n\tstatus : Boolean = \n\t\t\t\tasError((constraint &lt;&gt; null xor violationEvent &lt;&gt; null))\n}.status'"
 * @generated
 */
public interface ServiceLevelObjective extends HardRequirement {
	/**
	 * Returns the value of the '<em><b>Constraint</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Constraint</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint</em>' reference.
	 * @see #setConstraint(Constraint)
	 * @see camel.requirement.RequirementPackage#getServiceLevelObjective_Constraint()
	 * @model
	 * @generated
	 */
	Constraint getConstraint();

	/**
	 * Sets the value of the '{@link camel.requirement.ServiceLevelObjective#getConstraint <em>Constraint</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraint</em>' reference.
	 * @see #getConstraint()
	 * @generated
	 */
	void setConstraint(Constraint value);

	/**
	 * Returns the value of the '<em><b>Violation Event</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Violation Event</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Violation Event</em>' reference.
	 * @see #setViolationEvent(Event)
	 * @see camel.requirement.RequirementPackage#getServiceLevelObjective_ViolationEvent()
	 * @model
	 * @generated
	 */
	Event getViolationEvent();

	/**
	 * Sets the value of the '{@link camel.requirement.ServiceLevelObjective#getViolationEvent <em>Violation Event</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Violation Event</em>' reference.
	 * @see #getViolationEvent()
	 * @generated
	 */
	void setViolationEvent(Event value);

} // ServiceLevelObjective
