/**
 */
package camel.requirement.impl;

import camel.requirement.PaaSRequirement;
import camel.requirement.RequirementPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Paa SRequirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class PaaSRequirementImpl extends HardRequirementImpl implements PaaSRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected PaaSRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.PAA_SREQUIREMENT;
	}

} //PaaSRequirementImpl
