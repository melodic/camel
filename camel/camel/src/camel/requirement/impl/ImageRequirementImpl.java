/**
 */
package camel.requirement.impl;

import camel.requirement.ImageRequirement;
import camel.requirement.RequirementPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Image Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.impl.ImageRequirementImpl#getImages <em>Images</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ImageRequirementImpl extends HardRequirementImpl implements ImageRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImageRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.IMAGE_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<String> getImages() {
		return (EList<String>)eGet(RequirementPackage.Literals.IMAGE_REQUIREMENT__IMAGES, true);
	}

} //ImageRequirementImpl
