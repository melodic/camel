/**
 */
package camel.requirement.impl;

import camel.location.Location;

import camel.requirement.LocationRequirement;
import camel.requirement.RequirementPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Location Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.impl.LocationRequirementImpl#getLocations <em>Locations</em>}</li>
 *   <li>{@link camel.requirement.impl.LocationRequirementImpl#isAllRequired <em>All Required</em>}</li>
 * </ul>
 *
 * @generated
 */
public class LocationRequirementImpl extends HardRequirementImpl implements LocationRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected LocationRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.LOCATION_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<Location> getLocations() {
		return (EList<Location>)eGet(RequirementPackage.Literals.LOCATION_REQUIREMENT__LOCATIONS, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isAllRequired() {
		return (Boolean)eGet(RequirementPackage.Literals.LOCATION_REQUIREMENT__ALL_REQUIRED, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setAllRequired(boolean newAllRequired) {
		eSet(RequirementPackage.Literals.LOCATION_REQUIREMENT__ALL_REQUIRED, newAllRequired);
	}

} //LocationRequirementImpl
