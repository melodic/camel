/**
 */
package camel.requirement.impl;

import camel.constraint.Constraint;

import camel.requirement.RequirementPackage;
import camel.requirement.ServiceLevelObjective;

import camel.scalability.Event;
import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Service Level Objective</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.impl.ServiceLevelObjectiveImpl#getConstraint <em>Constraint</em>}</li>
 *   <li>{@link camel.requirement.impl.ServiceLevelObjectiveImpl#getViolationEvent <em>Violation Event</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ServiceLevelObjectiveImpl extends HardRequirementImpl implements ServiceLevelObjective {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ServiceLevelObjectiveImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.SERVICE_LEVEL_OBJECTIVE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Constraint getConstraint() {
		return (Constraint)eGet(RequirementPackage.Literals.SERVICE_LEVEL_OBJECTIVE__CONSTRAINT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setConstraint(Constraint newConstraint) {
		eSet(RequirementPackage.Literals.SERVICE_LEVEL_OBJECTIVE__CONSTRAINT, newConstraint);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Event getViolationEvent() {
		return (Event)eGet(RequirementPackage.Literals.SERVICE_LEVEL_OBJECTIVE__VIOLATION_EVENT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setViolationEvent(Event newViolationEvent) {
		eSet(RequirementPackage.Literals.SERVICE_LEVEL_OBJECTIVE__VIOLATION_EVENT, newViolationEvent);
	}

} //ServiceLevelObjectiveImpl
