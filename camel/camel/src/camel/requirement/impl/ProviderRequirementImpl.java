/**
 */
package camel.requirement.impl;

import camel.requirement.CloudType;
import camel.requirement.ProviderRequirement;
import camel.requirement.RequirementPackage;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Provider Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.impl.ProviderRequirementImpl#getProviderNames <em>Provider Names</em>}</li>
 *   <li>{@link camel.requirement.impl.ProviderRequirementImpl#getCloudType <em>Cloud Type</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ProviderRequirementImpl extends HardRequirementImpl implements ProviderRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ProviderRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.PROVIDER_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<String> getProviderNames() {
		return (EList<String>)eGet(RequirementPackage.Literals.PROVIDER_REQUIREMENT__PROVIDER_NAMES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CloudType getCloudType() {
		return (CloudType)eGet(RequirementPackage.Literals.PROVIDER_REQUIREMENT__CLOUD_TYPE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCloudType(CloudType newCloudType) {
		eSet(RequirementPackage.Literals.PROVIDER_REQUIREMENT__CLOUD_TYPE, newCloudType);
	}

} //ProviderRequirementImpl
