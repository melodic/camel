/**
 */
package camel.requirement.impl;

import camel.metric.MetricContext;
import camel.metric.MetricVariable;

import camel.requirement.OptimisationRequirement;
import camel.requirement.RequirementPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Optimisation Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.impl.OptimisationRequirementImpl#getMetricContext <em>Metric Context</em>}</li>
 *   <li>{@link camel.requirement.impl.OptimisationRequirementImpl#getMetricVariable <em>Metric Variable</em>}</li>
 *   <li>{@link camel.requirement.impl.OptimisationRequirementImpl#isMinimise <em>Minimise</em>}</li>
 * </ul>
 *
 * @generated
 */
public class OptimisationRequirementImpl extends SoftRequirementImpl implements OptimisationRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected OptimisationRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.OPTIMISATION_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricContext getMetricContext() {
		return (MetricContext)eGet(RequirementPackage.Literals.OPTIMISATION_REQUIREMENT__METRIC_CONTEXT, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetricContext(MetricContext newMetricContext) {
		eSet(RequirementPackage.Literals.OPTIMISATION_REQUIREMENT__METRIC_CONTEXT, newMetricContext);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public MetricVariable getMetricVariable() {
		return (MetricVariable)eGet(RequirementPackage.Literals.OPTIMISATION_REQUIREMENT__METRIC_VARIABLE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMetricVariable(MetricVariable newMetricVariable) {
		eSet(RequirementPackage.Literals.OPTIMISATION_REQUIREMENT__METRIC_VARIABLE, newMetricVariable);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public boolean isMinimise() {
		return (Boolean)eGet(RequirementPackage.Literals.OPTIMISATION_REQUIREMENT__MINIMISE, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinimise(boolean newMinimise) {
		eSet(RequirementPackage.Literals.OPTIMISATION_REQUIREMENT__MINIMISE, newMinimise);
	}

} //OptimisationRequirementImpl
