/**
 */
package camel.requirement.impl;

import camel.requirement.RequirementPackage;
import camel.requirement.ResourceRequirement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Resource Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class ResourceRequirementImpl extends HardRequirementImpl implements ResourceRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ResourceRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.RESOURCE_REQUIREMENT;
	}

} //ResourceRequirementImpl
