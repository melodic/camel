/**
 */
package camel.requirement.impl;

import camel.requirement.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class RequirementFactoryImpl extends EFactoryImpl implements RequirementFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static RequirementFactory init() {
		try {
			RequirementFactory theRequirementFactory = (RequirementFactory)EPackage.Registry.INSTANCE.getEFactory(RequirementPackage.eNS_URI);
			if (theRequirementFactory != null) {
				return theRequirementFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new RequirementFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case RequirementPackage.REQUIREMENT_MODEL: return (EObject)createRequirementModel();
			case RequirementPackage.SERVICE_LEVEL_OBJECTIVE: return (EObject)createServiceLevelObjective();
			case RequirementPackage.PROVIDER_REQUIREMENT: return (EObject)createProviderRequirement();
			case RequirementPackage.OS_REQUIREMENT: return (EObject)createOSRequirement();
			case RequirementPackage.SECURITY_REQUIREMENT: return (EObject)createSecurityRequirement();
			case RequirementPackage.LOCATION_REQUIREMENT: return (EObject)createLocationRequirement();
			case RequirementPackage.IMAGE_REQUIREMENT: return (EObject)createImageRequirement();
			case RequirementPackage.HORIZONTAL_SCALE_REQUIREMENT: return (EObject)createHorizontalScaleRequirement();
			case RequirementPackage.VERTICAL_SCALE_REQUIREMENT: return (EObject)createVerticalScaleRequirement();
			case RequirementPackage.OPTIMISATION_REQUIREMENT: return (EObject)createOptimisationRequirement();
			case RequirementPackage.RESOURCE_REQUIREMENT: return (EObject)createResourceRequirement();
			case RequirementPackage.PAA_SREQUIREMENT: return (EObject)createPaaSRequirement();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
			case RequirementPackage.CLOUD_TYPE:
				return createCloudTypeFromString(eDataType, initialValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
			case RequirementPackage.CLOUD_TYPE:
				return convertCloudTypeToString(eDataType, instanceValue);
			default:
				throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementModel createRequirementModel() {
		RequirementModelImpl requirementModel = new RequirementModelImpl();
		return requirementModel;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ServiceLevelObjective createServiceLevelObjective() {
		ServiceLevelObjectiveImpl serviceLevelObjective = new ServiceLevelObjectiveImpl();
		return serviceLevelObjective;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ProviderRequirement createProviderRequirement() {
		ProviderRequirementImpl providerRequirement = new ProviderRequirementImpl();
		return providerRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OSRequirement createOSRequirement() {
		OSRequirementImpl osRequirement = new OSRequirementImpl();
		return osRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SecurityRequirement createSecurityRequirement() {
		SecurityRequirementImpl securityRequirement = new SecurityRequirementImpl();
		return securityRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public LocationRequirement createLocationRequirement() {
		LocationRequirementImpl locationRequirement = new LocationRequirementImpl();
		return locationRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ImageRequirement createImageRequirement() {
		ImageRequirementImpl imageRequirement = new ImageRequirementImpl();
		return imageRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public HorizontalScaleRequirement createHorizontalScaleRequirement() {
		HorizontalScaleRequirementImpl horizontalScaleRequirement = new HorizontalScaleRequirementImpl();
		return horizontalScaleRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public VerticalScaleRequirement createVerticalScaleRequirement() {
		VerticalScaleRequirementImpl verticalScaleRequirement = new VerticalScaleRequirementImpl();
		return verticalScaleRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public OptimisationRequirement createOptimisationRequirement() {
		OptimisationRequirementImpl optimisationRequirement = new OptimisationRequirementImpl();
		return optimisationRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ResourceRequirement createResourceRequirement() {
		ResourceRequirementImpl resourceRequirement = new ResourceRequirementImpl();
		return resourceRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PaaSRequirement createPaaSRequirement() {
		PaaSRequirementImpl paaSRequirement = new PaaSRequirementImpl();
		return paaSRequirement;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CloudType createCloudTypeFromString(EDataType eDataType, String initialValue) {
		CloudType result = CloudType.get(initialValue);
		if (result == null) throw new IllegalArgumentException("The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCloudTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public RequirementPackage getRequirementPackage() {
		return (RequirementPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static RequirementPackage getPackage() {
		return RequirementPackage.eINSTANCE;
	}

} //RequirementFactoryImpl
