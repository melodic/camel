/**
 */
package camel.requirement.impl;

import camel.requirement.RequirementPackage;
import camel.requirement.SecurityRequirement;

import camel.security.SecurityControl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Security Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.impl.SecurityRequirementImpl#getSecurityControls <em>Security Controls</em>}</li>
 * </ul>
 *
 * @generated
 */
public class SecurityRequirementImpl extends HardRequirementImpl implements SecurityRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected SecurityRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.SECURITY_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	public EList<SecurityControl> getSecurityControls() {
		return (EList<SecurityControl>)eGet(RequirementPackage.Literals.SECURITY_REQUIREMENT__SECURITY_CONTROLS, true);
	}

} //SecurityRequirementImpl
