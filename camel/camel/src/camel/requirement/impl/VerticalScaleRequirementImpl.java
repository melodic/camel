/**
 */
package camel.requirement.impl;

import camel.requirement.RequirementPackage;
import camel.requirement.VerticalScaleRequirement;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Vertical Scale Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * @generated
 */
public class VerticalScaleRequirementImpl extends ScaleRequirementImpl implements VerticalScaleRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected VerticalScaleRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.VERTICAL_SCALE_REQUIREMENT;
	}

} //VerticalScaleRequirementImpl
