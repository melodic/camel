/**
 */
package camel.requirement.impl;

import camel.requirement.HorizontalScaleRequirement;
import camel.requirement.RequirementPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Horizontal Scale Requirement</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.impl.HorizontalScaleRequirementImpl#getMinInstances <em>Min Instances</em>}</li>
 *   <li>{@link camel.requirement.impl.HorizontalScaleRequirementImpl#getMaxInstances <em>Max Instances</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HorizontalScaleRequirementImpl extends ScaleRequirementImpl implements HorizontalScaleRequirement {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HorizontalScaleRequirementImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return RequirementPackage.Literals.HORIZONTAL_SCALE_REQUIREMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMinInstances() {
		return (Integer)eGet(RequirementPackage.Literals.HORIZONTAL_SCALE_REQUIREMENT__MIN_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMinInstances(int newMinInstances) {
		eSet(RequirementPackage.Literals.HORIZONTAL_SCALE_REQUIREMENT__MIN_INSTANCES, newMinInstances);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public int getMaxInstances() {
		return (Integer)eGet(RequirementPackage.Literals.HORIZONTAL_SCALE_REQUIREMENT__MAX_INSTANCES, true);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setMaxInstances(int newMaxInstances) {
		eSet(RequirementPackage.Literals.HORIZONTAL_SCALE_REQUIREMENT__MAX_INSTANCES, newMaxInstances);
	}

} //HorizontalScaleRequirementImpl
