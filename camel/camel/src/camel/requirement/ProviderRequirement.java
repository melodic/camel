/**
 */
package camel.requirement;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Provider Requirement</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link camel.requirement.ProviderRequirement#getProviderNames <em>Provider Names</em>}</li>
 *   <li>{@link camel.requirement.ProviderRequirement#getCloudType <em>Cloud Type</em>}</li>
 * </ul>
 *
 * @see camel.requirement.RequirementPackage#getProviderRequirement()
 * @model annotation="http://www.eclipse.org/emf/2002/Ecore constraints='provider_requirement_invalid_input'"
 *        annotation="http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot provider_requirement_invalid_input='Tuple {\n\tmessage : String = \'Provider Requirement: \' + self.name + \' should specify at least either specific providers or provider names when its cloud type is null\',\n\tstatus : Boolean = \n\t\t\t\tasError((cloudType = null or cloudType = CloudType::ANY) implies (providerNames-&gt;size() &gt; 0))\n}.status'"
 * @generated
 */
public interface ProviderRequirement extends HardRequirement {
	/**
	 * Returns the value of the '<em><b>Provider Names</b></em>' attribute list.
	 * The list contents are of type {@link java.lang.String}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Provider Names</em>' attribute list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Provider Names</em>' attribute list.
	 * @see camel.requirement.RequirementPackage#getProviderRequirement_ProviderNames()
	 * @model
	 * @generated
	 */
	EList<String> getProviderNames();

	/**
	 * Returns the value of the '<em><b>Cloud Type</b></em>' attribute.
	 * The literals are from the enumeration {@link camel.requirement.CloudType}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Cloud Type</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Cloud Type</em>' attribute.
	 * @see camel.requirement.CloudType
	 * @see #setCloudType(CloudType)
	 * @see camel.requirement.RequirementPackage#getProviderRequirement_CloudType()
	 * @model
	 * @generated
	 */
	CloudType getCloudType();

	/**
	 * Sets the value of the '{@link camel.requirement.ProviderRequirement#getCloudType <em>Cloud Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Cloud Type</em>' attribute.
	 * @see camel.requirement.CloudType
	 * @see #getCloudType()
	 * @generated
	 */
	void setCloudType(CloudType value);

} // ProviderRequirement
