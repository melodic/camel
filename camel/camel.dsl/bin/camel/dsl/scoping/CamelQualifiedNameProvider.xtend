package camel.dsl.scoping

import camel.deployment.CommunicationPortInstance
import camel.deployment.ComponentInstance
import camel.deployment.HostingPortInstance
import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider
import org.eclipse.xtext.naming.QualifiedName
import camel.requirement.Requirement
import camel.requirement.RequirementModel
import camel.deployment.CommunicationPort
import camel.deployment.Component
import camel.deployment.HostingPort
import camel.organisation.User
import camel.organisation.OrganisationModel
import camel.metric.Sensor
import camel.metric.MetricModel
import camel.execution.ExecutionModel
import camel.execution.Measurement
import camel.security.SecurityControl
import camel.security.SecurityModel
import camel.location.LocationModel
import camel.location.CloudLocation
import camel.location.GeographicalRegion
import camel.security.SecurityDomain
import camel.core.Feature
import camel.core.Attribute
import camel.core.NamedElement
import camel.core.CamelModel
import camel.mms.MmsConcept
import camel.mms.MetaDataModel
import camel.mms.MmsProperty
import java.util.ArrayList

class CamelQualifiedNameProvider extends DefaultDeclarativeQualifiedNameProvider {
	def qualifiedName(CommunicationPort port){
		val comp = port.eContainer as Component
		return QualifiedName.create(comp.name, port.name)
	}
	
	def qualifiedName(HostingPort port){
		val comp = port.eContainer as Component
		return QualifiedName.create(comp.name, port.name)
	}
	
	def qualifiedName(CommunicationPortInstance port){
		val ci = port.eContainer as ComponentInstance
		return QualifiedName.create(ci.name, port.name)
	}
	
	def qualifiedName(HostingPortInstance port){
		val ci = port.eContainer as ComponentInstance
		return QualifiedName.create(ci.name, port.name)
	}
	
	def qualifiedName(Requirement req){
		val rm = req.eContainer as RequirementModel
		return QualifiedName.create(rm.name, req.name)
	}
	
	/*def qualifiedName(User u){
		val org = u.eContainer as OrganisationModel
		return QualifiedName.create(org.name,u.name)
	}*/
	
	def qualifiedName(Sensor s){
		val mm = s.eContainer as MetricModel
		return QualifiedName.create(mm.name,s.name)
	}
	
	def qualifiedName(Measurement m){
		val mm = m.eContainer as ExecutionModel
		return QualifiedName.create(mm.name,m.name)
	}
	
	def qualifiedName(SecurityControl sc){
		val sm = sc.eContainer as SecurityModel
		return QualifiedName.create(sm.name,sc.name)
	}
	
	def qualifiedName(SecurityDomain sd){
		val sm = sd.eContainer as SecurityModel
		return QualifiedName.create(sm.name,sd.id)
	}
	
	def qualifiedName(CloudLocation cl){
		val lm = cl.eContainer
		if (lm instanceof CloudLocation){
			return QualifiedName.create(lm.id,cl.id);	
		}
		else{
			val lm2 = lm as LocationModel
			return QualifiedName.create(lm2.name,cl.id);	
		}
	}
	
	def qualifiedName(GeographicalRegion l){
		val lm = l.eContainer as LocationModel
		return QualifiedName.create(lm.name,l.id)
	}
	
	def qualifiedName(MmsConcept c){
		var lm = c.eContainer
		if (lm instanceof MetaDataModel) 
			return QualifiedName.create(lm.name,c.id)
		else 
			{
				var strs = new ArrayList<String>();
				strs.add((lm as MmsConcept).id)
				strs.add(c.id);
				while (lm !== null){
					lm = lm.eContainer
					if (lm instanceof MetaDataModel){
						strs.add(0,(lm as MetaDataModel).name)
						lm = null
					}
					else if (lm instanceof MmsConcept){
						strs.add(0,(lm as MmsConcept).id)
					}
					else{
						lm = null
					}
				}
				return QualifiedName.create(strs)
			}
	}
	
	def qualifiedName(MmsProperty p){
		var lm = p.eContainer
		if (lm instanceof MetaDataModel) 
			return QualifiedName.create(lm.name,p.id)
		else{ 
			//return QualifiedName.create((lm as MmsConcept).id,p.id)
			var strs = new ArrayList<String>();
			strs.add((lm as MmsConcept).id)
			strs.add(p.id);
			while (lm !== null){
				lm = lm.eContainer
				if (lm instanceof MetaDataModel){
					strs.add(0,(lm as MetaDataModel).name)
					lm = null
				}
				else if (lm instanceof MmsConcept){
					strs.add(0,(lm as MmsConcept).id)
				}
				else{
					lm = null
				}
			}
			return QualifiedName.create(strs)		
		}
	}
}