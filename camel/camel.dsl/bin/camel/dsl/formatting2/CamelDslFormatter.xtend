/*
 * generated by Xtext 2.12.0
 */
package camel.dsl.formatting2

import camel.core.Attribute
import camel.core.Feature
import camel.data.AbstractDataModel
import camel.data.Data
import camel.data.DataSource
import camel.dsl.services.CamelDslGrammarAccess
import com.google.inject.Inject
import org.eclipse.xtext.formatting.impl.AbstractDeclarativeFormatter
import org.eclipse.xtext.formatting.impl.FormattingConfig
import org.eclipse.xtext.formatting2.IFormattableDocument
import org.eclipse.xtext.formatting2.AbstractFormatter2

class CamelDslFormatter extends AbstractFormatter2 {
	
	@Inject extension CamelDslGrammarAccess
	
	/*override protected configureFormatting(FormattingConfig c) {
		for(pair: findKeywordPairs('{', '}')) {
			c.setIndentation(pair.first, pair.second)
			c.setLinewrap(1).after(pair.first)
			c.setLinewrap(1).before(pair.second)
			c.setLinewrap(1).after(pair.second)
		}
		for(comma: findKeywords(',')) {
			c.setNoLinewrap().before(comma)
			c.setNoSpace().before(comma)
			c.setLinewrap().after(comma)
		}
		c.setLinewrap(0, 1, 2).before(SL_COMMENTRule)
		c.setLinewrap(0, 1, 2).before(ML_COMMENTRule)
		c.setLinewrap(0, 1, 1).after(ML_COMMENTRule)
	}*/


	def dispatch void format(AbstractDataModel abstractDataModel, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		for (Attribute attribute : abstractDataModel.getAttributes()) {
			attribute.format;
		}
		for (Feature feature : abstractDataModel.getSubFeatures()) {
			feature.format;
		}
		for (Data data : abstractDataModel.getData()) {
			data.format;
		}
		for (DataSource dataSource : abstractDataModel.getDataSources()) {
			dataSource.format;
		}
	}

	def dispatch void format(Attribute attribute, extension IFormattableDocument document) {
		// TODO: format HiddenRegions around keywords, attributes, cross references, etc. 
		attribute.getValue.format;
	}
	
	// TODO: implement for Feature, Data, DataSource, MmsConcept, MmsConceptInstance, QualityAttribute, MeasurableAttribute, List, Range, RangeUnion, Limit, Dimensionless, DimensionedUnit, SingleUnit, CompositeUnit, UnitDimension, Sensor, ProvidedCommunication, ProvidedHost, RequiredCommunication, RequiredHost, RequirementSet, ScriptConfiguration, ClusterConfiguration, ResourceRequirement, LocationRequirement, ProviderRequirement, VerticalScaleRequirement, HorizontalScaleRequirement, SecurityRequirement, CloudLocation, GeographicalRegion, CloudProvider, SecurityCapability, SecurityControl, DataCentre, SecurityDomain, SecurityAttribute, RawSecurityMetric, CompositeSecurityMetric, Certifiable, MetricTemplate, CompositeMetric, RawMetric, MetricVariable, SoftwareComponent, VM, Container, OSRequirement, ImageRequirement, CamelModel, Action, Application, ExecutionModel, LocationModel, MetricModel, OrganisationModel, RequirementModel, ScalabilityModel, SecurityModel, TypeModel, UnitModel, ConstraintModel, MetaDataModel, DeploymentInstanceModel, DeploymentTypeModel, SoftwareComponentInstance, VMInstance, CommunicationInstance, HostingInstance, ContainerInstance, ProvidedCommunicationInstance, ProvidedHostInstance, RequiredCommunicationInstance, RequiredHostInstance, DataInstance, DataSourceInstance, Communication, Hosting, ComponentCoupling, SLOViolation, RuleTrigger, HistoryRecord, EventInstance, ApplicationMeasurement, SoftwareComponentMeasurement, CommunicationMeasurement, VMMeasurement, DataMeasurement, Window, Schedule, ObjectContext, CompositeMetricContext, RawMetricContext, SingleEvent, FunctionalEvent, NonFunctionalEvent, ServiceLevelObjective, IfThenConstraint, AttributeContext, ScalabilityRule, Event, BinaryEventPattern, UnaryEventPattern, Timer, Cause, HistoryInfo, ConcreteDataModel, Organisation, ExternalIdentifier, User, UserGroup, Role, RoleAssignment, Permission, ModelResourceFilter, ServiceResourceFilter, DataResourceFilter, SoftwareComponentResourceFilter, OptimisationRequirement, HorizontalScalingAction, VerticalScalingAction, SecurityMetricInstance, SecuritySLO
}
