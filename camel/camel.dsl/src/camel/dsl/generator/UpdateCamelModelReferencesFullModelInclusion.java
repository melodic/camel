package camel.dsl.generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature.Setting;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.util.EcoreUtil;

import camel.core.CamelModel;
import camel.core.Model;
import camel.core.NamedElement;
import camel.data.DataModel;
import camel.deployment.DeploymentModel;
import camel.metric.MetricModel;
import camel.metric.MetricTypeModel;
import camel.mms.MetaDataModel;
import camel.mms.MmsConcept;
import camel.mms.MmsConceptInstance;
import camel.mms.MmsObject;
import camel.mms.MmsProperty;
import camel.mms.MmsPropertyInstance;
import camel.organisation.OrganisationModel;
import camel.requirement.RequirementModel;
import camel.scalability.ScalabilityModel;
import camel.security.SecurityModel;
import camel.type.TypeModel;
import camel.unit.UnitModel;

public class UpdateCamelModelReferencesFullModelInclusion {
	
	static class ProcessedModel{
		public String name = null;
		public Model prevContent = null, newContent = null;
		public Map<String,EObject> map = null;
		public boolean handled = false;
		
		public ProcessedModel(String name) {
			this.name = name;
		}
		
		public ProcessedModel() {
			
		}
		
		public boolean equals(Object o) {
			if (o instanceof ProcessedModel) {
				ProcessedModel pm = (ProcessedModel)o;
				if (pm.name.equals(name)) return true;
			}
			
			return false;
		}
		
		public int hashCode() {
			return name.length();
		}
	}
	
	private static Hashtable<Resource,List<ProcessedModel>> resourceToModel = null;
	private static Resource currentResource = null;
	private static Set<ProcessedModel> unhandledModels = null;
	
	private static Model getModel(EObject obj){
		Model model = null;
		while (obj.eContainer() != null && !(obj.eContainer() instanceof CamelModel)){
			obj = obj.eContainer();
		}
		model = (Model)obj.eContainer();
		
		return model;
	}
	
	private static Map<String,EObject> populateMap(Model m){
		Map<String,EObject> map = new HashMap<String,EObject>();
		TreeIterator<Object> it = org.eclipse.emf.ecore.util.EcoreUtil.getAllContents(m,false);
		while (it.hasNext()) {
			Object o = it.next();
			if (o instanceof NamedElement) {
				NamedElement ne = (NamedElement)o;
				map.put(ne.getName(), ne);
			}
			else if (o instanceof MmsObject) {
				MmsObject mo = (MmsObject)o;
				map.put(mo.getName(), mo);
			}
		}
		
		return map;
	}
	
	private static ProcessedModel getProcessedModel(Resource res, Model model) {
		List<ProcessedModel> models = resourceToModel.get(res);
		if (models != null) {
			ProcessedModel toCheck = new ProcessedModel(model.getName());
			int index = models.indexOf(toCheck);
			ProcessedModel pm = null;
			if (index == -1) {
				System.out.println("2. MODEL: " + model.getName() + " not existed");				
				toCheck.prevContent = model;
				toCheck.newContent = EcoreUtil.copy(model);
				toCheck.map = populateMap(toCheck.newContent);
				models.add(toCheck);
				unhandledModels.add(toCheck);
				return toCheck;
			}
			else return models.get(index);
		}
		else {
			System.out.println("1. MODEL: " + model.getName() + " not existed");
			models = new ArrayList<ProcessedModel>();
			ProcessedModel pm = new ProcessedModel(model.getName());
			pm.prevContent = model;
			pm.newContent = EcoreUtil.copy(model);
			pm.map = populateMap(pm.newContent);
			models.add(pm);
			resourceToModel.put(res, models);
			unhandledModels.add(pm);
			return pm;
		}
	}
	
	private static void printObject(EObject o) {
		if (o instanceof NamedElement) {
			System.out.println("Got object: " + o + " with name: " + ((NamedElement)o).getName());
		}
		else if (o instanceof MmsObject) {
			System.out.println("Got object: " + o + " with name: " + ((MmsObject)o).getName());
		}
	}
	
	private static void handleExternalReferencesInModel(Model m){
		System.out.println("Checking external references for model: " + m.getName());
		java.util.Map<EObject,java.util.Collection<Setting>> map = EcoreUtil.ExternalCrossReferencer.find(m);
		if (map != null) {
			if (!map.isEmpty()) System.out.println("Model: " + m.getName() + " has external references");
			for (EObject refObj: map.keySet()) {
				printObject(refObj);
				Model model = getModel(refObj);
				Resource res = refObj.eResource();
				System.out.println("Resource is: " + res + " " + res.getURI());
				if (res != currentResource) {
					//Get New Model
					ProcessedModel pm = getProcessedModel(res,model);
					
					//Update external references to include new Model
					EObject replObj = null;
					if (refObj instanceof NamedElement) replObj = pm.map.get(((NamedElement)refObj).getName());
					else if (refObj instanceof MmsObject) replObj = pm.map.get(((MmsObject)refObj).getName());
					System.out.println("Replacing obj: " + refObj + " with obj: " + replObj);
					for (Setting st: map.get(refObj)) {
						Object target = st.get(true);
						  if (target instanceof EObject){
							  st.set(replObj);
						  }
						  else{
							  List l = (List)target;
							  int pos = l.indexOf(refObj);
							  //System.out.println("Got pos: " + pos);
							  if (pos != -1){
								  l.set(pos, replObj);
							  }
						  }
					}
				}
			}
		}
	}
	
	private static void addModel(Model m, CamelModel cm) {
		if (m instanceof CamelModel) {
			CamelModel model = (CamelModel)m;
			cm.getDeploymentModels().addAll(model.getDeploymentModels());
			cm.getRequirementModels().addAll(model.getRequirementModels());
			cm.getMetricModels().addAll(model.getMetricModels());
			cm.getScalabilityModels().addAll(model.getScalabilityModels());
			cm.getTypeModels().addAll(model.getTypeModels());
			cm.getSecurityModels().addAll(model.getSecurityModels());
			cm.getUnitModels().addAll(model.getUnitModels());
			cm.getOrganisationModels().addAll(model.getOrganisationModels());
			cm.getMetadataModels().addAll(model.getMetadataModels());
			cm.getDataModels().addAll(model.getDataModels());
			cm.getConstraintModels().addAll(model.getConstraintModels());
		}
	}
	
	public static void updateCamelModel(CamelModel cm){
		currentResource = cm.eResource();
		resourceToModel = new Hashtable<Resource,List<ProcessedModel>>();
		unhandledModels = new HashSet<ProcessedModel>();
		handleExternalReferencesInModel(cm);
		System.out.println("1. Unhandled models are now: " + unhandledModels.size() + " in size");
		while (!unhandledModels.isEmpty()) {
			List<ProcessedModel> toRem = new ArrayList<ProcessedModel>();
			for (ProcessedModel model: unhandledModels) {
				System.out.println("Model: " + model.name + " will be now handled");
				handleExternalReferencesInModel(model.newContent);
				addModel(model.newContent,cm);
				model.handled = true;
				System.out.println("Model: " + model.name + " was handled ");
				toRem.add(model);
			}
			unhandledModels.removeAll(toRem);
			System.out.println("2. Unhandled models are now: " + unhandledModels.size() + " in size");
		}
	}
}
