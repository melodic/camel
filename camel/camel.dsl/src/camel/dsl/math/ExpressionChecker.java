package camel.dsl.math;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import org.mariuszgromada.math.mxparser.Argument;
import org.mariuszgromada.math.mxparser.Expression;

import camel.metric.MetricFactory;
import camel.metric.CompositeMetric;
import camel.metric.CompositeMetricContext;
import camel.metric.Function;
import camel.metric.GroupingType;
import camel.metric.Metric;
import camel.metric.MetricContext;
import camel.metric.MetricTypeModel;
import camel.metric.MetricVariable;
import camel.metric.RawMetric;
import camel.unit.CompositeUnit;
import camel.unit.Unit;
import camel.unit.UnitModel;

public class ExpressionChecker {
	
	private static String printStringArray(String[] array){
		String s = "[" + array[0];
		for (int i = 1; i < array.length; i++){
			s += (", " + array[i]);
		}
		s += "]";
		
		return s;
	}
	
	public static String checkCompositeMetricContext(CompositeMetricContext cmc){
		String errorMsg = null;
		
		MetricTypeModel mm = (MetricTypeModel)cmc.eContainer();
		Map<String,Metric> allMetrics = new HashMap<String,Metric>();
		Set<String> compMetrics = new HashSet<String>();
		for (Metric m: mm.getMetrics()) allMetrics.put(m.getName(),m);
		CompositeMetric metric = (CompositeMetric)cmc.getMetric();
		String formula = metric.getFormula();
		if (formula != null) {
			Expression e = new Expression(formula);
			String[] missingArgs = e.getMissingUserDefinedArguments();
			for (String s: missingArgs) {
				Metric m = allMetrics.get(s);
				if (m != null) compMetrics.add(s);
 			}
		}
		
		if (!compMetrics.isEmpty()) {
			GroupingType gt = cmc.getGroupingType();
			for (MetricContext mc: cmc.getComposingMetricContexts()) {
				Metric m = mc.getMetric();
				if (!compMetrics.contains(m.getName())) {
					errorMsg = "Metric: " + metric.getName() + " has composite context: " + cmc.getName() + " which includes the composing metric context: " + mc.getName() 
					+ " that corresponds to a metric which is not a component metric of: " + metric.getName();
					break;
				}
				if (mc instanceof CompositeMetricContext){
					GroupingType gt2 = ((CompositeMetricContext)mc).getGroupingType();
					if (gt2.getValue() > gt.getValue()){
						errorMsg = "Metric: " + metric.getName() + " has composite context: " + cmc.getName() + " which includes the composing metric context: " + mc.getName() 
						+ " that has a higher grouping type than the metric's composite context";
						break;
					}
				}
			}
		}
		
		return errorMsg;
	}
	
	public static String checkFunction(Function function){
		String errorMsg = null;
		
		//org.mariuszgromada.math.mxparser.Function f = new org.mariuszgromada.math.mxparser.Function("f","x + ","x");
		Expression e = new Expression(function.getExpression());
		for (String s: function.getArguments()){
			e.addArguments(new Argument(s));
		}
		if (!e.checkLexSyntax()) errorMsg = "Lexical Syntax of Function Expression is wrong";
		else if (!e.checkSyntax()){
			String[] missingArgs = e.getMissingUserDefinedArguments(); 
			String[] missingFuncs = e.getMissingUserDefinedFunctions(); 
			errorMsg = "Syntax of Function Expression is wrong";
			if (missingArgs != null && missingArgs.length > 0){
				errorMsg += ("\nThe following arguments are missing: " + printStringArray(missingArgs));
			}
			if (missingFuncs != null && missingFuncs.length > 0){
				errorMsg += ("\nThe following functions are missing: " + printStringArray(missingFuncs));
			}
		}
		
		return errorMsg;
	}
	
	public static String checkFormula(Metric m, MetricTypeModel mm){
		String errorMsg = null;
		
		Expression e = null;
		if (m instanceof CompositeMetric)
			e = new Expression(new String(((CompositeMetric)m).getFormula().trim()));
		else
			e = new Expression(new String(((MetricVariable)m).getFormula().trim()));
		//e.addConstants(new Constant("x",0));
		//e.addConstants(new Constant("y",1));
		for (Metric metric: mm.getMetrics()){
			if (!metric.getName().equals(m.getName())) e.addArguments(new Argument(metric.getName()));
		}
		for (Function f: mm.getFunctions()){
			e.addFunctions(new org.mariuszgromada.math.mxparser.Function(f.getName(),f.getExpression(),f.getArguments().toArray(new String[0])));
		}
	
		if (!e.checkLexSyntax()) errorMsg = "Lexical Syntax of Formula Expression is wrong";
		else if (!e.checkSyntax()){
			System.out.println("The syntax was wrong");
			try{
				String[] missingArgs = e.getMissingUserDefinedArguments(); 
				String[] missingFuncs = e.getMissingUserDefinedFunctions(); 
				errorMsg = "Syntax of Formula Expression is wrong";
				//System.out.println("1. Syntax error found. About communicate the message: " + errorMsg);
				boolean found = false;
				if (missingArgs != null && missingArgs.length > 0){
					String args = printStringArray(missingArgs);
					if (args.matches("(.*)(\\W|\\s)" + m.getName() + "(\\W)(.*)")) errorMsg += "\nCannot use recursively the composite metric: " + m.getName() + " inside its formula";
					else errorMsg += ("\nThe following arguments are missing: " + args);
					found = true;
				}
				//System.out.println("2. Syntax error found. About communicate the message: " + errorMsg);
				if (missingFuncs != null && missingFuncs.length > 0){
					errorMsg += ("\nThe following functions are missing: " + printStringArray(missingFuncs));
					found = true;
				}
				//System.out.println("3. Syntax error found. About communicate the message: " + errorMsg);
				if (!found) errorMsg = null;
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return errorMsg;
	}
	
	public static String checkUnitFormula(CompositeUnit cu, UnitModel um){
		String errorMsg = null;
		
		Expression e = new Expression(new String(cu.getFormula().trim()));
		//e.addConstants(new Constant("x",0));
		//e.addConstants(new Constant("y",1));
		for (Unit u: um.getUnits()){
			if (!u.getName().equals(cu.getName())) e.addArguments(new Argument(u.getName()));
		}
		//Maybe for the future
		/*for (Function f: mm.getFunctions()){
			e.addFunctions(new org.mariuszgromada.math.mxparser.Function(f.getName(),f.getExpression(),f.getArguments().toArray(new String[0])));
		}*/
	
		if (!e.checkLexSyntax()) errorMsg = "Lexical Syntax of Formula Expression is wrong";
		else if (!e.checkSyntax()){
			System.out.println("The syntax was wrong");
			try{
				String[] missingArgs = e.getMissingUserDefinedArguments(); 
				String[] missingFuncs = e.getMissingUserDefinedFunctions(); 
				errorMsg = "Syntax of Formula Expression is wrong";
				//System.out.println("1. Syntax error found. About communicate the message: " + errorMsg);
				boolean found = false;
				if (missingArgs != null && missingArgs.length > 0){
					String args = printStringArray(missingArgs);
					if (args.matches("(.*)(\\W|\\s)" + cu.getName() + "(\\W)(.*)")) errorMsg += "\nCannot use recursively the composite unit: " + cu.getName() + " inside its formula";
					else errorMsg += ("\nThe following arguments are missing: " + args);
					found = true;
				}
				//System.out.println("2. Syntax error found. About communicate the message: " + errorMsg);
				if (missingFuncs != null && missingFuncs.length > 0){
					errorMsg += ("\nThe following functions are missing: " + printStringArray(missingFuncs));
					found = true;
				}
				//System.out.println("3. Syntax error found. About communicate the message: " + errorMsg);
				if (!found) errorMsg = null;
			}
			catch(Exception ex){
				ex.printStackTrace();
			}
		}
		
		return errorMsg;
	}
	
	public static String[] getArguments(String formula) {
		Expression e = new Expression(formula);
		return e.getMissingUserDefinedArguments();
	}
	
	public static void main(String[] args){
		MetricTypeModel mm = MetricFactory.eINSTANCE.createMetricTypeModel();
		mm.setName("test");
		RawMetric rm = MetricFactory.eINSTANCE.createRawMetric();
		rm.setName("x");
		mm.getMetrics().add(rm);
		CompositeMetric cm = MetricFactory.eINSTANCE.createCompositeMetric();
		cm.setName("CM1");
		cm.setFormula("CM1+2");
		mm.getMetrics().add(cm);
		/*rm = CmlFactory.eINSTANCE.createRawMetric();
		rm.setName("y");
		mm.getMetrics().add(rm);*/
		/*Function f = CmlFactory.eINSTANCE.createFunction();
		f.setName("g");
		f.setExpression("x^2");
		f.getArguments().add("x");
		mm.getFunctions().add(f);*/
		//System.out.println("" + checkFormula("x + g(y)",mm));
		System.out.println("" + checkFormula(cm,mm));
	}
}
