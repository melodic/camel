package camel.dsl.matching;

import org.eclipse.xtext.ui.editor.contentassist.FQNPrefixMatcher.LastSegmentFinder;

public class SimpleLastSegmentFinder implements LastSegmentFinder {
	
	public String getLastSegment(String fqn, char delimiter) {
		int idx = fqn.lastIndexOf(delimiter);
		if (idx != -1 && idx < fqn.length() - 1) {
			return fqn.substring(idx + 1);
		}
		return null;
	}
}
