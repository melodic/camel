package camel.dsl.validation;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import camel.core.CamelModel;
import camel.deployment.BuildConfiguration;
import camel.deployment.ClusterConfiguration;
import camel.deployment.Configuration;
import camel.deployment.DeploymentTypeModel;
import camel.deployment.SoftwareComponent;
import camel.deployment.RequiredHost;
import camel.deployment.RequirementSet;
import camel.deployment.ScriptConfiguration;
import camel.deployment.ServerlessConfiguration;
import camel.requirement.HorizontalScaleRequirement;
import camel.requirement.OptimisationRequirement;
import camel.requirement.Requirement;
import camel.requirement.RequirementModel;

public class ValidationChecker {
	private static boolean emptyRequirementSet(RequirementSet set) {
		if (set != null) {
			if (set.getLocationRequirement() != null) return false;
			else if (set.getOsRequirement() != null) return false;
			else if (set.getImageRequirement() != null) return false;
			else if (set.getResourceRequirement() != null) return false;
			else if (set.getProviderRequirement() != null) return false;
			else if (set.getHorizontalScaleRequirement() != null) return false;
		}
		
		return true;
	}
	
	private static List<String> checkCompReqs(CamelModel cm) {
		List<String> messages = null;
		DeploymentTypeModel dm = (DeploymentTypeModel)cm.getDeploymentModels().get(0);
		RequirementSet global = dm.getGlobalRequirementSet();
		if (global == null) {
			boolean noReq = false;
			for (SoftwareComponent sc: dm.getSoftwareComponents()) {
				if (emptyRequirementSet(sc.getRequirementSet())) {
					if (!noReq) {
						messages = new ArrayList<String>();
						noReq = true;
					}
					messages.add("Warning: No requirement has been supplied for SoftwareComponent: " + sc.getName());
				}
			}
		}
		
		return messages;
	}
	
	private static int countCommandsInConfig(Configuration configuration) {
		int count = 0;
		if (configuration instanceof ScriptConfiguration) {
			ScriptConfiguration config = (ScriptConfiguration)configuration;
			String imageId = config.getImageId();
			if (imageId == null || imageId.trim().equals("")){
				if (config.getDownloadCommand() != null && !config.getDownloadCommand().trim().equals("")) count++;
				if (config.getUploadCommand() != null && !config.getUploadCommand().trim().equals("")) count++;
				if (config.getConfigureCommand() != null && !config.getConfigureCommand().trim().equals("")) count++;
				if (config.getUpdateCommand() != null && !config.getUpdateCommand().trim().equals("")) count++;
				if (config.getStartCommand() != null && !config.getStartCommand().trim().equals("")) count++;
				if (config.getStopCommand() != null && !config.getStopCommand().trim().equals("")) count++;
				if (config.getInstallCommand() != null && !config.getInstallCommand().trim().equals("")) count++;
			}
			else count = 2;
		}
		else if (configuration instanceof ClusterConfiguration) {
			ClusterConfiguration cc = (ClusterConfiguration)configuration;
			String downloadURL = cc.getDownloadURL();
			if (downloadURL != null && !cc.getAnnotations().isEmpty()) count++;
		}
		else if (configuration instanceof ServerlessConfiguration){
			ServerlessConfiguration sc = (ServerlessConfiguration)configuration;
			String url = sc.getBinaryCodeURL();
			if (url == null || url.trim().equals("")){
				BuildConfiguration bc = sc.getBuildConfiguration();
				if (bc != null){
					url = bc.getSourceCodeURL();
					if (url != null && !url.trim().equals("")) count++;
				}
			}
			else count++;
		}
		
		return count;
	}
	
	private static List<String> checkComponents(CamelModel cm) {
		List<String> messages = null;
		DeploymentTypeModel dm = (DeploymentTypeModel)cm.getDeploymentModels().get(0);
		EList<SoftwareComponent> comps = dm.getSoftwareComponents();
		if (comps.isEmpty()) {
			messages = new ArrayList<String>();
			messages.add("Error: No software components exist in deployment model: " + dm.getName());
		}
		else {
			boolean error = false;
			boolean warning = false;
			messages = new ArrayList<String>();
			HorizontalScaleRequirement hsr = null;
			RequirementSet global = dm.getGlobalRequirementSet();
			if (global != null) hsr = global.getHorizontalScaleRequirement();
			for (SoftwareComponent comp: comps) {
				boolean hosted = false;
				RequiredHost rh = comp.getRequiredHost();
				int minInstances = 0;
				RequirementSet rs = comp.getRequirementSet();
				if (rs != null && rs.getHorizontalScaleRequirement() != null) hsr = rs.getHorizontalScaleRequirement();
				if (hsr != null) minInstances = hsr.getMinInstances();
				if (minInstances > 0 && rh == null) {
					error = true;
					messages.add("Error: SoftwareComponent: " + comp.getName() + " has no required host port");
				}
				else if (rh == null) {
					warning = true;
					messages.add("Warning: SoftwareComponent: " + comp.getName() + " has no required host port");
				}
				else hosted = true;
				
				if (comp.getConfigurations().isEmpty()) {
					//Checking if component is hosted
					if (hosted) {
						error = true;
						messages.add("Error: Software Component: " + comp.getName() + " does not have any configuration while requires to be hosted");
					}
					else {
						warning = true;
						messages.add("Warning: Software Component: " + comp.getName() + " does not have any configuration");
					}
				}
				else {
					//Just checking how many commands have been given for component, if one then we have warning
					Configuration config = comp.getConfigurations().get(0);
					int res = countCommandsInConfig(config); 
					if (res == 1 && config instanceof ScriptConfiguration) {
						warning = true;
						messages.add("Warning: Software Component: " + comp.getName() + " has just one configuration command");
					}
					else if (res == 0){
						error = true;
						messages.add("Error: Software Component: " + comp.getName() + " is not properly configured. Configuration: " + config.getName() + " is either: (a) script-based & no commands have been supplied or imageID; or (b) it is cluster-based and no annotations or download URL has been provided");
					}
				}
			}
			if (!error && !warning) messages = null;
		}
		
		return messages;
	}
	
	public static String isDeployable(CamelModel cm) {
		String message = null;
		
		if (cm.getDeploymentModels().isEmpty()) {
			message = "Error: No deployment model exists for application: " + cm.getApplication().getName();
		}
		else{
			List<String> vmMessages = checkCompReqs(cm);
			List<String> compMessages = checkComponents(cm);
			
			if (vmMessages != null || compMessages != null) {
				message = "Issues with deployability of CAMEL model have been found:\n";
				System.out.println("Messages are: " + vmMessages + " " + compMessages);
				if (vmMessages != null) {
					for (int i = 0; i < vmMessages.size(); i++) message += (vmMessages.get(i) + "\n");
				}
				if (compMessages != null) {
					for (int i = 0; i < compMessages.size(); i++) message += (compMessages.get(i) + "\n");
				}
			}
		}
		
		return message;
	}
	
	public static String containsOptimisationRequirement(RequirementModel rm) {
		String message = null;
		
		boolean ok = false;
		for (Requirement r: rm.getRequirements()) {
			if (r instanceof OptimisationRequirement) {
				ok = true;
				break;
			}
		}
		
		if (!ok) message = "Warning: RequirementModel: " + rm.getName() + " does not have an optimisation requirement. The default cost optimisation requirement will apply for application deployment reasoning";
		
		return message;
	}

}
