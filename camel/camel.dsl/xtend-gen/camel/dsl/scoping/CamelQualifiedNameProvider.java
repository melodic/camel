package camel.dsl.scoping;

import camel.deployment.CommunicationPort;
import camel.deployment.CommunicationPortInstance;
import camel.deployment.Component;
import camel.deployment.ComponentInstance;
import camel.deployment.HostingPort;
import camel.deployment.HostingPortInstance;
import camel.execution.ExecutionModel;
import camel.execution.Measurement;
import camel.location.CloudLocation;
import camel.location.GeographicalRegion;
import camel.location.LocationModel;
import camel.metric.MetricTypeModel;
import camel.metric.Sensor;
import camel.mms.MetaDataModel;
import camel.mms.MmsConcept;
import camel.mms.MmsProperty;
import camel.requirement.Requirement;
import camel.requirement.RequirementModel;
import camel.security.SecurityControl;
import camel.security.SecurityDomain;
import camel.security.SecurityModel;
import java.util.ArrayList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.naming.DefaultDeclarativeQualifiedNameProvider;
import org.eclipse.xtext.naming.QualifiedName;

@SuppressWarnings("all")
public class CamelQualifiedNameProvider extends DefaultDeclarativeQualifiedNameProvider {
  public QualifiedName qualifiedName(final CommunicationPort port) {
    EObject _eContainer = port.eContainer();
    final Component comp = ((Component) _eContainer);
    return QualifiedName.create(comp.getName(), port.getName());
  }
  
  public QualifiedName qualifiedName(final HostingPort port) {
    EObject _eContainer = port.eContainer();
    final Component comp = ((Component) _eContainer);
    return QualifiedName.create(comp.getName(), port.getName());
  }
  
  public QualifiedName qualifiedName(final CommunicationPortInstance port) {
    EObject _eContainer = port.eContainer();
    final ComponentInstance ci = ((ComponentInstance) _eContainer);
    return QualifiedName.create(ci.getName(), port.getName());
  }
  
  public QualifiedName qualifiedName(final HostingPortInstance port) {
    EObject _eContainer = port.eContainer();
    final ComponentInstance ci = ((ComponentInstance) _eContainer);
    return QualifiedName.create(ci.getName(), port.getName());
  }
  
  public QualifiedName qualifiedName(final Requirement req) {
    EObject _eContainer = req.eContainer();
    final RequirementModel rm = ((RequirementModel) _eContainer);
    return QualifiedName.create(rm.getName(), req.getName());
  }
  
  /**
   * def qualifiedName(User u){
   * val org = u.eContainer as OrganisationModel
   * return QualifiedName.create(org.name,u.name)
   * }
   */
  public QualifiedName qualifiedName(final Sensor s) {
    EObject _eContainer = s.eContainer();
    final MetricTypeModel mm = ((MetricTypeModel) _eContainer);
    return QualifiedName.create(mm.getName(), s.getName());
  }
  
  public QualifiedName qualifiedName(final Measurement m) {
    EObject _eContainer = m.eContainer();
    final ExecutionModel mm = ((ExecutionModel) _eContainer);
    return QualifiedName.create(mm.getName(), m.getName());
  }
  
  public QualifiedName qualifiedName(final SecurityControl sc) {
    EObject _eContainer = sc.eContainer();
    final SecurityModel sm = ((SecurityModel) _eContainer);
    return QualifiedName.create(sm.getName(), sc.getName());
  }
  
  public QualifiedName qualifiedName(final SecurityDomain sd) {
    EObject _eContainer = sd.eContainer();
    final SecurityModel sm = ((SecurityModel) _eContainer);
    return QualifiedName.create(sm.getName(), sd.getId());
  }
  
  public QualifiedName qualifiedName(final CloudLocation cl) {
    final EObject lm = cl.eContainer();
    if ((lm instanceof CloudLocation)) {
      return QualifiedName.create(((CloudLocation)lm).getId(), cl.getId());
    } else {
      final LocationModel lm2 = ((LocationModel) lm);
      return QualifiedName.create(lm2.getName(), cl.getId());
    }
  }
  
  public QualifiedName qualifiedName(final GeographicalRegion l) {
    EObject _eContainer = l.eContainer();
    final LocationModel lm = ((LocationModel) _eContainer);
    return QualifiedName.create(lm.getName(), l.getId());
  }
  
  public QualifiedName qualifiedName(final MmsConcept c) {
    EObject lm = c.eContainer();
    if ((lm instanceof MetaDataModel)) {
      return QualifiedName.create(((MetaDataModel)lm).getName(), c.getId());
    } else {
      ArrayList<String> strs = new ArrayList<String>();
      strs.add(((MmsConcept) lm).getId());
      strs.add(c.getId());
      while ((lm != null)) {
        {
          lm = lm.eContainer();
          if ((lm instanceof MetaDataModel)) {
            strs.add(0, ((MetaDataModel) lm).getName());
            lm = null;
          } else {
            if ((lm instanceof MmsConcept)) {
              strs.add(0, ((MmsConcept) lm).getId());
            } else {
              lm = null;
            }
          }
        }
      }
      return QualifiedName.create(strs);
    }
  }
  
  public QualifiedName qualifiedName(final MmsProperty p) {
    EObject lm = p.eContainer();
    if ((lm instanceof MetaDataModel)) {
      return QualifiedName.create(((MetaDataModel)lm).getName(), p.getId());
    } else {
      ArrayList<String> strs = new ArrayList<String>();
      strs.add(((MmsConcept) lm).getId());
      strs.add(p.getId());
      while ((lm != null)) {
        {
          lm = lm.eContainer();
          if ((lm instanceof MetaDataModel)) {
            strs.add(0, ((MetaDataModel) lm).getName());
            lm = null;
          } else {
            if ((lm instanceof MmsConcept)) {
              strs.add(0, ((MmsConcept) lm).getId());
            } else {
              lm = null;
            }
          }
        }
      }
      return QualifiedName.create(strs);
    }
  }
}
