camel model PeopleFlow{
	deployment type model PeopleFlowDepModel{
		
		software HDFSComponent{
			[MetaDataModel.MELODICMetadataSchema.Big_DataModel.DataManagement.DataStorage.FileSystems.DistributedFileSystems.ClusterDistributed.CentralizedMetadata.HDFS]
			provided communication HDFSProvidedPort port 0
			longLived
		}
		
		software Hive{
			provided communication HiveProvidedPort port 0
			required communication HDFSRequiredPort port 0
			longLived
		}
		
		software SparkOnDemand{
			[MetaDataModel.MELODICMetadataSchema.Big_DataModel.DataManagement.BigDataProcessing.HybridProcessing.ApacheSPARK]
			generates data [ PeopleFlowDataModel.LocalStorageData ]
			consumes data [PeopleFlowDataModel.TableData, PeopleFlowDataModel.ConfigurationFiles]
			required communication HiveRequiredPort port 0
		}
		
		software SparkInMemory{
			[MetaDataModel.MELODICMetadataSchema.Big_DataModel.DataManagement.BigDataProcessing.HybridProcessing.ApacheSPARK]
			consumes data [PeopleFlowDataModel.StreamData]
		}
		
		communication SparkOnDemandToHive from SparkOnDemand.HiveRequiredPort to Hive.HiveProvidedPort
		communication HiveToHDFS from Hive.HDFSRequiredPort to HDFSComponent.HDFSProvidedPort
		
		coupling HiveHDFSCoupling{
			type same-host
			software [ HDFSComponent, Hive ]
		}
	} 
	
	data type model PeopleFlowDataModel{
		data StreamData {
			source ExternalDataSource
			feature InputVelocity{
				attribute isInput [ MetaDataModel.MELODICMetadataSchema.Big_DataModel.Big_DataAspects.DataDensity.Velocity.isInputVelocity] : boolean true
				attribute rate [MetaDataModel.MELODICMetadataSchema.Big_DataModel.Big_DataAspects.DataDensity.Velocity.hasRate] : int 50 UnitTemplateCamelModel.UnitTemplateModel.GigaBytesPerDays  
			}
		}
		
		data LocalStorageData {
			[MetaDataModel.MELODICMetadataSchema.Big_DataModel.Big_DataAspects.DataVariety.Format.FileFormat]
		}
		
		data TableData{
			[MetaDataModel.MELODICMetadataSchema.Big_DataModel.Big_DataAspects.DataVariety.Format.Key_ValuePairs]
		}
		
	
		
		
		data ConfigurationFiles {
			[MetaDataModel.MELODICMetadataSchema.Big_DataModel.Big_DataAspects.DataVariety.Format.FileFormat]
			source ExternalDataSource
		}
		
		data source ExternalDataSource{
			external
		}
		
		data S3BucketData {
			source S3DataSource
			feature ufsUri {
				attribute ufsUri [ MetaDataModel.MELODICMetadataSchema.ApplicationPlacementModel.IaaS.Storage.Off_InstanceStorage.ObjectStorage.ufsUri] : string "s3a://ferozbucket"
			}
		}
		
		data source S3DataSource {
			external
		}
		
		data source FiveDS{
			component PeopleFlowDepModel.Hive
		}
	}
	
	
metric type model PeopleFlowMetrModel{
		measurable attribute BytesReadFromDS sensors [PeopleFlowMetrModel.BytesReadFromDSSensor]
		sensor BytesReadFromDSSensor {
			config ''
		}
		measurable attribute BytesWrittenToDS sensors [PeopleFlowMetrModel.BytesWrittenToDSSensor]
		sensor BytesWrittenToDSSensor {
			config ''
		}	
		
		template BytesReadFromDSTemplate{
			attribute BytesReadFromDS
			unit UnitTemplateCamelModel.UnitTemplateModel.Bytes
			value type TypeTemplateCamelModel.TypeTemplateModel.ZeroToPositiveInfinityInteger
		}
		
		template BytesWrittenToDSTemplate{
			attribute BytesWrittenToDS
			unit UnitTemplateCamelModel.UnitTemplateModel.Bytes
			value type TypeTemplateCamelModel.TypeTemplateModel.ZeroToPositiveInfinityInteger			
		}
		
		raw metric BytesReadFromDS {
			template BytesReadFromDSTemplate
		}
		
		raw metric BytesWrittenToDS {
			template BytesWrittenToDSTemplate
		}
		
		composite metric SumBytesReadFromDS{
			template BytesReadFromDSTemplate
			formula: ('sum(BytesReadFromDS)')
		}
		
		composite metric SumBytesWrittenToDS{
			template BytesWrittenToDSTemplate
			formula: ('sum(BytesWrittenToDS)')
		}
			
		window Win20Min {
			type sliding
			size type time-only
			time size 20
			time unit UnitTemplateCamelModel.UnitTemplateModel.Minutes
		}
		
		schedule ScheduleBytesReadAndWritten { 
			interval 1
			time unit UnitTemplateCamelModel.UnitTemplateModel.Minutes
		}
		
		schedule ScheduleSumBytesReadAndWritten {
			interval 10
			time unit UnitTemplateCamelModel.UnitTemplateModel.Minutes
		}
		
		object context SparkStreamContext{
			component PeopleFlowDepModel.SparkInMemory
			data PeopleFlowDataModel.StreamData
			
		}
		
		raw metric context SparkStreamContextBytesReadFromDS{
			metric BytesReadFromDS
			sensor PeopleFlowMetrModel.BytesReadFromDSSensor
			schedule PeopleFlowMetrModel.ScheduleBytesReadAndWritten
			object context SparkStreamContext
		}
		
		raw metric context SparkStreamContextBytesWrittenToDS{
			metric BytesWrittenToDS
			sensor PeopleFlowMetrModel.BytesWrittenToDSSensor
			schedule PeopleFlowMetrModel.ScheduleBytesReadAndWritten
			object context PeopleFlowMetrModel.SparkStreamContext
		}		
		
		composite metric context SparkStreamContextSumBytesReadFromDS {
			metric SumBytesReadFromDS
			grouping global
			window PeopleFlow.PeopleFlowMetrModel.Win20Min
			schedule ScheduleBytesReadAndWritten
			object context SparkStreamContext
			composing contexts [PeopleFlowMetrModel.SparkStreamContextBytesReadFromDS]
		}
		
		
		composite metric context SparkStreamContextSumBytesWrittenToDS {
			metric SumBytesWrittenToDS
			grouping global
			window PeopleFlow.PeopleFlowMetrModel.Win20Min
			schedule PeopleFlowMetrModel.ScheduleSumBytesReadAndWritten
			object context SparkStreamContext
			composing contexts [PeopleFlowMetrModel.SparkStreamContextBytesWrittenToDS]
		}		
	}
	
}
   
   
   

