package camel.dsl.ui.hover 


import camel.dsl.services.CamelDslGrammarAccess
import com.google.inject.Inject
import org.eclipse.xtext.Keyword
import camel.dsl.ui.documentation.DocumentationReader

// This is Xtend , not Java
class MyKeywordHovers {
    @Inject CamelDslGrammarAccess ga;
    def hoverText(Keyword k) {
        val result = switch (k) {
        	//metric model coverage
        	case ga.metricTypeModelAccess.modelKeyword_3,
            case ga.metricTypeModelAccess.typeKeyword_2,
            case ga.metricTypeModelAccess.metricKeyword_1: ("<b>" + DocumentationReader.getFullDescription("MetricTypeModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
                
            case ga.rawMetricAccess.rawKeyword_0,
            case ga.rawMetricAccess.metricKeyword_1: ("<b>" + DocumentationReader.getFullDescription("RawMetric").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.compositeMetricAccess.compositeKeyword_0,
            case ga.compositeMetricAccess.metricKeyword_1: ("<b>" + DocumentationReader.getFullDescription("CompositeMetric").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.metricTemplateAccess.templateKeyword_0: ("<b>" + DocumentationReader.getFullDescription("MetricTemplate").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.metricVariableAccess.variableKeyword_0: ("<b>" + DocumentationReader.getFullDescription("MetricVariable").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.scheduleAccess.scheduleKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Schedule").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.windowAccess.windowKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Window").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.sensorAccess.sensorKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Sensor").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.functionAccess.functionKeyword_1: ("<b>" + DocumentationReader.getFullDescription("Function").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.objectContextAccess.objectKeyword_1,
            case ga.objectContextAccess.contextKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ObjectContext").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.rawMetricContextAccess.rawKeyword_0,
            case ga.rawMetricContextAccess.metricKeyword_1,
            case ga.rawMetricContextAccess.contextKeyword_2: ("<b>" + DocumentationReader.getFullDescription("RawMetricContext").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.compositeMetricContextAccess.compositeKeyword_0,
            case ga.compositeMetricContextAccess.metricKeyword_1,
            case ga.compositeMetricContextAccess.contextKeyword_2: ("<b>" + DocumentationReader.getFullDescription("CompositeMetricContext").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            //constraint coverage
            case ga.constraintModelAccess.constraintKeyword_1,
            case ga.constraintModelAccess.modelKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ConstraintModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.metricConstraintAccess.constraintKeyword_1,
            case ga.metricConstraintAccess.metricKeyword_0: ("<b>" + DocumentationReader.getFullDescription("MetricConstraint").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.metricVariableConstraintAccess.constraintKeyword_1,
            case ga.metricVariableConstraintAccess.variableKeyword_0: ("<b>" + DocumentationReader.getFullDescription("MetricVariableConstraint").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.logicalConstraintAccess.logicalKeyword_0,
            case ga.logicalConstraintAccess.constraintKeyword_1: ("<b>" + DocumentationReader.getFullDescription("LogicalConstraint").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.ifThenConstraintAccess.ifKeyword_0,
            case ga.ifThenConstraintAccess.constraintKeyword_1: ("<b>" + DocumentationReader.getFullDescription("IfThenConstraint").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            //unit model coverage
            case ga.unitModelAccess.unitKeyword_1,
            case ga.unitModelAccess.modelKeyword_2: ("<b>" + DocumentationReader.getFullDescription("UnitModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.dimensionlessAccess.dimensionlessKeyword_1: ("<b>" + DocumentationReader.getFullDescription("Dimensionless").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.singleUnitAccess.singleKeyword_1,
            case ga.singleUnitAccess.unitKeyword_2: ("<b>" + DocumentationReader.getFullDescription("SingleUnit").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.compositeUnitAccess.compositeKeyword_0,
            case ga.compositeUnitAccess.unitKeyword_1: ("<b>" + DocumentationReader.getFullDescription("CompositeUnit").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.unitDimensionAccess.dimensionKeyword_1: ("<b>" + DocumentationReader.getFullDescription("UnitDimension").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            //location model coverage
            case ga.locationModelAccess.locationKeyword_1,
            case ga.locationModelAccess.modelKeyword_2: ("<b>" + DocumentationReader.getFullDescription("LocationModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.geographicalRegionAccess.regionKeyword_0: ("<b>" + DocumentationReader.getFullDescription("GeographicalRegion").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.cloudLocationAccess.cloudKeyword_0,
            case ga.cloudLocationAccess.locationKeyword_1: ("<b>" + DocumentationReader.getFullDescription("CloudLocation").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            //type model coverage
            case ga.typeModelAccess.typeKeyword_1,
            case ga.typeModelAccess.modelKeyword_2: ("<b>" + DocumentationReader.getFullDescription("TypeModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.booleanValueAccess.booleanKeyword_0: ("<b>" + DocumentationReader.getFullDescription("BooleanValue").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.stringValueAccess.stringKeyword_0: ("<b>" + DocumentationReader.getFullDescription("StringValue").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.floatValueAccess.floatKeyword_0: ("<b>" + DocumentationReader.getFullDescription("FloatValue").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.doubleValueAccess.doubleKeyword_0: ("<b>" + DocumentationReader.getFullDescription("DoubleValue").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.rangeAccess.rangeKeyword_1: ("<b>" + DocumentationReader.getFullDescription("Range").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.rangeUnionAccess.rangeKeyword_0,
            case ga.rangeUnionAccess.unionKeyword_1: ("<b>" + DocumentationReader.getFullDescription("RangeUnion").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.listAccess.listKeyword_0: ("<b>" + DocumentationReader.getFullDescription("List").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.stringValueTypeAccess.stringKeyword_1,
            case ga.stringValueTypeAccess.typeKeyword_2: ("<b>" + DocumentationReader.getFullDescription("StringValueType").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.booleanValueTypeAccess.booleanKeyword_1,
            case ga.booleanValueTypeAccess.typeKeyword_2: ("<b>" + DocumentationReader.getFullDescription("BooleanValueType").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.limitAccess.limitKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Limit").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            // security model coverage
            case ga.securityModelAccess.securityKeyword_1,
            case ga.securityModelAccess.modelKeyword_2: ("<b>" + DocumentationReader.getFullDescription("SecurityModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.securityDomainAccess.domainKeyword_0: ("<b>" + DocumentationReader.getFullDescription("SecurityDomain").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.securityControlAccess.securityKeyword_0,
            case ga.securityControlAccess.controlKeyword_1: ("<b>" + DocumentationReader.getFullDescription("SecurityControl").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.securityAttribute_ImplAccess.securityKeyword_0,
            case ga.securityAttribute_ImplAccess.attributeKeyword_1: ("<b>" + DocumentationReader.getFullDescription("SecurityAttribute").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.certifiableAccess.certifiableKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Certifiable").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.rawSecurityMetricAccess.rawKeyword_0,
            case ga.rawSecurityMetricAccess.securityKeyword_1,
            case ga.rawSecurityMetricAccess.metricKeyword_2: ("<b>" + DocumentationReader.getFullDescription("RawSecurityMetric").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.compositeSecurityMetricAccess.compositeKeyword_0,
            case ga.compositeSecurityMetricAccess.securityKeyword_1,
            case ga.compositeSecurityMetricAccess.metricKeyword_2: ("<b>" + DocumentationReader.getFullDescription("CompositeSecurityMetric").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.securitySLOAccess.securityKeyword_0,
            case ga.securitySLOAccess.SLOKeyword_1: ("<b>" + DocumentationReader.getFullDescription("SecuritySLO").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            //organisation model coverage
            case ga.organisationModelAccess.organisationKeyword_1,
            case ga.organisationModelAccess.modelKeyword_2: ("<b>" + DocumentationReader.getFullDescription("OrganisationModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.organisation_ImplAccess.organisationKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Organisation").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.cloudProviderAccess.providerKeyword_0: ("<b>" + DocumentationReader.getFullDescription("CloudProvider").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.userAccess.userKeyword_0: ("<b>" + DocumentationReader.getFullDescription("User").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.roleAccess.roleKeyword_1: ("<b>" + DocumentationReader.getFullDescription("Role").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.userGroupAccess.userKeyword_0,
            case ga.userGroupAccess.groupKeyword_1: ("<b>" + DocumentationReader.getFullDescription("UserGroup").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.permissionAccess.permissionKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Permission").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.roleAssignmentAccess.roleKeyword_0,
            case ga.roleAssignmentAccess.assignmentKeyword_1: ("<b>" + DocumentationReader.getFullDescription("RoleAssignment").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.modelResourceFilterAccess.modelKeyword_0,
            case ga.modelResourceFilterAccess.resourceKeyword_1,
            case ga.modelResourceFilterAccess.filterKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ModelResourceFilter").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.serviceResourceFilterAccess.serviceKeyword_0,
            case ga.serviceResourceFilterAccess.resourceKeyword_1,
            case ga.serviceResourceFilterAccess.filterKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ServiceResourceFilter").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.dataResourceFilterAccess.dataKeyword_0,
            case ga.dataResourceFilterAccess.resourceKeyword_1,
            case ga.dataResourceFilterAccess.filterKeyword_2: ("<b>" + DocumentationReader.getFullDescription("DataResourceFilter").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.softwareComponentResourceFilterAccess.softwareKeyword_0,
            case ga.softwareComponentResourceFilterAccess.resourceKeyword_1,
            case ga.softwareComponentResourceFilterAccess.filterKeyword_2: ("<b>" + DocumentationReader.getFullDescription("SoftwareComponentResourceFilter").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.platformCredentialsAccess.platformKeyword_0,
            case ga.platformCredentialsAccess.credentialsKeyword_1: ("<b>" + DocumentationReader.getFullDescription("PlatformCredentials").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.cloudCredentialsAccess.cloudKeyword_0,
            case ga.cloudCredentialsAccess.credentialsKeyword_1: ("<b>" + DocumentationReader.getFullDescription("CloudCredentials").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.externalIdentifierAccess.externalKeyword_0,
            case ga.externalIdentifierAccess.identifierKeyword_1: ("<b>" + DocumentationReader.getFullDescription("ExternalIdentifier").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            //requirement model coverage
            case ga.requirementModelAccess.requirementKeyword_0,
            case ga.requirementModelAccess.modelKeyword_1: ("<b>" + DocumentationReader.getFullDescription("RequirementModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.resourceRequirementAccess.resourceKeyword_1,
            case ga.resourceRequirementAccess.requirementKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ResourceRequirement").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.paaSRequirementAccess.paasKeyword_1,
            case ga.paaSRequirementAccess.requirementKeyword_2: ("<b>" + DocumentationReader.getFullDescription("PaaSRequirement").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.OSRequirementAccess.osKeyword_0,
            case ga.OSRequirementAccess.requirementKeyword_1: ("<b>" + DocumentationReader.getFullDescription("OSRequirement").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.imageRequirementAccess.imageKeyword_0,
            case ga.imageRequirementAccess.requirementKeyword_1: ("<b>" + DocumentationReader.getFullDescription("ImageRequirement").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
                        
            case ga.providerRequirementAccess.providerKeyword_1,
            case ga.providerRequirementAccess.requirementKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ProviderRequirement").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.serviceLevelObjectiveAccess.sloKeyword_0: ("<b>" + DocumentationReader.getFullDescription("ServiceLevelObjective").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.optimisationRequirementAccess.optimisationKeyword_1,
            case ga.optimisationRequirementAccess.requirementKeyword_2: ("<b>" + DocumentationReader.getFullDescription("OptimisationRequirement").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.securityRequirementAccess.securityKeyword_0,
            case ga.securityRequirementAccess.requirementKeyword_1: ("<b>" + DocumentationReader.getFullDescription("SecurityRequirement").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.locationRequirementAccess.locationKeyword_0,
            case ga.locationRequirementAccess.requirementKeyword_1: ("<b>" + DocumentationReader.getFullDescription("LocationRequirement").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.horizontalScaleRequirementAccess.horizontalKeyword_0,
            case ga.horizontalScaleRequirementAccess.scaleKeyword_1,
            case ga.horizontalScaleRequirementAccess.requirementKeyword_2: ("<b>" + DocumentationReader.getFullDescription("HorizontalScaleRequirement").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.verticalScaleRequirementAccess.verticalKeyword_1,
            case ga.verticalScaleRequirementAccess.scaleKeyword_2,
            case ga.verticalScaleRequirementAccess.requirementKeyword_3: ("<b>" + DocumentationReader.getFullDescription("VerticalScaleRequirement").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            
            //deployment type model coverage
            case ga.deploymentTypeModelAccess.deploymentKeyword_1,
            case ga.deploymentTypeModelAccess.typeKeyword_2,
            case ga.deploymentTypeModelAccess.modelKeyword_3: ("<b>" + DocumentationReader.getFullDescription("DeploymentTypeModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.softwareComponentAccess.softwareKeyword_0: ("<b>" + DocumentationReader.getFullDescription("SoftwareComponent").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.VMAccess.vmKeyword_1: ("<b>" + DocumentationReader.getFullDescription("VM").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.paaSAccess.paasKeyword_1: ("<b>" + DocumentationReader.getFullDescription("PaaS").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.containerAccess.containerKeyword_1: ("<b>" + DocumentationReader.getFullDescription("Container").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.communicationAccess.communicationKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Communication").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.hostingAccess.hostingKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Hosting").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.providedCommunicationAccess.providedKeyword_1,
            case ga.providedCommunicationAccess.communicationKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ProvidedCommunication").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.requiredCommunicationAccess.requiredKeyword_0,
            case ga.requiredCommunicationAccess.communicationKeyword_1: ("<b>" + DocumentationReader.getFullDescription("RequiredCommunication").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.providedHostAccess.providedKeyword_1,
            case ga.providedHostAccess.hostKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ProvidedHost").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.requiredHostAccess.requiredKeyword_1,
            case ga.requiredHostAccess.hostKeyword_2: ("<b>" + DocumentationReader.getFullDescription("RequiredHost").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.scriptConfigurationAccess.scriptKeyword_1,
            case ga.scriptConfigurationAccess.configurationKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ScriptConfiguration").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.clusterConfigurationAccess.clusterKeyword_1,
            case ga.clusterConfigurationAccess.configurationKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ClusterConfiguration").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.paaSConfigurationAccess.paasKeyword_1,
            case ga.paaSConfigurationAccess.configurationKeyword_2: ("<b>" + DocumentationReader.getFullDescription("PaaSConfiguration").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.serverlessConfigurationAccess.serverlessKeyword_1,
            case ga.serverlessConfigurationAccess.configurationKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ServerlessConfiguration").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
           
            case ga.buildConfigurationAccess.buildKeyword_1,
            case ga.buildConfigurationAccess.configurationKeyword_2: ("<b>" + DocumentationReader.getFullDescription("BuildConfiguration").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.eventConfigurationAccess.eventKeyword_1,
            case ga.eventConfigurationAccess.configurationKeyword_2: ("<b>" + DocumentationReader.getFullDescription("EventConfiguration").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
           
            case ga.locationCouplingAccess.couplingKeyword_0: ("<b>" + DocumentationReader.getFullDescription("LocationCoupling").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.requirementSetAccess.requirementsKeyword_1: ("<b>" + DocumentationReader.getFullDescription("RequirementSet").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            //data type model coverage
            case ga.dataTypeModelAccess.dataKeyword_1,
            case ga.dataTypeModelAccess.typeKeyword_2,
            case ga.dataTypeModelAccess.modelKeyword_3: ("<b>" + DocumentationReader.getFullDescription("DataTypeModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.dataAccess.dataKeyword_1: ("<b>" + DocumentationReader.getFullDescription("Data").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.dataSourceAccess.dataKeyword_0,
            case ga.dataSourceAccess.sourceKeyword_1: ("<b>" + DocumentationReader.getFullDescription("DataSource").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            //data instance model coverage
            case ga.dataInstanceModelAccess.dataKeyword_1,
            case ga.dataInstanceModelAccess.instanceKeyword_2,
            case ga.dataInstanceModelAccess.modelKeyword_3: ("<b>" + DocumentationReader.getFullDescription("DataInstanceModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.dataInstanceAccess.dataKeyword_1,
            case ga.dataInstanceAccess.instanceKeyword_2: ("<b>" + DocumentationReader.getFullDescription("DataInstance").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.dataSourceInstanceAccess.dataKeyword_0,
            case ga.dataSourceInstanceAccess.sourceKeyword_1,
            case ga.dataSourceInstanceAccess.instanceKeyword_2,: ("<b>" + DocumentationReader.getFullDescription("DataSourceInstance").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            //scalability model coverage
            case ga.scalabilityModelAccess.scalabilityKeyword_1,
            case ga.scalabilityModelAccess.modelKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ScalabilityModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.scalabilityRuleAccess.ruleKeyword_0: ("<b>" + DocumentationReader.getFullDescription("ScalabilityRule").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.horizontalScalingActionAccess.horizontalKeyword_0,
            case ga.horizontalScalingActionAccess.scalingKeyword_1,
            case ga.horizontalScalingActionAccess.actionKeyword_2: ("<b>" + DocumentationReader.getFullDescription("HorizontalScalingAction").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.verticalScalingActionAccess.verticalKeyword_0,
            case ga.verticalScalingActionAccess.scalingKeyword_1,
            case ga.verticalScalingActionAccess.actionKeyword_2: ("<b>" + DocumentationReader.getFullDescription("VerticalScalingAction").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.functionalEventAccess.functionalKeyword_0: ("<b>" + DocumentationReader.getFullDescription("FunctionalEvent").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.nonFunctionalEventAccess.nonfunctionalKeyword_0: ("<b>" + DocumentationReader.getFullDescription("NonFunctionalEvent").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.unaryEventPatternAccess.unaryKeyword_0,
            case ga.unaryEventPatternAccess.eventKeyword_1,
            case ga.unaryEventPatternAccess.patternKeyword_2: ("<b>" + DocumentationReader.getFullDescription("UnaryEventPattern").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.binaryEventPatternAccess.binaryKeyword_0,
            case ga.binaryEventPatternAccess.eventKeyword_1,
            case ga.binaryEventPatternAccess.patternKeyword_2: ("<b>" + DocumentationReader.getFullDescription("BinaryEventPattern").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.timerAccess.timerKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Timer").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            //metadata model coverage
            case ga.metaDataModelAccess.metadataKeyword_1,
            case ga.metaDataModelAccess.modelKeyword_2: ("<b>" + DocumentationReader.getFullDescription("MetaDataModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.mmsConceptAccess.conceptKeyword_0: ("<b>" + DocumentationReader.getFullDescription("MmsConcept").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.mmsPropertyAccess.propertyKeyword_0: ("<b>" + DocumentationReader.getFullDescription("MmsProperty").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.mmsConceptInstanceAccess.conceptKeyword_0,
            case ga.mmsConceptInstanceAccess.instanceKeyword_1: ("<b>" + DocumentationReader.getFullDescription("MmsConceptInstance").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.mmsPropertyInstanceAccess.propertyKeyword_0,
            case ga.mmsPropertyInstanceAccess.instanceKeyword_1: ("<b>" + DocumentationReader.getFullDescription("MmsPropertyInstance").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            //camel model coverage
            case ga.camelModelAccess.camelKeyword_1,
            case ga.camelModelAccess.modelKeyword_2: ("<b>" + DocumentationReader.getFullDescription("CamelModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.qualityAttribute_ImplAccess.qualityKeyword_1,
            case ga.qualityAttribute_ImplAccess.attributeKeyword_2: ("<b>" + DocumentationReader.getFullDescription("QualityAttribute").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.attribute_ImplAccess.attributeKeyword_1: ("<b>" + DocumentationReader.getFullDescription("Attribute").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.measurableAttributeAccess.measurableKeyword_1,
            case ga.measurableAttributeAccess.attributeKeyword_2: ("<b>" + DocumentationReader.getFullDescription("MeasurableAttribute").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.applicationAccess.applicationKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Application").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.actionAccess.actionKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Action").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.featureAccess.featureKeyword_1: ("<b>" + DocumentationReader.getFullDescription("Feature").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            default: ''''''
        }
        result.toString;
    }
}