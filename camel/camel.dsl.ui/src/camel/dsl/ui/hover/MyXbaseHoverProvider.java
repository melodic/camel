package camel.dsl.ui.hover;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.jface.internal.text.html.HTMLPrinter;
import org.eclipse.jface.text.IRegion;
import org.eclipse.xtext.Keyword;
import org.eclipse.xtext.ui.editor.hover.html.DefaultEObjectHoverProvider;
import org.eclipse.xtext.ui.editor.hover.html.XtextBrowserInformationControlInput;
import camel.dsl.ui.hover.MyKeywordHovers;

import com.google.inject.Inject;

import camel.metric.Metric;
 
@SuppressWarnings("restriction")
public class MyXbaseHoverProvider extends DefaultEObjectHoverProvider {
    /** Utility mapping keywords and hovertext. */
    @Inject MyKeywordHovers keywordHovers;
    //@Inject org.eclipse.jface.viewers.ILabelProvider labelProvider;
    
    @Override
    protected String getFirstLine(EObject o) {
		if (o instanceof Metric) {
			return "Metric: " + ((Metric)o).getName();
		}
		return super.getFirstLine(o);
	}
 
    @Override
    protected XtextBrowserInformationControlInput getHoverInfo(EObject obj, IRegion region, XtextBrowserInformationControlInput prev) {
        if (obj instanceof Keyword) {
            String html = getHoverInfoAsHtml(obj);
            if (html != null) {
                StringBuffer buffer = new StringBuffer(html);
                HTMLPrinter.insertPageProlog(buffer, 0, getStyleSheet());
                HTMLPrinter.addPageEpilog(buffer);
                return new XtextBrowserInformationControlInput(prev, obj, buffer.toString(), getLabelProvider());
            }
        }
        return super.getHoverInfo(obj, region, prev);
    }
 
    @Override
    protected String getHoverInfoAsHtml(EObject o){
        if (o instanceof Keyword)
            return keywordHovers.hoverText((Keyword) o);
        return super.getHoverInfoAsHtml(o);
    }
}