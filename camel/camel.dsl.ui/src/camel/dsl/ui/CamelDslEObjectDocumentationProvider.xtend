package camel.dsl.ui

import org.eclipse.emf.ecore.EObject
import org.eclipse.xtext.documentation.IEObjectDocumentationProvider
import camel.dsl.ui.documentation.DocumentationReader
import camel.core.NamedElement

class CamelDslEObjectDocumentationProvider implements IEObjectDocumentationProvider {
	override String getDocumentation(EObject o) {
		//System.out.println("1. Class name is: " + o.eClass.name + " " + DocumentationReader.getShortDescription(o.eClass.name));
		if (o instanceof NamedElement){
			//System.out.println("2. Class name is: " + o.eClass.name + " " + DocumentationReader.getShortDescription(o.eClass.name));
			return DocumentationReader.getShortDescription(o.eClass.name);
		}
		return null
	}
}
