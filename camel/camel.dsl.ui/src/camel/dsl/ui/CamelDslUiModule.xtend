/*
 * generated by Xtext 2.12.0
 */
package camel.dsl.ui

import org.eclipse.xtend.lib.annotations.FinalFieldsConstructor
import org.eclipse.xtext.documentation.IEObjectDocumentationProvider
import org.eclipse.xtext.ui.editor.hover.IEObjectHover
import camel.dsl.ui.hover.MyXbaseDispatchingEObjectTextHover
import org.eclipse.xtext.ui.editor.hover.IEObjectHoverProvider
import camel.dsl.ui.hover.MyXbaseHoverProvider

/**
 * Use this class to register components to be used within the Eclipse IDE.
 */
@FinalFieldsConstructor
class CamelDslUiModule extends AbstractCamelDslUiModule {
	
	def Class<? extends IEObjectDocumentationProvider> bindIEObjectDocumentationProvider() {
		return CamelDslEObjectDocumentationProvider
	}
	
	override Class<? extends IEObjectHover> bindIEObjectHover() {
		return MyXbaseDispatchingEObjectTextHover
	}
	
	def Class<? extends IEObjectHoverProvider> bindIEObjectHoverProvider() {
		return MyXbaseHoverProvider
	}
	
}
