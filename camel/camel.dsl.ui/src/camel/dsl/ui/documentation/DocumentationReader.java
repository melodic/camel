package camel.dsl.ui.documentation;

import java.io.File;
import java.io.FileInputStream;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.eclipse.core.runtime.FileLocator;
import org.eclipse.core.runtime.Path;
import org.eclipse.core.runtime.Platform;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;


public class DocumentationReader {
	public static class DocumentationEntry{
		public String className;
		public String shortDescr;
		public String fullDescr;
		
		public DocumentationEntry() {
			
		}
	}
	
	static {
		readEntries();
	}
	
	private static Map<String,DocumentationEntry> nameToEntry;
	private static final String fileName = "input/documentation.xlsx";
	
	private static void readEntries() {
		nameToEntry = new HashMap<String,DocumentationEntry>();
		
		try {
			URL url = FileLocator.find(Platform.getBundle("camel.dsl.ui"), new Path(fileName), null);
			System.out.println("Got url:" + url);
			//FileInputStream file = new FileInputStream();
		    
			//Get the workbook instance for XLS file
			XSSFWorkbook workbook = new XSSFWorkbook(url.openStream());
			
			//Get first sheet from the workbook
			XSSFSheet sheet = workbook.getSheetAt(0);
			    
			//Iterate through each rows from first sheet
			Iterator<Row> rowIterator = sheet.iterator();
			if (rowIterator.hasNext()) rowIterator.next();
			while(rowIterator.hasNext()){
				Row row = rowIterator.next();
				Cell cell = row.getCell(0);
				if (cell != null) {
					String name = cell.getStringCellValue();
					cell = row.getCell(1);
					String shortDescr = cell.getStringCellValue();
					cell = row.getCell(2);
					String fullDescr = cell.getStringCellValue();
					DocumentationEntry de = new DocumentationEntry();
					de.className = name;
					de.shortDescr = shortDescr;
					de.fullDescr = fullDescr;
					nameToEntry.put(name, de);
				}
			}
			//file.close();
		}
		catch(Exception e) {
			e.printStackTrace();
		}

	}
	
	public static String getShortDescription(String className) {
		DocumentationEntry de = nameToEntry.get(className);
		System.out.println("DocumentationEntry: " + de);
		if (de != null) return de.shortDescr;
		return null;
	}
	
	public static String getFullDescription(String className) {
		DocumentationEntry de = nameToEntry.get(className);
		if (de != null) return de.fullDescr;
		return null;
	}
	
	public static void main(String[] args) {
		System.out.println("Get RawMetric: " + getShortDescription("RawMetric"));
	}
	
}
