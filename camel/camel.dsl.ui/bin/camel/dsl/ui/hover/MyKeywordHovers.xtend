package camel.dsl.ui.hover 


import camel.dsl.services.CamelDslGrammarAccess
import com.google.inject.Inject
import org.eclipse.xtext.Keyword
import camel.dsl.ui.documentation.DocumentationReader

// This is Xtend , not Java
class MyKeywordHovers {
    @Inject CamelDslGrammarAccess ga;
    def hoverText(Keyword k) {
        val result = switch (k) {
            case ga.metricModelAccess.modelKeyword_2,
            case ga.metricModelAccess.metricKeyword_1: ("<b>" + DocumentationReader.getFullDescription("MetricModel").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
                
            case ga.rawMetricAccess.rawKeyword_0,
            case ga.rawMetricAccess.metricKeyword_1: ("<b>" + DocumentationReader.getFullDescription("RawMetric").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.compositeMetricAccess.compositeKeyword_0,
            case ga.compositeMetricAccess.metricKeyword_1: ("<b>" + DocumentationReader.getFullDescription("CompositeMetric").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.metricTemplateAccess.templateKeyword_0: ("<b>" + DocumentationReader.getFullDescription("MetricTemplate").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.metricVariableAccess.variableKeyword_0: ("<b>" + DocumentationReader.getFullDescription("MetricVariable").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.scheduleAccess.scheduleKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Schedule").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.windowAccess.windowKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Window").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.sensorAccess.sensorKeyword_0: ("<b>" + DocumentationReader.getFullDescription("Sensor").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.measurableAttributeAccess.measurableKeyword_1,
            case ga.measurableAttributeAccess.attributeKeyword_2: ("<b>" + DocumentationReader.getFullDescription("MeasurableAttribute").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.functionAccess.functionKeyword_1: ("<b>" + DocumentationReader.getFullDescription("Function").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.objectContextAccess.objectKeyword_1,
            case ga.objectContextAccess.contextKeyword_2: ("<b>" + DocumentationReader.getFullDescription("ObjectContext").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.rawMetricContextAccess.rawKeyword_0,
            case ga.rawMetricContextAccess.metricKeyword_1,
            case ga.rawMetricContextAccess.contextKeyword_2: ("<b>" + DocumentationReader.getFullDescription("RawMetricContext").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            
            case ga.compositeMetricContextAccess.compositeKeyword_0,
            case ga.compositeMetricContextAccess.metricKeyword_1,
            case ga.compositeMetricContextAccess.contextKeyword_2: ("<b>" + DocumentationReader.getFullDescription("CompositeMetricContext").replace("Syntax:","</b><br><br><i>Syntax:</i><br>").replace("\n","<br>"))
            default: ''''''
        }
        result.toString;
    }
}