package camel.dsl.ui;

import camel.core.NamedElement;
import camel.dsl.ui.documentation.DocumentationReader;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.documentation.IEObjectDocumentationProvider;

@SuppressWarnings("all")
public class CamelDslEObjectDocumentationProvider implements IEObjectDocumentationProvider {
  @Override
  public String getDocumentation(final EObject o) {
    if ((o instanceof NamedElement)) {
      return DocumentationReader.getShortDescription(((NamedElement)o).eClass().getName());
    }
    return null;
  }
}
