package camel.dsl.ui.hover;

import camel.dsl.services.CamelDslGrammarAccess;
import camel.dsl.ui.documentation.DocumentationReader;
import com.google.common.base.Objects;
import com.google.inject.Inject;
import org.eclipse.xtend2.lib.StringConcatenation;
import org.eclipse.xtext.Keyword;

@SuppressWarnings("all")
public class MyKeywordHovers {
  @Inject
  private CamelDslGrammarAccess ga;
  
  public String hoverText(final Keyword k) {
    String _xblockexpression = null;
    {
      String _switchResult = null;
      boolean _matched = false;
      Keyword _modelKeyword_3 = this.ga.getMetricTypeModelAccess().getModelKeyword_3();
      if (Objects.equal(k, _modelKeyword_3)) {
        _matched=true;
      }
      if (!_matched) {
        Keyword _typeKeyword_2 = this.ga.getMetricTypeModelAccess().getTypeKeyword_2();
        if (Objects.equal(k, _typeKeyword_2)) {
          _matched=true;
        }
      }
      if (!_matched) {
        Keyword _metricKeyword_1 = this.ga.getMetricTypeModelAccess().getMetricKeyword_1();
        if (Objects.equal(k, _metricKeyword_1)) {
          _matched=true;
        }
      }
      if (_matched) {
        String _replace = DocumentationReader.getFullDescription("MetricTypeModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
        _switchResult = ("<b>" + _replace);
      }
      if (!_matched) {
        Keyword _rawKeyword_0 = this.ga.getRawMetricAccess().getRawKeyword_0();
        if (Objects.equal(k, _rawKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _metricKeyword_1_1 = this.ga.getRawMetricAccess().getMetricKeyword_1();
          if (Objects.equal(k, _metricKeyword_1_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_1 = DocumentationReader.getFullDescription("RawMetric").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_1);
        }
      }
      if (!_matched) {
        Keyword _compositeKeyword_0 = this.ga.getCompositeMetricAccess().getCompositeKeyword_0();
        if (Objects.equal(k, _compositeKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _metricKeyword_1_2 = this.ga.getCompositeMetricAccess().getMetricKeyword_1();
          if (Objects.equal(k, _metricKeyword_1_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_2 = DocumentationReader.getFullDescription("CompositeMetric").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_2);
        }
      }
      if (!_matched) {
        Keyword _templateKeyword_0 = this.ga.getMetricTemplateAccess().getTemplateKeyword_0();
        if (Objects.equal(k, _templateKeyword_0)) {
          _matched=true;
          String _replace_3 = DocumentationReader.getFullDescription("MetricTemplate").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_3);
        }
      }
      if (!_matched) {
        Keyword _variableKeyword_0 = this.ga.getMetricVariableAccess().getVariableKeyword_0();
        if (Objects.equal(k, _variableKeyword_0)) {
          _matched=true;
          String _replace_4 = DocumentationReader.getFullDescription("MetricVariable").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_4);
        }
      }
      if (!_matched) {
        Keyword _scheduleKeyword_0 = this.ga.getScheduleAccess().getScheduleKeyword_0();
        if (Objects.equal(k, _scheduleKeyword_0)) {
          _matched=true;
          String _replace_5 = DocumentationReader.getFullDescription("Schedule").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_5);
        }
      }
      if (!_matched) {
        Keyword _windowKeyword_0 = this.ga.getWindowAccess().getWindowKeyword_0();
        if (Objects.equal(k, _windowKeyword_0)) {
          _matched=true;
          String _replace_6 = DocumentationReader.getFullDescription("Window").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_6);
        }
      }
      if (!_matched) {
        Keyword _sensorKeyword_0 = this.ga.getSensorAccess().getSensorKeyword_0();
        if (Objects.equal(k, _sensorKeyword_0)) {
          _matched=true;
          String _replace_7 = DocumentationReader.getFullDescription("Sensor").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_7);
        }
      }
      if (!_matched) {
        Keyword _functionKeyword_1 = this.ga.getFunctionAccess().getFunctionKeyword_1();
        if (Objects.equal(k, _functionKeyword_1)) {
          _matched=true;
          String _replace_8 = DocumentationReader.getFullDescription("Function").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_8);
        }
      }
      if (!_matched) {
        Keyword _objectKeyword_1 = this.ga.getObjectContextAccess().getObjectKeyword_1();
        if (Objects.equal(k, _objectKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _contextKeyword_2 = this.ga.getObjectContextAccess().getContextKeyword_2();
          if (Objects.equal(k, _contextKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_9 = DocumentationReader.getFullDescription("ObjectContext").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_9);
        }
      }
      if (!_matched) {
        Keyword _rawKeyword_0_1 = this.ga.getRawMetricContextAccess().getRawKeyword_0();
        if (Objects.equal(k, _rawKeyword_0_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _metricKeyword_1_3 = this.ga.getRawMetricContextAccess().getMetricKeyword_1();
          if (Objects.equal(k, _metricKeyword_1_3)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _contextKeyword_2_1 = this.ga.getRawMetricContextAccess().getContextKeyword_2();
          if (Objects.equal(k, _contextKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_10 = DocumentationReader.getFullDescription("RawMetricContext").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_10);
        }
      }
      if (!_matched) {
        Keyword _compositeKeyword_0_1 = this.ga.getCompositeMetricContextAccess().getCompositeKeyword_0();
        if (Objects.equal(k, _compositeKeyword_0_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _metricKeyword_1_4 = this.ga.getCompositeMetricContextAccess().getMetricKeyword_1();
          if (Objects.equal(k, _metricKeyword_1_4)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _contextKeyword_2_2 = this.ga.getCompositeMetricContextAccess().getContextKeyword_2();
          if (Objects.equal(k, _contextKeyword_2_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_11 = DocumentationReader.getFullDescription("CompositeMetricContext").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_11);
        }
      }
      if (!_matched) {
        Keyword _constraintKeyword_1 = this.ga.getConstraintModelAccess().getConstraintKeyword_1();
        if (Objects.equal(k, _constraintKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _modelKeyword_2 = this.ga.getConstraintModelAccess().getModelKeyword_2();
          if (Objects.equal(k, _modelKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_12 = DocumentationReader.getFullDescription("ConstraintModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_12);
        }
      }
      if (!_matched) {
        Keyword _constraintKeyword_1_1 = this.ga.getMetricConstraintAccess().getConstraintKeyword_1();
        if (Objects.equal(k, _constraintKeyword_1_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _metricKeyword_0 = this.ga.getMetricConstraintAccess().getMetricKeyword_0();
          if (Objects.equal(k, _metricKeyword_0)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_13 = DocumentationReader.getFullDescription("MetricConstraint").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_13);
        }
      }
      if (!_matched) {
        Keyword _constraintKeyword_1_2 = this.ga.getMetricVariableConstraintAccess().getConstraintKeyword_1();
        if (Objects.equal(k, _constraintKeyword_1_2)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _variableKeyword_0_1 = this.ga.getMetricVariableConstraintAccess().getVariableKeyword_0();
          if (Objects.equal(k, _variableKeyword_0_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_14 = DocumentationReader.getFullDescription("MetricVariableConstraint").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_14);
        }
      }
      if (!_matched) {
        Keyword _logicalKeyword_0 = this.ga.getLogicalConstraintAccess().getLogicalKeyword_0();
        if (Objects.equal(k, _logicalKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _constraintKeyword_1_3 = this.ga.getLogicalConstraintAccess().getConstraintKeyword_1();
          if (Objects.equal(k, _constraintKeyword_1_3)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_15 = DocumentationReader.getFullDescription("LogicalConstraint").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_15);
        }
      }
      if (!_matched) {
        Keyword _ifKeyword_0 = this.ga.getIfThenConstraintAccess().getIfKeyword_0();
        if (Objects.equal(k, _ifKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _constraintKeyword_1_4 = this.ga.getIfThenConstraintAccess().getConstraintKeyword_1();
          if (Objects.equal(k, _constraintKeyword_1_4)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_16 = DocumentationReader.getFullDescription("IfThenConstraint").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_16);
        }
      }
      if (!_matched) {
        Keyword _constraintKeyword_1_5 = this.ga.getConstraintContextAccess().getConstraintKeyword_1();
        if (Objects.equal(k, _constraintKeyword_1_5)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _contextKeyword_2_3 = this.ga.getConstraintContextAccess().getContextKeyword_2();
          if (Objects.equal(k, _contextKeyword_2_3)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_17 = DocumentationReader.getFullDescription("ConstraintContext").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_17);
        }
      }
      if (!_matched) {
        Keyword _unitKeyword_1 = this.ga.getUnitModelAccess().getUnitKeyword_1();
        if (Objects.equal(k, _unitKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _modelKeyword_2_1 = this.ga.getUnitModelAccess().getModelKeyword_2();
          if (Objects.equal(k, _modelKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_18 = DocumentationReader.getFullDescription("UnitModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_18);
        }
      }
      if (!_matched) {
        Keyword _dimensionlessKeyword_1 = this.ga.getDimensionlessAccess().getDimensionlessKeyword_1();
        if (Objects.equal(k, _dimensionlessKeyword_1)) {
          _matched=true;
          String _replace_19 = DocumentationReader.getFullDescription("Dimensionless").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_19);
        }
      }
      if (!_matched) {
        Keyword _singleKeyword_1 = this.ga.getSingleUnitAccess().getSingleKeyword_1();
        if (Objects.equal(k, _singleKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _unitKeyword_2 = this.ga.getSingleUnitAccess().getUnitKeyword_2();
          if (Objects.equal(k, _unitKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_20 = DocumentationReader.getFullDescription("SingleUnit").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_20);
        }
      }
      if (!_matched) {
        Keyword _compositeKeyword_0_2 = this.ga.getCompositeUnitAccess().getCompositeKeyword_0();
        if (Objects.equal(k, _compositeKeyword_0_2)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _unitKeyword_1_1 = this.ga.getCompositeUnitAccess().getUnitKeyword_1();
          if (Objects.equal(k, _unitKeyword_1_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_21 = DocumentationReader.getFullDescription("CompositeUnit").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_21);
        }
      }
      if (!_matched) {
        Keyword _dimensionKeyword_1 = this.ga.getUnitDimensionAccess().getDimensionKeyword_1();
        if (Objects.equal(k, _dimensionKeyword_1)) {
          _matched=true;
          String _replace_22 = DocumentationReader.getFullDescription("UnitDimension").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_22);
        }
      }
      if (!_matched) {
        Keyword _locationKeyword_1 = this.ga.getLocationModelAccess().getLocationKeyword_1();
        if (Objects.equal(k, _locationKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _modelKeyword_2_2 = this.ga.getLocationModelAccess().getModelKeyword_2();
          if (Objects.equal(k, _modelKeyword_2_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_23 = DocumentationReader.getFullDescription("LocationModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_23);
        }
      }
      if (!_matched) {
        Keyword _regionKeyword_0 = this.ga.getGeographicalRegionAccess().getRegionKeyword_0();
        if (Objects.equal(k, _regionKeyword_0)) {
          _matched=true;
          String _replace_24 = DocumentationReader.getFullDescription("GeographicalRegion").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_24);
        }
      }
      if (!_matched) {
        Keyword _cloudKeyword_0 = this.ga.getCloudLocationAccess().getCloudKeyword_0();
        if (Objects.equal(k, _cloudKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _locationKeyword_1_1 = this.ga.getCloudLocationAccess().getLocationKeyword_1();
          if (Objects.equal(k, _locationKeyword_1_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_25 = DocumentationReader.getFullDescription("CloudLocation").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_25);
        }
      }
      if (!_matched) {
        Keyword _typeKeyword_1 = this.ga.getTypeModelAccess().getTypeKeyword_1();
        if (Objects.equal(k, _typeKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _modelKeyword_2_3 = this.ga.getTypeModelAccess().getModelKeyword_2();
          if (Objects.equal(k, _modelKeyword_2_3)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_26 = DocumentationReader.getFullDescription("TypeModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_26);
        }
      }
      if (!_matched) {
        Keyword _booleanKeyword_0 = this.ga.getBooleanValueAccess().getBooleanKeyword_0();
        if (Objects.equal(k, _booleanKeyword_0)) {
          _matched=true;
          String _replace_27 = DocumentationReader.getFullDescription("BooleanValue").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_27);
        }
      }
      if (!_matched) {
        Keyword _stringKeyword_0 = this.ga.getStringValueAccess().getStringKeyword_0();
        if (Objects.equal(k, _stringKeyword_0)) {
          _matched=true;
          String _replace_28 = DocumentationReader.getFullDescription("StringValue").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_28);
        }
      }
      if (!_matched) {
        Keyword _floatKeyword_0 = this.ga.getFloatValueAccess().getFloatKeyword_0();
        if (Objects.equal(k, _floatKeyword_0)) {
          _matched=true;
          String _replace_29 = DocumentationReader.getFullDescription("FloatValue").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_29);
        }
      }
      if (!_matched) {
        Keyword _doubleKeyword_0 = this.ga.getDoubleValueAccess().getDoubleKeyword_0();
        if (Objects.equal(k, _doubleKeyword_0)) {
          _matched=true;
          String _replace_30 = DocumentationReader.getFullDescription("DoubleValue").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_30);
        }
      }
      if (!_matched) {
        Keyword _rangeKeyword_1 = this.ga.getRangeAccess().getRangeKeyword_1();
        if (Objects.equal(k, _rangeKeyword_1)) {
          _matched=true;
          String _replace_31 = DocumentationReader.getFullDescription("Range").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_31);
        }
      }
      if (!_matched) {
        Keyword _rangeKeyword_0 = this.ga.getRangeUnionAccess().getRangeKeyword_0();
        if (Objects.equal(k, _rangeKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _unionKeyword_1 = this.ga.getRangeUnionAccess().getUnionKeyword_1();
          if (Objects.equal(k, _unionKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_32 = DocumentationReader.getFullDescription("RangeUnion").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_32);
        }
      }
      if (!_matched) {
        Keyword _listKeyword_0 = this.ga.getListAccess().getListKeyword_0();
        if (Objects.equal(k, _listKeyword_0)) {
          _matched=true;
          String _replace_33 = DocumentationReader.getFullDescription("List").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_33);
        }
      }
      if (!_matched) {
        Keyword _stringKeyword_1 = this.ga.getStringValueTypeAccess().getStringKeyword_1();
        if (Objects.equal(k, _stringKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _typeKeyword_2_1 = this.ga.getStringValueTypeAccess().getTypeKeyword_2();
          if (Objects.equal(k, _typeKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_34 = DocumentationReader.getFullDescription("StringValueType").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_34);
        }
      }
      if (!_matched) {
        Keyword _booleanKeyword_1 = this.ga.getBooleanValueTypeAccess().getBooleanKeyword_1();
        if (Objects.equal(k, _booleanKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _typeKeyword_2_2 = this.ga.getBooleanValueTypeAccess().getTypeKeyword_2();
          if (Objects.equal(k, _typeKeyword_2_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_35 = DocumentationReader.getFullDescription("BooleanValueType").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_35);
        }
      }
      if (!_matched) {
        Keyword _limitKeyword_0 = this.ga.getLimitAccess().getLimitKeyword_0();
        if (Objects.equal(k, _limitKeyword_0)) {
          _matched=true;
          String _replace_36 = DocumentationReader.getFullDescription("Limit").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_36);
        }
      }
      if (!_matched) {
        Keyword _securityKeyword_1 = this.ga.getSecurityModelAccess().getSecurityKeyword_1();
        if (Objects.equal(k, _securityKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _modelKeyword_2_4 = this.ga.getSecurityModelAccess().getModelKeyword_2();
          if (Objects.equal(k, _modelKeyword_2_4)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_37 = DocumentationReader.getFullDescription("SecurityModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_37);
        }
      }
      if (!_matched) {
        Keyword _domainKeyword_0 = this.ga.getSecurityDomainAccess().getDomainKeyword_0();
        if (Objects.equal(k, _domainKeyword_0)) {
          _matched=true;
          String _replace_38 = DocumentationReader.getFullDescription("SecurityDomain").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_38);
        }
      }
      if (!_matched) {
        Keyword _securityKeyword_0 = this.ga.getSecurityControlAccess().getSecurityKeyword_0();
        if (Objects.equal(k, _securityKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _controlKeyword_1 = this.ga.getSecurityControlAccess().getControlKeyword_1();
          if (Objects.equal(k, _controlKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_39 = DocumentationReader.getFullDescription("SecurityControl").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_39);
        }
      }
      if (!_matched) {
        Keyword _securityKeyword_0_1 = this.ga.getSecurityAttribute_ImplAccess().getSecurityKeyword_0();
        if (Objects.equal(k, _securityKeyword_0_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _attributeKeyword_1 = this.ga.getSecurityAttribute_ImplAccess().getAttributeKeyword_1();
          if (Objects.equal(k, _attributeKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_40 = DocumentationReader.getFullDescription("SecurityAttribute").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_40);
        }
      }
      if (!_matched) {
        Keyword _certifiableKeyword_0 = this.ga.getCertifiableAccess().getCertifiableKeyword_0();
        if (Objects.equal(k, _certifiableKeyword_0)) {
          _matched=true;
          String _replace_41 = DocumentationReader.getFullDescription("Certifiable").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_41);
        }
      }
      if (!_matched) {
        Keyword _rawKeyword_0_2 = this.ga.getRawSecurityMetricAccess().getRawKeyword_0();
        if (Objects.equal(k, _rawKeyword_0_2)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _securityKeyword_1_1 = this.ga.getRawSecurityMetricAccess().getSecurityKeyword_1();
          if (Objects.equal(k, _securityKeyword_1_1)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _metricKeyword_2 = this.ga.getRawSecurityMetricAccess().getMetricKeyword_2();
          if (Objects.equal(k, _metricKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_42 = DocumentationReader.getFullDescription("RawSecurityMetric").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_42);
        }
      }
      if (!_matched) {
        Keyword _compositeKeyword_0_3 = this.ga.getCompositeSecurityMetricAccess().getCompositeKeyword_0();
        if (Objects.equal(k, _compositeKeyword_0_3)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _securityKeyword_1_2 = this.ga.getCompositeSecurityMetricAccess().getSecurityKeyword_1();
          if (Objects.equal(k, _securityKeyword_1_2)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _metricKeyword_2_1 = this.ga.getCompositeSecurityMetricAccess().getMetricKeyword_2();
          if (Objects.equal(k, _metricKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_43 = DocumentationReader.getFullDescription("CompositeSecurityMetric").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_43);
        }
      }
      if (!_matched) {
        Keyword _securityKeyword_0_2 = this.ga.getSecuritySLOAccess().getSecurityKeyword_0();
        if (Objects.equal(k, _securityKeyword_0_2)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _sLOKeyword_1 = this.ga.getSecuritySLOAccess().getSLOKeyword_1();
          if (Objects.equal(k, _sLOKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_44 = DocumentationReader.getFullDescription("SecuritySLO").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_44);
        }
      }
      if (!_matched) {
        Keyword _organisationKeyword_1 = this.ga.getOrganisationModelAccess().getOrganisationKeyword_1();
        if (Objects.equal(k, _organisationKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _modelKeyword_2_5 = this.ga.getOrganisationModelAccess().getModelKeyword_2();
          if (Objects.equal(k, _modelKeyword_2_5)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_45 = DocumentationReader.getFullDescription("OrganisationModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_45);
        }
      }
      if (!_matched) {
        Keyword _organisationKeyword_0 = this.ga.getOrganisation_ImplAccess().getOrganisationKeyword_0();
        if (Objects.equal(k, _organisationKeyword_0)) {
          _matched=true;
          String _replace_46 = DocumentationReader.getFullDescription("Organisation").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_46);
        }
      }
      if (!_matched) {
        Keyword _providerKeyword_0 = this.ga.getCloudProviderAccess().getProviderKeyword_0();
        if (Objects.equal(k, _providerKeyword_0)) {
          _matched=true;
          String _replace_47 = DocumentationReader.getFullDescription("CloudProvider").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_47);
        }
      }
      if (!_matched) {
        Keyword _userKeyword_0 = this.ga.getUserAccess().getUserKeyword_0();
        if (Objects.equal(k, _userKeyword_0)) {
          _matched=true;
          String _replace_48 = DocumentationReader.getFullDescription("User").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_48);
        }
      }
      if (!_matched) {
        Keyword _roleKeyword_1 = this.ga.getRoleAccess().getRoleKeyword_1();
        if (Objects.equal(k, _roleKeyword_1)) {
          _matched=true;
          String _replace_49 = DocumentationReader.getFullDescription("Role").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_49);
        }
      }
      if (!_matched) {
        Keyword _userKeyword_0_1 = this.ga.getUserGroupAccess().getUserKeyword_0();
        if (Objects.equal(k, _userKeyword_0_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _groupKeyword_1 = this.ga.getUserGroupAccess().getGroupKeyword_1();
          if (Objects.equal(k, _groupKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_50 = DocumentationReader.getFullDescription("UserGroup").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_50);
        }
      }
      if (!_matched) {
        Keyword _permissionKeyword_0 = this.ga.getPermissionAccess().getPermissionKeyword_0();
        if (Objects.equal(k, _permissionKeyword_0)) {
          _matched=true;
          String _replace_51 = DocumentationReader.getFullDescription("Permission").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_51);
        }
      }
      if (!_matched) {
        Keyword _roleKeyword_0 = this.ga.getRoleAssignmentAccess().getRoleKeyword_0();
        if (Objects.equal(k, _roleKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _assignmentKeyword_1 = this.ga.getRoleAssignmentAccess().getAssignmentKeyword_1();
          if (Objects.equal(k, _assignmentKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_52 = DocumentationReader.getFullDescription("RoleAssignment").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_52);
        }
      }
      if (!_matched) {
        Keyword _modelKeyword_0 = this.ga.getModelResourceFilterAccess().getModelKeyword_0();
        if (Objects.equal(k, _modelKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _resourceKeyword_1 = this.ga.getModelResourceFilterAccess().getResourceKeyword_1();
          if (Objects.equal(k, _resourceKeyword_1)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _filterKeyword_2 = this.ga.getModelResourceFilterAccess().getFilterKeyword_2();
          if (Objects.equal(k, _filterKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_53 = DocumentationReader.getFullDescription("ModelResourceFilter").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_53);
        }
      }
      if (!_matched) {
        Keyword _serviceKeyword_0 = this.ga.getServiceResourceFilterAccess().getServiceKeyword_0();
        if (Objects.equal(k, _serviceKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _resourceKeyword_1_1 = this.ga.getServiceResourceFilterAccess().getResourceKeyword_1();
          if (Objects.equal(k, _resourceKeyword_1_1)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _filterKeyword_2_1 = this.ga.getServiceResourceFilterAccess().getFilterKeyword_2();
          if (Objects.equal(k, _filterKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_54 = DocumentationReader.getFullDescription("ServiceResourceFilter").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_54);
        }
      }
      if (!_matched) {
        Keyword _dataKeyword_0 = this.ga.getDataResourceFilterAccess().getDataKeyword_0();
        if (Objects.equal(k, _dataKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _resourceKeyword_1_2 = this.ga.getDataResourceFilterAccess().getResourceKeyword_1();
          if (Objects.equal(k, _resourceKeyword_1_2)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _filterKeyword_2_2 = this.ga.getDataResourceFilterAccess().getFilterKeyword_2();
          if (Objects.equal(k, _filterKeyword_2_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_55 = DocumentationReader.getFullDescription("DataResourceFilter").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_55);
        }
      }
      if (!_matched) {
        Keyword _softwareKeyword_0 = this.ga.getSoftwareComponentResourceFilterAccess().getSoftwareKeyword_0();
        if (Objects.equal(k, _softwareKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _resourceKeyword_1_3 = this.ga.getSoftwareComponentResourceFilterAccess().getResourceKeyword_1();
          if (Objects.equal(k, _resourceKeyword_1_3)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _filterKeyword_2_3 = this.ga.getSoftwareComponentResourceFilterAccess().getFilterKeyword_2();
          if (Objects.equal(k, _filterKeyword_2_3)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_56 = DocumentationReader.getFullDescription("SoftwareComponentResourceFilter").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_56);
        }
      }
      if (!_matched) {
        Keyword _platformKeyword_0 = this.ga.getPlatformCredentialsAccess().getPlatformKeyword_0();
        if (Objects.equal(k, _platformKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _credentialsKeyword_1 = this.ga.getPlatformCredentialsAccess().getCredentialsKeyword_1();
          if (Objects.equal(k, _credentialsKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_57 = DocumentationReader.getFullDescription("PlatformCredentials").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_57);
        }
      }
      if (!_matched) {
        Keyword _cloudKeyword_0_1 = this.ga.getCloudCredentialsAccess().getCloudKeyword_0();
        if (Objects.equal(k, _cloudKeyword_0_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _credentialsKeyword_1_1 = this.ga.getCloudCredentialsAccess().getCredentialsKeyword_1();
          if (Objects.equal(k, _credentialsKeyword_1_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_58 = DocumentationReader.getFullDescription("CloudCredentials").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_58);
        }
      }
      if (!_matched) {
        Keyword _externalKeyword_0 = this.ga.getExternalIdentifierAccess().getExternalKeyword_0();
        if (Objects.equal(k, _externalKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _identifierKeyword_1 = this.ga.getExternalIdentifierAccess().getIdentifierKeyword_1();
          if (Objects.equal(k, _identifierKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_59 = DocumentationReader.getFullDescription("ExternalIdentifier").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_59);
        }
      }
      if (!_matched) {
        Keyword _requirementKeyword_0 = this.ga.getRequirementModelAccess().getRequirementKeyword_0();
        if (Objects.equal(k, _requirementKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _modelKeyword_1 = this.ga.getRequirementModelAccess().getModelKeyword_1();
          if (Objects.equal(k, _modelKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_60 = DocumentationReader.getFullDescription("RequirementModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_60);
        }
      }
      if (!_matched) {
        Keyword _resourceKeyword_1_4 = this.ga.getResourceRequirementAccess().getResourceKeyword_1();
        if (Objects.equal(k, _resourceKeyword_1_4)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _requirementKeyword_2 = this.ga.getResourceRequirementAccess().getRequirementKeyword_2();
          if (Objects.equal(k, _requirementKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_61 = DocumentationReader.getFullDescription("ResourceRequirement").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_61);
        }
      }
      if (!_matched) {
        Keyword _osKeyword_0 = this.ga.getOSRequirementAccess().getOsKeyword_0();
        if (Objects.equal(k, _osKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _requirementKeyword_1 = this.ga.getOSRequirementAccess().getRequirementKeyword_1();
          if (Objects.equal(k, _requirementKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_62 = DocumentationReader.getFullDescription("OSRequirement").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_62);
        }
      }
      if (!_matched) {
        Keyword _providerKeyword_1 = this.ga.getProviderRequirementAccess().getProviderKeyword_1();
        if (Objects.equal(k, _providerKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _requirementKeyword_2_1 = this.ga.getProviderRequirementAccess().getRequirementKeyword_2();
          if (Objects.equal(k, _requirementKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_63 = DocumentationReader.getFullDescription("ProviderRequirement").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_63);
        }
      }
      if (!_matched) {
        Keyword _sloKeyword_0 = this.ga.getServiceLevelObjectiveAccess().getSloKeyword_0();
        if (Objects.equal(k, _sloKeyword_0)) {
          _matched=true;
          String _replace_64 = DocumentationReader.getFullDescription("ServiceLevelObjective").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_64);
        }
      }
      if (!_matched) {
        Keyword _optimisationKeyword_1 = this.ga.getOptimisationRequirementAccess().getOptimisationKeyword_1();
        if (Objects.equal(k, _optimisationKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _requirementKeyword_2_2 = this.ga.getOptimisationRequirementAccess().getRequirementKeyword_2();
          if (Objects.equal(k, _requirementKeyword_2_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_65 = DocumentationReader.getFullDescription("OptimisationRequirement").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_65);
        }
      }
      if (!_matched) {
        Keyword _securityKeyword_0_3 = this.ga.getSecurityRequirementAccess().getSecurityKeyword_0();
        if (Objects.equal(k, _securityKeyword_0_3)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _requirementKeyword_1_1 = this.ga.getSecurityRequirementAccess().getRequirementKeyword_1();
          if (Objects.equal(k, _requirementKeyword_1_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_66 = DocumentationReader.getFullDescription("SecurityRequirement").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_66);
        }
      }
      if (!_matched) {
        Keyword _locationKeyword_0 = this.ga.getLocationRequirementAccess().getLocationKeyword_0();
        if (Objects.equal(k, _locationKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _requirementKeyword_1_2 = this.ga.getLocationRequirementAccess().getRequirementKeyword_1();
          if (Objects.equal(k, _requirementKeyword_1_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_67 = DocumentationReader.getFullDescription("LocationRequirement").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_67);
        }
      }
      if (!_matched) {
        Keyword _horizontalKeyword_0 = this.ga.getHorizontalScaleRequirementAccess().getHorizontalKeyword_0();
        if (Objects.equal(k, _horizontalKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _scaleKeyword_1 = this.ga.getHorizontalScaleRequirementAccess().getScaleKeyword_1();
          if (Objects.equal(k, _scaleKeyword_1)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _requirementKeyword_2_3 = this.ga.getHorizontalScaleRequirementAccess().getRequirementKeyword_2();
          if (Objects.equal(k, _requirementKeyword_2_3)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_68 = DocumentationReader.getFullDescription("HorizontalScaleRequirement").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_68);
        }
      }
      if (!_matched) {
        Keyword _verticalKeyword_1 = this.ga.getVerticalScaleRequirementAccess().getVerticalKeyword_1();
        if (Objects.equal(k, _verticalKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _scaleKeyword_2 = this.ga.getVerticalScaleRequirementAccess().getScaleKeyword_2();
          if (Objects.equal(k, _scaleKeyword_2)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _requirementKeyword_3 = this.ga.getVerticalScaleRequirementAccess().getRequirementKeyword_3();
          if (Objects.equal(k, _requirementKeyword_3)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_69 = DocumentationReader.getFullDescription("VerticalScaleRequirement").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_69);
        }
      }
      if (!_matched) {
        Keyword _deploymentKeyword_1 = this.ga.getDeploymentTypeModelAccess().getDeploymentKeyword_1();
        if (Objects.equal(k, _deploymentKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _typeKeyword_2_3 = this.ga.getDeploymentTypeModelAccess().getTypeKeyword_2();
          if (Objects.equal(k, _typeKeyword_2_3)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _modelKeyword_3_1 = this.ga.getDeploymentTypeModelAccess().getModelKeyword_3();
          if (Objects.equal(k, _modelKeyword_3_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_70 = DocumentationReader.getFullDescription("DeploymentTypeModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_70);
        }
      }
      if (!_matched) {
        Keyword _softwareKeyword_0_1 = this.ga.getSoftwareComponentAccess().getSoftwareKeyword_0();
        if (Objects.equal(k, _softwareKeyword_0_1)) {
          _matched=true;
          String _replace_71 = DocumentationReader.getFullDescription("SoftwareComponent").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_71);
        }
      }
      if (!_matched) {
        Keyword _vMKeyword_1 = this.ga.getVMAccess().getVMKeyword_1();
        if (Objects.equal(k, _vMKeyword_1)) {
          _matched=true;
          String _replace_72 = DocumentationReader.getFullDescription("VM").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_72);
        }
      }
      if (!_matched) {
        Keyword _containerKeyword_1 = this.ga.getContainerAccess().getContainerKeyword_1();
        if (Objects.equal(k, _containerKeyword_1)) {
          _matched=true;
          String _replace_73 = DocumentationReader.getFullDescription("Container").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_73);
        }
      }
      if (!_matched) {
        Keyword _communicationKeyword_0 = this.ga.getCommunicationAccess().getCommunicationKeyword_0();
        if (Objects.equal(k, _communicationKeyword_0)) {
          _matched=true;
          String _replace_74 = DocumentationReader.getFullDescription("Communication").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_74);
        }
      }
      if (!_matched) {
        Keyword _hostingKeyword_0 = this.ga.getHostingAccess().getHostingKeyword_0();
        if (Objects.equal(k, _hostingKeyword_0)) {
          _matched=true;
          String _replace_75 = DocumentationReader.getFullDescription("Hosting").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_75);
        }
      }
      if (!_matched) {
        Keyword _providedKeyword_1 = this.ga.getProvidedCommunicationAccess().getProvidedKeyword_1();
        if (Objects.equal(k, _providedKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _communicationKeyword_2 = this.ga.getProvidedCommunicationAccess().getCommunicationKeyword_2();
          if (Objects.equal(k, _communicationKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_76 = DocumentationReader.getFullDescription("ProvidedCommunication").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_76);
        }
      }
      if (!_matched) {
        Keyword _requiredKeyword_0 = this.ga.getRequiredCommunicationAccess().getRequiredKeyword_0();
        if (Objects.equal(k, _requiredKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _communicationKeyword_1 = this.ga.getRequiredCommunicationAccess().getCommunicationKeyword_1();
          if (Objects.equal(k, _communicationKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_77 = DocumentationReader.getFullDescription("RequiredCommunication").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_77);
        }
      }
      if (!_matched) {
        Keyword _providedKeyword_1_1 = this.ga.getProvidedHostAccess().getProvidedKeyword_1();
        if (Objects.equal(k, _providedKeyword_1_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _hostKeyword_2 = this.ga.getProvidedHostAccess().getHostKeyword_2();
          if (Objects.equal(k, _hostKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_78 = DocumentationReader.getFullDescription("ProvidedHost").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_78);
        }
      }
      if (!_matched) {
        Keyword _requiredKeyword_1 = this.ga.getRequiredHostAccess().getRequiredKeyword_1();
        if (Objects.equal(k, _requiredKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _hostKeyword_2_1 = this.ga.getRequiredHostAccess().getHostKeyword_2();
          if (Objects.equal(k, _hostKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_79 = DocumentationReader.getFullDescription("RequiredHost").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_79);
        }
      }
      if (!_matched) {
        Keyword _scriptKeyword_1 = this.ga.getScriptConfigurationAccess().getScriptKeyword_1();
        if (Objects.equal(k, _scriptKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _configurationKeyword_2 = this.ga.getScriptConfigurationAccess().getConfigurationKeyword_2();
          if (Objects.equal(k, _configurationKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_80 = DocumentationReader.getFullDescription("ScriptConfiguration").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_80);
        }
      }
      if (!_matched) {
        Keyword _clusterKeyword_1 = this.ga.getClusterConfigurationAccess().getClusterKeyword_1();
        if (Objects.equal(k, _clusterKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _configurationKeyword_2_1 = this.ga.getClusterConfigurationAccess().getConfigurationKeyword_2();
          if (Objects.equal(k, _configurationKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_81 = DocumentationReader.getFullDescription("ClusterConfiguration").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_81);
        }
      }
      if (!_matched) {
        Keyword _couplingKeyword_0 = this.ga.getLocationCouplingAccess().getCouplingKeyword_0();
        if (Objects.equal(k, _couplingKeyword_0)) {
          _matched=true;
          String _replace_82 = DocumentationReader.getFullDescription("LocationCoupling").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_82);
        }
      }
      if (!_matched) {
        Keyword _requirementsKeyword_1 = this.ga.getRequirementSetAccess().getRequirementsKeyword_1();
        if (Objects.equal(k, _requirementsKeyword_1)) {
          _matched=true;
          String _replace_83 = DocumentationReader.getFullDescription("RequirementSet").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_83);
        }
      }
      if (!_matched) {
        Keyword _dataKeyword_1 = this.ga.getDataTypeModelAccess().getDataKeyword_1();
        if (Objects.equal(k, _dataKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _typeKeyword_2_4 = this.ga.getDataTypeModelAccess().getTypeKeyword_2();
          if (Objects.equal(k, _typeKeyword_2_4)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _modelKeyword_3_2 = this.ga.getDataTypeModelAccess().getModelKeyword_3();
          if (Objects.equal(k, _modelKeyword_3_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_84 = DocumentationReader.getFullDescription("DataTypeModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_84);
        }
      }
      if (!_matched) {
        Keyword _dataKeyword_1_1 = this.ga.getDataAccess().getDataKeyword_1();
        if (Objects.equal(k, _dataKeyword_1_1)) {
          _matched=true;
          String _replace_85 = DocumentationReader.getFullDescription("Data").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_85);
        }
      }
      if (!_matched) {
        Keyword _dataKeyword_0_1 = this.ga.getDataSourceAccess().getDataKeyword_0();
        if (Objects.equal(k, _dataKeyword_0_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _sourceKeyword_1 = this.ga.getDataSourceAccess().getSourceKeyword_1();
          if (Objects.equal(k, _sourceKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_86 = DocumentationReader.getFullDescription("DataSource").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_86);
        }
      }
      if (!_matched) {
        Keyword _dataKeyword_1_2 = this.ga.getDataInstanceModelAccess().getDataKeyword_1();
        if (Objects.equal(k, _dataKeyword_1_2)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _instanceKeyword_2 = this.ga.getDataInstanceModelAccess().getInstanceKeyword_2();
          if (Objects.equal(k, _instanceKeyword_2)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _modelKeyword_3_3 = this.ga.getDataInstanceModelAccess().getModelKeyword_3();
          if (Objects.equal(k, _modelKeyword_3_3)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_87 = DocumentationReader.getFullDescription("DataInstanceModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_87);
        }
      }
      if (!_matched) {
        Keyword _dataKeyword_1_3 = this.ga.getDataInstanceAccess().getDataKeyword_1();
        if (Objects.equal(k, _dataKeyword_1_3)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _instanceKeyword_2_1 = this.ga.getDataInstanceAccess().getInstanceKeyword_2();
          if (Objects.equal(k, _instanceKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_88 = DocumentationReader.getFullDescription("DataInstance").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_88);
        }
      }
      if (!_matched) {
        Keyword _dataKeyword_0_2 = this.ga.getDataSourceInstanceAccess().getDataKeyword_0();
        if (Objects.equal(k, _dataKeyword_0_2)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _sourceKeyword_1_1 = this.ga.getDataSourceInstanceAccess().getSourceKeyword_1();
          if (Objects.equal(k, _sourceKeyword_1_1)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _instanceKeyword_2_2 = this.ga.getDataSourceInstanceAccess().getInstanceKeyword_2();
          if (Objects.equal(k, _instanceKeyword_2_2)) {
            _matched=true;
          }
        }
        if (!_matched) {
          _matched=true;
        }
        if (_matched) {
          String _replace_89 = DocumentationReader.getFullDescription("DataSourceInstance").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_89);
        }
      }
      if (!_matched) {
        Keyword _scalabilityKeyword_1 = this.ga.getScalabilityModelAccess().getScalabilityKeyword_1();
        if (Objects.equal(k, _scalabilityKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _modelKeyword_2_6 = this.ga.getScalabilityModelAccess().getModelKeyword_2();
          if (Objects.equal(k, _modelKeyword_2_6)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_90 = DocumentationReader.getFullDescription("ScalabilityModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_90);
        }
      }
      if (!_matched) {
        Keyword _ruleKeyword_0 = this.ga.getScalabilityRuleAccess().getRuleKeyword_0();
        if (Objects.equal(k, _ruleKeyword_0)) {
          _matched=true;
          String _replace_91 = DocumentationReader.getFullDescription("ScalabilityRule").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_91);
        }
      }
      if (!_matched) {
        Keyword _horizontalKeyword_0_1 = this.ga.getHorizontalScalingActionAccess().getHorizontalKeyword_0();
        if (Objects.equal(k, _horizontalKeyword_0_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _scalingKeyword_1 = this.ga.getHorizontalScalingActionAccess().getScalingKeyword_1();
          if (Objects.equal(k, _scalingKeyword_1)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _actionKeyword_2 = this.ga.getHorizontalScalingActionAccess().getActionKeyword_2();
          if (Objects.equal(k, _actionKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_92 = DocumentationReader.getFullDescription("HorizontalScalingAction").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_92);
        }
      }
      if (!_matched) {
        Keyword _verticalKeyword_0 = this.ga.getVerticalScalingActionAccess().getVerticalKeyword_0();
        if (Objects.equal(k, _verticalKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _scalingKeyword_1_1 = this.ga.getVerticalScalingActionAccess().getScalingKeyword_1();
          if (Objects.equal(k, _scalingKeyword_1_1)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _actionKeyword_2_1 = this.ga.getVerticalScalingActionAccess().getActionKeyword_2();
          if (Objects.equal(k, _actionKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_93 = DocumentationReader.getFullDescription("VerticalScalingAction").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_93);
        }
      }
      if (!_matched) {
        Keyword _functionalKeyword_0 = this.ga.getFunctionalEventAccess().getFunctionalKeyword_0();
        if (Objects.equal(k, _functionalKeyword_0)) {
          _matched=true;
          String _replace_94 = DocumentationReader.getFullDescription("FunctionalEvent").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_94);
        }
      }
      if (!_matched) {
        Keyword _nonfunctionalKeyword_0 = this.ga.getNonFunctionalEventAccess().getNonfunctionalKeyword_0();
        if (Objects.equal(k, _nonfunctionalKeyword_0)) {
          _matched=true;
          String _replace_95 = DocumentationReader.getFullDescription("NonFunctionalEvent").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_95);
        }
      }
      if (!_matched) {
        Keyword _unaryKeyword_0 = this.ga.getUnaryEventPatternAccess().getUnaryKeyword_0();
        if (Objects.equal(k, _unaryKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _eventKeyword_1 = this.ga.getUnaryEventPatternAccess().getEventKeyword_1();
          if (Objects.equal(k, _eventKeyword_1)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _patternKeyword_2 = this.ga.getUnaryEventPatternAccess().getPatternKeyword_2();
          if (Objects.equal(k, _patternKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_96 = DocumentationReader.getFullDescription("UnaryEventPattern").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_96);
        }
      }
      if (!_matched) {
        Keyword _binaryKeyword_0 = this.ga.getBinaryEventPatternAccess().getBinaryKeyword_0();
        if (Objects.equal(k, _binaryKeyword_0)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _eventKeyword_1_1 = this.ga.getBinaryEventPatternAccess().getEventKeyword_1();
          if (Objects.equal(k, _eventKeyword_1_1)) {
            _matched=true;
          }
        }
        if (!_matched) {
          Keyword _patternKeyword_2_1 = this.ga.getBinaryEventPatternAccess().getPatternKeyword_2();
          if (Objects.equal(k, _patternKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_97 = DocumentationReader.getFullDescription("BinaryEventPattern").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_97);
        }
      }
      if (!_matched) {
        Keyword _timerKeyword_0 = this.ga.getTimerAccess().getTimerKeyword_0();
        if (Objects.equal(k, _timerKeyword_0)) {
          _matched=true;
          String _replace_98 = DocumentationReader.getFullDescription("Timer").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_98);
        }
      }
      if (!_matched) {
        Keyword _metadataKeyword_1 = this.ga.getMetaDataModelAccess().getMetadataKeyword_1();
        if (Objects.equal(k, _metadataKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _modelKeyword_2_7 = this.ga.getMetaDataModelAccess().getModelKeyword_2();
          if (Objects.equal(k, _modelKeyword_2_7)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_99 = DocumentationReader.getFullDescription("MetaDataModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_99);
        }
      }
      if (!_matched) {
        Keyword _conceptKeyword_0 = this.ga.getMmsConceptAccess().getConceptKeyword_0();
        if (Objects.equal(k, _conceptKeyword_0)) {
          _matched=true;
          String _replace_100 = DocumentationReader.getFullDescription("MmsConcept").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_100);
        }
      }
      if (!_matched) {
        Keyword _propertyKeyword_0 = this.ga.getMmsPropertyAccess().getPropertyKeyword_0();
        if (Objects.equal(k, _propertyKeyword_0)) {
          _matched=true;
          String _replace_101 = DocumentationReader.getFullDescription("MmsProperty").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_101);
        }
      }
      if (!_matched) {
        Keyword _conceptKeyword_0_1 = this.ga.getMmsConceptInstanceAccess().getConceptKeyword_0();
        if (Objects.equal(k, _conceptKeyword_0_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _instanceKeyword_1 = this.ga.getMmsConceptInstanceAccess().getInstanceKeyword_1();
          if (Objects.equal(k, _instanceKeyword_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_102 = DocumentationReader.getFullDescription("MmsConceptInstance").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_102);
        }
      }
      if (!_matched) {
        Keyword _propertyKeyword_0_1 = this.ga.getMmsPropertyInstanceAccess().getPropertyKeyword_0();
        if (Objects.equal(k, _propertyKeyword_0_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _instanceKeyword_1_1 = this.ga.getMmsPropertyInstanceAccess().getInstanceKeyword_1();
          if (Objects.equal(k, _instanceKeyword_1_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_103 = DocumentationReader.getFullDescription("MmsPropertyInstance").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_103);
        }
      }
      if (!_matched) {
        Keyword _camelKeyword_1 = this.ga.getCamelModelAccess().getCamelKeyword_1();
        if (Objects.equal(k, _camelKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _modelKeyword_2_8 = this.ga.getCamelModelAccess().getModelKeyword_2();
          if (Objects.equal(k, _modelKeyword_2_8)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_104 = DocumentationReader.getFullDescription("CamelModel").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_104);
        }
      }
      if (!_matched) {
        Keyword _qualityKeyword_1 = this.ga.getQualityAttribute_ImplAccess().getQualityKeyword_1();
        if (Objects.equal(k, _qualityKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _attributeKeyword_2 = this.ga.getQualityAttribute_ImplAccess().getAttributeKeyword_2();
          if (Objects.equal(k, _attributeKeyword_2)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_105 = DocumentationReader.getFullDescription("QualityAttribute").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_105);
        }
      }
      if (!_matched) {
        Keyword _attributeKeyword_1_1 = this.ga.getAttribute_ImplAccess().getAttributeKeyword_1();
        if (Objects.equal(k, _attributeKeyword_1_1)) {
          _matched=true;
          String _replace_106 = DocumentationReader.getFullDescription("Attribute").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_106);
        }
      }
      if (!_matched) {
        Keyword _measurableKeyword_1 = this.ga.getMeasurableAttributeAccess().getMeasurableKeyword_1();
        if (Objects.equal(k, _measurableKeyword_1)) {
          _matched=true;
        }
        if (!_matched) {
          Keyword _attributeKeyword_2_1 = this.ga.getMeasurableAttributeAccess().getAttributeKeyword_2();
          if (Objects.equal(k, _attributeKeyword_2_1)) {
            _matched=true;
          }
        }
        if (_matched) {
          String _replace_107 = DocumentationReader.getFullDescription("MeasurableAttribute").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_107);
        }
      }
      if (!_matched) {
        Keyword _applicationKeyword_0 = this.ga.getApplicationAccess().getApplicationKeyword_0();
        if (Objects.equal(k, _applicationKeyword_0)) {
          _matched=true;
          String _replace_108 = DocumentationReader.getFullDescription("Application").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_108);
        }
      }
      if (!_matched) {
        Keyword _actionKeyword_0 = this.ga.getActionAccess().getActionKeyword_0();
        if (Objects.equal(k, _actionKeyword_0)) {
          _matched=true;
          String _replace_109 = DocumentationReader.getFullDescription("Action").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_109);
        }
      }
      if (!_matched) {
        Keyword _featureKeyword_1 = this.ga.getFeatureAccess().getFeatureKeyword_1();
        if (Objects.equal(k, _featureKeyword_1)) {
          _matched=true;
          String _replace_110 = DocumentationReader.getFullDescription("Feature").replace("Syntax:", "</b><br><br><i>Syntax:</i><br>").replace("\n", "<br>");
          _switchResult = ("<b>" + _replace_110);
        }
      }
      if (!_matched) {
        StringConcatenation _builder = new StringConcatenation();
        _switchResult = _builder.toString();
      }
      final String result = _switchResult;
      _xblockexpression = result.toString();
    }
    return _xblockexpression;
  }
}
