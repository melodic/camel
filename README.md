This is the git repository of CAMEL which contains: (a) the ecore model of CAMEL (i.e., its meta-model); (b) the domain code mapping to the CAMEL's meta-model; (c) the CAMEL's textual editor code.

- To create the jar files mapping to CAMEL and its DSL editor (e.g., in order to programmatically exploit them), you can just execute "mvn clean install" in the parent directory.

- To be able to use the CAMEL's textual editor, you need to perform the following: 
 (a) install a latest Eclipse environment
 (b) install the EMF, OCL, & Xtext plugins
 (c) import all camel* directories in the parent directory as projects in the Eclipse Environment
 (d) go to camel.dsl directory and its src/camel.dsl sub-directory. Right-click on the GenerateCamelDsl.mwe2 file, select "Run As" and then the option "MWE2 Workflow".
 (e) once this is done, right-click on the camel.dsl project, select "Run As" and then the option "Eclipse Application". 



The editor environment will be then launched. To start playing with the editor, you would need to create a new project, generate a <name>.camel file (where <name> is a name of your choice), and then just double-click on that file in the Package Explorer. The first time this is done, you will be asked to activate the Xtext nature. Please do so and select the option not to be asked again. Once this is performed, then the actual editor will be lauched and you will be able to start editing the model you have just generated.    
